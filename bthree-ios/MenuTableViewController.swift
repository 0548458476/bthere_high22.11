//
//  MenuTableViewController.swift
//  bthree-ios
//
//  Created by User on 22.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//תפריט צד
class MenuTableViewController: UITableViewController {
    
    var revealController: SWRevealViewController!
    var frontViewController: UIViewController!
    var frontNavigationController: UINavigationController? = nil
    var storybrd: UIStoryboard = UIStoryboard(name: "SupplierExist", bundle: nil)
    var generic = Generic()
    
    var arrayString:Array<String> = [NSLocalizedString("MY_CALENDAR", comment: ""),NSLocalizedString("MY_GIVESERVICE", comment: ""),NSLocalizedString("SETTINGS", comment: ""),NSLocalizedString("ABOUT_US", comment: ""),NSLocalizedString("REGISTER_BUSINESS", comment: ""),NSLocalizedString("PRINT", comment: ""),NSLocalizedString("LOG_OUT", comment: "")]
        //[NSLocalizedString("MY_CALENDAR", comment: ""),NSLocalizedString("MY_GIVESERVICE", comment: ""),NSLocalizedString("SETTINGS", comment: ""),NSLocalizedString("ABOUT_US", comment: ""),NSLocalizedString("REGISTER_BUSINESS", comment: ""),NSLocalizedString("PRINT", comment: ""),NSLocalizedString("LOG_OUT", comment: "")]
    var arrayStringSupplierExist =
        [NSLocalizedString("MY_CALENDAR", comment: ""),NSLocalizedString("BUSINESS_PAGE", comment: ""),NSLocalizedString("CUSTOMERS", comment: ""),NSLocalizedString("SETTINGS", comment: ""),NSLocalizedString("REPORTS", comment: ""),NSLocalizedString("ABOUT_US", comment: ""),NSLocalizedString("PRINT", comment: ""),NSLocalizedString("LOG_OUT", comment: "")]
    
    var arrayImages:Array<String> = ["myCalendar@x1.png","clients@x1.png","settings@x1.png","aboutUs@x1.png","registerAsSupplier.png","print@x1.png","signOut@x1.png"]
    var arrayImagesSupplierExist:Array<String> = ["myCalendar@x1.png","bussinesPage.png","clients@x1.png","settings@x1.png","Reports.png","aboutUs@x1.png","print@x1.png","signOut@x1.png"]

    var separatorView: UIView = UIView()
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P{
            self.revealViewController().rearViewRevealWidth = self.view.frame.width - 285
        }
        else if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            self.revealViewController().rearViewRevealWidth = self.view.frame.width - 230
        }
        if Global.sharedInstance.whichReveal == false
        {
            
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .None
        tableView.scrollEnabled = false
        // self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func viewWillLayoutSubviews() {
        let bounds = UIScreen.mainScreen().bounds
        
        let width = bounds.size.width
        let height = bounds.size.height
        let y = bounds.origin.y
        //width: width*0.8
        self.view.superview!.bounds = CGRect(x:bounds.origin.x + 30 ,y:y, width: width-180,height: height
        )
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Global.sharedInstance.whichReveal == false{
            return arrayString.count
        }
        return arrayStringSupplierExist.count
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MenuItemTableViewCell = tableView.dequeueReusableCellWithIdentifier("MenuItemTableViewCell") as! MenuItemTableViewCell
        if Global.sharedInstance.whichReveal == false
        {
            cell.setDisplayData(arrayString[indexPath.row], image: arrayImages[indexPath.row])
//            if indexPath.row == 5
//            {
//            cell.viewButtom.hidden = true
//            }
//            else
//            {
                cell.viewButtom.hidden = false
//            }
        }
        else
        {
            cell.setDisplayData(arrayStringSupplierExist[indexPath.row], image: arrayImagesSupplierExist[indexPath.row])
            if indexPath.row == (arrayStringSupplierExist.count - 1)
            {
                cell.viewButtom.hidden = true
            }
            else
            {
                cell.viewButtom.hidden = false
            }
        }
        cell.selectionStyle = .None
        cell.backgroundColor = Colors.sharedInstance.color5
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        let revealController: SWRevealViewController = self.revealViewController()
        let frontViewController: UIViewController = revealController.frontViewController

        let storybrd: UIStoryboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        if (frontViewController.isKindOfClass(UINavigationController)){
            frontNavigationController = (frontViewController as AnyObject) as? UINavigationController
        }
        //שיעשה clear למי שבחרו קודם
        tableView.reloadData()

        if Global.sharedInstance.whichReveal == false//לקוח
        {
            tableView.cellForRowAtIndexPath(indexPath)!.backgroundColor = Colors.sharedInstance.color3
            
            switch indexPath.row
            {
            case 0://היומן שלי
                Global.sharedInstance.isFromMenu = true
                //
               let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                
                let CalendarSupplier: ModelCalenderViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("ModelCalenderViewController")as! ModelCalenderViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: CalendarSupplier)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)//(בגלל שלמעלה ה- animated: false)שיסגור את התפריט ולא ישאיר אותו פתוח בצד
                Global.sharedInstance.isFromPrintCalender = false
            
            case 1://נותני השרות שלי
                let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                
                let giveMyServices:giveMyServicesViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("giveMyServicesViewController")as! giveMyServicesViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: giveMyServices)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)//שיסגור את התפריט ולא ישאיר אותו פתוח בצד
                break

            case 2://הגדרות
                let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                
                let DefinationsClient: DefinationsClientViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("DefinationsClientViewController")as! DefinationsClientViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: DefinationsClient)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)//שיסגור את התפריט ולא ישאיר אותו פתוח בצד
                break

            case 3://אודותינו
                
                let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                
                let aboutUs: AboutUsClientViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("AboutUsClientViewController")as! AboutUsClientViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: aboutUs)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)//שיסגור את התפריט ולא ישאיר אותו פתוח בצד
                break
              
            case 4://הרשם כעסק
                if Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") != nil
                {
                    let dicClientName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") as! Dictionary<String,String>
                    
                    if dicClientName["nvSupplierName"] == ""//אם אינו רשום כעסק
                    {
                        let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
                        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
                        
                        viewCon.delegateFirstSection = viewCon1
                        viewCon.delegateSecond1Section = viewCon1
                        
                        
                        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
                        
                        front.pushViewController(viewCon, animated: false)
                        
                        
                        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                        
                        let mainRevealController = SWRevealViewController()
                        
                        mainRevealController.frontViewController = front
                        mainRevealController.rearViewController = rearViewController
                        
                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                        window.rootViewController = mainRevealController
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("REGISTERED_AS_BUSINESS", comment: ""), vc: self)
                    }
                }
                else //אם אינו רשום כעסק
                {
                    let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
                    let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
                    
                    viewCon.delegateFirstSection = viewCon1
                    viewCon.delegateSecond1Section = viewCon1
                    
                    
                    let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
                    
                    front.pushViewController(viewCon, animated: false)
                    
                    
                    let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = front
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                }
                
                
                
            case 5://הדפס
                
                let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                
                let printCalendar: PrintCalendarViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("PrintCalendarViewController")as! PrintCalendarViewController
                revealController.presentViewController(printCalendar, animated: true, completion: nil)
                revealController.revealToggleAnimated(true)//שיסגור את התפריט ולא ישאיר אותו פתוח בצד
                break
            case 6://התנתק
                NSUserDefaults.standardUserDefaults().removeObjectForKey("verificationPhone")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("currentClintName")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("currentUserId")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("providerDic")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("isSupplierRegistered")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("supplierNameRegistered")
                
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LogOut")
                
                NSUserDefaults.standardUserDefaults().synchronize()

                Global.sharedInstance.didOpenBusinessDetails = false
                Global.sharedInstance.getEventsFromMyCalendar()
                let dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                
                if Reachability.isConnectedToNetwork() == false
                {
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                    api.sharedInstance.GetSysAlertsList(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        let sysAlert:SysAlerts = SysAlerts()
                        
                        Global.sharedInstance.arrSysAlerts = sysAlert.sysToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                        
                        if Global.sharedInstance.arrSysAlerts.count != 0
                        {
                            Global.sharedInstance.dicSysAlerts = sysAlert.sysToDic(Global.sharedInstance.arrSysAlerts)
                            Global.sharedInstance.arrayDicForTableViewInCell[0]![1] = sysAlert.SysnvAletName(8)
                            Global.sharedInstance.arrayDicForTableViewInCell[2]![1] = sysAlert.SysnvAletName(9)
                            Global.sharedInstance.arrayDicForTableViewInCell[2]![2] = sysAlert.SysnvAletName(12)
                            Global.sharedInstance.arrayDicForTableViewInCell[3]![1] = sysAlert.SysnvAletName(10)
                            Global.sharedInstance.arrayDicForTableViewInCell[3]![2] = sysAlert.SysnvAletName(12)
                            //2do
                            //במקום זה צריך להיות textField
                            //            Global.sharedInstance.arrayDicForTableViewInCell[4]![1] = sysAlert.SysnvAletName(12)
                        }
                        
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            if AppDelegate.showAlertInAppDelegate == false
                            {
                                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                                AppDelegate.showAlertInAppDelegate = true
                            }
                    })
                }
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let frontviewcontroller = storyboard.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                let vc = storyboard.instantiateViewControllerWithIdentifier("entranceViewController") as! entranceViewController
                frontviewcontroller?.pushViewController(vc, animated: false)
                
                //initialize REAR View Controller- it is the LEFT hand menu.
                
                let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.frontViewController = frontviewcontroller
                mainRevealController.rearViewController = rearViewController
                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                window.rootViewController = mainRevealController
                window.makeKeyAndVisible()
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let initialViewController:entranceViewController = storyboard.instantiateViewControllerWithIdentifier("entranceViewController") as! entranceViewController
//                
//                let testNavigationController = UINavigationController(rootViewController: initialViewController)
//                //revealController.revealToggleAnimated(true)
//                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
//                window.rootViewController = testNavigationController
                
            default:
                break
            }
        }
        else//ספק
        {
            tableView.cellForRowAtIndexPath(indexPath)!.backgroundColor = Colors.sharedInstance.color4
            
            switch indexPath.row {
                
            case 0:
                Global.sharedInstance.isFromMenu = true
                //
                let CalendarSupplier: CalendarSupplierViewController = storybrd.instantiateViewControllerWithIdentifier("CalendarSupplierViewController")as! CalendarSupplierViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: CalendarSupplier)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)
                Global.sharedInstance.isFromPrintCalender = false
                
                //
                //    if !(frontNavigationController!.topViewController! is CalendarSupplierViewController) {
                //        var CalendarSupplier: CalendarSupplierViewController = storybrd.instantiateViewControllerWithIdentifier("CalendarSupplierViewController")as! CalendarSupplierViewController
                //        frontNavigationController!.pushViewController(CalendarSupplier, animated: true)
                //    }
            //    revealController.revealToggleAnimated(true)
            case 2:
                //                if !(frontNavigationController!.topViewController! is MyCostumersViewController) {
                //                    var MyCostumers: MyCostumersViewController = storybrd.instantiateViewControllerWithIdentifier("MyCostumersViewController")as! MyCostumersViewController
                //                    frontNavigationController!.pushViewController(MyCostumers, animated: true)
                //                }
                //                revealController.revealToggleAnimated(true)
                let MyCostumers: MyCostumersViewController = storybrd.instantiateViewControllerWithIdentifier("MyCostumersViewController")as! MyCostumersViewController
                
                let navigationController: UINavigationController = UINavigationController(rootViewController: MyCostumers)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)
            case 3:
                let DefinationsController: DefinationsViewController = storybrd.instantiateViewControllerWithIdentifier("DefinationsViewController")as! DefinationsViewController
                
                let navigationController: UINavigationController = UINavigationController(rootViewController: DefinationsController)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)
                
                //      if !(frontNavigationController!.topViewController! is DefinationsViewController) {
                //        var DefinationsController: DefinationsViewController = storybrd.instantiateViewControllerWithIdentifier("DefinationsViewController")as! DefinationsViewController
                //        frontNavigationController!.pushViewController(DefinationsController, animated: true)
                //    }
            //    revealController.revealToggleAnimated(true)
            case 4:
                let ReportsController: ReportViewController = storybrd.instantiateViewControllerWithIdentifier("ReportViewController")as! ReportViewController
                
                let navigationController: UINavigationController = UINavigationController(rootViewController: ReportsController)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)
            case 5:
                let AboutUsController: AboutUsViewController = storybrd.instantiateViewControllerWithIdentifier("AboutUsViewController")as! AboutUsViewController
                
                let navigationController: UINavigationController = UINavigationController(rootViewController: AboutUsController)
                revealController.pushFrontViewController(navigationController, animated: false)
               revealController.revealToggleAnimated(true)
            case 6:
                let CalendarSupplier: CalendarSupplierViewController = storybrd.instantiateViewControllerWithIdentifier("CalendarSupplierViewController")as! CalendarSupplierViewController
                let navigationController: UINavigationController = UINavigationController(rootViewController: CalendarSupplier)
                revealController.pushFrontViewController(navigationController, animated: false)
                revealController.revealToggleAnimated(true)
                Global.sharedInstance.isFromPrintCalender = true
            
            case 7:
                
                NSUserDefaults.standardUserDefaults().removeObjectForKey("verificationPhone")
                
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LogOut")
                
                NSUserDefaults.standardUserDefaults().synchronize()
            
                Global.sharedInstance.didOpenBusinessDetails = false
                
                 Global.sharedInstance.getEventsFromMyCalendar()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController:entranceViewController = storyboard.instantiateViewControllerWithIdentifier("entranceViewController") as! entranceViewController
                
                let testNavigationController = UINavigationController(rootViewController: initialViewController)
                
                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                window.rootViewController = testNavigationController
            default: break
                
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if Global.sharedInstance.whichReveal == false
        {
            return tableView.frame.height / 7
        }
        return tableView.frame.height / 8
    }
    
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
    
    func gradientLine(cellView:UIView,width:CGFloat,height:CGFloat)
    {
        let lineForCells:UIImageView = UIImageView(frame: CGRectMake(0,height - 1, width  , 1.5))
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [
            UIColor.clearColor().CGColor,
            UIColor.whiteColor().CGColor,
            UIColor.clearColor().CGColor]
        gradient.locations = [0.0, 0.5, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: width - 100, height: 2)
        
        lineForCells.layer.insertSublayer(gradient, atIndex: 0)
        cellView.addSubview(lineForCells)
    }
}
