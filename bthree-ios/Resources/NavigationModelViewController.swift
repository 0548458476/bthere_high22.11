//
//  NavigationModelViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 2/17/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import Social

class NavigationModelViewController: UIViewController {
    
    //MARK: - Properties
    
    var backButton: UIBarButtonItem?
    var backButtonleft: UIBarButtonItem?
    var storyBoard1:UIStoryboard?
    var supplierStoryBoard:UIStoryboard?
    var button:UIButton = UIButton()

    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppDelegate.isDeviceLanguageRTL()
        {
            Global.sharedInstance.rtl = true
        }
        else
        {
            Global.sharedInstance.rtl = false
        }
        
        if self.revealViewController() != nil
        {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        supplierStoryBoard = UIStoryboard(name: "SupplierExist", bundle: nil)
        storyBoard1 = UIStoryboard(name: "Main", bundle: nil)
        let menueBtn: UIButton = UIButton(type: .Custom)
        let menueBtnImage: UIImage = UIImage(named: "icon-menu.png")!
        
        menueBtn.setBackgroundImage(menueBtnImage, forState: .Normal)
        
        menueBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: .TouchUpInside)

        menueBtn.addTarget(self, action: #selector(self.openSideMenu), forControlEvents: .TouchUpInside)
        
        menueBtn.frame = CGRectMake(-11, -7, 35, 28)

        let backButtonView: UIView = UIView(frame: CGRectMake(0, -15, 44, 34))
        backButtonView.bounds = CGRectOffset(backButtonView.bounds, -14, -7)
        backButtonView.addSubview(menueBtn)
        
        backButton = UIBarButtonItem(customView: backButtonView)
       // self.navigationItem.rightBarButtonItem = backButton
        
        
        
         if Global.sharedInstance.rtl
         {
            self.navigationItem.rightBarButtonItem = backButton

         }
         else
         {
            self.navigationItem.leftBarButtonItem = backButton
         }
        

        
        //Bthere picture
        let backView: UIView = UIView(frame: CGRectMake(0, 0, 120, 40))
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "3.png"))
        titleImageView.frame = CGRectMake(0, 0, titleImageView.frame.size.width * 0.8, 40)
        // Here I am passing origin as (45,5) but can pass them as your requirement.
        backView.addSubview(titleImageView)
        self.navigationItem.titleView = backView
        
        //label to show name of client or supplier
        let navLabel: UILabel = UILabel(frame: CGRectMake(self.view.frame.width - 90, 19, 90, 30))
        navLabel.textColor = UIColor.redColor()
        navLabel.textAlignment = .Center
        self.navigationController?.navigationBar.addSubview(navLabel)
        navLabel.backgroundColor = UIColor.clearColor()
        navLabel.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
        
       
        
        let leftbtn: UIButton = UIButton(type: .Custom)
        let leftbtnImage: UIImage?
        
        
        if Global.sharedInstance.isProvider == true
        {
            navLabel.textColor = Colors.sharedInstance.color4
            leftbtnImage = UIImage(named: "user-2.png")!
            
            if Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") != nil {
                
                var dicSupplierName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") as! Dictionary<String,String>
                if dicSupplierName["nvSupplierName"] != ""
                {
                    navLabel.text = dicSupplierName["nvSupplierName"]
                    //Global.sharedInstance.currentProvider.nvSupplierName
                }
                else
                {
                    navLabel.text = NSLocalizedString("NAME_BUSINESS", comment: "")
                }
            }
            else
            {
                navLabel.text = NSLocalizedString("NAME_BUSINESS", comment: "")
            }
            
        }
        else
        {
            navLabel.textColor = Colors.sharedInstance.color3
            leftbtnImage = UIImage(named: "4.png")!
            
            if Global.sharedInstance.defaults.valueForKey("currentClintName") != nil
            {
                
                var dicClientName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("currentClintName") as! Dictionary<String,String>
                if dicClientName["nvClientName"] != ""
                {
                    navLabel.text = dicClientName["nvClientName"]//Global.sharedInstance.currentUser.nvFirstName
                }
                else
                {
                    navLabel.text = NSLocalizedString("NAME_CUSTOMER", comment: "")
                }
            }
            else
            {
//מצב צפיה - כפתור חזור למסך כניסה למקרה שירצה להכנס כלקוח קיים
                self.navigationItem.rightBarButtonItem = nil
                self.navigationItem.leftBarButtonItem = nil

                let leftToViewMode: UIButton = UIButton(type: .Custom)
                leftToViewMode.setTitle("<", forState: .Normal)
                leftToViewMode.titleLabel?.font = UIFont(name: "OpenSansHebrew-Bold", size: 25)
                leftToViewMode.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                leftToViewMode.addTarget(self, action: #selector(NavigationModelViewController.goToEntrance), forControlEvents: .TouchUpInside)
                leftToViewMode.frame = CGRectMake(self.view.frame.size.width - 50, 0, 50, 50)
                self.navigationController?.navigationBar.addSubview(leftToViewMode)
                
                button = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: -15, width: 90, height: 60))
                button.backgroundColor = UIColor.clearColor()
                //button.setTitle("", forState: .Normal)
                button.addTarget(self, action: #selector(NavigationModelViewController.goToEntrance), forControlEvents: .TouchUpInside)
                self.navigationController?.navigationBar.addSubview(button)
                self.navigationItem.setHidesBackButton(true, animated:true)
            }
        }
              if Global.sharedInstance.defaults.valueForKey("currentClintName") != nil
        {
        
        leftbtn.setBackgroundImage(leftbtnImage, forState: .Normal)
        leftbtn.addTarget(self, action: #selector(NavigationModelViewController.providerCustomer), forControlEvents: .TouchUpInside)
        leftbtn.frame = CGRectMake(-30, -15, 30, 20)
        let backButtonV: UIView = UIView(frame: CGRectMake(0, -15, 40, 20))
        backButtonV.bounds = CGRectOffset(backButtonView.bounds, -14, -7)
        
        backButtonV.addSubview(leftbtn)
        backButtonleft = UIBarButtonItem(customView: backButtonV)
      //  self.navigationItem.leftBarButtonItem = backButtonleft
        
            
            if Global.sharedInstance.rtl
            {
                self.navigationItem.leftBarButtonItem = backButtonleft
                // button = UIButton(frame: CGRect(x: 0, y: -15, width: 90, height: 60))
                button = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: -15, width: 90, height: 60))

            }
            else
            {
                 button = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: -15, width: 90, height: 60))
                self.navigationItem.rightBarButtonItem = backButtonleft
            }

     //   button = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: -15, width: 90, height: 60))
        button.backgroundColor = UIColor.clearColor()
        //button.setTitle("", forState: .Normal)
        button.addTarget(self, action: #selector(NavigationModelViewController.providerCustomer), forControlEvents: .TouchUpInside)
        self.navigationController?.navigationBar.addSubview(button)
            
            /*
            if Global.sharedInstance.rtl
            {
                self.navigationItem.setLeftBarButtonItems([backButton!], animated: true)
            }
            else
            {
                self.navigationItem.setRightBarButtonItems([backButtonleft!], animated: true)
            }
 */

        }
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        
        

    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //move to customer or provider
    //when click on image provider/customer in top
    func providerCustomer()  {

        if Global.sharedInstance.isProvider == true//רוצה לעבור מספק ללקוח
        {
            //לא צריך לשאול שום שאלה כי ברור שהוא רשום כלקוח וממילא עוברים ללקוח קיים
            Global.sharedInstance.isProvider = false
            
            let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
            let vc = storyBoard1!.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
            frontviewcontroller?.pushViewController(vc, animated: false)
            
            Global.sharedInstance.whichReveal = false
            //initialize REAR View Controller- it is the LEFT hand menu.
            let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
            
            let mainRevealController = SWRevealViewController()
            
            mainRevealController.frontViewController = frontviewcontroller
            mainRevealController.rearViewController = rearViewController
            
            let window:UIWindow = UIApplication.sharedApplication().keyWindow!
            window.rootViewController = mainRevealController
        }
        else//רוצה לעבור מלקוח לספק
        {
            //צריך לבדוק האם הוא רשום כספק
            if Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") != nil
            {
                var dicClientName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") as! Dictionary<String,String>
                //רק אם רשום כספק ,אחרת לא קורה כלום!
                if dicClientName["nvSupplierName"] != ""
                {
                    Global.sharedInstance.isProvider = true
                    
                    let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                    let vc = supplierStoryBoard!.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
                    Global.sharedInstance.whichReveal = true
                    frontviewcontroller?.pushViewController(vc, animated: false)
                    
                    // initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                }
                else
                {
                    Alert.sharedInstance.showAlert(NSLocalizedString("NOT_SUPPLIER_REGISTER", comment: ""), vc: self)
                }
            }
            else
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("NOT_SUPPLIER_REGISTER", comment: ""), vc: self)
            }
        }
    }
    func openSideMenu()
    {
        view.endEditing(true)
    }
    
    func goToEntrance()
    {
        let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyBoard1!.instantiateViewControllerWithIdentifier("entranceViewController") as! entranceViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        Global.sharedInstance.whichReveal = false
        //initialize REAR View Controller- it is the LEFT hand menu.
        let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window:UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }


}
