//
//  ProviderDetailsObj.swift
//  Bthere
//
//  Created by User on 23.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//contain all provider details
class ProviderDetailsObj: NSObject{
    
    //MARK: - Variables
    
    var userObj:objUsers = objUsers()
    
    var objCustomer:Array<CustomerObj> = Array<CustomerObj>()
    
    var providerBuisnessDetailsObj:Provider = Provider()
    
    var providerGeneralDetailsObj:objGeneralDetails = objGeneralDetails()
    
    var providerAlertsSettingsObj:AddProviderAlertSettings = AddProviderAlertSettings()
    
    var providerProfileObj:AddProviderBuisnessProfile = AddProviderBuisnessProfile()
    
    //MARK: - Initial
    
    override init() {
        userObj = objUsers()
        
        providerBuisnessDetailsObj = Provider()
        
        providerGeneralDetailsObj = objGeneralDetails()
        
        providerAlertsSettingsObj = AddProviderAlertSettings()
        
        providerProfileObj = AddProviderBuisnessProfile()
    }
    
    init(_userObj:objUsers,_providerBuisnessDetailsObj:Provider,_providerGeneralDetailsObj:objGeneralDetails,_providerAlertsSettingsObj:AddProviderAlertSettings,_providerProfileObj:AddProviderBuisnessProfile)
    {
        userObj = _userObj
        
        providerBuisnessDetailsObj = _providerBuisnessDetailsObj
        
        providerGeneralDetailsObj = _providerGeneralDetailsObj
        
        providerAlertsSettingsObj = _providerAlertsSettingsObj
        
        providerProfileObj = _providerProfileObj
    }
    
    //MARK: - Functions
    
    //get: dictionary to convert,returns: ProviderDetailsObj from the dictionary
    func dicToProviderDetailsObj(dic:Dictionary<String,AnyObject>)->ProviderDetailsObj
    {
        let providerDetailsObj:ProviderDetailsObj = ProviderDetailsObj()
        
        if dic["objProviderBuisnessDetails"] as? NSDictionary != nil
        {
            providerDetailsObj.providerBuisnessDetailsObj =  providerDetailsObj.providerBuisnessDetailsObj.dicToProvider(dic["objProviderBuisnessDetails"] as! Dictionary<String,AnyObject>)
        }
        if dic["objProviderGeneralDetails"] as? NSDictionary != nil
        {
        providerDetailsObj.providerGeneralDetailsObj = providerDetailsObj.providerGeneralDetailsObj.dicToObjGeneralDetails(dic["objProviderGeneralDetails"] as! Dictionary<String,AnyObject>)
        
        }
        if dic["objProviderAlertsSettings"] as? NSDictionary != nil
        {
            providerDetailsObj.providerAlertsSettingsObj = providerDetailsObj.providerAlertsSettingsObj.dicToaddProviderAlertSettings(dic["objProviderAlertsSettings"] as! Dictionary<String,AnyObject>)
        }
        if dic["objProviderProfile"] as? NSDictionary != nil
        {
        providerDetailsObj.providerProfileObj = providerDetailsObj.providerProfileObj.dicToAddProviderBuisnessProfile(dic["objProviderProfile"] as! Dictionary<String,AnyObject>)
        }
        if dic["objUser"] as? NSDictionary != nil
        {
        providerDetailsObj.userObj = providerDetailsObj.userObj.dicToObjUsers(dic["objUser"] as! Dictionary<String,AnyObject>)
        }
        
        if dic["objCustomer"] as? NSDictionary != nil
        {
            let customer:CustomerObj = CustomerObj()
            providerDetailsObj.objCustomer = customer.customerObjToArray(dic["objCustomer"] as! Array<Dictionary<String,AnyObject>>)
        }
        
        return providerDetailsObj
    }
    
}
