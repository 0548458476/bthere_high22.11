
//verificationPhone
//  AppDelegate.swift
//  bthree-ios
//
//  Created by Lior Ronen on 2/9/16.
//  Copyright © 2016 Webit. All rights reserved.
//
import UIKit
import FBSDKCoreKit
import Foundation
import EventKit
import EventKitUI
import Security
import Fabric
import Crashlytics

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var languageBundle = NSBundle()
    var fromBackGround = false
    static var showAlertInAppDelegate = false//מציין האם כבר הוצגה הודעה אחת בappDelegate כדי למנוע כפילות הודעות
    
    var window: UIWindow?
    var screenBounds = UIScreen.mainScreen().bounds
    static var y = 0
    static var isOpenHoursWhichCell = false
    static var x = 0
    static var isFirstWorker = true
    static var isBtnCheck = false
    static var isDidReload = false
    static var isOpenHoursForNewService = false
    static var i = 0
    static var isFirst = false

    var locationManager = CLLocationManager()
    
    //שומר את כל מה שחוזר מהפונקציה:GetFieldsAndCatg
    static var arrDomains:Array<Domain> = Array<Domain>()
    static var arrDomainFilter:Array<Domain> = Array<Domain>()

    static var countCellEditOpenService = 0
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
        if AppDelegate.isDeviceLanguageRTL()
        {
            Global.sharedInstance.rtl = true
        }
        else
        {
            Global.sharedInstance.rtl = false
        }

        fromBackGround = false
        if CLLocationManager.locationServicesEnabled() {
            /*
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.pausesLocationUpdatesAutomatically = false
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
            */
            if  #available(iOS 10.0, *){
                if CLLocationManager.authorizationStatus() == .NotDetermined {
                    locationManager.requestAlwaysAuthorization()
                    self.locationManager.startUpdatingLocation()
                }
            }
            else
            {
                if self.locationManager.respondsToSelector(#selector(self.locationManager.requestAlwaysAuthorization)) {
                    self.locationManager.requestAlwaysAuthorization()
                    
                    
                    self.locationManager = CLLocationManager()
                    self.locationManager.delegate = self
                    self.locationManager.pausesLocationUpdatesAutomatically = false
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
 
                }
                self.locationManager.startUpdatingLocation()
            }
        }
        self.locationManager.stopMonitoringSignificantLocationChanges()
      /*
        if  #available(iOS 10.0, *){
            if CLLocationManager.authorizationStatus() == .NotDetermined {
                locationManager.requestAlwaysAuthorization()
               // self.locationManager.startUpdatingLocation()
                 self.locationManager.requestWhenInUseAuthorization()
            }
        }

        else
        {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
       self.locationManager.requestWhenInUseAuthorization()//2.3
        }

        //Check for location enabled on device
       // else
        //{
        if CLLocationManager.locationServicesEnabled()
        {
            if #available(iOS 9.0, *) {
                self.locationManager.allowsBackgroundLocationUpdates = true
            } else {
                // Fallback on earlier versions
            }
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //}
        */
        NSUserDefaults.standardUserDefaults().synchronize()

        application.statusBarHidden = true
        
//get the events from my calender
        Global.sharedInstance.getEventsFromMyCalendar()
      
        //מחיקה מהמכשיר
        
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("verificationPhone")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("currentClintName")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("currentUserId")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("providerDic")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("isSupplierRegistered")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("supplierNameRegistered")
//        
//        NSUserDefaults.standardUserDefaults().synchronize()
//

        //delete it
        let dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        if Reachability.isConnectedToNetwork() == false
        {
            Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        }
        else
        {
            api.sharedInstance.GetFieldsAndCatg(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                let domain:Domain = Domain()
                
                AppDelegate.arrDomains = domain.domainToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                
                if AppDelegate.arrDomains.count == 0
                {
                    Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
                    
                }
                var idLast = -1
                for domain in AppDelegate.arrDomains
                {
                    //הבדיקה היא כדי שלא יהיו כפולים כאשר נגשנו לשדה nvFieldName
                    //                if domain.iFieldRowId != idLast || idLast == -1
                    //                {
                    AppDelegate.arrDomainFilter.append(domain)
                    // }
                    //idLast = domain.iFieldRowId
                }
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                         AppDelegate.showAlertInAppDelegate = true
                    }
            })
            
            let dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            api.sharedInstance.GetSysAlertsList(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                let sysAlert:SysAlerts = SysAlerts()
                
                Global.sharedInstance.arrSysAlerts = sysAlert.sysToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                
                if Global.sharedInstance.arrSysAlerts.count != 0
                {
                    Global.sharedInstance.dicSysAlerts = sysAlert.sysToDic(Global.sharedInstance.arrSysAlerts)
                    Global.sharedInstance.arrayDicForTableViewInCell[0]![1] = sysAlert.SysnvAletName(8)
                    Global.sharedInstance.arrayDicForTableViewInCell[2]![1] = sysAlert.SysnvAletName(9)
                    Global.sharedInstance.arrayDicForTableViewInCell[2]![2] = sysAlert.SysnvAletName(12)
                    Global.sharedInstance.arrayDicForTableViewInCell[3]![1] = sysAlert.SysnvAletName(10)
                    Global.sharedInstance.arrayDicForTableViewInCell[3]![2] = sysAlert.SysnvAletName(12)
                    //2do
                    //במקום זה צריך להיות textField
                    //            Global.sharedInstance.arrayDicForTableViewInCell[4]![1] = sysAlert.SysnvAletName(12)
                }
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
            
            if Global.sharedInstance.defaults.valueForKey("currentUserId") != nil
            {
                var dicUserId:Dictionary<String,AnyObject> = Global.sharedInstance.defaults.valueForKey("currentUserId") as! Dictionary<String,AnyObject>
                
                if dicUserId["currentUserId"] as! Int != 0
                {
                    //קבלת פרטי הלקוח
                    var dicForServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    dicForServer["iUserId"] = dicUserId["currentUserId"] as! Int
                    
                    api.sharedInstance.GetCustomerDetails(dicForServer, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                        {
                            var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                            
                            dicForDefault["nvClientName"] = responseObject["Result"]!["nvFirstName"]
                            //שמירת שם הלקוח במכשיר
                            Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "currentClintName")
                            
                            var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                            
                            dicUserId["currentUserId"] = responseObject["Result"]!["iUserId"]
                            //save the userId on device
                            Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                            
                            Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
                            //בגלל שנשמר יום אחד קודם צריך להוסיף 3 שעות(זה לא עזר להוסיף timeZone)
                            Global.sharedInstance.currentUser.dBirthdate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dBirthdate
                                , options: [])!
                            Global.sharedInstance.currentUser.dMarriageDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dMarriageDate
                                , options: [])!
                        }
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            if AppDelegate.showAlertInAppDelegate == false
                            {
                                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                                AppDelegate.showAlertInAppDelegate = true
                            }
                    })
                }
                
                //קבלת פרטי הספק
                api.sharedInstance.getProviderAllDetails(dicUserId["currentUserId"] as! Int)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let frontviewcontroller = storyboard.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                let vc = storyboard.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
                frontviewcontroller?.pushViewController(vc, animated: false)
                
                //initialize REAR View Controller- it is the LEFT hand menu.
                
                let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.frontViewController = frontviewcontroller
                mainRevealController.rearViewController = rearViewController
                
                self.window!.rootViewController = mainRevealController
                self.window?.makeKeyAndVisible()
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let frontviewcontroller = storyboard.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                let vc = storyboard.instantiateViewControllerWithIdentifier("entranceViewController") as! entranceViewController
                frontviewcontroller?.pushViewController(vc, animated: false)
                
                //initialize REAR View Controller- it is the LEFT hand menu.
                
                let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.frontViewController = frontviewcontroller
                mainRevealController.rearViewController = rearViewController
                
                self.window!.rootViewController = mainRevealController
                self.window?.makeKeyAndVisible()
            }
        }
                application.registerForRemoteNotifications()
        //note: the project is registered from eztaxi10 mail and the name of the project in apis console is:bthere-31-10-16
        GMSServices.provideAPIKey("AIzaSyCZFlWab7bPnumji9Ia47JSl3hrbWsZxRY")
        //before the new key in 31-10-16
        //GMSServices.provideAPIKey("AIzaSyAMB_zlzBk2-_URbKg8wqvgrvkLN0Y5Kog")
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions:launchOptions)
        Fabric.with([Crashlytics.self])

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        if Global.sharedInstance.isGooglePlusChecked == true
        {
            Global.sharedInstance.isGooglePlusChecked = false
            return GPPURLHandler.handleURL(url, sourceApplication:sourceApplication, annotation: annotation)
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        self.locationManager.startMonitoringSignificantLocationChanges()
        print("entered background Mode")
    }
    func applicationWillEnterForeground(application: UIApplication) {
          
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
        
        if CLLocationManager.locationServicesEnabled() {
            /*
             self.locationManager = CLLocationManager()
             self.locationManager.delegate = self
             self.locationManager.pausesLocationUpdatesAutomatically = false
             self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
             // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
             */
            if  #available(iOS 10.0, *){
                if CLLocationManager.authorizationStatus() == .NotDetermined {
                    locationManager.requestAlwaysAuthorization()
                    self.locationManager.startUpdatingLocation()
                }
            }
            else
            {
                if self.locationManager.respondsToSelector(#selector(self.locationManager.requestAlwaysAuthorization)) {
                    self.locationManager.requestAlwaysAuthorization()
                    
                    
                    self.locationManager = CLLocationManager()
                    self.locationManager.delegate = self
                    self.locationManager.pausesLocationUpdatesAutomatically = false
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    
                    
                }
                self.locationManager.startUpdatingLocation()
            }
        }
        self.locationManager.stopMonitoringSignificantLocationChanges()
        
        
        
        /*
             if CLLocationManager.locationServicesEnabled() {
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.pausesLocationUpdatesAutomatically = false
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
         
            if  #available(iOS 10.0, *){
                if CLLocationManager.authorizationStatus() == .NotDetermined {
                    locationManager.requestAlwaysAuthorization()
                    self.locationManager.startUpdatingLocation()
                }
            }
            else
            {
                if self.locationManager.respondsToSelector(#selector(self.locationManager.requestAlwaysAuthorization)) {
                    self.locationManager.requestAlwaysAuthorization()
                }
                self.locationManager.startUpdatingLocation()
            }
                    }
        self.locationManager.stopMonitoringSignificantLocationChanges()
        */
        
        if Global.sharedInstance.isSettingsOpen == true
        {
            fromBackGround = true
        }
    }
    
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        Global.sharedInstance.currentLat = locValue.latitude
        Global.sharedInstance.currentLong = locValue.longitude
        
        if fromBackGround == true
        {
            fromBackGround = false
            if Global.sharedInstance.searchResult != nil
            {
                if Global.sharedInstance.currentLat != nil
                {
                    Global.sharedInstance.searchResult?.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
                    Global.sharedInstance.searchResult?.imgCircleDistance.hidden = false
                    Global.sharedInstance.searchResult?.imgCircle.hidden = true
                    Global.sharedInstance.searchResult?.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
                    
                    Global.sharedInstance.dicSearch["nvlong"] = Global.sharedInstance.currentLong
                    Global.sharedInstance.dicSearch["nvlat"] = Global.sharedInstance.currentLat
                    
                        api.sharedInstance.SearchByKeyWord(Global.sharedInstance.dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                            
                            if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                            {
                                Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                                Global.sharedInstance.dicSearchProviders = Global.sharedInstance.dicSearch
                                
                                /////
                                var dicPositiveDistance:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
                                
                                var dicNegativeDistance:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
                                
                                for result in Global.sharedInstance.dicResults {
                                    if (result["iDistance"] as! Int) == -1
                                    {
                                        dicNegativeDistance.append(result)
                                    }
                                    else
                                    {
                                        dicPositiveDistance.append(result)
                                    }
                                }
                                dicPositiveDistance.sortInPlace{
                                    (($0 )["iDistance"] as? Float) < (($1 )["iDistance"] as? Float)
                                }
                                Global.sharedInstance.dicResults = dicPositiveDistance
                                
                                for item in dicNegativeDistance
                                {
                                    Global.sharedInstance.dicResults.append(item)
                                }
                                /////
                                
                                Global.sharedInstance.searchResult?.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
                                Global.sharedInstance.searchResult?.imgCircleDistance.hidden = false
                                Global.sharedInstance.searchResult?.imgCircle.hidden = true
                                Global.sharedInstance.searchResult?.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
                                
                                Global.sharedInstance.searchResult?.lblResultCount.text = "\(Global.sharedInstance.dicResults.count) תוצאות חיפוש"
                                Global.sharedInstance.searchResult?.tblResults.reloadData()
                            }
                            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        })
                }
                else
                {
                    //פתיחת דף שנה מיקום
                    let viewCon:ChangeCityViewController = Global.sharedInstance.searchResult!.storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                    viewCon.delegate = Global.sharedInstance.searchResult
                    viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                    Global.sharedInstance.searchResult!.presentViewController(viewCon, animated: true, completion: nil)
                }
            }
        }
    }
    
    func changeLanguage(lang:String)
    {
        NSUserDefaults.standardUserDefaults().synchronize()
        NSUserDefaults.standardUserDefaults().setObject([lang], forKey: "AppleLanguages")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        
//        NSUserDefaults.standardUserDefaults().synchronize()
//        NSUserDefaults.standardUserDefaults().setObject([lang], forKey: "AppleLanguages")
//        
//        NSUserDefaults.standardUserDefaults().synchronize()
//        let language: String = NSLocale.preferredLanguages()[0]
//        print(language)
//        
//        let path = NSBundle.mainBundle().pathForResource(lang, ofType: "lproj")
//        let bundle = NSBundle(path: path!)
//        
//        languageBundle = NSBundle(path: NSBundle.mainBundle().pathForResource(NSLocale.preferredLanguages()[0], ofType: "lproj")!)!
    }
    class func isDeviceLanguageRTL() -> Bool {
        //  NSLocale.characterDirectionForLanguage(<#T##isoLangCode: String##String#>)
        return (NSLocale.characterDirectionForLanguage(NSLocale.preferredLanguages()[0]) == .RightToLeft)
    }
  
}
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
