//
//  MenuItemTableViewCell.swift
//  bthree-ios
//
//  Created by User on 22.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var di: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if DeviceType.IS_IPHONE_6{
      di.font = di.font.fontWithSize(16)
        }
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var viewButtom: UIView!
    func setDisplayData(str:String,image:String){
         di.lineBreakMode = NSLineBreakMode.ByWordWrapping
        di.numberOfLines = 2

        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
di.font = UIFont(name: di.font.fontName, size: 11)
        }
        di.text = str
        img.image = UIImage(named: image)
        //img.
    }

}
