//
//  Contact.swift
//  bthree-ios
//
//  Created by User on 11.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class Contact: NSObject {

    var iUserId : Int!
    var iUserStatusType : Int!
    var nvContactName : String = ""
    var nvPhone : String = ""
    var bIsNegotiableOnly : Bool!
    var bPostdatedCheck  = false
    var bIsVCheckMember = false
    var bIsSync = true
    
    override init() {
        
        iUserId = 0
        iUserStatusType = 0
        nvContactName = ""
        nvPhone = ""
        bIsNegotiableOnly = false
        bPostdatedCheck  = false
        bIsVCheckMember = false
        bIsSync = true
        
    }
    init(_iUserId : Int, _iUserStatusType : Int, _nvContactName : String, _nvPhone : String, _bIsVCheckMamber : Bool, _bIsNegotiableOnly : Bool, _bPostdatedCheck : Bool,_bIsSync:Bool)
    {
        iUserId = _iUserId
        iUserStatusType = _iUserStatusType
        nvContactName = _nvContactName
        nvPhone = _nvPhone
        bIsVCheckMember = _bIsVCheckMamber
        bIsNegotiableOnly = _bIsNegotiableOnly
        bPostdatedCheck = _bPostdatedCheck
        bIsSync = _bIsSync
    }
    
    init(contactDic : Dictionary<String, AnyObject>) {
        iUserId = contactDic["iUserId"] as! Int
        iUserStatusType = contactDic["iUserStatusType"] as! Int
        nvContactName = contactDic["nvContactName"] as! String
        nvPhone = contactDic["nvPhone"] as! String
        bIsVCheckMember = contactDic["bIsVCheckMember"] as! Bool
        bIsNegotiableOnly = contactDic["bIsNegotiableOnly"] as! Bool
        bPostdatedCheck = contactDic["bPostdatedCheck"] as! Bool
        bIsSync = contactDic["bIsSync"] as! Bool
    }
    
    func getContactDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iUserId"] = self.iUserId
        dic["iUserStatusType"] = self.iUserStatusType
        dic["nvContactName"] = self.nvContactName
        dic["nvPhone"] = self.nvPhone
        dic["bIsVCheckMember"] = self.bIsVCheckMember
        dic["bIsNegotiableOnly"] = self.bIsNegotiableOnly
        dic["bPostdatedCheck"] = self.bPostdatedCheck
        dic["bIsSync"] = self.bIsSync
        
        return dic
    }
//    func getContactFromJson(dicArray:NSDictionary)-> Contact
//    {
//        let contact:Contact = Contact()
//        contact.nvPhone = Global.sharedInstance.parseJsonToString(dicArray["nvPhone"]!)
//        contact.nvContactName = Global.sharedInstance.parseJsonToString(dicArray["nvContactName"]!)
//        contact.iUserId = Global.sharedInstance.parseJsonToInt(dicArray["iUserId"]!)
//        contact.iUserStatusType = Global.sharedInstance.parseJsonToInt(dicArray["iUserStatusType"]!)
//        contact.bIsVCheckMember = dicArray["bIsVCheckMember"] as! Bool
//        contact.bIsNegotiableOnly = dicArray["bIsNegotiableOnly"] as! Bool
//        contact.bPostdatedCheck = dicArray["bPostdatedCheck"] as! Bool
//        return contact
//    }
    
}
