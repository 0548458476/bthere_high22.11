//
//  objCalendarProperties.swift
//  bthree-ios
//
//  Created by User on 5.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class objCalendarProperties: NSObject {
    
    var iFirstCalendarViewType:Int = 0
    var dtCalendarOpenDate:String = String()
    var bLimitSeries:Bool = false
    var iMaxServiceForCustomer:Int = 0
    var iPeriodInWeeksForMaxServices:Int = 0
    var bSyncGoogleCalendar = false
    
    
    
    override init() {
        
        iFirstCalendarViewType = 0
        dtCalendarOpenDate = ""
        bLimitSeries = false
        iMaxServiceForCustomer = 0
        iPeriodInWeeksForMaxServices = 0
        bSyncGoogleCalendar = false
    }
    
    init(_iFirstCalendarViewType:Int,_dtCalendarOpenDate:String,_bLimitSeries:Bool,_iMaxServiceForCustomer:Int,_iPeriodInWeeksForMaxServices:Int,_bSyncGoogleCalendar:Bool) {
        
        iFirstCalendarViewType = _iFirstCalendarViewType
        dtCalendarOpenDate = _dtCalendarOpenDate
        bLimitSeries = _bLimitSeries
        iMaxServiceForCustomer = _iMaxServiceForCustomer
        iPeriodInWeeksForMaxServices = _iPeriodInWeeksForMaxServices
        bSyncGoogleCalendar = _bSyncGoogleCalendar
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iFirstCalendarViewType"] = iFirstCalendarViewType
        if Global.sharedInstance.isDateNil == false
        {
            dic["dtCalendarOpenDate"] = dtCalendarOpenDate
        }
        else
        {
            dic["dtCalendarOpenDate"] = ""
        }
        dic["bLimitSeries"] = bLimitSeries
        dic["iMaxServiceForCustomer"] = iMaxServiceForCustomer
        dic["iPeriodInWeeksForMaxServices"] = iPeriodInWeeksForMaxServices
        //dic["bSyncGoogleCalendar"] = bSyncGoogleCalendar
        
        return dic
    }
    
    //2do - delete
    func getDicExample()->Dictionary<String,AnyObject>
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iFirstCalendarViewType"] = 0
        dic["dtCalendarOpenDate"] = ""
        dic["bLimitSeries"] = false
        dic["iMaxServiceForCustomer"] = 22
        dic["iPeriodInWeeksForMaxServices"] = 22
        //dic["bSyncGoogleCalendar"] = bSyncGoogleCalendar
        
        return dic
    }

    
    
    func objCalendarPropertiesToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<objCalendarProperties>{
        
        var arrServiceProv:Array<objCalendarProperties> = Array<objCalendarProperties>()
        var ProvService:objCalendarProperties = objCalendarProperties()
        
        for i in 0  ..< arrDic.count
        {
            ProvService = dicToCalendarProperties(arrDic[i])
            arrServiceProv.append(ProvService)
        }
        return arrServiceProv
    }
    
    func dicToCalendarProperties(dic:Dictionary<String,AnyObject>)->objCalendarProperties
    {
        let calendarProp:objCalendarProperties = objCalendarProperties()
        
        calendarProp.iFirstCalendarViewType = Global.sharedInstance.parseJsonToInt(dic["iFirstCalendarViewType"]!)
        
        calendarProp.dtCalendarOpenDate = dic["dtCalendarOpenDate"]! as! String
        if dic["bLimitSeries"] != nil
        {
            if Global.sharedInstance.parseJsonToString(dic["bLimitSeries"]!) != ""
            {
                calendarProp.bLimitSeries = dic["bLimitSeries"] as! Bool
            }
            else
            {
                calendarProp.bLimitSeries = false
            }
        }
        else
        {
            calendarProp.bLimitSeries = false
        }
        calendarProp.iMaxServiceForCustomer = Global.sharedInstance.parseJsonToInt(dic["iMaxServiceForCustomer"]!)
        calendarProp.iPeriodInWeeksForMaxServices =  Global.sharedInstance.parseJsonToInt(dic["iPeriodInWeeksForMaxServices"]!)
        
        if dic["bSyncGoogleCalendar"] != nil
        {
            if Global.sharedInstance.parseJsonToString(dic["bSyncGoogleCalendar"]!) != ""
            {
                calendarProp.bSyncGoogleCalendar = dic["bSyncGoogleCalendar"] as! Bool
            }
            else
            {
                calendarProp.bSyncGoogleCalendar = false
            }
        }
        else
        {
            calendarProp.bSyncGoogleCalendar = false
        }
        
        return calendarProp
    }
}
