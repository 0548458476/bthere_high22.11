//
//  TriangeView.swift
//  bthree-ios
//
//  Created by User on 2.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

//import Foundation
import UIKit

class TriangeView: UIView {

    override func drawRect(rect: CGRect) {
        
        // Get Height and Width
        let layerHeight = self.layer.frame.height
        let layerWidth = self.layer.frame.width
        
        // Create Path
        let bezierPath = UIBezierPath()
        
        // Draw Points
        
 


        bezierPath.closePath()
        
        bezierPath.moveToPoint(CGPointMake(0, layerHeight))
        bezierPath.addLineToPoint(CGPointMake(layerWidth, layerHeight))
        bezierPath.addLineToPoint(CGPointMake(layerHeight-8, 0))
        bezierPath.addLineToPoint(CGPointMake(0, layerHeight+7))
        bezierPath.closePath()
        
        // Apply Color
        UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).setFill()
        bezierPath.fill()
        
        // Mask to Path
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.CGPath
        self.layer.mask = shapeLayer
    }
    
}