//
//  Validation.swift
//  bthree-ios
//
//  Created by User on 14.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class Validation: NSObject {
        class var sharedInstance:Validation {
            struct Static {
                static var onceToken:dispatch_once_t = 0
                static var instance:Validation?=nil
            }
            dispatch_once(&Static.onceToken){
                Static.instance=Validation()
            }
            return Static.instance!
        }
    func mailValidation(string:String)->Bool
    {
        return (NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")).evaluateWithObject(string)
        
    }
    
    func phoneNumberValidation(value: String) -> Bool {
        if value.characters.count == 9
        {  //טלפון
            return (NSPredicate(format:"SELF MATCHES %@", "^\\d{2}?([ .-]?)\\d{3}?([ .-]?)\\d{4}$")).evaluateWithObject(value)
        }
        else if value.characters.count == 10
        { //פלאפון
            return (NSPredicate(format:"SELF MATCHES %@", "^\\d{3}?([ .-]?)\\d{3}?([ .-]?)\\d{4}$")).evaluateWithObject(value)
        }
        else
        {
            return false
        }
        
    }
    
    func  idValidation(string:String)->Bool
    {
        if string.characters.count != 9//בדיקה אם מכיל 9 ספרות
        {
            return false
        }
        return true
        //תקינות של תעודת זהות
        //        var sum:Int = 0
        //        var devidedNumber:Int = 0
        //        var id:Int = (Int)(string)!
        //        let checkDigit:Int = id%10
        //        id = id/10
        //        for var i:Int = 0 ; i<8 ; i++
        //        {
        //            devidedNumber = id%10
        //            if i%2 == 0
        //            {
        //                let sumOfDevided = devidedNumber*2
        //                sum += sumOfDevided%10+sumOfDevided/10
        //            }
        //            else
        //            {
        //                sum += devidedNumber
        //            }
        //            id = id/10
        //        }
        //        if (sum+checkDigit)%10 == 0
        //        {
        //            return true
        //        }
        //        return false
    }
    
    //func to check validation of the business hours
    // return true - when hours were chosen
    // return false - when it is null
    func BusinessHoursValidate()-> Bool
    {
    for item in Global.sharedInstance.arrWorkHours
    {
        if item.iDayInWeekType != 0
        {
        return true
        }
    }
    return false
    }
    
    func isTzValid(tz:String) -> Bool {
                var sum:Int = 0
                var devidedNumber:Int = 0
                var id:Int = (Int)(tz)!
                let checkDigit:Int = id%10
                id = id/10
                for i:Int in 0  ..< 8 
                {
                    devidedNumber = id%10
                    if i%2 == 0
                    {
                        let sumOfDevided = devidedNumber*2
                        sum += sumOfDevided%10+sumOfDevided/10
                    }
                    else
                    {
                        sum += devidedNumber
                    }
                    id = id/10
                }
                if (sum+checkDigit)%10 == 0
                {
                    return true
                }
                return false
    }
}
