//
//  Global.swift
//  bthree-ios
//
//  Created by User on 11.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import Contacts
import AddressBook

class Global: NSObject {
    class var sharedInstance:Global {
        struct Static {
            static var onceToken:dispatch_once_t=0
            static var instance:Global?=nil
        }
        dispatch_once(&Static.onceToken){
            Static.instance=Global()
        }
        let deaufult:NSUserDefaults = NSUserDefaults.standardUserDefaults()

        if deaufult.valueForKey("LogOut") != nil
        {
            if deaufult.valueForKey("LogOut") as! Bool == true

            {
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "LogOut")
                NSUserDefaults.standardUserDefaults().synchronize()
                Static.instance = Global()
            }
        }
        
        return Static.instance!
    }
    
      var rtl:Bool = false
    var viewConNoInternet:UIViewController?//למקרה שאין אינטרנט בעת שמגיע להודעת       generic השגיאה שיסיר מהויו קונטרולר את ה  
   
    var freeHoursForWeek:Array<Array<providerFreeDaysObj>> = [Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>(),Array<providerFreeDaysObj>()]//מערך המכיל 7 ימים בשבוע עם השעות הפנויות לכל יום
    
    var bthereEventsForWeek:Array<Array<OrderDetailsObj>> =
        [Array<OrderDetailsObj>(),Array<OrderDetailsObj>()
            ,Array<OrderDetailsObj>(),Array<OrderDetailsObj>()
            ,Array<OrderDetailsObj>(),Array<OrderDetailsObj>()
            ,Array<OrderDetailsObj>()]//מערך המכיל 7 ימים בשבוע עם אירועי ביזר לכל יום
    
    var giveServices:giveMyServicesViewController!

    var datDesigncalendar:DayDesignCalendarViewController?
    var dayDesigncalendarAppointment12:dayClientForAppointment12ViewController?
    var dayDesignCalendarSupplier:DayDesignSupplierViewController?
    
    var freeHoursForCurrentDay:Array<providerFreeDaysObj> = []//מערך שמכיל את כל השעות הפנויות של היום שעליו לחצו
    
    //var datDesigncalendarToSupplier:DayDesigCalendarViewController?//n
     var indexRowForIdGiveService:Int = -1
     //שמירת האינדקס של השורה לצורך ההגעה לid של הנותן שרות
    var indexRowForIdService:Int = -1
    //שמירת האינדקס של השורה לצורך ההגעה לid של השרות
    //המשתנים הבאים הם לצורך הצגת הפרטים בפופאפ פרטי התור
    var currentProviderToCustomer:SearchResulstsObj = SearchResulstsObj()// שומר את פרטי הספק,בינתיים
    var hourFreeEvent = ""//השעה של הארוע הפנוי שנבחר
    var hourFreeEventEnd = ""//השעת סיום הארוע הפנוי שנבחר
    var hourBthereEvent = ""//השעה של תחילת הארוע של ביזר להצגה על הפופאפ של פרטי הזמנה
    var orderDetailsFoBthereEvent:OrderDetailsObj = OrderDetailsObj()
    var isFromViewMode = false//האם הגיע לרישום לקוח דרך מצב צפיה

    var eventBthereDate:NSDate = NSDate()
    var eventBthereDateStart:NSDate = NSDate()
    var eventBthereDateEnd:NSDate = NSDate()
    var dayFreeEvent = ""//היום של הארוע הפנוי שנבחר
    var NewEventToHour:String = ""
    
    var dateDayClick:NSDate = NSDate()//תאריך של הארוע הפנוי שנבחר להצגה בפופאפ
    var dateEventBthereClick = NSDate()//תאריך של הארוע של ביזר שנבחר להצגה בפופאפ
    var isShowClickDate:Bool = false// האם להראות יום מהתאריך הנוכחי או מתאריך שנלחץ בתצוגת חודש
    var calendarAppointment:ModelCalendarForAppointmentsViewController?

    var model:Int = -1//מציין באיזה מודל נמצאים: יומן לקוח=1 , יומן ספק שהלקוח רואה=2
    var calendarClient:ModelCalenderViewController!

    var calendarAppointmentForSupplier:CalendarSupplierViewController?

    var isSelected:Bool = false
    var fromCalendar:Bool = false
    var calendarDesigned:Int = 0
    var globalData:RgisterModelViewController!
    //לכתובת:
    var isAddressCity = false
    var GlobalDataForDelegate:GlobalDataViewController?
    var didOpenBusinessDetails = false
    
    var isRegisterProviderClick:Bool = false//לחצו על רישום כספק
    var isRegisterClientClick:Bool = false//לחצו על רישום לקוח

    var isVclicedForWorkerForEdit = false//האם מסומן וי בשעות זהות לעןבד בשביל עריכה
    var isClickNoEqual:Bool = false//האם לחץ על האיקס של שעות שונות בעובד
    var isClickEqual:Bool = false
    var clickNoCalendar:Bool = false
    var fromClickEqualHours:Bool = false
    var fromClickEqualHoursChange:Bool = false
    var fromEdit:Bool = false
    var isSetDataNull:Bool = false
    var eleventCon:CalendarSupplierViewController!
    var headersForTblInCell:Array<String> = []
//    var flagsHeadersForTblInCell:Array<Int> = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
    var PresentViewMe:UIViewController?
    var isDeleted = false
    var isDeletedGiveMyService = false
    
    var isSyncWithGoogelCalendar:Bool = true//האם יש סנכרון עם היומן האישי ביומן לקוח
    var isSyncWithGoogelCalendarSupplier:Bool = true //האם יש סנכרון עם היומן האישי ביומן ספק
    var isSyncWithGoogleCalendarAppointment:Bool = true//האם יש סנכרון עם היומן האישי בתצוגת ספק שהלקוח רואה

    var mycustomers:MyCostumersViewController?
    var listDesign:ListDesignViewController?
    var appointmentsCostumers:CostumerAppointmentsViewController?
    var WorkerCell:ItemInSection3TableViewCell?
    var isProvider = false//מציין האם נמצאים עכשיו בספק  
    var isFromMenu = false
    var dateFreeDays:Array<NSDate> = []
    var isFromPrintCalender = false
    var fReadRegulation = false//מציין האם מולא שדה קריאת תקנון ברישום
    var isHoursSelectedItem:Bool = false
    let calendar = NSCalendar.currentCalendar()
    var domainBuisness:String = ""
    var flagIfValidSection1:Bool = false
    var flagIsSecond1Valid:Bool = true
    var  isFirstSectionValid:Bool = true
    var isOpenHoursForPlus:Bool = false
    var isOpenHoursForPlusAction:Bool = false
    var countFlag = 0
    var whichReveal:Bool = false //לקוח או ספק
    var sectionKind:Int = 0
    var rowKind:Int = 0
    var giveServiceName:String = ""//שמירת שם של הנותן שרות בשביל שאם נחזור לעמוד נציג מזה
    var heightModel:CGFloat = 0
    var defaults = NSUserDefaults.standardUserDefaults()
    var heightForNotificationCell:CGFloat = 0
    var addressBook = ABAddressBookCreateWithOptions(nil, nil)?.takeRetainedValue()
    var phone:Array<String> = []
    var ifOpenCell:Bool = false
    var data:Array<String> = []
    var contactList:Array<Contact> = Array<Contact>()
    var selectedCellForEditService:Array<Bool> = []//מערך של פלאגים שמסמלים האם סל בעריכה של שרות פתוח או סגור
    ////googlePlus
    var googleSignIn: GPPSignIn!
    var signIn: GPPSignIn?
    var isGooglePlusChecked  = false
    var heightForCell:CGFloat = 0
    var categoriesArray:Array<String> = []
    var arrayViewModel:Array<UIViewController> = []//contains all reigster supplier views-helps to navigate between pages
    var arrEventsCurrentMonth:Array<EKEvent> = []
    var arrEvents:Array<EKEvent> = []
    var flagIsClickOnNoSameHour:Bool = false
    ///flags to check validation in registerController
    var registerViewCon:RegisterViewController?
    var isValid_FullName:Bool = true
    var isValid_Phone:Bool = true
    var isValid_Email:Bool = true
    var isValid_Address:Bool = true
    //עובדים
    var recessForWorker:Bool = false//האם נלחץ הפסקות לעובד
    var hoursForWorker:Bool = false//האם נלחץ שעות פעילות לעובד
    var hoursForWorkerFromPlus:Bool = false//האם נלחץ שעות פעילות לעובד מלחיצה על פלוס
    var recessForWorkerFromPlus:Bool = false//האם נלחץ הפסקות לעובד מלחיצה על פלוס
var dicGetFreeDaysForServiceProvider:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()// המשתנה אותו נשלח לשרת שמכיל את קודי נותני השרות ושל קודי השרותים 
    var hoursForWorkerFromEdit:Bool = false//האם נלחץ שעות פעילות לעובד בעריכה
    var lastChooseIndex:Int = -1 //בשביל הכל בסוג התראה רציתי לשמור על האינדקס האחרון שלחצו עליו
    var recessForWorkerFromEdit:Bool = false//האם נלחץ שעות הפסקות לעובד בעריכה
    var arrayGiveServicesKods:Array<Int> = []//מערך לשמירת הקודים של הנותני שרות לצורך שליחה לשרת כדי לקבל את השעות הפנויות
    var arrayServicesKods:Array<Int> = []//מערך לשמירת כל הקודים של השרותים
    var arrayServicesKodsToServer:Array<Int> = []//מערך לשרת לשמירת הקודים של השרותים לצורך שליחה לשרת כדי לקבל את השעות הפנויות
    //לדף של בחירת נותן שרות
    var idWorker:Int = -1//משתנה שיאותחל במס׳ של האיש צוות בעת לחיצה על השורה
    var giveServicesArray:Array<User> = []
    
    var arrayServicesNames:Array<String> = []//מערך של כל שמות השירותים לעסק
    var serviceName:String = ""//שם השרות אותו מזמינים
    var multipleServiceName:Array<String> = []//שם השרותים אותם מזמינים בבחירה מרובה
    
    var fromHourArray:Array<NSDate> = []//שעות התחלה של תורים ליום מסוים
    var endHourArray:Array<NSDate> = []//שעות סיום של תורים פנויים ליום מסוים
    
    var eventList:[AnyObject] = []
    
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var hasEvent = false
    var currentUser:User = User()//משתנה זה מאותחל בלקוח הנוכחי שעכשיו על המכשיר
    var currentProvider:Provider = Provider()
    //מכיל את מה שחוזר מהפונקציה getProviderAllDetails - כל פרטי הספק
    var currentProviderDetailsObj:ProviderDetailsObj = ProviderDetailsObj()
    var currentProviderBuisnessDetails:Provider = Provider()//שומר את מה שחוזר מהפונקציה getProviderBuisnessDetails-לעמוד הראשון של רישום ספק
    var RegisterNotEnd = false///מציין האם משתמש סיים את תהליך הרישום כולו : true=מילא חלק מהפרטים
    // בשביל דף התראות שומר לכל סל שיש בו בחירה מרובה מה נבחר בו
    var flagsHeadersForTblInCell:Dictionary<Int,Array<Array<Int>>> = [0:[[],[1,1,1,1]]
        ,2:[[],[-1,-1,-1],[-1,-1,-1]]
        ,3:[[],[-1,-1],[-1,-1,-1]]]
    var isCamera = false
    
    var MessageArray:Array<String> =
        [NSLocalizedString("WELCOME_TO_BTHERE", comment: ""),NSLocalizedString("KEY_SENTANCE_SLOGEN", comment: ""),NSLocalizedString("NEW_VERSION_HERE", comment: ""),NSLocalizedString("DISCOUNTS_FOR_FIRST_REGISTERS", comment: "")]
    var selectedItemsForSaveData:Dictionary<Int,Array<String>> = [0:["",""]
        ,2:["","",""]
        ,3:["","",""]
        ,4:["",""]]
    var isFromRegister = Bool()//מציין אם נכנסו דרך לוגין או הרשמה רגילה
    var imgCostumersArray:Array<String> = ["","4.png","user-icon.png","","4.png","user-icon.png"]
    var nameCostumersArray:Array<String> = ["דוד לוי","חיים","יצחק כהן","משה כץ","זאב בן דוד"]
    var fIsValidDetails = false
    //מערכים לשמירת מצב כל כפתור של וי או איקס בדף התראות
   
    var kindNotificationsTableViewCell:KindNotificationsTableViewCell?
    
    
    var isDescCellOpenFirst = true
    //מערכים השומרים לכל סקשין האם מסומן בוי או באיקס
    var arrNotificationsV:Array<Bool> = [true,true,true,true,true]
    var arrNotificationsX:Array<Bool> = [false,false,false,false,false]
    
    var isExtTblHeightAdded60 = false
    var tagCellOpenedInTbl = -1//save the cell's tag opened inTbl in notifications page(= -1 if itself opened)
    var isOpen :Bool = false//to know if tbl is open
    //שומר את כל מה הטבלאות שחוזרות מהפונקציה:GetSysAlertsList
    var arrSysAlerts:Array<SysAlerts> = Array<SysAlerts>()
    //שומר את כל מה הטבלאות שחוזרות מהפונקציה:GetSysAlertsList
    var dicSysAlerts: Dictionary<String,Array<SysAlerts>> =  Dictionary<String,Array<SysAlerts>>()
    var arrayDicForTableViewInCell:Dictionary<Int,Array<Array<String>>> =
        [0:[[],[]]
            ,2:[[],[],[]]
            ,3:[[],[],[]]]
    
    
    //אוביקט מוכן לשליחה לשרת לפונקציה:AddProviderAlertSettings
    var addProviderAlertSettings:AddProviderAlertSettings = AddProviderAlertSettings()
    //2do - למחוק בהמשך...
    //var selectedSavedCustomer = "פופאפ"//משתנה זמני עד שהשרת יעדכן בשימור לקוחות את האוציות של פופאפ,מייל,הודעות
    //אוביקט מוכן לשליחה לשרת לפונקציה:AddProviderBuisnessProfile
    var addProviderBuisnessProfile:AddProviderBuisnessProfile = AddProviderBuisnessProfile()

    var helperTable:UITableView?
    var helperTable1:UITableView?
    var countInitInGlobalData:Int = 0
    //SyncContacts מוכן לשליחה לשרת לפונקציה dictionary
    var dicSyncContacts:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
    
    var modelCalenderForAppointment:ModelCalendarForAppointmentsViewController?
    var modelCalender:ModelCalenderViewController?
    var itemInCol:ItemInCollectionInSection1CollectionViewCell?
    //מודל הרשמה-בשביל הדלגייט של שמירת פרטי הספק בשרת
    var rgisterModelViewController:RgisterModelViewController?
    var isFromBack = false
    
    //for auto complete
    var filterSubArr:Array<String> = []
    
    var currentOpenedMenu:UIViewController?

    var viewConGlobalData:GlobalDataViewController?
    var giveMyServices:giveMyServicesViewController?
    
    //----
    var currentLat: CLLocationDegrees?
    var currentLong: CLLocationDegrees?
    
    var searchDomain = ""//שומר את הטקס שנרשם בשורת החיפוש או את התחום מחיפוש מתקדם
    var dicSearchProviders:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//מתמלא לפי הנתונים שנשלחו לשרת בחיפוש, עבור שנה מיקום
    
    var whichSearchTag:Int = -1//חיפוש רגיל - 1, חיפוש מתקדם - 2
    var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//עבור השליחה לשרת
    var searchResult:SearchResultsViewController?
    var isSettingsOpen = false
    var viewCon2:BusinessProphilShowViewController?
    var providerBuisnessProfile:AddProviderBuisnessProfile = AddProviderBuisnessProfile()
    
    var viewCon:ListServicesViewController?
    var arrayWorkers:Array<String> = []
    var firstGiveService:Bool = true//מיועד כדי לגלול את הנותן שרות הראשון
    var firstShowResults:Bool = true//מיועד כדי לגלול את התוצאת חיפוש הראשונה מעט שמאלה
    var firstService:Bool = true //מיועד כדי לגלול את השירות הראשון מעט שמאלה
    var getFreeDaysForService:Array<providerFreeDaysObj> =  Array<providerFreeDaysObj>()
    var ordersOfClientsArray:Array<OrderDetailsObj> =  Array<OrderDetailsObj>()// מערך תורי ביזר ללקוח
    

    var isOpenSale:Bool = true//לדעת האם הויו של העלאת מבצע
    
    var placeIdForMap = ""

    var couponsForProvider:Array<CouponObj> = Array<CouponObj>()//רשימת מבצעים
    
    var dicResults:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
    var dicResultHourFree:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()//לתוכו מאוחסנות השעות הפנויות של נותן שרות

    var didOpenSearchResult = false
    var isFromSearchResults = false//כדי לדעת איך להציג את הדף פרופיל עסקי עריכה או תצוגה.
    
    //MARK: - ItemInSection3TableViewCell
    var txtAdressItem3 = false
    var isTblAddresssOpen:Int32 = 0
    var cutrrentRowForAddress:Int = -1 //save the current row, to know how many to scroll
    var isOpenAddressTbl = false // האם הטבלה של הכתובות פתוחה
    //true - לפתוח  false - לסגור
    var isStillOpen = false//נדלק בתוך הפונקציה שפותחת את הטבלה - כדי שבעת גלילה אדע אם עדיין הטבלה פתוחה
    var indexForArr:Int = -1
    var item3:ItemInSection3TableViewCell?
    var hoursActiveTableViewCell:HoursActiveTableViewCell?
    //flags for validation
    var isValid_fName:Bool = false
    var isValid_lName:Bool = false
    var isValidPhone:Bool = false
    var isValidEmail:Bool = false
    
    //MARK: -  GlobalData ״נתונים כלליים״
    var isReturn2RowsForHours = false
    var isOpenWorker = false//נדלק בעת הוספת עובד ונכבה בשמירת העובד,משמש כדי למנוע הצגת סל ריק בעת גלילה
    
    var txtSearch:String = ""
    var isFromSave = false
    var isReloadForEdit = false
    var isNewService = false//האם צריך לפתוח את הסל של שרות ומוצר מכפתור הוסף שרות
    var isAddWorker = false
    var isFromFirstSave = false
    var isOpenNewWorker = false
    var isOpenHours = false
    var countCellHoursOpenFromEdit = 0//מונה כמה סלים של שעות פעילות בעריכה פתוחים לצורך חישוב הגובה לאחר השנוי שכל פעם פתוח רק סל אחד משתנה זה יכול להכיל או אפס או 1
    var countCellEditOpen = 0//משתנה זה מכיל כמה סלים של עריכה פתוחים לצורך חישוב הגובה
    
    var isOpenHoursForNewWorker = false
    var fSection5 = false
    var indexPathNew:NSIndexPath?
    var indexPathWithSection5:NSIndexPath?
    var isContinuPressed = false//מציין האם לחצו על המשך בהרשמה
    var arrEditWorkers:Array<Bool> = []
    
    var headersCellRequired:Array<Bool> = [false,false,true,false,true]
     var selectedCell:Array<Bool> = [false,false,false,false,false,false]//מערך של פלאגים שמיצג האם הסלים החיצוניים פתוח או סגור
    var currentEditCellOpen:Int = -1//שומר את הסל הנוכחי שפתוח בנתונים כלליים כדי לדעת את מי לסגור בלחיצה על סל נוסף (לא ניתן לפתוח מספר סלים בו זמנית),
// = 1- כאשר הכל סגור
    
    //___________flags for validation_______________________
    var isClickCon:Bool = false
    var delegateValidData:validDataDelegate!=nil
    var isAllFlagsFalse = true//לפרטי עובדים
    var isFromEdit:Bool = false//דגל המציין האם הגיעו לפונקציה של התקינות מעריכה
    var isServiceProviderEditOpen = false//דגל המציין האם עריכת עובד פתוח
    var isFromEditService:Bool = false//דגל שנדלק בעת לחיצה על עריכת שרות או מוצר
    //לשרותי העסק
    var isValidDiscount = false
    var isValidMinConcurrentCustomers = false
    var isValidPrice = false
    var isValidServiceName = false
    var isValidTimeInterval = false
    var isValidTimeOfService = false
    var isValidMaxConcurrent = false
    var fDiscount = false
    var isNewServicePlusOpen = false //דגל למניעת ריקון הנתונים בשרות או מוצר בעת גלילה
    var fisValidWorker = false//the flag is true when the section 2 - workers is valid, if not = false
    //I use it because the func that check the validation always returns true
    var isFirst:Bool = true
    var isFirstBussinesServices:Bool = true
    var isFirstCalenderSetting:Bool = true
    
    var fDomain:Bool = false
    var fIsSaveConHoursPressed:Bool = false
    var fIsValidHours:Array<Bool> = [true,true,true,true,true,true,true]// בשביל העסק - שעות
        var fIsValidRest:Array<Bool> = [true,true,true,true,true,true,true]//בשביל העסק - הפסקות
    var fIsValidHoursChild:Array<Bool> = [true,true,true,true,true,true,true]//בשביל עובדים - שעות
    var fIsValidRestChild:Array<Bool> = [true,true,true,true,true,true,true]//בשביל עובדים - הפסקות
    
    var isValidHours:Bool = true
    var fIsEmptyOwnerHours = false
    var fIsEmptyBussinesServices = false
    var fIsSaveConBussinesServicesPressed:Bool = false
    var fIsEmptyCalenderSetting = false
    var fIsSaveConCalenderSettingPressed:Bool = false
    var isValidWorkerDetails:Bool = true
    var selectedCellEdit:Int = 0//שומר את האינדקס של העובד העכשוי שפתוח בעריכה
    
    //______________Properties to server__________________
    
    var isDateNil = false
    var GlobalDataVC:GlobalDataViewController?
    var hoursActive:HoursActiveTableViewCell?
    
    //--------for the delegate saveData------
    var itemInSection3TableViewCell:ItemInSection3TableViewCell?
    var saveTableViewCell:SaveTableViewCell?
    var itemInSection2TableViewCell:ItemInSection2TableViewCell?
    //---------------------------------------
    
    
//אוביקט שמכיל את כל מה שצריך לשלוח לשרת מדף נתונים כלליים
    var generalDetails:objGeneralDetails = objGeneralDetails()
   //מכיל את מה שחוזר מהפונקציה בשרת getProviderDetails -פרטי הספק מדף נתונים כלליים
    //var currenProviderDetails:objGeneralDetails = objGeneralDetails()
    var arrIsRestChecked:Array<Bool> = [false,false,false,false,false,false,false]
     var arrIsRestCheckedChild:Array<Bool> = [false,false,false,false,false,false,false]
    
    var isOwner:Bool = true
    
    //if isOwner=true -------------------
    
    var fIsRestBefore = false
    
    //שעות פעילות
    
    //שעות פעילות חדש-----
    var numbersOfLineInLblHours:Int = 1
    var numbersOfLineInLblRest = 0
    var isBreak = false
    var addRecess = false//אם לחצו על הוסף הפסקות נדלק,בסגירת הסל נכבה
    var onOpenTimeOpenHours = false//נדלק מיד בפתיחת הסל של שעות פעילות כי אז צריך לאתחל את הדייט פיקר לפי מה שנבחר כדי שיהיה שונה בשעות ובהפסקות
    var onOpenRecessHours = false//נדלק מיד בלחיצה על הוסף הפסקות של שעות פעילות כי אז צריך לאתחל את הדייט פיקר לפי מה שנבחר כדי שיהיה שונה בשעות ובהפסקות
    var isFirstHoursOpen = false//נדלק בפעם הראשונה שפותחים את השעות פעילות
    var isFirstRecessHoursOpen = false//נדלק בפעם הראשונה שפותחים את ההפסקות
    var isSelectAllHours = false
    var isSelectAllRest = false
    var isSelectAllHoursChild = false
    var isSelectAllRestChild = false
    var isRest = false//מציין האם נבחרו הפסקות,נדלק בפעם הראשונה שבוחרים הפסקות
    //------------
    
    
    var workingHours:objWorkingHours = objWorkingHours()
    var hourShow:String = ""//שומר את הסטרינג של השעות פעילות שבנחרו להצגה
    var hourShowRecess:String = ""//שומר את הסטרינג של ההפסקות שבנחרו להצגה

    var hourShowChild:String = ""
    var hourShowRecessChild:String = ""

    var isHourScrolled = false
    
    var hourShowFirstWorker:String = ""
    var hourShowRecessFirstWorker:String = ""
    //משתנים אלו הם לצורך רענון גובה הסל של הצגת השעות וההפסקות אם הגובה של הלייבל השתנה
    var lastLblHoursHeight:CGFloat = 0.0//שומר את גובה הלייבל שעות הקודם של הצגת השעות פעילות בנתונים כלליים
    var currentLblHoursHeight:CGFloat = 0.0//שומר את גובה הלייבל שעות הנוכחי של הצגת השעות פעילות בנתונים כלליים
    var lastLblRestHeight:CGFloat = 0.0//שומר את גובה הלייבל הפסקות הקודם של הצגת השעות פעילות בנתונים כלליים
    var currentLblRestHeight:CGFloat = 0.0//שומר את גובה הלייבל הפסקות הנוכחי של הצגת השעות פעילות בנתונים כלליים
    
    //שומר לכל יום את השעות פעילות שנבחרו
    var arrWorkHours:Array<objWorkingHours> = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
    //שומר לכל יום האם הוא בחור או לא כדי שכששומרים  את השעות פעילות נשווה לפי זה עם מה שנשמר באוביקט:arrWorkHours ונמחק את הימים שאין צורך לשומרם
    var isHoursSelected:Array<Bool> = [false,false,false,false,false,false,false]
    var isHoursSelectedRest:Array<Bool> = [false,false,false,false,false,false,false]
    var isHoursSelectedChild:Array<Bool> = [false,false,false,false,false,false,false]
    var isHoursSelectedRestChild:Array<Bool> = [false,false,false,false,false,false,false]
    
    ///הפסקות
    var workingHoursRest:objWorkingHours = objWorkingHours()
    var arrWorkHoursRest:Array<objWorkingHours> = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
    
    //היה כשהיה את השעות פעילות הישן
//    //indexPath to save the current index path of day which selected to save it's hours
//    var currentIndexPath:Int = -1//שעות פעילות
//    var currentIndexPathRest:Int = -1//הפסקות
    
    //indexPath to save the current button's tag of day which selected to save it's hours
    var currentBtnDayTag:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל שעות פעילות
    var lastBtnDayTag:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל שעות פעילות
    var currentBtnDayTagRest:Int = -1//לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל הפסקות
    var lastBtnDayTagRest:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל הפסקות 
    
    
    //-------------------------------------------
   
    
    //if isOwner=false -------------------
    
    //שעות פעילות
    var workingHoursChild:objWorkingHours = objWorkingHours()//אוביקט זמני לשמירת שעות ליום בודד
    var arrWorkHoursChild:Array<objWorkingHours> = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]//מערך זמני לשמירת שעות לעובד מסויים
    ///הפסקות
    var workingHoursRestChild:objWorkingHours = objWorkingHours()
    var arrWorkHoursRestChild:Array<objWorkingHours> = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
    
    //indexPath to save the current button's tag of day which selected to save it's hours
    var currentBtnDayTagChild:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל שעות פעילות
    var lastBtnDayTagChild:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל שעות פעילות
    var currentBtnDayTagRestChild:Int = -1//לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל הפסקות
    var lastBtnDayTagRestChild:Int = -1//שומר את הטאג של היום שעליו לחצו-הטאג מאותחל לפי שיום ראשון=0 וכו׳ בשביל הפסקות
    
//    //indexPath to save the current index path of day which selected to save it's hours
//    var currentIndexPathChild:Int = -1//שעות פעילות
//    var currentIndexPathRestChild:Int = -1//הפסקות
    
    //------------------------------------
    
    //מכיל את נתוני העובדים שהספק מוסיף בשביל השרת
     var serviceProvider:objServiceProviders = objServiceProviders()
     var serviceProviderForEdit = objServiceProvidersForEdit()
     // מערך של כל העובדים עם כל פרטיהם,שומר לכל עובד את השעות פעילות וההפסקות,זה עוזר לעריכת פרטי העובד
    var arrObjServiceProvidersForEdit:Array<objServiceProvidersForEdit> = Array<objServiceProvidersForEdit>()
    
    // flags to business service
    var isFirstOpenKeyBoard = false
    var isFirstCloseKeyBoard = false
    
    // flags to add/remove keyBoard events
    var didSection3Closed = true
    var didServicesClosed = true
    var didCalendarSettingClose = true
    
    var calendarSetting:CalenderSettingTableViewCell?
    var businessService:BussinesServicesTableViewCell?

    //MARK: - Client Exist
    //בשביל דף אני רוצה להזמין
    var indexCellSelected = 0//שומר את האינדקס של הסל שבו לחצו על בחירה מרובה
    var isFirstCellSelected = false//מציין האם זה הסל הראשון שבחור
    var numCellsCellected = 0//סופר כמה מסומנים יש כדי שאם אין שום מסומנים נציג את הצורה הרגילה של הסלים
    
    var providerName:String = String()//providerName after is select
    var providerID:Int = Int()//providerName after is select
   //flags for pop up "detailsAppointmetClientViewController":
    var whichDesignOpenDetailsAppointment:Int = 0//1 = opened from day,2 = from week,turn on when open the pop up, to know where to go back
    var isCancelAppointmentClick = false//turn on if click on cancel
//MONTHCALANDER
    var currDateSelected:NSDate = NSDate()
    var desingMonth:MonthDesignedViewController!
    var designMonthAppointment:MonthClientForAppointmentDesignViewController!
    //MonthDesignedViewController__________________________________________________
        var hourFreeEventInPlusMenu = ""//  בתפריט פלוס השעה של הארוע הפנוי שנבחר
    
    //WaitingList
     var arrWaitingList:Array<WaitingListObj> = Array<WaitingListObj>()//רשימת ההמתנה ללקוח
    //notificationsForDefinationsViewController
    var customerAlertsSettingsObj:CustomerAlertsSettingsObj = CustomerAlertsSettingsObj()//שומר את ההתרות שמסמן הלקוח בדף התראות מהגדרות
    
    
    //MARK: - Functions
    
    func cutStringBySpace(fullName:String,strToCutBy:String)->Array<String>
    {
        let name = fullName
        let nameArr = name.componentsSeparatedByString(strToCutBy)
//        let firstName: String = nameArr[0]
//        let lastName: String? = nameArr.count > 1 ? nameArr[1] : nil
        return nameArr
    }
    
    //ארועים לחודש מסויים
    func setEventsArray()
    {
        arrEventsCurrentMonth = []
        for item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            //let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday
            {
                arrEventsCurrentMonth.append(event)
                hasEvent = true
            }
        }
        
    }
    
    func setAllEventsArray()
    {
        arrEvents = []
        for item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            
            //let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
//            let yearEvent =  componentsEvent.year
//            let monthEvent = componentsEvent.month
//            let dayEvent = componentsEvent.day
        
//            if yearEvent == yearToday && monthEvent == monthToday
//            {
                arrEvents.append(event)
                hasEvent = true
//            }
        }
        
        arrEvents =   arrEvents.sort({ ($0 ).startDate.timeIntervalSinceNow < ($1 ).startDate.timeIntervalSinceNow })
    }
//ממירה תמונה לסטרינג
    func setImageToString(var image:UIImage)->String{
        //var data:NSData
        var imageData:NSString
        var dataForJPEGFile:NSData
        
        if let _:UIImage=image{
            dataForJPEGFile = UIImageJPEGRepresentation(image, 1.0)!
            image = UIImage(data:dataForJPEGFile)!
            let newSize:CGSize = CGSizeMake(image.size.width/2, image.size.height/2)
            UIGraphicsBeginImageContext(newSize)
            image.drawInRect(CGRectMake(0,0,newSize.width,newSize.height))
            let newImage:UIImage=UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            //data=UIImagePNGRepresentation(newImage)!
            dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.2)!//picks down
            imageData = dataForJPEGFile.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            return imageData as String
        }
        else{
            return ""
        }
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }


    ////////////////////
    //parse
    func parseJsonToInt(intToParse:AnyObject)->Int
    {
        let c = intToParse
        let int:String = (c.description)!
        if let checkIfParse = Int(int)
        {
            return checkIfParse
        }
        return 0
    }
    func parseJsonToFloat(floatToParse:AnyObject)->Float
    {
        let num = floatToParse
        let floatNum:String = (num.description)
        if (floatNum as NSString).floatValue != 0
        {
            return (floatNum as NSString).floatValue
        }
        return 0
        
//        if (floatToParse.text) != nil
//        {
//        if (floatToParse.text as NSString).floatValue != 0
//        {
//            return (floatToParse.text as NSString).floatValue
//        }
//        }
//        return 0
    }
    
    func parseJsonToDouble(doubleToParse:AnyObject)->Double
    {
        let num = doubleToParse
        let doubleNum:String = (num.description)
        if (doubleNum as NSString).doubleValue != 0
        {
            return (doubleNum as NSString).doubleValue
        }
        return 0
    }
    
    func parseJsonToString (stringToParse:AnyObject)-> String
    {
        //   if (stringToParse.isKindOfClass(NSNull.cl))
        let c:String = stringToParse.description
        if c == "<null>"
        {
            return ""
        }
        else if let checkIfParse:String = c
        {
            return checkIfParse
        }
        // if let string
        // {
        // return string
        //}
        return ""
    }
    
    func getDateFromString(dateString: String)->NSDate
        //NSString
    {
//       let date1 = Global.sharedInstance.cutStringBySpace(dateString, strToCutBy: " ")
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.dateStyle = .ShortStyle
        let date = dateFormatter.dateFromString(dateString)

        return date!

    }
    //ממיר דייט לדייט לשליחה לשרת
    func convertNSDateToString(dateTOConvert:NSDate)-> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //let dateStr = dateFormatter.stringFromDate(dateTOConvert)
        
        var myDateString = String(Int64(dateTOConvert.timeIntervalSince1970 * 1000))
        myDateString = "/Date(\(myDateString))/"
        
        
        return myDateString
    }
    
    func getStringFromDateString(dateString: String)-> NSDate
    {

        if dateString != ""
        {
            var fullName = dateString.componentsSeparatedByString("(")
            
            let lastName: String? = fullName[1]
            var fullNameArr = lastName!.componentsSeparatedByString("+")
            let lastNam: String? = fullNameArr[0]
            
            let myDouble = Double(lastNam!)
            //        let date = NSDate(timeIntervalSince1970:dateStr)
            //"/Date(1454097600000+0200)/"
            let date = NSDate(timeIntervalSince1970: myDouble!/1000.0)
            return date
        }

        return NSDate()
    }

    
    func setContacts()
    {
            print("Succesful.")
            if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined)
            {
                print("requesting access...")
                ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                    if success {
                        self.getContactNames()
                    }
                    else
                    {
                        print("error")
                    }
                })
                
            }
            else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted)
            {
                print("access denied")
            }
            else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized)
            {
                print("access granted")
                getContactNames()
            }
    }
    
    func getContactNames()
    {
        var indexForUserid : Int = 0
        
        let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        //var index:Int = 0
        //var dicPerContact = Array<Dictionary<String, String>>()
        //var dicContact = Dictionary<String, Array<Dictionary<String, String>>>()
        for record:ABRecordRef in contactList
        {
            let contactPerson: ABRecordRef = record
            let phones : ABMultiValueRef = ABRecordCopyValue(record,kABPersonPhoneProperty).takeUnretainedValue() as ABMultiValueRef
            
            
            for numberIndex : CFIndex  in 0 ..< 1
            {
                
                let phoneUnmaganed = ABMultiValueCopyValueAtIndex(phones, numberIndex)
                if let _ = phoneUnmaganed
                {
                    let phoneNumber : NSString = phoneUnmaganed.takeUnretainedValue() as! NSString
                    let newString = phoneNumber.stringByReplacingOccurrencesOfString("-", withString: "")
                    
                    phone.append("\(phoneNumber)")
                    phone.append(newString)
                    
                    let contactName: String = (ABRecordCopyCompositeName(contactPerson) != nil) ? (ABRecordCopyCompositeName(contactPerson).takeRetainedValue()as String) : " "
                  
                    data.append(contactName)
                    let myStringArr = contactName.componentsSeparatedByString(" ")
                    var name: String = ""
                    for str in myStringArr
                    {
                        name += str + " "
                    }
                    let c:Contact = Contact(_iUserId: indexForUserid++, _iUserStatusType: 1565, _nvContactName: name, _nvPhone: newString as String, _bIsVCheckMamber: false, _bIsNegotiableOnly: false, _bPostdatedCheck: false,_bIsSync: true)
                    
                    Global.sharedInstance.contactList.append(c)
                }
            }
        }
    }
    
   //פונקציה המאתחלת במערך את כל השעות הפנויות לשבוע המוצג
    func setFreeHours(currentDate:NSDate,dayOfWeek:Int)
    {
        
        freeHoursForWeek[dayOfWeek] = []
        for i in 0 ..< Global.sharedInstance.getFreeDaysForService.count
        {
            let dateDt = Global.sharedInstance.getStringFromDateString(Global.sharedInstance.getFreeDaysForService[i].dtDate)
            //היום שיש בו שעות פנויות
            let dateFormatter = NSDateFormatter();
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let chosenDate = dateFormatter.stringFromDate(currentDate)
            dateFormatter.timeZone = NSTimeZone(name: "Asia/Jerusalem")
            let dateFreeDayStr = dateFormatter.stringFromDate(dateDt)
            
            if chosenDate == dateFreeDayStr
            {
                freeHoursForWeek[dayOfWeek].append(Global.sharedInstance.getFreeDaysForService[i])
            }
        }
    }
    
    //פונקציה המאתחלת במערך את כל האירועים של ביזר לשבוע המוצג
    func getBthereEvents(currentDate:NSDate,dayOfWeek:Int)
    {
        bthereEventsForWeek[dayOfWeek] = []
        
        //מעבר על כל האירועים של ביזר ושמירת האירועים שביום שנשלח
        for item in Global.sharedInstance.ordersOfClientsArray
        {
            let btEvent = item
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: btEvent.dtDateOrder)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                bthereEventsForWeek[dayOfWeek].append(btEvent)
            }
        }
    }
    

    
    //פונקציה זו מקבלת את כל האירועים הנמצאים בלוח של המכשיר שלי
    func getEventsFromMyCalendar()
    {
        eventList = []
        // Override point for customization after application launch.
        let store: EKEventStore = EKEventStore()
        
        store.requestAccessToEntityType(.Event, completion: {
            granted, error in
            
            // put your handler code here
        })
        
        // Get the appropriate calendar
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        // Create the start date components
        let oneDayAgoComponents: NSDateComponents = NSDateComponents()
        //how many days ago to show the events of the calendar
        oneDayAgoComponents.day = -90
        let oneDayAgo: NSDate = calendar.dateByAddingComponents(oneDayAgoComponents, toDate: NSDate(), options: [])!
        // Create the end date components
        let oneYearFromNowComponents: NSDateComponents = NSDateComponents()
        oneYearFromNowComponents.year = 1
        let oneYearFromNow: NSDate = calendar.dateByAddingComponents(oneYearFromNowComponents, toDate: NSDate(), options: [])!
        
        // Create the predicate from the event store's instance method
        let predicate: NSPredicate = store.predicateForEventsWithStartDate(oneDayAgo, endDate: oneYearFromNow, calendars: nil)
        
        // Fetch all events that match the predicate
        let events: [EKEvent] = store.eventsMatchingPredicate(predicate)

        for  e:EKEvent in events
        {
            if !(e.notes == "Bthere")
            {
               eventList.append(e)
            }
        }

    }
    func changeSizeOfLabelByDevice(lbl:UILabel , size:CGFloat)
    {
    lbl.font = UIFont(name: (name: lbl.font.fontName), size: size)!
        
    }
    func changeSizeOfButtonByDevice(btn:UIButton , size:CGFloat)
    {
      btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font?.familyName)!, size: 13)!
        
    }
    func uniq<S : SequenceType, T : Hashable where S.Generator.Element == T>(source: S) -> [T] {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }

}




