//
//  Alert.swift
//  v-check
//
//  Created by User on 24.11.2015.
//  Copyright © 2015 User. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
var selfController:UIViewController = UIViewController()


class Alert: NSObject,UIAlertViewDelegate {
    class var sharedInstance:Alert
    {
        struct Static {
            static var onceToken:dispatch_once_t=0
            static var instance:Alert?=nil
        }
        dispatch_once(&Static.onceToken){
            Static.instance=Alert()
        }
        return Static.instance!
    }
    
    let generic:Generic = Generic()
    
    
//    let attributedString = NSAttributedString(string: "Invalid Name", attributes: [
//        NSFontAttributeName : UIFont (name: "OpenSansHebrew-Light", size: 18)!
//        ])
    
    ////simple alert to show
    ///get 2 variables: "mess" = content of the alert,"vc" = viewController to show the alert
    func showAlert(mess:String,vc:UIViewController)
    {
        let alertController = UIAlertController(title: "", message:
            mess, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.Default,handler: nil))
        vc.presentViewController(alertController, animated: true, completion: nil)
        
      //  alertController.setValue(attributedString, forKey: "attributedMessage")
        //alertController.message = mess
        
    }
    
    func showAlertDelegate(mess:String)
    {
        let text: UILabel = UILabel()
        text.font = UIFont(name: "OpenSansHebrew-Light", size: 15)

        text.text = mess
        text.textAlignment = NSTextAlignment.Center
        var frame:CGRect = CGRect()
        frame.size = CGSize(width: 100, height: 100)
        frame.origin = CGPoint(x: 30,y: 30)
        text.frame = frame
        text.backgroundColor = UIColor.redColor()
        let alert = UIAlertView()
        alert.addButtonWithTitle(NSLocalizedString("OK", comment: ""))
        alert.title = ""
        alert.message = mess
        alert.addSubview(text)
        alert.show() 
    }
    
    func showAlertAppDelegate(mess:String)
    {
        if Global.sharedInstance.viewConNoInternet != nil
        {
           generic.hideNativeActivityIndicator(Global.sharedInstance.viewConNoInternet!) 
        }
        
        let text: UILabel = UILabel()
        text.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
        
        text.text = mess
        text.textAlignment = NSTextAlignment.Center
        var frame:CGRect = CGRect()
        frame.size = CGSize(width: 100, height: 100)
        frame.origin = CGPoint(x: 30,y: 30)
        text.frame = frame
        text.backgroundColor = UIColor.redColor()
        let alert = UIAlertView()
        //alert.setValue(text, forKey: "accessoryView")
        alert.delegate = self
        alert.addButtonWithTitle(NSLocalizedString("OK", comment: ""))
        alert.title = ""
        alert.message = mess
        alert.addSubview(text)
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex == 0
        {
         exit(0)
        }
        }

    
///פונקציה זו מופעלת מדף הכניסה בלחיצה על כפתור הרשמה דרך גוגל או פייסבוק
    /// ומציגה אלרט עם 2 כפתורים של אישור וביטול
    ///בלחיצה על אישור:מקבל את הנתונים מגוגל או מפייסבוק
    //בביטול-מוריד את האלרט
   ///מקבלת:קונטרולר עליו להציג את האלרט,דגל לדעת על האם לחצו על גוגל=1 או פייסבוק=2
    func showAlertWith2Buttons(vc:UIViewController,whichChoose:Int)
    {
    let alert = UIAlertController(title: "", message:
        NSLocalizedString("POPUP_FB_GOOGLE", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
        if whichChoose == 1
        {
        //connect to GooglePlush-this method call to the "openUrl" method in the appDelegate
        Global.sharedInstance.isGooglePlusChecked = true
            
        Global.sharedInstance.googleSignIn.authenticate()
        }
        else
        {
            //בינתיים לא עשיתי על פייסבוק כי לא בטח שצריך-לשאול!
        }
        
    }))
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
    
    }))
        
   // alert.setValue(attributedString, forKey: "attributedMessage")
    
    vc.presentViewController(alert, animated: true, completion: nil)
    }
}
