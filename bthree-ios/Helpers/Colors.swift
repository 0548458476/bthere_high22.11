//
//  Colors.swift
//  BThere
//
//  Created by Tami wexelbom on 9.2.2016.
//  Copyright © 2016 tamy. All rights reserved.
//

import UIKit

class Colors: NSObject {

    class var sharedInstance:Colors {
        struct Static {
            static var onceToken:dispatch_once_t=0
            static var instance:Colors?=nil
        }
        dispatch_once(&Static.onceToken){
            Static.instance=Colors()
        }
        return Static.instance!
    }
    
    
    
    var color1:UIColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)//black color
    var grey:UIColor =  UIColor(red: 77/255.0, green: 77/255.0, blue: 77/255.0, alpha: 1.0)//gray color
    var color2:UIColor = UIColor(red: 245/255.0, green: 240/255.0, blue: 239/255.0, alpha: 1.0)//light A gray color
    var color3:UIColor = UIColor(red: 244/255.0, green: 148/255.0, blue: 113/255.0, alpha: 1.0)//orange color
    var color4:UIColor = UIColor(red: 146/255.0, green: 203/255.0, blue: 216/255.0, alpha: 1.0)//light blue color
    var color5:UIColor = UIColor(red: 76/255.0, green: 76/255.0, blue: 76/255.0, alpha: 1.0)// dark gray color
    var color6:UIColor = UIColor(red: 203/255.0, green: 197/255.0, blue: 190/255.0, alpha: 1.0)//light E gray color
    var color7:UIColor = UIColor(red: 217/255.0, green: 211/255.0, blue: 204/255.0, alpha: 1.0)//light D gray color
    var color8:UIColor = UIColor(red: 231/255.0, green: 225/255.0, blue: 218/255.0, alpha: 1.0)//light C gray color
    var color9:UIColor = UIColor(red: 245/255.0, green: 239/255.0, blue: 232/255.0, alpha: 1.0)//light B gray color
    var fontMainHeader:UIFont = UIFont(name: "OpenSansHebrew-Bold", size: 32)!
    var fontSecondHeader:UIFont = UIFont(name: "OpenSansHebrew-Bold", size: 23)!
    var fontSmallHeader:UIFont = UIFont(name: "OpenSansHebrew-Bold", size: 18)!
    var fontText1:UIFont = UIFont(name: "OpenSansHebrew-Light", size: 23)!
    var fontText2:UIFont = UIFont(name: "OpenSansHebrew-Regular", size: 23)!
    var fontText3:UIFont = UIFont(name: "OpenSansHebrew-Light", size: 16)!
    var fontText4:UIFont = UIFont(name: "OpenSansHebrew-Light", size: 12)!

    func addTopAndBottomBorderWithColor(color: UIColor, width: CGFloat,any :AnyObject) {
        self.addTopBorderWithColor(color, width: width, any: any as! UIView)
        self.addBottomBorderWithColor(color, width: width, any: any as! UIView)
    }
    //design func that get color, width and view and add top border
    func addTopBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(0, 0, any.frame.size.width , width)
        any.layer.addSublayer(border)
    }
        //design func that get color, width and view and add buttom  border
    func addBottomBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(0, any.frame.size.height - width, any.frame.size.width , width)
        any.layer.addSublayer(border)
    }

    
//    func addLeftBorderWithColor(color: UIColor, width: CGFloat,txtField :UITextField) {
//        let border = CALayer()
//        border.backgroundColor = color.CGColor
//        border.frame = CGRectMake(0, 0, width, txtField.frame.size.height)
//        txtField.layer.addSublayer(border)
//    }
        //design func that get color, width and view and add Right border
    func addRightBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(any.frame.size.width - width, 0, width, any.frame.size.height)
        any.layer.addSublayer(border)
    }
        //design func that get color, width and view and add Left border
    func addLeftBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.CGColor
        border.frame = CGRectMake(0, 0, width, any.frame.size.height)
        any.layer.addSublayer(border)
    }
}

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}