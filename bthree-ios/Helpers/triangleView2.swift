//
//  triangleView2.swift
//  bthree-ios
//
//  Created by User on 20.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class triangleView2: UIView {

    override func drawRect(rect: CGRect) {
        
        // Get Height and Width
        let layerHeight = self.layer.frame.height
        let layerWidth = self.layer.frame.width
        
        // Create Path
        let bezierPath = UIBezierPath()
        
        // Draw Points

        bezierPath.closePath()
        
        bezierPath.moveToPoint(CGPointMake(0, 0))
        bezierPath.addLineToPoint(CGPointMake(layerHeight + 12,0))
        bezierPath.addLineToPoint(CGPointMake(layerWidth / 2 ,layerHeight))
        bezierPath.addLineToPoint(CGPointMake(0, 0))
        bezierPath.closePath()
        
        // Apply Color
        UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).setFill()
        bezierPath.fill()
        
        // Mask to Path
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.CGPath
        self.layer.mask = shapeLayer
    }


}
