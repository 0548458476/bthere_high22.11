//
//  ModelCalenderViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/13/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol openSearchResultsDelegate
{
    func openSearchResults()
}

//מודל יומן לקוח(״יומן שלי״ מהתפריט צד‎)
class ModelCalenderViewController: NavigationModelViewController,
UITextFieldDelegate,/*dismissViewControllerDelegate,*/openSearchResultsDelegate,openFromMenuDelegate,UIGestureRecognizerDelegate,clickToDayDelegate,clickToDayInWeekDelegate,openDetailsOrderDelegate{
    
    //MARK: - Properties
    var delegateSetDate:setDateDelegate!=nil

    var attrs = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Bold", size: 23)!,
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSUnderlineStyleAttributeName : 1]
    var attrsDeselect = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 23)!]
    //the category design if click on month clik design open MonthDesigned
      // if click on Record button open design RecordDesign
     // if click on Week button open design WeekDesign
     // if click on Day button open design DayDesign
    var view1 : MonthDesignedViewController!
    var view2 :RecordDesignedViewController!
    var view3 :WeekDesignCalendarViewController!
    var view4 :DayDesignCalendarViewController!
    
     var generic:Generic = Generic()
    
    var attributedString = NSMutableAttributedString(string:"")
    var subView = UIView()
    var selectedView:UIViewController = UIViewController()
    var isAuto = false
    var con:SearchTableViewController?
    var storyboard1:UIStoryboard?
    //fix 2.3
    var filterSubArr:NSArray =  NSArray()
    //var filterSubArr:NSMutableArray =  NSMutableArray()
    
    var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//עבור השליחה לשרת
    
    //MARK: - Outlet
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBAction func btnSearch(sender: AnyObject) {
        Global.sharedInstance.viewConNoInternet = self
        dismissKeyboard()
        
        Global.sharedInstance.searchDomain = txtSearch.text!
        
        if txtSearch.text != ""
        {
            generic.showNativeActivityIndicator(self)
            
            dicSearch["nvKeyWord"] = txtSearch.text
            dicSearch["nvlong"] = Global.sharedInstance.currentLong
            dicSearch["nvlat"] = Global.sharedInstance.currentLat
            dicSearch["nvCity"] = nil
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                //search object by user text pressed
                api.sharedInstance.SearchByKeyWord(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""),vc:self)
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        self.dismissViewControllerAnimated(false, completion: nil)
                        Global.sharedInstance.dicSearchProviders = self.dicSearch
                        Global.sharedInstance.whichSearchTag = 1
                        self.openSearchResults()
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                })
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NOT_DATA_TO_SEARCH", comment: ""),vc: self)
        }
    }
    @IBOutlet weak var btnAdvantageSearch: UIButton!
    //חיפוש מתקדם
    @IBAction func btnAdvantageSearch(sender: AnyObject) {
        
        let viewCon:AdvantageSearchViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AdvantageSearchViewController") as! AdvantageSearchViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        viewCon.delegateSearch = self
        
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    @IBOutlet var recordBtn: UIButton!
    //open record( list) design
    @IBAction func recordBtn(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view2{
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForRecord()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendar == true{
                view2.btnSync.isCecked = true
            }
            else{
                 view2.btnSync.isCecked = false
            }
            self.view.addSubview(view2.view)
            selectedView = view2
        }
        else{
            
            sender.backgroundColor = Colors.sharedInstance.color6
            DeSelectDesighnedBtn(sender)
            sender.tag = 0
        }
        
    }
    @IBOutlet var imgMenu: UIImageView!
    
    @IBOutlet weak var lblAdvertisings: UILabel!
    @IBOutlet var monthlyBtn: UIButton!
        //open month design
    @IBAction func monthlyBtn(sender: UIButton) {
        if sender.tag == 0 || selectedView == view1
        {
            Calendar.sharedInstance.carrentDate = NSDate()
            
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForMonthly()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendar == true{
                view1.btnSync.isCecked = true
            }
            else{
                view1.btnSync.isCecked = false

            }
            self.view.addSubview(view1.view)
            selectedView = view1
        }
        else{
            sender.tag = 0
            sender.backgroundColor = Colors.sharedInstance.color7
            DeSelectDesighnedBtn(sender)
            selectedView = view2
        }
        
    }
    
    @IBOutlet var weekBtn: UIButton!
      //open week design
    @IBAction func weekBtn(sender: UIButton) {
        
        view3 = self.storyboard!.instantiateViewControllerWithIdentifier("WeekDesignCalendar") as! WeekDesignCalendarViewController
        view3.delegate = self
        
        if sender.tag == 0 || selectedView == view3
        {
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForWeek()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendar == true
            {
                view3.btnSync.isCecked = true
            }
            else
            {
                view3.btnSync.isCecked = false
            }
            
            let currentDate:NSDate = Global.sharedInstance.currDateSelected
            let dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
            for i in 0 ..< 7 {
                
                let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                
                Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
            }
            
            if selectedView == view1//חודש
            {
                Global.sharedInstance.currDateSelected = NSDate()
                view3.initDateOfWeek(NSDate())
            }
            else//יום
            {
                view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
            }
            
            view3.view.frame = CGRectMake(0, subView.frame.height, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
             self.view3.delegate = self
            self.view.addSubview(view3.view)
            selectedView = view3
        }
        else
        {
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color8
        }
    }
    
    @IBOutlet var dayBtn: UIButton!
      //open day design
    @IBAction func dayBtn(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view4
        {
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForDay()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendar == true{
                view4.btnSuny.isCecked = true
            }
            else{
                view4.btnSuny.isCecked = false
            }

            /////st‎
            //כדי שאם לוחץ על על ״יום״ אחרי שלחץ על יום מסויים -שלא יציג בתצוגת יום לפי היום ההוא אלא לפי התאריך של היום
            Global.sharedInstance.dateDayClick = NSDate()
            Global.sharedInstance.currDateSelected = NSDate()
            //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
            view4.initDate(Global.sharedInstance.currDateSelected)

            self.view.addSubview(view4.view)
            selectedView = view4


            //העתקתי מהפונקציה  ToDay 3 השורות האלה היו שם וכאן לא
            self.delegateSetDate = Global.sharedInstance.datDesigncalendar
            
            delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
            ////
        }
        else
        {
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color9
        }
    }
    
    //MARK: - Initial
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
    btnAdvantageSearch.setTitle(NSLocalizedString("ADVANTAGED_SEARCH", comment: ""), forState: .Normal)
        txtSearch.placeholder = NSLocalizedString("SERACH_SERVICE_DOMAIN", comment: "")
        recordBtn.setTitle(NSLocalizedString("DESIGN_LIST", comment: ""), forState: .Normal)
        monthlyBtn.setTitle(NSLocalizedString("DESIGN_MONTH", comment: ""), forState: .Normal)
        weekBtn.setTitle(NSLocalizedString("DESIGN_WEEK", comment: ""), forState: .Normal)
        dayBtn.setTitle(NSLocalizedString("DESIGN_DAY", comment: ""), forState: .Normal)
        lblAdvertisings.text = NSLocalizedString("ADVERTISINGS", comment: "")
        
//adjast design to  last iphone version
        checkDevice()
        
        if Global.sharedInstance.isCancelAppointmentClick == false//אם הגיעו מפרטי הזמנה כדי שאם משנה את הסנכרון כפי רצונו, הסנכרון יתקיים
        {
            Global.sharedInstance.isSyncWithGoogelCalendar = Global.sharedInstance.currentUser.bIsGoogleCalendarSync
        }
        //get orders by customer
        self.GetCustomerOrders()
        Global.sharedInstance.model = 1//סימן שהגעתי מהמודל של היומן שלי ללקוח
        Global.sharedInstance.calendarClient = self

//        self.navigationController!.navigationBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 64.0)
        Global.sharedInstance.whichReveal = false

         storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        
        con = self.storyboard!.instantiateViewControllerWithIdentifier("SearchTableViewController") as? SearchTableViewController
        
        Global.sharedInstance.modelCalender = self
        
        txtSearch.delegate = self
        
        subView.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + self.navigationController!.navigationBar.frame.size.height , view.frame.width, view.frame.height * 0.2)
        self.navigationController?.navigationBarHidden = false
        view1 = self.storyboard!.instantiateViewControllerWithIdentifier("MonthDesignedViewController") as! MonthDesignedViewController
        view1.view.frame = CGRectMake(0, subView.frame.height /* + (self.navigationController!.navigationBar.frame.size.height * 0.21) */, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view2 = self.storyboard!.instantiateViewControllerWithIdentifier("RecordDesignedViewController") as! RecordDesignedViewController
        view2.view.frame = CGRectMake(0, subView.frame.height /* + (self.navigationController!.navigationBar.frame.size.height * 0.21) */ , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view3 = self.storyboard!.instantiateViewControllerWithIdentifier("WeekDesignCalendar") as! WeekDesignCalendarViewController
        
        view3.view.frame = CGRectMake(0, subView.frame.height /* + (self.navigationController!.navigationBar.frame.size.height * 0.21) */, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view3.delegate = self
        view4 = self.storyboard!.instantiateViewControllerWithIdentifier("DayDesignCalendarViewController") as! DayDesignCalendarViewController
        
        view4.view.frame = CGRectMake(0, subView.frame.height /* + (self.navigationController!.navigationBar.frame.size.height * 0.21) */ , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        
        self.view.addSubview(self.con!.view)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ModelCalenderViewController.imageTapped))
        imgMenu.userInteractionEnabled = true
        imgMenu.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ModelCalenderViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        self.view.addSubview(self.con!.view)
    }
    
    //MARK: - AutoCopmlete
    
    func searchAutocompleteEntriesWithSubstring(substring: String)  
    {
        if substring.characters.count > 0
        {
            if txtSearch.text != ""
            {
                var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicSearch["nvKeyWord"] = substring
                
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                    api.sharedInstance.SearchWordCompletion(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                        {
                            self.filterSubArr = NSArray()
                            self.filterSubArr = responseObject["Result"] as! NSArray
                            //self.filterSubArr = responseObject["Result"] as! NSMutableArray
                            
                            if self.filterSubArr.count != 0
                            {
                                self.con!.filterSubArr = self.filterSubArr
                                
                                self.view.addSubview((self.con?.view)!)
                                self.con?.tableView.reloadData()
                                if Global.sharedInstance.rtl
                                {
                                    self.con!.view.frame = CGRectMake(140, self.txtSearch.frame.origin.y + self.txtSearch.frame.size.height, self.view.frame.width * 0.665625,  CGFloat(self.view.frame.size.height * 0.2))
                                }
                                else
                                {
                                    self.con!.view.frame = CGRectMake(0, self.txtSearch.frame.origin.y + self.txtSearch.frame.size.height, self.view.frame.width * 0.665625,  CGFloat(self.view.frame.size.height * 0.2))
                                }
                            }
                            else
                            {
                                self.con!.tableView.hidden = true
                            }
                            
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                    })
                }
            }
        }
        else
        {
            self.filterSubArr = []
            self.con!.filterSubArr = self.filterSubArr
            self.con?.tableView.reloadData()
        }
//        if self.filterSubArr.count != 0
//        {
//            self.con!.filterSubArr = self.filterSubArr
//            
//            self.view.addSubview((self.con?.view)!)
//            self.con?.tableView.reloadData()
//            self.con!.view.frame = CGRectMake(140, self.txtSearch.frame.origin.y + self.txtSearch.frame.size.height, self.view.frame.width * 0.665625,  CGFloat(self.view.frame.size.height * 0.2))
//        }
//        if self.filterSubArr.count == 0
//        {
//            self.con!.tableView.hidden = true
//        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        if Reachability.isConnectedToNetwork() == false && string != ""//כדי שהטקסט יכיל גם את האות האחרונה שהקליד
        {
            textField.text = startString
        }
        
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if substring == ""
        {
             self.con!.tableView.hidden = true
        }
        
        else
        {
            if startString.characters.count > 120
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY120_CHARACTERS", comment: ""),vc: self)
                return false
            }
            self.con!.tableView.hidden = false
            searchAutocompleteEntriesWithSubstring(substring)
        }
        return true
      }
    
    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard() {
//        self.con?.tableView.hidden = true
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
     //MARK: - others function
    //open plus menu
    func imageTapped(){
        
        Global.sharedInstance.currentOpenedMenu = self
        
        
        let viewCon:MenuPlusViewController = storyboard1!.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    //MARK: - Design button function
    //design to click button
    func SelectDesighnedBtn(btn:UIButton){
        //btn.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 23)
        btn.backgroundColor = UIColor.clearColor()
        //  btn.titleLabel?.font = ui
        underlineButton(btn, text: (btn.titleLabel?.text)!)
    }
    
    func underlineButton(button : UIButton, text: String) {
        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:text, attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        button.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    func DeSelectDesighnedBtn(btn:UIButton){
        //btn.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 23)
        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string: (btn.titleLabel?.text)!, attributes:attrsDeselect)
        attributedString.appendAttributedString(buttonTitleStr)
        btn.setAttributedTitle(attributedString, forState: .Normal)
        //btn.titleLabel?.font = UIFont.preferredFontForTextStyle(uifontt)
        //btn.backgroundColor = UIColor.clearColor()
        //  btn.titleLabel?.font = ui
        //underlineButton(btn, text: (btn.titleLabel?.text)!)
    }
    
    //    func boldFontWithFont(font: UIFont) -> UIFont {
    //        var fontD: UIFontDescriptor = font.fontDescriptor.fontDescriptorWithSymbolicTraits(uifo)
    //        return UIFont(descriptor: fontD, size: 0)
    //    }
    
    func SelectSingleForRecord(){
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    func SelectSingleForMonthly(){
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    
    func SelectSingleForWeek(){
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    
    func SelectSingleForDay(){
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
    }
    
    //    func dismissViewController()
    //    {
    //        self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
    //
    //        let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalendarForAppointments") as! ModelCalendarForAppointmentsViewController
    //
    //        self.navigationController?.pushViewController(viewCon, animated: false)
    //    }
    
     //MARK: - searchResult
//func that open searchResult controller
    func openSearchResults()
    {
        let frontviewcontroller = storyboard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController

//        let viewCon = self.storyboard!.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
//        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
//        //  viewCon.delegate = self
//        self.navigationController?.pushViewController(viewCon, animated: false)
    }
    
   
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
     func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView((con?.view)!)) {
            
            return false
        }
        return true
    }
    //func enter to day
    func clickToDay(){//open the day design when click one day in the month design
        
        SelectDesighnedBtn(dayBtn)
        //sender.tag = 1
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        self.delegateSetDate = Global.sharedInstance.datDesigncalendar
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)
        self.view.addSubview(view4.view)
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
    }
    //func click To Day In Week

    func clickToDayInWeek()
    {
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)

        self.view.addSubview(view4.view)
        self.delegateSetDate = Global.sharedInstance.datDesigncalendar
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
        underlineButton(dayBtn, text: (dayBtn.titleLabel?.text)!)
    }
    // get customer to orders
    func GetCustomerOrders()
    {
        var dic:Dictionary<String,AnyObject> =  Dictionary<String,AnyObject>()
        var arr = NSArray()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.GetCustomerOrders(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                print(responseObject["Result"])
                arr = responseObject["Result"] as! NSArray
                let ps:OrderDetailsObj = OrderDetailsObj()
                
                Global.sharedInstance.ordersOfClientsArray = ps.OrderDetailsObjToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                
                if Global.sharedInstance.isCancelAppointmentClick == true//לחצו על ביטול תור
                {
                    Global.sharedInstance.isCancelAppointmentClick = false
                    if Global.sharedInstance.whichDesignOpenDetailsAppointment == 1//הגיעו מיום
                    {
                        self.SelectDesighnedBtn(self.dayBtn)
                        self.dayBtn.tag = 1
                        self.SelectSingleForDay()
                        //כדי שאם לוחץ על על ״יום״ אחרי שלחץ על יום מסויים -שלא יציג בתצוגת יום לפי היום ההוא אלא לפי התאריך של היום
                        self.selectedView.view.removeFromSuperview()
                        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
                        self.view4.initDate(Global.sharedInstance.currDateSelected)
                        self.view.addSubview(self.view4.view)
                        self.selectedView = self.view4
                    }
                    else if Global.sharedInstance.whichDesignOpenDetailsAppointment == 2//הגיעו משבוע
                    {
                        self.SelectDesighnedBtn(self.weekBtn)
                        self.weekBtn.tag = 1
                        self.SelectSingleForWeek()
                        
                        self.selectedView.view.removeFromSuperview()
                        
                        self.view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
                        
                        self.view3.view.frame = CGRectMake(0, self.subView.frame.height, self.view.frame.width, self.view.frame.height - self.subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
                        self.view3.delegate = self
                        self.view.addSubview(self.view3.view)
                        
                        self.selectedView = self.view3
                    }
                    else if Global.sharedInstance.whichDesignOpenDetailsAppointment == 3
                    {
                        self.SelectDesighnedBtn(self.recordBtn)
                        self.SelectSingleForRecord()
                        
                        self.selectedView.view.removeFromSuperview()
                        
                        self.view2.view.frame = CGRectMake(0, self.subView.frame.height, self.view.frame.width, self.view.frame.height - self.subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
                        
                        self.view.addSubview(self.view2.view)
                        
                        self.selectedView = self.view2
                    }
                    else
                    {
                        self.view.addSubview(self.view1.view)
                        self.selectedView = self.view1
                        self.SelectDesighnedBtn(self.monthlyBtn)
                    }
                }
                else
                {
                    switch self.SysTableRowString(12, id: Global.sharedInstance.currentUser.iCalendarViewType)
                    {
                    case "חודשי":
                        self.view.addSubview(self.view1.view)
                        self.selectedView = self.view1
                        self.SelectDesighnedBtn(self.monthlyBtn)
                        break
                    case "שבועי":
                        let currentDate:NSDate = Global.sharedInstance.currDateSelected
                        let dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
                        for i in 0 ..< 7 {
                            let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                            Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
                        }
                        
                        
                        self.SelectDesighnedBtn(self.weekBtn)
                        self.weekBtn.tag = 1
                        self.SelectSingleForWeek()
                        self.selectedView.view.removeFromSuperview()
                        self.view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
                        self.view3.view.frame = CGRectMake(0, self.subView.frame.height, self.view.frame.width, self.view.frame.height - self.subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
                        self.view3.delegate = self
                        self.view.addSubview(self.view3.view)
                        self.selectedView = self.view3
                        break
                    case "יומי":
                        self.SelectDesighnedBtn(self.dayBtn)
                        self.dayBtn.tag = 1
                        self.SelectSingleForDay()
                        //כדי שאם לוחץ על על ״יום״ אחרי שלחץ על יום מסויים -שלא יציג בתצוגת יום לפי היום ההוא אלא לפי התאריך של היום
                        self.selectedView.view.removeFromSuperview()
                        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
                        self.view4.initDate(Global.sharedInstance.currDateSelected)
                        self.view.addSubview(self.view4.view)
                        self.selectedView = self.view4
                        break
                    default:
                        self.view.addSubview(self.view1.view)
                        self.selectedView = self.view1
                        self.SelectDesighnedBtn(self.monthlyBtn)
                        break
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
            })
        }
    }
//
    func checkDevice()
    {
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            let fontSize:CGFloat = (self.btnAdvantageSearch.titleLabel?.font.pointSize)!
            btnAdvantageSearch.titleLabel?.font = UIFont(name: (btnAdvantageSearch.titleLabel?.font?.familyName)!, size: 13)!
        }
    }
    
    
    //openDetailsOrderDelegate
    func openDetailsOrder(tag:Int)  {//פונקציה דליגטית שנקראת בעת לחיצה על רבוע פנוי או תפוס(לפי הטאג) ביומן ופותחת את פרטי ההזמנה במודל
        
        let storyboard = UIStoryboard(name: "ClientExist", bundle: nil)
        
        
        let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyboard.instantiateViewControllerWithIdentifier("detailsAppointmetClientViewController") as! detailsAppointmetClientViewController
        vc.tag = tag
        vc.fromViewMode = false
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
        
        //viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        //self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    //מחזיר את ה-string לשורה מסויימת מהטבלה שנבחרה
    //מקבלת את קוד הטבלה אליה לגשת ואת קוד ה-string
    func SysTableRowString(iTableRowId:Int,id:Int)->String
    {
        for sys in Global.sharedInstance.dicSysAlerts[iTableRowId.description]!
        {
            if sys.iTableId == iTableRowId && sys.iSysTableRowId == id
            {
                return sys.nvAletName
            }
        }
        return ""
    }
}
