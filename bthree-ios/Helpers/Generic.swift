//
//  Generic.swift
//  SafePay3DApp
//
//  Created by Lior Ronen on 12/14/15.
//  Copyright © 2015 racheliKrimalovski. All rights reserved.
//

import UIKit
var controller: UIViewController = UIViewController()

class Generic: UIViewController {

    var animatedDistance: CGFloat = 0.0
    let KEYBOARD_ANIMATION_DURATION: CGFloat = 0.3
    let MINIMUM_SCROLL_FRACTION: CGFloat = 0.2
    let MAXIMUM_SCROLL_FRACTION: CGFloat = 0.8
    let PORTRAIT_KEYBOARD_HEIGHT: CGFloat = 216
    let LANDSCAPE_KEYBOARD_HEIGHT: CGFloat = 162
    var activityIndicatorView:UIActivityIndicatorView = UIActivityIndicatorView()
//    convenience override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
//        self.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//        // Custom initialization
//    }
        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func showNativeActivityIndicator(cont: UIViewController) {
       self.hideNativeActivityIndicator(cont)
        controller = cont
        let avToShow: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        self.activityIndicatorView = avToShow
        avToShow.frame = CGRectMake(145, 160, 25, 25)
        avToShow.center = CGPointMake(UIScreen.mainScreen().bounds.size.width / 2, UIScreen.mainScreen().bounds.size.height / 2)
        avToShow.tag = 123
        avToShow.color = UIColor(red: 0.0, green: 0.0, blue: 85.0 / 255.0, alpha: 1.000)
        controller.view.addSubview(avToShow)
        avToShow.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func hideNativeActivityIndicator(cont: UIViewController) {
    
        let v:UIActivityIndicatorView = self.activityIndicatorView
       //
        v.removeFromSuperview()
    
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    func pushTextField(textField: UITextField, controller cont: UIViewController) {
        controller = cont
        let textFieldRect: CGRect = controller.view.convertRect(textField.bounds, fromView: textField)
        let viewRect: CGRect = controller.view.convertRect(controller.view.bounds, fromView: controller.view)
       let midline: CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
    let numerator: CGFloat = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height
    let denominator: CGFloat = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height
    var heightFraction: CGFloat = numerator / denominator
    if heightFraction < 0.0 {
    heightFraction = 0.0
    }
    else {
    if heightFraction > 1.0 {
    heightFraction = 1.0
    }
    }
    let orientation: UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
    if orientation == .Portrait || orientation == .PortraitUpsideDown {
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
    }
    else {
    animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
    }
    var viewFrame: CGRect = controller.view.frame
    viewFrame.origin.y -= animatedDistance
    UIView.beginAnimations(nil, context: nil)

         UIView.setAnimationBeginsFromCurrentState(true)
    UIView.setAnimationDuration(0.3)
    controller.view.frame = viewFrame
    UIView.commitAnimations()
}
    func returnTextField(textField: UITextField, controller cont: UIViewController) {
        controller = cont
        var viewFrame: CGRect = controller.view.frame
        viewFrame.origin.y += animatedDistance
        UIView.beginAnimations(nil, context: nil)
         UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.3)
        controller.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    class func shareGeneric() -> AnyObject {
        var sharedMyManager: Generic? = nil
        var onceToken: dispatch_once_t = 0
        
        dispatch_once(&onceToken, {() -> Void in
            sharedMyManager = self.init()
        })
        return sharedMyManager!
    }
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
