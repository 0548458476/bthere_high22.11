//
//  SPGooglePlacesAutocompleteQuery.swift
//  AutoCompleteGoogle
//



import Foundation
import CoreLocation
import UIKit

//@objc
//protocol SPGooglePlacesAutocompleteQueryDelegate
//{
//   optional func googlePlaceReload(places:[SPGooglePlacesAutocompletePlace])//check
//   optional func googlePlaceGeocoded(latitude:Double, longitude:Double)
//   optional func googlePlaceReverseGeocode(address: String , country: String ,city: String)
//    optional func googlePlaceReverseGeocode(address: String)
//}

protocol SPGooglePlacesAutocompleteQueryDelegate
{
    func googlePlaceReload(places:[SPGooglePlacesAutocompletePlace])//check
    func googlePlaceGeocoded(latitude:Double, longitude:Double)
    func googlePlaceReverseGeocode(address: String , country: String ,city: String)
    func googlePlaceReverseGeocode(address: String)
}

class SPGooglePlacesAutocompleteQuery:NSObject,NSURLConnectionDelegate,CLLocationManagerDelegate {
    
    var input = ""
    var sensor = false
    var key = ""
    var offset = 0
    var location: CLLocationCoordinate2D?
    var radius = 0.0
    var language = ""
    var data: NSMutableData?
    var referenceToGeocoded = ""
    var connectionAutocomplete = NSURLConnection()
    var connectionGeocoded : NSURLConnection = NSURLConnection()
    var connectionReverseGeocode = NSURLConnection()
    var locationManager = CLLocationManager()
    var delegate: SPGooglePlacesAutocompleteQueryDelegate! = nil

    //save the name of the city from all the address
    
    var city:String = ""
    
    
    override init() {
        super.init()
        sensor=true
        
        //key="AIzaSyBgqeatOXG68SBPQNXzubwe0BDY-1yLXlI"//"AIzaSyAFsaDn7vyI8pS53zBgYRxu0HfRwYqH-9E"// define ???
        key="AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg"//"AIzaSyB6Cx_qTAXcpQbxeSP5InlDch2W8Bum2tQ"//You should use the server api key instead of iOS api key
//        key="AIzaSyDM-qQb04BOnWedEGKGz8Dbv3ZGea54LlI"//bundel webit.travelwayz-ios
    
        location=CLLocationCoordinate2DMake(-1, -1);
        radius = 50.0
        input = "jer"
        location=CLLocationCoordinate2DMake(31.922866, 35.047609)
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func getAdrressFromCoordinate (latitude: Double, longitude: Double){
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks!.count > 0 {
                    let pm = placemarks![0] 
                    self.displayLocationInfo(pm)
                } else {
                    print("Problem with the date recieved from geocoder")
                }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //        if placemark != nil {
        var tempString : String = ""
        var country = ""
        var city = ""
        
        if(placemark.locality != nil){
            tempString = tempString +  placemark.locality! + "\n"
            city = placemark.locality!
        }
        if(placemark.postalCode != nil){
            tempString = tempString +  placemark.postalCode! + "\n"
        }
        if(placemark.administrativeArea != nil){
            tempString = tempString +  placemark.administrativeArea! + "\n"
        }
        if(placemark.country != nil){
            tempString = tempString +  placemark.country! + "\n"
            country = placemark.country!
        }
        print("Adrress = " + tempString)
        delegate.googlePlaceReverseGeocode(tempString, country: country, city: city)
//        delegate.googlePlaceReverseGeocode!(tempString, country: country, city: city)//check
    }
    
    func googleURLString() ->String{
        
        
//        error:
//       	"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%D7%9C&sensor=true&key=AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg&location=31.922866,35.047609&radius=50.000000"
//        
//        seccsess:
//        
//        https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%D7%97&types=geocode&language=en&key=AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg
//        
        
       // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location

        var url:String = ""
        if Global.sharedInstance.isAddressCity == false
        {
             url = NSString(format:"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&language=en&key=%@", input.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!,key) as String
        }
        else
        {
        //url for cities names only
         url = NSString(format:"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=(cities)&language=en&key=%@", input.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!,key) as String
        }
        

       // url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=ל&types=geocode&language=en&key=AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg"
//        if offset>0 {
//            url += NSString(format:"&offset=%u", offset) as String}
//        if location?.latitude != -1 {
//            url += NSString(format:"&location=%f,%f", location!.latitude, location!.longitude) as String}
//        if radius>0 {
//            url += NSString(format: "&radius=%f", radius) as String}
//        if !language.isEmpty {
//            url += NSString(format:"&language=%@", language) as String}
        return url
    }
    
    func googleGeocoded(referenceToGeocoded reference:String){
        referenceToGeocoded=reference
        "https://maps.googleapis.com/maps/api/place/details/json?reference=%@key=%@"
        let urlBasic = NSString(format:"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@", referenceToGeocoded.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!,key) as String
        let urlPath: String = urlBasic
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        connectionGeocoded = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connectionGeocoded.start()
    }
    
    func fetchPlaces(){
        data = NSMutableData()  // Declare Globally
        let urlPath: String = googleURLString()
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        connectionAutocomplete = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connectionAutocomplete.start()
    }
    
    func googleReverseGeocode(latitude:Double, longitude:Double){
        data = NSMutableData()  // Declare Globally
        let urlPath: String = "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&sensor=true"
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        connectionReverseGeocode = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connectionReverseGeocode.start()
    }
    
    func succeedWithPlaces(places: [Dictionary<String, AnyObject>]){
        
        var parsedPlaces:[SPGooglePlacesAutocompletePlace] = Array()
 
        for place: Dictionary<String, AnyObject> in places {

            let s = SPGooglePlacesAutocompletePlace.placeFromDictionary(place)
            parsedPlaces.append(s)
        }
        
        if delegate != nil
        {
         delegate.googlePlaceReload(parsedPlaces)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
//        var userLocation:CLLocation = newLocation
//        let long = userLocation.coordinate.longitude;
//        let lat = userLocation.coordinate.latitude;
        if newLocation != oldLocation {
            locationManager.stopUpdatingLocation()
        }
           }
    
    // MARK:NSURLConnectionDelegate
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.data?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!){
        //var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        
        do {
            if data?.description != "<>"{
                
                
                let jsonResult: AnyObject = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers))
                
                
                if connection == connectionAutocomplete{
                    let status = jsonResult.objectForKey("status") as! NSString
                    //let json = jsonResult as? Dictionary<String, AnyObject>
                    if status.isEqualToString("OK")
                    {
                        //var d: AnyObject? = jsonResult.objectForKey("predictions")
                        succeedWithPlaces(jsonResult.objectForKey("predictions") as! [Dictionary<String, AnyObject>])
                    }
                    if status.isEqualToString("INVALID_REQUEST")
                    {
                        //                var d: AnyObject? = jsonResult.objectForKey("predictions")
                        //       ®         succeedWithPlaces(jsonResult.objectForKey("predictions") as! [Dictionary<String, AnyObject>])
                    }
                    
                    return
                }
                else if connection == connectionGeocoded{
                    if let result = jsonResult.objectForKey("result") as? Dictionary<String, AnyObject> {
                        if let geometry = result["geometry"] as? Dictionary<String, AnyObject> {
                            if let location = geometry["location"] as? Dictionary<String, Double> {
                                if let lat = location["lat"]{
                                    if let lng = location["lng"]{
                                        //                                delegate.googlePlaceGeocoded!( lat , longitude: lng)//check
                                        delegate.googlePlaceGeocoded( lat , longitude: lng)
                                        
                                    }
                                }
                            }
                        }
                    }
                    return
                }
                else if connection == connectionReverseGeocode{
                    let status = jsonResult.objectForKey("status") as! NSString
                    //let json = jsonResult as? Dictionary<String, AnyObject>
                    if status.isEqualToString("OK")
                    {
                        let listResult =  jsonResult.objectForKey("results") as! NSArray
                        let address = listResult[0].objectForKey("formatted_address") as! String
                        print(address)
                        //                delegate.googlePlaceReverseGeocode!(address)//check
                        delegate.googlePlaceReverseGeocode(address)
                        
                    }
                }
            }
        }
        catch (_) {
            //here you can get access to all of the errors that occurred when trying to serialize
        }
    }
    
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse){
        data?.length=0
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError){
        data=nil
    }
}
