//
//  entranceViewController.swift
//  bthree-ios
//
//  Created by Tami wexelbom on 21.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol openControlersDelegate
{
    func openVerification(phone:String)
    func openCustomerOrProvider()
}
//דף כניסה ראשוני(כולל חיפוש מתקדם)
class entranceViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,openSearchResultsDelegate,openControlersDelegate,openChooseUserDelegate,openCustomerDetailsDelegate
    ,openFromMenuDelegate
{
    
    //MARK: - Outlet
    //===========Outlet====================
    
    @IBOutlet weak var btnLogIn: UIButton!
    
    @IBOutlet weak var tblSearch: UITableView!
    
    @IBOutlet var imgView: UIView!
    
    @IBOutlet weak var btnOpemMenu: UIImageView!
    
    @IBOutlet weak var btnOpenMenu: UIImageView!
    
    @IBOutlet weak var btnRegisterOutlet: UIButton!
    @IBAction func btnRegister(sender: AnyObject)
    {
        Global.sharedInstance.isFromViewMode = false
        let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.navigationController?.pushViewController(viewCon, animated: false)
    }
    @IBAction func btnSearchAction(sender: UIButton)
    {
        Global.sharedInstance.isProvider = false
        let viewCon:AdvantageSearchViewController = clientStoryBoard!.instantiateViewControllerWithIdentifier("AdvantageSearchViewController") as! AdvantageSearchViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        viewCon.delegateSearch = self
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var viewOnLogin: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBAction func btnLogIn(sender: AnyObject) {
        
        showPopUp()
    }
    
    var attributedStrings = NSMutableAttributedString(string:"")
    
    var isAuto = false
    
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBAction func btnSearch(sender: AnyObject) {
        Global.sharedInstance.viewConNoInternet = self
        dismissKeyboard()
        
        Global.sharedInstance.searchDomain = txtSearch.text!
        
        if txtSearch.text != ""
        {
            generic.showNativeActivityIndicator(self)
            
            Global.sharedInstance.dicSearch["nvKeyWord"] = txtSearch.text
            Global.sharedInstance.dicSearch["nvlong"] = Global.sharedInstance.currentLong
            Global.sharedInstance.dicSearch["nvlat"] = Global.sharedInstance.currentLat
            Global.sharedInstance.dicSearch["nvCity"] = nil
            
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.SearchByKeyWord(Global.sharedInstance.dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        self.dismissViewControllerAnimated(false, completion: nil)
                        Global.sharedInstance.dicSearchProviders = Global.sharedInstance.dicSearch
                        Global.sharedInstance.whichSearchTag = 1
                        self.delegateSearch.openSearchResults()
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                })
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NOT_DATA_TO_SEARCH", comment: ""),vc: self)
        }
    }
    
    @IBOutlet weak var btnSeachAdvantage: UIButton!
    
    @IBAction func btnLanguage(sender: AnyObject) {
        openLanguage()
    }
    
    @IBOutlet weak var lblAdvertisings: UILabel!
    @IBOutlet weak var btnLanguage: UIButton!
    
    //MARK: - Properties
    //===========Properties============
    
    var filterSubArr:NSArray =  NSArray()
    var delegateSearch:openSearchResultsDelegate!=nil
    
    var clientStoryBoard:UIStoryboard?
    var filtered:Array<String> = []
    
    var searchActive : Bool = false
    
    var generic:Generic = Generic()
    
    ///for underline of buttons
    var attrs = [
        NSFontAttributeName : UIFont(name: "OpenSansHebrew-Light", size: 19)!,
        NSForegroundColorAttributeName : Colors.sharedInstance.color1,
        NSUnderlineStyleAttributeName : 1]
    var attributedStringsUserExist = NSMutableAttributedString(string:"")
    var attributedStringsLanguage = NSMutableAttributedString(string:"")
    
    //MARK: - Initial
    //==============Initial============
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRegisterOutlet.setTitle(NSLocalizedString("REGISTER_BTN", comment: ""), forState: .Normal)
        btnSeachAdvantage.setTitle(NSLocalizedString("ADVANTAGED_SEARCH", comment: ""), forState: .Normal)
        txtSearch.placeholder =  NSLocalizedString("SERACH_SERVICE_DOMAIN", comment: "")
        btnLogin.setTitle(NSLocalizedString("LOGIN", comment: ""), forState: .Normal)
        lblAdvertisings.text = NSLocalizedString("ADVERTISINGS", comment: "")
        
        Global.sharedInstance.dicSearch = Dictionary<String,AnyObject>()//עבור השליחה לשרת
        
        delegateSearch = self
        
        let tapViewLogin = UITapGestureRecognizer(target: self, action: #selector(showPopUp))
        viewOnLogin.addGestureRecognizer(tapViewLogin)
        
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
                txtSearch.delegate = self
        
        // לפי בקשת הלקוחה, הורדתי את הכפתור לפתיחת תפריט +
        
        //        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(entranceViewController.imageTapped))
        //        btnOpemMenu.userInteractionEnabled = true
        //        btnOpemMenu.addGestureRecognizer(tapGestureRecognizer)
        self.view.bringSubviewToFront(tblSearch)
        
        let buttonTitleStr1 = NSMutableAttributedString(string:"Language", attributes:attrs)
        attributedStringsLanguage.appendAttributedString(buttonTitleStr1)
        btnLanguage.setAttributedTitle(attributedStringsLanguage, forState: .Normal)
        
        self.imgView.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        tblSearch.hidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(entranceViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        
        let backView: UIView = UIView(frame: CGRectMake(0, 0, 120, 40))
        
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "3.png"))
        titleImageView.frame = CGRectMake(0, 0, titleImageView.frame.size.width * 0.8, 40)
        // Here I am passing origin as (45,5) but can pass them as your requirement.
        backView.addSubview(titleImageView)
        
        self.navigationItem.titleView = backView
        
        UIApplication.sharedApplication().statusBarFrame.size.height
       
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        Colors.sharedInstance.addBottomBorderWithColor(UIColor.blackColor(), width: 1, any: lineView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - TableView
    //==========TableView===============
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterSubArr.count == 0
        {
            tblSearch.hidden = true
        }
        return filterSubArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Search")as! SearchTableViewCell
        cell.setDisplayData(filterSubArr[indexPath.row] as! String)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.09687
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        txtSearch.text = filterSubArr[indexPath.row] as? String
        tblSearch.hidden = true
    }
    
    //MARK: - Searching
    //==========Searching===========
    
    func searching(searchText: String) {
        let s:CGFloat = view.frame.size.height - tblSearch.frame.origin.x
        searchActive = true
        if searchText == ""{
            filtered =  []
        }
        else
        {
            filtered = Global.sharedInstance.categoriesArray.filter({(city) ->  Bool in
                var tmp: NSString = ""
                if tmp.description.characters.count >= searchText.characters.count{
                    tmp = tmp.substringToIndex(searchText.characters.count)
                }
                
                let range = tmp.rangeOfString(searchText,options: NSStringCompareOptions.CaseInsensitiveSearch)
                return range.location != NSNotFound
            })
        }
        if(filtered.count == 0){
            self.tblSearch.hidden = true
        }
        else{
            self.tblSearch.hidden = false
        }
        
        uploadTable()
    }
    
    func uploadTable(){
        self.tblSearch.reloadData()
    }
    
    func imageTapped(){
        
        Global.sharedInstance.currentOpenedMenu = self
        
        let viewCon:MenuPlusViewController = storyboard?.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print("begin")
    }
    func dismissKeyboard() {
//        tblSearch.hidden = true
        self.view.endEditing(true)
    }
    
    //for autoCopmlete:
    func searchAutocompleteEntriesWithSubstring(substring: String)
    {
        if substring.characters.count > 0
        {
            if txtSearch.text != ""
            {
                var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicSearch["nvKeyWord"] = substring
                
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                    api.sharedInstance.SearchWordCompletion(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                        {//1.fix 2.3
                            self.filterSubArr = NSArray() //from NSMutableArray
                            self.filterSubArr = responseObject["Result"] as! NSArray
                            self.tblSearch.reloadData()
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                    })
                }
            }
        }
    }
    
    ////for the autoComplete
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //בשביל ה-autoComplete
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if substring == ""
        {
            tblSearch.hidden = true
        }
        else
        {
            if startString.characters.count > 120
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY120_CHARACTERS", comment: ""),vc: self)
                return false
            }
            
            tblSearch.hidden = false
            searchAutocompleteEntriesWithSubstring(substring)
        }
        
        return true
    }
    
    func openSearchResults()
    {
        let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = clientStoryBoard?.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    //פתיחת קוד אימות
    func openVerification(phone:String)
    {
        let viewCon:MessageVerificationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVerificationViewController") as! MessageVerificationViewController
        viewCon.phone = phone
        viewCon.delegateCon = self
        viewCon.isFromPersonalDetails = false
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func openCustomerOrProvider()
    {
        let viewCon:CustomerOrDistributorViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CustomerOrDistributorViewController") as! CustomerOrDistributorViewController
        viewCon.delegate = self
        viewCon.delegate1 = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func openBuisnessDetails()
    {
        let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        
        viewCon.delegateFirstSection = viewCon1
        viewCon.delegateSecond1Section = viewCon1
        
        
        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        front.pushViewController(viewCon, animated: false)
        
        
        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = front
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func openCustomerDetails()
    {
        let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        let viewCon:ModelCalenderViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as! ModelCalenderViewController
        
        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        front.pushViewController(viewCon, animated: false)
        
        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = front
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
    //שהלחיצה ב-didSelect של טבלת חיפוש התחומים תהיה יותר בקלות
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(tblSearch)) {
            
            return false
        }
        return true
    }
    
    func showPopUp() {
        let viewCon:ExistingUserPhoneViewController = storyboard?.instantiateViewControllerWithIdentifier("existsUserPhone") as! ExistingUserPhoneViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func openLanguage(){
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc:langForEntranceViewController = storyboardCExist.instantiateViewControllerWithIdentifier("langForEntranceViewController") as! langForEntranceViewController
        
        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        front.pushViewController(vc, animated: false)
        
        
        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = front
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
        
    }
}
