//  objGeneralDetails.swift
//  bthree-ios
//
//  Created by User on 30.3.2016.
//  Copyright © 2016 Webit. All rights reserved.

import UIKit

class objGeneralDetails: NSObject {

    var iProviderUserId:Int = 0
    var iFieldId:Int = 0
    
    var arrObjWorkingHours:Array<objWorkingHours> = Array<objWorkingHours>()
    var arrObjServiceProviders:Array<objServiceProviders> = Array<objServiceProviders>()
    var arrObjProviderServices:Array<objProviderServices> = Array<objProviderServices>()
    var calendarProperties:objCalendarProperties = objCalendarProperties()

    override init() {
        
        iProviderUserId = 0
        iFieldId = 0
        arrObjWorkingHours = Array<objWorkingHours>()
        arrObjServiceProviders = Array<objServiceProviders>()
        arrObjProviderServices = Array<objProviderServices>()
        calendarProperties = objCalendarProperties()
    }
    
    init(_iProviderUserId:Int,_iFieldId:Int,_arrObjWorkingHours:Array<objWorkingHours>,_arrObjServiceProviders:Array<objServiceProviders>,_arrObjProviderServices:Array<objProviderServices>,_calendarProperties:objCalendarProperties) {
        
        iProviderUserId = _iProviderUserId
        iFieldId = _iFieldId
        arrObjWorkingHours = _arrObjWorkingHours
        arrObjServiceProviders = _arrObjServiceProviders
        arrObjProviderServices = _arrObjProviderServices
        calendarProperties = _calendarProperties
    }
    
    func getDicWorkingHours()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in Global.sharedInstance.generalDetails.arrObjWorkingHours
        {
            dic = it.getDic()
            arr.append(dic)
        }
        return arr
    }
    //2do-delete
    func getDicWorkingHoursExample()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        let arr:Array<AnyObject> = []
        return arr
    }
    
    func getDicServiceProviders()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in arrObjServiceProviders
        {
            dic = it.getDic()
            arr.append(dic)
        }
        return arr
    }
    //2do-delete
    func getDicServiceProvidersExample()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        let arr:Array<AnyObject> = []
        return arr
    }
    
    func getDicProviderServices()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for var it in arrObjProviderServices
        {
            dic = it.getDic()
            arr.append(dic)
        }
        return arr
    }
    
    //2do - delete
    func getDicProviderServicesExample()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        let arr:Array<AnyObject> = []
        return arr
    }

    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iProviderUserId"] = Global.sharedInstance.currentProvider.iIdBuisnessDetails
            //Global.sharedInstance.currentProvider.iUserId
        dic["iFieldId"] = iFieldId
        dic["objWorkingHours"] = getDicWorkingHours()
        dic["objServiceProviders"] = getDicServiceProviders()
        dic["objProviderServices"] = getDicProviderServices()
        dic["objCalendarProperties"] = calendarProperties.getDic()
        
        return dic
    }
    //2do-delete
    func getDicExample()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iProviderUserId"] = 0
        dic["iFieldId"] = 0
        
        dic["objWorkingHours"] = getDicWorkingHoursExample()
        dic["objServiceProviders"] = getDicServiceProvidersExample()
        dic["objProviderServices"] = getDicProviderServicesExample()
        dic["objCalendarProperties"] = calendarProperties.getDicExample()
        
        return dic
    }

    
    func dicToObjGeneralDetails(dic:Dictionary<String,AnyObject>)->objGeneralDetails
    {
        let generalDetails:objGeneralDetails = objGeneralDetails()
        
        generalDetails.iProviderUserId = Global.sharedInstance.parseJsonToInt(dic["iProviderUserId"]!)
        generalDetails.iFieldId = Global.sharedInstance.parseJsonToInt(dic["iFieldId"]!)
        if dic["objWorkingHours"] as? NSDictionary != nil
        {
            let wrHours:objWorkingHours = objWorkingHours()
            generalDetails.arrObjWorkingHours =  wrHours.objWorkingHoursToArray(dic["objWorkingHours"] as! Array<Dictionary<String,AnyObject>>)
        }
        if dic["objServiceProviders"] as? NSDictionary != nil
        {
            let servProviders:objServiceProviders = objServiceProviders()
            generalDetails.arrObjServiceProviders = servProviders.objServiceProvidersToArray(dic["objServiceProviders"] as! Array<Dictionary<String,AnyObject>>)
        }
        if dic["objProviderServices"] as? NSDictionary != nil
        {
            let providerServ:objProviderServices = objProviderServices()
            generalDetails.arrObjProviderServices = providerServ.objProviderServicesToArray(dic["objProviderServices"] as! Array<Dictionary<String,AnyObject>>)
        }
        if dic["objCalendarProperties"] as? NSDictionary != nil
        {
            let calendarProp:objCalendarProperties = objCalendarProperties()
            generalDetails.calendarProperties = calendarProp.dicToCalendarProperties(dic["objCalendarProperties"] as! Dictionary<String,AnyObject>)
        }
        
        return generalDetails
    }
}
