//
//  ChooseWorkerTableViewController.swift
//  bthree-ios
//
//  Created by User on 3.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
    // MARK: - Protocol
protocol hideTableDelegate
{
    func hideTable()
}
protocol reloadTableFreeDaysDelegate {
    func reloadTableFreeDays()
}
//תת טבלה של נותני שירות (בחר איש צוות)
class ChooseWorkerTableViewController: UITableViewController,hideTableDelegate,reloadTblDelegate
{
    // MARK: - Varibals
    var delegateFreeDays:reloadTableFreeDaysDelegate!=nil
    var delegate:selectWorkerDelegate!=nil

    var arrayWorkers:Array<String> = Array<String> ()

    var getFreeDaysForService:Array<providerFreeDaysObj> =  Array<providerFreeDaysObj>()
    var generic:Generic = Generic()
    // MARK: - Initials
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayWorkers = []

   self.delegateFreeDays = Global.sharedInstance.designMonthAppointment
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorStyle = .None
        Global.sharedInstance.modelCalenderForAppointment?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //function that send iProviderId and get list of Services Providers
    func getServicesProviderForSupplierfunc()
    {
        arrayWorkers = []
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = Global.sharedInstance.providerID
        var arrUsers:Array<User> = Array<User>()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.getServicesProviderForSupplierfunc(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_GIVES_SERVICE", comment: ""))
                    
                    
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    var Arr:NSArray = NSArray()
                    
                    Arr = responseObject["Result"] as! NSArray
                    let u:User = User()
                    arrUsers = u.usersToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    if arrUsers.count != 0
                    {
                        for u:User in arrUsers
                        {
                            let s = u.nvFirstName + " " + u.nvLastName
                            self.arrayWorkers.append(s)
                        }
                        self.tableView.reloadData()
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    
            })
        }
        
    }

   
    

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Global.sharedInstance.arrayWorkers.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WorkerNameTableViewCell") as! WorkerNameTableViewCell
        cell.selectionStyle = .None
        cell.setDisplayData(Global.sharedInstance.arrayWorkers[indexPath.row])
        if indexPath.row == Global.sharedInstance.arrayWorkers.count-1{
            cell.viewButtom.hidden = true
        }
        else{
            cell.viewButtom.hidden = false
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //אם בחרתי עובד מסוים
        if (tableView.cellForRowAtIndexPath(indexPath) as! WorkerNameTableViewCell).lblDesc.text! != NSLocalizedString("NOT_CARE", comment: "")// "לא משנה לי"//Global.sharedInstance.giveServicesArray.count > indexPath.row//(tableView.cellForRowAtIndexPath(indexPath) as! WorkerNameTableViewCell).lblDesc.text! != "לא משנה לי"
        {
            delegate.selectWorker((tableView.cellForRowAtIndexPath(indexPath) as! WorkerNameTableViewCell).lblDesc.text!)
            
            let id  = Global.sharedInstance.giveServicesArray[indexPath.row - 1].iUserId
            Global.sharedInstance.idWorker = id

            
            Global.sharedInstance.indexRowForIdGiveService = indexPath.row
            
            getFreeDaysForServiceProvider()
        }
        else//אם בחרתי - לא משנה לי
        {
            delegate.selectWorker((tableView.cellForRowAtIndexPath(indexPath) as! WorkerNameTableViewCell).lblDesc.text!)
            Global.sharedInstance.idWorker = -1
            Global.sharedInstance.indexRowForIdGiveService = -1
            getFreeDaysForServiceProvider()
        }
    }

    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return self.view.frame.size.height * 0.25
        //(tblDetailsHeight.constant / 5 )
        
    }
    
    func hideTable()
    {
        self.view.removeFromSuperview()
    }
    
    func reloadTbl(){
        self.tableView.reloadData()
    }
    func reloadHeight(){
        
    }

    //MARK: - api functions
    
    //function that send iProviderId and get list of Services Providers
//    func getServicesProviderForSupplierfunc()
//    {
//        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//        dicSearch["iProviderId"] = Global.sharedInstance.providerID
//        var arrUsers:Array<User> = Array<User>()
//        
//        api.sharedInstance.getServicesProviderForSupplierfunc(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
//            print(responseObject)
//            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
//            {
//                Alert.sharedInstance.showAlertDelegate("לא קיימים נותני שירות לספק זה ")
//                
//                self.delegateFreeDays = Global.sharedInstance.desingMonth
//                
//            }
//            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
//            {
//                var Arr:NSArray = NSArray()
//                
//                Arr = responseObject["Result"] as! NSArray
//                var u:User = User()
//                arrUsers = u.usersToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
//                if arrUsers.count == 0
//                {
//                    //Alert.sharedInstance.showAlertDelegate("לא קיימים נותני שירות לספק זה ")
//                    
//                    print(arrUsers.count)
//                }
//                    //                if arrUsers.count == 0
//                    //                {
//                    //                    Alert.sharedInstance.showAlertAppDelegate("אין תקשורת,נסה להרשם מאוחר יותר")
//                    //
//                    //                }
//                else
//                {
//                    
//                    for u:User in arrUsers
//                    {
//                        let s = u.nvFirstName + " " + u.nvLastName
//                        self.arrayWorkers.append(s)
//                    }
//                    self.tableView.reloadData()
//                }
//            }
//            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
//                
//        })
//        
//        
//    }
    
    //function that send ServiseProviders and lProviderServices and get list FreeDaysForService
    func getFreeDaysForServiceProvider(){
        //        var dicGetFreeDaysForServiceProvider:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        Global.sharedInstance.arrayGiveServicesKods = []
        Global.sharedInstance.dateFreeDays = []
        
        if Global.sharedInstance.idWorker == -1//אם בחרו - לא משנה לי
        {
            for var item in Global.sharedInstance.giveServicesArray
            {
                Global.sharedInstance.arrayGiveServicesKods.append(item.iUserId)//אחסון כל הקודים של נותני השרות כי בחר - לא משנה לי
            }
        }
        else
        {
            
            //מערך שאוחסנו בו השעות של הימים הפנויים
            Global.sharedInstance.arrayGiveServicesKods.append(Global.sharedInstance.idWorker)
        }
        Global.sharedInstance.dicGetFreeDaysForServiceProvider["lServiseProviderId"] = Global.sharedInstance.arrayGiveServicesKods
        Global.sharedInstance.dicGetFreeDaysForServiceProvider["lProviderServiceId"] = Global.sharedInstance.arrayServicesKodsToServer
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.GetFreeDaysForServiceProvider(Global.sharedInstance.dicGetFreeDaysForServiceProvider, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    var Arr:NSArray = NSArray()
                    
                    Arr = responseObject["Result"] as! NSArray
                    let ps:providerFreeDaysObj = providerFreeDaysObj()
                    
                    Global.sharedInstance.getFreeDaysForService = ps.objFreeDaysToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    //איפוס המערך מנתונים ישנים
                    Global.sharedInstance.dateFreeDays = []
                    for i in 0 ..< Global.sharedInstance.getFreeDaysForService.count{
                        let dateDt = Calendar.sharedInstance.addDay(Global.sharedInstance.getStringFromDateString(Global.sharedInstance.getFreeDaysForService[i].dtDate))
                        let provider:providerFreeDaysObj = Global.sharedInstance.getFreeDaysForService[i]
                        
                        let hourStart = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvFromHour)
                        Global.sharedInstance.fromHourArray.append(hourStart)
                        let hourEnd = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvToHour)
                        Global.sharedInstance.endHourArray.append(hourEnd)
                        
                        Global.sharedInstance.dateFreeDays.append(dateDt)
                    }
                    self.delegateFreeDays.reloadTableFreeDays()
                }
                 self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: "")) 
            })
        }
    }

   }
