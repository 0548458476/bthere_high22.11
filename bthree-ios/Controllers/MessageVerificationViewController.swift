//
//  MessageVerificationViewController.swift
//  bthree-ios
//
//  Created by Tami wexelbom on 18.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import Darwin
//תת דף של הודעת אימות 
class MessageVerificationViewController: UIViewController {

    //MARK: - Properties
    
    var numTries = 0
    var phone:String = ""
    var isProviderClickd = false
    var verification:String = ""
    var numClickedSendAgain:Int = 0//מונה את מספר הפעמים שלחצו על שלח שוב
    var generic:Generic = Generic()
    var delegate:openChooseUserDelegate!=nil
    var delegateCon:openControlersDelegate!=nil
    var delegate1:openCustomerDetailsDelegate!=nil
    var delegateShowPhone:popUpPhoneDelegate!=nil
    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
    var supplierStoryBoard:UIStoryboard?
    var storyboardMain:UIStoryboard?
    var currentUserToEdit:User?
    var isFromPersonalDetails = Bool()//מציין אם הגיעו מעדכון פרטים אישיים וערכו את הטלפון
    
    //MARK: - Outlet
    
    @IBOutlet weak var notGetVer: UILabel!
    
    @IBOutlet weak var enterVerification: UILabel!
    
    @IBOutlet weak var lblTitleMessageVer: UILabel!
    //כפתור שלח שוב
    @IBAction func btnSendAgain(sender: AnyObject)
    {
        numClickedSendAgain += 1
        if numClickedSendAgain == 2
        {
            //מעבר חזרה לפופאפ של הכנסת טלפון שוב
            self.dismissViewControllerAnimated(true, completion: nil)
            if delegateShowPhone != nil
            {
                self.delegateShowPhone.deleteTxtPhone()
            }
        }
        var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
        dicPhone["nvPhoneNumber"] = phone
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.GetAndSmsValidationCode(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.generic.hideNativeActivityIndicator(self)
                
                }
                ,failure: {(AFHTTPRequestOperation, NSError) -> Void in
            })
        }
    }
    @IBOutlet weak var btnSendAgain: UIButton!
    @IBOutlet weak var txtVerification: UITextField!
    
    @IBAction func cancelBtn(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        Global.sharedInstance.isRegisterProviderClick = false
        Global.sharedInstance.isRegisterClientClick = false
    }
    
    @IBOutlet weak var btnSendMessage: UIButton!
    //המשך
    @IBAction func btnSendMessage(sender: UIButton)
    {
        if isFromPersonalDetails == true
        {
            var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
            dicPhone["nvPhoneNumber"] = phone
            
            self.generic.showNativeActivityIndicator(self)
            if Reachability.isConnectedToNetwork() == false
            {
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            }
            else
            {
                api.sharedInstance.GetAndSmsValidationCode(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == 0//הצליח
                    {
                        if Int(self.txtVerification.text!) == responseObject["Result"] as? Int
                        {
                            //שמירת פרטי הבנ״א
                            self.UpdateUser()
                        }
                        else
                        {
                            Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_VERIFICATION", comment: ""), vc: self)
                            self.txtVerification.text = ""
                            self.numTries+=1
                            if self.numTries == 3
                            {
                                self.numTries = 0
                                exit(0)
                            }
                        }
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_VERIFICATION", comment: ""), vc: self)
                        self.txtVerification.text = ""
                        self.numTries+=1
                        if self.numTries == 3
                        {
                            self.numTries = 0
                            exit(0)
                        }
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    }
                    ,failure: {(AFHTTPRequestOperation, NSError) -> Void in
                })
            }
        }
        else if Global.sharedInstance.isFromRegister == false//הגיע מהלוגין
        {
            if txtVerification.text != ""
            {
                var dicLoginUser:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicLoginUser["nvVerCode"] = txtVerification.text
                dicLoginUser["nvPhone"] = phone
                
                self.generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                }
                else
                {
                    api.sharedInstance.LoginUser(dicLoginUser, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if (responseObject["Result"] as! Int) != 0
                        {
                            var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                            
                            dicUserId["currentUserId"] = responseObject["Result"] as! Int
                            //שמירת הuserId- במכשיר
                            Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                            
                            Global.sharedInstance.currentUser.iUserId = responseObject["Result"] as! Int
                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                                
                            self.openCustomerOrProvider()
                            
                            self.numTries = 0
                            
                            var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                            dicForDefault["verification"] = self.txtVerification.text
                            dicForDefault["phone"] = self.phone
                            //שמירת הטלפון והקוד ב-default
                            Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "verificationPhone")
                        }
                        else
                        {
                            Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_VERIFICATION", comment: ""), vc: self)
                            self.txtVerification.text = ""
                            self.numTries+=1
                            if self.numTries == 3
                            {
                                self.numTries = 0
                                exit(0)
                            }
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    })
                }
            }
            else
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("PASSWORD_NOT_ENTERD", comment: ""), vc: self)
            }
        }
        else//רישום רגיל
        {
            var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
            dicPhone["nvPhoneNumber"] = phone
            
            if self.verification == self.txtVerification.text //הקוד שהכניס תקין
            {
                self.numTries = 0
                
                var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicForDefault["verification"] = self.txtVerification.text
                dicForDefault["phone"] = self.phone
                //שמירת הטלפון והקוד ב-default
                Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "verificationPhone")
                
                self.dismissViewControllerAnimated(true, completion: nil)
                
                //שמירת הנתונים של ה-user בשרת
                var dicRegister:Dictionary<String,Dictionary<String,AnyObject>> = Dictionary<String,Dictionary<String,AnyObject>>()
                var dicDicRegister:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicDicRegister = Global.sharedInstance.currentUser.getDic()
                dicRegister["objUser"] = dicDicRegister
                
                self.generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                }
                else
                {
                    api.sharedInstance.RegisterUser(dicRegister, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        //                                2do לבטל הסלשה-מובטל רק כדי שלא יחסם הflow
                        ////                                if responseObject["Result"] as! Int != 0
                        /// /                               {
                        
                        var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        
                        dicUserId["currentUserId"] = responseObject["Result"] as! Int
                        //שמירת הuserId- במכשיר
                        Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                        
                        Global.sharedInstance.currentUser.iUserId = responseObject["Result"] as! Int
                        
                        if self.isProviderClickd == false
                        {
                            if Global.sharedInstance.isFromViewMode == true
                            {
                                self.openDetailsOrder(1)
                                Global.sharedInstance.isFromViewMode = false
                            }else
                            {
                                self.openCustomer()//פתיחת לקוח קיים
                            }
                        }
                        else
                        {
                            self.openProvider()
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            self.generic.hideNativeActivityIndicator(self)
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                    })
                }
            }
            else//הקוד שהכניס לא תואם למה שקיבל-לא תקין
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_VERIFICATION", comment: ""), vc: self)
                self.txtVerification.text = ""
                self.numTries+=1
                if self.numTries == 3
                {
                    self.numTries = 0
                    exit(0)
                }
            }
            
        }
        self.view.endEditing(true)//הורדת המקלדת
    }
    var attrs = [
        NSFontAttributeName : UIFont(name: "OpenSansHebrew-Bold", size: 14)!,
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSUnderlineStyleAttributeName : 1]
    var attributedStringsUserExist = NSMutableAttributedString(string:"")

    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notGetVer.text = NSLocalizedString("NOT_GET_VERIF", comment: "")
        enterVerification.text = NSLocalizedString("ENTER_VERIFICATION", comment: "")
        btnSendMessage.setTitle(NSLocalizedString("CONTINUE", comment: ""), forState: .Normal)
        btnSendAgain.setTitle(NSLocalizedString("SEND_AGAIN", comment: ""), forState: .Normal)
        
        supplierStoryBoard = UIStoryboard(name: "SupplierExist", bundle: nil)
        storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        lblTitleMessageVer.text = NSLocalizedString("MESSAGE_VERIFICATION", comment: "")
        btnSendMessage.setTitle(NSLocalizedString("CONTINUE", comment: ""), forState: .Normal)
        if isProviderClickd == true
        {
            lblTitleMessageVer.textColor = Colors.sharedInstance.color4
            btnSendMessage.backgroundColor = Colors.sharedInstance.color4
        }
        else
        {
            lblTitleMessageVer.textColor = Colors.sharedInstance.color3
            btnSendMessage.backgroundColor = Colors.sharedInstance.color3
        }
        let buttonTitleStr = NSMutableAttributedString(string:NSLocalizedString("SEND_AGAIN", comment: ""), attributes:attrs)
        attributedStringsUserExist.appendAttributedString(buttonTitleStr)


  btnSendAgain.setAttributedTitle(attributedStringsUserExist, forState: .Normal)
        btnSendAgain.contentHorizontalAlignment = .Right
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MessageVerificationViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    
    func openCustomer()
    {
        //קבלת פרטי הלקוח
        var dicGetCustomerDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicGetCustomerDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.GetCustomerDetails(dicGetCustomerDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                {
                    
                    var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicForDefault["nvClientName"] = responseObject["Result"]!["nvFirstName"]
                    //שמירת שם הלקוח במכשיר
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "currentClintName")
                    
                    var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicUserId["currentUserId"] = responseObject["Result"]!["iUserId"]
                    //שמירת הuserId- במכשיר
                    Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                    
                    Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    //בגלל שנשמר יום אחד קודם צריך להוסיף 3 שעות(זה לא עזר להוסיף timeZone)
                    Global.sharedInstance.currentUser.dBirthdate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dBirthdate
                        , options: [])!
                    Global.sharedInstance.currentUser.dMarriageDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dMarriageDate
                        , options: [])!
                    
                    self.openCustomerExist()
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
        }
    }
    
    //מעבר ללקוח קיים
    func openCustomerExist()
    {
        Global.sharedInstance.isProvider = false
        self.dismissViewControllerAnimated(false, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        let rgister:entranceCustomerViewController = self.storyboard!.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
        rgister.modalPresentationStyle = UIModalPresentationStyle.Custom
        frontViewController.pushViewController(rgister, animated: false)
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontViewController
        mainRevealController.rearViewController = rearViewController
        
        self.window.rootViewController = mainRevealController
        self.view.window?.makeKeyAndVisible()
    }
    
    func openProvider()
    {
        var dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.getProviderAllDetails(dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1
                {
                    Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_SERVER", comment: ""), vc: self)
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//ספק לא קיים
                {
                    self.dismissViewControllerAnimated(false, completion: nil)
                    //פתיחת רישום ספק
                    self.delegate.openBuisnessDetails()
                }
                else
                {
                    Global.sharedInstance.isProvider = true
                    
                    Global.sharedInstance.currentProviderDetailsObj = Global.sharedInstance.currentProviderDetailsObj.dicToProviderDetailsObj(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    
                    //שמירת שם הספק במכשיר
                    var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicForDefault["nvSupplierName"] = Global.sharedInstance.currentProviderDetailsObj.providerBuisnessDetailsObj.nvSupplierName
                    
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "supplierNameRegistered")
                    //מעבר לספק קיים
                    let frontviewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                    
                    let storyboardSupplierExist = UIStoryboard(name: "SupplierExist", bundle: nil)
                    
                    let vc = storyboardSupplierExist.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
                    
                    frontviewcontroller?.pushViewController(vc, animated: false)
                    
                    
                    //initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    

                    self.window.rootViewController = mainRevealController
                }
                self.generic.hideNativeActivityIndicator(self)
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
        }
    }
    
    //say which calendar open
    func openCustomerOrProvider()
    {
        //קבלת פרטי הלקוח
        var dicGetCustomerDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicGetCustomerDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.GetCustomerDetails(dicGetCustomerDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                {
                    var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicForDefault["nvClientName"] = responseObject["Result"]!["nvFirstName"]
                    //שמירת שם הלקוח במכשיר
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "currentClintName")
                    
                    var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicUserId["currentUserId"] = responseObject["Result"]!["iUserId"]
                    //שמירת הuserId- במכשיר
                    Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                    
                    Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    //בגלל שנשמר יום אחד קודם צריך להוסיף 3 שעות(זה לא עזר להוסיף timeZone)
                    Global.sharedInstance.currentUser.dBirthdate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dBirthdate
                        , options: [])!
                    Global.sharedInstance.currentUser.dMarriageDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dMarriageDate
                        , options: [])!
                    
                    //קבלת פרטי הספק כדי לבדוק האם הוא ספק וא״כ יש להעביר ליומן ספק,אחרת מעבירים ללקוח קיים
                    self.getProviderAllDetails(Global.sharedInstance.currentUser.iUserId)
                    
                    
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
        }
    }
    
    //פתיחת רישום ספק
    func openBuisnessDetails(){
        
        Global.sharedInstance.isProvider = true
        
        let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        
        viewCon.delegateFirstSection = viewCon1
        viewCon.delegateSecond1Section = viewCon1
        
        self.navigationController?.pushViewController(viewCon, animated: false)
    }
    
    //מחזיר את פרטי הספק,אם הוא ספק מעביר ליומן ספק ,אחרת ללקוח קיים
    func getProviderAllDetails(iUserId:Int)
    {
        var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicUserId["iUserId"] = iUserId
        
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            //קבלת פרטי הספק
            api.sharedInstance.getProviderAllDetails(dicUserId, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1//שגיאה
                {
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//ספק לא קיים
                {
                    NSUserDefaults.standardUserDefaults().removeObjectForKey("supplierNameRegistered")
                    
                    self.openCustomerExist()
                }
                    
                else
                {
                    if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                    {
                        Global.sharedInstance.currentProviderDetailsObj = Global.sharedInstance.currentProviderDetailsObj.dicToProviderDetailsObj(responseObject["Result"] as! Dictionary<String,AnyObject>)
                        
                        //שמירת שם הספק במכשיר
                        var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        
                        dicForDefault["nvSupplierName"] = Global.sharedInstance.currentProviderDetailsObj.providerBuisnessDetailsObj.nvSupplierName
                        
                        Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "supplierNameRegistered")
                        
                        Global.sharedInstance.isProvider = true
                        //מעבר ליומן ספק קיים
                        
                        let frontviewcontroller = self.storyboardMain!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                        let vc = self.supplierStoryBoard?.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
                        frontviewcontroller?.pushViewController(vc, animated: false)
                        
                        //initialize REAR View Controller- it is the LEFT hand menu.
                        
                        let rearViewController = self.storyboardMain!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                        
                        let mainRevealController = SWRevealViewController()
                        
                        mainRevealController.frontViewController = frontviewcontroller
                        mainRevealController.rearViewController = rearViewController
                        
                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                        window.rootViewController = mainRevealController
                        
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
        }
    }
    
    //פונקציה זו מופעלת כאשר הגיע לרישום מהזמנת תור במצב צפיה - לאחר הרישום מתבצעת ההזמנה
    func openDetailsOrder(tag:Int)  {
        
        let storyboard = UIStoryboard(name: "ClientExist", bundle: nil)

        let frontviewcontroller = storyboardMain!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyboard.instantiateViewControllerWithIdentifier("detailsAppointmetClientViewController") as! detailsAppointmetClientViewController
        vc.tag = tag
        vc.fromViewMode = true
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        let rearViewController = storyboardMain!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
        
    }
    
    //עדכון הפרטים בשרת
    func UpdateUser()
    {
        var dicDicRegister:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        var dicRegister:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicDicRegister = currentUserToEdit!.getDicToEdit()
        dicRegister["objUser"] = dicDicRegister
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.UpdateUser(dicRegister, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1
                {
                    Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_SAVECHANGES", comment: ""),vc: self)
                }
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    Alert.sharedInstance.showAlert(NSLocalizedString("SUCCESS_SAVECHANGES", comment: ""), vc: self)
                    self.numTries = 0
                    //שמירת הטלפון והקוד ב-default
                    var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    dicForDefault["verification"] = responseObject["Result"]
                    dicForDefault["phone"] = self.phone
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "verificationPhone")
                    //חזרה להגדרות
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let frontviewcontroller:UINavigationController = UINavigationController()
                    
                    let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
                    let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
                    
                    frontviewcontroller.pushViewController(vc, animated: false)
                    
                    let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                }
                
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }
}


