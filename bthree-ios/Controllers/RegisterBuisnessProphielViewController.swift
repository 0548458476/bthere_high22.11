//
//  RegisterBuisnessProphielViewController.swift
//  bthree-ios
//
//  Created by User on 29.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import AssetsLibrary
import CoreLocation
import CoreMotion
import Foundation
import MapKit

//דף 5 בהרשמה - פרופיל עסקי
class RegisterBuisnessProphielViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,
GMSMapViewDelegate,ImageCropViewControllerDelegate,WDImagePickerDelegate,NSURLConnectionDelegate,UITextViewDelegate {
    
    //MARK: - Properties
    //===========Properties=========
    
    
    var topSaleOpen:NSLayoutConstraint!
    var topSaleClose:NSLayoutConstraint!

    var latitude:CLLocationDegrees = 0
    var longitude:CLLocationDegrees = 0
    var isOnChosenAddress = true
    
    var flagFirst = false
    
    var flagImage = 0
    var scrollAbout:CGFloat = 0
    var scrollCampain:CGFloat = 0
    
    var longMessage:String = ""
    
    var tapGestureRecognizerForAlbom = UIGestureRecognizer()//logo
    var tapGestureRecognizerForCamera = UITapGestureRecognizer()//logo
    
    var tapGestureRecognizerForAlbom_3 = UIGestureRecognizer()//viewLightBlue3
    var tapGestureRecognizerForCamera_3 = UITapGestureRecognizer()//viewLightBlue3
    
    var tapGestureRecognizerForAlbom_campain = UIGestureRecognizer()//view_campain
    var tapGestureRecognizerForCamera_campain = UITapGestureRecognizer()//view_campain
    
    var tapGestureRecognizerForAlbom_Bottom = UIGestureRecognizer()
    var tapGestureRecognizerForCamera_Bottom = UITapGestureRecognizer()
    
    var tapGestureRecognizer2 = UITapGestureRecognizer()
    
    var changeImage:String = ""
    
    private var popoverController: UIPopoverController!
    private var iimagePicker: WDImagePicker!
    private var imagePickerWD: WDImagePicker!

    var imagePicker: UIImagePickerController!
    
    //MARK: - Outlet
    //==========outlet=========
    
    @IBOutlet weak var addImage5: UIImageView!
    @IBOutlet weak var addImage8: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //////----top view
    @IBOutlet weak var viewTop_Orange_1: UIView!
    @IBOutlet weak var lblNmeBuisness: UILabel!
    @IBOutlet weak var lblSlogenBuisness: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewOfLogo: UIView!
    
    ////view 2----------
    @IBOutlet weak var view_Black_2: UIView!
    
    //עפולה, 2 ק״מ ממיקומך
    @IBOutlet weak var imgWaze: UIImageView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblNumKM: UILabel!
    @IBOutlet weak var lblKMFromYou: UILabel!
    
    // דירוג: 8.9 | 32 מדרגים
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblNumRuting: UILabel!
    @IBOutlet weak var lblNumVoters: UILabel!
    @IBOutlet weak var lblVoters: UILabel!
    
    ////view 3----------
    
    @IBOutlet weak var addImage3: UIImageView!
    @IBOutlet weak var imgLightBlue3: UIImageView!
    @IBOutlet weak var view_lightBlue_3: UIView!
    @IBOutlet weak var lblHoursWork: UILabel!
    @IBOutlet weak var txtViewHoursWork: UITextView!
    @IBAction func btnLikeF(sender: AnyObject) {}
    @IBOutlet weak var btnLikeF: UIButton!
    @IBOutlet weak var btnShareF: UIButton!
    @IBAction func btnShareF(sender: AnyObject) {}
    @IBOutlet weak var btnInviteTor: UIButton!
    @IBAction func btnInviteTor(sender: AnyObject) {}
    
    //-----googleMaps----------
    @IBOutlet weak var viewParent_Map: UIView!
    @IBOutlet weak var viewMap: GMSMapView!

    @IBOutlet weak var addressLabel: UILabel!
    
    //About
    @IBOutlet weak var viewAbout_4: UIView!
    @IBOutlet weak var lblAbout: UILabel!
    @IBAction func btnReadMore(sender: AnyObject) {
        Alert.sharedInstance.showAlert(longMessage, vc: self)
    }
    
    @IBOutlet weak var btnReadMore: UIButton!
    
    // to show the text
    @IBOutlet weak var txtViewAbout: UITextView!
    @IBOutlet weak var view_lightBlue_5: UIView!    
    @IBOutlet weak var imgLightBlue5: UIImageView!
    @IBOutlet weak var lblUploadCampaign: UILabel!
    @IBOutlet weak var topToSale: NSLayoutConstraint!
    @IBOutlet weak var viewShowSale: UIView!
    @IBOutlet weak var viewUnderSale: UIView!
    @IBOutlet weak var constHeightSale: NSLayoutConstraint!
    
    //show sale image or not
    @IBAction func btnOpenSale(sender: AnyObject)
    {
        showHideSale()
    }
    
    //Opinion
    @IBOutlet weak var view_Opinion_6: UIView!
    @IBOutlet weak var lblOpinion: UILabel!
    @IBOutlet weak var btnMakeTor: UIButton!
    @IBAction func btnMakeTor(sender: AnyObject) {}
    @IBOutlet weak var view_thank_7: UIButton!
    @IBOutlet weak var view_lightBlue8: UIView!
    @IBOutlet weak var imgLightBlue8: UIImageView!
    @IBOutlet weak var viewBottom_9: UIView!
    
    //------ ViewToHide : Logo ----
    
    @IBOutlet weak var viewLogo: UIView!
    @IBOutlet weak var lblLogo: UILabel!
    @IBOutlet weak var lblCameraLogo: UILabel!
    @IBOutlet weak var lblAddFromAlbum: UILabel!
    @IBOutlet weak var imgFromAlbom: UIImageView!
    @IBOutlet weak var imgFromCamera: UIImageView!
    @IBOutlet weak var viewCameraLogo: UIView!
    @IBOutlet weak var viewAlbumLogo: UIView!
    
    //on click X to close popUp of logo
    @IBAction func btnCloseViewLogo(sender: AnyObject) {
        viewLogo.hidden = true
        viewTriangular.hidden = true
        //show popUp of slogen
        selectSlogen(imgPicture)
    }
    
    @IBOutlet weak var viewTriangular: TriangeView!
   
    //---- ViewToHide : viewSlogen ----
    
    @IBOutlet weak var triangleSlogen: TriangeView!
    @IBOutlet weak var viewSlogen: UIView!
    @IBOutlet weak var btnCloseSlogen: UIButton!
    
        //on click X to close popUp of slogen
    @IBAction func btnCloseSlogen(sender: AnyObject)
    {
        viewSlogen.hidden = true
        triangleSlogen.hidden = true
        dismissKeyboard()
        //show popUp of the business image
        selectPicture(imgPicture)
        
    }
    @IBOutlet weak var lblTitleSlogen: UILabel!
    @IBOutlet weak var txtViewSlogen: UITextView!
    @IBOutlet weak var btnSaveSlogen: UIButton!
    
        //on click to save the slogen
    @IBAction func btnSaveSlogen(sender: AnyObject) {
        viewSlogen.hidden = true
        triangleSlogen.hidden = true
        
        let arr:Array<String> = txtViewSlogen.text.componentsSeparatedByString("\n")
        if arr.count == 1
        {
           lblSlogenBuisness.text = txtViewSlogen.text
        }
        else
        {
            for row in arr {
                lblSlogenBuisness.text! += " " + (row)
            }
        }
        Global.sharedInstance.addProviderBuisnessProfile.nvSlogen = lblSlogenBuisness.text!

        dismissKeyboard()
        //show popUp of the business image
        selectPicture(imgPicture)
    }

    //------ ViewToHide : forViewLightBlue 3 ----
    
    @IBOutlet weak var viewAddPictureToView3: UIView!
    @IBOutlet weak var lblTakePicture3: UILabel!
    @IBOutlet weak var lblPicture: UILabel!
    @IBOutlet weak var lblAddPicture: UILabel!
    
    //on click X to close popUp of business image
    @IBAction func btnCloseView(sender: AnyObject) {
        
        self.scrollView.setContentOffset(CGPointMake(0, -self.scrollView.contentInset.top), animated: false)
        viewAddPictureToView3.hidden = true
        viewTriangular_3.hidden = true
        
        //show popUp of about the business
        selectAbout(imgPicture)
    }
    
    @IBOutlet weak var imgAddFromAlbom: UIImageView!
    @IBOutlet weak var imgAddFromCamera_for3: UIImageView!
    @IBOutlet weak var viewAlbum3: UIView!
    @IBOutlet weak var viewCamera3: UIView!
    @IBOutlet weak var viewTriangular_3: TriangeView!

    //------------------------------------------
    
    //------ ViewToHide : for about ----
    
    @IBOutlet weak var btnCloseAbout: UIButton!
    
    //on click X to close popUp of about the business
    @IBAction func btnCloseAbout(sender: AnyObject) {
        
        let topOffset: CGPoint = CGPointMake(0, scrollAbout)
        self.scrollView.setContentOffset(topOffset, animated: false)
        viewHideAbout.hidden = true
        viewTriangular_about.hidden = true
        
        if Global.sharedInstance.isOpenSale == true
        {
            //show popUp of sale
            selectCampain(imgCamera)
        }
        else
        {
            //show popUp of bottom image
            selectBottom(imgPicture)
        }

    }
    
    @IBOutlet weak var viewHideAbout: UIView!
    
    @IBOutlet weak var lblTitleAbout: UILabel!
    // to write the text - pop up
    @IBOutlet weak var textViewAbout: UITextView!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBAction func btnSave(sender: AnyObject) {
        
        let topOffset: CGPoint = CGPointMake(0, scrollAbout)
        self.scrollView.setContentOffset(topOffset, animated: false)

        //טיפול במקרה שהטקסט ב׳אודות׳ ארוך
        //ירידת שורה, או הצגת חלק מהטקסט עם כפתור ׳קרא עוד׳
        longMessage = textViewAbout.text
        viewHideAbout.hidden = true
        txtViewAbout.text = textViewAbout.text
        
        let numRows = txtViewAbout.text.componentsSeparatedByString("\n").count
        var arrSpaces:Array<String> = []

         if numRows >= 2
        {
            let aboutArr = txtViewAbout.text.componentsSeparatedByString("\n")
            
            if aboutArr[0].characters.count > 53
            {
                if aboutArr[0].characters.count > 104
                {
                    let index = aboutArr[0].startIndex.advancedBy(104)
                    txtViewAbout.text = aboutArr[0].substringToIndex(index)
                    
                    arrSpaces = txtViewAbout.text.componentsSeparatedByString(" ")
                    
                    if textViewAbout.text.characters[textViewAbout.text.startIndex.advancedBy(104)] != " " && txtViewAbout.text.characters.last != " "
                    {
                        txtViewAbout.text = ""
                        for item in arrSpaces
                        {
                            if item != arrSpaces.last
                            {
                                txtViewAbout.text! += (item + " ")
                            }
                            else
                            {
                        txtViewAbout.text.substringToIndex(txtViewAbout.text.endIndex.predecessor())
                            }
                        }
                    }

                    btnReadMore.hidden = false

                }
                else
                {
                txtViewAbout.text = aboutArr[0]
                btnReadMore.hidden = false
                }
            }
            else if aboutArr[1].characters.count > 53
            {
                let index = aboutArr[1].startIndex.advancedBy(53)
                txtViewAbout.text = aboutArr[0] + "\n" + aboutArr[1].substringToIndex(index)
                
                arrSpaces = txtViewAbout.text.componentsSeparatedByString(" ")
                
                if aboutArr[1].characters[aboutArr[1].startIndex.advancedBy(53)] != " " && txtViewAbout.text.characters.last != " "
                {
                    txtViewAbout.text = ""
                    for item in arrSpaces
                    {
                        if item != arrSpaces.last
                        {
                            txtViewAbout.text! += (item + " ")
                        }
                        else
                        {
                        txtViewAbout.text.substringToIndex(txtViewAbout.text.endIndex.predecessor())
                        }
                    }
                }

                btnReadMore.hidden = false
            }
            else if numRows > 2
            {
            txtViewAbout.text = aboutArr[0] + "\n" + aboutArr[1]
            btnReadMore.hidden = false
            }
        }

        else if txtViewAbout.text.characters.count > 104
        {
            let index = txtViewAbout.text.startIndex.advancedBy(104)
            txtViewAbout.text = (txtViewAbout.text.substringToIndex(index))
            
            arrSpaces = txtViewAbout.text.componentsSeparatedByString(" ")

            if textViewAbout.text.characters[textViewAbout.text.startIndex.advancedBy(104)] != " " && txtViewAbout.text.characters.last != " "
            {
                txtViewAbout.text = ""
                for item in arrSpaces
                {
                    if item != arrSpaces.last
                    {
                        txtViewAbout.text! += (item + " ")
                    }
                    else
                    {
                        txtViewAbout.text.substringToIndex(txtViewAbout.text.endIndex.predecessor())
                    }
                }
            }
            btnReadMore.hidden = false
        }
        else
        {
            btnReadMore.hidden = true
        }
        
        viewTriangular_about.hidden = true
        
        Global.sharedInstance.addProviderBuisnessProfile.nvAboutComment = txtViewAbout.text
        
        
        if Global.sharedInstance.isOpenSale == true
        {
            //show popUp of sale
            selectCampain(imgCamera)
        }
        else
        {
            //show popUp of bottom image
            selectBottom(imgPicture)
        }
        
    }
    
    @IBOutlet weak var viewTriangular_about: TriangeView!
    
    //------------------------------------------
    
    //-----viewHide Campaign--------
    
    @IBOutlet weak var viewCampaign: UIView!
    
    @IBOutlet weak var lblTakePictureCamp: UILabel!
    
    @IBOutlet weak var lblTitleCampaign: UILabel!
    
    @IBOutlet weak var lblAddFromAlbumToCamp: UILabel!
    
    //on click X to close popUp of sale image
    @IBAction func btnClose(sender: AnyObject) {
        
        let topOffset1: CGPoint = CGPointMake(0, scrollCampain + 0.085 * scrollView.contentSize.height)
        self.scrollView.setContentOffset(topOffset1, animated: false)
        
        viewCampaign.hidden = true
        viewTriangular_campain.hidden = true
        
        //show popUp of bottom image
        selectBottom(imgPicture)
    }
    @IBOutlet weak var imgPicture: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var viewCameraCampain: UIView!
    @IBOutlet weak var viewAlbumCampain: UIView!
    @IBOutlet weak var viewTriangular_campain: TriangeView!
    
    //------------------------------------------
    
    //-----viewHide Bottom--------
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblTakePictureBottom: UILabel!
    @IBOutlet weak var lblFromAlbumToOttom: UILabel!
    @IBOutlet weak var lblTitlePicture: UILabel!
    
    //on click X to close popUp of bottom image
    @IBAction func btnCloseBottom(sender: AnyObject) {
        
        viewBottom.hidden = true
        viewtriangular_bottom.hidden = true
    }
    
    @IBOutlet weak var imgAddAlbom: UIImageView!
    @IBOutlet weak var imgAddCamera: UIImageView!
    @IBOutlet weak var viewCameraBottom: UIView!
    @IBOutlet weak var viewAlbumBottom: UIView!
    @IBOutlet weak var viewtriangular_bottom: TriangeView!

    //MARK - Properties
    var locationManager = CLLocationManager()
    var key = ""
    
    //MARK: - Initial
    //===========Initial==============
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtViewSlogen.delegate = self
        key="AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg"
        lblSlogenBuisness.text = "   "
        imgLightBlue3.image = UIImage(named: "IMG_05072016_131013.png")
        imgLightBlue8.image = UIImage(named: "IMG_05072016_131024.png")
        btnReadMore.hidden = true
        self.viewLogo.hidden = false
        viewTriangular.hidden = false

        //set all the text's of the labels by Localizable
        lblVoters.text = NSLocalizedString("VOTERS", comment: "")
        lblRating.text = NSLocalizedString("LEVEL", comment: "")
        lblAbout.text = NSLocalizedString("ABOUT", comment: "")
        lblHoursWork.text = NSLocalizedString("HOURS_WORK", comment: "")
        btnInviteTor.setTitle(NSLocalizedString("FOR_TOR", comment: ""), forState: .Normal)
        lblTitleSlogen.text = NSLocalizedString("SLOGEN", comment: "")
        lblOpinion.text = NSLocalizedString("OPINION", comment: "")
        lblUploadCampaign.text = NSLocalizedString("UPLOAD_CAMPAIGN", comment: "")
        btnMakeTor.setTitle(NSLocalizedString("FOR_TOR", comment: ""), forState: .Normal)
        lblTakePicture3.text = NSLocalizedString("TAKE_PICTURE", comment: "")
        lblPicture.text = NSLocalizedString("PICTURE", comment: "")
        lblAddPicture.text = NSLocalizedString("ATTACH_PHOTO", comment: "")
        lblLogo.text = NSLocalizedString("LOGO", comment: "")
        lblCameraLogo.text = NSLocalizedString("TAKE_PICTURE", comment: "")
        lblAddFromAlbum.text = NSLocalizedString("ATTACH_PHOTO", comment: "")
        lblTitleAbout.text = NSLocalizedString("ABOUT_BUSINESS", comment: "")
        btnSave.setTitle(NSLocalizedString("SAVE_CONTINU", comment: ""), forState: .Normal)
        btnSaveSlogen.setTitle(NSLocalizedString("SAVE_CONTINU", comment: ""), forState: .Normal)
        lblTakePictureCamp.text = NSLocalizedString("TAKE_PICTURE", comment: "")
        lblTitleCampaign.text = NSLocalizedString("CAMPAIGN", comment: "")
        lblAddFromAlbumToCamp.text = NSLocalizedString("ATTACH_PHOTO", comment: "")
        lblTakePictureBottom.text = NSLocalizedString("TAKE_PICTURE", comment: "")
        lblTitlePicture.text = NSLocalizedString("PICTURE", comment: "")
        lblFromAlbumToOttom.text = NSLocalizedString("ATTACH_PHOTO", comment: "")
        view_thank_7.titleLabel?.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
        
  
        //אשמח לראותך משה - בינתיים הורדתי ע״פ הדרישה, אח״כ יהיה שם הלקוח...
        
//        let dicName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("currentClintName") as! Dictionary<String,String>
//        dicName["nvClientName"]
//        
//        if dicName["nvClientName"] != ""
//        {
//        view_thank_7.setTitle((NSLocalizedString("THANK", comment: "") + " - " + dicName["nvClientName"]!), forState: .Normal)
//        }
//        else
//        {
        view_thank_7.setTitle(NSLocalizedString("THANK", comment: ""), forState: .Normal)
//        }

        view_thank_7.enabled = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewAddPictureToView3.hidden = true
        viewHideAbout.hidden = true
        viewCampaign.hidden = true
        viewBottom.hidden = true
        viewSlogen.hidden = true
        
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_3.hidden = true
        viewTriangular_about.hidden = true
        viewTriangular_campain.hidden = true
        viewtriangular_bottom.hidden = true
        triangleSlogen.hidden = true
        
        tapOnViews()
        
        //show a map:
        viewMap.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

        imgLogo.image = UIImage(named: "clients@x1.png")
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.fetchPlaces()
        txtViewAbout.editable = false
        if flagFirst == false
        {
            flagFirst = true
            selectLogo(imgLogo)
        }
        txtViewHoursWork.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
        imgLogo.contentMode = .ScaleAspectFit
        imgLightBlue3.contentMode = .ScaleAspectFit
        imgLightBlue5.contentMode = .ScaleAspectFit
        imgLightBlue8.contentMode = .ScaleAspectFit

        addImage3.image = UIImage()
        addImage5.image = UIImage()
        addImage8.image = UIImage()

        viewMap.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        lblNmeBuisness.text = Global.sharedInstance.currentProvider.nvSupplierName
        if lblNmeBuisness.text?.characters.count > 24
        {
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
            {
                lblNmeBuisness.font = UIFont(name: "OpenSansHebrew-Bold", size: 13)
            }
            else
            {
                lblNmeBuisness.font = UIFont(name: "OpenSansHebrew-Bold", size: 14)
            }
        }

        //----------------
        
        lblCity.text = Global.sharedInstance.currentProvider.nvAddress

        txtViewHoursWork.text = "\(NSLocalizedString("HOURS_ACTIVITY", comment: "")):\(Global.sharedInstance.hourShow) \n \(Global.sharedInstance.hourShowRecess)"
    
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblCity.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
            lblRating.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
            lblVoters.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
//            lblNumKM.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
//            lblKMFromYou.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            btnSave.titleLabel?.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblUploadCampaign.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblOpinion.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblAbout.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblHoursWork.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblTakePicture3.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblCameraLogo.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblTakePictureCamp.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblTakePictureBottom.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblAddPicture.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblAddFromAlbum.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblAddFromAlbumToCamp.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
            lblFromAlbumToOttom.font = UIFont(name: "OpenSansHebrew-Regular", size: 15)
        }
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.contentSize.height = 1400
        scrollView.scrollEnabled = true
    }
    
    //MARK: - textView
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        var startString = ""
        if (textView.text != nil)
        {
            startString += textView.text!
        }
        startString += text
        
        //enable to enter only 120 characters in slogen
        if textView == txtViewSlogen
        {
            if startString.characters.count > 120
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY120_CHARACTERS", comment: ""), vc: self)
                return false
            }
            return true
        }
        return true
    }

    //MARK: - GoogleMaps
    //==================GoogleMaps===================
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 3
        if status == .AuthorizedWhenInUse {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            viewMap.myLocationEnabled = true
            viewMap.settings.myLocationButton = true

            viewMap.animateToZoom(18)
        }
    }
    

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {

              viewMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in

            if let address = response?.firstResult() {
                
                let lines = address.lines!// as! [String]
                self.addressLabel.text = lines.joinWithSeparator("\n")
                
                //*********
                let labelHeight = self.addressLabel.intrinsicContentSize().height
                self.viewMap.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,bottom: labelHeight, right: 0)
                
                UIView.animateWithDuration(0.25) {
                    self.view.layoutIfNeeded()
                }
            }
            else
            {
               self.googlePlaceGeocoded(self.latitude, longitude: self.longitude)
            }
        }
    }

    func didTapMyLocationButtonForMapView(mapView: GMSMapView) -> Bool {

                self.googlePlaceGeocoded(self.latitude, longitude: self.longitude)
                isOnChosenAddress = true
      return true
    }
    
    func fetchPlaces(){
        data = NSMutableData()  // Declare Globally
        let urlPath: String = googleURLString()
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        connectionAutocomplete = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connectionAutocomplete.start()
    }
    
    func googleURLString() ->String{
        var url:String = ""
        url = NSString(format:"https://maps.googleapis.com/maps/api/place/details/json?input=bar&placeid=%@&key=%@",Global.sharedInstance.placeIdForMap,key) as String
        return url
    }
    
    
    // MARK:NSURLConnectionDelegate
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.data?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!){
        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        do {
            let jsonResult: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
            
            if let result = jsonResult.objectForKey("result") as? Dictionary<String, AnyObject> {
                if let geometry = result["geometry"] as? Dictionary<String, AnyObject> {
                    if let location = geometry["location"] as? Dictionary<String, Double> {
                        if location["lat"] != nil
                        {
                            latitude = location["lat"]!
                            
                            if location["lng"] != nil
                            {
                                longitude = location["lng"]!
                                self.googlePlaceGeocoded(latitude, longitude: longitude)
                            }
                        }
                    }
                }
            }
            return
        }
        catch (_) {
            //here you can get access to all of the errors that occurred when trying to serialize
        }
    }
    
    
    
    func googlePlaceGeocoded(latitude:Double, longitude:Double)
    {
        viewMap.clear()
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        viewMap.selectedMarker = marker
        marker.map = viewMap
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 15)
        viewMap.camera = camera
        viewMap.animateToZoom(18)
        
    }

    
    
    //MARK: - Functions
    //==========Functions=================
    
    //add taps to the views to show popUps on tap them
    func tapOnViews()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(RegisterBuisnessProphielViewController.dismissKeyboard))
        tap.delegate=self
        self.view.addGestureRecognizer(tap)
        
        ///----------when tap on logo view
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectLogo(_:)))
        imgLogo.userInteractionEnabled = true
        imgLogo.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerSlogen = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectSlogen(_:)))
        lblSlogenBuisness.userInteractionEnabled = true
        lblSlogenBuisness.addGestureRecognizer(tapGestureRecognizerSlogen)
        
        tapGestureRecognizerForAlbom = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openAlbom(_:)))
        viewAlbumLogo.userInteractionEnabled = true
        viewAlbumLogo.addGestureRecognizer(tapGestureRecognizerForAlbom)
        
        tapGestureRecognizerForCamera = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openCamera(_:)))
        viewCameraLogo.userInteractionEnabled = true
        viewCameraLogo.addGestureRecognizer(tapGestureRecognizerForCamera)
        ///---------------------------------
        
        ///------when tap on view 3(the first view light  blue)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectPicture(_:)))
        view_lightBlue_3.userInteractionEnabled = true
        view_lightBlue_3.addGestureRecognizer(tapGestureRecognizer1)
        
        tapGestureRecognizerForAlbom_3 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openAlbom(_:)))
    viewAlbum3.userInteractionEnabled = true
        viewAlbum3.addGestureRecognizer(tapGestureRecognizerForAlbom_3)
        
        tapGestureRecognizerForCamera_3 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openCamera(_:)))
        viewCamera3.userInteractionEnabled = true
        viewCamera3.addGestureRecognizer(tapGestureRecognizerForCamera_3)
        //-------------------------------------------
        
        ///------when tap on view about
        tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectAbout(_:)))
       // tapGestureRecognizer2.delegate = self
        viewAbout_4.userInteractionEnabled = true
        viewAbout_4.addGestureRecognizer(tapGestureRecognizer2)
        //-------------------------------------------
        
        ///------when tap on view campain
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectCampain(_:)))
        view_lightBlue_5.userInteractionEnabled = true
        view_lightBlue_5.addGestureRecognizer(tapGestureRecognizer3)
        
        tapGestureRecognizerForAlbom_campain = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openAlbom(_:)))
        viewAlbumCampain.userInteractionEnabled = true
        viewAlbumCampain.addGestureRecognizer(tapGestureRecognizerForAlbom_campain)
        
        tapGestureRecognizerForCamera_campain = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openCamera(_:)))
        viewCameraCampain.userInteractionEnabled = true
        viewCameraCampain.addGestureRecognizer(tapGestureRecognizerForCamera_campain)
        //-------------------------------------------
        
        ///------when tap on the bottom view
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.selectBottom(_:)))
        view_lightBlue8.userInteractionEnabled = true
        view_lightBlue8.addGestureRecognizer(tapGestureRecognizer4)
        
        tapGestureRecognizerForAlbom_Bottom = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openAlbom(_:)))
        viewAlbumBottom.userInteractionEnabled = true
        viewAlbumBottom.addGestureRecognizer(tapGestureRecognizerForAlbom_Bottom)
        
        tapGestureRecognizerForCamera_Bottom = UITapGestureRecognizer(target:self, action:#selector(RegisterBuisnessProphielViewController.openCamera(_:)))
        viewCameraBottom.userInteractionEnabled = true
        viewCameraBottom.addGestureRecognizer(tapGestureRecognizerForCamera_Bottom)
        //-------------------------------------------
    }
    
    // MARK: - Select Image
    //===================Select Image=================
    
    //open the library to choose image
    func openAlbom(img: AnyObject)
    {
        
        if img as! UITapGestureRecognizer == tapGestureRecognizerForAlbom
        {
            changeImage = "logo"
        }
        else if img as! UITapGestureRecognizer == tapGestureRecognizerForAlbom_3
        {
            changeImage = "ligtyBlue3"
        }
        else if img as! UITapGestureRecognizer == tapGestureRecognizerForAlbom_campain
        {
            changeImage = "campain"
            
        }
        else if img as! UITapGestureRecognizer == tapGestureRecognizerForAlbom_Bottom
        {
            changeImage = "bottom"
        }
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.ActionSheet)
        
        /// to open images file
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("LOOK", comment: ""), style:UIAlertActionStyle.Default, handler:{ (UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                self.showResizablePicker(self.imgLogo)
                
            }
            }
            
            ))
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("CANCEL", comment: ""), style:UIAlertActionStyle.Cancel, handler:nil))
        self.presentViewController(settingsActionSheet, animated:true, completion:nil)
        
    }
    
    //MARK:-------cropImage
    func showResizablePicker(button: UIView) {
        self.imagePickerWD = WDImagePicker()
        self.imagePickerWD.cropSize = CGSizeMake(280, 280)
        self.imagePickerWD.delegate = self
        self.imagePickerWD.resizableCropArea = true
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController = UIPopoverController(contentViewController: self.imagePickerWD.imagePickerController)
            self.popoverController.presentPopoverFromRect(button.frame, inView: self.view, permittedArrowDirections: .Any, animated: true)
        } else {
            
            Global.sharedInstance.rgisterModelViewController!.presentViewController(self.imagePickerWD.imagePickerController, animated: true, completion: nil)
            
        }
    }
    
    //save the chosen picture - album
    func imagePicker(imagePicker: WDImagePicker, pickedImage: UIImage) {
        
        let img:UIImage = Global.sharedInstance.resizeImage(pickedImage, newWidth: pickedImage.size.width*0.5)
        let base64String = Global.sharedInstance.setImageToString(img)
        
        
        if changeImage=="ligtyBlue3"
        {
            UIGraphicsBeginImageContext(view_lightBlue_3.frame.size)
            pickedImage.drawInRect(view_lightBlue_3.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            addImage3.hidden = true
            imgLightBlue3.image = image
            
            Global.sharedInstance.addProviderBuisnessProfile.nvHeaderImage = base64String
            
            viewAddPictureToView3.hidden=true
            viewTriangular_3.hidden = true
            
            self.scrollView.setContentOffset(CGPointMake(0, -self.scrollView.contentInset.top), animated: false)
            selectAbout(imgCamera)
        }
        else if changeImage=="logo"
        {
            imgLogo.contentMode = UIViewContentMode.ScaleAspectFill
            imgLogo.clipsToBounds = true
            
            
            imgLogo.backgroundColor = UIColor.clearColor()
            viewOfLogo.backgroundColor = UIColor.clearColor()
            imgLogo.image = pickedImage
            viewLogo.hidden=true
            viewTriangular.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvILogoImage = base64String
            
            selectSlogen(imgPicture)
        }
        else if changeImage=="campain"
        {
            UIGraphicsBeginImageContext(view_lightBlue_5.frame.size)
            pickedImage.drawInRect(view_lightBlue_5.bounds)
            let image1: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            addImage5.hidden = true
            imgLightBlue5.image = image1
            
            viewCampaign.hidden = true
            viewTriangular_campain.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvCampaignImage = base64String
            
            let topOffset1: CGPoint = CGPointMake(0, scrollCampain + 0.085 * scrollView.contentSize.height)
            self.scrollView.setContentOffset(topOffset1, animated: false)
            selectBottom(imgCamera)
        }
        else if changeImage=="bottom"
        {
            UIGraphicsBeginImageContext(view_lightBlue8.frame.size)
            pickedImage.drawInRect(view_lightBlue8.bounds)
            let image1: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            addImage8.hidden = true
            imgLightBlue8.image = image1
            viewBottom.hidden = true
            viewtriangular_bottom.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvFooterImage = base64String
        }

        self.hideImagePicker()
    }
    
    func hideImagePicker() {
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController.dismissPopoverAnimated(true)
        } else {
            self.imagePickerWD.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    //open the camera
    func openCamera(img: AnyObject)
    {
        if img as! UITapGestureRecognizer  == tapGestureRecognizerForCamera
        {
            changeImage="logo"
        }
        else if img as! UITapGestureRecognizer  == tapGestureRecognizerForCamera_3
        {
            changeImage="ligtyBlue3"
        }
        else if img as! UITapGestureRecognizer  == tapGestureRecognizerForCamera_campain
        {
            changeImage="campain"
        }
            
        else if img as! UITapGestureRecognizer  == tapGestureRecognizerForCamera_Bottom
        {
            changeImage="bottom"
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
            
        else {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //save the chosen image - camera
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        let img:UIImage = Global.sharedInstance.resizeImage(image, newWidth: image.size.width*0.5)
        let base64String = Global.sharedInstance.setImageToString(img)
        
        //------------------------
        
        if changeImage=="ligtyBlue3"
        {
            UIGraphicsBeginImageContext(view_lightBlue_3.frame.size)
            image.drawInRect(view_lightBlue_3.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            addImage3.hidden = true
            imgLightBlue3.image = image
            
            Global.sharedInstance.addProviderBuisnessProfile.nvHeaderImage = base64String
            
            viewAddPictureToView3.hidden=true
            viewTriangular_3.hidden = true
            
            self.scrollView.setContentOffset(CGPointMake(0, -self.scrollView.contentInset.top), animated: false)
            selectAbout(imgCamera)
        }
        else if changeImage=="logo"
        {
            imgLogo.contentMode = UIViewContentMode.ScaleAspectFill
            imgLogo.clipsToBounds = true
            imgLogo.backgroundColor = UIColor.clearColor()
            viewOfLogo.backgroundColor = UIColor.clearColor()
            imgLogo.image = image
            viewLogo.hidden=true
            viewTriangular.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvILogoImage = base64String
            
            selectSlogen(imgPicture)
        }
        else if changeImage=="campain"
        {
            UIGraphicsBeginImageContext(view_lightBlue_5.frame.size)
            image?.drawInRect(view_lightBlue_5.bounds)
            let image1: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            addImage5.hidden = true
            imgLightBlue5.image = image1
            
            viewCampaign.hidden = true
            viewTriangular_campain.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvCampaignImage = base64String
            
            let topOffset1: CGPoint = CGPointMake(0, scrollCampain + 0.085 * scrollView.contentSize.height)
            self.scrollView.setContentOffset(topOffset1, animated: false)
            selectBottom(imgCamera)
        }
        else if changeImage=="bottom"
        {
            UIGraphicsBeginImageContext(view_lightBlue8.frame.size)
            image?.drawInRect(view_lightBlue8.bounds)
            let image1: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            addImage8.hidden = true
            imgLightBlue8.image = image1
            viewBottom.hidden = true
            viewtriangular_bottom.hidden = true
            
            Global.sharedInstance.addProviderBuisnessProfile.nvFooterImage = base64String
        }

        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - pop up
    //================pop up================
    
    //show popUp of logo
    func selectLogo(img: AnyObject)
    {
        flagImage = 1
        self.viewLogo.hidden = false
        viewTriangular.hidden = false
        
        ///hide all the views that on the base view
        viewAddPictureToView3.hidden = true
        viewHideAbout.hidden = true
        viewCampaign.hidden = true
        viewBottom.hidden = true
        viewSlogen.hidden = true
        //hide all the triangulars views
        viewTriangular_3.hidden = true
        viewTriangular_about.hidden = true
        viewTriangular_campain.hidden = true
        viewtriangular_bottom.hidden = true
        triangleSlogen.hidden = true
    }
    
    //show popUp of slogen
    func selectSlogen(img: AnyObject) {
        viewSlogen.hidden = false
        triangleSlogen.hidden = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewAddPictureToView3.hidden = true
        viewCampaign.hidden = true
        viewBottom.hidden = true
        self.viewHideAbout.hidden = true
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_3.hidden = true
        viewTriangular_campain.hidden = true
        viewtriangular_bottom.hidden = true
        viewTriangular_about.hidden = true
    }
    
    //show popUp of business image
    func selectPicture(img: AnyObject)
    {
        flagImage = 2
        let bottomOffset: CGPoint = CGPointMake(0, self.scrollView.contentSize.height - scrollView.contentSize.height * 0.92)
        self.scrollView.setContentOffset(bottomOffset, animated: true)
        
        self.viewAddPictureToView3.hidden = false
        viewTriangular_3.hidden = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewHideAbout.hidden = true
        viewCampaign.hidden = true
        viewBottom.hidden = true
        viewSlogen.hidden = true
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_about.hidden = true
        viewTriangular_campain.hidden = true
        viewtriangular_bottom.hidden = true
        triangleSlogen.hidden = true
    }
    
    //show popUp of about
    func selectAbout(img: AnyObject)
    {
        flagImage = 3
        scrollAbout = self.scrollView.bounds.size.height
        let bottomOffset: CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height - scrollView.contentSize.height * 0.178)
        self.scrollView.setContentOffset(bottomOffset, animated: true)
        
        self.btnCloseAbout.layer.zPosition = 1
        self.viewHideAbout.hidden = false
        viewTriangular_about.hidden = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewAddPictureToView3.hidden = true
        viewCampaign.hidden = true
        viewBottom.hidden = true
        viewSlogen.hidden = true
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_3.hidden = true
        viewTriangular_campain.hidden = true
        viewtriangular_bottom.hidden = true
        triangleSlogen.hidden = true
    }
    
    //show popUp of campaign
    func selectCampain(img: AnyObject)
    {
        flagImage = 4
        scrollCampain = self.scrollView.bounds.size.height
        let bottomOffset: CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height - scrollView.contentSize.height * 0.1)
        self.scrollView.setContentOffset(bottomOffset, animated: true)
        
        self.viewCampaign.hidden = false
        viewTriangular_campain.hidden = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewAddPictureToView3.hidden = true
        viewHideAbout.hidden = true
        viewBottom.hidden = true
        viewSlogen.hidden = true
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_3.hidden = true
        viewTriangular_about.hidden = true
        viewtriangular_bottom.hidden = true
        triangleSlogen.hidden = true
    }
    
    //show popUp of bottom image
    func selectBottom(img: AnyObject)
    {
        flagImage = 5
        let bottomOffset: CGPoint = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
        self.scrollView.setContentOffset(bottomOffset, animated: true)
        self.viewBottom.hidden = false
        viewtriangular_bottom.hidden = false
        
        ///hide all the views that on the base view
        viewLogo.hidden = true
        viewAddPictureToView3.hidden = true
        viewHideAbout.hidden = true
        viewCampaign.hidden = true
        viewSlogen.hidden = true
        //hide all the triangulars views
        viewTriangular.hidden = true
        viewTriangular_3.hidden = true
        viewTriangular_about.hidden = true
        viewTriangular_campain.hidden = true
        triangleSlogen.hidden = true
    }
    
    // MARK: - Key board
    //================Key Board=============
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        self.dismissKeyboard()
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cutHour(hour:String) -> String {
        var fixedHour = String(hour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        return fixedHour
    }
    
    func cropImage(image: UIImage) {
        let controller: ImageCropViewController = ImageCropViewController(image: image)
        controller.delegate = self
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    func ImageCropViewControllerSuccess(controller: UIViewController!, didFinishCroppingImage croppedImage: UIImage!)
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func ImageCropViewControllerDidCancel(controller: UIViewController!)
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = image
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage!, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }

    
    var data: NSMutableData?
    var connectionAutocomplete = NSURLConnection()
    
    
    
    //
    func showHideSale()
    {
        //if the sale image is shown - hide it
        if Global.sharedInstance.isOpenSale == true
        {
            Global.sharedInstance.isOpenSale = false
            viewShowSale.backgroundColor = Colors.sharedInstance.color9
            topSaleOpen = topToSale
            self.view.removeConstraint(topSaleOpen)
            let constraint = NSLayoutConstraint(item: viewUnderSale, attribute: .Top, relatedBy: .Equal, toItem: viewShowSale, attribute: .Top, multiplier: 1, constant: viewShowSale.frame.size.height)
            topSaleClose = constraint
            self.view.addConstraint(constraint)
            scrollView.contentSize.height = 1450 - view_lightBlue_5.frame.size.height
            
            if self.viewCampaign.hidden == false && viewTriangular_campain.hidden == false
            {
                self.viewCampaign.hidden = true
                viewTriangular_campain.hidden = true
                
                //show popUp of bottom image
                selectBottom(imgPicture)
            }
        }
        else
        {
            Global.sharedInstance.isOpenSale = true
            viewShowSale.backgroundColor = UIColor.clearColor()

            self.view.removeConstraint(topSaleClose)
            self.view.addConstraint(topToSale)
        }
    }
  
 }
