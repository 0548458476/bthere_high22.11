//
//  DefinationsClientViewController.swift
//  Bthere
//
//  Created by User on 8.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class DefinationsClientViewController: NavigationModelViewController {
    
    @IBOutlet weak var viewPDTop: UIView!
    @IBOutlet weak var viewPDRight: UIView!
    @IBOutlet weak var viewPDButtom: UIView!
    @IBOutlet weak var viewPDLeft: UIView!
    //--------
    
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var viewBRight: UIView!
    @IBOutlet weak var viewBLeft: UIView!
    @IBOutlet weak var viewBTop: UIView!
    @IBOutlet weak var viewBbuttom: UIView!
    //-----
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPay: UILabel!
    @IBOutlet weak var lblNotifications: UILabel!
    @IBOutlet weak var lblPersonalDetails: UILabel!
    @IBOutlet weak var lblLang: UILabel!
    
    @IBOutlet weak var viewGDLeft: UIView!
    @IBOutlet weak var viewGDTop: UIView!
    
    @IBOutlet weak var viewGDRight: UIView!
    @IBOutlet weak var viewGDButtom: UIView!
    //---
    
    @IBOutlet weak var viewNButtom: UIView!
    @IBOutlet weak var viewNTop: UIView!
    
    @IBOutlet weak var viewNLeft: UIView!
    @IBOutlet weak var viewNRight: UIView!
    //----
    
    @IBOutlet weak var viewPLeft: UIView!
    @IBOutlet weak var viewPRight: UIView!
    @IBOutlet weak var viewPButton: UIView!
    @IBOutlet weak var viewPTop: UIView!

    @IBOutlet weak var viewPerDetails: UIView!
    @IBOutlet weak var viewNotifications: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblPay.text = NSLocalizedString("PAYMENT", comment: "")
        lblNotifications.text = NSLocalizedString("NOTIFICATIONS", comment: "")
        lblPersonalDetails.text = NSLocalizedString("PERSONAL_DETAILS_TITLE", comment: "")
        lblLang.text = NSLocalizedString("LANG", comment: "")
        
        addShaddow(viewPDTop)
        addShaddow(viewPDButtom)
        addShaddow(viewPDRight)
        addShaddow(viewPDLeft)
        addShaddow(viewBRight)
        addShaddow(viewBLeft)
        addShaddow(viewBbuttom)
        addShaddow(viewBTop)
        addShaddow(viewGDTop)
        addShaddow(viewGDRight)
        addShaddow(viewGDButtom)
        addShaddow(viewGDLeft)
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openNotifications))
        self.viewNotifications.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openPersonDetails))
        self.viewPerDetails.addGestureRecognizer(tap2)
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openLanguage))
        self.viewLang.addGestureRecognizer(tap3)
        
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openPayments))
        self.viewPayment.addGestureRecognizer(tap4)

//        addShaddow(viewGDButtom)
//        addShaddow(viewGDButtom)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addShaddow(view:UIView) {
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowRadius = 1.5
    }

    func openNotifications(){
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("notificationsForDefinationsViewController") as! notificationsForDefinationsViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    //פרטים אישיים
    func openPersonDetails(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("personalDetailsViewController") as! personalDetailsViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }

    func openLanguage(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("langForClientViewController") as! langForClientViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func openPayments(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("PaymentMethodViewController") as! PaymentMethodViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
