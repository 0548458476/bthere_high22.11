//
//  DetailsAppointmentViewController.swift
//  bthree-ios
//
//  Created by User on 21.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// פרטי תור ספק
class DetailsAppointmentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var tblHoursServices: UITableView!

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblKind: UILabel!
    @IBOutlet weak var viewKindService: UIView!
    @IBOutlet weak var viewGiveService: UIView!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var tblListWorkers: UITableView!
    
    @IBOutlet weak var viewCancelTurn: UIView!
    @IBOutlet weak var viewHourSer: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var tblDates: UITableView!
    @IBOutlet weak var tblKinds: UITableView!
    @IBOutlet weak var viewIn: UIView!
    @IBOutlet weak var lblGiveServiceName: UILabel!
    @IBOutlet weak var viewCall: UIView!
    var x = 0
    var x1 = 0
    var x2  = 0
    var x3 = 0
    var arr:Array<String> = ["asfaf","afas","dfsd","sfdf"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewIn.bringSubviewToFront(tblListWorkers)
        tblListWorkers.separatorStyle = .None
        viewIn.bringSubviewToFront(tblKinds)
        
        tblKinds.separatorStyle = .None
        viewIn.bringSubviewToFront(tblHoursServices)
        tblHoursServices.separatorStyle = .None
        viewIn.bringSubviewToFront(tblDates)
        tblDates.separatorStyle = .None
       self.view.bringSubviewToFront(tblListWorkers)
       tblListWorkers.hidden = true
        tblDates.hidden = true

        tblHoursServices.hidden = true
        tblKinds.hidden = true

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.imageTapped))
        viewGiveService.userInteractionEnabled = true
        viewGiveService.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.imageTapped1))
        viewKindService.userInteractionEnabled = true
        viewKindService.addGestureRecognizer(tapGestureRecognizer1)
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.imageTapped3))
        viewDate.userInteractionEnabled = true
        viewDate.addGestureRecognizer(tapGestureRecognizer2)
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.imageTapped2))
        viewHourSer.userInteractionEnabled = true
        viewHourSer.addGestureRecognizer(tapGestureRecognizer3)
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.CancelTurnTapped))
        viewCancelTurn.userInteractionEnabled = true
        viewCancelTurn.addGestureRecognizer(tapGestureRecognizer4)
        let tapGestureRecognizer5 = UITapGestureRecognizer(target:self, action:#selector(DetailsAppointmentViewController.CallTapped))
        viewCall.userInteractionEnabled = true
        viewCall.addGestureRecognizer(tapGestureRecognizer5)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer()
        tap.delegate = self
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }

    @IBAction func btnCancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
           }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch(tableView){
        case tblListWorkers:
        let cell:GiveServiceNameTableViewCell = tableView.dequeueReusableCellWithIdentifier("GiveServiceNameTableViewCell")as!GiveServiceNameTableViewCell
        cell.setDisplayData(arr[indexPath.row])
        cell.selectionStyle = .None
        return cell
        case tblKinds:
            let cell:KindServiceNameTableViewCell = tableView.dequeueReusableCellWithIdentifier("KindServiceNameTableViewCell")as!KindServiceNameTableViewCell
            cell.setDisplayData(arr[indexPath.row])
            cell.selectionStyle = .None
            return cell
        
        case tblHoursServices:
        let cell:HoursTableViewCell = tableView.dequeueReusableCellWithIdentifier("HoursTableViewCell")as!HoursTableViewCell
        cell.setDisplayData(arr[indexPath.row])
        cell.selectionStyle = .None
        return cell
        case tblDates:
            let cell:DatesTableViewCell = tableView.dequeueReusableCellWithIdentifier("DatesTableViewCell")as!DatesTableViewCell
            cell.setDisplayData(arr[indexPath.row])
            cell.selectionStyle = .None
            return cell
        default:
            let cell:DatesTableViewCell = tableView.dequeueReusableCellWithIdentifier("DatesTableViewCell")as!DatesTableViewCell
            cell.setDisplayData(arr[indexPath.row])
            cell.selectionStyle = .None
            return cell
    }
   }


   func imageTapped(){
    if x == 0{
    tblListWorkers.hidden = false
        x = 1
   }
    else{
        x = 0
        tblListWorkers.hidden = true
    }
    
   }
    
    func imageTapped1(){
        if x1 == 0{
            tblKinds.hidden = false
            x1 = 1
        }
        else{
            x1 = 0
            tblKinds.hidden = true
        }
    }
    
    func imageTapped2(){
        if x2 == 0{
            tblHoursServices.hidden = false
            x2 = 1
        }
        else{
            x2 = 0
            tblHoursServices.hidden = true
        }
    }
    
    
    func imageTapped3(){
        
        if x3 == 0{
            tblDates.hidden = false
            x3 = 1
        }
        else{
            x3 = 0
            tblDates.hidden = true
        }
    }
    
    func CancelTurnTapped(){
        
    }
    
    func CallTapped(){
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == tblListWorkers{
            lblGiveServiceName.text = arr[indexPath.row]
            tblListWorkers.hidden = true

        }
        if tableView == tblKinds{
            lblKind.text = arr[indexPath.row]
            tblKinds.hidden = true
            
        }
        if tableView == tblHoursServices{
            lblHour.text = arr[indexPath.row]
            tblHoursServices.hidden = true
            
        }
        if tableView == tblDates{
            lblDate.text = arr[indexPath.row]
            tblDates.hidden = true
            
        }
    }

    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        self.dismissKeyboard()
        return false
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
