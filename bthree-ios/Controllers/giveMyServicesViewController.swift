//
//  giveMyServicesViewController.swift
//  Bthere
//
//  Created by User on 10.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol deleteItemDelegate {
    func deleteItem(indexPath:Int)
}
//לקוח קיים: נותני השרות שלי-מתפריט פלוס
class giveMyServicesViewController: NavigationModelViewController,dismissViewControllerDelegate,UIGestureRecognizerDelegate ,deleteItemDelegate,closeCollectionDelegate{
    
    var generic:Generic = Generic()
    
    var storyboard1:UIStoryboard?
    
    var arrProviders:Array<SearchResulstsObj> = Array<SearchResulstsObj>()
    
    //the sorted array by rating
    var dicResultSorted:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
    
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var tblResults: UITableView!
    
    // MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = NSLocalizedString("", comment: "")
        
        Global.sharedInstance.arrayServicesKods = []
        
        getProviders()
        
        Global.sharedInstance.giveMyServices = self
        

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(giveMyServicesViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        dicResultSorted = Global.sharedInstance.dicResults
        storyboard1 = UIStoryboard(name: "Main", bundle: nil)

        Global.sharedInstance.giveServices = self
        
        tblResults.separatorStyle = .None
               // Do any additional setup after loading the view.
        
        Global.sharedInstance.viewCon2 = storyboard1!.instantiateViewControllerWithIdentifier("BusinessProphilShowViewController") as? BusinessProphilShowViewController
        
        Global.sharedInstance.viewCon2!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        Global.sharedInstance.viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ListServicesViewController") as? ListServicesViewController
        Global.sharedInstance.viewCon!.backFromMyListServices = 1
        Global.sharedInstance.viewCon!.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Table View
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return Global.sharedInstance.dicResults.count
//        return arrProviders.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: giveMyServiceTableViewCell = self.tblResults.dequeueReusableCellWithIdentifier("giveMyServiceTableViewCell") as! giveMyServiceTableViewCell
        
        cell.setDisplayData(indexPath.section)
        cell.colViewResult.setContentOffset(CGPoint(x: 0 , y: 0), animated: false)
        cell.colViewResult.reloadData()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func dismissViewController()
    {
        let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalendarForAppointments") as! ModelCalendarForAppointmentsViewController
        
        self.navigationController?.pushViewController(viewCon, animated: false)
        
        // self.delegate.dismissViewController()
    }
    
    func dismissKeyboard() {
        
        self.view.endEditing(true)
    }
    
     //קבלת רשימת ספקים ללקוח
    func getProviders()
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.GetProviderListForCustomer(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    let searchObj:SearchResulstsObj = SearchResulstsObj()
                    Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                    self.arrProviders = searchObj.objProvidersToArray(Global.sharedInstance.dicResults)
                    self.tblResults.reloadData()
                }
                
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    
            })
        }
    }
    
    //מחיקת נותן שרות
    func deleteItem(indexPath:Int){
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iCustomerUserId"] = Global.sharedInstance.currentUser.iUserId
        dic["iProviderId"] = Global.sharedInstance.dicResults[indexPath]["iProviderId"]
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            // מחיקה מהשרת
            api.sharedInstance.RemoveProviderFromCustomer(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1// לא הצליחה המחיקה
                {
                    Alert.sharedInstance.showAlertDelegate("לא ניתן למחוק ספק זה")
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליחה המחיקה
                {
                    Global.sharedInstance.dicResults.removeAtIndex(indexPath)
                    Global.sharedInstance.isDeletedGiveMyService = true
                    self.tblResults.reloadData()
                }
                
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    
            })
        }
    }
    
    func closeCollection(collection:UICollectionView)
    {
        let index:NSIndexPath = NSIndexPath(forRow:0, inSection: 0)
        collection.scrollToItemAtIndexPath(index, atScrollPosition: UICollectionViewScrollPosition.Left, animated: false
        )
    }
}
