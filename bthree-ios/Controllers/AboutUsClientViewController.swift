//
//  AboutUsClientViewController.swift
//  Bthere
//
//  Created by Racheli Kroiz on 25.10.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class AboutUsClientViewController: NavigationModelViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtVAbout: UITextView!
    //לתקנון ותנאי השימוש
    @IBAction func btnRegulation(sender: AnyObject) {
        
        let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        
        let viewRegulation: RegulationClientViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("RegulationClientViewController")as! RegulationClientViewController
        viewRegulation.view.frame = CGRect(x: 0, y: 200, width: 50, height: 50)
//        self.view.addSubview(viewRegulation.view)
        viewRegulation.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewRegulation, animated: true, completion: nil)
//        self.presentViewController(viewRegulation, animated: true, completion: nil)
    }
    //MARK: - Outlet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = NSLocalizedString("ABOUT_US", comment: "")
        txtVAbout.text = NSLocalizedString("VIEW_ABOUT", comment: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
    }
}
