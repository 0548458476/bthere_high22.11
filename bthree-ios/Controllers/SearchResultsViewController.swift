//
//  SearchResultsViewController.swift
//  bthree-ios
//
//  Created by User on 21.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import NotificationCenter

protocol dismissViewControllerDelegate
{
    func dismissViewController()
}

protocol ReloadResultsDelegate {
    func ReloadResultsDelegate()
}

//דף תוצאות חיפוש
class SearchResultsViewController: NavigationModelViewController,dismissViewControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,ReloadResultsDelegate {
    
    @IBOutlet var btnSearch: UIButton!
    
    var generic:Generic = Generic()
    //fix 2.3
     var filterSubArr:NSArray =  NSArray()
//    var filterSubArr:NSMutableArray =  NSMutableArray()
    
    @IBOutlet weak var tblSearch: UITableView!
    
    @IBOutlet weak var btnChangeLocation: UIButton!
    
    @IBAction func btnChangeLocation(sender: AnyObject) {
         let viewCon:ChangeCityViewController = storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//עבור השליחה לשרת
    
    @IBAction func btnSearchClick(sender: AnyObject) {
        dismissKeyboard()
        
        Global.sharedInstance.searchDomain = txtSearch.text!
        
        if txtSearch.text != ""
        {
            Global.sharedInstance.dicSearch["nvKeyWord"] = txtSearch.text
            Global.sharedInstance.dicSearch["nvlong"] = Global.sharedInstance.currentLong
            Global.sharedInstance.dicSearch["nvlat"] = Global.sharedInstance.currentLat
            Global.sharedInstance.dicSearch["nvCity"] = nil
            
            generic.showNativeActivityIndicator(self)
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.SearchByKeyWord(Global.sharedInstance.dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_RESULTS", comment: ""))
                        self.tblSearch.hidden = true
                        Global.sharedInstance.dicResults = []
                        self.tblResults.reloadData()
                        self.lblResultCount.text = "0 \(NSLocalizedString("SEARCH_RESULT", comment: ""))"
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicSearchProviders = Global.sharedInstance.dicSearch
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        
                        self.lblResultCount.text = "\(Global.sharedInstance.dicResults.count) \(NSLocalizedString("SEARCH_RESULT", comment: ""))"
                        Global.sharedInstance.whichSearchTag = 1
                        self.tblResults.reloadData()
                        //    self.dismissViewControllerAnimated(false, completion: nil)
                        
                    }
                    self.tblResults.reloadData()
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                })
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NOT_DATA_TO_SEARCH", comment: ""),vc: self)
        }
    }
    
    @IBOutlet var lblResultCount: UILabel!
    
    var storyboard1:UIStoryboard?
    
    //the sorted array by rating
    var dicResultSorted:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
    
    // MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.sharedInstance.isSettingsOpen = false
        txtSearch.text = Global.sharedInstance.searchDomain
        txtSearch.placeholder = NSLocalizedString("SERACH_SERVICE_DOMAIN", comment: "")
        btnChangeLocation.setTitle(NSLocalizedString("CHANGE_LOCATION", comment: ""), forState: .Normal)
        lblSortBy.text = NSLocalizedString("ORDER_BY", comment: "")
        btnByRating.setTitle(NSLocalizedString("LEVEL", comment: ""), forState: .Normal)
        btnByDistance.setTitle(NSLocalizedString("DISTANCE", comment: ""), forState: .Normal)
        
        checkDevice()
        txtSearch.delegate = self
        tblSearch.hidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchResultsViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        dicResultSorted = Global.sharedInstance.dicResults
        storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        Global.sharedInstance.searchResult = self
        
        tblResults.separatorStyle = .None
        imgCircleDistance.hidden = false
        imgCircle.hidden = true
        
        Global.sharedInstance.viewCon2 = storyboard1!.instantiateViewControllerWithIdentifier("BusinessProphilShowViewController") as? BusinessProphilShowViewController
        
        Global.sharedInstance.viewCon2!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        Global.sharedInstance.viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ListServicesViewController") as? ListServicesViewController
        Global.sharedInstance.viewCon?.backFromMyListServices = 0
        Global.sharedInstance.arrayServicesKods = []
        Global.sharedInstance.viewCon!.delegate = self
          self.lblResultCount.text = "\(Global.sharedInstance.dicResults.count) \(NSLocalizedString("SEARCH_RESULT", comment: ""))"
        
        if Global.sharedInstance.dicSearchProviders["nvCity"] != nil//עשו חיפוש לפי עיר ולא לפי מיקום נוכחי
        {
            //מיון לפי דרוג
            Global.sharedInstance.dicResults.sortInPlace{
                (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
            }
            self.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
            self.imgCircle.hidden = false
            self.imgCircleDistance.hidden = true
            self.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
            self.tblResults.reloadData()
        }
        else
        {
            if CLLocationManager.locationServicesEnabled() == false//לא זוהה מיקום במכשיר
            {
                //מיון לפי הדרוג
                Global.sharedInstance.dicResults.sortInPlace{
                    (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
                }
                self.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
                self.imgCircle.hidden = false
                self.imgCircleDistance.hidden = true
                self.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
                self.tblResults.reloadData()
                
                //בלחיצה על אישור-פתיחת הגדרות
                let alertController = UIAlertController (title: "", message: NSLocalizedString("NO_AVAILABLE_LOCATION", comment: ""), preferredStyle: .Alert)
                
                let settingsAction = UIAlertAction(title: NSLocalizedString("OPEN_LOCATION_SETTINGS", comment: ""), style: .Default) { (_) -> Void in
                    
                    Global.sharedInstance.isSettingsOpen = true
                    //fix 2.3
                    if let url = NSURL(string: UIApplicationOpenSettingsURLString)
                    {//string:"prefs:root=LOCATION_SERVICES") {
                        if #available(iOS 10, *)
                        {
                            UIApplication.sharedApplication().openURL(url, options: [:], completionHandler: {
                                (success) in
                                print("succes open settings")
                                })
                        }
                        else
                        {
                            UIApplication.sharedApplication().openURL(NSURL(string:"prefs:root=LOCATION_SERVICES")!)
                        }
                    }
                    //UIApplication.sharedApplication().openURL(NSURL(string:"prefs:root=LOCATION_SERVICES")!)
                }
                //בלחיצה על ביטול-מיון לפי דרוג
                let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default) { (_) -> Void in
                    
                    //פתיחת דף שנה מיקום
                    let viewCon:ChangeCityViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                    viewCon.delegate = self
                    viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                    self.presentViewController(viewCon, animated: true, completion: nil)
                }
                alertController.addAction(settingsAction)
                alertController.addAction(cancelAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            else if Global.sharedInstance.currentLat == nil// לא זוהה המיקום בביזר
            {
                //מיון לפי הדרוג
                Global.sharedInstance.dicResults.sortInPlace{
                    (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
                }
                self.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
                self.imgCircle.hidden = false
                self.imgCircleDistance.hidden = true
                self.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
                tblResults.reloadData()
                
                //בלחיצה על אישור-פתיחת הגדרות
                let alertController = UIAlertController (title: "", message: NSLocalizedString("NO_AVAILABLE_LOCATION", comment: ""), preferredStyle: .Alert)
                
                let settingsAction = UIAlertAction(title: NSLocalizedString("OPEN_LOCATION_SETTINGS", comment: ""), style: .Default) { (_) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    
                    if #available(iOS 10, *)
                    {
                        if let url = settingsUrl
                        {
                            UIApplication.sharedApplication().openURL(url, options: [:], completionHandler: {
                                (success) in
                            })
                        }
                    }
                    else
                    {
                        if let url = settingsUrl {
                            UIApplication.sharedApplication().openURL(url)
                            Global.sharedInstance.isSettingsOpen = true
                        }
                    }
                }
                //בלחיצה על ביטול-מיון לפי דרוג
                let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default) { (_) -> Void in
                    
                    //פתיחת דף שנה מיקום
                    let viewCon:ChangeCityViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                    viewCon.delegate = self
                    viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                    self.presentViewController(viewCon, animated: true, completion: nil)
                }
                alertController.addAction(settingsAction)
                alertController.addAction(cancelAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        tblSearch.hidden = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
    }
    
    override func viewWillAppear(animated: Bool) {
//        let NotificationCenter:NSNotificationCenter = NSNotificationCenter()
//        
//        NotificationCenter.addObserver(self, selector: #selector(self.applicationWillEnterForeground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
    }
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblResults: UITableView!
    
    @IBOutlet weak var lblSortBy: UILabel!
    
    @IBOutlet weak var btnByRating: UIButton!
    //דרוג
    @IBAction func btnByRating(sender: AnyObject) {
        
        btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
        imgCircle.hidden = false
        imgCircleDistance.hidden = true
        btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
        
        Global.sharedInstance.dicResults.sortInPlace{
            (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
        }
        tblResults.reloadData()
    }
    
    @IBOutlet weak var btnByDistance: UIButton!
    //מרחק
    @IBAction func btnByDistance(sender: AnyObject) {
        btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
        imgCircleDistance.hidden = false
        imgCircle.hidden = true
        btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
        if CLLocationManager.locationServicesEnabled() == false//לא זוהה מיקום במכשיר
        {
            //מיון לפי הדרוג
            Global.sharedInstance.dicResults.sortInPlace{
                (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
            }
            self.btnByRating.titleLabel?.font = UIFont(name: "OpenSansHebrew-Bold", size: 20)
            self.imgCircle.hidden = false
            self.imgCircleDistance.hidden = true
            self.btnByDistance.titleLabel?.font = UIFont(name: "OpenSansHebrew-Light", size: 20)
            self.tblResults.reloadData()
            
            //בלחיצה על אישור-פתיחת הגדרות
            let alertController = UIAlertController (title: "", message: NSLocalizedString("NO_AVAILABLE_LOCATION", comment: ""), preferredStyle: .Alert)
            
            let settingsAction = UIAlertAction(title: NSLocalizedString("OPEN_LOCATION_SETTINGS", comment: ""), style: .Default) { (_) -> Void in
                
                Global.sharedInstance.isSettingsOpen = true
                
                if #available(iOS 10, *)
                {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl
                    {
                        UIApplication.sharedApplication().openURL(url, options: [:], completionHandler: {
                            (success) in
                        })
                    }
                }
                else{
                    UIApplication.sharedApplication().openURL(NSURL(string:"prefs:root=LOCATION_SERVICES")!)
                }
            }
            //בלחיצה על ביטול-מיון לפי דרוג
            let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default) { (_) -> Void in
                
                //פתיחת דף שנה מיקום
                let viewCon:ChangeCityViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                viewCon.delegate = self
                viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                self.presentViewController(viewCon, animated: true, completion: nil)
            }
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else if Global.sharedInstance.currentLat == nil//לא זוהה המיקום
        {
            //מיון לפי הדרוג
            Global.sharedInstance.dicResults.sortInPlace{
                (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
            }
            self.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
            self.imgCircle.hidden = false
            self.imgCircleDistance.hidden = true
            self.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
            tblResults.reloadData()
            
            
            //בלחיצה על אישור-פתיחת הגדרות
            let alertController = UIAlertController (title: "", message: NSLocalizedString("NO_AVAILABLE_LOCATION", comment: ""), preferredStyle: .Alert)
            
            let settingsAction = UIAlertAction(title: NSLocalizedString("OPEN_LOCATION_SETTINGS", comment: ""), style: .Default) { (_) -> Void in
                
                Global.sharedInstance.isSettingsOpen = true
                
                if #available(iOS 10, *)
                {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl
                    {
                        UIApplication.sharedApplication().openURL(url, options: [:], completionHandler: {
                            (success) in
                        })
                    }
                }
                else
                {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
            }
            //בלחיצה על ביטול-מיון לפי דרוג
            let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default) { (_) -> Void in
                Global.sharedInstance.dicResults.sortInPlace{
                    (($0 )["IInternalRank"] as? Int) > (($1 )["IInternalRank"] as? Int)
                }
                self.btnByRating.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 20)
                self.imgCircle.hidden = false
                self.imgCircleDistance.hidden = true
                self.btnByDistance.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 20)
                self.tblResults.reloadData()
                Global.sharedInstance.isSettingsOpen = false
                //פתיחת דף שנה מיקום
                let viewCon:ChangeCityViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeCityViewController") as! ChangeCityViewController
                viewCon.delegate = self
                viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                self.presentViewController(viewCon, animated: true, completion: nil)
            }
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        else
        {
            var dicPositiveDistance:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
            
            var dicNegativeDistance:Array<Dictionary<String,AnyObject>> = Array<Dictionary<String,AnyObject>>()
            
            for result in Global.sharedInstance.dicResults {
                if (result["iDistance"] as! Int) == -1
                {
                    dicNegativeDistance.append(result)
                }
                else
                {
                    dicPositiveDistance.append(result)
                }
            }
            
            //Global.sharedInstance.dicResults
            dicPositiveDistance.sortInPlace
                {
                (($0)["iDistance"] as? Float) < (($1)["iDistance"] as? Float)
                }
            
            Global.sharedInstance.dicResults = dicPositiveDistance
            
            for item in dicNegativeDistance
            {
                Global.sharedInstance.dicResults.append(item)
            }
            
            tblResults.reloadData()
        }
    }
    
    @IBOutlet weak var imgCircle: UIImageView!
    
    @IBOutlet weak var imgCircleDistance: UIImageView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    

    ////for the autoComplete
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //בשביל ה-autoComplete
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        if Reachability.isConnectedToNetwork() == false && string != ""//כדי שהטקסט יכיל גם את האות האחרונה שהקליד
        {
            textField.text = startString
        }
        
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if substring == ""
        {
            tblSearch.hidden = true
        }
        else
        {
            
            if startString.characters.count > 120
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY120_CHARACTERS", comment: ""),vc: self)
                return false
            }
            
            tblSearch.hidden = false
            searchAutocompleteEntriesWithSubstring(substring)
        }
        
        return true
    }
    
    
    //for autoCopmlete:
    func searchAutocompleteEntriesWithSubstring(substring: String)
    {
        if substring.characters.count >= 1
        {
            if txtSearch.text != ""
            {
                var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicSearch["nvKeyWord"] = substring
                
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                api.sharedInstance.SearchWordCompletion(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        self.filterSubArr = NSArray()
                        self.filterSubArr = responseObject["Result"] as! NSArray
                        for f in self.filterSubArr
                        {
                            print(f)
                        }
                        //  var s =  self.filterSubArr[0]
                        //                       print("תוצאות השלמה :\(self.filterSubArr)" )
                        self.tblSearch.reloadData()
                        
                        
                        //   self.openSearchResults()
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                })
                }
            }
        }
        tblSearch.reloadData()
    }

    //MARK: - Table View
    
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == tblSearch
        {
//            if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
//                  return self.view.frame.height * 0.075
//            }

            return self.view.frame.height * 0.09687
                   }
        
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            return 110
        }

        return 150
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == tblSearch
        {
            return 1
        }
        return Global.sharedInstance.dicResults.count
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblSearch
        {
            if filterSubArr.count == 0
            {
                tblSearch.hidden = true
            }
            return filterSubArr.count
        }
        return  1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView == tblSearch
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("Search")as! SearchTableViewCell
            cell.setDisplayData(filterSubArr[indexPath.row] as! String)
            return cell
        }

        let cell: SearchResultsTableViewCell = self.tblResults.dequeueReusableCellWithIdentifier("SearchResults") as! SearchResultsTableViewCell
        
        cell.setDisplayData(indexPath.section)
        
        cell.colViewResult.setContentOffset(CGPoint(x: 0 , y: 0), animated: false)

        cell.colViewResult.reloadData()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
        if tableView == tblSearch
        {
            txtSearch.text = filterSubArr[indexPath.row] as? String
            //txtSearch.text = filtered[indexPath.row]
            tblSearch.hidden = true
        }
    }
    

    func dismissViewController()
    {
        let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalendarForAppointments") as! ModelCalendarForAppointmentsViewController
        Global.sharedInstance.idWorker = -1
        self.navigationController?.pushViewController(viewCon, animated: false)
        
        
        // self.delegate.dismissViewController()
    }
    
    func dismissKeyboard() {
      
        tblSearch.hidden = true
        self.view.endEditing(true)
    }
    
    func  checkDevice()
    {
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            let fontSize:CGFloat = (self.btnChangeLocation.titleLabel?.font.pointSize)!
            btnChangeLocation.titleLabel!.font = UIFont(name: (self.btnChangeLocation.titleLabel?.font.familyName)!, size: 14)
            let fontSize1:CGFloat = (self.lblSortBy.font.pointSize)
            lblSortBy.font = UIFont(name: (self.lblSortBy.font.familyName), size: 18)
            let fontSize2:CGFloat = (self.btnByRating.titleLabel?.font.pointSize)!
            btnByRating.titleLabel!.font = UIFont(name: (self.btnByRating.titleLabel?.font.familyName)!, size: 18)
            let fontSize3:CGFloat = (self.btnByDistance.titleLabel?.font.pointSize)!
            btnByDistance.titleLabel!.font = UIFont(name: (self.btnByDistance.titleLabel?.font.familyName)!, size: 18)

            
        }
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view!.isDescendantOfView(tblSearch) {
            
            return false
        }
        return true
    }
    
    //פונקציה דליגייטית לריענון הטבלה לאחר שנה מיקום
    func ReloadResultsDelegate()
    {
        self.lblResultCount.text = "\(Global.sharedInstance.dicResults.count) \(NSLocalizedString("SEARCH_RESULT", comment: ""))"
        tblResults.reloadData()
    }
    
    func applicationWillEnterForeground(p:UIApplication) {
        print("df")
    }

}
