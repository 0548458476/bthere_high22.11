//
//  detailsAppointmetClientViewController.swift
//  Bthere
//
//  Created by User on 11.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
protocol openCustomerDelegate {
    func openCustomer()
}
protocol getCustomerOrdersDelegate {
    func GetCustomerOrders()
}
//פרטי התור
class detailsAppointmetClientViewController: NavigationModelViewController,openCustomerDelegate,openFromMenuDelegate,getCustomerOrdersDelegate{

    //MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSupllierName: UILabel!
    
    @IBOutlet weak var lblSupplierService: UILabel!
    
    @IBOutlet weak var imgPlusMenu: UIImageView!
    
    @IBOutlet weak var lblSupplierAddress: UILabel!
    
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var lblAdvertising: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    //בלחיצה על ביטול
    @IBAction func btnCancel(sender: AnyObject) {
        
        Global.sharedInstance.isCancelAppointmentClick = true
        showDetailsAppointmentViewController = false
        self.getFreeDaysForServiceProvider()//קבלת רשימת התורים הפנויים מעודכנת
    }
    
    @IBOutlet weak var btnOk: UIButton!
    
    //לחיצה על אישור
    @IBAction func btnOk(sender: UIButton) {
        
        if didClickOK == false
        {
            didClickOK = true
            let uniqueVals = uniq(Global.sharedInstance.arrayServicesKodsToServer)
            var dateToServer = Global.sharedInstance.dateDayClick
            
            //אם מתצוגת שבוע הוספתי יום- כי התאריך שהתקבל הוא מיום קודם
            if Global.sharedInstance.whichDesignOpenDetailsAppointment == 2
            {
                dateToServer = Calendar.sharedInstance.addDay(Global.sharedInstance.dateDayClick)
            }
            
            let order:OrderObj = OrderObj(_iSupplierId: Global.sharedInstance.providerID, _iUserId: Global.sharedInstance.currentUser.iUserId, _iSupplierUserId: Global.sharedInstance.arrayGiveServicesKods, _iProviderServiceId:uniqueVals /*Global.sharedInstance.arrayServicesKodsToServer*/, _dtDateOrder:dateToServer,_nvFromHour: Global.sharedInstance.hourFreeEvent, _nvComment: "")
            
            var dicOrderObj:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            
            print(Global.sharedInstance.dateDayClick.description)
            dicOrderObj["orderObj"] = order.getDic()
            
            if tag != 4//לא מתור חדש
            {
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                    api.sharedInstance.newOrder(dicOrderObj, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if responseObject["Error"]!!["ErrorCode"] as! Int == -1//אם אין תור פנוי בזמן הזה
                        {
                            if self.fromViewMode == true
                            {
                                Alert.sharedInstance.showAlert(NSLocalizedString("THX_REGISTERED", comment: ""), vc: self)
                                
                                self.timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(self.doDelayed), userInfo: nil, repeats: false)
                            }
                            else
                            {
                                Alert.sharedInstance.showAlert(NSLocalizedString("NO_TURN", comment: ""), vc: self)
                                self.timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.doDelayed), userInfo: nil, repeats: false)
                            }
                        }
                        else if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליח
                        {
                            self.getFreeDaysForServiceProvider()//קבלת רשימת התורים הפנויים מעודכנת
                            self.saveEventInDeviceCalander()
                        }
                        else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//שגיאה
                        {
                            Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_SERVER", comment: ""), vc: self)
                        }
                        
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {
                            (AFHTTPRequestOperation, NSError) -> Void in
                            self.generic.hideNativeActivityIndicator(self)
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                    })
                }
            }
            else//מתור חדש
            {
                delegateSetNewOrder.setNewOrder()
            }
        }
    }
    
    //MARK: - Properties
    
    var showDetailsAppointmentViewController:Bool = true
    var fromViewMode = false
    var generic:Generic = Generic()
    var delegateSetNewOrder:setNewOrderDelegate!=nil
    var clientStoryBoard:UIStoryboard?
    
    let dateFormatter = NSDateFormatter()
    let dateFormatter1 = NSDateFormatter()
    
    var dateEvent:NSDate = NSDate()
    var fromHour:String = ""
    var supplierName:String = ""
    var serviceName:String = ""
    var address:String = ""
    
    var didClickOK:Bool = false//מציין אם כבר לחצו על אישור פעם אחת כדי למנוע לחיצה כפולה
    
    var timer: NSTimer? = nil
    
    var storyboard1:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    var tag:Int = 0//מציין האם הגיעו מלחיצה על תור פנוי או מלליצה על תור של ביזר תפוס,1=פנוי,2=תפוס
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = NSLocalizedString("DETAILS_APPOINTMENT_TITLE", comment: "")
        btnOk.setTitle(NSLocalizedString("OK", comment: ""), forState: .Normal)
        btnCancel.setTitle(NSLocalizedString("CANCEL", comment: ""), forState: .Normal)
        lblAdvertising.text = NSLocalizedString("ADVERTISINGS", comment: "")
        
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblSupllierName.font = UIFont(name: lblSupllierName.font.fontName, size: 17)
            lblSupplierService.font = UIFont(name: lblSupplierService.font.fontName, size: 17)
            lblSupplierAddress.font = UIFont(name: lblSupplierAddress.font.fontName, size: 17)
            lblDay.font = UIFont(name: lblDay.font.fontName, size: 17)
            lblDate.font = UIFont(name: lblDate.font.fontName, size: 17)
            lblHour.font = UIFont(name: lblHour.font.fontName, size: 17)
        }
        
        showDetailsAppointmentViewController = true
        
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(detailsAppointmetClientViewController.imageTapped))
        imgPlusMenu.userInteractionEnabled = true
        imgPlusMenu.addGestureRecognizer(tapGestureRecognizer)

        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if tag == 1//מתור פנוי
        {
            lblTitle.text = NSLocalizedString("DETAILS_APPOINTMENT", comment: "")
            lblDate.text = dateFormatter.stringFromDate(Global.sharedInstance.dateDayClick)
            lblHour.text = Global.sharedInstance.hourFreeEvent
            lblDay.text = Global.sharedInstance.dayFreeEvent
            lblSupllierName.text = Global.sharedInstance.currentProviderToCustomer.nvProviderName
            lblSupplierService.text = Global.sharedInstance.serviceName
            lblSupplierAddress.text = Global.sharedInstance.currentProviderToCustomer.nvAdress
            
            btnOk.hidden = false
            btnCancel.hidden = false
        }
        else if tag == 2//מתור תפוס
        {
            lblTitle.text = NSLocalizedString("DETAILS_MEETING", comment: "")
            lblDate.text = dateFormatter.stringFromDate(Global.sharedInstance.dateEventBthereClick)
            lblHour.text = Global.sharedInstance.hourBthereEvent
            lblDay.text = Global.sharedInstance.dayFreeEvent
            lblSupllierName.text = Global.sharedInstance.orderDetailsFoBthereEvent.nvSupplierName
            var serviceName = ""
            for item in Global.sharedInstance.orderDetailsFoBthereEvent.objProviderServiceDetails
            {
                if serviceName == ""
                {
                    serviceName = item.nvServiceName
                }
                else
                {
                    serviceName = "\(serviceName),\(item.nvServiceName)"
                }
            }
            
            lblSupplierService.text = serviceName
            lblSupplierAddress.text = Global.sharedInstance.orderDetailsFoBthereEvent.nvAddress
            
            btnOk.hidden = true
            btnCancel.hidden = true
            let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissPopUp))
            view.addGestureRecognizer(tap)
        }
        else if tag == 3//מתצוגת רשימה מיומן לקוח
        {
            lblDate.text = dateFormatter.stringFromDate(Global.sharedInstance.dateEventBthereClick)
            lblHour.text = Global.sharedInstance.hourBthereEvent
            lblDay.text = Global.sharedInstance.dayFreeEvent
            lblSupllierName.text = Global.sharedInstance.orderDetailsFoBthereEvent.nvSupplierName
            lblSupplierService.text = Global.sharedInstance.serviceName
            lblSupplierAddress.text = Global.sharedInstance.orderDetailsFoBthereEvent.nvAddress
            
            btnOk.hidden = true
            btnCancel.hidden = true
            let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissPopUp))
            view.addGestureRecognizer(tap)
        }
        else if tag == 4//מתור חדש
        {
            lblDate.text = dateFormatter.stringFromDate(dateEvent)
            lblHour.text = fromHour
            lblDay.text = convertDays(Calendar.sharedInstance.getDayOfWeek(dateEvent)!)
            lblSupllierName.text = supplierName
            lblSupplierService.text = serviceName
            lblSupplierAddress.text = address
        }
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        // Do any additional setup after loading the view.
        didClickOK = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //פונקציה שנקראת לאחר ההשהיה של הטיימר בשביל הצגת הפופאפ
    func doDelayed()
    {
        Global.sharedInstance.isCancelAppointmentClick = true
        showDetailsAppointmentViewController = false
        self.getFreeDaysForServiceProvider()//קבלת רשימת התורים הפנויים מעודכנת
    }

    //שמירת ארוע ביזר במכשיר
    func saveEventInDeviceCalander()
    {
        let eventStore : EKEventStore = EKEventStore()
        
        eventStore.requestAccessToEntityType(.Event, completion: { (granted, error) in
            if (granted) && (error == nil)
            {
                let event = EKEvent(eventStore: eventStore)
                let serviceName =  "\(Global.sharedInstance.currentProviderToCustomer.nvProviderName) \(Global.sharedInstance.serviceName)"
                event.notes = "Bthere"
                event.title = serviceName
                event.allDay = false
                event.startDate = Global.sharedInstance.eventBthereDateStart
                event.endDate = Global.sharedInstance.eventBthereDateEnd
                
                event.calendar = eventStore.defaultCalendarForNewEvents
                do
                {
                    try eventStore.saveEvent(event, span: .ThisEvent)
                    print("success - " + event.title)
                }
                catch let e as NSError
                {
                    print(e)
                    return
                }
            }
            else
            {
                
            }
        })
    }
    
   //קבלת ימים פנויים לנותן שרות
    func getFreeDaysForServiceProvider(){
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        
        formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
        
        Global.sharedInstance.dicGetFreeDaysForServiceProvider["lProviderServiceId"] = Global.sharedInstance.arrayServicesKodsToServer
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.GetFreeDaysForServiceProvider(Global.sharedInstance.dicGetFreeDaysForServiceProvider, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    dateFormatter.timeZone = NSTimeZone(name: "GMT")
                    let ps:providerFreeDaysObj = providerFreeDaysObj()
                    
                    Global.sharedInstance.getFreeDaysForService = ps.objFreeDaysToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    
                    //איפוס המערך מנתונים ישנים
                    Global.sharedInstance.dateFreeDays = []
                    
                    for i in 0 ..< Global.sharedInstance.getFreeDaysForService.count{
                        let dateDt = Calendar.sharedInstance.addDay(Global.sharedInstance.getStringFromDateString(Global.sharedInstance.getFreeDaysForService[i].dtDate))//היום שיש בו שעות פנויות
                        
                        Global.sharedInstance.dateFreeDays.append(dateDt)//מערך שמכיל את תאריכי הימים בהם אפשר לקבוע תור ז״א יש בהם שעות פנויות
                        
                    }
                    Global.sharedInstance.fromHourArray = Global.sharedInstance.dateFreeDays
                    Global.sharedInstance.endHourArray = Global.sharedInstance.dateFreeDays
                    
                    for i in 0 ..< Global.sharedInstance.dateFreeDays.count{//יש לי מערך של תאריכים ובאותו מקום של התאריך במערך יש לי את השעות של התורים הפנויים של אותו תאריך
                        let provider:providerFreeDaysObj = Global.sharedInstance.getFreeDaysForService[i]
                        
                        let hourStart = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvFromHour)
                        
                        Global.sharedInstance.fromHourArray[i] = hourStart
                        let hourEnd = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvToHour)
                        Global.sharedInstance.endHourArray[i] = hourEnd
                        
                    }
                    
                    let currentDate:NSDate = NSDate()
                    var dayOfWeekToday = 0
                    
                    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
                    
                    //אתחול מערך השעות הפנויות לשבוע
                    //ואתחול מערך האירועים של ביזר
                    for i in 0 ..< 7 {
                        
                        let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                        
                        Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
                        Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
                    }
                    self.GetCustomerOrders()
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }

      //פותחת את תפריט הפלוס הצידי
    func imageTapped(){
        
        let viewCon:MenuPlusViewController = storyboard1.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }

    //פתיחת דף ראשי של לקוח קיים
   func openCustomer()
   {
        Calendar.sharedInstance.carrentDate = NSDate()
    
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
        let viewCon = storyBoard.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as!  entranceCustomerViewController
        self.navigationController?.pushViewController(viewCon, animated: false)
   }
    
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
    //ממין מערך שלא יהיה עם ערכים כפולים
    func uniq<S : SequenceType, T : Hashable where S.Generator.Element == T>(source: S) -> [T] {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    //קבלת רשימת התורים של לקוח בביזר
    func GetCustomerOrders()
    {
        var dic:Dictionary<String,AnyObject> =  Dictionary<String,AnyObject>()
        var arr = NSArray()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetCustomerOrders(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            print(responseObject["Result"])
            arr = responseObject["Result"] as! NSArray
            let ps:OrderDetailsObj = OrderDetailsObj()
            
            Global.sharedInstance.ordersOfClientsArray = ps.OrderDetailsObjToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
            
            if self.showDetailsAppointmentViewController == true
            {
                self.dismissViewControllerAnimated(false, completion: nil)
                let viewCon = self.clientStoryBoard!.instantiateViewControllerWithIdentifier("PopUpDetailsAppointmentViewController") as! PopUpDetailsAppointmentViewController
                viewCon.delegate = self
                
                viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                self.presentViewController(viewCon, animated: false, completion: nil)
            }
            else
            {
                self.dismiss()
            }
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
    func dismiss()
    {
        if Global.sharedInstance.model == 2//יומן ספק שהלקוח רואה
        {
            Global.sharedInstance.isCancelAppointmentClick = true
            Calendar.sharedInstance.carrentDate = NSDate()
            
            let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalendarForAppointments") as!  ModelCalendarForAppointmentsViewController
            self.navigationController?.pushViewController(viewCon, animated: false)
        }
        else//יומן לקוח
        {
            Global.sharedInstance.isCancelAppointmentClick = true
            Calendar.sharedInstance.carrentDate = NSDate()
            
            let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as!  ModelCalenderViewController
            self.navigationController?.pushViewController(viewCon, animated: false)
        }
    }
    
    func dismissPopUp()
    {
        dismiss()
    }
    
    //convert day from int to string
    //get int day and returns itws string value
    func convertDays(day:Int) -> String {
        
        switch day {
        case 1:
            return NSLocalizedString("FIRST", comment: "")
        case 2:
            return NSLocalizedString("SECOND", comment: "")
        case 3:
            return NSLocalizedString("THIRD", comment: "")
        case 4:
            return NSLocalizedString("FOURTH", comment: "")
        case 5:
            return NSLocalizedString("FIFTH", comment: "")
        case 6:
            return NSLocalizedString("SIX", comment: "")
        case 7:
            return NSLocalizedString("SEVEN", comment: "")
        default:
            return NSLocalizedString("FIRST", comment: "")
        }
    }
}
