//
//  SplashViewController.swift
//  bthree-ios
//
//  Created by User on 13.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

/**
 * Snippet from CodeTrench.com
 */
//ספלש
class SplashViewController: UIViewController {
    
    @IBOutlet weak var backGroundImg: UIImageView!
    
    @IBOutlet weak var logoImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

     backGroundImg.image = UIImage(named: "main.jpg")
    logoImg.image = UIImage(named: "1.png")
        // Show the home screen after a bit. Calls the show() function.
        
        let timer = NSTimer.scheduledTimerWithTimeInterval(
            4, target: self, selector: #selector(FBSDKSharingDialog.show), userInfo: nil, repeats: false
        )
        
    }
    
    /*
    * Gets rid of the status bar
    */
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    /*
    * Shows the app's main home screen.
    * Gets called by the timer in viewDidLoad()
    */
    func show() {
        self.performSegueWithIdentifier("splashToEntrance", sender: self)
    }
    

}
