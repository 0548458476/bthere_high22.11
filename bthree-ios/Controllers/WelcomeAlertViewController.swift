//
//  WelcomeAlertViewController.swift
//  bthree-ios
//
//  Created by Tami wexelbom on 2/17/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//פופאפ ברוך הבא לאחר רישום ספק
class WelcomeAlertViewController: UIViewController {
    var delegate:openRegisterDelegate!=nil
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lbl1: UILabel!
    @IBOutlet var lbl2: UILabel!
    @IBOutlet var btnOk: UIButton!
    
    @IBAction func btnCancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true) {
            self.delegate.openRegisterView()
        }
    }
    @IBAction func btnOk(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnOk.setTitle(NSLocalizedString("OK", comment: ""), forState: .Normal)
        if DeviceType.IS_IPHONE_6 {
          lblTitle.font = Colors.sharedInstance.fontSecondHeader
        }
        else if  DeviceType.IS_IPHONE_5{
            lblTitle.font = Colors.sharedInstance.fontSmallHeader

        }
        
        lblTitle.text = NSLocalizedString("WELCOME", comment: "")
        lbl1.text = NSLocalizedString("OPEN_CALENSER1", comment: "")
        lbl2.text = NSLocalizedString("OPEN_CALENSER2", comment: "")
        btnOk.setTitle(NSLocalizedString("OK", comment: ""), forState: .Normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
