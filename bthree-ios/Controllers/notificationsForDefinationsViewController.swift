//
//  notificationsForDefinationsViewController.swift
//  Bthere
//
//  Created by User on 8.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//תפריט המבוגר לקוח-הגדרות---התראות
class notificationsForDefinationsViewController: NavigationModelViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - Properties
    
    var arrayHeaders:Array<String> = [NSLocalizedString("DISCOUNT_MINUTE_90", comment: ""),NSLocalizedString("MINUTES_BEFORE_MEETING_20", comment: ""),NSLocalizedString("OK_FROM_BUSINESS", comment: ""),NSLocalizedString("UPDATE_WAIT_TURN", comment: ""),NSLocalizedString("UPDATES_NEWS", comment: "")]
    
    var generic:Generic = Generic()
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblAdvertising: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblNotifTItle: UILabel!
    @IBOutlet weak var tblNotifications: UITableView!
    //שמור
    @IBAction func btnSave(sender: AnyObject) {
        
        var dicAddAlert:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicAddAlert["customerAlertsSettingsObj"] = Global.sharedInstance.customerAlertsSettingsObj.getDic()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.AddAlertSettingsForCustomer(dicAddAlert, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -1 || responseObject["Error"]!!["ErrorCode"] as! Int == -2//לא הצליח
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_SAVE", comment: ""), vc: self)
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("SUCCESS_SETTINGS", comment: ""), vc: self)
                //open DefinationsClientViewController
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let frontviewcontroller:UINavigationController = UINavigationController()
                
                let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
                let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
                
                frontviewcontroller.pushViewController(vc, animated: false)
                
                let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                
                let mainRevealController = SWRevealViewController()
                
                mainRevealController.frontViewController = frontviewcontroller
                mainRevealController.rearViewController = rearViewController
                
                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                window.rootViewController = mainRevealController
                
            }
            self.generic.hideNativeActivityIndicator(self)
            
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        })
        }
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblAdvertising.text = NSLocalizedString("ADVERTISINGS", comment: "")
        btnSave.setTitle(NSLocalizedString("SAVE_BTN", comment: ""), forState: .Normal)
        lblNotifTItle.text = NSLocalizedString("NOTIFICATIONS", comment: "")
        var dicGetAlert:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicGetAlert["iUserId"] = Global.sharedInstance.currentUser.iUserId
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetAlertSettingsForCustomer(dicGetAlert, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == 1 // הצליח
            {
                Global.sharedInstance.customerAlertsSettingsObj = Global.sharedInstance.customerAlertsSettingsObj.getFromDic(responseObject["Result"] as! Dictionary<String,AnyObject>)
                self.tblNotifications.reloadData()
            }
            self.generic.hideNativeActivityIndicator(self)
            
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        })
        }
        
        tblNotifications.separatorStyle = .None
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayHeaders.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("notificationForDefinationsTableViewCell")as!notificationForDefinationsTableViewCell
        cell.selectionStyle = .None
        cell.tag = indexPath.row
        cell.setDisplayData(arrayHeaders[indexPath.row],select:ItemToBool(indexPath.row))
        if indexPath.row != 4
        {
            cell.viewButtom.hidden = true
        }
        else
        {
            cell.viewButtom.hidden = false
        }
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tblNotifications.frame.size.height/5
    }
    
    //מקבלת אינדקס של השורה בטבלה ומחזירה את הערך(true או false)למשתנה הזה
    func ItemToBool(indexPath:Int) -> Bool
    {
        switch indexPath {
        case 0:
            return Global.sharedInstance.customerAlertsSettingsObj.b90thAlertTime
            
        case 1:
            return Global.sharedInstance.customerAlertsSettingsObj.b20minutesBeforeService
            
        case 2:
            return Global.sharedInstance.customerAlertsSettingsObj.bPermissionsFromBusinesses
            
        case 3:
            return Global.sharedInstance.customerAlertsSettingsObj.bOrderInWaiting
            
        case 4:
            return Global.sharedInstance.customerAlertsSettingsObj.bUpdatesAndNews
            
        default:
            return true
        }
    }
}
