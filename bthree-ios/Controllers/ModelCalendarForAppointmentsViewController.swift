//
//  ModelCalendarForAppointmentsViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/22/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol selectWorkerDelegate{
    
    func selectWorker(text:String)
}
protocol openFromMenuDelegate
{
    func openFromMenu(con:UIViewController)
}
protocol setDateDelegate {
    func setDateClick(date:NSDate)
}
protocol openMonthDelegte {
    func openMonthFromChooseWorker()
}

protocol goToRegisterDelegate {
    func goToRegister()
}
//מודל תצוגת יומנים - של ספק שהלקוח רואה
class ModelCalendarForAppointmentsViewController: NavigationModelViewController,selectWorkerDelegate,openFromMenuDelegate,UIGestureRecognizerDelegate,clickToDayDelegate,clickToDayInWeekDelegate,openDetailsOrderDelegate,openMonthDelegte,goToRegisterDelegate{

    var delegateSetDate:setDateDelegate!=nil
    var delegateReload:reloadTblDelegate!=nil
    var delegate:hideTableDelegate!=nil
    var HeightView: CGFloat  = CGFloat()
    var storyboard1:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    @IBOutlet weak var lblChooseWorker: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblLogo: UIImageView!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    var btnDayClick:UIButton = UIButton()
    var x = 0
    var view1 : MonthClientForAppointmentDesignViewController!
    var view3 :weekClientForAppointmentDesign12ViewController!//weekClientForAppointmentDesignViewController!
    var view4 :dayClientForAppointment12ViewController!//dayClientForAppointmentDesignViewController!
    var attributedString = NSMutableAttributedString(string:"")
    var subView = UIView()
    var selectedView:UIViewController = UIViewController()
    var filterSubArr:Array<String> = []
    let generic:Generic = Generic()
    
   
    var isAuto = false
    
    var viewCon:ChooseWorkerTableViewController!
    
    @IBOutlet var imgPlusMenu: UIImageView!
    var attrs = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Bold", size: 23)!,
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSUnderlineStyleAttributeName : 1]
    var attrsDeselect = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 23)!]
    @IBOutlet var imgMenu: UIImageView!
    //לחיצה על הלשונית חודש
    @IBAction func btnMonth(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view1
        {
            Calendar.sharedInstance.carrentDate = NSDate()
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForMonthly()
            selectedView.view.removeFromSuperview()
            self.view.addSubview(view1.view)
            selectedView = view1
        }
        else
        {
            sender.tag = 0
            sender.backgroundColor = Colors.sharedInstance.color7
            DeSelectDesighnedBtn(sender)
            //selectedView = view2
        }
        
    }
    @IBOutlet var btnMonth: UIButton!
    //לחיצה על הלשונית שבוע
    @IBAction func btnWeek(sender: UIButton)
    {
        if sender.tag == 0 || selectedView == view3
        {
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForWeek()
            
            view3 = self.storyboard?.instantiateViewControllerWithIdentifier("weekClientForAppointmentDesign12ViewController") as! weekClientForAppointmentDesign12ViewController
            view3.delegate = self
            

            if selectedView == view1//חודש
            {
                Global.sharedInstance.currDateSelected = NSDate()
                view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
                
            }
            else//יום
            {
                view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
            }
            
            view3.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
            
            selectedView.view.removeFromSuperview()
            let currentDate:NSDate = Global.sharedInstance.currDateSelected
            let dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!

            //אתחול מערך השעות הפנויות לשבוע
            for i in 0 ..< 7 {
                
                let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                
                Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
                Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
            }

            self.view.addSubview(view3.view)
            
            selectedView = view3
        }
        else{
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color8
        }
    }
    @IBOutlet var btnWeek: UIButton!
    //לחיצה על הלשונית יום
    @IBAction func btnDay(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view4{
            SelectDesighnedBtn(sender)
            
            sender.tag = 1
            btnDayClick = sender
            SelectSingleForDay()
            selectedView.view.removeFromSuperview()
            
            /////st‎
            //כדי שאם לוחץ על על ״יום״ אחרי שלחץ על יום מסויים -שלא יציג בתצוגת יום לפי היום ההוא אלא לפי התאריך של היום
            Global.sharedInstance.dateDayClick = NSDate()
            Global.sharedInstance.currDateSelected = NSDate()
            
            //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
            view4.initDate(Global.sharedInstance.currDateSelected)
            self.view.addSubview(view4.view)
            selectedView = view4
            
            //העתקתי מהפונקציה  ToDay 3 השורות האלה היו שם וכאן לא
            self.delegateSetDate = Global.sharedInstance.dayDesigncalendarAppointment12
            delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
        }
        else{
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color9
        }
    }
    @IBOutlet var btnDay: UIButton!
    
    @IBOutlet weak var btnOpenTbl: UIButton!
    
    //MARK: - Initial
    
    override func viewDidLoad()
    {
        btnMonth.setTitle(NSLocalizedString("DESIGN_MONTH", comment: ""), forState: .Normal)
        btnWeek.setTitle(NSLocalizedString("DESIGN_WEEK", comment: ""), forState: .Normal)
        btnDay.setTitle(NSLocalizedString("DESIGN_DAY", comment: ""), forState: .Normal)
        
        changeSizeAdjastDevice()
        HeightView = 90
        super.viewDidLoad()
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
         HeightView = 70
        }

        if Global.sharedInstance.giveServiceName != ""{
        lblChooseWorker.text = Global.sharedInstance.giveServiceName
        }
        else
        {
            lblChooseWorker.text = NSLocalizedString("CHOOSE_GIVE_SERVICE", comment: "")
        }
        
        Global.sharedInstance.isSyncWithGoogleCalendarAppointment = Global.sharedInstance.currentUser.bIsGoogleCalendarSync
        
        Global.sharedInstance.model = 2//סימן שהגעתי ממודל של היומן תורים מול לקוח
        Global.sharedInstance.whichReveal = false
        btnOpenTbl.addTarget(self, action: #selector(ModelCalendarForAppointmentsViewController.imageTappedWorkersMenu), forControlEvents: UIControlEvents.TouchUpInside)

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ModelCalendarForAppointmentsViewController.imageTapped))
        imgPlusMenu.userInteractionEnabled = true
        imgPlusMenu.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerForWorkers = UITapGestureRecognizer(target:self, action:#selector(ModelCalendarForAppointmentsViewController.imageTappedWorkersMenu))
        imgMenu.userInteractionEnabled = true
        imgMenu.addGestureRecognizer(tapGestureRecognizerForWorkers)
        
        subView.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + self.navigationController!.navigationBar.frame.size.height , view.frame.width, view.frame.height * 0.2)
        view1 = self.storyboard?.instantiateViewControllerWithIdentifier("MonthClientForAppointmentDesignViewController") as! MonthClientForAppointmentDesignViewController
        view1.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - HeightView)
        view1.delegate = self
      //  view1.view.backgroundColor  = UIColor.redColor()
      //  self.delegateFreeDays = view1

        view3 = self.storyboard?.instantiateViewControllerWithIdentifier("weekClientForAppointmentDesign12ViewController") as! weekClientForAppointmentDesign12ViewController
         view3.delegate = self
        view3.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - HeightView)
        
        view4 = self.storyboard?.instantiateViewControllerWithIdentifier("dayClientForAppointment12ViewController") as! dayClientForAppointment12ViewController //self.storyboard?.instantiateViewControllerWithIdentifier("dayClientForAppointmentDesignViewController") as! dayClientForAppointmentDesignViewController
        view4.initDate(Global.sharedInstance.currDateSelected)
        view4.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - HeightView)
        Global.sharedInstance.dayDesigncalendarAppointment12 = view4
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ModelCalendarForAppointmentsViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        if Global.sharedInstance.isCancelAppointmentClick == true//לחצו על ביטול תור
        {
            Global.sharedInstance.isCancelAppointmentClick = false
            if Global.sharedInstance.whichDesignOpenDetailsAppointment == 1//הגיעו מיום
            {
                SelectDesighnedBtn(btnDay)
                btnDay.tag = 1
                btnDayClick = btnDay
                SelectSingleForDay()
                selectedView.view.removeFromSuperview()
                //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
                view4.initDate(Global.sharedInstance.currDateSelected)
                self.view.addSubview(view4.view)
                selectedView = view4
                //העתקתי מהפונקציה  ToDay 3 השורות האלה היו שם וכאן לא
                self.delegateSetDate = Global.sharedInstance.dayDesigncalendarAppointment12
                delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
            }
            else if Global.sharedInstance.whichDesignOpenDetailsAppointment == 2//הגיעו משבוע
            {
                SelectDesighnedBtn(btnWeek)
                btnWeek.tag = 1
                SelectSingleForWeek()
                selectedView.view.removeFromSuperview()
//מחדש יוצרים אותו כדי שיגיע שוב ל-didLoad- שיאתחל את התאריך לפי התאריך שלחץ וכן את הימים המאופשרים
                view3 = self.storyboard?.instantiateViewControllerWithIdentifier("weekClientForAppointmentDesign12ViewController") as! weekClientForAppointmentDesign12ViewController
                view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
                view3.delegate = self
                view3.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - HeightView)
                self.view.addSubview(view3.view)
                selectedView = view3
            }
            else
            {
                self.view.addSubview(view1.view)
                selectedView = view1
                SelectDesighnedBtn(btnMonth)
            }
        }
        else
        {
            self.view.addSubview(view1.view)
            selectedView = view1
            SelectDesighnedBtn(btnMonth)
        }
        
        viewCon = self.storyboard!.instantiateViewControllerWithIdentifier("ChooseWorkerTableViewController") as! ChooseWorkerTableViewController
        self.delegateReload = viewCon
    viewCon.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * HeightView) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)

        Global.sharedInstance.calendarAppointment = self
        btnDayClick.titleLabel?.text = "יום"
        lblName.text = Global.sharedInstance.currentProviderToCustomer.nvProviderName
        lblDescription.text = Global.sharedInstance.currentProviderToCustomer.nvProviderSlogan
        
        var decodedimage:UIImage = UIImage()
        let dataDecoded:NSData = NSData(base64EncodedString: (Global.sharedInstance.currentProviderToCustomer.nvProviderLogo), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            lblLogo.image = decodedimage
            lblLogo.contentMode = .ScaleAspectFill
        }
        else
        {
            lblLogo.backgroundColor = Colors.sharedInstance.color4
            lblLogo.image = UIImage(named: "clients@x1.png")
            lblLogo.contentMode = .ScaleAspectFit
        }
    }
    
    @IBOutlet weak var viewContactMan: UIView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        lblName.text = Global.sharedInstance.currentProviderToCustomer.nvProviderName
        lblDescription.text = Global.sharedInstance.currentProviderToCustomer.nvProviderSlogan
    }

    func imageTappedWorkersMenu(){
        if x == 0{

    viewCon.view.frame = CGRectMake(viewContactMan.frame.origin.x, btnWeek.frame.origin.y, viewContactMan.frame.width ,self.view.frame.size.height * 0.2)
            viewCon.tableView.reloadData()
            self.view.addSubview(viewCon.view)
            
            
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.delegate = viewCon
            viewCon.delegate = self
            x = 1
        }
        else{
            
            delegate.hideTable()
            x = 0
        }
    }
    
    func imageTapped(){
        
        Global.sharedInstance.currentOpenedMenu = self
        
        let viewCon:MenuPlusViewController = storyBoard1?.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    //פונקציה זו מקבלת כפתור ועושה לו תצוגה בחורה ז״א קו מתחת וכתב מודגש
    func SelectDesighnedBtn(btn:UIButton){
        //btn.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 23)
        btn.backgroundColor = UIColor.clearColor()
        //  btn.titleLabel?.font = ui
        underlineButton(btn, text: (btn.titleLabel?.text)!)
    }
    
    func underlineButton(button : UIButton, text: String) {
        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:text, attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        button.setAttributedTitle(attributedString, forState: .Normal)
    }
    //פונקציה זו מקבלת כפתור והופכת אותו מתצוגה בחורה לתצוגה לא בחורה
    func DeSelectDesighnedBtn(btn:UIButton){
        //btn.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 23)
        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string: (btn.titleLabel?.text)!, attributes:attrsDeselect)
        attributedString.appendAttributedString(buttonTitleStr)
        btn.setAttributedTitle(attributedString, forState: .Normal)
        //btn.titleLabel?.font = UIFont.preferredFontForTextStyle(uifontt)
        //btn.backgroundColor = UIColor.clearColor()
        //  btn.titleLabel?.font = ui
        //underlineButton(btn, text: (btn.titleLabel?.text)!)
    }
    //ברגע שלוחצים על תצוגה מסוימת שרק היא תהיה בחורה בפונקציה זו זה בלחיצה על חודש  אח״כ יש גם בעת לחיצה על שבוע או יום
    func SelectSingleForMonthly(){
        btnWeek.tag = 0
        DeSelectDesighnedBtn(btnWeek)
        btnWeek.backgroundColor = Colors.sharedInstance.color8
        btnDay.tag = 0
        btnDay.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(btnDay)
    }
    
    func SelectSingleForWeek(){
        btnMonth.tag = 0
        btnMonth.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(btnMonth)
        
        btnDay.tag = 0
        btnDay.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(btnDay)
    }
    
    func SelectSingleForDay(){
        btnMonth.tag = 0
        btnMonth.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(btnMonth)
        btnWeek.tag = 0
        DeSelectDesighnedBtn(btnWeek)
        btnWeek.backgroundColor = Colors.sharedInstance.color8
    }
    
    func dismissKeyboard() {
        if x == 1
        {
            delegate.hideTable()
            x = 0
        }
        self.view.endEditing(true)
    }
    
    func selectWorker(text:String)
    {
        Global.sharedInstance.giveServiceName = text
        lblChooseWorker.text = text
        x = 1
        imageTappedWorkersMenu()
    }
    
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
     func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(self.viewCon.view)) {
            
            return false
        }
        return true
    }

    //enter to current date
    func clickToDay(){//open the day design when click one day in the month design
        
        generic.showNativeActivityIndicator(self)
        SelectDesighnedBtn(btnDay)
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        self.delegateSetDate = Global.sharedInstance.dayDesigncalendarAppointment12
        
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)
        self.view.addSubview(view4.view)
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
        
        generic.hideNativeActivityIndicator(self)
    }
     //enter to current date in week design
    func clickToDayInWeek()
    {
        SelectDesighnedBtn(btnDay)
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)

        self.view.addSubview(view4.view)

        self.delegateSetDate = Global.sharedInstance.dayDesigncalendarAppointment12 
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
        
    }

    //openDetailsOrderDelegate
    func openDetailsOrder(tag:Int)  {//פונקציה דליגטית שנקראת בעת לחיצה על רבוע פנוי או תפוס(לפי הטאג) ביומן ופותחת את פרטי ההזמנה במודל
       
        let storyboard = UIStoryboard(name: "ClientExist", bundle: nil)

        let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyboard.instantiateViewControllerWithIdentifier("detailsAppointmetClientViewController") as! detailsAppointmetClientViewController
        vc.tag = tag
        vc.fromViewMode = false
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
      }
  
    func changeSizeAdjastDevice()
    {
        lblChooseWorker.font = UIFont(name: (lblChooseWorker.font?.fontName)!, size: 13.0)
    }
    
    func openMonthFromChooseWorker()
    {
        Calendar.sharedInstance.carrentDate = NSDate()
        SelectDesighnedBtn(btnMonth)
        btnMonth.tag = 1
        SelectSingleForMonthly()
        selectedView.view.removeFromSuperview()
        self.view.addSubview(view1.view)
        selectedView = view1
    }
    
    //בלחיצה על תור פנוי ממצב צפיה מוצגת הודעה אם מעונין להרשם כלקוח ומעבירו להרשמה
    func goToRegister()
    {
        let alert = UIAlertController(title: "", message:
            NSLocalizedString(NSLocalizedString("REGISTER_NOW", comment: ""), comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
            
            Global.sharedInstance.isFromViewMode = true
            let viewCon = self.storyboard1.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.navigationController?.pushViewController(viewCon, animated: false)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Cancel , handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //מחזיר את ה-string לשורה מסויימת מהטבלה שנבחרה
    //מקבלת את קוד הטבלה אליה לגשת ואת קוד ה-string
    func SysTableRowString(iTableRowId:Int,id:Int)->String
    {
        for sys in Global.sharedInstance.dicSysAlerts[iTableRowId.description]!
        {
            if sys.iTableId == iTableRowId && sys.iSysTableRowId == id
            {
                return sys.nvAletName
            }
        }
        return ""
    }
}
