//
//  PopUpDetailsAppointmentViewController.swift
//  Bthere
//
//  Created by Racheli Kroiz on 1.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

//פופאפ מלחיצה על אישור בהזמנת תור-הצגת פרטי התור ומודיע על שליחת התור לשרת
class PopUpDetailsAppointmentViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var txtView: UITextView!
    
    //MARK: - Variables
    
    var delegate:openCustomerDelegate!=nil
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //tap on the page dismiss it
        let touchOnView:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dimissViewController))
        self.view.addGestureRecognizer(touchOnView)
        //אם לא לוחצים על הויו זה יורד לאחר מספר שניות
        let delay = 3.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            self.dimissViewController()
        })
        
        if Global.sharedInstance.giveServiceName == "" || Global.sharedInstance.giveServiceName == NSLocalizedString("NOT_CARE", comment: "")
        {
            txtView.text = "\(NSLocalizedString("THX", comment: "")) \(Global.sharedInstance.currentUser.nvFirstName),\n\(NSLocalizedString("ASK_SEND_TO_GIVE_SERVICE", comment: ""))"
        }
        else
        {
            
            txtView.text = "\(NSLocalizedString("THX", comment: "")) \(Global.sharedInstance.currentUser.nvFirstName),\n\(NSLocalizedString("ASK_SEND", comment: ""))\(Global.sharedInstance.giveServiceName) \(NSLocalizedString("TO_OK", comment: ""))"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func to dismiss the popUp
    func dimissViewController()  {
        self.dismissViewControllerAnimated(false, completion: nil)
        delegate.openCustomer()
    }

}
