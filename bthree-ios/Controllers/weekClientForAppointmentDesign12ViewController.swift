//
//  weekClientForAppointmentDesign12ViewController.swift
//  Bthere
//
//  Created by User on 15.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
//תצוגת שבוע של יומן ספק שהלקוח רואה

protocol enterOnDayDelegate {
    func enterOnDay(tag:Int)
}

class weekClientForAppointmentDesign12ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,enterOnDayDelegate {

    var delegate:clickToDayInWeekDelegate!
    //MARK: - Outlet
    @IBOutlet var btnDay1: UIButton!
    @IBOutlet var btnDay2: UIButton!
    @IBOutlet var btnDay3: UIButton!
    @IBOutlet var btnDay4: UIButton!
    @IBOutlet var btnDay5: UIButton!
    @IBOutlet var btnDay6: UIButton!
    @IBOutlet var btnDay7: UIButton!

    var  arrayDays: NSMutableArray = NSMutableArray()
    var  arrayButtons: Array<UIButton> = Array<UIButton>()
    var  arrayLabelsDayNum: Array<UILabel> = Array<UILabel>()
    var  arrayLabelsdate: Array<UILabel> = Array<UILabel>()
    var monthArray:Array<Int> = Array<Int>()
    let generic:Generic = Generic()
    var firstViewInFreeHour:Int = -1//הטג של הויו הראשון בשעה פנויה, עליו נציג את השעות
    
    var hightCell:CGFloat = 0
    var hightViewBlue:CGFloat = 0//גובה של הוי הצבוע שמראה על התור הפנוי
    var hightViewClear:CGFloat = 0
    var minute:Int = 0
    var minute1:Int = 0
    var isShowFreeDay:Int = 0//flag to know if there isnt any free hour in that hour


    @IBOutlet weak var ingTrailing: NSLayoutConstraint!
    @IBOutlet weak var imgCurrentDay: UIImageView!
    
    @IBOutlet weak var lblDay1: UILabel!
    
    @IBOutlet weak var lblDay2: UILabel!
    
    @IBOutlet weak var lblDay3: UILabel!
    
    @IBOutlet weak var lblDay4: UILabel!
    
    @IBOutlet weak var lblDay5: UILabel!
    
    @IBOutlet weak var lblDay6: UILabel!
    
    @IBOutlet weak var lblDay7: UILabel!
    
    
    @IBOutlet weak var lblDayOfWeek1: UILabel!
    
    @IBOutlet weak var lblDayOfWeek2: UILabel!
    
    @IBOutlet weak var lblDayOfWeek3: UILabel!
    
    @IBOutlet weak var lblDayOfWeek4: UILabel!
    
    @IBOutlet weak var lblDayOfWeek5: UILabel!
    
    @IBOutlet weak var lblDayOfWeek6: UILabel!
    
    @IBOutlet weak var lblDayOfWeek7: UILabel!
    
    @IBOutlet weak var lblDays: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    //MARK: - IBAction
    // if eye design is set on  need to show device event
    
    @IBAction func btnSync(sender: eyeSynCheckBox) {
        
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = true
            collWeek.reloadData()
        }
        else
        {
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = false
            collWeek.reloadData()
        }
        
    }
    // onClick on the spech day in week
    @IBAction func btnEnterDateClick(sender: AnyObject) {
        enterOnDay(sender.tag)
    }

    
    //הקודם  (ולא הבא)
    @IBAction func btnNext(sender: AnyObject) {
        //בלחיצה על הקודם מוצגים הימים בשבוע הקודם
        checkDevice()
        hasEvent = false
        /////////////
        let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
        
        let otherDate:NSDate = currentDate
        
        //show month for each day in week
        lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -1).description
        lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -2).description
        lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -3).description
        lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -4).description
        lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -5).description
        lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -6).description
        lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -7).description
        
        currentDate =  Calendar.sharedInstance.reduceAddDay_Date(otherDate, reduce: day, add: -7)
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
        monthToday = componentsCurrent.month
        
        
        
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -1))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -2))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -3))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -4))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -5))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -6))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -7))
        
        

        lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"//change 13.10.2016

        var monthName:String = ""
        //get the month and day of week names - short
        if monthToday == 0
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
        }
        else
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
        }
//        lblDate.text = "\(monthName) \(yearToday)"
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        //אתחול המערך של תאריכי השבוע
        datesInWeekArray = []
        for i in 0..<7
        {
            datesInWeekArray.append(
                Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
        }
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        //אתחול מערך השעות הפנויות לשבוע
        generic.showNativeActivityIndicator(self)
        
        for i in 0 ..< 7 {
            
            let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
            
            Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
            Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
        }
        
        self.generic.hideNativeActivityIndicator(self)
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        var  isFindToday:Bool = false
        //בדיקה האם היום הנוכחי מוצג כעת
        for item in datesInWeekArray
        {
            let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)

            let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
            if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
            {
                isFindToday = true
            }
        }
        if isFindToday
        {
            imgCurrentDay.hidden = false
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
            UIView.animateWithDuration(1, animations: {
                self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                    CGFloat(self.dayOfWeekToday))
            })
        }
        else
        {
            imgCurrentDay.hidden = true
        }
        setEventsArray(currentDate)
        lblDate.text = "\(monthName) \(yearToday)"
        setDateEnablity()
        
        collWeek.reloadData()
    }
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    //הבא (ולא הקודם)
    @IBAction func btnPrevious(sender: AnyObject) {
        //בלחיצה על הבא מוצגים הימים בשבוע הבא
        hasEvent = false
        /////////////
        checkDevice()
        
        let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
        
        let otherDate:NSDate = currentDate
        
        //show month for each day in week
        lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 13).description
        lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 12).description
        lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 11).description
        lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 10).description
        lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 9).description
        lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 8).description
        lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 7).description
        
        currentDate =  Calendar.sharedInstance.reduceAddDay_Date(otherDate, reduce: day, add: 13)
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
        monthToday = componentsCurrent.month
        
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 13))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 12))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 11))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 10))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 9))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 8))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 7))
        
       lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"
        
        var monthName:String = ""
        //get the month and day of week names - short
        if monthToday == 0
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
        }
        else
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
        }
        
//        lblDate.text = "\(monthName) \(yearToday)"
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        //אתחול המערך של תאריכי השבוע
        datesInWeekArray = []
        for i in 0..<7
        {
            datesInWeekArray.append(
                Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
        }
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        //אתחול מערך השעות הפנויות לשבוע
        generic.showNativeActivityIndicator(self)
        for i in 0 ..< 7 {
            
            let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
            
            Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
            Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
        }
        generic.hideNativeActivityIndicator(self)

        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        var  isFindToday:Bool = false
        //בדיקה האם היום הנוכחי מוצג כעת
        for item in datesInWeekArray
        {
            let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)
            
            
            let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
            if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
            {
                
                isFindToday = true
                
            }
        }
        if isFindToday
        {
            imgCurrentDay.hidden = false
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
            UIView.animateWithDuration(1, animations: {
                self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                    CGFloat(self.dayOfWeekToday))
            })
        }
        else{
            imgCurrentDay.hidden = true
        }
        
        setEventsArray(currentDate)// set all event in spech date
        lblDate.text = "\(monthName) \(yearToday)"
        setDateEnablity() //set enabelity design if date passed
        collWeek.reloadData() //refresh
    }
    
    @IBOutlet weak var collWeek: UICollectionView!
    
    //MARK: - Properties
    
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var arrHoursInt:Array<Int> = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    var arrHours:Array<String> = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","7:00","8:00",
                                  "9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"]
    
    var arrEventsCurrentDay:Array<EKEvent> = []
    var flag = false
    var hasEvent = false
    var currentDate:NSDate = NSDate()
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var dayOfWeekToday = 0
    var datesInWeekArray:Array<NSDate> = Array<NSDate>()//array of nsdate in week
    
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSync: eyeSynCheckBox!

    //MARK: - Initial
    //func that get date to init the design by date
    func initDateOfWeek(date:NSDate)
    {
        datesInWeekArray = []
        currentDate = date
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        for var i in 0..<7
        {
            datesInWeekArray.append(
                Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
        }
    }
    //MARK: - Initials Function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkDevice()
        imgCurrentDay.hidden = true

//        if let  d:NSDate = Global.sharedInstance.currDateSelected
//        {
//            currentDate =  Global.sharedInstance.currDateSelected
//        }
        
        if Global.sharedInstance.isSyncWithGoogleCalendarAppointment == true
        {
            btnSync.isCecked = true
        }
        else
        {
            btnSync.isCecked = false
        }
        
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        
        hasEvent = false
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
    
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        collWeek.transform = scalingTransform

        
        setEventsArray(currentDate)
        setDate()
        arrayDays = [Int(lblDay1.text!)!,Int(lblDay2.text!)!,Int(lblDay3.text!)!,Int(lblDay4.text!)!,Int(lblDay5.text!)!,Int(lblDay6.text!)!,Int(lblDay7.text!)!]

        arrayButtons.append(btnDay1)
        arrayButtons.append(btnDay2)
        arrayButtons.append(btnDay3)
        arrayButtons.append(btnDay4)
        arrayButtons.append(btnDay5)
        arrayButtons.append(btnDay6)
        arrayButtons.append(btnDay7)

        arrayLabelsDayNum.append(lblDayOfWeek1)
        arrayLabelsDayNum.append(lblDayOfWeek2)
        arrayLabelsDayNum.append(lblDayOfWeek3)
        arrayLabelsDayNum.append(lblDayOfWeek4)
        arrayLabelsDayNum.append(lblDayOfWeek5)
        arrayLabelsDayNum.append(lblDayOfWeek6)
        arrayLabelsDayNum.append(lblDayOfWeek7)
        
        arrayLabelsdate.append(lblDay1)
        arrayLabelsdate.append(lblDay2)
        arrayLabelsdate.append(lblDay3)
        arrayLabelsdate.append(lblDay4)
        arrayLabelsdate.append(lblDay5)
        arrayLabelsdate.append(lblDay6)
        arrayLabelsdate.append(lblDay7)
        
        setDateEnablity()
        imgCurrentDay.hidden = true
        
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        monthArray = []
        Global.sharedInstance.getEventsFromMyCalendar()
        
        if Global.sharedInstance.isSyncWithGoogleCalendarAppointment == true
        {
            btnSync.isCecked = true
        }
        else
        {
            btnSync.isCecked = false
        }
        
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        
        hasEvent = false
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        setEventsArray(currentDate)
        setDate()
        arrayDays = [Int(lblDay1.text!)!,Int(lblDay2.text!)!,Int(lblDay3.text!)!,Int(lblDay4.text!)!,Int(lblDay5.text!)!,Int(lblDay6.text!)!,Int(lblDay7.text!)!]
        
        arrayButtons.append(btnDay1)
        arrayButtons.append(btnDay2)
        arrayButtons.append(btnDay3)
        arrayButtons.append(btnDay4)
        arrayButtons.append(btnDay5)
        arrayButtons.append(btnDay6)
        arrayButtons.append(btnDay7)
        
        arrayLabelsDayNum.append(lblDayOfWeek1)
        arrayLabelsDayNum.append(lblDayOfWeek2)
        arrayLabelsDayNum.append(lblDayOfWeek3)
        arrayLabelsDayNum.append(lblDayOfWeek4)
        arrayLabelsDayNum.append(lblDayOfWeek5)
        arrayLabelsDayNum.append(lblDayOfWeek6)
        arrayLabelsDayNum.append(lblDayOfWeek7)
        
        arrayLabelsdate.append(lblDay1)
        arrayLabelsdate.append(lblDay2)
        arrayLabelsdate.append(lblDay3)
        arrayLabelsdate.append(lblDay4)
        arrayLabelsdate.append(lblDay5)
        arrayLabelsdate.append(lblDay6)
        arrayLabelsdate.append(lblDay7)
        
        setDateEnablity()
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        var  isFindToday:Bool = false
        for item in datesInWeekArray
        {
            let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)
            
            
            let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
            if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
            {
                
                isFindToday = true
                
            }
        }
        if isFindToday
        {
            imgCurrentDay.hidden = false
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
            UIView.animateWithDuration(1, animations: {
                self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                    CGFloat(self.dayOfWeekToday))
            })
        }
        else{
            imgCurrentDay.hidden = true
        }
    }
    // set enable design if date passed
    func setDateEnablity()
    {
        for var i in 0  ..< datesInWeekArray.count
        {
            if self.small(datesInWeekArray[i], rhs: NSDate())
            {
                (arrayButtons[6-i] as UIButton).enabled = false
                (arrayLabelsDayNum[6-i] as UILabel).textColor = Colors.sharedInstance.color7
                (arrayLabelsdate[6-i] as UILabel).textColor =  Colors.sharedInstance.color7
            }
                //st - כדי שירענן את הכפתורים ולא ישאיר לפי הקודם
            else
            {
                (arrayButtons[6-i] as UIButton).enabled = true
                (arrayLabelsDayNum[6-i] as UILabel).textColor =  UIColor.whiteColor()
                (arrayLabelsdate[6-i] as UILabel).textColor =  UIColor.whiteColor()
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - collectionView
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        isShowFreeDay = 0
        
        let cell:EventsForWeek12ViewsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("EventsForWeek12ViewsCollectionViewCell",forIndexPath: indexPath) as! EventsForWeek12ViewsCollectionViewCell
        
        cell.indexDayOfWeek = indexPath.row % 8 - 1
        cell.indexHourOfDay = indexPath.row / 8

        cell.view0to5.backgroundColor = UIColor.clearColor()
        cell.view5to10.backgroundColor = UIColor.clearColor()
        cell.view10to15.backgroundColor = UIColor.clearColor()
        cell.view15to20.backgroundColor = UIColor.clearColor()
        cell.view20to25.backgroundColor = UIColor.clearColor()
        cell.view25to30.backgroundColor = UIColor.clearColor()
        cell.view30to35.backgroundColor = UIColor.clearColor()
        cell.view35to40.backgroundColor = UIColor.clearColor()
        cell.view40to45.backgroundColor = UIColor.clearColor()
        cell.view45to50.backgroundColor = UIColor.clearColor()
        cell.view50to55.backgroundColor = UIColor.clearColor()
        cell.view55to60.backgroundColor = UIColor.clearColor()
        
        cell.delegateRegister = Global.sharedInstance.calendarAppointment
        
        
        for view in cell.view0to5.subviews
        {
            if view as? UILabel != cell.lblHours1
            {
            view.removeFromSuperview()
            }
        }
        for view in cell.view5to10.subviews
        {
            if view as? UILabel != cell.lblHours2
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view10to15.subviews
        {
            if view as? UILabel != cell.lblHours3
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view15to20.subviews
        {
            if view as? UILabel != cell.lblHours4
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view20to25.subviews
        {
            if view as? UILabel != cell.lblHours5
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view25to30.subviews
        {
            if view as? UILabel != cell.lblHours6
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view30to35.subviews
        {
            if view as? UILabel != cell.lblHours7
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view35to40.subviews
        {
            if view as? UILabel != cell.lblHours8
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view40to45.subviews
        {
            if view as? UILabel != cell.lblHours9
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view45to50.subviews
        {
            if view as? UILabel != cell.lblHours10
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view50to55.subviews
        {
            if view as? UILabel != cell.lblHours11
            {
                view.removeFromSuperview()
            }
        }
        for view in cell.view55to60.subviews
        {
            if view as? UILabel != cell.lblHours12
            {
                view.removeFromSuperview()
            }
        }

        cell.viewTopInTop.hidden = true
        cell.viewMiddleInTop.hidden = true
        cell.viewButtomInTop.hidden = true
        cell.viewButtominButtom.hidden = true
        cell.viewMiddleInButtom.hidden = true
        cell.viewTopInButtom.hidden = true
        
        
        cell.lblHours1.text = ""
        cell.lblHours2.text = ""
        cell.lblHours3.text = ""
        cell.lblHours4.text = ""
        cell.lblHours5.text = ""
        cell.lblHours6.text = ""
        cell.lblHours7.text = ""
        cell.lblHours8.text = ""
        cell.lblHours9.text = ""
        cell.lblHours10.text = ""
        cell.lblHours11.text = ""
        cell.lblHours12.text = ""
        cell.lblHours1.backgroundColor = UIColor.clearColor()
        cell.lblHours2.backgroundColor = UIColor.clearColor()
        cell.lblHours3.backgroundColor = UIColor.clearColor()
        cell.lblHours4.backgroundColor = UIColor.clearColor()
        cell.lblHours5.backgroundColor = UIColor.clearColor()
        cell.lblHours6.backgroundColor = UIColor.clearColor()
        cell.lblHours7.backgroundColor = UIColor.clearColor()
        cell.lblHours8.backgroundColor = UIColor.clearColor()
        cell.lblHours9.backgroundColor = UIColor.clearColor()
        cell.lblHours10.backgroundColor = UIColor.clearColor()
        cell.lblHours11.backgroundColor = UIColor.clearColor()
        cell.lblHours12.backgroundColor = UIColor.clearColor()
        

        
        
        cell.hasEvent =  false
        cell.hasBthereEvent = false
        cell.txtviewDesc.text = ""
        cell.txtViewDescBottom.text = ""
        cell.viewBottom.backgroundColor = UIColor.clearColor()
        cell.viewTop.backgroundColor = UIColor.clearColor()

        cell.delegateClickOnDay = self
        cell.delegate = Global.sharedInstance.calendarAppointment
        
        let cell1:HoursCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Hours",forIndexPath: indexPath) as! HoursCollectionViewCell
        
        

        cell.transform = scalingTransform
        cell1.transform = scalingTransform

        
        
        if indexPath.row == 0 || indexPath.row % 8 == 0
        {
            cell1.setDisplayData(arrHours[indexPath.row / 8])
            return cell1
        }
        else
        {
            
            var index:Int = 0
            hasEvent = false
            
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
            
            let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: indexPath.row % 8)
            
            setEventsArray(curDate)
            
            //מעבר על השעות הפנויות בשבוע זה
            for var item in Global.sharedInstance.freeHoursForWeek[(indexPath.row % 8) - 1]
            {
                firstViewInFreeHour = -1
                
                let hourStart = Global.sharedInstance.getStringFromDateString(item.objProviderHour.nvFromHour)
                
                let hourEnd = Global.sharedInstance.getStringFromDateString(item.objProviderHour.nvToHour)
                
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
                
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                
                if arrHoursInt[indexPath.row / 8] == hourS//אם שעת ההתחלה שווה לשעה המוצגת
                {
                    if hourE == hourS{//אם גם שעת הסיום שווה לשעה המוצגת
                        
                        showView(cell.view0to5, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view5to10, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view10to15, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view15to20, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view20to25, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view25to30, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view30to35, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view35to40, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view40to45, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view45to50, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view50to55, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view55to60, from: minuteS, to: minuteE, param: 0, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    }
                        
                        
                    else//שעת סיום שונה משעת ההתחלה
                    {
                        showView(cell.view0to5, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view5to10, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view10to15, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view15to20, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view20to25, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view25to30, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view30to35, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view35to40, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view40to45, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view45to50, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view50to55, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        showView(cell.view55to60, from: minuteS, to: minuteE, param: 1, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                        
                    }
                    isShowFreeDay = 1
                    //cell.hourFree = hourS_Show + ":" + minuteS_Show
                }
                else if arrHoursInt[indexPath.row / 8] > hourS && arrHoursInt[indexPath.row / 8] < hourE//אם הוא באמצע הארוע ז״א ששעת התחלה  קטנה מהשעה  המוצגת ושעת הסיום גדולה ש
                {
                    cell.view0to5.backgroundColor = Colors.sharedInstance.color4
                    cell.view0to5.alpha = 0.5
                    cell.lblHours1.tag = index
                    
                    cell.view5to10.backgroundColor = Colors.sharedInstance.color4
                    cell.view5to10.alpha = 0.5
                    cell.lblHours2.tag = index
                    
                    cell.view10to15.backgroundColor = Colors.sharedInstance.color4
                    cell.view10to15.alpha = 0.5
                    cell.lblHours3.tag = index
                    
                    cell.view15to20.backgroundColor = Colors.sharedInstance.color4
                    cell.view15to20.alpha = 0.5
                    cell.lblHours4.tag = index
                    
                    cell.view20to25.backgroundColor = Colors.sharedInstance.color4
                    cell.view20to25.alpha = 0.5
                    cell.lblHours5.tag = index
                    
                    cell.view25to30.backgroundColor = Colors.sharedInstance.color4
                    cell.view25to30.alpha = 0.5
                    cell.lblHours6.tag = index
                    
                    cell.view30to35.backgroundColor = Colors.sharedInstance.color4
                    cell.view30to35.alpha = 0.5
                    cell.lblHours7.tag = index
                    
                    cell.view35to40.backgroundColor = Colors.sharedInstance.color4
                    cell.view35to40.alpha = 0.5
                    cell.lblHours8.tag = index
                    
                    cell.view40to45.backgroundColor = Colors.sharedInstance.color4
                    cell.view40to45.alpha = 0.5
                    cell.lblHours9.tag = index
                    
                    cell.view45to50.backgroundColor = Colors.sharedInstance.color4
                    cell.view45to50.alpha = 0.5
                    cell.lblHours10.tag = index
                    
                    cell.view50to55.backgroundColor = Colors.sharedInstance.color4
                    cell.view50to55.alpha = 0.5
                    cell.lblHours11.tag = index
                    
                    cell.view55to60.backgroundColor = Colors.sharedInstance.color4
                    cell.view55to60.alpha = 0.5
                    cell.lblHours12.tag = index
                    
                    cell.indexDayOfWeek = (indexPath.row % 8) - 1
                    
                    isShowFreeDay = 1
                }
                else if arrHoursInt[indexPath.row / 8] == hourE//שעת סיום שווה לשעה המוצגת

                {
                    showView(cell.view0to5, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view5to10, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view10to15, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view15to20, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view20to25, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view25to30, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view30to35, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view35to40, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view40to45, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view45to50, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view50to55, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    showView(cell.view55to60, from: 0, to: minuteE, param: 2, index: index, cell: cell, indexDayOfWeek: indexPath.row % 8)
                    isShowFreeDay = 1
                }
                index += 1
            }
           //מעבר על האירועים של ביזר
            //////////////////
            for btEvent in Global.sharedInstance.bthereEventsForWeek[(indexPath.row % 8) - 1]
            {
                cell.hasBthereEvent = true
                let str:String = btEvent.objProviderServiceDetails[0].nvServiceName
                firstViewInFreeHour = -1
                
                let hourStart = Global.sharedInstance.getStringFromDateString(btEvent.nvFromHour)
                
                let hourEnd = Global.sharedInstance.getStringFromDateString(btEvent.nvToHour)
                
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
                
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                
                if arrHoursInt[indexPath.row / 8] == hourS//אם שעת ההתחלה שווה לשעה המוצגת
                {
                    if hourE == hourS{//אם גם שעת הסיום שווה לשעה המוצגת
                        
                        if minuteS < 5
                        {
                            
                            cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: true)
                        }
                        else if minuteS < 40
                        {
                           cell.setDisplayData(1, isTop: false, description: str,descTop: false, isBthereEvent: true)
                        }
                        addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 0)
                        addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 0)
                    }
                        
                        
                    else//שעת סיום שונה משעת ההתחלה
                    {
                        if minuteS < 5
                        {
                            
                            cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: true)
                        }
                        else if minuteS < 40
                        {
                            cell.setDisplayData(1, isTop: false, description: str,descTop: false, isBthereEvent: true)
                        }
                        addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view50to55, from: minuteS, to: minuteE, param: 1)
                        addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 1)
                        
                        
                    }
                    isShowFreeDay = 1
                    //cell.hourFree = hourS_Show + ":" + minuteS_Show
                }
                else if arrHoursInt[indexPath.row / 8] > hourS && arrHoursInt[indexPath.row / 8] < hourE//אם הוא באמצע הארוע ז״א ששעת התחלה  קטנה מהשעה  המוצגת ושעת הסיום גדולה ש
                {
                    if minuteS > 40
                    {
                    cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: true)
                    }
                    
                    addRightLeftBorder(cell.view0to5)
                    addRightLeftBorder(cell.view5to10)
                    addRightLeftBorder(cell.view10to15)
                    addRightLeftBorder(cell.view15to20)
                    addRightLeftBorder(cell.view20to25)
                    addRightLeftBorder(cell.view25to30)
                    addRightLeftBorder(cell.view30to35)
                    addRightLeftBorder(cell.view35to40)
                    addRightLeftBorder(cell.view40to45)
                    addRightLeftBorder(cell.view45to50)
                    addRightLeftBorder(cell.view50to55)
                    addRightLeftBorder(cell.view55to60)

                }
                else if arrHoursInt[indexPath.row / 8] == hourE//שעת סיום שווה לשעה המוצגת
                    
                {
                    if hourS == hourE - 1 && minuteS > 40
                    {
                        cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: true)
                    }
                    
                    
                    
                    addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view50to55, from: minuteS, to: minuteE, param: 2)
                    addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 2)
                }
                index += 1
            }
            //////////////////
//אירועים של המכשיר

            if hasEvent == true
            {
                //cell.hasEvent =  true
                
                dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
                
                let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: indexPath.row % 8)
                

                setEventsArray(curDate)

                if hasEvent == true && Global.sharedInstance.isSyncWithGoogleCalendarAppointment == true
                {
                     //מעבר על האירועים של המכשיר
                    for eve in arrEventsCurrentDay

                    {
                        cell.hasBthereEvent = false
                        let str:String = eve.title
                        firstViewInFreeHour = -1
                        
                        let componentsStart = calendar.components([.Hour, .Minute], fromDate: eve.startDate)
                        
                        let componentsEnd = calendar.components([.Hour, .Minute], fromDate: eve.endDate)
                        
                        let hourS = componentsStart.hour //hour start
                        let minuteS = componentsStart.minute// minute start
                        
                        let hourE = componentsEnd.hour//hour end
                        
                        let minuteE = componentsEnd.minute//minute end
                        
                        var hourS_Show:String = hourS.description
                        var hourE_Show:String = hourE.description
                        var minuteS_Show:String = minuteS.description
                        var minuteE_Show:String = minuteE.description
                        
                        if hourS < 10 //for single hour that show 09
                        {
                            hourS_Show = "0\(hourS)"
                        }
                        if hourE < 10
                        {
                            hourE_Show = "0\(hourE)"
                        }
                        if minuteS < 10
                        {
                            minuteS_Show = "0\(minuteS)"
                        }
                        if minuteE < 10
                        {
                            minuteE_Show = "0\(minuteE)"
                        }

                        
                        if arrHoursInt[indexPath.row / 8] == hourS//אם שעת ההתחלה שווה לשעה המוצגת
                        {
                            if hourE == hourS{//אם גם שעת הסיום שווה לשעה המוצגת
                                
                                if minuteS < 5
                                {
                                    
                                    cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: false)
                                }
                                else if minuteS < 40
                                {
                                    cell.setDisplayData(1, isTop: false, description: str,descTop: false, isBthereEvent: false)
                                }
                                addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 0)
                                addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 0)
                            }
                                
                                
                            else//שעת סיום שונה משעת ההתחלה
                            {
                                if minuteS < 5
                                {
                                    
                                    cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: false)
                                }
                                else if minuteS < 40
                                {
                                    cell.setDisplayData(1, isTop: false, description: str,descTop: false, isBthereEvent: false)
                                }
                                addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view50to55, from: minuteS, to: minuteE, param: 1)
                                addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 1)
                                
                                
                            }
                            isShowFreeDay = 1
                            //cell.hourFree = hourS_Show + ":" + minuteS_Show
                        }
                        else if arrHoursInt[indexPath.row / 8] > hourS && arrHoursInt[indexPath.row / 8] < hourE//אם הוא באמצע הארוע ז״א ששעת התחלה  קטנה מהשעה  המוצגת ושעת הסיום גדולה ש
                        {
                            if minuteS > 40
                            {
                                cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: false)
                            }
                            
                            addRightLeftBorder(cell.view0to5)
                            addRightLeftBorder(cell.view5to10)
                            addRightLeftBorder(cell.view10to15)
                            addRightLeftBorder(cell.view15to20)
                            addRightLeftBorder(cell.view20to25)
                            addRightLeftBorder(cell.view25to30)
                            addRightLeftBorder(cell.view30to35)
                            addRightLeftBorder(cell.view35to40)
                            addRightLeftBorder(cell.view40to45)
                            addRightLeftBorder(cell.view45to50)
                            addRightLeftBorder(cell.view50to55)
                            addRightLeftBorder(cell.view55to60)
                            
                        }
                        else if arrHoursInt[indexPath.row / 8] == hourE//שעת סיום שווה לשעה המוצגת
                            
                        {
                            if hourS == hourE - 1 && minuteS > 40
                            {
                                cell.setDisplayData(1, isTop: true, description: str,descTop: true, isBthereEvent: false)
                            }
                            
                            
                            
                            addBorderToBtEvent(cell.view0to5, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view5to10, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view10to15, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view15to20, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view20to25, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view25to30, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view30to35, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view35to40, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view40to45, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view45to50, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view50to55, from: minuteS, to: minuteE, param: 2)
                            addBorderToBtEvent(cell.view55to60, from: minuteS, to: minuteE, param: 2)
                        }
                        index += 1
                    }
                    

                    
                }
            }
            else if isShowFreeDay == 0
            {
                
                cell.setDisplayData(0, isTop: false,description: "",descTop: true, isBthereEvent: false)
                
                return cell
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 192//8*24
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize(width: view.frame.size.width / 8, height:  view.frame.size.width / 9)
        
    }
    
    //MARK: - ScrollView
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        //       collWeek.reloadData()
        // flag = true
        
    }
    
    func setDate()
    {
        //the day of week from date - (int)
        let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
        
        let otherDate:NSDate = currentDate
        
        //show month for each day in week
        lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 6).description
        lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 5).description
        lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 4).description
        lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 3).description
        lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 2).description
        lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 1).description
        lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 0).description
        
        
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 6))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 5))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 4))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 3))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 2))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 1))
        monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 0))
        
        
        
lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"
                   var monthName:String = ""
        //get the month and day of week names - short
        if monthToday == 0
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
        }
        else
        {
            monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
        }
        // - long
        //NSDateFormatter().monthSymbols[monthToday - 1]
        //let dayName = NSDateFormatter().weekdaySymbols[day]
        
        lblDate.text = "\(monthName) \(yearToday)"
    }
    
    
    
    
    //פונקציה הבודקת האם לצבוע את הויו אותו היא מקבלת
    //מקבלת:
    //view: הויו אותו יש לצבוע
    //from,to:לשם הבדיקה האם נמצא בטווח של שעה פנויה - האם לצבוע
    //param: כאשר הוא 0 נבדוק האם נמצא בטווח של הדקות,
    //כאשר הוא 1 נבדוק האם גדול מ from
    //וכאשר הוא 2 נבדוק האם קטן מ to
    //index: המיקום של השעה הפנויה במערך

    func showView(view:UIView,from:Int,to:Int,param:Int, index:Int,cell:EventsForWeek12ViewsCollectionViewCell,indexDayOfWeek:Int)
    {
        cell.indexDayOfWeek = indexDayOfWeek - 1
        if param == 0
        {
            if view.tag >= from && view.tag <= to
            {
                view.backgroundColor = Colors.sharedInstance.color4
                view.alpha = 0.5
                
                saveFreeHourForView(view.tag, index: index, cell: cell)
                
                if firstViewInFreeHour == -1
                {
                    firstViewInFreeHour = view.tag
                }
            }
        }
        else if param == 1
        {
            if view.tag >= from
            {
                view.backgroundColor = Colors.sharedInstance.color4
                view.alpha = 0.5
                
                saveFreeHourForView(view.tag, index: index, cell: cell)
                
                if firstViewInFreeHour == -1
                {
                    firstViewInFreeHour = view.tag
                }
            }
        }
        else if param == 2
        {
            if view.tag <= to
            {
                view.backgroundColor = Colors.sharedInstance.color4
                view.alpha = 0.5
                saveFreeHourForView(view.tag, index: index, cell: cell)
            }
        }
    }
    
    func addBorderToBtEvent(view:UIView,from:Int,to:Int,param:Int)
    {
        if param == 0//בשעת התחלה הזהה לש.סיום
        {
            if view.tag >= from && view.tag <= to
            {
                addRightLeftBorder(view)

                if firstViewInFreeHour == -1
                {
                    firstViewInFreeHour = view.tag
                    addTopBorder(view)
                }
            }
        }
        else if param == 1//בשעת התחלה השונה משעת סיום
        {
            if view.tag >= from
            {
                addRightLeftBorder(view)
 
                if firstViewInFreeHour == -1
                {
                    firstViewInFreeHour = view.tag
                    addTopBorder(view)
                    
                }
            }
        }
        else if param == 2//בשעת סיום
        {
            if view.tag <= to
            {
                 addRightLeftBorder(view)
                if view.tag == to || (view.tag < to && (view.tag + 5) > to)
                {
                    addBottomBorder(view)
                }
            }
        }
    }
    
    //פונקציה זו שומרת בטג של הלייבל (1 מתוך 12) את המיקום במערך של השעה הפנויה בה הוא נמצא.
    //הפונקציה מקבלת טג של הויו כדי לזהות את הליבל וכן אינדקס של המערך.
    func saveFreeHourForView(tag:Int,index:Int, cell:EventsForWeek12ViewsCollectionViewCell)
    {
        switch tag {
        case 0:
            cell.lblHours1.tag = index
        case 5:
            cell.lblHours2.tag = index
        case 10:
            cell.lblHours3.tag = index
        case 15:
            cell.lblHours4.tag = index
        case 20:
            cell.lblHours5.tag = index
        case 25:
            cell.lblHours6.tag = index
        case 30:
            cell.lblHours7.tag = index
        case 35:
            cell.lblHours8.tag = index
        case 40:
            cell.lblHours9.tag = index
        case 45:
            cell.lblHours10.tag = index
        case 50:
            cell.lblHours11.tag = index
        case 55:
            cell.lblHours12.tag = index
            
        default:
            cell.lblHours1.tag = index
        }
        
    }

    func setEventsArray(today:NSDate)
    {
        arrEventsCurrentDay = []
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: today)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
    
    //check which date is prev
    internal func small(lhs: NSDate, rhs: NSDate) -> Bool {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        let isToday:Bool = calendar.isDateInToday(lhs);
        if isToday
        {
            return false
        }
        else
        {
            return lhs.compare(rhs) == .OrderedAscending
        }
    }
    //MARK: - Delegate To Enter date
    func enterOnDay(tag:Int)
    {
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
        
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        let td:Int =  arrayDays[tag] as! Int
        componentsCurrent.day = td
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        let dateSelected = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday , add: 7 - tag)
        
        Global.sharedInstance.currDateSelected = dateSelected
        Global.sharedInstance.dateDayClick = dateSelected
        delegate.clickToDayInWeek()
    }
    // if eye design is set on  need to show device event

    func showSync()
    {
        if btnSync.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSync.isCecked = true
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = true
            collWeek.reloadData()
        }
        else
        {
            btnSync.isCecked = false
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = false
            collWeek.reloadData()
        }
    }
    //change design if devise is smaller than 6
    func checkDevice()
    {
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            let fontSize:CGFloat = self.lblDate.font.pointSize;
            lblDays.font = UIFont(name: lblDays.font.fontName, size: 22)
            lblDate.font = UIFont(name: lblDate.font.fontName, size: 17)
            
        }
    }
    
//MARK: - borders
    //הוספת מסגרת לאירוע
    
    func addRightLeftBorder(myView:UIView)
    {
        let leftBorder = UIView()
        leftBorder.frame = CGRectMake(0, 0, 2, myView.frame.size.height)
        leftBorder.backgroundColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)//Colors.sharedInstance.color1
        myView.addSubview(leftBorder)
        
        let rightBorder = UIView()
        rightBorder.frame = CGRectMake(view.frame.size.width / 8 - 2, 0, 2, myView.frame.size.height)
        rightBorder.backgroundColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)//Colors.sharedInstance.color1
        myView.addSubview(rightBorder)
    }
    
    func addBottomBorder(myView:UIView)
    {
        let bottomBorder = UIView()
        bottomBorder.backgroundColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)//Colors.sharedInstance.color1
        bottomBorder.frame = CGRectMake(0, myView.frame.size.height - 2, view.frame.size.width - (view.frame.size.width / 8), 2)
        myView.addSubview(bottomBorder)
        
    }
    
    func addTopBorder(myView:UIView)
    {
        let topBorder = UIView()
        topBorder.backgroundColor = UIColor(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)//Colors.sharedInstance.color1
        topBorder.frame = CGRectMake(0, 0, view.frame.size.width - (view.frame.size.width / 8), 2)
        myView.addSubview(topBorder)

        
    }
}
