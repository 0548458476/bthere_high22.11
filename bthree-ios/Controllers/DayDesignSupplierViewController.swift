//
//  DayDesignSupplierViewController.swift
//  Bthere
//
//  Created by User on 1.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

//ספק - תצוגת יום
class DayDesignSupplierViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,setDateDelegate {
    
    //MARK: - Outlet
    @IBOutlet weak var colDay: UICollectionView!
    @IBOutlet weak var lblDayOfMonth: UILabel!
    @IBOutlet weak var lblDayOfWeek: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    //MARK: - IBAction
    @IBAction func btnNext(sender: AnyObject){
        //הקודם ולא הבא
        hasEvent = false
        currentDate = Calendar.sharedInstance.reduceDay(currentDate)
        Global.sharedInstance.currDateSelected = currentDate
        setEventsArray()
        colDay.reloadData()
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
        
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        
        setDate()
        
    }
    
    @IBAction func btnPrevious(sender: AnyObject){
        //הבא ולא הקודם
        hasEvent = false
        currentDate =  Calendar.sharedInstance.addDay(currentDate)
        //st
        Global.sharedInstance.currDateSelected = currentDate
        setEventsArray()
        colDay.reloadData()
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
        
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        
        setDate()
        
    }
    
    
    //MARK: - Properties
    
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var arrHoursInt:Array<Int> =
        [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    var arrHours:Array<String> = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"]
    var arrEventsCurrentDay:Array<EKEvent> = []
    
    var flag = false
    var hasEvent = false
    var currentDate:NSDate = NSDate()
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    
    var hightCell:CGFloat = 0
    var hightViewBlue:CGFloat = 0//גובה של הוי הצבוע שמראה על התור הפנוי
    var hightViewClear:CGFloat = 0
    var minute:Int = 0
    var minute1:Int = 0
    

    @IBOutlet weak var viewSync: UIView!
   
    @IBOutlet weak var btnSyncGoogelSupplier: eyeSynCheckBox!
    
    //MARK: - Initial
    func initDate(date:NSDate)
    {
        currentDate = date
    }
    //MARK: - Initial Funcions

    override func viewDidLoad(){
        super.viewDidLoad()
        
        Global.sharedInstance.dayDesignCalendarSupplier = self
        
        if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true{
            btnSyncGoogelSupplier.isCecked = true
        }

        view.sendSubviewToBack(lblDay)
        
        hasEvent = false
        arrEventsCurrentDay = []
     
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        colDay.transform = scalingTransform
        
        setEventsArray()
        setDate()
        
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
    }
    
    override func viewWillAppear(animated: Bool) {
        Global.sharedInstance.getEventsFromMyCalendar()
        setEventsArray()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setDateClick(date:NSDate){// take a date save in global when day in month clicked and show the date in day design
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: date)
        
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        
        
        //the day of week from date - (int)
        let day:Int = Calendar.sharedInstance.getDayOfWeek(date)! - 1
        
        //the day of week from date - (letter - string)
        lblDayOfWeek.text = NSDateFormatter().veryShortWeekdaySymbols[day]
        
        //the day of month from date - (int)
        lblDayOfMonth.text = dayToday.description
        
        //get the month and day of week names - short
        
        if monthToday == 0 {
            monthToday = 1
        }
        let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
        // - long
        //NSDateFormatter().monthSymbols[monthToday - 1]
        let dayName = NSDateFormatter().weekdaySymbols[day]
        //cut the word "יום" from the string
        let myLongDay: String = dayName
        
        if language == "he"
        {
            var myShortDay = myLongDay.componentsSeparatedByString(" ")
            lblDay.text = myShortDay[1]
        }
        else
        {
            lblDay.text = myLongDay
        }
        lblDate.text = "\(dayToday) \(monthName) \(yearToday)"
    }
    
   //Set sync Design  if eye set on
    @IBAction func btnSyncWithGoogel(sender: eyeSynCheckBox) {
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
            colDay.reloadData()
        }
        else{
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
            colDay.reloadData()
        }
    }
    //set labels context by spech date selected
    func setDate()
    {
        //the day of week from date - (int)
        let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
        
        //the day of week from date - (letter - string)
        lblDayOfWeek.text = NSDateFormatter().veryShortWeekdaySymbols[day]
        
        //the day of month from date - (int)
        lblDayOfMonth.text = dayToday.description
        
        //get the month and day of week names - short
        
        if monthToday == 0 {
            monthToday = 1
        }
        let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
        // - long
        //NSDateFormatter().monthSymbols[monthToday - 1]
        let dayName = NSDateFormatter().weekdaySymbols[day]
        //cut the word "יום" from the string
        let myLongDay: String = dayName
        
        if language == "he"
        {
            var myShortDay = myLongDay.componentsSeparatedByString(" ")
            lblDay.text = myShortDay[1]
        }
        else
        {
            lblDay.text = myLongDay
        }
        lblDate.text = "\(dayToday) \(monthName) \(yearToday)"
        
    }
    //MARK: -EventFunction
    //get device events of day
    func setEventsArray()
    {
        arrEventsCurrentDay = []
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
    
    
    //MARK: - Collection View
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        let cell:EventsWeekViewsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("EventsWeekViews",forIndexPath: indexPath) as! EventsWeekViewsCollectionViewCell
        
        cell.viewTop.backgroundColor = UIColor.clearColor()
        cell.viewBottom.backgroundColor = UIColor.clearColor()
        
        cell.lblHoursTop.text = ""
        cell.lblDescTop.text = ""
        cell.lblHoursBottom.text = ""
        cell.lblDescBottom.text = ""
        
        let cell1:HoursCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Hours",forIndexPath: indexPath) as! HoursCollectionViewCell
        
        cell1.lblHours.text = ""
        
        //        if language == "he"
        //        {
        if Global.sharedInstance.rtl
        {
        cell.transform = scalingTransform
        cell1.transform = scalingTransform
        }
        //        }
        
        if indexPath.row % 2 == 0//השורה הראשונה
        {
            cell1.setDisplayData(arrHours[indexPath.row / 2])
            return cell1
        }
        else
        {
            if hasEvent == true && Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                //מעבר על האירועים האישיים
                for var eve in arrEventsCurrentDay
                {
                    let componentsStart = calendar.components([.Hour, .Minute], fromDate: eve.startDate)
                    
                    let componentsEnd = calendar.components([.Hour, .Minute], fromDate: eve.endDate)
                    
                    let hourS = componentsStart.hour
                    let minuteS = componentsStart.minute
                    
                    let hourE = componentsEnd.hour
                    let minuteE = componentsEnd.minute
                    
                    var hourS_Show:String = hourS.description
                    var hourE_Show:String = hourE.description
                    var minuteS_Show:String = minuteS.description
                    var minuteE_Show:String = minuteE.description
                    
                    if hourS < 10
                    {
                        hourS_Show = "0\(hourS)"
                    }
                    if hourE < 10
                    {
                        hourE_Show = "0\(hourE)"
                    }
                    if minuteS < 10
                    {
                        minuteS_Show = "0\(minuteS)"
                    }
                    if minuteE < 10
                    {
                        minuteE_Show = "0\(minuteE)"
                    }
                    
                    if arrHoursInt[indexPath.row / 2] == hourS
                    {
                        if minuteS > 0// שעת התחלה גדולה מ-0
                        {
                            cell.setDisplayDataDay("", descTop: "", hourBottom: "\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descBottom: eve.title)
                            
                            
                            if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                            {
                                minute = Int(minuteE) - Int(minuteS)//קבלת הזמן של הארוע
                                hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                minute1 = Int(minuteS)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(false, heightTop: (CGFloat(minute1) / 60), heightButtom: (CGFloat(minute) / 60), fromPage: 1, eventKind:0)
                                //לעשות שקוף לטופ לצבוע את הבטם
                            }
                                
                            else
                            {
                                minute = 60 - Int(minuteS)
                                hightViewBlue = ((CGFloat(minute) / 60) *
                                    hightCell)
                                minute1 = Int(minuteS)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(false,heightTop: (CGFloat(minute1) / 60), heightButtom:
                                    (CGFloat(minute) / 60), fromPage: 1, eventKind:0)
                                //לעשות שקוף לטופ לצבוע את הבטם
                            }
                            
                        }
                        else//שעת התחלה 00
                        {
                            cell.setDisplayDataDay("\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descTop: eve.title, hourBottom: "", descBottom: "")
                            
                            if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                            {
                                minute = Int(minuteE)
                                hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                
                                minute1 = 60 - Int(minuteE)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), fromPage: 1, eventKind:0)
                                
                                //לצבוע את הטופ
                            }
                            else
                            {
                                hightViewBlue = hightCell
                                hightViewClear = 0
                                cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, fromPage: 1, eventKind:0)
                                //לצבוע את הטופ
                            }
                        }
                        
                    }
                    else if arrHoursInt[indexPath.row / 2] > hourS && arrHoursInt[indexPath.row / 2] < hourE//אמצע הארוע
                    {
                        cell.setDisplayDataDay("", descTop: "", hourBottom: "", descBottom: "")
                        
                        hightViewBlue = hightCell
                        hightViewClear = 0
                        cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, fromPage: 1, eventKind:0)
                        //לצבוע את הטופ
                    }
                        
                    else if arrHoursInt[indexPath.row / 2] == hourE//סיום הארוע
                    {
                        cell.setDisplayDataDay( "", descTop: "", hourBottom: "", descBottom: "")
                        
                        minute = Int(minuteE)
                        hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                        minute1 = 60 - Int(minuteE)
                        hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                        cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), fromPage: 1, eventKind:0)
                        //לצבוע את הטופ
                    }
                    
                }
            }
            else
            {
                
                cell.IsToppTop = false
                cell.IsMiddleeTop = false
                
                cell.IsButtomTop = false
                cell.IsToppButtom = false
                cell.IsMiddleeButtom = false
                cell.IsButtomButtom = false
                
                
                cell.setDisplayDataDay("", descTop: "", hourBottom: "", descBottom: "")
                return cell
                
            }
        }
        
        //        if indexPath.row == 47
        //        {
        //            showEventsViews()
        //
        //        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 48//2*24
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        
        if indexPath.row % 2 == 0//העמודה הראשונה
        {
            return CGSize(width: view.frame.size.width / 8, height:  view.frame.size.width / 8)
        }
        
        return CGSize(width: view.frame.size.width - (view.frame.size.width / 8), height:  view.frame.size.width / 8)
    }
    
    //MARK: - ScrollView
    
    //    func scrollViewDidScroll(scrollView: UIScrollView) {
    //
    //        colDay.reloadData()
    //
    //    }
    //sync design
    func showSync()
    {
        if btnSyncGoogelSupplier.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSyncGoogelSupplier.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
            colDay.reloadData()
        }
        else
        {
            btnSyncGoogelSupplier.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
            colDay.reloadData()
        }

    }
    
}





