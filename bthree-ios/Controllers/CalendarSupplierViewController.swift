//
//  CalendarSupplierViewController.swift
//  Bthere
//
//  Created by User on 22.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//מודל יומן של ספק קיים
class CalendarSupplierViewController:  NavigationModelViewController,
    UIGestureRecognizerDelegate ,openFromMenuDelegate,clickToDayDelegate, clickToDayInWeekDelegate
{
    //MARK: - Varibals

    var delegateSetDate:setDateDelegate!=nil

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    var clientStoryBoard:UIStoryboard?
    var attrs = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Bold", size: 23)!,
        NSForegroundColorAttributeName : UIColor.blackColor(),
        NSUnderlineStyleAttributeName : 1]
    var attrsDeselect = [
        NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 23)!]
    
    var view1 : MonthDesignSupplierViewController!//month Design
    var view2 :ListDesignViewController!//list Design
    var view3 :WeekDesignSupplierViewController!//week Design
    var view4 :DayDesignSupplierViewController!//day Design
    
    var attributedString = NSMutableAttributedString(string:"")
    var subView = UIView()
    var selectedView:UIViewController = UIViewController()
    var isAuto = false
    
    var con:SearchTableViewController?// search result controller
    
    //MARK: - Outlet
    @IBOutlet var recordBtn: UIButton!
    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var monthlyBtn: UIButton!
    @IBOutlet var weekBtn: UIButton!
    @IBOutlet var dayBtn: UIButton!
  
    //MARK: - IBAction
//when click on list design open list controller
    @IBAction func recordBtn(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view2{
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForRecord()
            con?.view.removeFromSuperview()
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            
            self.view.addSubview(view2.view)
            
            if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                view2.btnSyncGoogleSupplier.isCecked = true
            }
            else
            {
                view2.btnSyncGoogleSupplier.isCecked = false
            }
            
            selectedView = view2
        }
        else
        {
            sender.backgroundColor = Colors.sharedInstance.color6
            DeSelectDesighnedBtn(sender)
            sender.tag = 0
        }
        
    }
    //לשונית חודש
    //when click on month design open month controller
       @IBAction func monthlyBtn(sender: UIButton) {
        if sender.tag == 0 || selectedView == view1
        {
            Calendar.sharedInstance.carrentDate = NSDate()
            
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForMonthly()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                view1.btnSyn.isCecked = true
            }
            else
            {
                view1.btnSyn.isCecked = false
            }
            view1.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
            self.view.addSubview(view1.view)
            selectedView = view1
        }
        else{
            sender.tag = 0
            sender.backgroundColor = Colors.sharedInstance.color7
            DeSelectDesighnedBtn(sender)
            selectedView = view2
        }
        
    }
    
    //לשונית שבוע
      //when click on week design open week controller
    @IBAction func weekBtn(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view3{
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForWeek()
            
            con?.view.removeFromSuperview()
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            
            if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                view3.btnSyncWithGoogelSupplier.isCecked = true
            }
            else
            {
                view3.btnSyncWithGoogelSupplier.isCecked = false
            }
            view3.delegate = self

             view3.initDateOfWeek(Global.sharedInstance.currDateSelected)
            self.view.addSubview(view3.view)
            selectedView  = view3
            
        }
        else{
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color8
        }
    }
    
    //לשונית יום
        //when click on day design open day controller
      @IBAction func dayBtn(sender: UIButton) {
        
        if sender.tag == 0 || selectedView == view4{
            SelectDesighnedBtn(sender)
            sender.tag = 1
            SelectSingleForDay()
            con?.view.removeFromSuperview()
            
            view1.view.removeFromSuperview()
            view2.view.removeFromSuperview()
            view3.view.removeFromSuperview()
            view4.view.removeFromSuperview()
            if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                view4.btnSyncGoogelSupplier.isCecked = true
            }
            else
            {
                view4.btnSyncGoogelSupplier.isCecked = false
            }
            self.view.addSubview(view4.view)
            selectedView = view4
        }
        else{
            sender.tag = 0
            DeSelectDesighnedBtn(sender)
            sender.backgroundColor = Colors.sharedInstance.color9
        }
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Calendar.sharedInstance.carrentDate = NSDate()
        
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        
        //on click print calendar in side menu
        if Global.sharedInstance.isFromPrintCalender == true
        {
            let printCalendarController: PrintCalendarViewController = storyboard!.instantiateViewControllerWithIdentifier("PrintCalendarViewController")as! PrintCalendarViewController
            printCalendarController.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.presentViewController(printCalendarController, animated: true, completion: nil)
        }
        
        Global.sharedInstance.whichReveal = true
        Global.sharedInstance.eleventCon = self
        
        subView.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + self.navigationController!.navigationBar.frame.size.height , view.frame.width, view.frame.height * 0.15)
        self.navigationController?.navigationBarHidden = false
        view1 = self.storyboard!.instantiateViewControllerWithIdentifier("MonthDesignSupplierViewController") as! MonthDesignSupplierViewController
        view1.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view2 = self.storyboard!.instantiateViewControllerWithIdentifier("ListDesignViewController") as! ListDesignViewController
        view2.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view3 = self.storyboard!.instantiateViewControllerWithIdentifier("WeekDesignSupplierViewController") as! WeekDesignSupplierViewController
        view3.delegate = self
        view3.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view3.delegate = self
        view4 = self.storyboard!.instantiateViewControllerWithIdentifier("DayDesignSupplierViewController") as! DayDesignSupplierViewController
        
        view4.view.frame = CGRectMake(0, subView.frame.height + (self.navigationController!.navigationBar.frame.size.height * 0.21) , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CalendarSupplierViewController.imageTapped))
        imgMenu.userInteractionEnabled = true
        imgMenu.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CalendarSupplierViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        self.view.addSubview(view1.view)
        selectedView = view1
        
        SelectDesighnedBtn(monthlyBtn)
        if  Global.sharedInstance.isFromMenu == false{
            //        let storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
            //        let viewCon:ExistProviderFirstViewController = storyboard.instantiateViewControllerWithIdentifier("ExistProviderFirstViewController") as! ExistProviderFirstViewController
            //        
            //        
            //        self.presentViewController(viewCon, animated: true, completion: nil)//בינתיים מוסלש צריך לדעת בדיוק מתי להראות אותו ומתי לא
        }
        else{
            Global.sharedInstance.isFromMenu = false
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
    }
    
    //MARK: - AutoCopmlete
    
    //func on writing  - search providers
    func searchAutocompleteEntriesWithSubstring(substring: String)
    {
        Global.sharedInstance.filterSubArr = []
        
        for subject in AppDelegate.arrDomainFilter
        {
            let myString:NSString! = subject.nvCategoryName as NSString
            
            let substringRange :NSRange! = myString.rangeOfString(substring)
            
            if (substringRange.location == 0)
            {
                Global.sharedInstance.filterSubArr.append(subject.nvCategoryName)
                isAuto = true
            }
            
        }
        //if there are results
        if Global.sharedInstance.filterSubArr.count != 0
        {
            con!.view.frame = CGRectMake(140, 110, self.view.frame.width * 0.665625, self.view.frame.height/3)
            con?.tableView.reloadData()
            self.view.addSubview(con!.view)
        }
        else
        {
            con?.view.removeFromSuperview()
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        searchAutocompleteEntriesWithSubstring(substring)
        return true
    }
    
    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //con?.view.removeFromSuperview()
    }
   
 //on click the menu plus - open it
    func imageTapped(){
        
        let storyBoard = UIStoryboard(name:"SupplierExist", bundle: nil)
        let viewCon:MenuPlusSupplierViewController = storyBoard.instantiateViewControllerWithIdentifier("MenuPlusSupplierViewController") as! MenuPlusSupplierViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        viewCon.delegate = self
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    //Highlight the selected tab - month / week / day / list design
    func SelectDesighnedBtn(btn:UIButton){

        btn.backgroundColor = UIColor.clearColor()
        underlineButton(btn, text: (btn.titleLabel?.text)!)
    }
    
    //Highlight the selected tab - month / week / day / list design
    func underlineButton(button : UIButton, text: String) {
        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:text, attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        button.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    //cancel the highlight of selected tab - month / week / day / list design
    func DeSelectDesighnedBtn(btn:UIButton)
    {

        attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string: (btn.titleLabel?.text)!, attributes:attrsDeselect)
        attributedString.appendAttributedString(buttonTitleStr)
        btn.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    func SelectSingleForRecord()
    {
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    
    func SelectSingleForMonthly(){
        
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    
    func SelectSingleForWeek(){
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
        dayBtn.tag = 0
        dayBtn.backgroundColor = Colors.sharedInstance.color9
        DeSelectDesighnedBtn(dayBtn)
    }
    
    func SelectSingleForDay(){
        monthlyBtn.tag = 0
        monthlyBtn.backgroundColor = Colors.sharedInstance.color7
        DeSelectDesighnedBtn(monthlyBtn)
        weekBtn.tag = 0
        DeSelectDesighnedBtn(weekBtn)
        weekBtn.backgroundColor = Colors.sharedInstance.color8
        recordBtn.backgroundColor = Colors.sharedInstance.color6
        DeSelectDesighnedBtn(recordBtn)
        recordBtn.tag = 0
    }
    
    
    func openSearchResults()//check why not used
    {
        let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = clientStoryBoard?.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController

    }
    
    //delegte func to open pages from plus menu
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(view1.tblWorkers)) {
            
            return false
        }
        return true
    }
    
    func clickToDay(){//open the day design when click one day in the month design
        
        SelectDesighnedBtn(dayBtn)
        //sender.tag = 1
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        self.delegateSetDate = Global.sharedInstance.dayDesignCalendarSupplier
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)

        self.view.addSubview(view4.view)
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
    }

    func clickToDayInWeek()//open the day design when click one day in the week design
    {
        SelectSingleForDay()
        selectedView.view.removeFromSuperview()
        //אתחול התאריך הנוכחי לפי התאריך שלחצו עליו
        view4.initDate(Global.sharedInstance.currDateSelected)

        self.view.addSubview(view4.view)
        self.delegateSetDate = Global.sharedInstance.dayDesignCalendarSupplier
        
        selectedView = view4
        delegateSetDate.setDateClick(Global.sharedInstance.dateDayClick)
        
    }

//
//    
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
}