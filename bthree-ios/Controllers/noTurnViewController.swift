//
//  noTurnViewController.swift
//  Bthere
//
//  Created by User on 11.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//פופאפ אין תור פנוי התואם את בקשתך
class noTurnViewController: UIViewController {

    //MARK: - Properties
    
    var dtOrderTime:NSDate = NSDate()
    var orderObj:OrderObj?
    var generic:Generic = Generic()
    var delegate:OpenDetailsAppointmentDelegate!=nil
    var delegateDismiss:dismissDelegate!=nil
    var timer: NSTimer? = nil
    
    //MARK: - Outlet
    
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var dpChangeHour: UIDatePicker!
    @IBOutlet weak var lblNoAppointment: UILabel!
    
    @IBOutlet var btnClose: UIButton!
    //בלחיצה על האיקס
    @IBAction func btnCloseAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var btnOk: UIButton!
    ///בלחיצה על אישור
    @IBAction func btnOkAction(sender: AnyObject) {
        saveOrderTimeInString()
        
        var dicOrderObj:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicOrderObj["orderObj"] = orderObj!.getDic()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        //מסיון לקבוע תור שוב
        api.sharedInstance.newOrder(dicOrderObj, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -1//אם אין תור פנוי בזמן הזה
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_TURN", comment: ""), vc: self)
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליח
            {
                Calendar.sharedInstance.saveEventInDeviceCalander()
                Alert.sharedInstance.showAlert(NSLocalizedString("SUCCESS_TURN", comment: ""), vc: self)
                self.dismissViewControllerAnimated(true, completion:nil)
                self.GetCustomerOrders()
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//שגיאה
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
            }
            
            self.generic.hideNativeActivityIndicator(self)
            },failure:
            {
                (AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
    @IBOutlet weak var lblWhatToDo: UILabel!
    //הכנס לרשימת המתנה
    @IBAction func btnGoToWaitingList(sender: AnyObject) {
        
        var dicOrderObj:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicOrderObj["orderObj"] = orderObj!.getDic()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.addUserToWaitingList(dicOrderObj, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליח
            {
                let alertController = UIAlertController(title: "", message: NSLocalizedString("SUCCESS_ADD_TO_WAIT_LIST", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
                self.presentViewController(alertController, animated: true, completion: nil)

                self.timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: #selector(self.doDelayed), userInfo: nil, repeats: false)
                
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == -2 || responseObject["Error"]!!["ErrorCode"] as! Int == -1//שגיאה
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
            }
            
            self.generic.hideNativeActivityIndicator(self)
            
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        })
        }
    }
    //פונקציה הנקראת לאחר שנגמר הטיימר
    func doDelayed(t: NSTimer) {
        self.dismissViewControllerAnimated(false, completion: nil)
        self.delegateDismiss.dismiss()
    }

    @IBOutlet weak var btnGoToWaitingList: UIButton!
    
    @IBOutlet weak var btnNearAppointments: UIButton!
    
    @IBAction func btnNearAppointments(sender: AnyObject)
    {
        
    }
    
    @IBOutlet weak var btnChangeDay: UIButton!
    //החלף יום
    @IBAction func btnChangeDay(sender: AnyObject) {
        showPicker(view,hidden: false)
    }
    
    @IBOutlet weak var btnOtherSupplier: UIButton!
    
    @IBAction func btnOtherSupplier(sender: AnyObject) {
    }
    
    //MARK:INITIAL
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNoAppointment.text = NSLocalizedString("NO_TURN_AS_ASK", comment: "")
        lblWhatToDo.text = NSLocalizedString("WHAT_TO_DO", comment: "")
        btnOk.setTitle(NSLocalizedString("OK", comment: ""), forState: .Normal)
        btnGoToWaitingList.setTitle(NSLocalizedString("ENTER_TO_WAIT_LIST", comment: ""), forState: .Normal)
        btnNearAppointments.setTitle(NSLocalizedString("NEAR_TURNS", comment: ""), forState: .Normal)
        btnChangeDay.setTitle(NSLocalizedString("CHANGE_DAY", comment: ""), forState: .Normal)
        btnOtherSupplier.setTitle(NSLocalizedString("OTHER_SUPPLIER", comment: ""), forState: .Normal)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        view.addGestureRecognizer(tap)
        
        dpChangeHour.backgroundColor = Colors.sharedInstance.color6
        dpChangeHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpChangeHour.setValue(0.8, forKeyPath: "alpha")
        dpChangeHour.datePickerMode = UIDatePickerMode.Date
        dpChangeHour.setValue(false, forKey: "highlightsToday")
        dpChangeHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        dpChangeHour.backgroundColor = UIColor.blackColor()
        
        viewPicker.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //הצגת פחקר לבחירת תאריך אחר
    func showPicker(view: UIView,hidden:Bool) {
        UIView.transitionWithView(view, duration: 0.5, options: .TransitionCrossDissolve, animations: {() -> Void in
            self.viewPicker.hidden = hidden
            }, completion: { _ in })
    }
    //בעת גלילת הפיקר
    func handleDatePicker(sender: UIDatePicker) {
       if sender == dpChangeHour
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "HH:mm:SS"
            dtOrderTime  = sender.date
        }
    }
    
    func dismiss()
    {
       saveOrderTimeInString()
       showPicker(view,hidden: true)
    }
    //שמירת שעת הארוע בסטרינג
    func saveOrderTimeInString()
    {
        let calendar = NSCalendar.currentCalendar()
        let componentsStart = calendar.components([.Hour, .Minute], fromDate: dtOrderTime)
        
        let hourS = componentsStart.hour
        let minuteS = componentsStart.minute
        
        var hourS_Show:String = hourS.description
        var minuteS_Show:String = minuteS.description
        
        if hourS < 10
        {
            hourS_Show = "0\(hourS)"
        }
        if minuteS < 10
        {
            minuteS_Show = "0\(minuteS)"
        }
        
        let fullHourS = "\(hourS_Show):\(minuteS_Show)"
        orderObj?.nvFromHour = fullHourS
    }
    
    //קבלת הארועים ללקוח
    func GetCustomerOrders()
    {
        var dic:Dictionary<String,AnyObject> =  Dictionary<String,AnyObject>()
        var arr = NSArray()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetCustomerOrders(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in

            arr = responseObject["Result"] as! NSArray
            let ps:OrderDetailsObj = OrderDetailsObj()
            
            Global.sharedInstance.ordersOfClientsArray = ps.OrderDetailsObjToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
            
            self.delegate.openDetailsAppointment()
            
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
}
