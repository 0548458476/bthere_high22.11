//
//  ExistingUserViewController.swift
//  bthree-ios
//
//  Created by User on 3.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//דף לקוח קיים(לבדוק אם משתמשים)
class ExistingUserViewController: UIViewController {
    
    @IBOutlet weak var existUser: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var passwosd: UILabel!
    

    @IBOutlet weak var txtFName: UITextField!
    
    @IBOutlet weak var txtFPassword: UITextField!
    
    @IBOutlet weak var btnConnect: UIButton!
    
    @IBAction func btnConnect(sender: AnyObject) {
        
        let viewCon:CustomerOrDistributorViewController = storyboard?.instantiateViewControllerWithIdentifier("CustomerOrDistributorViewController") as! CustomerOrDistributorViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        txtFName.borderStyle = .None
        txtFPassword.borderStyle = .None
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExistingUserViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        name.text = NSLocalizedString("NAME_USER", comment: "")
        passwosd.text = NSLocalizedString("PASSWORD_USER", comment: "")
        existUser.text = NSLocalizedString("EXISTS_USER", comment: "")
        btnConnect.setTitle(NSLocalizedString("BTN_CONNECT", comment: ""), forState: .Normal)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
