//
//  NewAppointmentClientViewController.swift
//  Bthere
//
//  Created by User on 1.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//


protocol OpenDetailsAppointmentDelegate {
    func openDetailsAppointment()
}
protocol setNewOrderDelegate {
    func setNewOrder()
}

protocol dismissDelegate {
    func dismiss()
}

import UIKit

//הוסף תור ללקוח- קביעת תור חדש
class NewAppointmentClientViewController: UIViewController,UITextViewDelegate,OpenDetailsAppointmentDelegate,setNewOrderDelegate,dismissDelegate{
    
    //MARK: - Properties
    
    var clientStoryBoard:UIStoryboard?
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var storyBoard1:UIStoryboard?
    var dateFormatter:NSDateFormatter = NSDateFormatter()
    var order:OrderObj?
    var delegatGetCustomerOrders:getCustomerOrdersDelegate!=nil
    var generic:Generic = Generic()
    var arrProviders:Array<SearchResulstsObj> = Array<SearchResulstsObj>()
    var arrSeviceProviders:Array<User> =  Array<User>()
    var arrSeviceTypes:Array<objProviderServices> = Array<objProviderServices>()
    var supplierID:Int = Int()
    var supplierAddress:String = ""
    
    
    var serviceProvidersID: Int = Int()
    var providerServiceID:Int = Int()
    var dtOrder:NSDate = NSDate()
    var dtOrderTime:NSDate = NSDate()
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblNameServicer: UILabel!
    
    @IBOutlet weak var lblServiceTypeSelected: UILabel!
    
    @IBOutlet weak var lblNameSupplier: UILabel!
    @IBOutlet weak var lblTitleNewTurn: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var dpDate: UIDatePicker!
    
    @IBOutlet weak var lblDateSelected: UILabel!
    
    @IBOutlet weak var dpHour: UIDatePicker!
    
    @IBOutlet weak var lblHourSelected: UILabel!
    
    @IBOutlet weak var lblSupplier: UILabel!
    
    @IBOutlet weak var viewSupplier: UIView!
    
    //ספק
    @IBAction func btnOpenSupplier(sender: AnyObject) {
        
        dpDate.hidden = true
        dpDate.tag = 0
        dpHour.hidden = true
        dpHour.tag = 0
        
        if tblSuppliers.tag == 0//סגור
        {
            tblSuppliers.tag = 1
            tblSuppliers.hidden = false
        }
        else
        {
            tblSuppliers.tag = 0
            tblSuppliers.hidden = true
        }
        tblSuppliers.reloadData()
    }
    
    @IBOutlet weak var btnOpenSupplier: UIButton!
    
    @IBOutlet weak var tblSuppliers: UITableView!
    
    @IBOutlet weak var lblServiceProvider: UILabel!
    
    @IBOutlet weak var viewServiceProvider: UIView!
    
    @IBAction func btnOpenServiceProvider(sender: AnyObject) {
        
        dpDate.hidden = true
        dpDate.tag = 0
        dpHour.hidden = true
        dpHour.tag = 0
        if supplierID != 0
        {
            if tblServiceProvider.tag == 0//close
            {
                
                tblServiceProvider.tag = 1
                tblServiceProvider.hidden = false
            }
            else
            {
                tblServiceProvider.tag = 0
                tblServiceProvider.hidden = true
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_SELECTED_SUPPLIER", comment: ""), vc: self)
        }
        tblSuppliers.reloadData()
    }
    
    @IBOutlet weak var tblServiceProvider: UITableView!
    
    @IBOutlet weak var lblServiceType: UILabel!
    
    @IBOutlet weak var viewServiceType: UIView!
    
    @IBAction func btnOpenServiceType(sender: AnyObject) {
        
        dpDate.hidden = true
        dpDate.tag = 0
        dpHour.hidden = true
        dpHour.tag = 0
        
        if tblServiceType.tag == 0
        {
            tblServiceType.tag = 1
            tblServiceType.hidden = false
        }
        else
        {
            tblServiceType.tag = 0
            tblServiceType.hidden = true
        }
    }
    
    @IBOutlet weak var tblServiceType: UITableView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var viewDate: UIView!
    
    @IBOutlet weak var btnOpenDate: UIButton!
    
    @IBAction func btnOpenDate(sender: AnyObject) {
        
        dpHour.hidden = true
        dpHour.tag = 0
        
        if dpDate.tag == 0
        {
            dpDate.tag = 1
            dpDate.hidden = false
        }
        else
        {
            dpDate.tag = 0
            dpDate.hidden = true
        }
    }
    
    
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var viewHour: UIView!
    
    @IBAction func btnOpenHour(sender: AnyObject) {
        
        dpDate.hidden = true
        dpDate.tag = 0
        
        if dpHour.tag == 0
        {
            dpHour.tag = 1
            dpHour.hidden = false
        }
        else
        {
            dpHour.tag = 0
            dpHour.hidden = true
        }
    }
    
    @IBOutlet weak var lblRemark: UILabel!
    
    @IBOutlet weak var txtVRemark: UITextView!
    
    @IBOutlet weak var btnMakeAppointment: UIButton!
    //קבע תור
    @IBAction func btnMakeAppointment(sender: AnyObject) {
        
        var arrayServicesKods:Array<Int> = []//מערך לשמירת כל הקודים של השרותים
        var arrayServicesKodsToServer:Array<Int> = []
        arrayServicesKods.append(serviceProvidersID)
        arrayServicesKodsToServer.append(providerServiceID)
        self.saveOrderTimeInString()
        order = OrderObj(_iSupplierId:supplierID, _iUserId: Global.sharedInstance.currentUser.iUserId, _iSupplierUserId: arrayServicesKods, _iProviderServiceId: arrayServicesKodsToServer, _dtDateOrder:Global.sharedInstance.eventBthereDate, _nvFromHour:Global.sharedInstance.hourFreeEventInPlusMenu, _nvComment: txtVRemark.text)
        
        var dicOrderObj:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicOrderObj["orderObj"] = order!.getDic()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.CheckIfOrderIsAvailable(dicOrderObj, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליח
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    
                    let date = Global.sharedInstance.getStringFromDateString(responseObject["Result"] as! String)
                    Global.sharedInstance.NewEventToHour = dateFormatter.stringFromDate(date)
                    self.openDetailsAppointment()//פתיחת פרטי התור
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == -1//זמן תפוס
                {
                    let viewCon:noTurnViewController = self.clientStoryBoard!.instantiateViewControllerWithIdentifier("noTurnViewController") as! noTurnViewController
                    viewCon.delegate = self
                    viewCon.delegateDismiss = self
                    viewCon.orderObj = self.order
                    viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                    self.presentViewController(viewCon, animated: true, completion: nil)
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//שגיאה
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
                }
                
                self.generic.hideNativeActivityIndicator(self)
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
            })
        }
    }
    
    //MARK: - initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        storyBoard1 = UIStoryboard(name: "Main", bundle: nil)
        
        let dateFormatterHours = NSDateFormatter()
        dateFormatterHours.dateFormat = "HH:mm:SS"
        dpHour.date = dateFormatterHours.dateFromString("12:30:00")!
        
        getProviders()
        
        dpDate.hidden = true
        dpHour.hidden = true
        
        if language != "he" && (DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P) {
            
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 21)
            lblSupplier.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 13)
        }
        else if language != "he" && (DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS) {
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 18)
            lblSupplier.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 12)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 11)
        }
            
        else if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 22)
            
            lblSupplier.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 17)
        }
        
        txtVRemark.delegate = self
        
        tblSuppliers.separatorStyle = .None
        tblServiceProvider.separatorStyle = .None
        tblServiceType.separatorStyle = .None
        
        tblSuppliers.hidden = true
        tblServiceProvider.hidden = true
        tblServiceType.hidden = true
        
        //the table is closed
        tblSuppliers.tag = 0
        tblServiceProvider.tag = 0
        tblServiceType.tag = 0
        
        lblTitleNewTurn.text = NSLocalizedString("NEW_TURN_TITLE", comment: "")
        lblSupplier.text = NSLocalizedString("COSTUMER", comment: "")
        lblServiceProvider.text = NSLocalizedString("SERVICE_PROVIDER", comment: "")
        lblServiceType.text = NSLocalizedString("SERVICE_TYPE", comment: "")
        lblDate.text = NSLocalizedString("DATE", comment: "")
        lblHour.text = NSLocalizedString("HOUR", comment: "")
        lblRemark.text = NSLocalizedString("REMARK", comment: "")
        
        btnMakeAppointment.setTitle(NSLocalizedString("MAKE_APPOINTMENT", comment: ""), forState: .Normal)
        tblSuppliers.separatorStyle = .None
        
        dpDate.backgroundColor = Colors.sharedInstance.color6
        dpDate.setValue(UIColor.blackColor(), forKeyPath: "textColor")
        dpDate.setValue(0.8, forKeyPath: "alpha")
        dpDate.datePickerMode = UIDatePickerMode.Date
        dpDate.setValue(false, forKey: "highlightsToday")
        dpDate.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        dpHour.backgroundColor = Colors.sharedInstance.color6
        dpHour.setValue(UIColor.blackColor(), forKeyPath: "textColor")
        dpHour.setValue(0.8, forKeyPath: "alpha")
        dpHour.datePickerMode = UIDatePickerMode.Time
        dpHour.setValue(false, forKey: "highlightsToday")
        dpHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - dismiss key board
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            txtVRemark.resignFirstResponder()
        }
        super.touchesBegan(touches, withEvent:event)
    }
    
    //MARK: - table view
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblSuppliers{
            return arrProviders.count
        }
        else if tableView == tblServiceProvider
        {
            return arrSeviceProviders.count
        }
        else
        {
            return arrSeviceTypes.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell =
            tableView.dequeueReusableCellWithIdentifier("newTurnTableViewCell")as!newTurnTableViewCell
        
        if tableView == tblSuppliers
        {
            cell.setDisplayData((arrProviders[indexPath.row] as SearchResulstsObj).nvProviderName
            )
        }
        else if tableView == tblServiceProvider
        {
            let s = "\((arrSeviceProviders[indexPath.row] as User).nvFirstName) \((arrSeviceProviders[indexPath.row] as User).nvLastName)"
            cell.setDisplayData(s)
        }
        else
        {
            let s = "\((arrSeviceTypes[indexPath.row] as objProviderServices).nvServiceName)"
            cell.setDisplayData(s)
        }
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == tblSuppliers
        {
            lblSupplier.text = (arrProviders[indexPath.row] as SearchResulstsObj).nvProviderName
            supplierID = (arrProviders[indexPath.row] as SearchResulstsObj).iProviderId
            supplierAddress = (arrProviders[indexPath.row] as SearchResulstsObj).nvAdress
            getServicesProviderForSupplier((arrProviders[indexPath.row] as SearchResulstsObj).iProviderId)
            getProviderServicesForSupplier((arrProviders[indexPath.row] as SearchResulstsObj).iProviderId)
            
            tblSuppliers.hidden = true
            tblSuppliers.tag = 0
        }
        if tableView == tblServiceProvider
        {
            let fullName = "\((arrSeviceProviders[indexPath.row] as User).nvFirstName) \((arrSeviceProviders[indexPath.row] as User).nvLastName)"
            lblServiceProvider.text = fullName
            tblServiceProvider.hidden = true
            tblServiceProvider.tag = 0
            serviceProvidersID = (arrSeviceProviders[indexPath.row] as User).iUserId
        }
        if tableView == tblServiceType
        {
            let serviceName = (arrSeviceTypes[indexPath.row] as objProviderServices).nvServiceName
            lblServiceType.text = serviceName
            tblServiceType.hidden = true
            tblServiceType.tag = 0
            providerServiceID = (arrSeviceTypes[indexPath.row] as objProviderServices).iProviderServiceId
            
        }
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        switch tableView {
        case tblSuppliers:
            if arrProviders.count < 3
            {
                tblSuppliers.frame.size.height = self.view.frame.size.height * 0.07 * CGFloat(arrProviders.count)
            }
            
        case tblServiceType:
            if arrSeviceTypes.count < 3
            {
                tblServiceType.frame.size.height = self.view.frame.size.height * 0.07 * CGFloat(arrSeviceTypes.count)
            }
        case tblServiceProvider:
            if arrSeviceProviders.count < 3
            {
                tblServiceProvider.frame.size.height = self.view.frame.size.height * 0.07 * CGFloat(arrSeviceProviders.count)
            }
        default:
            return self.view.frame.size.height * 0.07
        }
        return self.view.frame.size.height * 0.07
        
    }
    
    //קבלת רשימת ספקים ללקוח
    func getProviders()
    {
        let searchObj:SearchResulstsObj = SearchResulstsObj()
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.GetProviderListForCustomer(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                    self.arrProviders = searchObj.objProvidersToArray(Global.sharedInstance.dicResults)
                    self.tblSuppliers.reloadData()
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    print(NSError)
            })
        }
    }
    
    // MARK: - DatePicker
    
    //בעת גלילת הפיקר
    func handleDatePicker(sender: UIDatePicker) {
        if sender == dpDate//תאריך
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dtOrder = sender.date
            lblDateSelected.text = dateFormatter.stringFromDate(sender.date)
        }
        else if sender == dpHour//שעות
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "HH:mm:SS"
            dtOrderTime  = sender.date
            lblHourSelected.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    //קבלת נותני שרות לספק
    func getServicesProviderForSupplier(supplierId:Int)
    {
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = supplierId
        var arrUsers:Array<User> = Array<User>()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.getServicesProviderForSupplierfunc(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_GIVES_SERVICE", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    var Arr:NSArray = NSArray()
                    
                    Arr = responseObject["Result"] as! NSArray
                    let u:User = User()
                    arrUsers = u.usersToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    
                    if arrUsers.count != 0
                    {
                        self.arrSeviceProviders = arrUsers
                        self.tblServiceProvider.reloadData()
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    
            })
        }
        
    }
    
    //קבלת שרותים לספק
    func  getProviderServicesForSupplier(isupplierID:Int)
    {
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = isupplierID
        var arrUsers:Array<User> = Array<User>()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.getProviderServicesForSupplier(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                var Arr:NSArray = NSArray()
                
                Arr = responseObject["Result"] as! NSArray
                let ps:objProviderServices = objProviderServices()
                
                self.arrSeviceTypes = ps.objProviderServicesToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                if self.arrSeviceTypes.count == 0
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
                }
                else
                {
                    self.tblServiceType.reloadData()
                }
            }
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                
        })
        }
    }
    //קבע תור
    func setNewOrder()
    {
        var arrayServicesKods:Array<Int> = []//מערך לשמירת כל הקודים של השרותים
        var arrayServicesKodsToServer:Array<Int> = []
        arrayServicesKods.append(serviceProvidersID)
        arrayServicesKodsToServer.append(providerServiceID)
        self.saveOrderTimeInString()
        order = OrderObj(_iSupplierId:supplierID, _iUserId: Global.sharedInstance.currentUser.iUserId, _iSupplierUserId: arrayServicesKods, _iProviderServiceId: arrayServicesKodsToServer, _dtDateOrder:Global.sharedInstance.eventBthereDate , _nvFromHour:Global.sharedInstance.hourFreeEventInPlusMenu, _nvComment: txtVRemark.text)
        
        var dicOrderObj:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicOrderObj["orderObj"] = order!.getDic()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.newOrder(dicOrderObj, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -1//אם אין תור פנוי בזמן הזה
            {
                let viewCon:noTurnViewController = self.clientStoryBoard!.instantiateViewControllerWithIdentifier("noTurnViewController") as! noTurnViewController
                viewCon.delegate = self
                viewCon.orderObj = self.order
                viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                self.presentViewController(viewCon, animated: true, completion: nil)
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1//הצליח
            {
                Calendar.sharedInstance.saveEventInDeviceCalander()
                self.getFreeDaysForServiceProvider()
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//שגיאה
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
            }
            self.generic.hideNativeActivityIndicator(self)
            },failure:
            {
                (AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
    //קבלת ימים פנויים לנותן שרות
    func getFreeDaysForServiceProvider(){
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        
        formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
        
        Global.sharedInstance.dicGetFreeDaysForServiceProvider["lProviderServiceId"] = Global.sharedInstance.arrayServicesKodsToServer
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetFreeDaysForServiceProvider(Global.sharedInstance.dicGetFreeDaysForServiceProvider, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                dateFormatter.timeZone = NSTimeZone(name: "GMT")
                let ps:providerFreeDaysObj = providerFreeDaysObj()
                
                Global.sharedInstance.getFreeDaysForService = ps.objFreeDaysToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                
                //איפוס המערך מנתונים ישנים
                Global.sharedInstance.dateFreeDays = []
                
                for i in 0 ..< Global.sharedInstance.getFreeDaysForService.count{
                    let dateDt = Calendar.sharedInstance.addDay(Global.sharedInstance.getStringFromDateString(Global.sharedInstance.getFreeDaysForService[i].dtDate))//היום שיש בו שעות פנויות
                    
                    Global.sharedInstance.dateFreeDays.append(dateDt)//מערך שמכיל את תאריכי הימים בהם אפשר לקבוע תור ז״א יש בהם שעות פנויות
                }
                Global.sharedInstance.fromHourArray = Global.sharedInstance.dateFreeDays
                Global.sharedInstance.endHourArray = Global.sharedInstance.dateFreeDays
                
                
                for i in 0 ..< Global.sharedInstance.dateFreeDays.count{//יש לי מערך של תאריכים ובאותו מקום של התאריך במערך יש לי את השעות של התורים הפנויים של אותו תאריך
                    let provider:providerFreeDaysObj = Global.sharedInstance.getFreeDaysForService[i]
                    
                    let hourStart = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvFromHour)
                    
                    Global.sharedInstance.fromHourArray[i] = hourStart
                    let hourEnd = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvToHour)
                    Global.sharedInstance.endHourArray[i] = hourEnd
                    
                }
                
                let currentDate:NSDate = NSDate()
                var dayOfWeekToday = 0
                
                dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
                
                //אתחול מערך השעות הפנויות לשבוע
                //ואתחול מערך האירועים של ביזר
                for i in 0 ..< 7 {
                    
                    let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                    
                    Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
                    Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
                }
                self.delegatGetCustomerOrders.GetCustomerOrders()
                //self.GetCustomerOrders()
            }
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
    func saveOrderTimeInString()
    {
        let calendar = NSCalendar.currentCalendar()
        let componentsStart = calendar.components([.Hour, .Minute], fromDate: dtOrderTime)
        
        let hourS = componentsStart.hour
        let minuteS = componentsStart.minute
        
        Global.sharedInstance.eventBthereDate = dtOrder
        
        Global.sharedInstance.eventBthereDateStart = Calendar.sharedInstance.getPartsOfDate(Global.sharedInstance.eventBthereDate , to: componentsStart)
        
        var hourS_Show:String = hourS.description
        var minuteS_Show:String = minuteS.description
        
        if hourS < 10
        {
            hourS_Show = "0\(hourS)"
        }
        if minuteS < 10
        {
            minuteS_Show = "0\(minuteS)"
        }
        
        let fullHourS = "\(hourS_Show):\(minuteS_Show)"
        Global.sharedInstance.hourFreeEventInPlusMenu = fullHourS
    }
    
    //פתיחת פרטי הזמנה
    func openDetailsAppointment()
    {
        let storyboard = UIStoryboard(name: "ClientExist", bundle: nil)
        
        let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyboard.instantiateViewControllerWithIdentifier("detailsAppointmetClientViewController") as! detailsAppointmetClientViewController
        self.delegatGetCustomerOrders = vc
        
        vc.tag = 4//תור חדש
        vc.fromViewMode = false
        vc.delegateSetNewOrder = self
        
        vc.dateEvent = dtOrder
        vc.fromHour = (order!.nvFromHour)
        vc.supplierName = self.lblSupplier.text!
        vc.serviceName = lblServiceType.text!
        vc.address = supplierAddress
        
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func dismiss()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let frontviewcontroller = storyBoard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = storyboard.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
        
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        let rearViewController = storyBoard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
}

