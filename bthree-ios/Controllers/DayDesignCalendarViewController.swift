//
//  DayDesignCalendarViewController.swift
//  bthree-ios
//
//  Created by User on 17.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

// לקוח - תצוגת יום
class DayDesignCalendarViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,setDateDelegate,UIGestureRecognizerDelegate{

        var pointOneLast:CGPoint = CGPoint()
        var pointOne:CGPoint = CGPoint()
        var pointTwo:CGPoint = CGPoint()
        var fIsBig = false
        var firstViewInFreeHour:Int = -1//הטג של הויו הראשון בשעה פנויה, עליו נציג את השעות
        var selectedCellIndexPath: NSIndexPath?
        var currentIndexPath:NSIndexPath?
        var hightCell:CGFloat = 0
        var hightViewBlue:CGFloat = 0//גובה של הוי הצבוע שמראה על התור הפנוי
        var hightViewClear:CGFloat = 0
        var minute:Int = 0
        var minute1:Int = 0
        
        @IBOutlet weak var viewSync: UIView!
        
        
        @IBAction func btnSync(sender: eyeSynCheckBox) {
            if sender.isCecked == false
            {
                Global.sharedInstance.getEventsFromMyCalendar()
                Global.sharedInstance.isSyncWithGoogelCalendar = true
                colDay.reloadData()
            }
            else
            {
                Global.sharedInstance.isSyncWithGoogelCalendar = false
                colDay.reloadData()
            }
            
        }
        //MARK: - Outlet
        @IBOutlet weak var colDay: UICollectionView!
        @IBOutlet weak var lblDayOfMonth: UILabel!
        @IBOutlet weak var lblDayOfWeek: UILabel!
        @IBOutlet weak var imgCircleToday: UIImageView!
        @IBOutlet weak var lblDay: UILabel!
        @IBOutlet weak var lblDate: UILabel!
        
        @IBAction func btnNext(sender: AnyObject){
            //הקודם ולא הבא
            
            hasEvent = false
            currentDate =  Calendar.sharedInstance.reduceDay(currentDate)
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date1String = dateFormatter.stringFromDate(currentDate)
            let date2String = dateFormatter.stringFromDate(NSDate())
            if date1String == date2String {
                imgCircleToday.hidden = false
            }
            else
            {
                imgCircleToday.hidden = true
            }

            Global.sharedInstance.dateDayClick = currentDate
            Global.sharedInstance.currDateSelected = currentDate
            setEventsArray()
            setBThereEventsArray()
            //כדי שיציג לכל יום את השעות הפנויות שלו ולא לפי היום הקודם
            setDateClick(currentDate)
            colDay.reloadData()
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            setDate()
        }
        @IBAction func btnPrevious(sender: AnyObject){
            //הבא ולא הקודם
            hasEvent = false
            currentDate =  Calendar.sharedInstance.addDay(currentDate)
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date1String = dateFormatter.stringFromDate(currentDate)
            let date2String = dateFormatter.stringFromDate(NSDate())
            if date1String == date2String {
                imgCircleToday.hidden = false
            }
            else
            {
                imgCircleToday.hidden = true
            }
            
            Global.sharedInstance.dateDayClick = currentDate
            Global.sharedInstance.currDateSelected = currentDate
            setEventsArray()
            setBThereEventsArray()
            //כדי שיציג לכל יום את השעות הפנויות שלו ולא לפי היום הקודם
            setDateClick(currentDate)
            colDay.reloadData()
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            setDate()
        }
        
        
        //MARK: - Properties
        
        let pinchRecognizer = UIPinchGestureRecognizer()
        // indicates that the pinch is in progress
        var pinchInProgress = false
        
        
        let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
        var arrHoursInt:Array<Int> = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,0]
        var arrHours:Array<String> = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","7:00","8:00",
                                      "9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","00:00"]
        var arrEventsCurrentDay:Array<EKEvent> = []
        var arrBThereEventsCurrentDay:Array<OrderDetailsObj> = []
        
        var flag = false
        var hasEvent = false
        var currentDate:NSDate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        var dayToday:Int = 0
        var monthToday:Int = 0
        var yearToday:Int = 0
        
        @IBOutlet weak var btnSuny: eyeSynCheckBox!
        //MARK: - Initial
        
        //    @IBOutlet weak var btnSyncGoogelSupplier: eyeSynCheckBox!
        // set Date with date selected in month or week design
        func initDate(date:NSDate)
        {
            currentDate = date
        }
        //MARK: - Initial Function
        override func viewDidLoad()
        {
            super.viewDidLoad()
            checkDevice()
            fIsBig = false
            
            Global.sharedInstance.datDesigncalendar = self
            if Global.sharedInstance.isSyncWithGoogelCalendar == true
            {
                btnSuny.isCecked = true
            }
            else
            {
                btnSuny.isCecked = false
            }
            
            hightCell = view.frame.size.width / 8
            

            view.sendSubviewToBack(lblDay)
            
            hasEvent = false
            arrEventsCurrentDay = []
            
                 if language == "he"
            {
                if Global.sharedInstance.rtl
                {
                var scalingTransform : CGAffineTransform!
                scalingTransform = CGAffineTransformMakeScale(-1, 1)
                colDay.transform = scalingTransform
                colDay.transform = scalingTransform
                colDay.transform = scalingTransform
                }
            }
            
            setEventsArray()
            setBThereEventsArray()
            setDate()
            
            let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
            viewSync.addGestureRecognizer(tapSync)
            
            pinchRecognizer.addTarget(self, action: #selector(self.handlePinch(_:)))
            colDay.addGestureRecognizer(pinchRecognizer)
        }
        
        override func viewWillAppear(animated: Bool) {
            
            fIsBig = false
            if Global.sharedInstance.isSyncWithGoogelCalendar == true
            {
                btnSuny.isCecked = true
            }
            else
            {
                btnSuny.isCecked = false
            }
            
            setEventsArray()
            setBThereEventsArray()
            
        }
        override func viewDidAppear(animated: Bool) {
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date1String = dateFormatter.stringFromDate(currentDate)
            let date2String = dateFormatter.stringFromDate(NSDate())
            if date1String == date2String {
                imgCircleToday.hidden = false
            }
            else
            {
                imgCircleToday.hidden = true
            }

            fIsBig = false
            Global.sharedInstance.getEventsFromMyCalendar()
            setEventsArray()
            setBThereEventsArray()
            setDate()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    func setDateClick(date:NSDate){// take a date save in global when day in month clicked and show the date in day design
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate:date)
            //Global.sharedInstance.dateDayClick)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            
            //the day of week from date - (int)
            let day:Int = Calendar.sharedInstance.getDayOfWeek(date)! - 1
            
            //the day of week from date - (letter - string)
            lblDayOfWeek.text = NSDateFormatter().veryShortWeekdaySymbols[day]
            Global.sharedInstance.dayFreeEvent = NSDateFormatter().weekdaySymbols[day]
                //lblDayOfWeek.text!
            
            //the day of month from date - (int)
            lblDayOfMonth.text = dayToday.description
            
            //get the month and day of week names - short
            
            if monthToday == 0 {
                monthToday = 1
            }
            let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
            // - long
            //NSDateFormatter().monthSymbols[monthToday - 1]
            let dayName = NSDateFormatter().weekdaySymbols[day]
            //cut the word "יום" from the string
            let myLongDay: String = dayName
            
            if language == "he"
            {
                var myShortDay = myLongDay.componentsSeparatedByString(" ")
                lblDay.text = myShortDay[1]
            }
            else
            {
                lblDay.text = myLongDay
            }
         
            lblDate.text = "\(dayToday) \(monthName) \(yearToday)"
            
            colDay.reloadData()
        }
        
        func setDate()
        {
            //the day of week from date - (int)
            let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
            
            //the day of week from date - (letter - string)
            lblDayOfWeek.text = NSDateFormatter().veryShortWeekdaySymbols[day]
            
            //the day of month from date - (int)
            lblDayOfMonth.text = dayToday.description
            
            //get the month and day of week names - short
            
            if monthToday == 0 {
                monthToday = 1
            }
            let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
            // - long
            //NSDateFormatter().monthSymbols[monthToday - 1]
            let dayName = NSDateFormatter().weekdaySymbols[day]
            //cut the word "יום" from the string
            let myLongDay: String = dayName
            
            if language == "he"
            {
                var myShortDay = myLongDay.componentsSeparatedByString(" ")
                if myShortDay.count > 1
                {
                    lblDay.text = myShortDay[1]
                }
            }
            else
            {
                lblDay.text = myLongDay
            }
            lblDate.text = "\(dayToday) \(monthName) \(yearToday)"
            
        }
    //MARK: Set Event Function
        //set events of spech day from device
        func setEventsArray()
        {
            arrEventsCurrentDay = []
            for var item in Global.sharedInstance.eventList
            {
                let event = item as! EKEvent
                
                let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
                
                let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
                
                yearToday =  componentsCurrent.year
                monthToday = componentsCurrent.month
                dayToday = componentsCurrent.day
                
                let yearEvent =  componentsEvent.year
                let monthEvent = componentsEvent.month
                let dayEvent = componentsEvent.day
                
                if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
                {
                    arrEventsCurrentDay.append(event)
                    hasEvent = true
                }
            }
        }
    //set events of spech day from Bthere

        func setBThereEventsArray()
        {
            arrBThereEventsCurrentDay = []
            for var item in Global.sharedInstance.ordersOfClientsArray
            {
                let btEvent = item
                
                let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
                
                let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: btEvent.dtDateOrder)
                
                yearToday =  componentsCurrent.year
                monthToday = componentsCurrent.month
                dayToday = componentsCurrent.day
                
                let yearEvent =  componentsEvent.year
                let monthEvent = componentsEvent.month
                let dayEvent = componentsEvent.day
                
                if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
                {
                    arrBThereEventsCurrentDay.append(btEvent)
                    hasEvent = true
                }
            }
        }
        
        var isShowFreeDay:Int = 0//flag to know if there isnt any free hour in that hour
        
        //MARK: - Collection View
        
        func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
            
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            
            isShowFreeDay = 0
            
            let cell:EventsWeek12ViewsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("EventsWeekViews12",forIndexPath: indexPath) as! EventsWeek12ViewsCollectionViewCell
            
            cell.indexHourOfDay = indexPath.row/2
            cell.arrBThereEventsCurrentDay = arrBThereEventsCurrentDay
            
            currentIndexPath = indexPath
            
            cell.delegate = Global.sharedInstance.calendarClient
            cell.viewTopEvent.backgroundColor = UIColor.clearColor()
            cell.viewBottomEvent.backgroundColor = UIColor.clearColor()
            cell.lblHoursTop.text = ""
            cell.lblDescTop.text = ""
            cell.lblHoursBottom.text = ""
            cell.lblDescBottom.text = ""
            
            cell.view0to5.backgroundColor = UIColor.clearColor()
            cell.view5to10.backgroundColor = UIColor.clearColor()
            cell.view10to15.backgroundColor = UIColor.clearColor()
            cell.view15to20.backgroundColor = UIColor.clearColor()
            cell.view20to25.backgroundColor = UIColor.clearColor()
            cell.view25to30.backgroundColor = UIColor.clearColor()
            cell.view30to35.backgroundColor = UIColor.clearColor()
            cell.view35to40.backgroundColor = UIColor.clearColor()
            cell.view40to45.backgroundColor = UIColor.clearColor()
            cell.view45to50.backgroundColor = UIColor.clearColor()
            cell.view50to55.backgroundColor = UIColor.clearColor()
            cell.view55to60.backgroundColor = UIColor.clearColor()
            
            cell.view0to5.subviews.forEach({ $0.removeFromSuperview() })
            cell.view5to10.subviews.forEach({ $0.removeFromSuperview() })
            cell.view10to15.subviews.forEach({ $0.removeFromSuperview() })
            cell.view15to20.subviews.forEach({ $0.removeFromSuperview() })
            cell.view20to25.subviews.forEach({ $0.removeFromSuperview() })
            cell.view25to30.subviews.forEach({ $0.removeFromSuperview() })
            cell.view30to35.subviews.forEach({ $0.removeFromSuperview() })
            cell.view35to40.subviews.forEach({ $0.removeFromSuperview() })
            cell.view40to45.subviews.forEach({ $0.removeFromSuperview() })
            cell.view45to50.subviews.forEach({ $0.removeFromSuperview() })
            cell.view50to55.subviews.forEach({ $0.removeFromSuperview() })
            cell.view55to60.subviews.forEach({ $0.removeFromSuperview() })
            
            cell.lblHours1.text = ""
            cell.lblHours2.text = ""
            cell.lblHours3.text = ""
            cell.lblHours4.text = ""
            cell.lblHours5.text = ""
            cell.lblHours6.text = ""
            cell.lblHours7.text = ""
            cell.lblHours8.text = ""
            cell.lblHours9.text = ""
            cell.lblHours10.text = ""
            cell.lblHours11.text = ""
            cell.lblHours12.text = ""
            cell.lblHours1.backgroundColor = UIColor.clearColor()
            cell.lblHours2.backgroundColor = UIColor.clearColor()
            cell.lblHours3.backgroundColor = UIColor.clearColor()
            cell.lblHours4.backgroundColor = UIColor.clearColor()
            cell.lblHours5.backgroundColor = UIColor.clearColor()
            cell.lblHours6.backgroundColor = UIColor.clearColor()
            cell.lblHours7.backgroundColor = UIColor.clearColor()
            cell.lblHours8.backgroundColor = UIColor.clearColor()
            cell.lblHours9.backgroundColor = UIColor.clearColor()
            cell.lblHours10.backgroundColor = UIColor.clearColor()
            cell.lblHours11.backgroundColor = UIColor.clearColor()
            cell.lblHours12.backgroundColor = UIColor.clearColor()
            if Global.sharedInstance.rtl
            {
            cell.transform = scalingTransform
            }
            let cell1:HoursCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Hours",forIndexPath: indexPath) as! HoursCollectionViewCell
            
            cell1.lblHours.text = ""
            if Global.sharedInstance.rtl
            {
            cell1.transform = scalingTransform
            }
            if indexPath.row % 2 == 0//השורה הראשונה
            {
                cell1.setDisplayData(arrHours[indexPath.row / 2])
                return cell1
            }
            else
            {
                //מעבר על הארועים של ביזר והצגתם
                for item in arrBThereEventsCurrentDay {
                    
                    let hourStart = Global.sharedInstance.getStringFromDateString(item.nvFromHour)
                    
                    let hourEnd = Global.sharedInstance.getStringFromDateString(item.nvToHour)
                    
                    let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
                    
                    let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
                    
                    let hourS = componentsStart.hour
                    let minuteS = componentsStart.minute
                    
                    let hourE = componentsEnd.hour
                    let minuteE = componentsEnd.minute
                    
                    var hourS_Show:String = hourS.description
                    var hourE_Show:String = hourE.description
                    var minuteS_Show:String = minuteS.description
                    var minuteE_Show:String = minuteE.description
                    
                    if hourS < 10
                    {
                        hourS_Show = "0\(hourS)"
                    }
                    if hourE < 10
                    {
                        hourE_Show = "0\(hourE)"
                    }
                    if minuteS < 10
                    {
                        minuteS_Show = "0\(minuteS)"
                    }
                    if minuteE < 10
                    {
                        minuteE_Show = "0\(minuteE)"
                    }
                    
                    if arrHoursInt[indexPath.row / 2] == hourS
                    {
                        if minuteS > 0// שעת התחלה גדולה מ-0
                        {
                            cell.setDisplayDataDay("", descTop: "", hourBottom: "\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descBottom: "\(item.objProviderServiceDetail.nvServiceName) \(item.nvSupplierName)")
                            
                            
                            if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                            {
                                minute = Int(minuteE) - Int(minuteS)//קבלת הזמן של הארוע
                                hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                minute1 = Int(minuteS)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(false, heightTop: (CGFloat(minute1) / 60), heightButtom: (CGFloat(minute) / 60), eventKind: 1)
                                //לעשות שקוף לטופ לצבוע את הבטם
                            }
                                
                            else
                            {
                                minute = 60 - Int(minuteS)
                                hightViewBlue = ((CGFloat(minute) / 60) *
                                    hightCell)
                                minute1 = Int(minuteS)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(false,heightTop: (CGFloat(minute1) / 60), heightButtom:
                                    (CGFloat(minute) / 60), eventKind: 1)
                                //לעשות שקוף לטופ לצבוע את הבטם
                            }
                            
                        }
                        else//שעת התחלה 00
                        {
                            cell.setDisplayDataDay("\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descTop: "\(item.objProviderServiceDetail.nvServiceName) \(item.nvSupplierName)", hourBottom: "", descBottom: "")
                            
                            if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                            {
                                minute = Int(minuteE)
                                hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                
                                minute1 = 60 - Int(minuteE)
                                hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                cell.hourStart = hourS_Show + ":" + minuteS_Show
                                cell.hourEnd = hourE_Show
                                cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 1)
                                
                                //לצבוע את הטופ
                            }
                            else
                            {
                                hightViewBlue = hightCell
                                hightViewClear = 0
                                cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, eventKind: 1)
                                //לצבוע את הטופ
                            }
                        }
                        
                    }
                    else if arrHoursInt[indexPath.row / 2] > hourS && arrHoursInt[indexPath.row / 2] < hourE//אמצע הארוע
                    {
                        
                        cell.setDisplayDataDay("", descTop: "", hourBottom: "", descBottom: "")
                        
                        hightViewBlue = hightCell
                        hightViewClear = 0
                        //                       cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, eventKind: 1)
                        //לצבוע את הטופ
                    }
                        
                    else if arrHoursInt[indexPath.row / 2] == hourE//סיום הארוע
                    {
                        cell.setDisplayDataDay( "", descTop: "", hourBottom: "", descBottom: "")
                        
                        minute = Int(minuteE)
                        hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                        minute1 = 60 - Int(minuteE)
                        hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                        //                        if minuteE == 0
                        //                        {
                        //                            cell.setDisplayViewsEvents(false, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 1)
                        //                        }
                        //                        else
                        //                        {
                        //                            cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 1)
                        //                        }
                        //לצבוע את הטופ
                    }
                }
                
                if hasEvent == true && Global.sharedInstance.isSyncWithGoogelCalendar == true
                {
                    //מעבר על הארועים האישיים מהיומן של המכשיר ליום זה והצגתם
                    for var eve in arrEventsCurrentDay
                    {
                        let componentsStart = calendar.components([.Hour, .Minute], fromDate: eve.startDate)
                        
                        let componentsEnd = calendar.components([.Hour, .Minute], fromDate: eve.endDate)
                        
                        let hourS = componentsStart.hour
                        let minuteS = componentsStart.minute
                        
                        let hourE = componentsEnd.hour
                        let minuteE = componentsEnd.minute
                        
                        var hourS_Show:String = hourS.description
                        var hourE_Show:String = hourE.description
                        var minuteS_Show:String = minuteS.description
                        var minuteE_Show:String = minuteE.description
                        
                        if hourS < 10
                        {
                            hourS_Show = "0\(hourS)"
                        }
                        if hourE < 10
                        {
                            hourE_Show = "0\(hourE)"
                        }
                        if minuteS < 10
                        {
                            minuteS_Show = "0\(minuteS)"
                        }
                        if minuteE < 10
                        {
                            minuteE_Show = "0\(minuteE)"
                        }
                        
                        if arrHoursInt[indexPath.row / 2] == hourS
                        {
                            if minuteS > 0// שעת התחלה גדולה מ-0
                            {
                                cell.setDisplayDataDay("", descTop: "", hourBottom: "\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descBottom: eve.title)
                                
                                
                                if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                                {
                                    minute = Int(minuteE) - Int(minuteS)//קבלת הזמן של הארוע
                                    hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                    minute1 = Int(minuteS)
                                    hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                    cell.hourStart = hourS_Show + ":" + minuteS_Show
                                    cell.hourEnd = hourE_Show
                                    cell.setDisplayViewsEvents(false, heightTop: (CGFloat(minute1) / 60), heightButtom: (CGFloat(minute) / 60), eventKind: 0)
                                    //לעשות שקוף לטופ לצבוע את הבטם
                                }
                                    
                                else
                                {
                                    minute = 60 - Int(minuteS)
                                    hightViewBlue = ((CGFloat(minute) / 60) *
                                        hightCell)
                                    minute1 = Int(minuteS)
                                    hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                    cell.hourStart = hourS_Show + ":" + minuteS_Show
                                    cell.hourEnd = hourE_Show
                                    cell.setDisplayViewsEvents(false,heightTop: (CGFloat(minute1) / 60), heightButtom:
                                        (CGFloat(minute) / 60), eventKind: 0)
                                    //לעשות שקוף לטופ לצבוע את הבטם
                                }
                                
                            }
                            else//שעת התחלה 00
                            {
                                cell.setDisplayDataDay("\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)", descTop: eve.title, hourBottom: "", descBottom: "")
                                
                                if hourE == hourS//אם שעת סיום זהה לשעת התחלה
                                {
                                    minute = Int(minuteE)
                                    hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                                    
                                    minute1 = 60 - Int(minuteE)
                                    hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                                    cell.hourStart = hourS_Show + ":" + minuteS_Show
                                    cell.hourEnd = hourE_Show
                                    cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 0)
                                    
                                    //לצבוע את הטופ
                                }
                                else
                                {
                                    hightViewBlue = hightCell
                                    hightViewClear = 0
                                    cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, eventKind: 0)
                                    //לצבוע את הטופ
                                }
                            }
                            
                        }
                        else if arrHoursInt[indexPath.row / 2] > hourS && arrHoursInt[indexPath.row / 2] < hourE//אמצע הארוע
                        {
                            
                            cell.setDisplayDataDay("", descTop: "", hourBottom: "", descBottom: "")
                            
                            hightViewBlue = hightCell
                            hightViewClear = 0
                            //                            cell.setDisplayViewsEvents(true, heightTop: 1, heightButtom: 0, eventKind: 0)
                            //לצבוע את הטופ
                        }
                            
                        else if arrHoursInt[indexPath.row / 2] == hourE//סיום הארוע
                        {
                            cell.setDisplayDataDay( "", descTop: "", hourBottom: "", descBottom: "")
                            
                            minute = Int(minuteE)
                            hightViewBlue = ((CGFloat(minute) / 60) * hightCell)
                            minute1 = 60 - Int(minuteE)
                            hightViewClear = ((CGFloat(minute1) / 60) * hightCell)
                            //                            if minuteE == 0
                            //                            {
                            //                                cell.setDisplayViewsEvents(false, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 0)
                            //                            }
                            //                            else
                            //                            {
                            //                                cell.setDisplayViewsEvents(true, heightTop: (CGFloat(minute) / 60), heightButtom: (CGFloat(minute1) / 60), eventKind: 0)
                            //                            }
                            
                            //לצבוע את הטופ
                        }
                        
                    }
                }
                else if isShowFreeDay == 0
                {
                    // cell.btnOPenOrderOutlet.enabled = false
                    
                    cell.setDisplayDataDay("", descTop: "", hourBottom: "", descBottom: "")
                    return cell
                }
                
                
            }
            
            //        if indexPath.row == 47
            //        {
            //            showEventsViews()
            //
            //        }
            //            if selectedCellIndexPath == indexPath
            //            {
            //                selectedCellIndexPath = nil
            //            }
            return cell
        }
        
        func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 48//2*24
        }
        
        func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            
            if indexPath.row % 2 == 0//העמודה הראשונה
            {
                if fIsBig == true//selectedCellIndexPath == indexPath
                {
                    return CGSize(width: view.frame.size.width / 8, height:  (view.frame.size.width / 8) + 100)
                }
                return CGSize(width: view.frame.size.width / 8, height:  view.frame.size.width / 8)
            }
            if fIsBig == true//selectedCellIndexPath == indexPath
            {
                //selectedCellIndexPath = nil
                return CGSize(width: view.frame.size.width - (view.frame.size.width / 8), height:  (view.frame.size.width / 8) + 100)
                
            }
            return CGSize(width: view.frame.size.width - (view.frame.size.width / 8), height:  view.frame.size.width / 8)
        }
        
        func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
        {
            
            //            selectedCellIndexPath = indexPath
            //            colDay.reloadData()
            
            
        }
        
        func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
            if (touch.view!.isDescendantOfView(colDay)) {
                
                return false
            }
            return true
        }
        
        //    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        //        if gestureReconizer.state != UIGestureRecognizerState.Ended {
        //            return
        //        }
        //
        //        let p = gestureReconizer.locationInView(colDay)
        //        let indexPath = colDay.indexPathForItemAtPoint(p)
        //        selectedCellIndexPath = indexPath
        //        colDay.reloadData()
        //    }
        
        
        
        
        
        //        func dismissKeyboard() {
        //
        //            self.view.endEditing(true)
        //        }
        override func viewWillLayoutSubviews() {
            if Global.sharedInstance.isSyncWithGoogelCalendar == true{
                btnSuny.isCecked = true
            }else{
                btnSuny.isCecked = false
            }
            
        }
        //MARK: - ScrollView
        
        //    func scrollViewDidScroll(scrollView: UIScrollView) {
        //
        //        colDay.reloadData()
        //
        //    }
        
        //פונקציה הבודקת האם לצבוע את הויו אותו היא מקבלת
        //מקבלת:
        //view: הויו אותו יש לצבוע
        //from,to:לשם הבדיקה האם נמצא בטווח של שעה פנויה - האם לצבוע
        //param: כאשר הוא 0 נבדוק האם נמצא בטווח של הדקות,
        //כאשר הוא 1 נבדוק האם גדול מ from
        //וכאשר הוא 2 נבדוק האם קטן מ to
        //index: המיקום של השעה הפנויה במערך
        
        
        func showView(view:UIView,from:Int,to:Int,param:Int, index:Int,cell:EventsWeek12ViewsCollectionViewCell)
        {
            if param == 0
            {
                if view.tag >= from && view.tag <= to
                {
                    view.backgroundColor = Colors.sharedInstance.color4
                    addRightLeftBorder(view)
                    view.alpha = 0.5
                    
                    saveFreeHourForView(view.tag, index: index, cell: cell)
                    
                    if firstViewInFreeHour == -1
                    {
                        firstViewInFreeHour = view.tag
                    }
                }
            }
            else if param == 1
            {
                if view.tag >= from
                {
                    view.backgroundColor = Colors.sharedInstance.color4
                    addRightLeftBorder(view)
                    view.alpha = 0.5
                    
                    saveFreeHourForView(view.tag, index: index, cell: cell)
                    
                    if firstViewInFreeHour == -1
                    {
                        firstViewInFreeHour = view.tag
                    }
                }
            }
            else if param == 2
            {
                if view.tag <= to
                {//10-15
                    view.backgroundColor = Colors.sharedInstance.color4
                    addRightLeftBorder(view)
                    if view.tag == to || (view.tag < to && (view.tag + 5) > to)
                    {
                        addBottomBorder(view)
                    }
                    
                    view.alpha = 0.5
                    saveFreeHourForView(view.tag, index: index, cell: cell)
                }
            }
        }
        
        //פונקציה זו שומרת בטג של הלייבל (1 מתוך 12) את המיקום במערך של השעה הפנויה בה הוא נמצא.
        //הפונקציה מקבלת טג של הויו כדי לזהות את הליבל וכן אינדקס של המערך.
        func saveFreeHourForView(tag:Int,index:Int, cell:EventsWeek12ViewsCollectionViewCell)
        {
            switch tag {
            case 0:
                cell.lblHours1.tag = index
            case 5:
                cell.lblHours2.tag = index
            case 10:
                cell.lblHours3.tag = index
            case 15:
                cell.lblHours4.tag = index
            case 20:
                cell.lblHours5.tag = index
            case 25:
                cell.lblHours6.tag = index
            case 30:
                cell.lblHours7.tag = index
            case 35:
                cell.lblHours8.tag = index
            case 40:
                cell.lblHours9.tag = index
            case 45:
                cell.lblHours10.tag = index
            case 50:
                cell.lblHours11.tag = index
            case 55:
                cell.lblHours12.tag = index
                
            default:
                cell.lblHours1.tag = index
            }
            
        }
        
        func showSync()
        {
            if btnSuny.isCecked == false
            {
                Global.sharedInstance.getEventsFromMyCalendar()
                btnSuny.isCecked = true
                Global.sharedInstance.isSyncWithGoogelCalendar = true
                colDay.reloadData()
            }
            else
            {
                btnSuny.isCecked = false
                Global.sharedInstance.isSyncWithGoogelCalendar = false
                colDay.reloadData()
            }
            
        }
        
        func handlePinch(recognizer: UIPinchGestureRecognizer) {
            if recognizer.state == .Began {
                
                pointOne = recognizer.locationOfTouch(0, inView: colDay)
                pointTwo = recognizer.locationOfTouch(1, inView: colDay)
                
                if pointOneLast == CGPoint()
                {
                    fIsBig = true
                    pointOneLast = pointOne
                }
                else
                {
                    if pointOneLast.y < pointOne.y
                    {
                        fIsBig = false
                    }
                    else
                    {
                        fIsBig = true
                    }
                    pointOneLast = pointOne
                }
                //selectedCellIndexPath = indexPath
                colDay.reloadData()
            }
            if recognizer.state == .Changed
                && pinchInProgress
                && recognizer.numberOfTouches() == 2 {
                // pinchChanged(recognizer)
                print("changed")
            }
            if recognizer.state == .Ended {
                // pinchEnded(recognizer)
                print("ended")
            }
        }
        func checkDevice()
        {
            if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
                //            let fontSize:CGFloat = self.lblDay.font.pointSize;
                //            let fontSize1:CGFloat = self.lblDate.font.pointSize;
                //            lblDay.backgroundColor = UIColor.redColor()
                //            lblDate.backgroundColor = UIColor.redColor()
                
                lblDay.font = UIFont(name: lblDay.font.fontName, size: 20)
                lblDate.font = UIFont(name: lblDate.font.fontName, size: 13)
                
            }
            
        }
        //MARK: borders
        //הוספת מסגרת לאירוע
        
        func addRightLeftBorder(myView:UIView)
        {
            let leftBorder = UIView()
            leftBorder.frame = CGRectMake(0, 0, 3, myView.frame.size.height)
            leftBorder.backgroundColor = Colors.sharedInstance.color6
            myView.addSubview(leftBorder)
            
            let rightBorder = UIView()
            rightBorder.frame = CGRectMake(view.frame.size.width - (view.frame.size.width / 8) - 3, 0, 3, myView.frame.size.height)
            rightBorder.backgroundColor = Colors.sharedInstance.color6
            myView.addSubview(rightBorder)
        }
        
        func addBottomBorder(myView:UIView)
        {
            let bottomBorder = UIView()
            bottomBorder.backgroundColor = Colors.sharedInstance.color6
            bottomBorder.frame = CGRectMake(0, myView.frame.size.height - 3, view.frame.size.width - (view.frame.size.width / 8), 3)
            myView.addSubview(bottomBorder)
            
        }
}

