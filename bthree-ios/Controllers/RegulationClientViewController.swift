//
//  RegulationClientViewController.swift
//  Bthere
//
//  Created by Racheli Kroiz on 26.10.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//תקנון ותנאי השימוש
class RegulationClientViewController: NavigationModelViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = NSLocalizedString("REGULATION_TITLE", comment: "")
        txtView.text = NSLocalizedString("REGULATION_VIEW", comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}