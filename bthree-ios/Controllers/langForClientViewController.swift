//
//  langForClientViewController.swift
//  Bthere
//
//  Created by User on 10.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit


protocol changeLanguageDelegate{
    func changeLanguage(iLang:String)
}
class langForClientViewController: NavigationModelViewController ,UITableViewDataSource,UITableViewDelegate,deleteMessageDelegate,changeLanguageDelegate{
    
    var arrayLanguages:Array<String> = ["עברית","English","русский","العربية"]
    var arrayLanguagesKey:Array<String> = ["he","en","ru","ar"]
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var indexLanguageInIphone = 0//שומר את המיקום של שפת האייפון שבמכשיר

    var section = 0
    var langSelected = ""
    
    @IBOutlet weak var tblLang: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBAction func btnSave(sender: AnyObject) {
        
        self.changeLanguage(langSelected)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = NSLocalizedString("LANG", comment: "")
        btnSave.setTitle(NSLocalizedString("SAVE_BTN", comment: ""), forState: .Normal)
        
        switch language
        {
            case "he"://עברית
            indexLanguageInIphone = 0
            section = 0
            break
            case "en"://אנגלית
                 indexLanguageInIphone = 1
                 section = 1
            break
            case "ru"://רוסית
                indexLanguageInIphone = 2
                section = 2
            break
            case "ar"://ערבית
                indexLanguageInIphone = 3
                section = 3
            break
        default:
            break
        }
        
        tblLang.separatorStyle = .None
        tblLang.scrollEnabled = false
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg-pic-supplier@x1.jpg")!)
      
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("lagnForClientTableViewCell")as!lagnForClientTableViewCell
        if indexPath.row == self.section//&& indexPath.row == indexLanguageInIphone
        
        {
            cell.setDisplayData(arrayLanguages[indexPath.row],tag: 1)
        }
        else
        {
            cell.setDisplayData(arrayLanguages[indexPath.row],tag: 0)
        }
        cell.selectionStyle = .None
        cell.row = indexPath.row
        cell.delegate = self
//        cell.delegateChangeLanguage = self
        if indexPath.row != self.section{
            cell.viewLang.backgroundColor = UIColor.clearColor()
            cell.lblLang.textColor = UIColor.blackColor()
            cell.viewLang.tag = 0
        }
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        if (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.tag == 0
        {
            (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.backgroundColor = Colors.sharedInstance.color3
            (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).lblLang.textColor = UIColor.whiteColor()
            (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.tag = 1
            deleteMessage(indexPath.row)
            langSelected = arrayLanguagesKey[indexPath.row]
        }
        else
        {
            if (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.tag == 0
            {
                (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.backgroundColor = UIColor.clearColor()
                (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).lblLang.textColor = UIColor.blackColor()
                (tableView.cellForRowAtIndexPath(indexPath) as! lagnForClientTableViewCell).viewLang.tag = 0
            }
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return view.frame.size.height*0.1
    }
    //clear the language which selected before the current selection
    //get the section of current selected language
    func deleteMessage(section:Int){
        self.section = section
        tblLang.reloadData()
    }
    
    func changeLanguage(iLang:String)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.changeLanguage(iLang)
        Alert.sharedInstance.showAlert(NSLocalizedString("LANG_CHANGE_NEXR_ENTRANCE", comment: ""),vc: self)
    }
}
