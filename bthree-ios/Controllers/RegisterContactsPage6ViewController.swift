//
//  RegisterContactsPage6ViewController.swift
//  BThere
//
//  Created by Lior Ronen on 2/8/16.
//  Copyright © 2016 Sara Zingul. All rights reserved.
//

import UIKit
//דף 5 בהרשמה סנכרון עם אנשי קשר (לבדוק אם משתמשים)
class RegisterContactsPage6ViewController: UIViewController {
    
    let imageV:UIImage = UIImage(named: "OK-select-strock-black.png")! //"34.png"
    //let imageX:UIImage = UIImage(named: "35.png")!
    let imageX:UIImage = UIImage(named: "cancel-select-strock.png")!
    
    let imageV1:UIImage = UIImage(named: "OK-strock-black.png")!//34-.png
    let imageX1:UIImage = UIImage(named: "35-.png")!
    var flagSync = false
    var flagApproval = false
    var isFirst1 = true
    var isFirst2 = true
    
    var phones:Array<String> = []
    
    
    
    @IBOutlet weak var btnSyncV: UIButton!
    
    @IBAction func btnSyncV(sender: AnyObject) {
        
        if flagSync == false
        {
            for con in Global.sharedInstance.contactList
            {
                phones.append(con.nvPhone)
            }
            
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
            
            flagSync = true
            btnSyncV.setBackgroundImage(imageV, forState: .Normal)
            btnSyncX.setBackgroundImage(imageX1, forState: .Normal)
            
            ///Global.sharedInstance.dicSyncContacts["bAutoApproval"] = false
            
            isFirst2 = false
            flagApproval = false
            btnApprovalX.setBackgroundImage(imageX, forState: .Normal)
            btnApprovalV.setBackgroundImage(imageV1, forState: .Normal)
            
            ////סימון הכפתור השני בהתאם למה שנבחר פה
            for con in Global.sharedInstance.contactList
            {
                phones.append(con.nvPhone)
            }
            
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
            
            flagSync = true
            btnSyncV.setBackgroundImage(imageV, forState: .Normal)
            btnSyncX.setBackgroundImage(imageX1, forState: .Normal)
        }
        else
        {
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = []
            flagSync = false
            btnSyncV.setBackgroundImage(imageV1, forState: .Normal)
            btnSyncX.setBackgroundImage(imageX, forState: .Normal)
            
            /////
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = true
            
            flagApproval = true
            btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
            btnApprovalX.setBackgroundImage(imageX1, forState: .Normal)
        }
    }
    
    @IBOutlet weak var btnSyncX: UIButton!
    
    @IBAction func btnSyncX(sender: AnyObject) {
        
        if flagSync == true || isFirst1 == true
        {
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = []
            
            isFirst1 = false
            flagSync = false
            btnSyncX.setBackgroundImage(imageX, forState: .Normal)
            btnSyncV.setBackgroundImage(imageV1, forState: .Normal)
            
            ////סימון השני בהתאם למה שנבחר פה
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = true
            
            flagApproval = true
            btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
            btnApprovalX.setBackgroundImage(imageX1, forState: .Normal)
        }
        else
        {
            for con in Global.sharedInstance.contactList
            {
                phones.append(con.nvPhone)
            }
            
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
            
            flagSync = true
            btnSyncX.setBackgroundImage(imageX1, forState: .Normal)
            btnSyncV.setBackgroundImage(imageV, forState: .Normal)
            
            ////סימון הכפתור השני בהתאם למה שנבחר פה
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = false
            
            isFirst2 = false
            flagApproval = false
            btnApprovalX.setBackgroundImage(imageX, forState: .Normal)
            btnApprovalV.setBackgroundImage(imageV1, forState: .Normal)
        }
    }
    
    @IBOutlet weak var btnApprovalV: UIButton!
    
    
    @IBAction func btnApprovalV(sender: AnyObject) {
        
        btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
        
        
        if flagApproval == false
        {
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = true
            
            flagApproval = true
            btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
            btnApprovalX.setBackgroundImage(imageX1, forState: .Normal)
            
            ////סימון בחירה ידנית ב-x
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = []
            isFirst1 = false
            flagSync = false
            btnSyncX.setBackgroundImage(imageX, forState: .Normal)
            btnSyncV.setBackgroundImage(imageV1, forState: .Normal)
        }
        else
        {
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = false
            
            flagApproval = false
            btnApprovalV.setBackgroundImage(imageV1, forState: .Normal)
            btnApprovalX.setBackgroundImage(imageX, forState: .Normal)
            
            //////
            for con in Global.sharedInstance.contactList
            {
                phones.append(con.nvPhone)
            }
            
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
            
            flagSync = true
            btnSyncV.setBackgroundImage(imageV, forState: .Normal)
            btnSyncX.setBackgroundImage(imageX1, forState: .Normal)
            
            ///Global.sharedInstance.dicSyncContacts["bAutoApproval"] = false
            
            isFirst2 = false
            flagApproval = false
            btnApprovalX.setBackgroundImage(imageX, forState: .Normal)
            btnApprovalV.setBackgroundImage(imageV1, forState: .Normal)
            
        }
    }
    
    @IBOutlet weak var btnApprovalX: UIButton!
    
    @IBAction func btnApprovalX(sender: AnyObject)
    {
        if flagApproval == true || isFirst2 == true
        {
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = false
            
            isFirst2 = false
            flagApproval = false
            btnApprovalX.setBackgroundImage(imageX, forState: .Normal)
            btnApprovalV.setBackgroundImage(imageV1, forState: .Normal)
            
            ////
            for con in Global.sharedInstance.contactList
            {
                phones.append(con.nvPhone)
            }
            
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
            
            flagSync = true
            btnSyncV.setBackgroundImage(imageV, forState: .Normal)
            btnSyncX.setBackgroundImage(imageX1, forState: .Normal)
        }
        else
        {
            Global.sharedInstance.dicSyncContacts["bAutoApproval"] = true
            
            flagApproval = true
            btnApprovalX.setBackgroundImage(imageX1, forState: .Normal)
            btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
            
            ////סימון בחירה ידנית ב-x
            Global.sharedInstance.dicSyncContacts["nvPhoneList"] = []
            isFirst1 = false
            flagSync = false
            btnSyncX.setBackgroundImage(imageX, forState: .Normal)
            btnSyncV.setBackgroundImage(imageV1, forState: .Normal)
        }
    }
    
    @IBOutlet weak var lblSync: UILabel!
    
    @IBOutlet weak var lblApprovalAll: UILabel!
    
    
    @IBOutlet var textViewExplain: UITextView!
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    
    
    // MARK: - initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.sharedInstance.setContacts()
        
        lblSync.text = NSLocalizedString("ASK_FOR_SYNC", comment: "")
        lblApprovalAll.text = NSLocalizedString("APPROVAL_ALL", comment: "")
        textViewExplain.text = NSLocalizedString("SYNC_EXPLAIN", comment: "")
    }
    
    
    override func viewWillAppear(animated: Bool) {
        textViewExplain.selectable = false
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //בחירה דיפולטיבית
        Global.sharedInstance.dicSyncContacts["bAutoApproval"] = true
        
        flagApproval = true
        btnApprovalV.setBackgroundImage(imageV, forState: .Normal)
        btnApprovalX.setBackgroundImage(imageX1, forState: .Normal)
        
        ////סימון בחירה ידנית ב-x
        Global.sharedInstance.dicSyncContacts["nvPhoneList"] = []
        isFirst1 = false
        flagSync = false
        btnSyncX.setBackgroundImage(imageX, forState: .Normal)
        btnSyncV.setBackgroundImage(imageV1, forState: .Normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
