//
//  personalDetailsViewController.swift
//  Bthere
//
//  Created by User on 9.8.2016.
//  Copyright © 2016 Webit. All rights reserved.


import UIKit
//פרטים אישיים-לקוח קיים
class personalDetailsViewController:NavigationModelViewController, UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate, UIImagePickerControllerDelegate,WDImagePickerDelegate,UINavigationControllerDelegate,popUpPhoneDelegate
 {
    //MARK - Outlet
    
    @IBOutlet weak var lbladvertising: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnImgPerson: UIButton!
    @IBOutlet weak var dp: UIDatePicker!
    @IBOutlet weak var viewSyncCalendar: UIView!
    @IBOutlet weak var viewWeddingDate: UIView!
    @IBOutlet weak var viewDateBurn: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var btnIn: UIButton!
    @IBOutlet weak var viewSync: UIView!
    @IBOutlet weak var btnOpenTbl: UIButton!
    @IBOutlet weak var tblSelectDesigned: UITableView!
    @IBOutlet weak var btnSelect: CheckBoxForExistSupplierOk!
    @IBOutlet weak var btnNoSelect: CheckBoxForExistSupplierCancel!
    
    @IBAction func btnSelectAction(sender: AnyObject) {
        
            if(btnSelect.isCecked == true)
            {
                btnSelect.isCecked = false
                btnNoSelect.isCecked = true
                currentUserToEdit.bIsGoogleCalendarSync = false
            }
            else
            {
                btnSelect.isCecked = true
                btnNoSelect.isCecked = false
                currentUserToEdit.bIsGoogleCalendarSync = true
            }
    }
    
    @IBAction func btnNoSelectAction(sender: AnyObject) {
        if(btnNoSelect.isCecked == true)
        {
            btnNoSelect.isCecked = false
            btnSelect.isCecked = true
            currentUserToEdit.bIsGoogleCalendarSync = true
        }
        else
        {
            btnNoSelect.isCecked = true
            btnSelect.isCecked = false
            currentUserToEdit.bIsGoogleCalendarSync = false
        }
    }
    @IBOutlet weak var txtWeddingDate: UITextField!
    @IBOutlet weak var txtDateBurn: UITextField!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var viewDesign: UIView!
    @IBOutlet weak var plusTap: UIImageView!
    @IBOutlet weak var lblValidEmail: UILabel!
    @IBOutlet weak var lblValidPhone: UILabel!
    @IBOutlet weak var dpWedding: UIDatePicker!
    @IBOutlet weak var lblReqLastName: UILabel!
    @IBOutlet weak var lblReqFirstName: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblSync: UILabel!
    @IBOutlet weak var lblDesignCalendar: UILabel!
    
    //MARK: - Properties
    
    private var popoverController: UIPopoverController!
    private var imagePickerWD: WDImagePicker!
    
    var dateServerBorn = NSDate()
    var dateServerMarried = NSDate()
    
    var flag_FullName:Bool = false
    var flag_LastName:Bool = false
    var flag_Phone:Bool = false
    var flag_Email:Bool = false
    var flag_Date:Bool = false
    var generic:Generic = Generic()
    var isSaveClick:Bool = false
    var isCheckEmailEnd:Bool = false
    var isCheckPhoneEnd:Bool = false
    var isCheckNameEnd:Bool = false
    var isCheckLastNameEnd:Bool = false
    var currentUserToEdit:User = User()
    var userPhoneBeforeEdit = ""
    var userMailBeforeEdit = ""
    var arrGoodPhone:Array<Character> = Array<Character>()
    var arrayDeisgned:Array<String> = Array<String>()//["יומית","שבועית","חודשית"]
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        arrayDeisgned = Global.sharedInstance.arrayDicForTableViewInCell[2]![2]
        currentUserToEdit = Global.sharedInstance.currentUser
         dpWedding.hidden = true
        dp.backgroundColor = Colors.sharedInstance.color1
        dp.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dp.setValue(0.8, forKeyPath: "alpha")
        dp.datePickerMode = UIDatePickerMode.Date
        dp.setValue(false, forKey: "highlightsToday")
        dp.addTarget(self, action: #selector(personalDetailsViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        components.year = -80
        let minDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        components.year = -0
        let maxDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        self.dp.minimumDate = minDate
        self.dp.maximumDate = maxDate
        self.dpWedding.maximumDate = maxDate
        
        dpWedding.backgroundColor = Colors.sharedInstance.color1
        dpWedding.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpWedding.setValue(0.8, forKeyPath: "alpha")
        dpWedding.datePickerMode = UIDatePickerMode.Date
        dpWedding.setValue(false, forKey: "highlightsToday")
        dpWedding.addTarget(self, action: #selector(personalDetailsViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        lblReqLastName.hidden = true
        lblReqFirstName.hidden = true
        tblSelectDesigned.hidden = true
        btnImgPerson.enabled = false
        lblValidEmail.hidden = true
        lblValidPhone.hidden = true
        btnOpenTbl.layer.borderWidth = 1
        btnOpenTbl.layer.borderColor = UIColor.grayColor().CGColor
        self.btnOpenTbl.layer.shadowColor = UIColor.grayColor().CGColor
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)

        view.addGestureRecognizer(tap)
        self.btnOpenTbl.layer.shadowOpacity = 0.4
        self.btnOpenTbl.layer.shadowRadius = 1.5
        tblSelectDesigned.separatorStyle = .None
         btnIn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        dp.hidden = true
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        
        lblTitle.text = NSLocalizedString("PERSONAL_DETAILS_TITLE", comment: "")
        btnSave.setTitle(NSLocalizedString("SAVE_BTN", comment: ""), forState: .Normal)
        lbladvertising.text = NSLocalizedString("ADVERTISINGS", comment: "")
        lblSync.text = NSLocalizedString("SYNC_CALENDAR", comment: "")
        lblDesignCalendar.text = NSLocalizedString("CALENDAR_DESIGN", comment: "")
        
         txtFirstName.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("NAME_PD", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
         txtLastName.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("LAST_NAME_DP", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
         txtMail.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("MAIL_DP", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
         txtPhone.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("PHONE_USER", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
         txtDateBurn.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("DATEBURN_LBL", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
         txtWeddingDate.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("MARRIAGE_DATE", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 15)!])
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtMail.delegate = self
        txtWeddingDate.delegate = self
        txtPhone.delegate = self
        txtDateBurn.delegate =  self
        txtFirstName.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        txtDateBurn.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        txtLastName.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
                txtMail.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
                txtPhone.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
                txtWeddingDate.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        //אתחול לפי פרטי הבנ״א
        txtFirstName.text = Global.sharedInstance.currentUser.nvFirstName
        txtLastName.text = Global.sharedInstance.currentUser.nvLastName
        txtMail.text = Global.sharedInstance.currentUser.nvMail
        txtPhone.text = Global.sharedInstance.currentUser.nvPhone
        userPhoneBeforeEdit = Global.sharedInstance.currentUser.nvPhone
        userMailBeforeEdit = Global.sharedInstance.currentUser.nvMail
        if Global.sharedInstance.currentUser.iCalendarViewType == 0
        {
            btnIn.setTitle(arrayDeisgned[2], forState: .Normal)
        }
        else
        {
            btnIn.setTitle(SysTableRowString(12,id: Global.sharedInstance.currentUser.iCalendarViewType), forState: .Normal)
        }
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //dateFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX")
        
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        var componentsBDay = calendar.components([.Day, .Month, .Year], fromDate: Global.sharedInstance.currentUser.dBirthdate)
        
        if componentsCurrent.year == componentsBDay.year && componentsCurrent.month == componentsBDay.month && componentsCurrent.day == componentsBDay.day
        {
            txtDateBurn.text = ""
        }
        else
        {
            txtDateBurn.text = dateFormatter.stringFromDate(Global.sharedInstance.currentUser.dBirthdate)
        }
        componentsBDay = calendar.components([.Day, .Month, .Year], fromDate: Global.sharedInstance.currentUser.dMarriageDate)
        if componentsCurrent.year == componentsBDay.year && componentsCurrent.month == componentsBDay.month && componentsCurrent.day == componentsBDay.day
        {
            txtWeddingDate.text = ""
        }
        else
        {
            txtWeddingDate.text = dateFormatter.stringFromDate(Global.sharedInstance.currentUser.dMarriageDate)
        }
        
        img.layer.cornerRadius = 28
        img.clipsToBounds = true
        img.layer.borderWidth = 3
        img.layer.borderColor = UIColor.whiteColor().CGColor
    
        let dataDecoded:NSData = NSData(base64EncodedString: (Global.sharedInstance.currentUser.nvImage), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
       
        var decodedimage:UIImage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            
            decodedimage = UIImage(data: dataDecoded)!
            img.image = decodedimage
        }
        else
        {
            img.image = UIImage(named: "user-3.png")
        }
        if  Global.sharedInstance.currentUser.bIsGoogleCalendarSync == true{
           btnSelect.isCecked = true
            
        }
        else{
            btnNoSelect.isCecked = true
        }
    }
    override func viewDidAppear(animated: Bool) {
        Colors.sharedInstance.addTopAndBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewFirstName)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewLastName)
         Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewPhone)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1,width: 1, any: viewDesign)

         Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewEmail)
         Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewDateBurn)
         Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewWeddingDate)
         Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewSync)
       //  Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewLastName)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    func addShaddow(view:UIView) {
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = CGSizeZero
        view.layer.shadowRadius = 1.5
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayDeisgned.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("DescDesignTableViewCell")as!DescDesignTableViewCell
        cell.selectionStyle = .None
        cell.setDisplayData(arrayDeisgned[indexPath.row])
        if indexPath.row == 2{
            cell.viewButtom.hidden = true
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        btnIn.setTitle(arrayDeisgned[indexPath.row], forState: .Normal)
        tblSelectDesigned.hidden = true
        btnIn.tag = 0
        let stringSelected = (tableView.cellForRowAtIndexPath(indexPath) as! DescDesignTableViewCell).desc.text!
        currentUserToEdit.iCalendarViewType = SysTableRowId(12, str: stringSelected)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tblSelectDesigned.frame.size.height/3
    }
    
    @IBAction func btnOpenTable(sender: UIButton) {
        if sender.tag == 0{
            tblSelectDesigned.hidden = false
            sender.tag = 1
        }
        else{
            tblSelectDesigned.hidden = true
            sender.tag = 0
        }
    }
    
    //כדי שהטבלה הפנימית של תצוגת יומן תהיה לחיצה
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view!.isDescendantOfView(tblSelectDesigned)  {
            
            return false
        }
        return true
    }

    @IBAction func btnOpenCamera(sender: UIButton) {
       // setImageBig(viewBigImage, hidden: true)
        //txtAddress.dismissSuggestionTableView()
        Global.sharedInstance.isCamera = true
        ImagesCamera()
    }
    
    
    @IBAction func btnClickNoSelect(sender: CheckBoxForExistSupplierCancel) {
        btnSelect.isCecked = false
    }
    @IBAction func btnClickSelect(sender: CheckBoxForExistSupplierOk) {
        btnNoSelect.isCecked = false
    }
    //שמור
    @IBAction func btnSave(sender: UIButton) {
        
        isSaveClick = true
        
        if txtFirstName.text != ""
        {
            validationName()
        }
        else if txtLastName.text != ""
        {
            validationLastName()
        }
        else if txtPhone.text != ""
        {
            self.validationPhone()
        }
        else if txtMail.text != ""
        {
            self.validationEmail()
        }
        else
        {
            self.validToUpdate()
        }
    }
    func saveUserDetails()
    {
        //set validation
        currentUserToEdit.iUserId = Global.sharedInstance.currentUser.iUserId
        
        currentUserToEdit.nvUserName = txtFirstName.text!
        currentUserToEdit.nvFirstName = txtFirstName.text!
        currentUserToEdit.nvLastName = txtLastName.text!
        currentUserToEdit.dBirthdate = dateServerBorn//Global.sharedInstance.getDateFromString(txtDateBurn.text!)
        currentUserToEdit.dMarriageDate = dateServerMarried//Global.sharedInstance.getDateFromString(txtWeddingDate.text!)
        currentUserToEdit.nvMail = txtMail.text!
        //        currentUserToEdit.nvAdress = txt.text!
        currentUserToEdit.nvPhone = txtPhone.text!
        if btnNoSelect.isCecked
        {
            currentUserToEdit.bIsGoogleCalendarSync = false
        }
        else
        {
            currentUserToEdit.bIsGoogleCalendarSync = true
        }
    }
    
    //עדכון הפרטים בשרת
    func UpdateUser()
    {
        var dicDicRegister:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
             var dicRegister:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicDicRegister = currentUserToEdit.getDicToEdit()
        dicRegister["objUser"] = dicDicRegister
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.UpdateUser(dicRegister, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1
                {
                    Alert.sharedInstance.showAlert("אירעה תקלה בשמירת הפרטים",vc: self)
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
//                    if self.userPhoneBeforeEdit != self.txtPhone.text//ערכו את הטלפון
//                    {
//                        let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let viewCon:MessageVerificationViewController = MainStoryboard.instantiateViewControllerWithIdentifier("MessageVerificationViewController") as! MessageVerificationViewController
//                        viewCon.phone = self.txtPhone.text!
//                        viewCon.currentUserToEdit = self.currentUserToEdit
//                        viewCon.isFromPersonalDetails = true
//                        viewCon.delegateShowPhone = self
//                        Global.sharedInstance.isFromRegister = false
//                        
//                        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
//                        self.presentViewController(viewCon, animated: true, completion: nil)
//                    }
//                    else
//                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("SUCCESS_SAVECHANGES", comment: ""), vc: self)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let frontviewcontroller:UINavigationController = UINavigationController()
                        
                        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
                        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
                        
                        frontviewcontroller.pushViewController(vc, animated: false)
                        
                        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                        
                        let mainRevealController = SWRevealViewController()
                        
                        mainRevealController.frontViewController = frontviewcontroller
                        mainRevealController.rearViewController = rearViewController
                        
                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                        window.rootViewController = mainRevealController
//                    }
                }
                
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }

    }
    func ImagesCamera()
    {
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.ActionSheet)
        /// to open camera
        
        //        if Global.sharedInstance.isCamera == true
        //        {
        settingsActionSheet.addAction(UIAlertAction(title: NSLocalizedString("OPEN_CAMERA", comment: ""), style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            
        }))
        //        }
        //        else
        //        {
        /// to open images file
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("OPEN_ALBUM", comment: ""), style:UIAlertActionStyle.Default, handler:{ (UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                //                    let imagePicker = UIImagePickerController()
                //                    imagePicker.delegate = self
                //                    imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                //                    imagePicker.allowsEditing = true
                //                    self.presentViewController(imagePicker, animated: true, completion: nil)
              self.showResizablePicker(self.btnImgPerson)
                
            }
            }
            
            ))
        
        //  }
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("CANCEL", comment: ""), style:UIAlertActionStyle.Cancel, handler:nil))
        self.presentViewController(settingsActionSheet, animated:true, completion:nil)
        
    }
    
  //  MARK:-------cropImage
    
    func showResizablePicker(button: UIView) {
        self.imagePickerWD = WDImagePicker()
        self.imagePickerWD.cropSize = CGSizeMake(280, 280)
        self.imagePickerWD.delegate = self
        self.imagePickerWD.resizableCropArea = true
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController = UIPopoverController(contentViewController: self.imagePickerWD.imagePickerController)
            self.popoverController.presentPopoverFromRect(button.frame, inView: self.view, permittedArrowDirections: .Any, animated: true)
        } else {
            self.presentViewController(self.imagePickerWD.imagePickerController, animated: true, completion: nil)
        }
    }
    
    func imagePicker(imagePicker: WDImagePicker, pickedImage: UIImage) {
        //  uploadImageLbl.text = "צפה"
        img.image = Global.sharedInstance.resizeImage(pickedImage, newWidth: pickedImage.size.width*0.5)
        //self.btnImgPerson.setImage(image, forState: UIControlState.Normal)
       
        
        // self.img.image = pickedImage
        self.hideImagePicker()
    }
    
    func hideImagePicker() {
        ImagesCamera()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController.dismissPopoverAnimated(true)
        } else {
            self.imagePickerWD.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
   
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
       img.image = image
        if picker.sourceType == UIImagePickerControllerSourceType.Camera
        {
            // self.img.image = self.cropToBounds(image, width: 0.6, height: 0.6)
          //  self.img.image = Global.sharedInstance.resizeImage( self.img.image!, newWidth: image.size.width*0.5)
         
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                self.popoverController.dismissPopoverAnimated(true)
            } else {
                picker.dismissViewControllerAnimated(true, completion: nil)
            }
            //
            //           self.showResizablePicker(self.viewImage)
        }
        else
        {
            img.image = Global.sharedInstance.resizeImage(image, newWidth: image.size.width*0.5)
            //btnImgPerson.setImage(image, forState: UIControlState.Normal)
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                self.popoverController.dismissPopoverAnimated(true)
            } else {
                picker.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = image
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage!, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    // MARK: - KeyBoard
    
    //=======================KeyBoard================
    
    
    func dismissKeyboard() {
        dp.hidden = true
        dpWedding.hidden = true
        //tableView.hidden = true
        view.endEditing(true)
    }
    
    // MARK: - TextField
    //=========================TextField==============
    //    func textFieldShouldReturn(textField: UITextField) -> Bool {
    //        self.view.endEditing(true)
    //        return false
    //    }
    //
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch(textField){
        case(txtFirstName):
            txtLastName.becomeFirstResponder()
        case(txtLastName):
            txtPhone.becomeFirstResponder()
        case(txtPhone):
            //txtAddress.becomeFirstResponder()
            //case(txtAddress):
            txtMail.becomeFirstResponder()
        case(txtMail):
              txtDateBurn.becomeFirstResponder()
        case(txtDateBurn):
            txtWeddingDate.becomeFirstResponder()
        case(txtWeddingDate):
            dismissKeyboard()
        default:
            txtFirstName.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
      
        
        if textField == txtDateBurn
        {
            textField.inputView = UIView()
            dp.hidden = false
            dp.setDate(NSDate(), animated: false)
        }
        else if textField == txtWeddingDate{
            textField.inputView = UIView()
            dpWedding.hidden = false
            dpWedding.setDate(NSDate(), animated: false)
        }
        else if textField == txtFirstName
        {
            //txtAddress.dismissSuggestionTableView()
            self.lblReqFirstName.hidden = true
        }
        else if textField == txtLastName
        {
            //txtAddress.dismissSuggestionTableView()
            self.lblReqLastName.hidden = true
        }
       
        else
        {
            dp.hidden = true
            
        }
        if textField != txtWeddingDate{
            dpWedding.hidden = true
        }
        isSaveClick = false
        
        checkValidation(textField)
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        switch(textField)
        {
        case txtFirstName:
            currentUserToEdit.nvFirstName = textField.text!
            dp.hidden = true
            dpWedding.hidden = true
        case txtLastName:
            currentUserToEdit.nvLastName = textField.text!
            dp.hidden = true
            dpWedding.hidden = true
            
        case txtPhone:
            currentUserToEdit.nvPhone = textField.text!
            lblValidPhone.hidden = true
            dp.hidden = true
            dpWedding.hidden = true
        case txtDateBurn:
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if textField.text! != ""
            {
                currentUserToEdit.dBirthdate = dateServerBorn//dateFormatter.dateFromString(textField.text!)!
            }
            dp.hidden = true
            dpWedding.hidden = true
        case txtWeddingDate:
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if textField.text! != ""
            {
                currentUserToEdit.dMarriageDate = dateServerMarried//dateFormatter.dateFromString(textField.text!)!
            }
            dp.hidden = true
            dpWedding.hidden = true
        case txtMail:
            currentUserToEdit.nvMail = textField.text!
            
        default:
            lblValidEmail.hidden = true
            dp.hidden = true
        }
    }

    func getStringFromDateString(dateString: String)-> NSDate
    {
        if dateString != ""
        {
            let lastName: String? = dateString
            let lastNam: String? = lastName
            
            let myDouble = Double(lastNam!)
            let date = NSDate(timeIntervalSince1970: myDouble!/1000.0)
            return date
        }
        return NSDate()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        fDidBegin = false
//        timer?.invalidate()
//        timer = nil
        if true {
            
           // timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.doDelayed), userInfo: string, repeats: false)
            
            
            //        if textField == txtAddress {
            //            googlePlacesAutocomplete.input = textField.text! + string
            //            googlePlacesAutocomplete.fetchPlaces()
            //        }
            
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            
            
            if textField == txtPhone
            {
                if startString.characters.count > 10
                {    txtPhone.resignFirstResponder()
                    Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_TEN_DIGITS", comment: ""), vc: self)
                    
                    //                dismissKeyboard()
                    return false
                }
                else
                {
                    return true
                }
            }
            return true
        }
    }
    
    //MARK: - Validation
    
    func checkValidation(textField:UITextField)
    {
        
        ///* הערה:בכל אחת מהבדיקות בדקתי גם האם הטקסטפילד לא שווה אלי בעצמי למקרה שהטקסטפילד לא תקין והא היה האחרון שערכתי ועכשיו אני רוצה להכנס אליו שוב לתקן אותו
        
        
        //================בדיקה מי ה-textField הקודם שמולא כדי לבדוק את תקינותו==============
        
        ///---------------------------------------בדיקת השם------------------------
        
        if flag_FullName == true && textField != txtFirstName && txtFirstName.text != ""
        {
            validationName()
            
            flag_FullName = false
        }
        
            ///-----------------------------בדיקת השם משפחה---------------------------
            
        else if flag_LastName == true && textField != txtLastName && txtLastName.text != ""
        {
            validationLastName()
            
            flag_LastName = false
        }
            
            ///-----------------------------בדיקת הטלפון-----------------------
            
        else if flag_Phone == true && textField != txtPhone && txtPhone.text != ""
        {
            validationPhone()
            
            flag_Phone = false
        }
            
            ///-----------------------------בדיקת הכתובת------------------------
            //        else if flag_address == true && textField != txtAddress && txtAddress.text != ""
            //        {
            //            validationAddress()
            //            flag_address = false
            //        }
            
            
            
            ///--------------------בדיקת המייל------------------------
            
        else if flag_Email == true && textField != txtMail && txtMail.text != ""
        {
            validationEmail()
            
            flag_Email = false
        }
        
        //=======================================================================
        
        //בדיקה מי ה-textField הנוכחי שנבחר
        //והדלקת דגל מתאים,כדי לדעת בפעם הבאה מי היה הקודם לצורך בדיקת תקינות
        switch textField
        {
        case txtFirstName:
            flag_FullName = true
            break
            
        case txtLastName:
            flag_LastName = true
            break
            
        case txtPhone:
            flag_Phone = true
            break
            
            //        case txtAddress:
            //            flag_address = true
            //            break
            
        case txtMail:
            flag_Email = true
            break
            
        case txtDateBurn:
            flag_Date = true
            break
            
        default:
            break
        }
    }
    //for first and last name
    func isValidName(input: String) -> Bool {
        var numSpace = 0
        for chr in input.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z")  && !(chr >= "א" && chr <= "ת") && !(chr == " "))  {
                return false
            }
            if chr == " "
            {
                numSpace += 1
            }
        }
        if numSpace == input.characters.count || numSpace == input.characters.count - 1// אם יש רק רווחים או רק אות אחת והשאר רווחים
        {
            return false
        }
        else if numSpace > 0
        {
            let arr = input.componentsSeparatedByString(" ")
            for word in arr {
                if word.characters.count == 1
                {
                    return false
                }
            }
        }
        return true
    }
    
    func validationName()
    {
        isCheckNameEnd = true
        
        if txtFirstName.text!.characters.count == 1 || isValidName(txtFirstName.text!) == false ///שם פרטי
        {
            lblReqFirstName.text = NSLocalizedString("ERROR_FNAME", comment: "")
            lblReqFirstName.hidden = false
            //lblFullName.textColor = UIColor.redColor()
            Global.sharedInstance.isValid_FullName = false
        }
        else
        {
            Global.sharedInstance.isValid_FullName = true
        }
        
        if self.isSaveClick == true
        {
            if txtLastName.text != ""
            {
                self.validationLastName()
            }
            else if txtPhone.text != ""
            {
                self.validationPhone()
            }
            else if txtMail.text != ""
            {
                self.validationEmail()
            }
            else
            {
                self.validToUpdate()
            }
            
        }
    }
    
    
    func validationLastName()
    {
        isCheckLastNameEnd = true
        
        if txtLastName.text!.characters.count == 1 || isValidName(txtLastName.text!) == false ///שם משפחה
        {
            lblReqLastName.text = NSLocalizedString("ERROR_LNAME", comment: "")
            lblReqLastName.hidden = false
            Global.sharedInstance.isValid_lName = false
        }
        else
        {
            Global.sharedInstance.isValid_lName = true
        }
        
        if self.isSaveClick == true
        {
            if txtPhone.text != ""
            {
                self.validationPhone()
            }
            else if txtMail.text != ""
            {
                self.validationEmail()
            }
            else
            {
                self.validToUpdate()
            }
        }
    }
    
    func validationPhone()
    {
        var flag = true
        arrGoodPhone = []
        var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
        dicPhone["nvPhone"] = txtPhone.text
        ///בדיקה שהטלפון לא קיים כבר במערכת
        
        self.generic.showNativeActivityIndicator(self)
        
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.CheckPhoneValidity(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.isCheckPhoneEnd = true
                
                if responseObject["Result"] as! Int == 0 && self.userPhoneBeforeEdit != self.txtPhone.text//קיים
                {
                    self.lblValidPhone.hidden = false
                    self.lblValidPhone.text = NSLocalizedString("PHONE_EXIST", comment: "")
                    Global.sharedInstance.isValid_Phone = false
                }
                else //לא קיים
                {
                    Global.sharedInstance.isValid_Phone = true
                }
                let index0 = self.txtPhone.text?.startIndex.advancedBy(0)
                let index1 = self.txtPhone.text?.startIndex.advancedBy(1)
                if self.txtPhone.text?.characters.count < 10 || (self.txtPhone.text?.characters[index0!] != "0" || self.txtPhone.text?.characters[index1!] != "5")
                {
                    Global.sharedInstance.isValid_Phone = false
                  self.lblValidPhone.text = NSLocalizedString("PHONE_ERROR", comment: "")
                   self.lblValidPhone.hidden = false
                    
                }
                
                //בדיקה שהטלפון תקין
                let specialCharacterRegEx  = "[*]?[0-9]+"
                let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
                let specialresult = texttest2.evaluateWithObject(self.txtPhone.text)
                
                
                if !specialresult && self.txtPhone.text != ""// לא תקין
                {
                    for char in (self.txtPhone.text?.characters)!
                    {
                        if (char >= "0" && char <= "9") || char == "*"
                        {
                            let c:Character = char
                            self.arrGoodPhone.append(c)
                        }
                            
                        else
                        {
                          self.lblValidPhone.text = NSLocalizedString("PHONE_ERROR", comment: "")
                            // self.lblPhone.textColor = UIColor.redColor()
                            self.lblValidPhone.hidden = false
                            self.txtPhone.text = ""
                            Global.sharedInstance.isValid_Phone = true
                            flag = false
                            break
                        }
                    }
                    
                    if flag == true
                    {
                        self.txtPhone.text = ""
                        for i in 0 ..< self.arrGoodPhone.count
                        {
                            self.txtPhone.text = self.txtPhone.text! + String(Array(self.arrGoodPhone)[i])
                        }
                        Global.sharedInstance.isValid_Phone = true
                    }
                }
                    
                else
                {
                    //אם לא היה תקין בגלל השרת
                    if Global.sharedInstance.isValid_Phone != false
                    {
                        Global.sharedInstance.isValid_Phone = true
                    }
                }
                
                ///אם הגיעו מהכפתור של הרשם
                if self.isSaveClick == true
                {
                    if self.txtMail.text != ""
                    {
                        self.validationEmail()
                    }
                    else
                    {
                        self.validToUpdate()
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    print("Error: ", NSError!.localizedDescription)
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }
    
    func validationEmail()
    {
        var dicMail:Dictionary<String,String> = Dictionary<String,String>()
        dicMail["nvEmail"] = txtMail.text
        
        ///בדיקה שהמייל לא קיים כבר במערכת
        self.generic.showNativeActivityIndicator(self)
        
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.CheckMailValidity(dicMail, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.isCheckEmailEnd = true
                
                if responseObject["Result"] as! Int == 0 && self.userMailBeforeEdit != self.txtMail.text//קיים
                {
                   self.lblValidEmail.hidden = false
                   self.lblValidEmail.text = NSLocalizedString("MAIL_EXIST", comment: "")
                    Global.sharedInstance.isValid_Email = false
                }
                else //לא קיים
                {
                    Global.sharedInstance.isValid_Email = true
                }
                
                ///בדיקה שהמייל תקין
                if !Validation.sharedInstance.mailValidation(
                    self.txtMail.text!) && self.txtMail.text != ""
                {
                   self.lblValidEmail.hidden = false
                    //self.lblEmail.textColor = UIColor.redColor()
                    //Alert.sharedInstance.showAlert(NSLocalizedString("MAIL_ERROR", comment: ""), vc: self)
                    self.lblValidEmail.text = NSLocalizedString("MAIL_ERROR", comment: "")
                    
                    Global.sharedInstance.isValid_Email = false
                }
                else //תקין
                {
                    //אם לא היה תקין בגלל השרת
                    if Global.sharedInstance.isValid_Email != false
                    {
                        Global.sharedInstance.isValid_Email = true
                    }
                }
                
                ///אם הגיעו מהכפתור של הרשם יש לבדוק אם אפשר לעבור עמוד
                if self.isSaveClick == true
                {
                    self.validToUpdate()
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    print("Error: ", NSError!.localizedDescription)
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }
    
    func validToUpdate()
    {
        var x = 0
        
        ///---------בדיקה שהשדות חובה מלאים
        
        if self.txtFirstName.text == ""
        {
            self.lblReqFirstName.hidden = false
            self.lblReqFirstName.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
        if self.txtLastName.text == ""
        {
            self.lblReqLastName.hidden = false
            self.lblReqLastName.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
        if self.txtMail.text == ""
        {
            self.lblValidEmail.hidden = false
            self.lblValidEmail.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
        if self.txtPhone.text == ""
        {
            self.lblValidPhone.hidden = false
            self.lblValidPhone.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
    
        if !Global.sharedInstance.isValid_FullName || !Global.sharedInstance.isValid_lName || !Global.sharedInstance.isValid_Phone || !Global.sharedInstance.isValid_Email
        {
            x = 1
        }
        
        if x == 0//הכל תקין
        {
            if self.img.image != nil
            {
                currentUserToEdit.nvImage = Global.sharedInstance.setImageToString(self.img.image!)
            }
            else
            {
                currentUserToEdit.nvImage = ""
            }
            if self.userPhoneBeforeEdit != self.txtPhone.text//ערכו את הטלפון
            {
                let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewCon:MessageVerificationViewController = MainStoryboard.instantiateViewControllerWithIdentifier("MessageVerificationViewController") as! MessageVerificationViewController
                viewCon.phone = self.txtPhone.text!
                viewCon.currentUserToEdit = self.currentUserToEdit
                viewCon.isFromPersonalDetails = true
                viewCon.delegateShowPhone = self
                Global.sharedInstance.isFromRegister = false
                
                viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                self.presentViewController(viewCon, animated: true, completion: nil)
            }
            else
            {
                UpdateUser()
            }
        }
    }
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if sender == dp
        {
            dateServerBorn = sender.date
            
            dateServerBorn = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: dateServerBorn
                , options: [])!
            
            txtDateBurn.text = dateFormatter.stringFromDate(sender.date)
        }
        else
        {
            dateServerMarried = sender.date
            
            dateServerMarried = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: dateServerMarried
                , options: [])!
            
            txtWeddingDate.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
    }
 

    func maskRoundedImage(image: UIImage, radius: Float) -> UIImage {
        let imageView: UIImageView = UIImageView(image: image)
        var layer: CALayer = CALayer()
        layer = imageView.layer
        
        layer.masksToBounds = true
        layer.cornerRadius = CGFloat(radius)
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
    
    //מחזיר את הקוד לשורה מסויימת מהטבלה שנבחרה-בשביל השליחה לשרת
    //מקבלת את קוד הטבלה אליה לגשת ואת הסטרינג שנבחר
    func SysTableRowId(iTableRowId:Int,str:String)->Int
    {
        for sys in Global.sharedInstance.dicSysAlerts[iTableRowId.description]!
        {
            if sys.iTableId == iTableRowId && sys.nvAletName == str
            {
                return sys.iSysTableRowId
            }
        }
        return 0
    }
    
    //מחזיר את ה-string לשורה מסויימת מהטבלה שנבחרה
    //מקבלת את קוד הטבלה אליה לגשת ואת קוד ה-string
    func SysTableRowString(iTableRowId:Int,id:Int)->String
    {
        for sys in Global.sharedInstance.dicSysAlerts[iTableRowId.description]!
        {
            if sys.iTableId == iTableRowId && sys.iSysTableRowId == id
            {
                return sys.nvAletName
            }
        }
        return ""
    }
    
    func openCustomer()
    {
        //קבלת פרטי הלקוח
        var dicGetCustomerDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicGetCustomerDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        self.generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.GetCustomerDetails(dicGetCustomerDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                {
                    
                    var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicForDefault["nvClientName"] = responseObject["Result"]!["nvFirstName"]
                    //שמירת שם הלקוח במכשיר
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "currentClintName")
                    
                    var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                    dicUserId["currentUserId"] = responseObject["Result"]!["iUserId"]
                    //שמירת הuserId- במכשיר
                    Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                    
                    Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    //בגלל שנשמר יום אחד קודם צריך להוסיף 3 שעות(זה לא עזר להוסיף timeZone)
                    Global.sharedInstance.currentUser.dBirthdate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dBirthdate
                        , options: [])!
                    Global.sharedInstance.currentUser.dMarriageDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dMarriageDate
                        , options: [])!
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    if AppDelegate.showAlertInAppDelegate == false
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        AppDelegate.showAlertInAppDelegate = true
                    }
            })
        }
    }
    
    func deleteTxtPhone()
    {
        txtPhone.text = ""
    }
}
