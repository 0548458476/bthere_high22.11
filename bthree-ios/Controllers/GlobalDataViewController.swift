
import UIKit

protocol validDataDelegate{
    func validData()
}
protocol reloadTblDelegate {
    func reloadTbl()
    func reloadHeight()
}

protocol scrollOnEditDelegate {
    func scrollOnEdit(keyBoardSize:CGRect,textField:UITextField)
    func scrollOnEndEdit(keyBoardSize:CGRect)
}
protocol saveDataToWorkerDelegate {
    func  saveDataToWorker()->Bool
}
protocol closeLastCellSelegate {
    func closeCellFromOtherCell()
}
protocol genericDelegate {
    func showGeneric()
    func hideGeneric()
}
//דף 2 בהרשמה נתונים כלליים
class GlobalDataViewController: UIViewController, UICollectionViewDataSource ,UICollectionViewDelegate ,UITableViewDataSource,UITableViewDelegate ,ReloadCollectionDelegate ,ReloadTableDelegate,UIGestureRecognizerDelegate,addRowForSection3Delegate,reloadTableForSaveDelegate,putSubDelegate,ReloadTableWorkersDelegate,reloadTableForNewWorkerDelegate,editServiceDelegate,reloadServicesTableDelegate,reloadTableForNewServiceDelegate,editWorkerDelegate,dismiss,viewErrorForFirstSectionDelegate,validSection1Delegate,validDataDelegate,scrollOnEditDelegate,reloadTblDelegate,closeLastCellSelegate,genericDelegate{
    
    //MARK: - Properties
    
    var generic:Generic = Generic()
    
    var delegateDP:setDatePickerRestDelegate!=nil
    var delegateTableAddress:tableViewAddressDelegate!=nil
    var delegateEnabledBtnDays:enabledBtnDaysDelegate!=nil
    
    var x  = 0
    var isOpen:Bool = false
    var a:Array<Bool> = []
    var flagsHelp:Array<Bool> = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,]
    //למקרה שסוגרים את העובד החדש ע״י לחיצה על סל אחר
    var newWorkerIsClose = false
    var newServiceIsClose = false
    
    var WorkersTbl:UITableView = UITableView()
    var ServicesTbl:UITableView = UITableView()
    var daysTbl:UITableView = UITableView()
    var hoursTbl:UITableView = UITableView()
    
    var indexPathWithRow:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    var indexPathWithRowRest:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    var days:Array<String> = [NSLocalizedString("SUNDAY_DAY", comment: ""),NSLocalizedString("MONDAY_DAY", comment: ""),NSLocalizedString("TUESDAY_DAY", comment: ""),NSLocalizedString("WEDNSDAY_DAY", comment: ""),NSLocalizedString("THIRTHDAY_DAY", comment: ""),NSLocalizedString("FRIDAY_DAY", comment: ""),NSLocalizedString("SHABAT_DAY", comment: "")]
    
    var indexPathCalendar:NSIndexPath = NSIndexPath(forRow: 0, inSection: 5)
    
    var headersCell:Array<String> =
        [NSLocalizedString("DOMAIN", comment: ""),NSLocalizedString("HOURS_ACTIVITY", comment: ""),NSLocalizedString("ADD_WORKERS", comment: ""),NSLocalizedString("SERVICE_PRODUCT", comment: ""),NSLocalizedString("TURN_SETTINGS", comment: "")]
    
    var selectedCellForEdit:Array<Bool> = []//מערך זה מיצג האם הסל פתוח או סגור
    var cellOpenForEdit:Dictionary<Int,Int> = [0:2,1:2,2:2,3:2,4:2,5:2]
    var notificationsRowsInSection:Dictionary<Int,Int> = [0:2,1:2,2:2,3:2,4:2]
   
    var flags:Array<Bool> = []
    
    var delegateLineForLabel:lineCountForLabelDelegate!=nil
    var delegateSaveWorker:saveDataToWorkerDelegate!=nil
    var delegateSaveBussines:saveDataToWorkerDelegate!=nil
    var delegateSaveCalendar:saveDataToWorkerDelegate!=nil
    var delegateActiveHours:hoursActiveDelegate!=nil
   
    @IBOutlet weak var hightTbl: NSLayoutConstraint!
    @IBOutlet weak var extTbl: UITableView!
    @IBOutlet weak var constHight: NSLayoutConstraint!
    var lastIndex:NSIndexPath = NSIndexPath(forRow: -1, inSection: 0)
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //לאפס את הדף בחזרה לדף
        setPropertiesOfHoursActiveToDefult()
        //אפוס נתונים
        Global.sharedInstance.hoursForWorker = false
        Global.sharedInstance.headersCellRequired = [false,false,true,false,true]
        Global.sharedInstance.currentEditCellOpen = -1
        Global.sharedInstance.isContinuPressed = false
        AppDelegate.countCellEditOpenService = 0
        Global.sharedInstance.selectedCellForEditService = []
        Global.sharedInstance.fromEdit = false
        Global.sharedInstance.countCellEditOpen = 0
        Global.sharedInstance.countCellHoursOpenFromEdit = 0
        Global.sharedInstance.ifOpenCell = false
        Global.sharedInstance.isClickEqual = false
        Global.sharedInstance.isClickNoEqual = false
        Global.sharedInstance.isOpenHours = false
        Global.sharedInstance.fSection5 = false
        Global.sharedInstance.isOpenWorker = false
        Global.sharedInstance.isOpenNewWorker = false
        Global.sharedInstance.isOpenHoursForPlus = false
        Global.sharedInstance.isOpenHoursForNewWorker = false
        Global.sharedInstance.isServiceProviderEditOpen = false
        Global.sharedInstance.isAllFlagsFalse = false
        Global.sharedInstance.isOpenHoursForPlusAction = false
        Global.sharedInstance.isNewService = false
        Global.sharedInstance.domainBuisness = ""
        Global.sharedInstance.generalDetails.arrObjServiceProviders = []
        Global.sharedInstance.generalDetails.arrObjProviderServices = []
        Global.sharedInstance.isFromFirstSave = false
        Global.sharedInstance.isFromSave = false
        
        ///----------------------------------------
        Global.sharedInstance.selectedCell = [false,false,false,false,false,false]
       ///----------------------------------------
        
        a = flagsHelp
        self.delegateSaveWorker = Global.sharedInstance.WorkerCell
        WorkersTbl.scrollEnabled = false
        Global.sharedInstance.delegateValidData = self
        Global.sharedInstance.GlobalDataVC = self
        
        if  DeviceType.IS_IPHONE_6P {
            Global.sharedInstance.heightForCell = 60
        }
        else{
            if    DeviceType.IS_IPHONE_6 {
                Global.sharedInstance.heightForCell = 54.5
            }
            else{
                if DeviceType.IS_IPHONE_5{
                    Global.sharedInstance.heightForCell = 46
                    
                }
                else{
                    Global.sharedInstance.heightForCell = 39.1
                }
            }
        }
        
        Global.sharedInstance.helperTable = extTbl
        extTbl.separatorStyle = .None
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GlobalDataViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        daysTbl.alwaysBounceVertical = false
        hoursTbl.alwaysBounceVertical = false
        
        Global.sharedInstance.GlobalDataVC = self
        Global.sharedInstance.rgisterModelViewController?.delegateCloseLast = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        print(Global.sharedInstance.generalDetails)
        
        if Global.sharedInstance.isFirst == true
        {
            Global.sharedInstance.isFirst = false
        }
        if Global.sharedInstance.isFirstBussinesServices == true
        {
            Global.sharedInstance.isFirstBussinesServices = false
        }
        if Global.sharedInstance.isFirstCalenderSetting == true
        {
            Global.sharedInstance.isFirstCalenderSetting = false
        }
        else
        {
            extTbl.reloadData()
        }
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        switch (tableView){
        case extTbl:
            return 5
        case daysTbl:
            return 1
        case hoursTbl:
            return 1
        case WorkersTbl:
            return Global.sharedInstance.generalDetails.arrObjServiceProviders.count
        case ServicesTbl:
            return Global.sharedInstance.generalDetails.arrObjProviderServices.count
        default:
            return 1
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == extTbl
        {
            if section == 1//שעות פעילות
            {
                if Global.sharedInstance.selectedCell[section] == true//פתוח
                {
                    //אם לא נבחרו שעות
                    if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                    {
                        Global.sharedInstance.isReturn2RowsForHours = false
                        return 3//שעות פעילות,דייט פיקר,הפסקות
                    }
                    else
                    {
                        Global.sharedInstance.isReturn2RowsForHours = false
                        return 4//שעות פעילות,דייט פיקר,הפסקות,הצגת השעות
                    }
                }
                else// Global.sharedInstance.selectedCell[section] = false
                {
                    //אם לא נבחרו שעות
                    if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                    {
                        Global.sharedInstance.isReturn2RowsForHours = false
                        return 1//רק את השעות פעילות
                    }
                    else
                    {
                        Global.sharedInstance.isReturn2RowsForHours = true
                        return 2//שעות פעילות + הצגת השעות שנבחרו
                    }
                }
            }
            
            if Global.sharedInstance.isFromSave == true && section == 2 && Global.sharedInstance.isOpenNewWorker == true && Global.sharedInstance.isOpenHoursForPlusAction == false//בפעם הראשונה כל פתיחת הוספת עובדים
            {
                return 4
            }
            if section == 2 && Global.sharedInstance.isOpenHoursForPlusAction == true//מלחיצה על הפלוס של הוספת עובד חדש
            {
                return 6
            }
            
            if Global.sharedInstance.isFromFirstSave == true && section == 3 && Global.sharedInstance.isNewService == true
            {
                return 4
                
            }
            if Global.sharedInstance.isFromFirstSave == true && section == 3 {
                return 3
            }
            
            if Global.sharedInstance.isFromSave == true && section == 2
            {
                return 3
            }
            if section == 2 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.hourShowFirstWorker == ""
            {
                return 4
            }
            else if section == 2 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.hourShowFirstWorker != ""
            {
                return 5
            }
            if Global.sharedInstance.selectedCell[section] == true
            {
                return notificationsRowsInSection[section]!
            }
            
            return 1
        }
        else if tableView == daysTbl{
            return days.count
        }
        else if tableView == hoursTbl{
            return days.count
        }
        else if tableView == WorkersTbl
        {
            if flagsHelp.count > 0
            {
                if selectedCellForEdit[section] == true && ( flagsHelp[section] == false || Global.sharedInstance.isOpenHoursForNewWorker == false) && Global.sharedInstance.isClickNoEqual == false{
                    return 2
                }
                if ( flagsHelp[section] == true  && selectedCellForEdit[section] == true
                    && Global.sharedInstance.isOpenHoursForNewWorker == true) || (Global.sharedInstance.isClickNoEqual == true && selectedCellForEdit[section] == true){
                    return 4
                }
                
            }
            else{
                if selectedCellForEdit[section] == true {
                    return 2
                }
                
            }
            return 1
        }
        else if tableView == ServicesTbl{
            if Global.sharedInstance.selectedCellForEditService[section] == true{
                
                return 2
                
            }
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView == extTbl
        {
            Global.sharedInstance.indexPathNew = indexPath
            
            switch (indexPath.row)
            {
            case 0:
                
                let cell:MainTableViewCell = tableView.dequeueReusableCellWithIdentifier("MainTableViewCell")as!MainTableViewCell
                
                cell.tag = indexPath.section
                cell.delegateKbCalendar = Global.sharedInstance.calendarSetting
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                if indexPath.section == Global.sharedInstance.currentEditCellOpen
                {
                    cell.setDisplayData(headersCell[indexPath.section],hidden: Global.sharedInstance.headersCellRequired[indexPath.section],imageArrow: "supplier-iphone-icons@x1-07.png",textArrow: "")
                }
                else
                {
                    cell.setDisplayData(headersCell[indexPath.section],hidden: Global.sharedInstance.headersCellRequired[indexPath.section],imageArrow: "",textArrow: "<")
                }
                
                
                if indexPath.section == 0
                {
                    cell.viewtop.hidden = false
                    //בשביל ה-placeHolder
                    cell.txtHolder.hidden = false
                    cell.txtHolder.userInteractionEnabled = false
                    
                    cell.txtHolder.attributedPlaceholder = NSAttributedString(string:headersCell[indexPath.section], attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 17)!])
                    
                    if Global.sharedInstance.domainBuisness != ""
                    {
                        cell.txtHolder.text = Global.sharedInstance.domainBuisness
                    }
                    else
                    {
                        cell.txtHolder.text = ""
                    }
                    cell.txtSub.hidden = true
                    cell.lblDesc.hidden = true
                }
                else
                {
                    cell.txtSub.enabled = false
                    cell.txtHolder.hidden = true
                    cell.txtSub.hidden = true
                    cell.lblDesc.hidden = false
                    cell.viewtop.hidden = true
                    
                    if indexPath.section != 1
                    {
                        cell.txtSub.text = ""
                    }
                }
                
                if Global.sharedInstance.isContinuPressed == true
                {
                    if Global.sharedInstance.isFirstSectionValid == false && indexPath.section == 0 {
                        cell.lblMessageError.text = NSLocalizedString("REQUIREFIELD", comment: "")
                        cell.lblMessageError.textColor = UIColor.redColor()
                        Global.sharedInstance.isFirstSectionValid = true
                    }
                        
                    else if !Global.sharedInstance.isHoursSelected.contains(true) && indexPath.section == 1
                    {
                        if Global.sharedInstance.isFirst == false
                        {
                            cell.lblMessageError.text = NSLocalizedString("REQUIREFIELD", comment: "")
                            cell.lblMessageError.textColor = UIColor.redColor()
                        }
                        else
                        {
                            cell.lblMessageError.textColor = UIColor.clearColor()
                        }
                    }
                        
                    else if !Global.sharedInstance.isValidHours && indexPath.section == 1
                    {
                        cell.lblMessageError.text = NSLocalizedString("ILLEGAL_HOURS", comment: "")
                        cell.lblMessageError.textColor = UIColor.redColor()
                    }
                        
                    else if (
                        Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0
                            && indexPath.section == 3) || (Global.sharedInstance.fIsSaveConBussinesServicesPressed == false && indexPath.section == 3)
                    {
                        if Global.sharedInstance.isFirstBussinesServices == false
                            
                        {
                            cell.lblMessageError.text = NSLocalizedString("REQUIREFIELD", comment: "")
                            cell.lblMessageError.textColor = UIColor.redColor()
                        }
                        else
                        {
                            cell.lblMessageError.textColor = UIColor.clearColor()
                        }
                    }
                        
                        
                    else{
                        cell.lblMessageError.text = ""
                    }
                }
                else
                {
                    cell.lblMessageError.textColor = UIColor.clearColor()
                }
                
                if cell.txtSub.text != ""{
                    cell.lblMessageError.textColor = UIColor.clearColor()
                    
                }
                
                if indexPath.section == 1
                {
                    if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                    {
                        cell.viewButtom.hidden = false
                    }
                    else
                    {
                        cell.viewButtom.hidden = true
                    }
                }
                else
                {
                    cell.viewButtom.hidden = false
                }
                return cell
                
            //אם לחצו עליו
            default:
                if Global.sharedInstance.fSection5 == true
                {
                    Global.sharedInstance.indexPathNew = NSIndexPath(forRow: Global.sharedInstance.indexPathNew!.row, inSection: 5)
                }
                
                switch(indexPath.section)
                {
                //תחום העסק
                case(0):
                    
                    let cell:Section2TableViewCell = tableView.dequeueReusableCellWithIdentifier("Section2TableViewCell")as!Section2TableViewCell
                    cell.delegat = self
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.setDisplayData()
                    return cell
                case (1)://שעות פעילות
                    
                    if Global.sharedInstance.addRecess == false//לא לחצו על הוסף הפסקות
                    {
                        if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                        {
                            if indexPath.row == 1
                            {
                                //שעות פעילות חדש
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = true
                                cell.dtFromHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])
                                cell.dtToHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                            else
                            {
                                //סל של הפסקות
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = false
                                
                                return cell
                            }
                        }
                        else//צריך להציג גם את הסל של הצגת השעות שנבחרו
                        {
                            if indexPath.row == 1
                            {
                                let cell:SelectedHoursTableViewCell = (tableView.dequeueReusableCellWithIdentifier("SelectedHoursTableViewCell") as? SelectedHoursTableViewCell)!
                                cell.delegateReloadTbl = self
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                if Global.sharedInstance.isReturn2RowsForHours == true
                                {
                                    cell.viewBottom.hidden = false
                                }
                                else
                                {
                                    cell.viewBottom.hidden = true
                                }
                                return cell
                            }
                            
                            if indexPath.row == 2
                            {
                                //שעות פעילות חדש
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = true
                                cell.btnSelectAllDays.tag = 1
                                cell.dtFromHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])
                                cell.dtToHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Year, value: -16, toDate: NSDate(), options: [])
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                            else
                            {
                                //סל של הפסקות
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = false
                                
                                return cell
                            }
                        }
                    }
                    else//לחצו על הוסף הפסקות
                    {
                        if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                        {
                            if indexPath.row == 1//הצגת הסל של הוסף הפסקות
                            {
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = true
                                
                                return cell
                            }
                            else//הצגת השעות
                            {
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = false
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                        }
                        else
                        {
                            if indexPath.row == 1//הצגת השעות שנבחרו
                            {
                                let cell:SelectedHoursTableViewCell = (tableView.dequeueReusableCellWithIdentifier("SelectedHoursTableViewCell") as? SelectedHoursTableViewCell)!
                                cell.delegateReloadTbl = self
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.viewBottom.hidden = true
                                return cell
                            }
                            if indexPath.row == 2//הצגת הסל של הוסף הפסקות
                            {
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = true
                                
                                return cell
                            }
                            else//הצגת השעות
                            {
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = false
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                        }
                    }
                    
                    
                case (2):
                    if Global.sharedInstance.isFromSave == false && Global.sharedInstance.isReloadForEdit == false
                    {
                        if indexPath.row == 1
                        {
                            
                            let cell:ItemInSection3TableViewCell = tableView.dequeueReusableCellWithIdentifier("ItemInSection3TableViewCell")as!ItemInSection3TableViewCell
                            cell.delegateGeneric = self
                            Global.sharedInstance.cutrrentRowForAddress = indexPath.row
                            cell.delegateKbBusiness = Global.sharedInstance.businessService
                            
                            if Global.sharedInstance.isOpenHoursForPlusAction == false && Global.sharedInstance.fromEdit == false && Global.sharedInstance.isOpenWorker == false
                            {
                                cell.setDisplayDataNull()
                                Global.sharedInstance.isSetDataNull = false
                                Global.sharedInstance.item3 = cell
                            }
                            Global.sharedInstance.itemInSection3TableViewCell = cell
                            
                            cell.delegate = self
                            cell.delegateSave = self
                            cell.delegateScroll = self
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.tag = indexPath.section
                            cell.indexCell = indexPath.row
                            
                            if Global.sharedInstance.item3 != nil
                            {
                                return Global.sharedInstance.item3!
                            }
                            else//פעם ראשונה שלא שמור כלום בגלובל
                            {
                                Global.sharedInstance.item3 = cell
                                cell.setDisplayDataNull()
                                return cell
                            }
                        }
                        
                        if Global.sharedInstance.hoursForWorker == true && Global.sharedInstance.hourShowFirstWorker == ""
                        {
                            if indexPath.row == 2{     //שעות פעילות
                                
                                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = true
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                            //סל של הפסקות
                            let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.delegateReloadTbl = self
                            cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                            cell.viewBottom.hidden = false
                            
                            return cell
                        }
                        else if Global.sharedInstance.hourShowFirstWorker != "" && Global.sharedInstance.hoursForWorker == true
                        {
                            if indexPath.row == 2
                            {
                                let cell:SelectedHoursTableViewCell = (tableView.dequeueReusableCellWithIdentifier("SelectedHoursTableViewCell") as? SelectedHoursTableViewCell)!
                                cell.delegateReloadTbl = self
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                if Global.sharedInstance.isReturn2RowsForHours == true
                                {
                                    cell.viewBottom.hidden = false
                                }
                                else
                                {
                                    cell.viewBottom.hidden = true
                                }
                                return cell
                            }
                                
                            else  if indexPath.row == 3{     //שעות פעילות חדש
                                
                                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = true
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                            //סל של הפסקות
                            let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.delegateReloadTbl = self
                            cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                            cell.viewBottom.hidden = false
                            
                            return cell
                        }
                        else if Global.sharedInstance.recessForWorker == true
                        {
                            if indexPath.row == 2//הצגת הסל של הוסף הפסקות
                            {
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = true
                                
                                return cell
                            }
                            else//הצגת השעות
                            {
                                let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delagetReloadHeight = self
                                cell.viewBottom.hidden = false
                                
                                Global.sharedInstance.hoursActive = cell
                                
                                return cell
                            }
                        }
                    }
                    else if Global.sharedInstance.isFromSave == true
                    {
                        if indexPath.row == 1{
                            Global.sharedInstance.isAddWorker = true
                            let cell:ListWorkersTableViewCell = tableView.dequeueReusableCellWithIdentifier("ListWorkersTableViewCell")as!ListWorkersTableViewCell
                            cell.delegate = self
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.delegate = self
                            cell.setDispalyData()
                            return cell
                        }
                        else if indexPath.row == 2
                        {
                            let cell:NewWorkerTableViewCell = tableView.dequeueReusableCellWithIdentifier("NewWorkerTableViewCell")as!NewWorkerTableViewCell
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.delegate = self
                            cell.tag = indexPath.section
                            
                            isOpen = cell.isOpen
                            
                            return cell
                        }
                            
                        else if indexPath.row == 3 && isOpen == true//+
                        {
                            let cell:ItemInSection3TableViewCell = tableView.dequeueReusableCellWithIdentifier("ItemInSection3TableViewCell")as!ItemInSection3TableViewCell
                            cell.delegateGeneric = self
                            Global.sharedInstance.cutrrentRowForAddress = indexPath.row
                            
                            cell.delegateKbBusiness = Global.sharedInstance.businessService
                            
                            Global.sharedInstance.isOpenHoursForPlus = true
                            
                            if Global.sharedInstance.isOpenHoursForPlusAction == false && Global.sharedInstance.fromEdit == false && Global.sharedInstance.isOpenWorker == false
                            {
                                //כדי שלא יציג את הנתונים לפי הקודם
                                cell.setDisplayDataNull()
                            }
                            Global.sharedInstance.itemInSection3TableViewCell = cell
                            Global.sharedInstance.indexForArr = -11
                            cell.delegate = self
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            cell.delegateSave = self
                            cell.delegateScroll = self
                            cell.tag = indexPath.section
                            Global.sharedInstance.fromEdit = false
                            Global.sharedInstance.selectedCell[indexPath.section] = true
                            Global.sharedInstance.currentEditCellOpen = indexPath.section
                            
                            return cell
                        }
                            
                        else if Global.sharedInstance.isOpenHoursForPlusAction == true
                        {
                            
                            //שעות פעילות לעובד בלחיצה על פלוס
                            if Global.sharedInstance.hoursForWorkerFromPlus == true
                            {
                                if indexPath.row == 4//שעות פעילות חדש
                                {
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                    
                                    
                                    let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                                    cell.delagetReloadHeight = self
                                    cell.viewBottom.hidden = true
                                    
                                    Global.sharedInstance.hoursActive = cell
                                    
                                    return cell
                                }
                                //סל של הפסקות
                                let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                cell.delegateReloadTbl = self
                                cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                cell.viewBottom.hidden = false
                                
                                return cell
                            }
                            else{
                                
                                if indexPath.row == 4//הצגת הסל של הוסף הפסקות
                                {
                                    let cell:RecessesTableViewCell = (tableView.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                                    cell.delegateReloadTbl = self
                                    cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                                    cell.viewBottom.hidden = true
                                    
                                    return cell
                                }
                                else//הצגת השעות
                                {
                                    let cell:HoursActiveTableViewCell = (tableView.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                                    cell.delagetReloadHeight = self
                                    cell.viewBottom.hidden = false
                                    
                                    Global.sharedInstance.hoursActive = cell
                                    
                                    return cell
                                }
                            }
                        }
                        
                    }
                    else if Global.sharedInstance.isReloadForEdit == true{
                        if indexPath.row == 1{
                            
                        }
                    }
                    
                case (3):
                    if Global.sharedInstance.isFromFirstSave == false{
                        let cell:BussinesServicesTableViewCell = tableView.dequeueReusableCellWithIdentifier("BussinesServicesTableViewCell")as!BussinesServicesTableViewCell
                        
                        cell.delegateKb = Global.sharedInstance.itemInSection3TableViewCell
                        cell.delegateScroll = self
                        cell.delegateSave = self
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        if Global.sharedInstance.isNewServicePlusOpen == false
                        {
                            cell.setDisplayDataNull()
                        }
                        cell.tag = indexPath.section
                        return cell
                    }
                    
                    if indexPath.row == 1{
                        let cell:ListServicesTableViewCell = tableView.dequeueReusableCellWithIdentifier("ListServicesTableViewCell")as!ListServicesTableViewCell
                        cell.delegate = self
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        cell.setDisplayData()
                        cell.tag = indexPath.section
                        return cell
                    }
                    else if indexPath.row == 2{
                        let cell:AddNewServiceTableViewCell = tableView.dequeueReusableCellWithIdentifier("AddNewServiceTableViewCell")as!AddNewServiceTableViewCell
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        cell.delegate = self
                        // cell.delegate = self
                        cell.tag = indexPath.section
                        return cell
                    }
                    else//מלחיצה על פלוס
                    {
                        let cell:BussinesServicesTableViewCell = tableView.dequeueReusableCellWithIdentifier("BussinesServicesTableViewCell")as!BussinesServicesTableViewCell
                        
                        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                        
                        
                        if Global.sharedInstance.isNewServicePlusOpen == false
                        {
                            cell.setDisplayDataNull()
                        }
                        
                        cell.txtPrice.tag = -11
                        
                        cell.tag = -20
                        cell.delegateKb = Global.sharedInstance.itemInSection3TableViewCell
                        cell.delegateSave = self
                        cell.delegateScroll = self
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        //cell.tag = indexPath.section
                        Global.sharedInstance.currentEditCellOpen = indexPath.section
                        return cell
                    }
                    
                case (4):
                    let cell:CalenderSettingTableViewCell = tableView.dequeueReusableCellWithIdentifier("CalenderSettingTableViewCell")as!CalenderSettingTableViewCell
                    Global.sharedInstance.fromCalendar = true
                    
                    Global.sharedInstance.businessService?.delegateKbCalendar = cell
                    Global.sharedInstance.itemInSection3TableViewCell?.delegateKbCalendar = cell
                    
                    cell.delegateSave = self
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.tag = indexPath.section
                    cell.delegate = self
                    cell.delegateScroll = self
                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                    
                    cell.delegateKb = Global.sharedInstance.itemInSection3TableViewCell
                    cell.delegateKbBusiness = Global.sharedInstance.businessService
                    
                    return cell
                    
                default:
                    let cell:Section2TableViewCell = tableView.dequeueReusableCellWithIdentifier("Section2TableViewCell")as!Section2TableViewCell
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.delegat = self
                    cell.setDisplayData()
                    return cell
                }
            }
        }
            
        else if tableView == WorkersTbl
        {
            if indexPath.row == 0
            {
                let cell:WorkerInListTableViewCell = tableView.dequeueReusableCellWithIdentifier("WorkerInListTableViewCell")as! WorkerInListTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                cell.tag = indexPath.section
                cell.delegate = self
                
                let Fname = Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].objsers.nvFirstName
                let Lname =  Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].objsers.nvLastName
                
                let name = "\(Fname) \(Lname)"
                cell.setDisplayData(name)
                
                if Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].bSameWH == false && Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].objsers.iUserStatusType == 25 {
                    flags[indexPath.section] = true
                    flagsHelp[indexPath.section] = true
                }
                else
                {
                    flags[indexPath.section] = false
                    flagsHelp[indexPath.section] = false
                }
                if Global.sharedInstance.isClickNoEqual == false && Global.sharedInstance.isClickEqual == false && Global.sharedInstance.clickNoCalendar == false
                {
                    if flags[indexPath.section] == true{
                        if Global.sharedInstance.isOpenHoursForNewWorker == false
                        {
                            Global.sharedInstance.isOpenHoursForNewWorker = true
                        }
                    }
                    else
                    {
                        Global.sharedInstance.isOpenHoursForNewWorker = false
                    }
                }
                if Global.sharedInstance.ifOpenCell == false{ Global.sharedInstance.fromEdit = false
                }
                return cell
            }
                
            else if indexPath.row == 1//מעריכה
            {
                let cell:ItemInSection3TableViewCell = extTbl.dequeueReusableCellWithIdentifier("ItemInSection3TableViewCell")as!ItemInSection3TableViewCell
                cell.delegateGeneric = self
                Global.sharedInstance.cutrrentRowForAddress = indexPath.row
                
                Global.sharedInstance.fromEdit = true
                Global.sharedInstance.itemInSection3TableViewCell = cell
                Global.sharedInstance.indexForArr = indexPath.section
                cell.delegateKbBusiness = Global.sharedInstance.businessService
                cell.delegate = self
                cell.delegateZeroMaxMin = Global.sharedInstance.hoursActive
                Global.sharedInstance.isOwner = false
                cell.delegateSave = self
                cell.delegateScroll = self
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.setDisplayData(Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].objsers,bsameW: Global.sharedInstance.generalDetails.arrObjServiceProviders[indexPath.section].bSameWH)
                cell.tag = -30
                let index = NSIndexPath(forRow: 1, inSection: 2)
                if Global.sharedInstance.addRecess == false
                {
                    extTbl.scrollToRowAtIndexPath(index, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                }
                Global.sharedInstance.currentEditCellOpen = 2
                Global.sharedInstance.selectedCell[2] = true
                return cell
            }
            
            //--------------------------------
            if Global.sharedInstance.hoursForWorkerFromEdit == true
            {
                if indexPath.row == 2
                {
                    //שעות פעילות חדש
                    let cell:HoursActiveTableViewCell = (extTbl.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.delagetReloadHeight = self
                    cell.viewBottom.hidden = true
                    
                    Global.sharedInstance.hoursActive = cell
                    
                    return cell
                }
                else
                {
                    //סל של הפסקות
                    let cell:RecessesTableViewCell = (extTbl.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.viewBottom.hidden = true
                    cell.delegateReloadTbl = self
                    cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                    return cell
                }
            }
            else if Global.sharedInstance.recessForWorkerFromEdit == true
            {
                if indexPath.row == 2
                {
                    //סל של הפסקות
                    let cell:RecessesTableViewCell = (extTbl.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.viewBottom.hidden = true
                    cell.delegateReloadTbl = self
                    cell.delgateEnabledDays = Global.sharedInstance.hoursActive
                    return cell
                }
                else
                {
                    //שעות פעילות חדש
                    let cell:HoursActiveTableViewCell = (extTbl.dequeueReusableCellWithIdentifier("HoursActiveTableViewCell") as? HoursActiveTableViewCell)!
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.delagetReloadHeight = self
                    cell.viewBottom.hidden = true
                    
                    Global.sharedInstance.hoursActive = cell
                    
                    return cell
                }
            }
            let cell:RecessesTableViewCell = (extTbl.dequeueReusableCellWithIdentifier("RecessesTableViewCell") as? RecessesTableViewCell)!
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.viewBottom.hidden = true
            cell.delegateReloadTbl = self
            cell.delgateEnabledDays = Global.sharedInstance.hoursActive
            return cell
            
        }
            
        else
        {
            if indexPath.row == 0
            {
                let cell:ServiceInListTableViewCell = tableView.dequeueReusableCellWithIdentifier("ServiceInListTableViewCell")as! ServiceInListTableViewCell
                cell.delegate = self
                cell.tag = indexPath.section
                
                cell.setDispalyData(Global.sharedInstance.generalDetails.arrObjProviderServices[indexPath.section].nvServiceName)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                return cell
            }
            else//מעריכה
            {
                let cell:BussinesServicesTableViewCell = extTbl.dequeueReusableCellWithIdentifier("BussinesServicesTableViewCell")as!BussinesServicesTableViewCell
                
                cell.delegateKb = Global.sharedInstance.itemInSection3TableViewCell
                cell.delegateSave = self
                cell.delegateScroll = self
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.setDisplayData(Global.sharedInstance.generalDetails.arrObjProviderServices[indexPath.section])
                cell.tag = indexPath.section
                cell.txtPrice.tag = indexPath.section
                
                ////////////////////////
                Global.sharedInstance.currentEditCellOpen = 3
                Global.sharedInstance.selectedCell[3] = true
                ////////////////////////
                let index = NSIndexPath(forRow: 1, inSection: 3)
                extTbl.scrollToRowAtIndexPath(index, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                return cell
            }
        }
        
        let cell:ServiceInListTableViewCell = tableView.dequeueReusableCellWithIdentifier("ServiceInListTableViewCell")as! ServiceInListTableViewCell
        
        cell.setDispalyData(Global.sharedInstance.generalDetails.arrObjProviderServices[indexPath.section].nvServiceName)
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.row == 0
        {
            if Global.sharedInstance.selectedCell[indexPath.section] == true{//האם הלחיצה היא בשביל סגירה צריך להפעיל בדיקות תקינות ובמידה שזה תקין לבצע שמירה
                switch indexPath.section {
                case 0:
                    
                    if indexPath.row == 0
                    {
                        if indexPath.section != 2{
                            
                            if Global.sharedInstance.selectedCell[indexPath.section] == false
                            {
                                if indexPath.section == 0
                                {
                                    if AppDelegate.arrDomains.count > 0
                                    {
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = true
                                }
                            }
                            else
                            {
                                Global.sharedInstance.selectedCell[indexPath.section] = false
                                if Global.sharedInstance.currentEditCellOpen == indexPath.section
                                {
                                    Global.sharedInstance.currentEditCellOpen = -1
                                }
                            }
                            
                            tableView.reloadData()
                            
                            if Global.sharedInstance.selectedCell[indexPath.section] == true
                            {
                                Global.sharedInstance.fSection5 = true
                                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                            }
                        }
                        else{
                            
                            if Global.sharedInstance.selectedCell[indexPath.section] == false
                            {
                                if indexPath.section != 2{
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                    Global.sharedInstance.selectedCell[indexPath.section] = true
                                }
                                else{
                                    if Global.sharedInstance.isFromSave == false{
                                        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                        
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                }
                            }
                            else
                            {
                                Global.sharedInstance.selectedCell[indexPath.section] = false
                                Global.sharedInstance.isOpenHours = false
                                AppDelegate.isBtnCheck = true
                                
                            }
                            tableView.reloadData()
                            if Global.sharedInstance.selectedCell[indexPath.section] == true
                            {
                                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                            }
                        }
                    }
                    
                case 1:///שעות פעילות
                    
                    //שמירת הנתונים בגלובל שיהיו מוכנים לשליחה לשרת
                    
                    var workingHours:objWorkingHours = objWorkingHours()
                    Global.sharedInstance.generalDetails.arrObjWorkingHours = Array<objWorkingHours>()
                    
                    //עובר על השעות של כל הימים בשבוע של הספק
                    for i in 0 ..< Global.sharedInstance.arrWorkHours.count {
                        //יש הפסקות
                        if Global.sharedInstance.isHoursSelectedRest[i]
                        {
                            Global.sharedInstance.GlobalDataVC!.delegateActiveHours.checkValidityHours(i)
                            if Global.sharedInstance.fIsValidHours[i] == true && Global.sharedInstance.fIsValidRest[i] == true//רק אם תקין
                            {
                                //שמירת הנתונים
                                workingHours = objWorkingHours(
                                    _iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                                    _nvFromHour: Global.sharedInstance.arrWorkHours[i].nvFromHour,
                                    _nvToHour: Global.sharedInstance.arrWorkHoursRest[i].nvFromHour)
                                
                                Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                                //---------------------------
                                workingHours = objWorkingHours(
                                    _iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                                    _nvFromHour: Global.sharedInstance.arrWorkHoursRest[i].nvToHour,
                                    _nvToHour: Global.sharedInstance.arrWorkHours[i].nvToHour)
                                
                                Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                            }
                        }
                        else //אין הפסקות
                        {
                            Global.sharedInstance.GlobalDataVC?.delegateActiveHours.checkValidityHours(i)
                            if Global.sharedInstance.fIsValidHours[i] == true
                            {
                                workingHours = objWorkingHours(
                                    _iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                                    _nvFromHour: Global.sharedInstance.arrWorkHours[i].nvFromHour,
                                    _nvToHour: Global.sharedInstance.arrWorkHours[i].nvToHour)
                                
                                if Global.sharedInstance.isHoursSelected[i] == true
                                {
                                    Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                                }
                            }
                        }
                    }
                    //עובר על כל השעות ובודק האם יש יום שהשעות לא חוקיות
                    var isBreak = false
                    for bool in Global.sharedInstance.fIsValidHours
                    {
                        if bool == false
                        {
                            Global.sharedInstance.isValidHours = false
                            isBreak = true
                            break
                        }
                    }
                    if Global.sharedInstance.isValidHours == false
                        && isBreak == true
                    {
                        
                    }
                    else
                    {
                        Global.sharedInstance.isValidHours = true
                    }
                    
                    Global.sharedInstance.selectedCell[indexPath.section] = false
                    
                    if Global.sharedInstance.addRecess != false//אם פתחו בכלל את ההפסקות ויש צורך להפעיל(גם כדי למנוע קריסה)
                    {
                        Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
                    }
                    Global.sharedInstance.addRecess = false
                    
                    if indexPath.section == Global.sharedInstance.currentEditCellOpen
                    {
                        Global.sharedInstance.currentEditCellOpen = -1//הכל סגור
                    }
                    for var item in Global.sharedInstance.fIsValidHours
                    {
                        if item == true{
                            Global.sharedInstance.isHoursSelectedItem = true
                        }
                    }
                    extTbl.reloadData()
                    
                case 2:
                    if  delegateSaveWorker.saveDataToWorker() == true && Global.sharedInstance.fisValidWorker == true{
                        if indexPath.row == 0
                        {
                            if indexPath.section != 2{
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section == 0
                                    {
                                        if AppDelegate.arrDomains.count > 0
                                        {
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                        }
                                    }
                                    else
                                    {
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                }
                                
                                tableView.reloadData()
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == true
                                {
                                    Global.sharedInstance.fSection5 = true
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                }
                            }
                            else
                            {
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section != 2{
                                        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                    else
                                    {
                                        Global.sharedInstance.isServiceProviderEditOpen = false
                                        
                                        if Global.sharedInstance.isFromSave == false
                                        {
                                            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                        }
                                        else
                                        {
                                            Global.sharedInstance.currentEditCellOpen = -1
                                            Global.sharedInstance.isOpenWorker = false
                                            if Global.sharedInstance.isFromSave == true
                                            {
                                                
                                                if selectedCellForEdit.contains(true)//אם יש עובד הפתוח בעריכה
                                                {
                                                    Global.sharedInstance.hoursForWorkerFromEdit = false
                                                    for i in 0 ..< selectedCellForEdit.count{
                                                        if selectedCellForEdit[i] == true
                                                        {
                                                            let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                                                            let cellMy = (WorkersTbl.cellForRowAtIndexPath(index) as! WorkerInListTableViewCell)
                                                            
                                                            cellMy.isOpen = false
                                                            cellMy.isEdit = 0
                                                            
                                                            selectedCellForEdit[i] = false
                                                            if Global.sharedInstance.countCellEditOpen > 0{
                                                                Global.sharedInstance.countCellEditOpen -= 1
                                                            }
                                                            Global.sharedInstance.ifOpenCell = false
                                                            Global.sharedInstance.currentEditCellOpen = -1
                                                            Global.sharedInstance.selectedCell[2] = false
                                                            if flags[i] == true{
                                                                if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                                                                    Global.sharedInstance.countCellHoursOpenFromEdit = 0
                                                                }
                                                                
                                                                Global.sharedInstance.isOpenHoursForNewWorker = false
                                                            }
                                                            
                                                            break
                                                            
                                                            selectedCellForEdit[i] = false
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    let index:NSIndexPath = NSIndexPath(forRow: 2, inSection: 2)
                                                    if extTbl.cellForRowAtIndexPath(index) != nil
                                                    {
                                                        (extTbl.cellForRowAtIndexPath(index) as! NewWorkerTableViewCell).isOpen = false
                                                        Global.sharedInstance.isOpenNewWorker = false
                                                        Global.sharedInstance.isOpenHoursForPlusAction = false
                                                        
                                                        Global.sharedInstance.currentEditCellOpen = -1
                                                        Global.sharedInstance.selectedCell[indexPath.section] = false
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                    Global.sharedInstance.isOpenHours = false
                                    AppDelegate.isBtnCheck = true
                                    
                                }
                                tableView.reloadData()
                                if Global.sharedInstance.selectedCell[indexPath.section] == true
                                {
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                }
                            }
                        }
                    }
                    else
                    {
                        Global.sharedInstance.selectedCell[indexPath.section] = false
                        if Global.sharedInstance.currentEditCellOpen == indexPath.section
                        { Global.sharedInstance.isOpenHours = false
                            Global.sharedInstance.isOpenHoursForPlusAction = false
                            Global.sharedInstance.isOpenWorker = false
                            Global.sharedInstance.isOpenNewWorker = false
                            newWorkerIsClose = true
                            Global.sharedInstance.currentEditCellOpen = -1
                            
                            Global.sharedInstance.isValidWorkerDetails = true
                        }
                        if Global.sharedInstance.generalDetails.arrObjServiceProviders.count == 0
                        {
                            Global.sharedInstance.isFromSave = false
                        }
                        extTbl.reloadData()
                    }
                case 3:
                    if delegateSaveBussines.saveDataToWorker() == true{
                        Global.sharedInstance.currentEditCellOpen = -1
                        if indexPath.row == 0
                        {
                            if indexPath.section != 2{
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section == 0
                                    {
                                        if AppDelegate.arrDomains.count > 0
                                        {
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                            
                                        }
                                    }
                                    else{
                                        
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                }
                                else
                                {
                                    if Global.sharedInstance.currentEditCellOpen == indexPath.section//אם סגרתי את עריכת מוצר בשדה שרותים או מוצרים - בעצמו.
                                    {
                                        Global.sharedInstance.currentEditCellOpen = -1
                                        Global.sharedInstance.isNewServicePlusOpen = false
                                    }
                                    if Global.sharedInstance.selectedCellForEditService.contains(true)//אם יש שרות הפתוח בעריכה
                                    {
                                        for i in 0 ..< Global.sharedInstance.selectedCellForEditService.count{//מחפש את הסל שבעריכה וסוגר אותו
                                            if Global.sharedInstance.selectedCellForEditService[i] == true{
                                                if delegateSaveBussines.saveDataToWorker() == true{
                                                    
                                                    let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                                                    let cellMy = ServicesTbl.cellForRowAtIndexPath(index) as! ServiceInListTableViewCell
                                                    cellMy.isOpen = false
                                                    cellMy.isEdit = 0
                                                    Global.sharedInstance.selectedCellForEditService[i] = false
                                                    Global.sharedInstance.isFromEditService = false
                                                    Global.sharedInstance.currentEditCellOpen = -1
                                                    Global.sharedInstance.selectedCell[3] = false
                                                    
                                                    if AppDelegate.countCellEditOpenService > 0{
                                                        AppDelegate.countCellEditOpenService -= 1
                                                    }
                                                    break
                                                }
                                                
                                            }
                                        }
                                    }
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                    Global.sharedInstance.isNewServicePlusOpen = false
                                    
                                    
                                }
                                
                                
                                tableView.reloadData()
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == true
                                {
                                    Global.sharedInstance.fSection5 = true
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                    
                                }
                            }
                            else
                            {
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section != 2{
                                        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                    else{
                                        if Global.sharedInstance.isFromSave == false{
                                            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                        }
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                    Global.sharedInstance.isOpenHours = false
                                    AppDelegate.isBtnCheck = true
                                    
                                }
                                tableView.reloadData()
                                if Global.sharedInstance.selectedCell[indexPath.section] == true
                                {
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        Global.sharedInstance.selectedCell[indexPath.section] = false
                        if Global.sharedInstance.currentEditCellOpen == indexPath.section
                        {
                            Global.sharedInstance.isNewServicePlusOpen = false
                            Global.sharedInstance.currentEditCellOpen = -1
                        }
                        if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0
                        {
                            extTbl.reloadData()
                        }
                    }
                case 4:
                    
                    if delegateSaveCalendar.saveDataToWorker() == true {
                        if indexPath.row == 0
                        {
                            if indexPath.section != 2{
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section == 0
                                    {
                                        if AppDelegate.arrDomains.count > 0
                                        {
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                            
                                        }
                                    }
                                    else{
                                        if indexPath.section != 4{//לא יודעת למה השורה הזו מה שקרה שהוא הגיע כפולס ולכן הוא תמיד נכנס לפה לכן כתבתי שונה מארבע סוג של עקיפה אבל יש פה בעיה
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                        }
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                }
                                
                                tableView.reloadData()
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == true {
                                    Global.sharedInstance.fSection5 = true
                                    //      tableView.reloadData()
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                }
                                
                            }
                            else{
                                
                                if Global.sharedInstance.selectedCell[indexPath.section] == false
                                {
                                    if indexPath.section != 2{
                                        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                        Global.sharedInstance.selectedCell[indexPath.section] = true
                                    }
                                    else{
                                        if Global.sharedInstance.isFromSave == false{
                                            if indexPath.section != 4{
                                                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                            }
                                            Global.sharedInstance.selectedCell[indexPath.section] = true
                                        }
                                    }
                                }
                                else
                                {
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                    Global.sharedInstance.isOpenHours = false
                                    AppDelegate.isBtnCheck = true
                                    
                                }
                                tableView.reloadData()
                                if Global.sharedInstance.selectedCell[indexPath.section] == true
                                {
                                    tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                                }
                            }
                        }
                        if Global.sharedInstance.currentEditCellOpen == indexPath.section
                        {
                            Global.sharedInstance.currentEditCellOpen = -1
                        }
                    }
                    
                default:
                    return
                }
            }
                
            else{//אם זה true-הלחיצה בשביל פתיחה
                
                if Global.sharedInstance.currentEditCellOpen != -1
                {
                    closeCellFromOtherCell()
                }
                if  Global.sharedInstance.currentEditCellOpen != indexPath.section && (Global.sharedInstance.currentEditCellOpen == -1 || Global.sharedInstance.selectedCell[Global.sharedInstance.currentEditCellOpen] == false)
                {
                    if indexPath.row == 0
                    {
                        if Global.sharedInstance.selectedCell[indexPath.section] == false
                        {
                            
                            if indexPath.section == 0//תחום
                            {
                                if AppDelegate.arrDomains.count > 0
                                {
                                    Global.sharedInstance.currentEditCellOpen = indexPath.section
                                    Global.sharedInstance.selectedCell[indexPath.section] = true
                                    
                                }
                            }
                            else if indexPath.section == 1//שעות פעילות
                            {
                                Global.sharedInstance.currentEditCellOpen = indexPath.section
                                Global.sharedInstance.selectedCell[indexPath.section] = true
                                Global.sharedInstance.onOpenTimeOpenHours = true
                            }
                            else
                            {
                                if Global.sharedInstance.isFromSave == true && indexPath.section == 2//  אם כבר נוסף עובד
                                {
                                    Global.sharedInstance.currentEditCellOpen = -1
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                }
                                else if Global.sharedInstance.generalDetails.arrObjProviderServices.count != 0  && indexPath.section == 3//אם כבר נוסף שרות
                                {
                                    Global.sharedInstance.currentEditCellOpen = -1
                                    Global.sharedInstance.selectedCell[indexPath.section] = false
                                }
                                else
                                {
//להוסיף בדיקה
                                    
                                    
                                    
                                    Global.sharedInstance.currentEditCellOpen = indexPath.section
                                    Global.sharedInstance.selectedCell[indexPath.section] = true
                                    
                                }
                            }
                        }
                        else
                        {
                            Global.sharedInstance.selectedCell[indexPath.section] = false
                        }
                        
                        tableView.reloadData()
                        
                        if Global.sharedInstance.selectedCell[indexPath.section] == true {
                            Global.sharedInstance.fSection5 = true
                            //      tableView.reloadData()
                            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                        }
                    }
                }
                else if Global.sharedInstance.currentEditCellOpen == indexPath.section
                {
                    Global.sharedInstance.currentEditCellOpen = -1
                    extTbl.reloadData()
                }
            }
        }
        
        switch (tableView)
        {
        case extTbl: break
            
        default: return
            
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if tableView == extTbl
        {
            switch (indexPath.section)
            {
            case 0:
                if indexPath.row != 0
                {
                    return view.frame.size.height * 0.4111
                }
            case 1:
                if Global.sharedInstance.addRecess == false//לא לחצו על הוסף הפסקות
                {
                    if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""//אין שום שעות שנבחרו להצגה
                    {
                        if indexPath.row == 1//הצגת הגלגלת
                        {
                            return view.frame.size.height * 0.65
                        }
                        else if indexPath.row == 2//סל של הוסף הפסקות
                        {
                            return view.frame.size.height * 0.13
                        }
                    }
                    else//נבחרו שעות פעילות
                    {
                        if indexPath.row == 1//הצגת הסל של הצגת השעות שנבחרו
                        {
                            if Global.sharedInstance.GlobalDataVC!.delegateLineForLabel != nil
                            {
                                Global.sharedInstance.GlobalDataVC!.delegateLineForLabel.lineForLabels()
                            }
                            
                            if Global.sharedInstance.hourShowRecess == ""//יש רק שעות ולא הפסקות
                            {
                                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
                                {
                                    return view.frame.size.height * 0.0975 * CGFloat(Global.sharedInstance.numbersOfLineInLblHours)
                                }
                                else
                                {
                                    return view.frame.size.height * 0.0775 * CGFloat(Global.sharedInstance.numbersOfLineInLblHours)
                                }
                            }
                            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
                            {
                                return view.frame.size.height * 0.075 * (CGFloat(Global.sharedInstance.numbersOfLineInLblHours) + CGFloat(Global.sharedInstance.numbersOfLineInLblRest))
                            }
                            return view.frame.size.height * 0.065 * (CGFloat(Global.sharedInstance.numbersOfLineInLblHours) + CGFloat(Global.sharedInstance.numbersOfLineInLblRest))
                        }
                        if indexPath.row == 2//הצגת הגלגלת
                        {
                            return view.frame.size.height * 0.65
                        }
                        else if indexPath.row == 3//סל של הוסף הפסקות
                        {
                            return view.frame.size.height * 0.13
                        }
                    }
                }
                else//לחצו על הוסף הפסקות
                {
                    if Global.sharedInstance.hourShow == "" && Global.sharedInstance.hourShowRecess == ""
                    {
                        if indexPath.row == 1//סל של הוסף הפסקות
                        {
                            return view.frame.size.height * 0.13
                        }
                        else if indexPath.row == 2//הצגת הגלגלת
                        {
                            return view.frame.size.height * 0.65
                        }
                    }
                    else
                    {
                        if Global.sharedInstance.GlobalDataVC!.delegateLineForLabel != nil
                        {
                            Global.sharedInstance.GlobalDataVC!.delegateLineForLabel.lineForLabels()
                        }
                        
                        if indexPath.row == 1//הצגת השעות שנבחרו
                        {
                            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
                            {
                                return view.frame.size.height * 0.06 * (CGFloat(Global.sharedInstance.numbersOfLineInLblHours) + CGFloat(Global.sharedInstance.numbersOfLineInLblRest))
                            }
                            
                            return view.frame.size.height * 0.06 * (CGFloat(Global.sharedInstance.numbersOfLineInLblHours) + CGFloat(Global.sharedInstance.numbersOfLineInLblRest))
                        }
                        else if indexPath.row == 2//הוסף הפסקות
                        {
                            return view.frame.size.height * 0.13
                        }
                        else if indexPath.row == 3//גלגלת
                        {
                            if Global.sharedInstance.hourShowRecess != ""
                            {
                                return view.frame.size.height * 0.68
                            }
                            return view.frame.size.height * 0.68
                        }
                    }
                }
            case 2:
                if Global.sharedInstance.isFromSave == true && Global.sharedInstance.isOpenNewWorker == false
                {
                    if indexPath.row == 0 {
                        return Global.sharedInstance.heightForCell
                    }
                    else if indexPath.row == 1
                    {
                        if Global.sharedInstance.ifOpenCell == false && Global.sharedInstance.countCellEditOpen == 0
                        {
                            return view.frame.size.height * 0.09 * CGFloat(Global.sharedInstance.generalDetails.arrObjServiceProviders.count)
                        }
                        let acalculate =   (view.frame.size.height * 0.09 * CGFloat(Global.sharedInstance.generalDetails.arrObjServiceProviders.count)) + (view.frame.size.height * 0.69 * CGFloat(Global.sharedInstance.countCellEditOpen)) //+   (Global.sharedInstance.heightForCell * CGFloat(Global.sharedInstance.countCellEditOpen))//בגלל שביטלו את השמור והמשך צריך לא לחשבן את הגובה של הסל הזה
                        
                        let calculate =   (CGFloat( Global.sharedInstance.countCellHoursOpenFromEdit) * view.frame.size.height * 0.8375 - (Global.sharedInstance.heightForCell * CGFloat(Global.sharedInstance.countCellHoursOpenFromEdit)))
                        
                        if (flagsHelp.contains(true) && Global.sharedInstance.isOpenHoursForNewWorker == true) || Global.sharedInstance.isClickNoEqual == true{
                            return acalculate + calculate + view.frame.size.height * 0.07//כדי להקטין את הגודל
                        }
                        return acalculate + calculate
                    }
                    else if indexPath.row == 2{
                        return view.frame.size.height * 0.09
                    }
                    
                    return Global.sharedInstance.heightForCell * CGFloat(Global.sharedInstance.generalDetails.arrObjServiceProviders.count)
                }
                if Global.sharedInstance.isOpenNewWorker == true
                {
                    if indexPath.row == 0 {
                        return Global.sharedInstance.heightForCell
                    }
                    else if indexPath.row == 1{
                        if Global.sharedInstance.generalDetails.arrObjServiceProviders.count > 5{
                            return view.frame.size.height * 0.45
                        }
                        return view.frame.size.height * 0.09 * CGFloat(Global.sharedInstance.generalDetails.arrObjServiceProviders.count)
                        
                    }
                    else  if indexPath.row == 2
                    {
                        return view.frame.size.height * 0.09
                    }
                    else  if indexPath.row == 3
                    {
                        return view.frame.size.height * 0.69
                    }
                    else if Global.sharedInstance.isOpenHoursForPlusAction == false{
                        return Global.sharedInstance.heightForCell
                    }
                        
                        
                    else if indexPath.row == 5 && Global.sharedInstance.hoursForWorkerFromPlus == true{
                        return view.frame.size.height * 0.13
                        
                    }
                    else if Global.sharedInstance.hoursForWorkerFromPlus
                    {
                        return view.frame.size.height * 0.65
                        
                    }
                    else if indexPath.row == 5 && Global.sharedInstance.recessForWorkerFromPlus == true{
                        return view.frame.size.height * 0.65
                        
                    }
                    else if Global.sharedInstance.recessForWorkerFromPlus{
                        return view.frame.size.height * 0.13
                    }
                    
                }
                if indexPath.row == 1 {
                    return view.frame.size.height * 0.69
                }
                else if indexPath.row == 2 && Global.sharedInstance.isOpenHours == false{
                    return Global.sharedInstance.heightForCell
                }
                else if indexPath.row == 2 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.hoursForWorker == true
                {
                    return view.frame.size.height * 0.65
                    
                }
                    
                else if indexPath.row == 3 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.hoursForWorker == true{
                    return view.frame.size.height * 0.13// הפסקות
                }
                else if indexPath.row == 2 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.recessForWorker == true
                {
                    return view.frame.size.height * 0.13
                }
                else if indexPath.row == 3 && Global.sharedInstance.isOpenHours == true && Global.sharedInstance.recessForWorker == true
                {
                    return view.frame.size.height * 0.65
                }
                
            case 3:
                if Global.sharedInstance.isFromFirstSave == false{
                    if indexPath.row != 0{
                        return view.frame.size.height * 0.75
                    }
                }
                else  {
                    if indexPath.row == 0{
                        return Global.sharedInstance.heightForCell
                    }
                    else if indexPath.row == 1{
                        
                        if AppDelegate.countCellEditOpenService == 0{
                            return view.frame.size.height * 0.09 * CGFloat(Global.sharedInstance.generalDetails.arrObjProviderServices.count)
                            
                        }
                        return (view.frame.size.height * 0.09 * CGFloat(Global.sharedInstance.generalDetails.arrObjProviderServices.count)) + (view.frame.size.height * 0.75 * CGFloat(AppDelegate.countCellEditOpenService))
                    }
                    else if indexPath.row == 3
                    {
                        return view.frame.size.height * 0.75
                    }
                    return view.frame.size.height * 0.09
                    
                }
            case 4:
                if indexPath.row != 0{
                    return view.frame.size.height * 0.45
                }
            default:
                
                return Global.sharedInstance.heightForCell
            }
            var height = 60 / Global.sharedInstance.heightModel
            return Global.sharedInstance.heightForCell
            //return 60.0
        }
        if tableView == WorkersTbl{
            if indexPath.row == 0{
                return (view.frame.size.height * 0.45)/5
            }
            else if indexPath.row == 1{
                return view.frame.size.height * 0.69
            }
            else if Global.sharedInstance.isOpenHoursForNewWorker == true && indexPath.row == 2 && Global.sharedInstance.hoursForWorkerFromEdit == true
            {
                return view.frame.size.height * 0.65
            }
            else if indexPath.row == 3 && Global.sharedInstance.hoursForWorkerFromEdit == true{
                return view.frame.size.height * 0.13
            }
            else if Global.sharedInstance.isOpenHoursForNewWorker == true && indexPath.row == 2 && Global.sharedInstance.recessForWorkerFromEdit == true{
                
                return view.frame.size.height * 0.13
            }
            else if indexPath.row == 3 && Global.sharedInstance.recessForWorkerFromEdit == true{
                return view.frame.size.height * 0.65
            }
            
            return Global.sharedInstance.heightForCell
        }
        if tableView == daysTbl
        {
            return daysTbl.layer.frame.height / 7
        }
        if tableView == hoursTbl
        {
            return hoursTbl.layer.frame.height / 7
        }
        
        if tableView == ServicesTbl{
            if indexPath.row == 0{
                return (view.frame.size.height * 0.45)/5
            }
            else if indexPath.row == 1{
                return view.frame.size.height * 0.75
            }
            return 50
        }
        return 50
    }
    
    //MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return AppDelegate.arrDomainFilter.count
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        //Aligning right to left on UICollectionView
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        let cell:ItemInCollectionInSection1CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemInCollectionInSection1CollectionViewCell",forIndexPath: indexPath) as! ItemInCollectionInSection1CollectionViewCell
//        if Global.sharedInstance.rtl
//        {
            cell.transform = scalingTransform
//        }
        cell.btnCheck.isCecked = false
        if Global.sharedInstance.domainBuisness != ""
        {
            if Global.sharedInstance.domainBuisness == AppDelegate.arrDomainFilter[indexPath.row].nvCategoryName
            {
                
                cell.btnCheck.isCecked = true
            }
            else{
                cell.btnCheck.isCecked = false
                
            }
        }
        else{
            cell.btnCheck.isCecked = false
            
        }
        cell.setDisplayData(AppDelegate.arrDomainFilter[indexPath.row].nvCategoryName)
        
        cell.delegate = self
        cell.btnCheck.tag = indexPath.row
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width/2, height: view.frame.size.height * 0.4111/4)
    }
    
    //MARK: - gestureRecognizer for dismissKeyboard
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
        dismissKeyboard()
        return false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: - ReloadCollectionDelegate
    func ReloadCollection(collImages:UICollectionView){
        collImages.reloadData()
    }
    
    //MARK: - addRowForSection3Delegate
    func addRow(tag: Int, row:Int) {
        
        if Global.sharedInstance.isOpenHoursForPlusAction == true{//לחיצה על איקס בשעות פעילות שונות בהוסף עובד
            extTbl.reloadData()
        }
        else if tag == -50{
            Global.sharedInstance.isOpenHours = false//סגירת שעות פעילות של הפעם הראשונה
            
            extTbl.reloadData()
        }
        else if tag == -90{//לחיצה על איקס של יומן אישי בעריכה
            Global.sharedInstance.isOpenHoursForNewWorker = false
            extTbl.reloadData()
            self.WorkersTbl.reloadData()
            if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                Global.sharedInstance.countCellHoursOpenFromEdit = 0
            }
        }
        else if tag == -70{//איקס בשעות פעילות של עריכה
            Global.sharedInstance.isClickNoEqual = true
            extTbl.reloadData()
            Global.sharedInstance.isClickNoEqual = true
            self.WorkersTbl.reloadData()
            
            Global.sharedInstance.countCellHoursOpenFromEdit = 1
            
        }
        else if tag == -80{//ןי בשעות פעילות של עריכה
            if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                Global.sharedInstance.countCellHoursOpenFromEdit = 0
            }
            extTbl.reloadData()
            self.WorkersTbl.reloadData()
            
        }
        else  if tag != -30{//לחיצה על איקס של עובד אישי//או על וי בשעות זהות בהוסף עובד
            Global.sharedInstance.isOpenHours = true
            
            extTbl.reloadData()
            
        }
        else{
            extTbl.reloadData()
        }
    }
    
    //MARK: - ReloadTableDelegate
    func ReloadTable(tblHoursActive:UITableView,tblHoursRest:UITableView)
    {
        daysTbl = tblHoursActive
        hoursTbl = tblHoursRest
        self.daysTbl.reloadData()
        hoursTbl.reloadData()
    }
    
    //MARK: - reloadTableForSaveDelegate
    func reloadTableForSave(tag:Int, btnTag:Int){//פונקציה זו נקראת בד״כ כשלאחר שמירה התצוגה משתנית
        
        if tag == -100 && btnTag == 3
        {
            Global.sharedInstance.selectedCell[btnTag] = false
            extTbl.reloadData()
        }
            
        else if tag == -10 || btnTag == -10{//לחצו על השמירה של פרטי עובד לאחר לחיצה על כפתור פלוס להוספת עובד
            Global.sharedInstance.isFromSave = true
            Global.sharedInstance.isOpenNewWorker = false
            if btnTag == -10{
                Global.sharedInstance.isOpenHoursForPlus = false
                Global.sharedInstance.isOpenHoursForPlusAction = false
            }
            extTbl.reloadData()
        }
        else if tag == -20{//לחצו על השמירה של פרטי שרות לאחר לחיצה על כפתור פלוס להוספת שרות
            Global.sharedInstance.isNewService = false
            Global.sharedInstance.isFromFirstSave = true
            
            extTbl.reloadData()
        }
        else if btnTag == -1 {// שמור והמשך של שעות פעילות שבתוך עריכת עובד
            
            selectedCellForEdit[tag] = false
            let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: tag)
            let cell = WorkersTbl.cellForRowAtIndexPath(index) as! WorkerInListTableViewCell
            let index1:NSIndexPath = NSIndexPath(forRow: 2, inSection: tag)
            let cell1 = WorkersTbl.cellForRowAtIndexPath(index1) as? ItemInSection2TableViewCell
            //flags[tag] = 0
            if let a = cell1{
                cell1?.isOpen = false
                Global.sharedInstance.countCellHoursOpenFromEdit = 0//מס׳ שעות פעילות בתוך עריכה פתוחים לצורך חישוב הגובה של הסל החיצוני
            }
            cell.isOpen = false
            Global.sharedInstance.countCellEditOpen -= 1// מס׳ שורות של עריכה פתוחות לצורך חישוב הגובה של הסל החיצוני
            
            Global.sharedInstance.isOpenHoursForNewWorker = false
            Global.sharedInstance.ifOpenCell = false
            extTbl.reloadData()
            self.WorkersTbl.reloadData()
            return
        }
        else if btnTag == -2 {
            Global.sharedInstance.selectedCellForEditService[tag] = false
            AppDelegate.countCellEditOpenService -= 1
            let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: tag)
            let cell = ServicesTbl.cellForRowAtIndexPath(index) as! ServiceInListTableViewCell
            cell.isOpen = false
            extTbl.reloadData()
            ServicesTbl.reloadData()
            return
        }
        else if tag == 2 && Global.sharedInstance.currentEditCellOpen == 2{//שמירה של עובד בפעם הראשונה
            Global.sharedInstance.selectedCell[tag] = false
            Global.sharedInstance.isFromSave = true
            extTbl.reloadData()
        }
        else  if tag  == 3 && Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0{//שמירה של שרות בפעם הראשונה
            if Global.sharedInstance.isFromFirstSave == false
            {
                Global.sharedInstance.isFromFirstSave = true
            }
            else{
                Global.sharedInstance.isFromFirstSave = false
            }
            extTbl.reloadData()
        }
        else    if tag == 1 || tag == 4 {
            Global.sharedInstance.selectedCell[tag] = false
            if tag == 1{
                for var item in Global.sharedInstance.fIsValidHours{
                    if item == true{
                        Global.sharedInstance.isHoursSelectedItem = true
                    }
                }
                // isHoursSelected
            }
            extTbl.reloadData()
        }
        else if tag == -200
        {
            let index:NSIndexPath = NSIndexPath(forRow: 2, inSection: 2)
            let index1:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            Global.sharedInstance.isOpenNewWorker = false
            extTbl.reloadData()
            
            let cell = extTbl.cellForRowAtIndexPath(index) as! NewWorkerTableViewCell
            cell.isOpen = false
        }
        else if tag == -1{
            Global.sharedInstance.selectedCell[2] = false
            extTbl.reloadData()
        }
        else {
            notificationsRowsInSection[2] = 2
            extTbl.reloadData()
        }
    }
    
    //MARK: - putSubDelegate
    func scrollTotop()
    {
        extTbl.setContentOffset(CGPointZero, animated:true)
    }
    
    //לאחר בחירת התחום יוצג התחום שנבחר בסל שמעליו
    func putSub(tag:Int){
        
        let indexTxtSub:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)//מיקום של השורה הראשונה שלשם תכנס התחום שבחר
        let indexItemInCollection:NSIndexPath = NSIndexPath(forRow: tag, inSection: 0)//מיקום של איבר באוסף
        let indexItemDepenExtTabl:NSIndexPath = NSIndexPath(forRow: 1, inSection: 0)
        let cellCollection = extTbl.cellForRowAtIndexPath(indexItemDepenExtTabl) as! Section2TableViewCell
        
        extTbl.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Fade)
        
        let cell = cellCollection.collSubject.cellForItemAtIndexPath(indexItemInCollection) as! ItemInCollectionInSection1CollectionViewCell
        let str =  cell.lblDescSubject.text
        //Global.sharedInstance.itemInCol = cell
        Global.sharedInstance.domainBuisness = str!
        
        (extTbl.cellForRowAtIndexPath(indexTxtSub) as! MainTableViewCell).txtSub.text = str
        Global.sharedInstance.headersCellRequired[0] = true//בשביל הסתרת השדה כוכבית
        ///שמירת  האיידי של שדה התחום שנבחר ע״מ שיהיה מוכן לשליחה לשרת
        Global.sharedInstance.generalDetails.iFieldId = AppDelegate.arrDomainFilter[tag].iCategoryRowId
        extTbl.reloadData()
        
    }
    
    //MARK: - ReloadTableWorkersDelegate
    func ReloadTableWorkers(workersTbl:UITableView){
        if Global.sharedInstance.generalDetails.arrObjServiceProviders.count > 0{
            let index:NSIndexPath = NSIndexPath(forRow: 2, inSection: 3)
            let cell = extTbl.cellForRowAtIndexPath(index) as? NewWorkerTableViewCell
            if let a = cell{
                cell!.isOpen = false
            }
        }
        // flagsHelp = flags
        flags = []
        
        WorkersTbl = workersTbl
        
        for var item in Global.sharedInstance.generalDetails.arrObjServiceProviders{
            selectedCellForEdit.append(false)
            flags.append(false)
        }
        
        self.WorkersTbl.reloadData()
        
    }
    //פתיחה או סגירה של השעות פעילות
    //MARK: - reloadTableForNewWorkerDelegate
    func reloadTableForNewWorker(cell:NewWorkerTableViewCell){
        
        if cell.isOpen == false
        {
            Global.sharedInstance.isOpenNewWorker = true
        }
        if Global.sharedInstance.currentEditCellOpen != -1
        {
            closeCellFromOtherCell()
        }
        if cell.tag == Global.sharedInstance.currentEditCellOpen
        {
            Global.sharedInstance.currentEditCellOpen = -1
            cell.isOpen = false
            extTbl.reloadData()
        }
            
        else if Global.sharedInstance.currentEditCellOpen == -1 || Global.sharedInstance.selectedCell[Global.sharedInstance.currentEditCellOpen] == false
        {
            if cell.isOpen == false || cell.isOpen == true && newWorkerIsClose == true
            {
                Global.sharedInstance.isServiceProviderEditOpen = false
                Global.sharedInstance.hoursForWorkerFromEdit = false
                isOpen = true
                Global.sharedInstance.isOpenWorker = false
                Global.sharedInstance.fromEdit = false
                Global.sharedInstance.currentEditCellOpen = cell.tag
                cell.isOpen = true
                newWorkerIsClose = false
                var indexCell:NSIndexPath = NSIndexPath(forRow: 3, inSection: 2)
                Global.sharedInstance.hoursForWorkerFromPlus = true
                Global.sharedInstance.isOpenNewWorker = true
                extTbl.reloadData()
                let index:NSIndexPath = NSIndexPath(forRow: 2, inSection: 2)
                extTbl.scrollToRowAtIndexPath(index, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                
                flagsHelp = a
                
                //x1 = 0//נסוי לשיים פה אולי זה יעזור שהתחומיים יפתחו אחרי עריכה הוסף לחיצה על תחומים
                
            }
            else{
                if delegateSaveWorker.saveDataToWorker() == true{
                    Global.sharedInstance.hoursForWorkerFromPlus = false
                    cell.isOpen = false
                    Global.sharedInstance.isOpenNewWorker = false
                    Global.sharedInstance.isOpenHoursForPlus = false
                    extTbl.reloadData()
                }
            }
        }
    }
    
    //MARK: - reloadServicesTableDelegate
    func reloadServicesTable(tbl:UITableView){
        if Global.sharedInstance.isFromSave == true{
            let index:NSIndexPath = NSIndexPath(forRow: 2, inSection: 4)
            let cell = extTbl.cellForRowAtIndexPath(index) as? NewWorkerTableViewCell
            if cell != nil
            {
                cell!.isOpen = false
            }
        }
        ServicesTbl = tbl
        for item in Global.sharedInstance.generalDetails.arrObjProviderServices{
            Global.sharedInstance.selectedCellForEditService.append(false)
        }
        ServicesTbl.reloadData()
    }
    
    //MARK: - reloadTableForNewServiceDelegate
    func reloadTableForNewService(cell:AddNewServiceTableViewCell){
        
        if Global.sharedInstance.currentEditCellOpen != -1
        {
            closeCellFromOtherCell()
        }
        if cell.isOpen == false
        {
            Global.sharedInstance.isNewService = true
        }
        if Global.sharedInstance.currentEditCellOpen == -1 || Global.sharedInstance.selectedCell[Global.sharedInstance.currentEditCellOpen] == false
        {
            
            if cell.isOpen == false || newServiceIsClose == true{
                
                if cell.tag == Global.sharedInstance.currentEditCellOpen//סגירת השרות בלחיצה על פלוס (שממנו גם פתחו)
                {
                    cell.isOpen = false
                    newServiceIsClose = true
                    Global.sharedInstance.isNewService = false
                    Global.sharedInstance.currentEditCellOpen = -1
                    
                }
                else
                {
                    cell.isOpen = true
                    newServiceIsClose = false
                    Global.sharedInstance.isNewService = true
                }
                
            }
            else{
                if delegateSaveBussines.saveDataToWorker() == true{
                    cell.isOpen = false
                    Global.sharedInstance.isNewService = false
                }
            }
            extTbl.reloadData()
        }
    }
    
    //MARK: - editWorkerDelegate
    
    //נקראת בעת פתיחת או סגירת עריכה
    func reloadTableForEdit(tag:Int,my:WorkerInListTableViewCell)
    {
        if Global.sharedInstance.addRecess == true//נבחרו הפסקות
        {
            Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
        }
        
        Global.sharedInstance.addRecess = false
        
        if my.isEdit == 1//מעריכה
        {Global.sharedInstance.isClickNoEqual = false

              if(Global.sharedInstance.currentEditCellOpen != -1 && Global.sharedInstance.selectedCell[2] == false) //|| x1 == 1

                //לסגירת סל אחר בשביל לפתוח את עריכה
            {
                closeCellFromOtherCell()
            }
            if (Global.sharedInstance.currentEditCellOpen != -1 && Global.sharedInstance.selectedCell[Global.sharedInstance.currentEditCellOpen] == false && Global.sharedInstance.currentEditCellOpen != 2)/*לפתיחת עריכה לאחר סגירת סקשין אחר*/
                || (Global.sharedInstance.currentEditCellOpen == 2 && Global.sharedInstance.selectedCell[2] == true /*&& Global.sharedInstance.isOpenNewWorker == false*/)/*לסגירת העריכה בלחיצה על סל אחר או בלחיצה נוספת על ערוך*/
                || (Global.sharedInstance.currentEditCellOpen == -1 && Global.sharedInstance.selectedCell[2] == false)//פתיחת העריכה בלחיצה על ערוך אחר שסגרתי בערוך או בפעם הראשונה שלחצו לפתיחת עריכה
            {
                
                Global.sharedInstance.isOpenNewWorker = false//בינתיים שמתי פה לא ברור אם תמיד צריך אותו
                Global.sharedInstance.isOpenHoursForPlusAction = false
                Global.sharedInstance.isOpenHoursForPlus = false
                flagsHelp = a
                let indexCell:NSIndexPath = NSIndexPath(forRow: 2, inSection: 2)
                if extTbl.cellForRowAtIndexPath(indexCell) != nil
                {
                    // האם ההוסף עובד פתוח
                    if  (extTbl.cellForRowAtIndexPath(indexCell)as!NewWorkerTableViewCell).isOpen == true && newWorkerIsClose == false
                    {
                        
                        if delegateSaveWorker.saveDataToWorker() == true
                        {
                            Global.sharedInstance.hoursForWorkerFromPlus = false
                            (extTbl.cellForRowAtIndexPath(indexCell)as!NewWorkerTableViewCell).isOpen = false
                        }
                        
                    }
                }
                if  selectedCellForEdit[tag] == false//אם לפתוח את עריכה
                {
                    Global.sharedInstance.isServiceProviderEditOpen = true
                    
                    Global.sharedInstance.hoursForWorkerFromEdit = true
                    Global.sharedInstance.hoursForWorkerFromPlus = false
                    if Global.sharedInstance.generalDetails.arrObjServiceProviders[tag].bSameWH == false && Global.sharedInstance.generalDetails.arrObjServiceProviders[tag].objsers.iUserStatusType ==
                        25
                    {
                        Global.sharedInstance.countCellHoursOpenFromEdit = 1
                        
                        flags[tag] = true
                        flagsHelp[tag] = true
                        Global.sharedInstance.isOpenHoursForNewWorker = true
                        Global.sharedInstance.onOpenTimeOpenHours = true
                    }
                    else
                    {
                        Global.sharedInstance.isOpenHoursForNewWorker = false
                    }
                    
                    my.isOpen = true
                    my.isEdit = 1
                    for i in 0 ..< selectedCellForEdit.count{
                        if i != tag &&  selectedCellForEdit[i] == true{
                            
                            if delegateSaveWorker.saveDataToWorker() == true{
                                
                                
                                
                                let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                                let cellMy = (WorkersTbl.cellForRowAtIndexPath(index) as! WorkerInListTableViewCell)
                                
                                cellMy.isOpen = false
                                cellMy.isEdit = 0
                                
                                selectedCellForEdit[i] = false
                                if Global.sharedInstance.countCellEditOpen > 0{
                                    Global.sharedInstance.countCellEditOpen -= 1
                                }
                                Global.sharedInstance.ifOpenCell = false
                                Global.sharedInstance.currentEditCellOpen = -1
                                Global.sharedInstance.selectedCell[2] = false
                                
                                Global.sharedInstance.countCellHoursOpenFromEdit = 0
                                
                                if flags[i] == true{
                                    if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                                        Global.sharedInstance.countCellHoursOpenFromEdit = 0
                                    }
                                    Global.sharedInstance.isOpenHoursForNewWorker = false
                                }
                            }
                            selectedCellForEdit[i] = false
                            break
                        }
                    }
                    Global.sharedInstance.selectedCellEdit = tag
                    //בעת פתיחת העריכה -אתחול המערכים ששומרים באופן זמני את השעות פעילות לפי מה שכבר נבחר כדי שגם אם לא גוללים הנתונים לא יתאפסו וכן כדי למזג בין הנתונים כשגוללים,וכן אתחול המשתנים של בחירת הסמן את כל הימים
                    
                    //אתחול בשביל שעות
                    Global.sharedInstance.arrWorkHoursChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].arrObjWorkingHours
                    Global.sharedInstance.isSelectAllHoursChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].isSelectAllHours
                    Global.sharedInstance.isHoursSelectedChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].isHoursSelected
                    
                    //אתחול בשביל הפסקות
                    Global.sharedInstance.arrWorkHoursRestChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].arrObjWorkingRest
                    Global.sharedInstance.isSelectAllRestChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].isSelectAllRecess
                    Global.sharedInstance.isHoursSelectedRestChild = Global.sharedInstance.arrObjServiceProvidersForEdit[Global.sharedInstance.selectedCellEdit].isHoursSelectedRest
                    
                    if flags[tag] == true
                    {
                        Global.sharedInstance.countCellHoursOpenFromEdit = 1
                        Global.sharedInstance.isOpenHoursForNewWorker = true
                    }
                    selectedCellForEdit[tag] = true
                    
                    Global.sharedInstance.fromEdit = true
                    Global.sharedInstance.countCellEditOpen = 1
                    Global.sharedInstance.ifOpenCell = true
                }
                else//סגירת עריכה
                {
                    
                    if delegateSaveWorker.saveDataToWorker() == true{
                        my.isOpen = false
                        
                        selectedCellForEdit[tag] = false
                        if Global.sharedInstance.countCellEditOpen > 0{
                            Global.sharedInstance.countCellEditOpen -= 1
                        }
                        Global.sharedInstance.ifOpenCell = false
                        
                        Global.sharedInstance.currentEditCellOpen = -1

                        Global.sharedInstance.selectedCell[2] = false

                        
                        Global.sharedInstance.countCellHoursOpenFromEdit = 0
                        flagsHelp = a
                        Global.sharedInstance.flagIsClickOnNoSameHour = true
                        if flags[tag] == true{
                            if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                                Global.sharedInstance.countCellHoursOpenFromEdit = 0
                                
                            }
                            my.isOpenHours = false
                            Global.sharedInstance.isOpenHoursForNewWorker = false
                        }
                    }
                    Global.sharedInstance.hoursForWorkerFromEdit = false
                    Global.sharedInstance.isServiceProviderEditOpen = false
                }
            }
            
            extTbl.reloadData()
            WorkersTbl.reloadData()
            
            extTbl.reloadData()
        }
        else if my.isEdit == 0//ממחיקה
        {
            if selectedCellForEdit[tag] == true{
                if Global.sharedInstance.countCellEditOpen > 0{
                    Global.sharedInstance.countCellEditOpen -= 1
                    if Global.sharedInstance.countCellEditOpen == 0{

                     Global.sharedInstance.selectedCell[2] = false
                       Global.sharedInstance.fromEdit = false
                        Global.sharedInstance.currentEditCellOpen = -1
                    }
                }
                Global.sharedInstance.ifOpenCell = false
                selectedCellForEdit.removeAtIndex(tag)
                my.isOpen = false
                
                if flags[tag] == true{
                    if    Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                        Global.sharedInstance.countCellHoursOpenFromEdit = 0
                    }
                    my.isOpenHours = false
                    Global.sharedInstance.isOpenHoursForNewWorker = false
                }
            }
            extTbl.reloadData()
            
            WorkersTbl.reloadData()
            extTbl.reloadData()
            
        }
        else {
            if selectedCellForEdit[tag] == true{
                Global.sharedInstance.countCellEditOpen -= 1
                Global.sharedInstance.ifOpenCell = false
                selectedCellForEdit.removeAtIndex(tag)
                my.isOpen = false
                if flags[tag] == true{
                    if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                        Global.sharedInstance.countCellHoursOpenFromEdit = 0
                    }
                    my.isOpenHours = false
                    Global.sharedInstance.isOpenHoursForNewWorker = false
                }
                
            }
            else{
                selectedCellForEdit.removeAtIndex(tag)
            }
            extTbl.reloadData()
            
            WorkersTbl.reloadData()
            extTbl.reloadData()
        }
        
    }
    
    //MARK: - editServiceDelegate
    
    func reloadTableForEditService(tag:Int,my:ServiceInListTableViewCell){
        let indexCell:NSIndexPath = NSIndexPath(forRow: 2, inSection: 3)
        var cell:AddNewServiceTableViewCell!
        if extTbl.cellForRowAtIndexPath(indexCell) != nil{
            cell = extTbl.cellForRowAtIndexPath(indexCell) as! AddNewServiceTableViewCell
            
        }
        if my.isEdit == 1{//פתיחה וסגירה של עריכה
            
            if Global.sharedInstance.currentEditCellOpen != -1 && Global.sharedInstance.selectedCell[3] == false//לסגירת סל אחר בשביל לפתוח את עריכה
            {
                closeCellFromOtherCell()
            }
            if (Global.sharedInstance.currentEditCellOpen != -1 && Global.sharedInstance.selectedCell[Global.sharedInstance.currentEditCellOpen] == false && Global.sharedInstance.currentEditCellOpen != 3)/*לפתיחת עריכה לאחר סגירת סל אחר*/
                || (Global.sharedInstance.currentEditCellOpen == 3 && Global.sharedInstance.selectedCell[3] == true)/*לסגירת העריכה בלחיצה על סל אחר או בלחיצה נוספת על ערוך*/
                || (Global.sharedInstance.currentEditCellOpen == -1 && Global.sharedInstance.selectedCell[3] == false)//פתיחת העריכה בלחיצה על ערוך אחר שסגרתי בערוך
                || Global.sharedInstance.isNewService == true || cell.isOpen == true
            {Global.sharedInstance.isNewService = false
                let indexCell:NSIndexPath = NSIndexPath(forRow: 2, inSection: 3)
                if extTbl.cellForRowAtIndexPath(indexCell) != nil
                {
                    if  (extTbl.cellForRowAtIndexPath(indexCell)as!AddNewServiceTableViewCell).isOpen == true
                    {
                        (extTbl.cellForRowAtIndexPath(indexCell)as!AddNewServiceTableViewCell).isOpen = false
                    }
                    
                }
                
                if Global.sharedInstance.selectedCellForEditService[tag] == false//
                {
                    
                    my.isOpen = true
                    my.isEdit = 1
                    for i in 0 ..< Global.sharedInstance.selectedCellForEditService.count{
                        if i != tag &&  Global.sharedInstance.selectedCellForEditService[i] == true{
                            if delegateSaveBussines.saveDataToWorker() == true{
                                
                                let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                                let cellMy = ServicesTbl.cellForRowAtIndexPath(index) as! ServiceInListTableViewCell
                                cellMy.isOpen = false
                                cellMy.isEdit = 0
                                Global.sharedInstance.selectedCellForEditService[i] = false
                                Global.sharedInstance.isFromEditService = false
                                Global.sharedInstance.currentEditCellOpen = -1
                                Global.sharedInstance.selectedCell[3] = false
                                
                                if AppDelegate.countCellEditOpenService > 0{
                                    AppDelegate.countCellEditOpenService -= 1
                                }
                                break
                            }
                            
                        }
                    }
                    Global.sharedInstance.selectedCellForEditService[tag] = true
                    AppDelegate.countCellEditOpenService += 1
                    Global.sharedInstance.isFromEditService = true
                }
                else{
                    if delegateSaveBussines.saveDataToWorker() == true{
                        my.isOpen = false
                        Global.sharedInstance.selectedCellForEditService[tag] = false
                        Global.sharedInstance.isFromEditService = false
                        Global.sharedInstance.currentEditCellOpen = -1
                        Global.sharedInstance.selectedCell[3] = false
                        
                        if AppDelegate.countCellEditOpenService > 0{
                            AppDelegate.countCellEditOpenService -= 1
                        }
                    }
                }
            }
            extTbl.reloadData()
            ServicesTbl.reloadData()
        }
        else if my.isEdit == 0{//מחיקה אם יש רק איבר אחד
            if Global.sharedInstance.selectedCellForEditService[tag] == true{
                if AppDelegate.countCellEditOpenService > 0{
                    AppDelegate.countCellEditOpenService -= 1
                }
                
                Global.sharedInstance.selectedCellForEditService.removeAtIndex(tag)
                my.isOpen = false
            }
            else{
                Global.sharedInstance.selectedCellForEditService.removeAtIndex(tag)
            }
            extTbl.reloadData()
            ServicesTbl.reloadData()
            
            ServicesTbl.reloadData()
            extTbl.reloadData()
        }
        else
        {
            if Global.sharedInstance.selectedCellForEditService[tag] == true{
                if  AppDelegate.countCellEditOpenService > 0{
                    AppDelegate.countCellEditOpenService -= 1
                }
                Global.sharedInstance.selectedCellForEditService.removeAtIndex(tag)
                my.isOpen = false
                
                
            }
            else
            {
                Global.sharedInstance.selectedCellForEditService.removeAtIndex(tag)
            }
            extTbl.reloadData()
            
            ServicesTbl.reloadData()
            extTbl.reloadData()
        }
        
    }
    
    func dismmissKeybourd(){
        view.endEditing(true)
    }
    
    func viewErrorForFirstSection(){
        Global.sharedInstance.isFirstSectionValid = false
        
        Global.sharedInstance.helperTable!.reloadData()
    }
    
    func validSection1(){
        
        Global.sharedInstance.flagIsSecond1Valid = false
        
        Global.sharedInstance.helperTable!.reloadData()
    }
    
    func validData() {
        extTbl.reloadData()
    }
    
    
    func scrollOnEdit(keyBoardSize:CGRect,textField:UITextField)
    {
        var frame = self.extTbl.frame
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.3)
        frame.size.height -= keyBoardSize.height - 110
        self.extTbl.frame = frame
        let rect = self.extTbl.convertRect(textField.bounds, fromView: textField)
        self.extTbl.scrollRectToVisible(rect, animated: false)
        UIView.commitAnimations()
    }
    
    func scrollOnEndEdit(keyBoardSize:CGRect)
    {
        var frame = self.extTbl.frame
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.3)
        
        if keyBoardSize.height != 0
        {
            frame.size.height += keyBoardSize.height - 110
        }
        else{
            frame.size.height += 216 - 110
        }
        self.extTbl.frame = frame
        UIView.commitAnimations()
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
        if Global.sharedInstance.GlobalDataVC!.delegateTableAddress != nil
        {
            Global.sharedInstance.GlobalDataVC!.delegateTableAddress.tableViewAddress(extTbl.contentOffset.y)
        }
        
    }
    
    func closeCellFromOtherCell()//סגירת סל בעת פתיחת סל אחר
    {
        //סגירת הסל שהיה פתוח גם אם אינן תקין
        if Global.sharedInstance.currentEditCellOpen == 0 //|| x1 == 1 //תחומים
        {
            Global.sharedInstance.selectedCell[0] = false
        }
        else if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            //שמירת הנתונים בגלובל שיהיו מוכנים לשליחה לשרת
            var workingHours:objWorkingHours = objWorkingHours()
            Global.sharedInstance.generalDetails.arrObjWorkingHours = Array<objWorkingHours>()
            
            //עובר על השעות של כל הימים בשבוע של הספק
            for i in 0 ..< Global.sharedInstance.arrWorkHours.count {
                //יש הפסקות
                if Global.sharedInstance.isHoursSelectedRest[i]
                {
                    Global.sharedInstance.GlobalDataVC!.delegateActiveHours.checkValidityHours(i)
                    if Global.sharedInstance.fIsValidHours[i] == true && Global.sharedInstance.fIsValidRest[i] == true//רק אם תקין
                    {
                        //שמירת הנתונים
                        workingHours = objWorkingHours(
                            _iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                            _nvFromHour: Global.sharedInstance.arrWorkHours[i].nvFromHour,
                            _nvToHour: Global.sharedInstance.arrWorkHoursRest[i].nvFromHour)
                        
                        Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                        //---------------------------
                        workingHours = objWorkingHours(
                            _iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                            _nvFromHour: Global.sharedInstance.arrWorkHoursRest[i].nvToHour,
                            _nvToHour: Global.sharedInstance.arrWorkHours[i].nvToHour)
                        
                        Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                        
                        //   Global.sharedInstance.arrIsRestChecked[i] = false //איפוס המשתנה
                    }
                }
                else //אין הפסקות
                {
                    Global.sharedInstance.GlobalDataVC?.delegateActiveHours.checkValidityHours(i)
                    if Global.sharedInstance.fIsValidHours[i] == true
                    {
                        workingHours = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHours[i].iDayInWeekType,
                                                       _nvFromHour: Global.sharedInstance.arrWorkHours[i].nvFromHour,
                                                       _nvToHour: Global.sharedInstance.arrWorkHours[i].nvToHour)
                        
                        if Global.sharedInstance.isHoursSelected[i] == true
                        {
                            Global.sharedInstance.generalDetails.arrObjWorkingHours.append(workingHours)
                        }
                    }
                }
            }
            //עובר על כל השעות ובודק האם יש יום שהשעות לא חוקיות
            var isBreak = false
            for bool in Global.sharedInstance.fIsValidHours
            {
                if bool == false
                {
                    Global.sharedInstance.isValidHours = false
                    isBreak = true
                    break
                }
            }
            if Global.sharedInstance.isValidHours == false
                && isBreak == true
            {
                
            }
            else
            {
                Global.sharedInstance.isValidHours = true
            }
            
            for item in Global.sharedInstance.fIsValidHours{
                if item == true{
                    Global.sharedInstance.isHoursSelectedItem = true
                }
            }

            Global.sharedInstance.selectedCell[1] = false

            if Global.sharedInstance.addRecess != false//אם פתחו בכלל את ההפסקות ויש צורך להפעיל(גם כדי למנוע קריסה)
            {
                Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
            }
            Global.sharedInstance.addRecess = false
        }
        else if Global.sharedInstance.currentEditCellOpen == 2//עובדים
        {
            if selectedCellForEdit.contains(true)//אם יש עובד הפתוח בעריכה
            {
                Global.sharedInstance.hoursForWorkerFromEdit = false
                for i in 0 ..< selectedCellForEdit.count{
                    if selectedCellForEdit[i] == true{
                        
                        if delegateSaveWorker.saveDataToWorker() == true
                        {
                            flagsHelp = a
                            let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                            let cellMy = (WorkersTbl.cellForRowAtIndexPath(index) as! WorkerInListTableViewCell)
                            
                            cellMy.isOpen = false
                            cellMy.isEdit = 0
                            
                            selectedCellForEdit[i] = false
                            if Global.sharedInstance.countCellEditOpen > 0{
                                Global.sharedInstance.countCellEditOpen -= 1
                            }
                            Global.sharedInstance.ifOpenCell = false
                            Global.sharedInstance.currentEditCellOpen = -1

                            Global.sharedInstance.selectedCell[2] = false

                            if flags[i] == true{
                                if Global.sharedInstance.countCellHoursOpenFromEdit > 0{
                                    Global.sharedInstance.countCellHoursOpenFromEdit = 0
                                }
                                Global.sharedInstance.isOpenHoursForNewWorker = false
                            }
                            
                        }
                        
                        selectedCellForEdit[i] = false
                        break
                    }
                }
                Global.sharedInstance.isServiceProviderEditOpen = false
            }
            else
            {
                if isOpen == true//הוספת עובד
                {
                    Global.sharedInstance.isOpenNewWorker = false
                }
                if delegateSaveWorker.saveDataToWorker() == true
                {
                    //אם הגיע מפלוס
                    if isOpen == true//הוספת עובד +
                    {
                        Global.sharedInstance.isOpenHoursForPlusAction = false
                        isOpen = false
                        Global.sharedInstance.hoursForWorkerFromPlus = false
                        newWorkerIsClose = true
                        Global.sharedInstance.isOpenHoursForPlus = false
                        Global.sharedInstance.isFromSave = true
                        flagsHelp = a
                    }
                    else//עובדים בפעם הראשונה
                    {
                        Global.sharedInstance.isOpenHours = false
                        AppDelegate.isBtnCheck = true
                        if Global.sharedInstance.fisValidWorker == true
                        {
                            Global.sharedInstance.isFromSave = true
                        }
                        else
                        {
                            Global.sharedInstance.isValidWorkerDetails = true
                            Global.sharedInstance.isFromSave = false
                        }
                    }
                    Global.sharedInstance.selectedCell[2] = false
                    Global.sharedInstance.isOpenWorker = false
                    Global.sharedInstance.isOpenNewWorker = false
                }
            }
        }
        else if Global.sharedInstance.currentEditCellOpen == 3//שרותים ומוצרים
        {
            if Global.sharedInstance.selectedCellForEditService.contains(true)//אם יש שרות הפתוח בעריכה
            {
                for i in 0 ..< Global.sharedInstance.selectedCellForEditService.count{//מחפש את הסל שבעריכה וסוגר אותו
                    if Global.sharedInstance.selectedCellForEditService[i] == true{
                        if delegateSaveBussines.saveDataToWorker() == true{
                            
                            let index:NSIndexPath = NSIndexPath(forRow: 0, inSection: i)
                            let cellMy = ServicesTbl.cellForRowAtIndexPath(index) as! ServiceInListTableViewCell
                            cellMy.isOpen = false
                            cellMy.isEdit = 0
                            Global.sharedInstance.selectedCellForEditService[i] = false
                            Global.sharedInstance.isFromEditService = false
                            Global.sharedInstance.currentEditCellOpen = -1
                            Global.sharedInstance.selectedCell[3] = false
                            
                            if AppDelegate.countCellEditOpenService > 0{
                                AppDelegate.countCellEditOpenService -= 1
                            }
                            break
                        }
                        
                    }
                }
            }
                
            else if delegateSaveBussines.saveDataToWorker() == true//שמירת השרות חדש מ+ ומהפעם הראשונה
            {
                //אם תקין
                Global.sharedInstance.isNewServicePlusOpen = false
                Global.sharedInstance.selectedCell[3] = false
                newServiceIsClose = true
                Global.sharedInstance.isFromFirstSave = true
            }
            else//לא תקין
            {
                if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0
                {
                    Global.sharedInstance.isFromFirstSave = false
                }
                else
                {
                    Global.sharedInstance.isFromFirstSave = true
                }
                
                Global.sharedInstance.selectedCell[3] = false
                newServiceIsClose = true
                Global.sharedInstance.isNewService = false
                Global.sharedInstance.isNewServicePlusOpen = false
                
            }
        }
        else if Global.sharedInstance.currentEditCellOpen == 4//הגדרות יומנים
        {
            if delegateSaveCalendar.saveDataToWorker() == true
            {
                Global.sharedInstance.selectedCell[4] = false
            }
        }
    }
    
    //reload external table
    func reloadTbl()
    {
        self.extTbl.reloadData()
    }
    
    func reloadHeight()
    {
        self.extTbl.endUpdates()
    }
    //איפוס הנתונים השייכים לשעות פעילות
    func setPropertiesOfHoursActiveToDefult()
    {
        Global.sharedInstance.arrWorkHours = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
        Global.sharedInstance.isHoursSelected = [false,false,false,false,false,false,false]
        Global.sharedInstance.isHoursSelectedRest = [false,false,false,false,false,false,false]
        Global.sharedInstance.isHoursSelectedChild = [false,false,false,false,false,false,false]
        Global.sharedInstance.isHoursSelectedRestChild = [false,false,false,false,false,false,false]
        Global.sharedInstance.workingHoursRest = objWorkingHours()
        Global.sharedInstance.arrWorkHoursRest = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
        Global.sharedInstance.currentBtnDayTag = -1
        Global.sharedInstance.lastBtnDayTag = -1
        Global.sharedInstance.currentBtnDayTagRest = -1
        Global.sharedInstance.lastBtnDayTagRest = -1
        Global.sharedInstance.numbersOfLineInLblHours = 1
        Global.sharedInstance.numbersOfLineInLblRest = 0
        Global.sharedInstance.isBreak = false
        Global.sharedInstance.addRecess = false
        Global.sharedInstance.onOpenTimeOpenHours = false
        Global.sharedInstance.onOpenRecessHours = false
        Global.sharedInstance.isFirstHoursOpen = false
        Global.sharedInstance.isFirstRecessHoursOpen = false
        Global.sharedInstance.isSelectAllHours = false
        Global.sharedInstance.isSelectAllRest = false
        Global.sharedInstance.isSelectAllHoursChild = false
        Global.sharedInstance.isSelectAllRestChild = false
        Global.sharedInstance.isRest = false
        Global.sharedInstance.workingHours = objWorkingHours()
        Global.sharedInstance.hourShow = ""
        Global.sharedInstance.hourShowRecess = ""
        Global.sharedInstance.hourShowChild = ""
        Global.sharedInstance.hourShowRecessChild = ""
        Global.sharedInstance.isHourScrolled = false
        Global.sharedInstance.hourShowFirstWorker = ""
        Global.sharedInstance.hourShowRecessFirstWorker = ""
        Global.sharedInstance.lastLblHoursHeight = 0.0
        Global.sharedInstance.currentLblHoursHeight = 0.0
        Global.sharedInstance.lastLblRestHeight = 0.0
        Global.sharedInstance.currentLblRestHeight = 0.0
        Global.sharedInstance.workingHoursChild = objWorkingHours()
        Global.sharedInstance.arrWorkHoursChild = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
        Global.sharedInstance.workingHoursRestChild = objWorkingHours()
        Global.sharedInstance.arrWorkHoursRestChild = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
        
        Global.sharedInstance.currentBtnDayTagChild = -1
        Global.sharedInstance.lastBtnDayTagChild = -1
        Global.sharedInstance.currentBtnDayTagRestChild = -1
        Global.sharedInstance.lastBtnDayTagRestChild = -1
    }
    
    func showGeneric()
    {
        self.generic.showNativeActivityIndicator(self)
    }
    
    func hideGeneric()
    {
        self.generic.hideNativeActivityIndicator(self)
    }
}

