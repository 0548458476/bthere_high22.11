//
//  NewEventViewController.swift
//  Bthere
//
//  Created by User on 12.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit

protocol editTextInCellDelegate {
    func editTextInCell(section:Int,text:AnyObject)
}
//קבע אירוע - דרך תפריט פלוס
class NewEventViewController: UIViewController,editTextInCellDelegate,UIGestureRecognizerDelegate {

    let dateFormatt:NSDateFormatter = NSDateFormatter()
    
    var dicEvent:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
    
    @IBOutlet weak var viewPage: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //קבע אירוע
    @IBAction func btnAddEvent(sender: AnyObject) {
        if dicEvent["date"]?.description != nil && dicEvent["hours"]?.description != "" && dicEvent["name"]?.description != ""
        {
            saveEventInDeviceCalander()
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("FILL_ALL_DATA", comment: ""), vc: self)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnAddEvent: UIButton!
    
    var selectedSection = -1
    var textArr:Array<String> = [NSLocalizedString("EVENT_NAME", comment: ""),NSLocalizedString("DATE", comment: ""),NSLocalizedString("HOUR", comment: ""),NSLocalizedString("NOTES", comment: "")]
    
    //MARK: ---------initial ---------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = NSLocalizedString("NEW_EVENT", comment: "")
        btnAddEvent.setTitle(NSLocalizedString("SET_EVENT", comment: ""), forState: .Normal)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        btnAddEvent.titleLabel?.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)

    }
    override func viewDidAppear(animated: Bool) {
        tableView.alwaysBounceVertical = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    //MARK: -------table view -------
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSection != -1 && section == selectedSection//אם לחצו על אחד הסלים לפתיחה
        {
            return 2
        }
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //הסל הדיפולטיבי - טקסט
        let  cell = tableView.dequeueReusableCellWithIdentifier("NewEventTableViewCell")as! NewEventTableViewCell
        cell.delegate = self
        
        if indexPath.row == 0
        {
            cell.setDisplatData(textArr[indexPath.section])
            
            if indexPath.section != 0
            {
                cell.txtFText.enabled = false
                switch indexPath.section {
                case 1:
                    dateFormatt.dateFormat = "dd/MM/YYYY"
                    if dicEvent["date"]?.description != nil
                    {
                    cell.txtFText.text = dateFormatt.stringFromDate(dicEvent["date"] as! NSDate)
                    }
                case 2:
                    cell.txtFText.text = dicEvent["hours"]?.description
                case 3:
                    cell.txtFText.text = dicEvent["remark"]?.description
                default:
                    cell.txtFText.text = dicEvent["name"]?.description
                }
            }
            else
            {
                cell.txtFText.text = dicEvent["name"]?.description
            }
            
            return cell
        }
            //הסל השני - תאריך
        else if indexPath.section == 1 && indexPath.row == 1
        {
            let  cellDate = tableView.dequeueReusableCellWithIdentifier("NewEventDateTableViewCell")as! NewEventDateTableViewCell
            cellDate.delegate = self
            return cellDate
        }
            //הסל השלישי - שעות האירוע
        else if indexPath.section == 2 && indexPath.row == 1
        {
            let  cellHour = tableView.dequeueReusableCellWithIdentifier("NewEventHoursTableViewCell")as! NewEventHoursTableViewCell
            cellHour.delegate = self
            return cellHour
        }
            //הסל הרביעי - הערות
        else if indexPath.section == 3 && indexPath.row == 1
        {
            let  cellRemark = tableView.dequeueReusableCellWithIdentifier("NewEventRemarkTableViewCell")as! NewEventRemarkTableViewCell
            cellRemark.delegate = self
            return cellRemark
        }
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {

            //לחיצה על אותו סל שפתוח כעת, סוגר אותו
            if selectedSection == indexPath.section && indexPath.row == 0// || indexPath.section == 0
            {
                selectedSection = -1
            }
            else
            {
                selectedSection = indexPath.section
            }
            
            tableView.reloadData()
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return self.view.frame.size.height / 15
        }
        else if indexPath.section == 3
        {
            return self.view.frame.size.height / 5
        }
        else if indexPath.section == 2
        {
            return self.view.frame.size.height / 4
        }
        else if indexPath.section == 1
        {
            return self.view.frame.size.height / 3
        }
        return 50
        
    }
    
//שמירת הנתונים מהסלים בתוך הטקסט פילד ע״י דליגייט
    func editTextInCell(section:Int,text:AnyObject)
    {
        let indexPathToEdit:NSIndexPath = NSIndexPath(forRow: 0, inSection: section)
        (tableView.cellForRowAtIndexPath(indexPathToEdit) as! NewEventTableViewCell).txtFText.text = text.description
        dateFormatt.dateFormat = "dd/MM/YYYY"
        switch section {
        case 0:
            dicEvent["name"] = text.description
        case 1:
            if text.description != ""
            {
            dicEvent["date"] = text as! NSDate
            (tableView.cellForRowAtIndexPath(indexPathToEdit) as! NewEventTableViewCell).txtFText.text = dateFormatt.stringFromDate(text as! NSDate)
            }
            else
            {
                dicEvent["date"] = text.description
            }
        case 2:
            dicEvent["hours"] = text.description
        case 3:
            dicEvent["remark"] = text.description
        default:
            dicEvent["name"] = text.description
        }
    }
    
    func dismissKeyBoard()
    {
        view.endEditing(true)
    }
    
    //שמירת האירוע החדש במכשיר האישי
    func saveEventInDeviceCalander()
    {
        let calendar = NSCalendar.currentCalendar()
        
        //cut "12:40-15:50"
        let hours : String = (self.dicEvent["hours"]?.description)!
        let hoursArr : [String] = hours.componentsSeparatedByString("-")
        
        let startHour : String = hoursArr[0]//12:40
        let endHour : String = hoursArr[1]//15:50


        dateFormatt.dateFormat = "HH:mm"
        
        var startDate:NSDate = dateFormatt.dateFromString(startHour)!
        var endDate:NSDate = dateFormatt.dateFromString(endHour)!
        
        let componentsStart = calendar.components([.Hour, .Minute], fromDate: startDate)
        
        let componentsEnd = calendar.components([.Hour, .Minute], fromDate: endDate)
        
        dateFormatt.dateFormat = "dd/MM/YYYY"
    
        let eventDate:NSDate = dicEvent["date"] as! NSDate
        
         //יצירת תאריכי התחלה וסיום לאירוע - ע״פ התאריך והשעות.
        startDate = Calendar.sharedInstance.getPartsOfDate( eventDate, to: componentsStart)
        endDate = Calendar.sharedInstance.getPartsOfDate(eventDate, to: componentsEnd)
        
        //בדיקה את התאריך סיום גדול מתאריך התחלה
        if small(endDate, rhs: startDate) == true
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_HOURS", comment: ""), vc: self)
        }
        else
        {
         //יצירת האירוע
            let eventStore : EKEventStore = EKEventStore()
            
            eventStore.requestAccessToEntityType(.Event, completion: { (granted, error) in
                if (granted) && (error == nil) {
                    let event = EKEvent(eventStore: eventStore)
                    event.title = (self.dicEvent["name"]?.description)!
                    event.notes = "\(self.dicEvent["remark"]?.description) :by_BThere"
        //הוספתי את :by_BThere להערות לזיהוי שזהו אירוע שנקבע באפליקצית ביזר
                    event.allDay = false
                    event.startDate = startDate
                    event.endDate = endDate
                    
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    do {
                        try eventStore.saveEvent(event, span: .ThisEvent)
                        Alert.sharedInstance.showAlert(NSLocalizedString("SUCCESS_NEW_EVENT", comment: ""), vc: self)
                       
                    } catch let e as NSError {
                        print(e)
                        return
                    }
                } else {
                }
            })
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool
    {
        if touch.view!.isDescendantOfView(tableView)
        {
            return false
        }
        return true
    }
    
    //compare between date
    func small(lhs: NSDate, rhs: NSDate) -> Bool {
        
        return lhs.compare(rhs) == .OrderedAscending
    }

}
