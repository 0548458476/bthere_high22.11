//
//  MyWaitingListViewController.swift
//  bthree-ios
//
//  Created by User on 29.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol deleteFromWaitingListDelegate {
    func deleteFromWaitingList()
}

//לקוח - רשימת המתנה שלי
class MyWaitingListViewController: UIViewController,deleteFromWaitingListDelegate {

    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblWaitingList: UITableView!
    
    var generic:Generic = Generic()
    var isFromDelegate = false
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFromDelegate = false
        addTopBorder(tblWaitingList, color: UIColor.blackColor())
        tblWaitingList.separatorStyle = .None
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.getWaitingListForCustomer(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int != 1
                {
                    print(responseObject["Error"])
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_TURNS_INMYLISTWAITING", comment: ""))
                    }
                    else
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
                    }
                }
                else
                {
                    let waitingList:WaitingListObj = WaitingListObj()
                    
                    Global.sharedInstance.arrWaitingList = waitingList.dicToArrayWaitingList(responseObject["Result"] as! Array<Dictionary<String, AnyObject>>)
                    self.tblWaitingList.reloadData()
                    
                }
                
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
        
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return Global.sharedInstance.arrWaitingList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MyWaitingList")as! MyWaitingListTableViewCell
        
        let separatorView: UIView = UIView(frame: CGRectMake(0, cell.frame.height + cell.frame.height * 0.0325, cell.frame.width + (cell.frame.width / 3), 1))
        separatorView.layer.borderColor = UIColor.blackColor().CGColor
        separatorView.layer.borderWidth = 1
        cell.contentView.addSubview(separatorView)
        cell.delegate = self
        if isFromDelegate == true
        {
        cell.colWaitingList.reloadData()
        }
        
        cell.setDisplayData(indexPath.section)
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.1125
    }
    
    func addTopBorder(any:UIView,color:UIColor)
    {
        let borderTop = CALayer()
        
        borderTop.frame = CGRectMake(0, 0, any.layer.frame.width + (any.layer.frame.width / 3), 1)
        
        borderTop.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderTop)
    }
    
    func deleteFromWaitingList() {
        isFromDelegate = true
        tblWaitingList.reloadData()
    }

}
