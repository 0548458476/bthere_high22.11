//
//  RegulationsViewController.swift
//  bthree-ios
//
//  Created by User on 8.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// דף תקנון ברישום
class RegulationsViewController: UIViewController {


    var delegate:didReadRegulationDelegte!=nil

    @IBOutlet weak var lblRegulationTitle: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    
    @IBAction func btnOk(sender: AnyObject) {
        Global.sharedInstance.fReadRegulation = true
        delegate.didReadRegulation()
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func btnCancel(sender: AnyObject) {
        
        Global.sharedInstance.fReadRegulation = false
        delegate.didReadRegulation()
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblRegulationTitle.text =  NSLocalizedString("REG_TITLE", comment: "")
        btnCancel.setTitle(NSLocalizedString("CANCEL", comment: ""), forState: .Normal)
        btnOk.setTitle(NSLocalizedString("OK", comment: ""), forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
