//
//  ExistingUserPhoneViewController.swift
//  bthree-ios
//
//  Created by User on 12.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//להזין טלפון אם משתמש קיים 
class ExistingUserPhoneViewController: UIViewController,UITextFieldDelegate {
    
    var didClickOnConnect:Bool = false//פלג זה מיועד למנוע מהמשתמש ללחוץ כמה פעמים על הכפתור התחבר.
    var delegate:openControlersDelegate!=nil
    var notValid = false
        var generic:Generic = Generic()
    
    @IBOutlet weak var existUser: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtFPhone: UITextField!
    @IBOutlet weak var btnConnect: UIButton!
    @IBAction func btnConnect(sender: AnyObject) {
        
        if didClickOnConnect == false
        {
            didClickOnConnect = true
        if txtFPhone.text == ""
        {
            didClickOnConnect = false
            self.txtFPhone.textColor = UIColor.redColor()
            self.txtFPhone.text = NSLocalizedString("REQUIRED_FIELD", comment: "")
            notValid = true
        }
        else
        {
            
            notValid = false
            var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
            dicPhone["nvPhone"] = txtFPhone.text
            
            if Reachability.isConnectedToNetwork() == false
            {
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                didClickOnConnect = false
            }
            else
            {
                api.sharedInstance.CheckPhoneValidity(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Result"] as! Int == 1 //לא קיים
                    {
                        self.txtFPhone.textColor = UIColor.redColor()
                        self.txtFPhone.text = NSLocalizedString("PHONE_ERROR2", comment: "")
                        self.notValid = true
                        self.didClickOnConnect = false
                    }
                        
                    else
                    {
                        self.didClickOnConnect = true
                        self.notValid = false
                        var dicRestoreVerCode:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        dicRestoreVerCode["nvPhone"] = self.txtFPhone.text
                        
                        if Reachability.isConnectedToNetwork() == false
                        {
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                        }
                        else
                        {
                            self.generic.showNativeActivityIndicator(self)
                            if Reachability.isConnectedToNetwork() == false
                            {
                                self.generic.hideNativeActivityIndicator(self)
                                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                            }
                            else
                            {
                                api.sharedInstance.RestoreVerCode(dicRestoreVerCode, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                                    
                                    Global.sharedInstance.isFromRegister = false
                                    
                                    self.dismissViewControllerAnimated(false, completion: nil)
                                    
                                    self.delegate.openVerification(self.txtFPhone.text!)
                                    self.generic.hideNativeActivityIndicator(self)
                                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                                })
                            }
                        }
                    }
                    
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.didClickOnConnect = false
                })
            }
        }
    }
    }
    
    //MARK: - Initial
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.view.bringSubviewToFront(cancelBtn)
        
        txtFPhone.addTarget(self, action: #selector(ExistingUserPhoneViewController.textFieldDidChange), forControlEvents: .EditingChanged)
        
        txtFPhone.delegate = self
//        txtFPhone.borderStyle = .None
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExistingUserPhoneViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
//        txtFPhone.layer.cornerRadius = 6
        
        lblPhone.text = NSLocalizedString("PHONE_USER", comment: "")
        existUser.text = NSLocalizedString("EXISTS_USER", comment: "")
        btnConnect.setTitle(NSLocalizedString("BTN_CONNECT", comment: ""), forState: .Normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - KeyBoard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBAction func cancelbtn(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: - TextField
    
    func textFieldDidBeginEditing(textField: UITextField) {
        txtFPhone.textColor = UIColor.whiteColor()
        txtFPhone.text = ""
    }
    
    func textFieldDidChange()
    {
        if notValid == true
        {
            let lastCharacter = txtFPhone.text!.substringFromIndex(txtFPhone.text!.endIndex.predecessor())
            
            notValid = false
            txtFPhone.text = lastCharacter
            txtFPhone.textColor = UIColor.whiteColor()
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        if startString.characters.count > 10
        {
            txtFPhone.resignFirstResponder()
            Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_TEN_DIGITS", comment: ""), vc: self)
            
            return false
        }
        else
        {
            return true
        }
    }
}
