//
//  BlockHoursViewController.swift
//  Bthere
//
//  Created by User on 5.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//  ספק קיים חסימת שעות ביומן בהם א״א להזמין תור
class BlockHoursViewController: NavigationModelViewController {

    @IBOutlet weak var vDateToBlock: UIView!
    
    @IBOutlet weak var lblDateToBlock: UILabel!
    @IBOutlet weak var dPDate: UIDatePicker!
    @IBAction func btnClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        dPDate.hidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BlockHoursViewController.openDates))
        vDateToBlock.addGestureRecognizer(tap)
        
        dPDate.datePickerMode = UIDatePickerMode.Date
        dPDate.backgroundColor = Colors.sharedInstance.color1
        dPDate.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dPDate.setValue(false, forKey: "highlightsToday")
        
        dPDate.addTarget(self, action: #selector(RegisterViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        dPDate.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lblDateToBlock.text = dateFormatter.stringFromDate(sender.date)

    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
//        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
//        
//        txtfDetails.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
func openDates()
{
    if dPDate.hidden == true
    {
        dPDate.hidden = false
    }
    else
    {
        dPDate.hidden = true
    }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
