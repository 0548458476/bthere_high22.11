//
//  NearbyVacantAppointmentViewController.swift
//  Bthere
//
//  Created by User on 1.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים-תורים פנויים קרובים
class NearbyVacantAppointmentViewController: UIViewController{
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBAction func btnClose(sender: AnyObject) {
    }
    
    @IBOutlet weak var tblAppointments: UITableView!
    
    //MARK: - Properties
    
    let calendar = NSCalendar.currentCalendar()
    var datesArray:Array <NSDate> = [NSDate(timeIntervalSinceReferenceDate: -12345.0),NSDate(timeIntervalSinceReferenceDate: -12345.0),NSDate(timeIntervalSinceReferenceDate: +123000089.0)]
    var textArray:Array <String> =
        ["תספורת אישה בחצי מחיר",
         "1 + 1 על מסיכות השיער",
         "גוונים ב₪200 עד השעה 2:00"]
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTopBorder(tblAppointments, color: UIColor.blackColor())
        tblAppointments.separatorStyle = .None
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        
        if DeviceType.IS_IPHONE_6{
            lblTitle.font = UIFont(name: "OpenSansHebrew-Bold", size: 28)!
        }
        else
        { 
            if DeviceType.IS_IPHONE_6P
            {
                lblTitle.font = UIFont(name: "OpenSansHebrew-Bold", size: 30)!
            }
            else
            {
                if DeviceType.IS_IPHONE_5
                {
                    lblTitle.font =  UIFont(name: "OpenSansHebrew-Bold", size: 24)!
                }
                else
                {
                    lblTitle.font = Colors.sharedInstance.fontSecondHeader
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("AppointmentNearByTableViewCell")as! AppointmentNearByTableViewCell
        
        
        let separatorView: UIView = UIView(frame: CGRectMake(0, cell.frame.height + cell.frame.height * 0.0325, cell.frame.width + (cell.frame.width / 3), 1))
        separatorView.layer.borderColor = UIColor.blackColor().CGColor
        separatorView.layer.borderWidth = 1
        cell.contentView.addSubview(separatorView)
        
        cell.setDisplayData(shortDate(datesArray[indexPath.row]),hour:  getHour(datesArray[indexPath.row]), text: textArray[indexPath.row])
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.1125
    }
    
    func addTopBorder(any:UIView,color:UIColor)
    {
        let borderTop = CALayer()
        
        borderTop.frame = CGRectMake(0, 0, any.layer.frame.width + (any.layer.frame.width / 3), 1)
        
        borderTop.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderTop)
    }
    
    func getHour(date:NSDate)-> String
    {
        let components = calendar.components([.Hour,.Minute], fromDate: date)
        let hour =  components.hour
        let minute =  components.minute
        return "\(hour):\(minute)"
    }
    
    func shortDate(date:NSDate) -> String {
        
        let components = calendar.components([.Day, .Month, .Year], fromDate: date)
        
        let components1 = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        let yearToday =  components1.year
        let monthToday = components1.month
        let dayToday = components1.day
        
        if year == yearToday && month == monthToday && day == dayToday
        {
            return NSLocalizedString("TODAY", comment: "")
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.stringFromDate(date)
    }
    
}
