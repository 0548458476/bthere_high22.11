//
//  DeleteCustomerViewController.swift
//  Bthere
//
//  Created by User on 2.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קים פופאפ מחיקת לקוח
class DeleteCustomerViewController: UIViewController {
    var row:Int = 0
    var delegate:deleteItemInTableView!=nil
    var delegateCloseCollectionDelegate:closeCollectionDelegate!=nil
    var coll:UICollectionView?
    @IBOutlet weak var lblNameCustomer: UILabel!
    @IBAction func btnClose(sender: UIButton) {
          self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var btnDeleteOk: UIButton!
    
    @IBAction func btnDeleteOk(sender: UIButton) {
        delegateCloseCollectionDelegate.closeCollection(coll!)
        Global.sharedInstance.isDeleted = false
//        let index:NSIndexPath = NSIndexPath(forRow: row, inSection: secton)
        delegate.deleteItem(row)
        self.dismissViewControllerAnimated(true, completion: nil)
  
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
