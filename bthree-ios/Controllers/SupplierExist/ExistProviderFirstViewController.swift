//
//  ExistProviderFirstViewController.swift
//  bthree-ios
//
//  Created by User on 10.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//דף  ראשון של ספק (כעת קבע הרשאת עובדים)
class ExistProviderFirstViewController: NavigationModelViewController {
    
    
    @IBOutlet weak var lblNameProvider: UILabel!
    
    @IBOutlet weak var lblWelcome: UILabel!
    
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBAction func btnUpdate(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnNo(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var btnNo: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let a = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") {
        
        var dicSupplierName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") as! Dictionary<String,String>
        
        lblNameProvider.text = lblNameProvider.text! + dicSupplierName["nvSupplierName"]!
        }
        else{
            lblNameProvider.text = ""
        }
            //Global.sharedInstance.currentProvider.nvSupplierName
        
        if lblNameProvider.text?.characters.count > 25
        {
            lblNameProvider.font = UIFont(name: "OpenSansHebrew-Bold", size: 13)
            
        }
       else if lblNameProvider.text?.characters.count > 20
        {
            lblNameProvider.font = UIFont(name: "OpenSansHebrew-Bold", size: 16)
            
        }
        else if lblNameProvider.text?.characters.count > 10
        {
            lblNameProvider.font = UIFont(name: "OpenSansHebrew-Bold", size: 24)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
