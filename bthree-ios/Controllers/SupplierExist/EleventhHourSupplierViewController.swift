
//  EleventhHourSupplierViewController.swift
//  bthree-ios
//
//  Created by User on 8.5.2016.
//  Copyright © 2016 Webit. All rights reserved.


import UIKit
//ספק קיים - דף הדקה ה90
class EleventhHourSupplierViewController: NavigationModelViewController
    ,UITableViewDelegate,UITableViewDataSource
{
    
    //MARK: - Outlet
     var delegate:openFromMenuDelegate! = nil
    var generic = Generic()
    
    @IBOutlet weak var viewAddReduce: UIView!
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    ///הוסף מבצע
    @IBAction func btnAddRedude(sender: UIButton) {
       
            let navigationController:UINavigationController = UINavigationController()
            let storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
            let viewCon:NewRedocueViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NewRedocueViewController") as! NewRedocueViewController
            navigationController.viewControllers = [viewCon]
            self.dismissViewControllerAnimated(false, completion: { () -> Void in
                self.delegate.openFromMenu(navigationController)
                
            })

      
       
    }
    @IBOutlet weak var tblCampaigns: UITableView!
    //MARK: - Properties
   
//    var datesArray:Array <NSDate> = [NSDate(),NSDate(timeIntervalSinceReferenceDate: -12345.0),NSDate(timeIntervalSinceReferenceDate: +123000089.0),NSDate()]

    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTopBorder(tblCampaigns, color: UIColor.blackColor())
        tblCampaigns.separatorStyle = .None
        
        var dicListReduces:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicListReduces["iProviderId"] = Global.sharedInstance.currentProvider.iIdBuisnessDetails
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetCouponsForProvider(dicListReduces, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate("לא קיימים מבצעים לספק זה")
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                let coupon:CouponObj = CouponObj()
                Global.sharedInstance.couponsForProvider = coupon.arrayToCouponObj(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                self.dismissViewControllerAnimated(false, completion: nil)
                // self.delegateSearch.openSearchResults()
            }
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global.sharedInstance.couponsForProvider.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("EleventhHourSupplierTableViewCell")as! EleventhHourSupplierTableViewCell
        
        let separatorView: UIView = UIView(frame: CGRectMake(0, cell.frame.height + 0.1095 * (cell.frame.height) , cell.frame.width + (cell.frame.width / 3), 1))
        separatorView.layer.borderColor = UIColor.blackColor().CGColor
        separatorView.layer.borderWidth = 1
        cell.contentView.addSubview(separatorView)
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.setDisplayData(Global.sharedInstance.couponsForProvider[indexPath.row].dDate, campaign: Global.sharedInstance.couponsForProvider[indexPath.row].nvCouponName)
        
        //cell.setDisplayData(imagesArray[indexPath.row], date: shortDate(datesArray[indexPath.row]), text: arrString[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.1125
    }

    func shortDate(date:NSDate) -> String {
        
 //       let calendar = NSCalendar.currentCalendar()
//let components = calendar.components([.Day, .Month, .Year], fromDate: date)
//let components1 = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
//        let year =  components.year
//        let month = components.month
//        let day = components.day
//        
//        let yearToday =  components1.year
//        let monthToday = components1.month
//        let dayToday = components1.day
       
//        if year == yearToday && month == monthToday && day == dayToday
//        {
//            return "היום"
//        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.stringFromDate(date)
    }
    
    func addTopBorder(any:UIView,color:UIColor)
    {
        let borderTop = CALayer()
        
        borderTop.frame = CGRectMake(0, 0, any.layer.frame.width + (any.layer.frame.width / 3), 1)
        
        borderTop.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderTop)
    }
}
