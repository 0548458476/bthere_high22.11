//
//  PrintCalendarViewController.swift
//  bthree-ios
//
//  Created by User on 15.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים - דף הדפס יומן
class PrintCalendarViewController: NavigationModelViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var lblPrintTitle: UILabel!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var viewButtonPdf: UIView!
    var fromColl = 0
    @IBOutlet weak var txtToDate: UITextField!
    @IBOutlet weak var monthAfter: UIButton!
    @IBOutlet weak var monthBefore: UIButton!
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var yearBefore: UIButton!
    @IBOutlet weak var yearAter: UIButton!
    @IBOutlet weak var viewIn: UIView!
    @IBOutlet weak var collDays: UICollectionView!
    
    @IBOutlet weak var lblSendPdf: UILabel!
    @IBOutlet weak var record: CheckBoxPrintCalendar!
    var countOpenCallDays = 0//כמות הפעמים שפתח את האוסף הראשון
    var countOpenCallDaysTodate = 0//כמות הפעמים שפתח את האוסף השני
    @IBOutlet weak var lblYearTodate: UILabel!
    @IBOutlet weak var week: CheckBoxPrintCalendar!
    @IBOutlet weak var lblMonthTodate: UILabel!
    var dateFormatter = NSDateFormatter()
    var selectMonth:Int = 0
    @IBOutlet weak var collToDate: UICollectionView!
    @IBOutlet weak var viewInToDate: UIView!
    @IBAction func btnYearBefore(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.logicalOneYearAgo(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        collDays.reloadData()
        changeLblDate()
    }
    
    @IBAction func btnYearAfter(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.oneYearNext(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        collDays.reloadData()
        changeLblDate()
        
    }
    @IBAction func btnMonthBefore(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.removeMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        collDays.reloadData()
        changeLblDate()
    }
    
    @IBAction func btnMonthAfter(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.addMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        
        collDays.reloadData()
        changeLblDate()
    }
    @IBOutlet weak var day: CheckBoxPrintCalendar!
    
    var numDaysInMonth:Int = 0
    var dateFirst:NSDate = NSDate()
    var dayInWeek:Int = 0
    var i:Int = 1
    var x:Int = 1
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    @IBAction func btnCancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewIn.tag = 0
        viewInToDate.tag = 0
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        //        tap.delegate = self
        txtDate.delegate = self
        //view.addGestureRecognizer(tap)
        
        
        changeLblDate()
        viewIn.hidden = true
        viewInToDate.hidden = true
        txtDate.delegate = self
        txtToDate.delegate = self
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        i = 1
        x = 1
        if language == "he"
        {
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
        }
        
        if language == "he"
        {
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            collToDate.transform = scalingTransform
            collToDate.transform = scalingTransform
            collToDate.transform = scalingTransform
        }
        
        //         txtDate.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        txtDate.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidBeginEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //        self.view.bringSubviewToFront(collDays)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //return Global.sharedInstance.arrDomains.count
        // return arrDomainFilter.count
        if (numDaysInMonth > 30 && dayInWeek == 6
            ) || (numDaysInMonth > 29 && dayInWeek == 7){
            return 42
        }
        return 35
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        //        if indexPath.section == 0 {
        //            let cell:ImageDetailsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageDetailsCollectionViewCell", forIndexPath: indexPath) as! ImageDetailsCollectionViewCell
        //            cell.setDisplayData(detailsProduct.sNameFileD)
        //            cell.transform = scalingTransform
        //            return cell
        //        }
        let cell:PrintCalendarCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("PrintCalendarCollectionViewCell",forIndexPath: indexPath) as! PrintCalendarCollectionViewCell
        //        cell.imgToday.hidden = true
        //        cell.lblDayDesc.alpha = 1.0
        //        cell.lblDayDesc.text = ""
        //        cell.lblDayDesc.textColor = Colors.sharedInstance.color1
        //        if moneForBackColor % 7 == 0{
        //            cell.lblDayDesc.textColor = Colors.sharedInstance.color3
        //        }
        //        moneForBackColor += 1
        if indexPath.row >= (dayInWeek - 1)  && indexPath.row < (numDaysInMonth + dayInWeek - 1 ){
            if collectionView == collDays{
                cell.setDisplayData(i)
                i += 1
            }
            else{
                cell.setDisplayData(x)
                x += 1
                
            }
        }
        else{
            cell.setNull()
            
        }
        
        //        cell.delegate = self
        //        cell.btnCheck.tag = indexPath.row
        if language == "he"
        {
            cell.transform = scalingTransform
        }
        return cell
        
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if let a = collectionView.cellForItemAtIndexPath(indexPath){
            if collectionView == collDays{
                let dateComponents = NSDateComponents()
                //        dateComponents.year = Int(lblYear.text!)!
                dateComponents.month = selectMonth
                dateComponents.day = Int( (collectionView.cellForItemAtIndexPath(indexPath) as!PrintCalendarCollectionViewCell).lblDayDesc.text!)!
                
                let userCalendar = NSCalendar.currentCalendar()
                let someDateTime:NSDate = userCalendar.dateFromComponents(dateComponents)!
                let dateFormat: NSDateFormatter = NSDateFormatter()
                
                dateFormat.dateFormat = "dd/MM/yyyy"
                
                
                
                let dateString: String = dateFormat.stringFromDate(someDateTime)
                txtDate.text = dateString
                viewIn.hidden = true
            }
            else{
                let dateComponents = NSDateComponents()
                //            dateComponents.year = Int(lblYearTodate.text!)!
                dateComponents.month = selectMonth
                dateComponents.day = Int( (collectionView.cellForItemAtIndexPath(indexPath) as!PrintCalendarCollectionViewCell).lblDayDesc.text!)!
                //        dateComponents.timeZone = NSTimeZone(name: "Asia/Tokyo") // user's own time zone is used if not specified
                //        dateComponents.hour = 8
                //        dateComponents.minute = 34
                
                // Create date from components
                let userCalendar = NSCalendar.currentCalendar() // user calendar
                let someDateTime:NSDate = userCalendar.dateFromComponents(dateComponents)!

                //        let components = userCalendar.components([.Day , .Month , .Year], fromDate: someDateTime)
                //        let dateSelect:NSDate = userCalendar.dateFromComponents(components)!
                //        let year =  components.year
                //        let month = components.month
                //        let day = components.day
                let dateFormat: NSDateFormatter = NSDateFormatter()
                //        dateFormat.locale = hebrew
                //        dateFormat.calendar = Mycalendar
                // dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
                //        dateFormatter.timeStyle = .NoStyle
                //        let usDateFormat = NSDateFormatter.dateFormatFromTemplate("MMddyyyy", options: 0, locale: NSLocale(localeIdentifier: "en-US"))
                
                dateFormat.dateFormat = "dd/MM/yyyy"
                
                
                //        let dateString: String = dateFormat.stringFromDate(dateSelect)
                let dateString: String = dateFormat.stringFromDate(someDateTime)
                txtToDate.text = dateString
                viewInToDate.hidden = true
                
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == collDays{
            return CGSize(width:  collDays.frame.size.width / 7, height:   collDays.frame.size.height / 5)
        }
        return CGSize(width:  collToDate.frame.size.width / 7, height:   collToDate.frame.size.height / 5)
    }
    
    //MARK: - TextField
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        //כדי שלא ימחקו הנתונים תמיד
        if textField == txtDate{
            viewIn.hidden = false
            fromColl = 0
            if countOpenCallDays == 0{
                Calendar.sharedInstance.carrentDate = NSDate()
                countOpenCallDays += 1}
            
            changeLblDate()
            
            //txtDate.enabled = false
        }
        else if textField == txtToDate{
            viewInToDate.hidden = false
            fromColl = 1
            if countOpenCallDaysTodate == 0{
                Calendar.sharedInstance.carrentDate = NSDate()
                countOpenCallDaysTodate += 1
            }
            changeLblDate()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        
        return true
    }
    
    // MARK: - KeyBoard
    
    //=======================KeyBoard================
    
    
    func dismissKeyboard() {
        viewIn.hidden = true
        viewIn.tag = 0
        //לבינתיים לא לשכח למחוק
        viewInToDate.hidden = true//לבינתיים לא לשכח למחוק
        viewInToDate.tag = 0
        view.endEditing(true)
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        dismissKeyboard()
        return true;
    }
    
    func changeLblDate(){
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var str:String = ""
        var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
        selectMonth = Int(s1[1])!
        var index:Int = 0
        let characters = s1[1].characters.map { String($0) }
        if characters[0] == String(0){
            str = NSDateFormatter().monthSymbols[Int(characters[1])! - 1]
            //monthArray[Int(characters[1])!]
        }
        else{
            str = NSDateFormatter().monthSymbols[Int(s1[1])! - 1]
            //monthArray[Int(s1[1])!]
            
        }
        
        //lblMonth.text = str + " " + s1[2]
        if viewIn.tag == 1{
            lblMonth.text = str
            lblYear.text = s1[2]
        }
        if viewInToDate.tag == 1{
            lblMonthTodate.text = str
            lblYearTodate.text = s1[2]
        }
        let hebrew: NSLocale?
        if language == "he"
        {
            // Hebrew, Israel
            hebrew = NSLocale(localeIdentifier: "he_IL")
        }
        else
        {
            hebrew = NSLocale(localeIdentifier: "en_IL")
        }
        
        //NSHebrewCalendar
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        let today: NSDate = NSDate()
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.locale = hebrew
        dateFormat.calendar = Mycalendar
        dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
        let dateString: String = dateFormat.stringFromDate(today)
    }
    
    @IBAction func btnOpenTblToDate(sender: AnyObject) {
        if viewInToDate.tag == 0{
            viewInToDate.hidden = false
            viewInToDate.tag = 1
            changeLblDate()
        }
        else{
            viewInToDate.hidden = true
            viewInToDate.tag = 0
            changeLblDate()
        }
    }
    
    @IBAction func btnOpenTblFromDate(sender: UIButton) {
        if viewIn.tag == 0{
            viewIn.hidden = false
            viewIn.tag = 1
            changeLblDate()
        }
        else{
            viewIn.hidden = true
            viewIn.tag = 0
            changeLblDate()
        }
    }
    @IBOutlet weak var btnOpenTblFromDate: UIButton!
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(collDays) || touch.view!.isDescendantOfView(collToDate)) {
            
            return false
        }
        return true
    }
    
    //
    
    
    //        lblHebrewDate.text = dateString    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
