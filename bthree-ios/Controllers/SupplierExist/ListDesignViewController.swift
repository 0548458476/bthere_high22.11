//
//  ListDesignViewController.swift
//  Bthere
//
//  Created by User on 23.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
//דף ספק -  תצוגת רשימה
class ListDesignViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,openFromMenuDelegate{
    var dateturn:String = ""
    var headersArray:Array<String> = ["2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  "]
    var RowsArrayHours:Array<String> = ["08:00-09:30","08:00-09:30","08:00-09:30"]
    var RowsArrayDescs:Array<String> = ["מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית("]
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var year5Month =  0
    var month5Month = 0
    var day5Month = 0
    var yearEvent =  0
    var monthEvent = 0
    var dayEvent = 0
    //מכיל את כל הארועים מהמכשיר ושל ביזר להצגה מחולק לפי תאריך(לכל תאריך יש את הארועים שלו:הקי=תאריך הארוע)
    var dicArrayEventsToShow:Dictionary<String,Array<allKindEventsForListDesign>> = Dictionary<String,Array<allKindEventsForListDesign>>()
    var sortDicEvents:[(String,Array<allKindEventsForListDesign>)] = []//מכיל את כל הארועים הנ״ל בצורה ממויינת לפי תאירך ולפי שעות לכל יום
    let dateFormatter = NSDateFormatter()
    
    
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSyncGoogleSupplier: eyeSynCheckBox!
    //MAERK: - Initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        //------------------------------------------
        let dateToday = NSDate()
        let dateIn5Month = Calendar.sharedInstance.addMonths(NSDate(), numMonthAdd: 5)
        Global.sharedInstance.setAllEventsArray()
        dicArrayEventsToShow = Dictionary<String,Array<allKindEventsForListDesign>>()
        
        //עובר על הארועים מהמכשיר
        for event in Global.sharedInstance.arrEvents
        {
            let dateEvent = event.startDate
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
            year5Month =  components5Month.year
            month5Month = components5Month.month
            day5Month = components5Month.day
            yearEvent =  componentsEvent.year
            monthEvent = componentsEvent.month
            dayEvent = componentsEvent.day
            
            //אם בטווח של 5 חודשים מהיום
            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
            {
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: event.startDate)
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: event.endDate)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                //ליצור אובקט
                //בדיקה אם קיים כזה קי(תאריך)
                let eventPhone:allKindEventsForListDesign = allKindEventsForListDesign(
                    _dateEvent: event.startDate,
                    _title: event.title,
                    _fromHour: "\(hourS_Show):\(minuteS_Show)",
                    _toHour: "\(hourE_Show):\(minuteE_Show)",
                    _tag: 2,
                    _nvAddress: "",
                    _nvSupplierName: "",
                    _iDayInWeek: -1,
                    _nvServiceName: "",
                    _nvComment: "")
                
                
                if dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] != nil
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
                else
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] = Array<allKindEventsForListDesign>()
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
            }
        }
        //עובר על הארועים של ביזר
//        for eventBthere in Global.sharedInstance.ordersOfClientsArray
//        {
//            let dateEvent = eventBthere.dtDateOrder
//            let calendar:NSCalendar = NSCalendar.currentCalendar()
//            
//            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
//            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
//            year5Month =  components5Month.year
//            month5Month = components5Month.month
//            day5Month = components5Month.day
//            yearEvent =  componentsEvent.year
//            monthEvent = componentsEvent.month
//            dayEvent = componentsEvent.day
//            
//            //אם בטווח של 5 חודשים מהיום
//            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
//            {
//                let hourStart = Global.sharedInstance.getStringFromDateString(eventBthere.nvFromHour)
//                let hourEnd = Global.sharedInstance.getStringFromDateString(eventBthere.nvToHour)
//                
//                let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
//                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
//                
//                let hourS = componentsStart.hour
//                let minuteS = componentsStart.minute
//                
//                let hourE = componentsEnd.hour
//                let minuteE = componentsEnd.minute
//                
//                var hourS_Show:String = hourS.description
//                var hourE_Show:String = hourE.description
//                var minuteS_Show:String = minuteS.description
//                var minuteE_Show:String = minuteE.description
//                
//                if hourS < 10
//                {
//                    hourS_Show = "0\(hourS)"
//                }
//                if hourE < 10
//                {
//                    hourE_Show = "0\(hourE)"
//                }
//                if minuteS < 10
//                {
//                    minuteS_Show = "0\(minuteS)"
//                }
//                if minuteE < 10
//                {
//                    minuteE_Show = "0\(minuteE)"
//                }
//                //ליצור אובקט
//                //בדיקה אם קיים כזה קי(תאריך)
//                let eventBtheree:allKindEventsForListDesign = allKindEventsForListDesign(_dateEvent: eventBthere.dtDateOrder, _title: eventBthere.description, _fromHour: "\(hourS_Show):\(minuteS_Show)", _toHour: "\(hourE_Show):\(minuteE_Show)", _tag: 1)
//                
//                if dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] != nil
//                {
//                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
//                }
//                else
//                {
//                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] = Array<allKindEventsForListDesign>()
//                }
//            }
//        }
        //מיון לפי ימים
        sortDicEvents = dicArrayEventsToShow.sort{ dateFormatter.dateFromString($0.0)!.compare(dateFormatter.dateFromString($1.0)!) == .OrderedAscending}
        
        print(sortDicEvents)
        //מיון לכל יום לפי השעות
        var i = 0
        for event in sortDicEvents
        {
            sortDicEvents[i].1.sortInPlace({ $0.dateEvent.compare($1.dateEvent) == NSComparisonResult.OrderedAscending })
            i += 1
        }
        tblData.reloadData()
        //------------------------------------------
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true{
            btnSyncGoogleSupplier.isCecked = true
        }
        
        AppDelegate.i = 0
        
        tblData.separatorStyle = .None
        
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        dicArrayEventsToShow.removeAll()
        dicArrayEventsToShow = Dictionary<String,Array<allKindEventsForListDesign>>()
        
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        Global.sharedInstance.getEventsFromMyCalendar()
        
        //------------------אתחול המערכים להצגת הארועים בצורה ממויינת------------------------
        
        let dateToday = NSDate()
        let dateIn5Month = Calendar.sharedInstance.addMonths(NSDate(), numMonthAdd: 5)
        
        //עובר על הארועים מהמכשיר
        for event in Global.sharedInstance.arrEvents
        {
            let dateEvent = event.startDate
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
            year5Month =  components5Month.year
            month5Month = components5Month.month
            day5Month = components5Month.day
            yearEvent =  componentsEvent.year
            monthEvent = componentsEvent.month
            dayEvent = componentsEvent.day
            
            //אם בטווח של 5 חודשים מהיום
            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
            {
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: event.startDate)
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: event.endDate)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                //ליצור אובקט
                //בדיקה אם קיים כזה קי(תאריך)
                let eventPhone:allKindEventsForListDesign = allKindEventsForListDesign(
                    _dateEvent: event.startDate,
                    _title: event.title,
                    _fromHour: "\(hourS_Show):\(minuteS_Show)",
                    _toHour: "\(hourE_Show):\(minuteE_Show)",
                    _tag: 2,
                    _nvAddress: "",
                    _nvSupplierName: "",
                    _iDayInWeek: -1,
                    _nvServiceName: "",
                    _nvComment: "")
                
                if dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] != nil
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
                else
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] = Array<allKindEventsForListDesign>()
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
            }
        }
        //עובר על הארועים של ביזר
        for eventBthere in Global.sharedInstance.ordersOfClientsArray
        {
            let dateEvent = eventBthere.dtDateOrder
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
            year5Month =  components5Month.year
            month5Month = components5Month.month
            day5Month = components5Month.day
            yearEvent =  componentsEvent.year
            monthEvent = componentsEvent.month
            dayEvent = componentsEvent.day
            
            //אם בטווח של 5 חודשים מהיום
            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
            {
                let hourStart = Global.sharedInstance.getStringFromDateString(eventBthere.nvFromHour)
                let hourEnd = Global.sharedInstance.getStringFromDateString(eventBthere.nvToHour)
                
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                //ליצור אוביקט
                //בדיקה אם קיים כזה קי(תאריך)
                let eventBtheree:allKindEventsForListDesign = allKindEventsForListDesign(
                    _dateEvent: eventBthere.dtDateOrder,
                    _title: eventBthere.description,
                    _fromHour: "\(hourS_Show):\(minuteS_Show)",
                    _toHour: "\(hourE_Show):\(minuteE_Show)",
                    _tag: 1,
                    _nvAddress: eventBthere.nvAddress,
                    _nvSupplierName: eventBthere.nvSupplierName,
                    _iDayInWeek: eventBthere.iDayInWeek,
                    _nvServiceName: eventBthere.objProviderServiceDetail.nvServiceName,
                    _nvComment: eventBthere.nvComment)
                
                if dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] != nil
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
                }
                else
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] = Array<allKindEventsForListDesign>()
                }
            }
        }
        //מיון לפי ימים
        //sortDicEvents = []
        sortDicEvents = [(String,Array<allKindEventsForListDesign>)]()
        sortDicEvents = dicArrayEventsToShow.sort{ dateFormatter.dateFromString($0.0)!.compare(dateFormatter.dateFromString($1.0)!) == .OrderedAscending}
        
        print(sortDicEvents)
        //מיון לכל יום לפי השעות
        var i = 0
        for event in sortDicEvents
        {
            sortDicEvents[i].1.sortInPlace({ $0.dateEvent.compare($1.dateEvent) == NSComparisonResult.OrderedAscending })
            i += 1
        }
        tblData.reloadData()
        //------------------------------------------
    }
    
    @IBOutlet var tblData: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSyncWithGoogel(sender: eyeSynCheckBox) {
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
        }
        else
        {
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
        }
    }
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sortDicEvents.count
        //return arrEventsIn5Month.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return sortDicEvents[section].1.count + 1
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let event = sortDicEvents[indexPath.section]
        
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        let componentsToday = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateFormatter.dateFromString(event.0)!)
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        
        let yearEvent =  componentsEvent.year
        let monthEvent = componentsEvent.month
        let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthEvent - 1]
        let dayEvent = componentsEvent.day
//
        if indexPath.row == 0
        {
            let cell:HeaderRecordTableViewCell = tableView.dequeueReusableCellWithIdentifier("HeaderRecordTableViewCell")as!HeaderRecordTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            let dayWeek = Calendar.sharedInstance.getDayOfWeek(dateFormatter.dateFromString(event.0)!)
            let dayInWeek = NSDateFormatter().veryShortWeekdaySymbols[dayWeek! - 1]
            if componentsToday.day == dayEvent && componentsToday.month == monthEvent && componentsToday.year == yearEvent
            {
                cell.imgToday.hidden = false
            }
            else
            {
                cell.imgToday.hidden = true
            }
            let str =  "," + String(dayEvent) + " " + String(monthName) + " " + String(yearEvent)
            let myNSString = String(yearEvent) as NSString
            let str1 = myNSString.substringWithRange(NSRange(location: 2, length: 2))
            dateturn = String(dayEvent) + "." + String(monthEvent) + "." + String(str1)
            cell.setDisplayData(str,daydesc: dayInWeek)
         
            return cell
        }
        else
        {
        
        let cell:RowInListTableViewCell = tableView.dequeueReusableCellWithIdentifier("RowInListTableViewCell")as!RowInListTableViewCell
        cell.dateTurn = dateturn
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.sortArrEvnt = sortDicEvents
            
            
            let st =  "\(event.1[indexPath.row - 1].fromHour) - \(event.1[indexPath.row - 1].toHour)"
            cell.setDisplayData(st, _desc:event.1[indexPath.row - 1].title,_EventFrom: event.1[indexPath.row - 1].tag,_index: indexPath.section)
            return cell
            
//         cell.setDisplayData(indexPath.section)
//        return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 25
        }
        return self.view.frame.height * 0.18
    }
    
    //openFromMenuDelegate
    func openFromMenu(con:UIViewController){
        self.presentViewController(con, animated: true, completion: nil)
    }

    func showSync()
    {
        if btnSyncGoogleSupplier.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSyncGoogleSupplier.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
        }
        else
        {
            btnSyncGoogleSupplier.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
        }
    }
    
    func small(lhs: NSDate, rhs: NSDate) -> Bool {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        
        //        let isToday:Bool = calendar.isDateInToday(lhs);
        //        if isToday
        //        {
        //            return false
        //        }
        //        else
        //        {
        return lhs.compare(rhs) == .OrderedAscending
        //        }
    }
    
}

