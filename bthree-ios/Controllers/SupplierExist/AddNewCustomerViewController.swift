//
//  AddNewCustomerViewController.swift
//  bthree-ios
//
//  Created by User on 1.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//תת דף הוספת לקוח  מתוך לקוחות שלי
class AddNewCustomerViewController: NavigationModelViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    var isValidFirstName:Bool = true
    var isValidLastName:Bool = true
    var isValidAdress:Bool = true
    var isValidEmail:Bool = true
    var isValidPhone:Bool = true
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var viewIn: UIView!
    @IBOutlet weak var dP: UIDatePicker!
    
    @IBOutlet weak var txtFName: UITextField!
    
    @IBOutlet weak var txtLName: UITextField!
    
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var txtStreet: UITextField!
    
    @IBOutlet weak var txtNumStreet: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var btnPrefix: UIButton!
    
    @IBAction func btnPrefix(sender: AnyObject) {
        if tblPrefix.hidden == true
        {
            tblPrefix.hidden = false
        }
        else
        {
            tblPrefix.hidden = true
        }
    }
    
    @IBOutlet weak var btnMakeAppointment: UIButton!
    
    @IBAction func btnMakeAppointment(sender: AnyObject) {
        
        if txtFName.text == "" || txtFName.text == NSLocalizedString("REQUIREFIELD", comment: ""){
            isValidFirstName = false
            txtFName.text = NSLocalizedString("REQUIRED_FIELD", comment: "")
            txtFName.textColor = UIColor.redColor()
        }
        else{
            isValidFirstName = true
        }
       if txtLName.text == "" || txtLName.text == NSLocalizedString("REQUIREFIELD", comment: ""){
            isValidLastName = false
            txtLName.text = NSLocalizedString("REQUIREFIELD", comment: "")//"שדה חובה*"
            txtLName.textColor = UIColor.redColor()
        }
       else{
        isValidLastName = true
        }
        if txtEmail.text == "" || txtEmail.text == NSLocalizedString("REQUIREFIELD", comment: ""){
            isValidEmail = false
            txtEmail.text = NSLocalizedString("REQUIREFIELD", comment: "")//"שדה חובה*"
            txtEmail.textColor = UIColor.redColor()
        }
        else if !Validation.sharedInstance.mailValidation(txtEmail.text!){
            isValidEmail = false
            txtEmail.text = NSLocalizedString("MAIL_VALID", comment: "")//"חובה להכניס אימייל תקין*"
            txtEmail.textColor = UIColor.redColor()
        }
        else{
           isValidEmail = true
        }
        if txtPhone.text == "" || txtPhone.text == NSLocalizedString("REQUIREFIELD", comment: ""){
            isValidPhone = false
            txtPhone.text = NSLocalizedString("REQUIREFIELD", comment: "")//"שדה חובה*"
            txtPhone.textColor = UIColor.redColor()
        }
        else{
            isValidPhone = true
        }
        if isValidPhone == true && isValidEmail == true && isValidLastName == true &&
            isValidFirstName == true{
            self.dismissViewControllerAnimated(true, completion:nil)
        }
    }
    
    @IBOutlet weak var imgOpenPrefix: UIImageView!
    
    @IBOutlet weak var txtViewComments: UITextView!
    
    @IBOutlet weak var tblPrefix: UITableView!
    
    var arrPrefix:Array<String> = ["050","052","053","054","058"]
    
    @IBAction func btnVIPyes(sender: CheckBoxForExistSupplierOk) {
        btnVIPno.isCecked = false
    }
    @IBOutlet weak var btnVIPyes: CheckBoxForExistSupplierOk!
    
    @IBAction func btnVIPno(sender: CheckBoxForExistSupplierCancel) {
        btnVIPyes.isCecked = false
    }
    @IBOutlet weak var btnVIPno: CheckBoxForExistSupplierCancel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPrefix.hidden = true
        dP.backgroundColor = Colors.sharedInstance.color1
        dP.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dP.setValue(0.8, forKeyPath: "alpha")
        dP.datePickerMode = UIDatePickerMode.Date
        self.viewIn.bringSubviewToFront(dP)
        dP.hidden = true
        self.view.bringSubviewToFront(dP)
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -80
        let minDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        
        components.year = -0
        let maxDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        
        dP.minimumDate = minDate
        dP.maximumDate = maxDate
        
        
        tblPrefix.separatorStyle = .None
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
         tap.delegate = self
        txtDate.delegate = self
        view.addGestureRecognizer(tap)
        
        txtDate.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        dP.addTarget(self, action: #selector(AddNewCustomerViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    txtFName.delegate = self
        txtLName.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
          btnPrefix.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // dateFormatter.dateFormat = "yyyyMMdd"
        
        var toNSDate : NSDate!
        txtDate.text = dateFormatter.stringFromDate(sender.date)
        //dP.hidden = true
        
        //        if lastTextField == txtDate {
        //            txtDate.text = dateFormatter.stringFromDate(sender.date)
        //
        //            toNSDate = dateFormatter.dateFromString(txtDate.text!)
        //            toNSDate = NSCalendar.currentCalendar().dateByAddingUnit(
        //                .Day,
        //                value: 1,
        //                toDate: toNSDate,
        //                options: NSCalendarOptions(rawValue: 0))
        //            stringDate = DateHelper.sharedInstance.convertNSDateToString(toNSDate)
        //
        //        }
        //
        //        else if lastTextField == copyTxtDate {
        //            copyTxtDate.text = dateFormatter.stringFromDate(sender.date)
        //            toNSDate = dateFormatter.dateFromString(copyTxtDate.text!)
        //            toNSDate = NSCalendar.currentCalendar().dateByAddingUnit(
        //                .Day,
        //                value: 1,
        //                toDate: toNSDate,
        //                options: NSCalendarOptions(rawValue: 0))
        //            stringDate = DateHelper.sharedInstance.convertNSDateToString(toNSDate)
        //        }
    }
    
    //    func datePickerValueChanged(sender:UIDatePicker) {
    //        //,textfield:UITextField
    //        let dateFormatter = NSDateFormatter()
    //
    //        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
    //
    //        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
    //
    //        cell2 = tblDetails.cellForRowAtIndexPath(indexPathDate) as? PersonalDetailsTableViewCell
    //
    //        cell2!.txtfDetails.text = dateFormatter.stringFromDate(sender.date)
    //
    //    }
    
    // MARK: - KeyBoard
    
    //=======================KeyBoard================
    
    
    func dismissKeyboard() {
        dP.hidden = true
        view.endEditing(true)
        tblPrefix.hidden = true
    }
    
    // MARK: - TextField
    //=========================TextField==============
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.textColor = UIColor.blackColor()
        if textField.text == NSLocalizedString("REQUIREFIELD", comment: "") || textField.text == NSLocalizedString("MAIL_VALID", comment: "")
        {
            textField.text = ""
        }

        if textField == txtDate
        {
            textField.inputView = UIView()
            dP.hidden = false
        }
        else
        {
            dP.hidden = true
        }
        
        
        
        //        self.validPhone.text = ""
        //        self.validEmal.text = ""
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        dismissKeyboard()
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
    }
    
    //MARK: - tableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrPrefix.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell =
            tableView.dequeueReusableCellWithIdentifier("newTurnTableViewCell")as!newTurnTableViewCell
        
        cell.setDisplayData(arrPrefix[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        btnPrefix.setTitle((tableView.cellForRowAtIndexPath(indexPath) as! newTurnTableViewCell).lblText.text, forState: .Normal)
        tblPrefix.hidden = true
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return tblPrefix.frame.height / 3
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(tblPrefix)) {
            
            return false
        }
        return true
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
