//
//  editUserDetailsViewController.swift
//  Bthere
//
//  Created by User on 1.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים-(הגדרות) דף עריכת פרטי לקוח
class editUserDetailsViewController: NavigationModelViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBOutlet weak var txtFName: UITextField!
    
    @IBOutlet weak var txtLName: UITextField!
    
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtBirthDate: UITextField!
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var btnImgAlbum: UIButton!
    
    @IBAction func btnImgAlbum(sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    @IBOutlet weak var btnImgCamera: UIButton!
    
    @IBAction func btnImgCamera(sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.Custom
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
            
        else {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
    }
    @IBOutlet weak var viewHebrew: UIView!
    
    @IBOutlet weak var lblHebrew: UILabel!
    
    @IBOutlet weak var viewArabic: UIView!
    
    @IBOutlet weak var lblArabic: UILabel!
    
    @IBOutlet weak var viewEnglish: UIView!
    
    @IBOutlet weak var lblEnglish: UILabel!
    
    @IBOutlet weak var viewRussian: UIView!
    
    @IBOutlet weak var lblRussian: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBAction func btnSave(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        txtEmail.delegate = self
        txtFName.delegate = self
        txtLName.delegate = self
        txtPhone.delegate = self
        txtBirthDate.delegate = self
        
        
        
        let tapLng1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeToHebrew))
        self.viewHebrew.addGestureRecognizer(tapLng1)
        
        let tapLng2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeToArabic))
        self.viewArabic.addGestureRecognizer(tapLng2)
        
        let tapLng3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeToEnglish))
        self.viewEnglish.addGestureRecognizer(tapLng3)
        
        let tapLng4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeToRussian))
        self.viewRussian.addGestureRecognizer(tapLng4)
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
    }
    
    
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func changeToHebrew()
    {
        language = "he"
        viewHebrew.backgroundColor = Colors.sharedInstance.color3
        viewEnglish.backgroundColor = UIColor.clearColor()
        viewArabic.backgroundColor = UIColor.clearColor()
        viewRussian.backgroundColor = UIColor.clearColor()
        changeLanguage(language as String)
        
    }
    
    func changeToArabic()
    {
        language = "ar"
        viewArabic.backgroundColor = Colors.sharedInstance.color3
        
        viewEnglish.backgroundColor = UIColor.clearColor()
        viewHebrew.backgroundColor = UIColor.clearColor()
        viewRussian.backgroundColor = UIColor.clearColor()
        changeLanguage(language as String)

    }
    
    func changeToEnglish()
    {
        language = "en"
        viewEnglish.backgroundColor = Colors.sharedInstance.color3
        
        viewArabic.backgroundColor = UIColor.clearColor()
        viewHebrew.backgroundColor = UIColor.clearColor()
        viewRussian.backgroundColor = UIColor.clearColor()
        changeLanguage(language as String)
    }
    
    func changeToRussian()
    {
        language = "ru"
        viewRussian.backgroundColor = Colors.sharedInstance.color3
        viewEnglish.backgroundColor = UIColor.clearColor()
        viewHebrew.backgroundColor = UIColor.clearColor()
        viewArabic.backgroundColor = UIColor.clearColor()
         changeLanguage(language as String)
        
    }
    func changeLanguage(iLang:String)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
              appDelegate.changeLanguage(iLang)
        
            Alert.sharedInstance.showAlert(NSLocalizedString("LANG_CHANGE_NEXR_ENTRANCE", comment: ""),vc: self)
    }
    
    
    //MARK: - Images
    
    /// ImagesCamera() - this function open images file
    /// or camera to get an image.

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //place the image that selected from the library
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        let img:UIImage = Global.sharedInstance.resizeImage(image, newWidth: image.size.width*0.5)
        let base64String = Global.sharedInstance.setImageToString(img)
        
            UIGraphicsBeginImageContext(imgUser.frame.size)
            image.drawInRect(imgUser.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            imgUser.image = image
            
            //Global.sharedInstance.addProviderBuisnessProfile.nvHeaderImage = base64String
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }


}
