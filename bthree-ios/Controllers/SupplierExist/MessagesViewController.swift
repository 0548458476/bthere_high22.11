//
//  MessagesViewController.swift
//  Bthere
//
//  Created by User on 1.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים דף הודעות מגיע מתפריט צד
class MessagesViewController: NavigationModelViewController ,UITableViewDelegate,UITableViewDataSource,deleteMessageDelegate{

    @IBOutlet weak var tblMessages: UITableView!
    var selectedMessages:Array<Bool> = [false,false,false,false]//מערך זה אותחל כרגע באופן ידני אך לאחר הקריאה לשרת יאותחל כמספר ההודעות שהתקבלו מהשרת
    override func viewDidLoad() {
        super.viewDidLoad()
tblMessages.separatorStyle = .None
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return Global.sharedInstance.MessageArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if selectedMessages[section] == false{
            return 1
        }
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0{
            let  cell = tableView.dequeueReusableCellWithIdentifier("MessageTableViewCell")as!MessageTableViewCell
            cell.setDisplayData(Global.sharedInstance.MessageArray[indexPath.section],dateMes: NSDate())
            cell.selectionStyle = .None
            if indexPath.section == 0{
                cell.viewTop.hidden = false
            }
            else{
                cell.viewTop.hidden = true
            }
            if selectedMessages[indexPath.section] == true{
                cell.viewButtom.hidden = true
            }
            else{
                  cell.viewButtom.hidden = false
            }
            return cell
        }
        let  cell = tableView.dequeueReusableCellWithIdentifier("AllMessageTableViewCell")as!AllMessageTableViewCell
        cell.section = indexPath.section
        cell.delegate = self
        cell.selectionStyle = .None
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if selectedMessages[indexPath.section] == false{
            selectedMessages[indexPath.section] = true
        }
        else{
            selectedMessages[indexPath.section] = false

        }
        //tblMessages.reloadData()
        self.tblMessages.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: .Fade)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0{
        return view.frame.size.height*0.1
        }
        return view.frame.size.height*0.2

    }
    
    func  deleteMessage(section:Int){
        selectedMessages.removeAtIndex(section)
        
     tblMessages.reloadData()
    }
    //    var verticalSpace = NSLayoutConstraint(item: self.imageView, attribute: .Bottom, relatedBy: .Equal, toItem: self.button, attribute: .Bottom, multiplier: 1, constant: 50)
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
