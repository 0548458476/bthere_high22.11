//
//  SupplierCustomerViewController.swift
//  bthree-ios
//
//  Created by User on 10.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים (לבדוק אם משתמשים)
class SupplierCustomerViewController: UIViewController {
    
    
    @IBOutlet weak var viewSupplierCustomer: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    
    var supplierStoryBoard:UIStoryboard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        supplierStoryBoard = UIStoryboard(name: "SupplierExist", bundle: nil)
        
        self.navigationController?.navigationBarHidden = true
        
        if Global.sharedInstance.isProvider == true
        {
            viewSupplierCustomer.backgroundColor = Colors.sharedInstance.color3
            
            if Global.sharedInstance.defaults.valueForKey("currentClintName") != nil {
                
                var dicClientName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("currentClintName") as! Dictionary<String,String>
                
                lblName.text = dicClientName["nvClientName"]//Global.sharedInstance.currentUser.nvFirstName
                
            }
            else
            {
                lblName.text = ""
            }
        }
        else
        {
            viewSupplierCustomer.backgroundColor = Colors.sharedInstance.color4
            
            
            if Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") != nil {
                
                var dicSupplierName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("supplierNameRegistered") as! Dictionary<String,String>
                
                lblName.text = dicSupplierName["nvSupplierName"]
                //Global.sharedInstance.currentProvider.nvSupplierName
            }
            else
            {
                lblName.text = ""
            }
        }

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openNextVC))
        viewSupplierCustomer.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openNextVC() {
        if Global.sharedInstance.isProvider == true
        {
            Global.sharedInstance.isProvider = false
            
            let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
            let vc = storyboard!.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
            frontviewcontroller?.pushViewController(vc, animated: false)
            
            
       //initialize REAR View Controller- it is the LEFT hand menu.
            let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
            
            let mainRevealController = SWRevealViewController()
            
            mainRevealController.frontViewController = frontviewcontroller
            mainRevealController.rearViewController = rearViewController
            
            let window :UIWindow = UIApplication.sharedApplication().keyWindow!
            window.rootViewController = mainRevealController
   
        }
        else
        {
            Global.sharedInstance.isProvider = true

            let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
            let vc = supplierStoryBoard!.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
            frontviewcontroller?.pushViewController(vc, animated: false)
            
// initialize REAR View Controller- it is the LEFT hand menu.
 
            let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
            
            let mainRevealController = SWRevealViewController()
            
            mainRevealController.frontViewController = frontviewcontroller
            mainRevealController.rearViewController = rearViewController
            
            let window :UIWindow = UIApplication.sharedApplication().keyWindow!
            window.rootViewController = mainRevealController            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
