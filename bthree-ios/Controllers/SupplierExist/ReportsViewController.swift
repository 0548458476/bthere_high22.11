//
//  ReportsViewController.swift
//  Bthere
//
//  Created by User on 31.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol PushReportDelegate {
    func PushReport(viewCon:UIViewController)
}

// ספק קים דף רשימת של כל הדוחות הקימים
class ReportsViewController: NavigationModelViewController,UITableViewDelegate,UITableViewDataSource{
    var arrayNameReports:Array<String> = ["ניצולת תפעולית","מס׳ לקוחות"]
    var delegate:PushReportDelegate!=nil
    @IBOutlet weak var tblReports: UITableView!
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReports.separatorStyle = .None
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayNameReports.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ReportInListTableViewCell")as!ReportInListTableViewCell
        cell.setDisplayData(arrayNameReports[indexPath.row])
        if indexPath.row == 0{
            cell.viewTop.hidden = false
        }
        else{
            cell.viewTop.hidden = true
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            self.dismissViewControllerAnimated(true, completion: {
                let storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
                let viewCon:OperationalUtilizationViewController = storyboard.instantiateViewControllerWithIdentifier("OperationalUtilizationViewController") as! OperationalUtilizationViewController
                self.delegate.PushReport(viewCon)
            })
        default:
            print("asd")
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.06
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
