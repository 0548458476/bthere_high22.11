//
//  MenuPlusSupplierViewController.swift
//  Bthere
//
//  Created by User on 23.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol presentTableDelegate{
    func presentMenuOnSelf(view:UIViewController)
}
//תפריט פלוס 
class MenuPlusSupplierViewController: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet weak var viewNewAppointment: UIView!
 var delegate:openFromMenuDelegate! = nil
    @IBOutlet weak var viewNumCustomers: UIView!
    @IBOutlet weak var viewLastMinute: UIView!
    @IBOutlet weak var viewBlockingHours: UIView!
    @IBOutlet weak var viewHelp: UIView!
    @IBOutlet weak var viewUpdateTurn: UIView!
    
    @IBOutlet var lblLastMinute: UILabel!
    @IBOutlet var lblWaitingList: UILabel!
    @IBOutlet var lblMesseges: UILabel!
    @IBOutlet var lblNewOrder: UILabel!
    @IBAction func btnCancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var viewI: UIView!
    @IBOutlet weak var viewListWait: UIView!
    @IBOutlet weak var viewMessages: UIView!
    var storyBoard:UIStoryboard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    storyBoard = UIStoryboard(name:"SupplierExist", bundle: nil)
    let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedViewHelp))
        viewHelp.userInteractionEnabled = true
        viewHelp.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.delegate = self
    let tapGestureRecognizerNewAppointment = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedNewAppointment))
        viewNewAppointment.userInteractionEnabled = true
        viewNewAppointment.addGestureRecognizer(tapGestureRecognizerNewAppointment)
        tapGestureRecognizerNewAppointment.delegate = self
        let tapGestureRecognizerLastMinute = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedLastMinute))
        viewLastMinute.userInteractionEnabled = true
        viewLastMinute.addGestureRecognizer(tapGestureRecognizerLastMinute)
        tapGestureRecognizerLastMinute.delegate = self
        
        let tapGestureRecognizerI = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedI))
        viewI.userInteractionEnabled = true
        viewI.addGestureRecognizer(tapGestureRecognizerI)
        tapGestureRecognizerI.delegate = self
        
        let tapGestureMessages = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedMessagesViewController))
        viewMessages.userInteractionEnabled = true
        viewMessages.addGestureRecognizer(tapGestureMessages)
        tapGestureMessages.delegate = self
        
        let tapGestureNumCustomers = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedNumCustomers))
        viewNumCustomers.userInteractionEnabled = true
        viewNumCustomers.addGestureRecognizer(tapGestureNumCustomers)
        tapGestureNumCustomers.delegate = self
        
        
        let tapGestureUpdateTurn = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedUpdateTurn))
        viewUpdateTurn.userInteractionEnabled = true
        viewUpdateTurn.addGestureRecognizer(tapGestureUpdateTurn)
        tapGestureUpdateTurn.delegate = self
        
        let tapGestureBlockingHours = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedBlockHours))
        viewBlockingHours.userInteractionEnabled = true
        viewBlockingHours.addGestureRecognizer(tapGestureBlockingHours)
        tapGestureBlockingHours.delegate = self

        
        let tapGestureListWait = UITapGestureRecognizer(target:self, action:#selector(MenuPlusSupplierViewController.imageTappedListWait))
        viewListWait.userInteractionEnabled = true
        viewListWait.addGestureRecognizer(tapGestureListWait)
        tapGestureListWait.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageTapped(){
        
        //let storyBoard = UIStoryboard(name:"SupplierExist", bundle: nil)
        let viewCon:MenuPlusSupplierViewController = storyBoard!.instantiateViewControllerWithIdentifier("MenuPlusSupplierViewController") as! MenuPlusSupplierViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func imageTappedViewHelp()  {
        let navigationController:UINavigationController = UINavigationController()
   //     var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:HelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HelpViewController") as! HelpViewController
     
         navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)

        })
       
    }

    func imageTappedNewAppointment()  {
        let navigationController:UINavigationController = UINavigationController()
    //    var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:NewTurnViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NewTurnViewController") as! NewTurnViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedLastMinute()  {
        let navigationController:UINavigationController = UINavigationController()
    //    var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:EleventhHourSupplierViewController = self.storyboard?.instantiateViewControllerWithIdentifier("EleventhHourSupplierViewController") as! EleventhHourSupplierViewController
        viewCon.delegate = Global.sharedInstance.eleventCon

        
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedI()  {
        let navigationController:UINavigationController = UINavigationController()
    //    var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:InformationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InformationViewController") as! InformationViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedMessagesViewController()  {
        let navigationController:UINavigationController = UINavigationController()
     //   var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:MessagesViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessagesViewController") as! MessagesViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedNumCustomers()  {
        let navigationController:UINavigationController = UINavigationController()
      //  var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:NumCustumersParallelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NumCustumersParallelViewController") as! NumCustumersParallelViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }

    func imageTappedUpdateTurn()  {
        let navigationController:UINavigationController = UINavigationController()
    //    var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:UpdateAppointmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UpdateAppointmentViewController") as! UpdateAppointmentViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedBlockHours()  {
        let navigationController:UINavigationController = UINavigationController()
       // var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:BlockHoursViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BlockHoursViewController") as! BlockHoursViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    
    func imageTappedListWait()  {
        let navigationController:UINavigationController = UINavigationController()
     //   var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let viewCon:WaitingListViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WaitingListViewController") as! WaitingListViewController
        navigationController.viewControllers = [viewCon]
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.delegate.openFromMenu(navigationController)
            
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
