//
//  CostumerAppointmentsViewController.swift
//  Bthere
//
//  Created by User on 24.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק  דף רשימת  תורים 
class CostumerAppointmentsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,openFromMenuDelegate{
    @IBAction func btnClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    var headersArray:Array<String> = ["2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  "]
    var RowsArrayHours:Array<String> = ["08:00-09:30","08:00-09:30","08:00-09:30"]
    var RowsArrayDescs:Array<String> = ["מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית("]
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    
    @IBOutlet weak var tblData: UITableView!
    //MAERK: - Initial
    
    override func viewDidLoad() {
    super.viewDidLoad()
    Global.sharedInstance.appointmentsCostumers = self
    
    Global.sharedInstance.setAllEventsArray()
    AppDelegate.i = 0
    tblData.separatorStyle = .None
    // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return Global.sharedInstance.arrEvents.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    //return 3
    return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let event =  Global.sharedInstance.arrEvents[indexPath.section]
    
    let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
    let componentsToday = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
    let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
    //print(event.startDate)
    yearToday =  componentsCurrent.year
    monthToday = componentsCurrent.month
    dayToday = componentsCurrent.day
    
    let yearEvent =  componentsEvent.year
    let monthEvent = componentsEvent.month
    let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthEvent - 1]
    
    let dayEvent = componentsEvent.day
    //
    if indexPath.row == 0{
    let cell:HeaderRecordTableViewCell = tableView.dequeueReusableCellWithIdentifier("HeaderRecordTableViewCell")as!HeaderRecordTableViewCell
    cell.selectionStyle = UITableViewCellSelectionStyle.None
    let dayWeek = Calendar.sharedInstance.getDayOfWeek(event.startDate)
    let dayInWeek = NSDateFormatter().veryShortWeekdaySymbols[dayWeek! - 1]
    if componentsToday.day == dayEvent{
    cell.imgToday.hidden = false
    }
    else{
    cell.imgToday.hidden = true
    }
    let str =  "," + String(dayEvent) + " " + String(monthName) + " " + String(yearEvent)
    cell.setDisplayData(str,daydesc: dayInWeek)
    return cell
    }
    
    
    
    let cell:AppointmentTableViewCell = tableView.dequeueReusableCellWithIdentifier("AppointmentTableViewCell")as!AppointmentTableViewCell
    cell.selectionStyle = UITableViewCellSelectionStyle.None
    cell.setDisplayData(indexPath.section)
    return cell
    
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.row == 0{
    return 25
    }
    return self.view.frame.height * 0.08
    }
    
    //openFromMenuDelegate
    func openFromMenu(con:UIViewController){
    self.presentViewController(con, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
