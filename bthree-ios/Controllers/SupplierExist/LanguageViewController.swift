//
//  LanguageViewController.swift
//  Bthere
//
//  Created by User on 1.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// דף לבחור שפה למכשיר 
class LanguageViewController: NavigationModelViewController ,UITableViewDataSource,UITableViewDelegate,deleteMessageDelegate{
    var arrayLanguages:Array<String> = ["עברית","English","русский","العربية"]
    var section = 0
    
    @IBOutlet weak var tblLang: UITableView!
    
    
    @IBOutlet weak var viewBack: UIView!
    
    @IBAction func btnSave(sender: AnyObject) {
    }
  
    //MARK: - Initial
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tblLang.separatorStyle = .None
        tblLang.scrollEnabled = false
  self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg-pic-supplier@x1.jpg")!)
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(back))
        viewBack.addGestureRecognizer(tap)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
      
            let  cell = tableView.dequeueReusableCellWithIdentifier("LanguageTableViewCell")as!LanguageTableViewCell
            cell.setDisplayData(arrayLanguages[indexPath.row])
        cell.selectionStyle = .None
        cell.row = indexPath.row
        cell.delegate = self
        if indexPath.row != self.section{
            cell.viewLang.backgroundColor = UIColor.clearColor()
            cell.lblLang.textColor = UIColor.blackColor()
            cell.viewLang.tag = 0
        }
        if indexPath.row == 0{
            cell.viewTop.hidden = false
        }
        else{
               cell.viewTop.hidden = true
        }
        return cell
      
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return view.frame.size.height*0.1
    }
    
    func  deleteMessage(section:Int){
        self.section = section
        tblLang.reloadData()
    }
    
    func back()
    {
        let revealController: SWRevealViewController = self.revealViewController()
        let frontViewController: UIViewController = revealController.frontViewController
        let frontNavigationController: UINavigationController? = nil
        let storybrd: UIStoryboard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let DefinationsController: DefinationsViewController = storybrd.instantiateViewControllerWithIdentifier("DefinationsViewController")as! DefinationsViewController
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: DefinationsController)
        revealController.pushFrontViewController(navigationController, animated: true)
    }
}
