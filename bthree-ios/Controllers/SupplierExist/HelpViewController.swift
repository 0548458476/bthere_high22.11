//
//  HelpViewController.swift
//  Bthere
//
//  Created by User on 19.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// דף עזרה
class HelpViewController: NavigationModelViewController,UITableViewDelegate,UITableViewDataSource {
    var questionsArray:Array<String> = ["איך אני מוסיף לקוח חדש?","החלפתי שם לעסק מה אני צריך לעשות","באיזה גודל אני צריכהלעשות את הלוגו",""]
    @IBOutlet weak var viewAddQusetionTap: UIView!
    
    @IBOutlet weak var viewInsertQuestion: UIView!
    @IBOutlet weak var topTblCon: NSLayoutConstraint!
    @IBOutlet weak var tblQuestions: UITableView!
    var topTbl:NSLayoutConstraint!
    var topTblClose:NSLayoutConstraint!

    @IBAction func btnClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func btnCls(sender: UIButton) {
        //topTbl = topTblCon
        view.removeConstraint(topTblClose)
//        let verticalConstraint = NSLayoutConstraint(item: tblQuestions, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: viewInsertQuestion, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: viewInsertQuestion.frame.height)
        viewInsertQuestion.hidden = true
        view.addConstraint(topTblCon)
    }
    @IBOutlet weak var btnCloseCell: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
    tblQuestions.separatorStyle = .None
        let gesture = UITapGestureRecognizer(target: self, action: #selector(HelpViewController.openViewQuestions))
        self.viewAddQusetionTap.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
  return questionsArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
         }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
      
        if indexPath.row == 0{
        let  cell = tableView.dequeueReusableCellWithIdentifier("QuestionTableViewCell")as!QuestionTableViewCell
        cell.setDisplayData(questionsArray[indexPath.section])
        return cell
        }
        let  cell = tableView.dequeueReusableCellWithIdentifier("QuestionTableViewCell")as!QuestionTableViewCell
        cell.selectionStyle = .None
        return cell
          }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return view.frame.size.height*0.1
    }
//    var verticalSpace = NSLayoutConstraint(item: self.imageView, attribute: .Bottom, relatedBy: .Equal, toItem: self.button, attribute: .Bottom, multiplier: 1, constant: 50)

    func openViewQuestions()  {
       
        topTbl = topTblCon
        view.removeConstraint(topTbl)
        let verticalConstraint = NSLayoutConstraint(item: tblQuestions, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: viewInsertQuestion, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: viewInsertQuestion.frame.height)
        
        topTblClose = verticalConstraint
         viewInsertQuestion.hidden = false
        view.addConstraint(verticalConstraint)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
