//
//  MyCostumersViewController.swift
//  bthree-ios
//
//  Created by User on 21.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים רשימת הלקוחות שלי
class MyCostumersViewController: NavigationModelViewController,UIGestureRecognizerDelegate,UITextFieldDelegate,deleteItemInTableView,closeCollectionDelegate {
    var arrayCustomer:Array<String> = []
    @IBOutlet weak var tblCustomers: UITableView!
    @IBOutlet weak var viewAddCustomer: UIView!
    
    @IBOutlet weak var txtSearchCustomer: UITextField!
    
    @IBOutlet weak var viewSearch: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance.mycustomers = self
        Global.sharedInstance.PresentViewMe = self
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MyCostumersViewController.viewAddCustomerTapped))
        viewAddCustomer.userInteractionEnabled = true
        viewAddCustomer.addGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer.delegate = self
      
        // Do any additional setup after loading the view.
        
        let tapKeyBoard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard))
        tapKeyBoard.delegate = self
        self.view.addGestureRecognizer(tapKeyBoard)
        
        txtSearchCustomer.delegate = self
        txtSearchCustomer.text = NSLocalizedString("SEARCH_CUSTOMER", comment: "")
    }
    @IBAction func btnOpenNewCustomer(sender: UIButton) {
        let navigationController:UINavigationController = UINavigationController()
        //     var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
          var storyboard = UIStoryboard(name: "SupplierExist", bundle: nil)
         let viewCon:AddNewCustomerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddNewCustomerViewController") as! AddNewCustomerViewController
        navigationController.viewControllers = [viewCon]
       
        self.presentViewController(navigationController, animated: true, completion: nil)

    }
    
    override func viewDidAppear(animated: Bool) {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View
    

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.08
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return Global.sharedInstance.nameCostumersArray.count
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: MyCostumerTableViewCell = tableView.dequeueReusableCellWithIdentifier("MyCostumerTableViewCell") as! MyCostumerTableViewCell
        
        cell.row = indexPath.section
        cell.viewDelegate = self
       
//        let index:NSIndexPath = NSIndexPath(forRow:0, inSection: 0)
        if Global.sharedInstance.isDeleted == true{
             cell.collItems.reloadData()
        }
//        cell.collItems.scrollToItemAtIndexPath(index, atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
//
//        }
            cell.setDisplayData(indexPath.section)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
        
    func viewAddCustomerTapped()  {
       
          }
    
        // MARK: - text field
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == txtSearchCustomer
        {
            if textField.text == NSLocalizedString("SEARCH_CUSTOMER", comment: "")
            {
                textField.text = ""
            }
        }
    }

    func textFieldDidEndEditing(textField: UITextField) {
        if textField == txtSearchCustomer
        {
            if textField.text == ""
            {
                textField.text = NSLocalizedString("SEARCH_CUSTOMER", comment: "")
            }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func dismissKeyBoard() {
        view.endEditing(true)
    }
    
    
    
    func deleteItem(indexPath:Int){
         Global.sharedInstance.isDeleted = true
      
            Global.sharedInstance.nameCostumersArray.removeAtIndex(indexPath)
         Global.sharedInstance.imgCostumersArray.removeAtIndex(indexPath)
        tblCustomers.reloadData()

    }
    
    func closeCollection(collection:UICollectionView)
    {
        let index:NSIndexPath = NSIndexPath(forRow:0, inSection: 0)
//        if Global.sharedInstance.isDeleted == true{
//collection.reloadData()
            collection.scrollToItemAtIndexPath(index, atScrollPosition: UICollectionViewScrollPosition.Left, animated: false
        )
            
//        }
    }
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//        if (touch.view!.isDescendantOfView()) {
//            
//            return false
//        }
//        return true
//    }

}
