//
//  NewTurnViewController.swift
//  bthree-ios
//
//  Created by User on 1.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים -להוסיף תור חדש   
class NewTurnViewController: NavigationModelViewController
,UITextViewDelegate{
    
    @IBOutlet weak var lblNameServicer: UILabel!

    @IBOutlet weak var lblServiceTypeSelected: UILabel!
    @IBOutlet weak var lblNameCustomer: UILabel!
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    //MARK: - connections
    
    
    @IBOutlet weak var lblTitleNewTurn: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBOutlet weak var lblCostumer: UILabel!
    
    @IBOutlet weak var viewCostumer: UIView!
    
    @IBAction func btnOpenCostumer(sender: AnyObject) {
        if tblCostumers.tag == 0
        {
            tblCostumers.tag = 1
            tblCostumers.hidden = false
        }
        else
        {
            tblCostumers.tag = 0
            tblCostumers.hidden = true
        }
        
    }
    
    @IBOutlet weak var tblCostumers: UITableView!
    
    
    @IBOutlet weak var lblServiceProvider: UILabel!
    
    @IBOutlet weak var viewServiceProvider: UIView!
    
    @IBAction func btnOpenServiceProvider(sender: AnyObject) {
        
        if tblServiceProvider.tag == 0
        {
            tblServiceProvider.tag = 1
            tblServiceProvider.hidden = false
        }
        else
        {
            tblServiceProvider.tag = 0
            tblServiceProvider.hidden = true
        }
    }
    
    @IBOutlet weak var tblServiceProvider: UITableView!
    
    
    
    @IBOutlet weak var lblServiceType: UILabel!
    
    @IBOutlet weak var viewServiceType: UIView!
    
    @IBAction func btnOpenServiceType(sender: AnyObject) {
        
        if tblServiceType.tag == 0
        {
            tblServiceType.tag = 1
            tblServiceType.hidden = false
        }
        else
        {
            tblServiceType.tag = 0
            tblServiceType.hidden = true
        }
    }
    
    @IBOutlet weak var tblServiceType: UITableView!
    
    
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var viewDate: UIView!
    
    @IBOutlet weak var btnOpenDate: UIButton!
    
    @IBAction func btnOpenDate(sender: AnyObject) {
    }
    
    
    
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var viewHour: UIView!
    
    @IBAction func btnOpenHour(sender: AnyObject) {
    }
    
    @IBOutlet weak var lblRemark: UILabel!
    
    @IBOutlet weak var txtVRemark: UITextView!
    
    
    
    @IBOutlet weak var btnMakeAppointment: UIButton!
    
    @IBAction func btnMakeAppointment(sender: AnyObject) {
    }
    
    
    @IBOutlet weak var lblSendMessage: UILabel!
    
    @IBOutlet weak var btnSendMessage: UIButton!
    
    @IBAction func btnSendMessage(sender: AnyObject) {
        var image:UIImage = UIImage()
        if btnSendMessage.tag == 0
        {
            btnSendMessage.tag = 1
            image = UIImage(named: "15a.png")!
        }
        else
        {
            btnSendMessage.tag = 0
            image = UIImage(named: "16a.png")!
        }
        
        UIGraphicsBeginImageContext(btnSendMessage.frame.size)
        image.drawInRect(btnSendMessage.bounds)
        let image1: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        btnSendMessage.backgroundColor = UIColor(patternImage: image1)
    }
    
    var arrProviders:Array<String> = ["David","Moshe","Levi","Daniel"]
    var arrSeviceProviders:Array<String> = ["Avi","Uri","Simcha","Meir"]
    var arrSeviceTypes:Array<String> = ["רפואה אלטרנטיבית","ספרות","פסיכולוגיה","יעוץ"]
    
    //MARK: - initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

         if language != "he" && (DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P) {
            
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 21)
            
            lblCostumer.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblSendMessage.font = UIFont(name: "OpenSansHebrew-Light", size: 13)
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 13)
        }
         else if language != "he" && (DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS) {
            
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 18)
            
            lblCostumer.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 12)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblSendMessage.font = UIFont(name: "OpenSansHebrew-Light", size: 12)
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 11)
         }

         else if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            
            lblTitleNewTurn.font = UIFont(name: "OpenSansHebrew-Bold", size: 22)
            
            lblCostumer.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceProvider.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblServiceType.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblDate.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblHour.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblRemark.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            lblSendMessage.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            btnMakeAppointment.titleLabel!.font =  UIFont(name: "OpenSansHebrew-Light", size: 17)
        }
        
        
        txtVRemark.delegate = self
        
        tblCostumers.separatorStyle = .None
        tblServiceProvider.separatorStyle = .None
        tblServiceType.separatorStyle = .None
        
        tblCostumers.hidden = true
        tblServiceProvider.hidden = true
        tblServiceType.hidden = true
        
        //the table is closed
        tblCostumers.tag = 0
        tblServiceProvider.tag = 0
        tblServiceType.tag = 0
        
        btnSendMessage.tag = 0
        
        lblTitleNewTurn.text = NSLocalizedString("NEW_TURN_TITLE", comment: "")
        lblCostumer.text = NSLocalizedString("COSTUMER", comment: "")
        lblServiceProvider.text = NSLocalizedString("SERVICE_PROVIDER", comment: "")
        lblServiceType.text = NSLocalizedString("SERVICE_TYPE", comment: "")
        lblDate.text = NSLocalizedString("DATE", comment: "")
        lblHour.text = NSLocalizedString("HOUR", comment: "")
        lblRemark.text = NSLocalizedString("REMARK", comment: "")
        lblSendMessage.text = NSLocalizedString("SEND_MESSAGE", comment: "")
        btnMakeAppointment.setTitle(NSLocalizedString("MAKE_APPOINTMENT", comment: ""), forState: .Normal)
        tblCostumers.separatorStyle = .None
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - dismiss key board
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            txtVRemark.resignFirstResponder()
        }
        super.touchesBegan(touches, withEvent:event)
    }
    
    //    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    //        if text == "\n" {
    //            textView.resignFirstResponder()
    //            return false
    //        }
    //        return true
    //    }
    
    
    //MARK: - table view
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCostumers{
            return arrProviders.count
        }
        else if tableView == tblServiceProvider
        {
            return arrSeviceProviders.count
        }
        else
        {
            return arrSeviceTypes.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell =
        tableView.dequeueReusableCellWithIdentifier("newTurnTableViewCell")as!newTurnTableViewCell
        
        if tableView == tblCostumers{
            cell.setDisplayData(arrProviders[indexPath.row])
        }
        else if tableView == tblServiceProvider
        {
            cell.setDisplayData(arrSeviceProviders[indexPath.row])
        }
        else
        {
            cell.setDisplayData(arrSeviceTypes[indexPath.row])
        }
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == tblCostumers{
         lblNameCustomer.text = arrProviders[indexPath.row]
            tblCostumers.hidden = true
            tblCostumers.tag = 0
        }
        if tableView == tblServiceProvider{
            lblNameServicer.text = arrSeviceProviders[indexPath.row]
            tblServiceProvider.hidden = true
            tblServiceProvider.tag = 0
        }
        if tableView == tblServiceType{
            lblServiceTypeSelected.text = arrSeviceTypes[indexPath.row]
            tblServiceType.hidden = true
            tblServiceType.tag = 0
        }
        
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return self.view.frame.size.height * 0.07
    }

    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
