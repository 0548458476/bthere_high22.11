//
//  PopSyncContactViewController.swift
//  Bthere
//
//  Created by User on 3.7.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//פופאפ של סנכרון אנשי קשר
class PopSyncContactViewController: NavigationModelViewController {

    @IBOutlet weak var lblWhySync: UILabel!
    @IBOutlet weak var txtSyncReason1: UILabel!
    
    @IBOutlet weak var txtSyncReason: UITextView!
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBAction func btnCon(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnContinue.setTitle(NSLocalizedString("CONTINUE", comment: ""), forState: .Normal)
        lblWhySync.text = NSLocalizedString("WYH_SYNC", comment: "")
        txtSyncReason.text = NSLocalizedString("SYNC_REASON", comment: "")
        txtSyncReason1.text = NSLocalizedString("SYNC_REASON1", comment: "")
        txtSyncReason1.font = UIFont (name: "OpenSansHebrew-Light", size: 16)
        txtSyncReason.font = UIFont (name: "OpenSansHebrew-Light", size: 16)
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            txtSyncReason1.font = UIFont (name: "OpenSansHebrew-Light", size: 15)
            txtSyncReason.font = UIFont (name: "OpenSansHebrew-Light", size: 15)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
