//
//  ListServicesViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/23/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol showMoreInfoDelegate
{
    func showMoreInfo(index:Int)
    func showOdrerTurn()
}
protocol reloadTblServiceDelegte {
    func reloadTblService()
}
// תת דף רשימת שירותים
class ListServicesViewController: NavigationModelViewController,UITableViewDelegate,UITableViewDataSource,showMoreInfoDelegate,openSearchResultsDelegate,openFromMenuDelegate,reloadTblServiceDelegte {
    
    @IBOutlet weak var lblAdvertising: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewBlack: UIView!
    
    @IBOutlet weak var imgImage: UIImageView!
    
    //עפולה, 2 ק״מ ממיקומך
    @IBOutlet weak var imgWaze: UIImageView!
    
    @IBOutlet weak var openPlusMenu: UIImageView!
   
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var lblNumKM: UILabel!
    
    @IBOutlet weak var lblKMFromYou: UILabel!
    
    // דירוג: 8.9 | 32 מדרגים
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var lblNumRuting: UILabel!
    
    @IBOutlet weak var lblNumVoters: UILabel!
    
    @IBOutlet weak var lblVoters: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    var generic:Generic = Generic()
    
    var delegateSearch:openSearchResultsDelegate!=nil
    var delegateReloadCol:reloadCollectionDelegate!=nil
    var clientStoryBoard:UIStoryboard?
    var storyboard1:UIStoryboard?
    
    var delegate:dismissViewControllerDelegate!=nil
    
    var backFromMyListServices = 0//מציין האם הגיעו מרשימת נותני השרות שלי 
    
    var ProviderServicesArray:Array<objProviderServices> = Array<objProviderServices>()
    var ProviderServicesArraySort:Array<objProviderServices> = Array<objProviderServices>()
    var iServiceType = 0
    var x = 0
    var indexRow = 0
    var isSort = false//מציין האם נבחר מישהו למיין לפיו את התוצאות
    
    @IBAction func btnClose(sender: AnyObject) {
        if backFromMyListServices == 1//מנותני השרות שלי
        {
            //פתיחת נותני השרות
            openGiveMyService()
        }
        else
        {
            openSearchResults()
        }
    }
    
    @IBAction func btnOpenMenu(sender: UIButton) {

       if x == 0{
    
       tblDropDownList.hidden = false
            x = 1
      }
        else{
           x = 0
            tblDropDownList.hidden = true
        }
          tblDropDownList.reloadData()

    }
    @IBOutlet var btnOpenMenu: UIButton!
    var arrayServices:Array<String> = [NSLocalizedString("SERVICE", comment: ""),NSLocalizedString("PRODUCT", comment: ""),NSLocalizedString("SERIES_TURNS", comment: ""),NSLocalizedString("GIFT_CARD", comment: "")]
   
    @IBOutlet weak var tblDropDownList: UITableView!
    @IBOutlet var tblServices: UITableView!
    
    //MARK: - Initial
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnOpenMenu.setTitle(NSLocalizedString("SERVICE", comment: ""), forState: .Normal)
        lblTitle.text = NSLocalizedString("WANT_ORDER", comment: "")
        lblAdvertising.text = NSLocalizedString("ADVERTISINGS", comment: "")
        //מיון דיפולטיבי לפי שירות
        isSort = true
        iServiceType = 90
        ProviderServicesArraySort = []
        for proService in ProviderServicesArray {
            if proService.iServiceType == iServiceType
            {
                ProviderServicesArraySort.append(proService)
            }
        }
        isSort = true
        tblServices.reloadData()
        
        storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        self.view.bringSubviewToFront(tblDropDownList)
        tblServices.separatorStyle = .None
        tblDropDownList.separatorStyle = .None
         tblDropDownList.hidden = true
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ListServicesViewController.imageTapped))
        openPlusMenu.userInteractionEnabled = true
        openPlusMenu.addGestureRecognizer(tapGestureRecognizer)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {

        tblServices.reloadData()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
        
        Global.sharedInstance.currentProviderToCustomer = SearchResulstsObj(
            _nvProviderName: (Global.sharedInstance.dicResults[indexRow]["nvProviderName"]?.description)!,
            _nvAdress: Global.sharedInstance.dicResults[indexRow]["nvAdress"]!.description,
            _nvProviderSlogan: Global.sharedInstance.dicResults[indexRow]["nvProviderSlogan"]!.description,
            _nvProviderLogo: (Global.sharedInstance.dicResults[indexRow]["nvProviderLogo"]?.description)!)
        
        Global.sharedInstance.providerID = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
        
        lblName.text = Global.sharedInstance.dicResults[indexRow]["nvProviderName"]?.description
        lblDesc.text = Global.sharedInstance.dicResults[indexRow]["nvProviderSlogan"]!.description
        lblCity.text = Global.sharedInstance.dicResults[indexRow]["nvAdress"]!.description
        lblNumRuting.text = Global.sharedInstance.dicResults[indexRow]["IInternalRank"]!.description
        lblNumVoters.text = Global.sharedInstance.dicResults[indexRow]["iCustomerRank"]!.description
        
        if Global.sharedInstance.dicResults[indexRow]["iDistance"]!.description != "-1"
        {
        lblNumKM.text = String(format: "%.2f", (Global.sharedInstance.dicResults[indexRow]["iDistance"] as! Float))
         
        lblKMFromYou.text = NSLocalizedString("KILOMETER_FROM_YOU", comment: "")
        }
        else
        {
            lblNumKM.text = ""
            lblKMFromYou.text = ""
        }

        var decodedimage:UIImage = UIImage()
        
        let dataDecoded:NSData = NSData(base64EncodedString: (Global.sharedInstance.dicResults[indexRow]["nvProviderLogo"]?.description)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            imgLogo.image = decodedimage
            imgLogo.contentMode = .ScaleAspectFill
        }
        else
        {
            imgLogo.backgroundColor = Colors.sharedInstance.color4
            imgLogo.image = UIImage(named: "clients@x1.png")
            imgLogo.contentMode = .ScaleAspectFit
        }

        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblDesc.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
            lblName.font = UIFont(name: "OpenSansHebrew-Bold", size: 15)
        }
        
    }
    
    // MARK: - TabelView
    //=========================TabelView=================

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == tblServices
        {
        return 70
        }
        return tblDropDownList.frame.size.height * (1/4)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == tblServices
        {
            if isSort == false
            {
                return self.ProviderServicesArray.count
            }
            return self.ProviderServicesArraySort.count
        }
        return 1
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblServices
        {
            return 1
        }
        
        return arrayServices.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        if tableView == tblServices
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("ServicesTableViewCell") as! ServicesTableViewCell
            delegateReloadCol = cell
            cell.index = indexPath.section
            cell.selectionStyle = .None
            cell.delegateMoreInfo = self
            if isSort == false
            {
                cell.ProviderServicesArray = self.ProviderServicesArray
            }
            else
            {
                cell.ProviderServicesArray = self.ProviderServicesArraySort
            }
            cell.parent_ListServicesViewController = self
            cell.setDisplayData(indexPath.section)
            //כדי להחזיר את הגלילה הצידה
//            cell.collServices.setContentOffset(CGPoint(x: 0 , y: 0), animated: false)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("ItemInListTableViewCell") as! ItemInListTableViewCell
            cell.selectionStyle = .None
            cell.setDisplayData(arrayServices[indexPath.row])
            if indexPath.row == 3{
                cell.viewButtom.hidden = true
            }
            else{
                cell.viewButtom.hidden = false
            }
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if tableView == tblDropDownList
        {
            btnOpenMenu.setTitle((tableView.cellForRowAtIndexPath(indexPath) as! ItemInListTableViewCell).lblDesc.text, forState: .Normal)
            tblDropDownList.hidden = true
            
            if arrayServices[indexPath.row] == NSLocalizedString("PRODUCT", comment: "")
            {
                iServiceType = 89
            }
            else if arrayServices[indexPath.row] == NSLocalizedString("SERVICE", comment: "")
            {
                iServiceType = 90
            }
            ProviderServicesArraySort = []
            for proService in ProviderServicesArray {
                if proService.iServiceType == iServiceType
                {
                    ProviderServicesArraySort.append(proService)
                }
            }
            isSort = true
            tblServices.reloadData()
            
            //delegateReloadCol.reloadCollection(ProviderServicesArraySort)
            x = 0
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
    }
    
    
    func showMoreInfo(index:Int)
    {
        let viewCon:ServiceInformationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ServiceInformation") as! ServiceInformationViewController
        
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        viewCon.serviceName = ProviderServicesArray[index].nvServiceName
        viewCon.sumOfService = String(ProviderServicesArray[index].iPrice)
        viewCon.durationOfService = String(ProviderServicesArray[index].iTimeOfService)
        viewCon.name = lblName.text!
        viewCon.desc = lblDesc.text!
        viewCon.address = lblCity.text!
        viewCon.km = lblNumKM.text!
        viewCon.imageLogo = imgLogo.image!
        viewCon.numRuting = lblNumRuting.text!
        viewCon.numVoters
            = lblNumVoters.text!
        
        
        var decodedimage:UIImage = UIImage()
        
        let dataDecoded:NSData = NSData(base64EncodedString: (Global.sharedInstance.dicResults[indexRow]["nvProviderHeader"]?.description)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            viewCon.img = decodedimage
        }
        else
        {
            viewCon.img = UIImage(named: "IMG_05072016_131013.png")!
        }
        
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func showOdrerTurn()
    {
        //                //    קבלת רשימת נותני שרות לספק הנבחר getServicesProviderForSupplier//
        getServicesProviderForSupplierfunc()
    }

    func getServicesProviderForSupplierfunc()
    {
        Global.sharedInstance.giveServiceName = ""
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = Global.sharedInstance.providerID
        var arrUsers:Array<User> = Array<User>()
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            api.sharedInstance.getServicesProviderForSupplierfunc(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_GIVES_SERVICE", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    var Arr:NSArray = NSArray()
                    
                    Arr = responseObject["Result"] as! NSArray
                    let u:User = User()
                    arrUsers = u.usersToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    Global.sharedInstance.giveServicesArray = arrUsers
                    Global.sharedInstance.arrayGiveServicesKods = []
                    for var item in arrUsers{
                        Global.sharedInstance.arrayGiveServicesKods.append(item.iUserId)//אחסון הקודים של נותני השרות לצורך השליחה לשרת כדי לקבל את השעות הפנויות
                    }
                    Global.sharedInstance.dicGetFreeDaysForServiceProvider["lServiseProviderId"] = Global.sharedInstance.arrayGiveServicesKods
                    self.getFreeDaysForServiceProvider()
                    self.GetCustomerOrders()
                    if arrUsers.count == 0
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_GIVES_SERVICE", comment: ""))
                        
                    }
                    else
                    {
                        Global.sharedInstance.arrayWorkers = []
                        for u:User in arrUsers
                        {
                            let s = u.nvFirstName + " " + u.nvLastName
                            Global.sharedInstance.arrayWorkers.append(s)
                        }
                        Global.sharedInstance.arrayWorkers.insert(NSLocalizedString("NOT_CARE", comment: ""), atIndex: 0)//כדי שיהיה הראשון
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
        
    }
    
    
    func getFreeDaysForServiceProvider(){
        //        var dicGetFreeDaysForServiceProvider:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        
        
       
        //formatter.locale = NSLocale(localeIdentifier: "US_en")
        formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
        let uniqueVals = Global.sharedInstance.uniq(Global.sharedInstance.arrayServicesKodsToServer)
        Global.sharedInstance.arrayServicesKodsToServer = uniqueVals
        Global.sharedInstance.dicGetFreeDaysForServiceProvider["lProviderServiceId"] = Global.sharedInstance.arrayServicesKodsToServer
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
            //מחזירה ימים פנויים לספק
            api.sharedInstance.GetFreeDaysForServiceProvider(Global.sharedInstance.dicGetFreeDaysForServiceProvider, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                {
                    let date = NSDate()
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    
                    dateFormatter.timeZone = NSTimeZone(name: "GMT")
                    var Arr:NSArray = NSArray()
                    
                    Arr = responseObject["Result"] as! NSArray
                    let ps:providerFreeDaysObj = providerFreeDaysObj()
                    
                    Global.sharedInstance.getFreeDaysForService = ps.objFreeDaysToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                    //איפוס המערך מנתונים ישנים
                    Global.sharedInstance.dateFreeDays = []
                    for i in 0 ..< Global.sharedInstance.getFreeDaysForService.count{
                        //היום שיש בו שעות פנויות
                        let dateDt = Calendar.sharedInstance.addDay(Global.sharedInstance.getStringFromDateString(Global.sharedInstance.getFreeDaysForService[i].dtDate))
                        
                        Global.sharedInstance.dateFreeDays.append(dateDt)//מערך שמכיל את תאריכי הימים בהם אפשר לקבוע תור ז״א יש בהם שעות פנויות
                        
                    }
                    Global.sharedInstance.fromHourArray = Global.sharedInstance.dateFreeDays
                    Global.sharedInstance.endHourArray = Global.sharedInstance.dateFreeDays
                    
                    
                    for i in 0 ..< Global.sharedInstance.dateFreeDays.count{//יש לי מערך של תאריכים ובאותו מקום של התאריך במערך יש לי את השעות של התורים הפנויים של אותו תאריך
                        let provider:providerFreeDaysObj = Global.sharedInstance.getFreeDaysForService[i]
                        
                        let hourStart = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvFromHour)
                        
                        Global.sharedInstance.fromHourArray[i] = hourStart
                        let hourEnd = Global.sharedInstance.getStringFromDateString(provider.objProviderHour.nvToHour)
                        Global.sharedInstance.endHourArray[i] = hourEnd
                        
                    }
                    
                    let currentDate:NSDate = NSDate()
                    var dayOfWeekToday = 0
                    
                    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
                    
                    //אתחול מערך השעות הפנויות לשבוע
                    for i in 0 ..< 7 {
                        
                        let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i + 1)
                        
                        Global.sharedInstance.setFreeHours(curDate, dayOfWeek: i)
                        Global.sharedInstance.getBthereEvents(curDate, dayOfWeek: i)
                    }
                    
                    self.generic.hideNativeActivityIndicator(self)
                    
                    self.dismissViewControllerAnimated(false, completion: nil)
                    //self.delegate.dismissViewController()
                    let viewCon = self.storyboard?.instantiateViewControllerWithIdentifier("ModelCalendarForAppointments") as! ModelCalendarForAppointmentsViewController
                    Global.sharedInstance.idWorker = -1
                    self.navigationController?.pushViewController(viewCon, animated: false)
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
           //self.GetCustomerOrders()
    }
    
    func openSearchResults()
    {
        let frontviewcontroller = storyboard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func openGiveMyService()
    {
        let frontviewcontroller = storyboard1!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        
        let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        let giveMyServices:giveMyServicesViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("giveMyServicesViewController")as! giveMyServicesViewController
        
        frontviewcontroller?.pushViewController(giveMyServices, animated: false)
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard1!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    //פונקציה זו מחזירה את התורים של הלקוח
    func GetCustomerOrders()  {
        var dic:Dictionary<String,AnyObject> =  Dictionary<String,AnyObject>()
        var arr = NSArray()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
         api.sharedInstance.GetCustomerOrders(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            print(responseObject["Result"])
            arr = responseObject["Result"] as! NSArray
            let ps:OrderDetailsObj = OrderDetailsObj()
            Global.sharedInstance.ordersOfClientsArray = ps.OrderDetailsObjToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
           // print(Global.sharedInstance.ordersOfClientsArray)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
         })
    }
    
    func imageTapped(){
        
        let storyboard1:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewCon:MenuPlusViewController = storyboard1.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }

    func reloadTblService()
    {
        tblServices.reloadData()
    }
    }
