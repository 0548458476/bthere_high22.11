//
//  MenuPlusViewController.swift
//  bthree-ios
//
//  Created by User on 22.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// דף תפריט פלוס
class MenuPlusViewController: UIViewController {
    
    
    var delegate:openFromMenuDelegate!=nil
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var viewNewAppointment: UIView!
    
    @IBOutlet weak var imgNewAppointment: UIImageView!
    
  
    @IBOutlet weak var viewNewEvent: UIView!
    
    @IBOutlet weak var imgNewEvent: UIImageView!
//    @IBOutlet weak var viewBusinessPage: UIView!
//    
//    @IBOutlet weak var imgBisnesPage: UIImageView!
    
    
    @IBOutlet weak var viewLittleLate: UIView!
    
    @IBOutlet weak var imgLittleLate: UIImageView!
    
    @IBOutlet weak var viewListWait: UIView!
    
    @IBOutlet weak var imgListWait: UIImageView!
    
//    @IBOutlet weak var viewI: UIView!
    
//    @IBOutlet weak var imgI: UIImageView!
    
    @IBOutlet weak var viewNotArrive: UIView!
    
    @IBOutlet weak var imgNotArrive: UIImageView!
    
    @IBOutlet weak var viewHelp: UIView!
    
    @IBOutlet weak var imgHelp: UIImageView!
    
    @IBOutlet weak var viewUpdateTurn: UIView!
    
    @IBOutlet weak var imgUpdateTurn: UIImageView!
    
    @IBOutlet weak var viewLastMinute: UIView!
    
    @IBOutlet weak var imgLastMinute: UIImageView!
    
    var clientStoryBoard:UIStoryboard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        
       
//        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openviewBusinessPage))
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewHelp))
//        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewI))
        let tap3:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openNewEvent))
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewLastMinute))
        let tap5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewListWait))
        let tap6: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewLittleLate))
        let tap7: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewNewAppointment))
        let tap8: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewNotArrive))
        let tap9: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuPlusViewController.openViewUpdateTurn))
        
//        viewBusinessPage.addGestureRecognizer(tap1)
        viewHelp.addGestureRecognizer(tap2)
        viewNewEvent.addGestureRecognizer(tap3)
        //viewI.addGestureRecognizer(tap3)
        viewLastMinute.addGestureRecognizer(tap4)
        viewListWait.addGestureRecognizer(tap5)
        viewLittleLate.addGestureRecognizer(tap6)
        viewNewAppointment.addGestureRecognizer(tap7)
        viewNotArrive.addGestureRecognizer(tap8)
        viewUpdateTurn.addGestureRecognizer(tap9)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func openviewBusinessPage()
    {
        
    }
    
    func openNewEvent()
    {
        let viewCon:NewEventViewController = clientStoryBoard?.instantiateViewControllerWithIdentifier("NewEventViewController") as! NewEventViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.openFromMenu(viewCon)
    }
    
    func openViewHelp()
    {}
    
    func openViewI()
    {}
    
    func openViewLastMinute()
    {
        let viewCon:EleventhHourViewController = clientStoryBoard?.instantiateViewControllerWithIdentifier("EleventhHourViewController") as! EleventhHourViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.openFromMenu(viewCon)
        
    }
    
    func openViewListWait()
    {
        let viewCon:MyWaitingListViewController = clientStoryBoard!.instantiateViewControllerWithIdentifier("MyWaitingListViewController") as! MyWaitingListViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.openFromMenu(viewCon)
    }
    
    func openViewLittleLate()
    {}
    
    //תור חדש
    func openViewNewAppointment()
    {
        let viewCon:NewAppointmentClientViewController = clientStoryBoard!.instantiateViewControllerWithIdentifier("NewAppointmentClientViewController") as! NewAppointmentClientViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.openFromMenu(viewCon)
    
    }
    
    func openViewNotArrive()
    {}
    
    func openViewUpdateTurn()
    {}
    override func viewWillLayoutSubviews() {
        let bounds = UIScreen.mainScreen().bounds
        
        let width = bounds.size.width
        let height = bounds.size.height
        //let y = bounds.origin.y
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
             self.view.superview!.bounds = CGRect(x:0,y:-220,width:width,height: height)
        }
        else
        {
            self.view.superview!.bounds = CGRect(x:0,y:-300,width:width,height: height)
        }

    }
    
    
}