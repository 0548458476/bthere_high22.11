//
//  ContactSyncViewController.swift
//  Bthere
//
//  Created by User on 29.6.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol hideChooseAllDelegate {
    func hideChooseAll()
}
//דף 5 בהרשמה סנכרון עם אנשי קשר

class ContactSyncViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,hideChooseAllDelegate {
   var x = 0
   
    @IBOutlet weak var lblChooseCustomers: UILabel!
    
    @IBOutlet weak var lblChooseAll: UILabel!
    
    @IBOutlet weak var btnChooseAll: UIButton!
    @IBOutlet weak var viewchooseAll: UIView!
    
    @IBOutlet weak var tblConcats: UITableView!
    
    //MARK: - initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblChooseAll.text = NSLocalizedString("SELECT_ALL", comment: "")
        lblChooseCustomers.text = NSLocalizedString("CHOOSE_CUSTOMER", comment: "")
        
        Global.sharedInstance.setContacts()
        tblConcats.separatorStyle = .None
        
        btnChooseAll.hidden = false
        for contact in Global.sharedInstance.contactList
        {
            contact.bIsSync = true
        }
        
        let tapViewAll:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chooseAll))
        
        viewchooseAll.addGestureRecognizer(tapViewAll)
        
//        if Global.sharedInstance.isClickCon == true{
      
//        }
//  self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg-pic-supplier@x1.jpg")!)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if x == 0
        {
            let viewCon:PopSyncContactViewController = storyboard.instantiateViewControllerWithIdentifier("PopSyncContactViewController") as! PopSyncContactViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            //viewCon.delegate = Global.sharedInstance.rgisterModelViewController
            self.presentViewController(viewCon, animated: true, completion: nil)
            x = 1
        }
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblChooseCustomers.font = UIFont(name: "OpenSansHebrew-Regular", size: 12.5)
            lblChooseAll.font = UIFont(name: "OpenSansHebrew-Regular", size: 12.5)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Global.sharedInstance.contactList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("ContactManSyncTableViewCell")as!ContactManSyncTableViewCell
        
        cell.delegate = self
        cell.btnISsync.tag = indexPath.row
        
       cell.setDisplayData(Global.sharedInstance.contactList[indexPath.row].nvContactName)

        cell.btnISsync.hidden = !Global.sharedInstance.contactList[indexPath.row].bIsSync
        cell.selectionStyle = .None
        
        if indexPath.row == 0
        {
            cell.viewTOp.hidden = false
        }
        else{
            cell.viewTOp.hidden = true
        }
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (tableView.cellForRowAtIndexPath(indexPath)as!ContactManSyncTableViewCell).btnISsync.hidden == true//כדי להדליק איש קשר
        {
            (tableView.cellForRowAtIndexPath(indexPath)as!ContactManSyncTableViewCell).btnISsync.hidden = false
            Global.sharedInstance.contactList[indexPath.row].bIsSync = true
            
            var isChooseAll = true
            //בדיקה האם להדליק את הכפתור- בחר הכל, אם בחרתי את כולם
            for Item in Global.sharedInstance.contactList
            {
                if Item.bIsSync == false
                {
                    isChooseAll = false
                }
            }
            if isChooseAll == true
            {
                btnChooseAll.hidden = false
            }
        }
        else//כדי לכבות איש קשר
        {
            (tableView.cellForRowAtIndexPath(indexPath)as!ContactManSyncTableViewCell).btnISsync.hidden = true
            Global.sharedInstance.contactList[indexPath.row].bIsSync = false
            btnChooseAll.hidden = true
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return view.frame.size.height*0.18
    }
    
    func chooseAll()
    {
        if btnChooseAll.hidden == true
        {
            btnChooseAll.hidden = false
            for contact in Global.sharedInstance.contactList
            {
                contact.bIsSync = true
            }
        }
        else
        {
            btnChooseAll.hidden = true
            for contact in Global.sharedInstance.contactList
            {
                contact.bIsSync = false
            }
        }
        tblConcats.reloadData()
    }


    
    //פונקציה המופעלת בעת לחיצה על כפתור של איש קשר כדי לכבותו,
    //הפונקציה מכבה את הכפתור ״בחר הכל״
    func hideChooseAll()
    {
        btnChooseAll.hidden = true
    }
}
