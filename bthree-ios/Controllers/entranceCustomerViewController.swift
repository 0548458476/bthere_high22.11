
//  entranceCustomerViewController.swift
//  Bthere
//
//  Created by User on 7.7.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// כניסה ללקוח קיים
class entranceCustomerViewController: NavigationModelViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,openFromMenuDelegate,openSearchResultsDelegate {

    
    @IBOutlet weak var tblSearch: UITableView!
    
    @IBOutlet var imgView: UIView!
    
    @IBOutlet weak var btnOpemMenu: UIImageView!

    @IBOutlet weak var txtSearch: UITextField!
    
    //    var filterSubArr:Array<String> = []
    //fix 2.3
     var filterSubArr:NSArray =  NSArray()
    //var filterSubArr:NSMutableArray =  NSMutableArray()//fix 2.3
    var attributedStrings = NSMutableAttributedString(string:"")
    var generic:Generic = Generic()
    
    var isAuto = false
    
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//עבור השליחה לשרת
    
    
    @IBAction func btnSearch(sender: AnyObject) {
        
        dismissKeyboard()
        Global.sharedInstance.searchDomain = txtSearch.text!
        
        if txtSearch.text != ""
        {
            Global.sharedInstance.dicSearch["nvKeyWord"] = txtSearch.text
            Global.sharedInstance.dicSearch["nvlong"] = Global.sharedInstance.currentLong
            Global.sharedInstance.dicSearch["nvlat"] = Global.sharedInstance.currentLat
            Global.sharedInstance.dicSearch["nvCity"] = nil
            
            generic.showNativeActivityIndicator(self)
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.SearchByKeyWord(Global.sharedInstance.dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_RESULTS", comment: ""))
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        print("תוצאות חיפוש :\(Global.sharedInstance.dicResults)" )
                        Global.sharedInstance.dicSearchProviders = Global.sharedInstance.dicSearch
                        Global.sharedInstance.whichSearchTag = 1
                        self.openSearchResults()
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                })
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NOT_DATA_TO_SEARCH", comment: ""),vc: self)
        }
        
        //try function
        
        
        // to write in the server func of search...
        //        if Global.sharedInstance.dicResults.count != 0
        //        {
        //            openSearchResults()
        //        }
        //        else
        //        {
        //            Alert.sharedInstance.showAlertDelegate("לא קיימים ספקים  המתאימים לחיפוש")
        //        }
    }
    
    @IBOutlet weak var btnSeachAdvantage: UIButton!
    
    //MARK: - Properties
    //===========Properties============
    var clientStoryBoard:UIStoryboard?
    var filtered:Array<String> = []
    
    var searchActive : Bool = false
    
    //MARK: - initial
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.placeholder = NSLocalizedString("SERACH_SERVICE_DOMAIN", comment: "")
        btnSeachAdvantage.setTitle(NSLocalizedString("ADVANTAGED_SEARCH", comment: ""), forState: .Normal)
        
        clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        txtSearch.delegate = self

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(entranceCustomerViewController.imageTapped))
        btnOpemMenu.userInteractionEnabled = true
        btnOpemMenu.addGestureRecognizer(tapGestureRecognizer)
        
        self.view.bringSubviewToFront(tblSearch)
        
        self.imgView.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        tblSearch.hidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(entranceCustomerViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
      
        self.navigationItem.setHidesBackButton(true, animated:true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - TableView
    //==========TableView===============
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterSubArr.count == 0
        {
            tblSearch.hidden = true
        }
        return filterSubArr.count
        
        
        //return filtered.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Search")as! SearchTableViewCell
        
            cell.setDisplayData(filterSubArr[indexPath.row]  as! String )
                       //        cell.textLabel?.text = filterSubArr[indexPath.row]
        //            //filtered[indexPath.row]
        //        cell.textLabel?.textAlignment = NSTextAlignment.Right
        //        cell.textLabel?.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
        //        cell.layer.borderColor = Colors.sharedInstance.
        //cell.layer.borderWidth = 1
        
        //        var  selectionColor:UIView = UIView()
        //        selectionColor.backgroundColor = Colors.sharedInstance.green
        //        cell.selectedBackgroundView = selectionColor
        return cell

            }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.09687
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        txtSearch.text = filterSubArr[indexPath.row] as? String
        //txtSearch.text = filtered[indexPath.row]
        tblSearch.hidden = true
    }
    
    //MARK: - Searching
    //==========Searching===========
    
    func textFieldDidChange(textField: UITextField) {
        searching(txtSearch.text!)
    }
    
    func searching(searchText: String) {
        let s:CGFloat = view.frame.size.height - tblSearch.frame.origin.x
        searchActive = true
        if searchText == ""{
            filtered =  []
        }else{
            filtered = Global.sharedInstance.categoriesArray.filter({(city) ->  Bool in
                var tmp: NSString = ""
                if tmp.description.characters.count >= searchText.characters.count{
                    tmp = tmp.substringToIndex(searchText.characters.count)
                }
                
                let range = tmp.rangeOfString(searchText,options: NSStringCompareOptions.CaseInsensitiveSearch)
                return range.location != NSNotFound
            })
            
            
        }
        if(filtered.count == 0){
            self.tblSearch.hidden = true
        }
        else{
            self.tblSearch.hidden = false
        }
        
        uploadTable()
    }
    
    func uploadTable(){
        self.tblSearch.reloadData()
    }
    
    func imageTapped(){
        
        Global.sharedInstance.currentOpenedMenu = self
        
        let viewCon:MenuPlusViewController = storyboard?.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    
    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func dismissKeyboard() {
        tblSearch.hidden = true
        self.view.endEditing(true)
    }
    
    
    //for autoCopmlete:
    /**
     func searchAutocompleteEntriesWithSubstring
     - parameter bar: substring from textfield
         */
    func searchAutocompleteEntriesWithSubstring(substring: String)
    {
        if substring.characters.count > 0
        {
            if txtSearch.text != ""
            {
                var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dicSearch["nvKeyWord"] = substring
                
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                    tblSearch.hidden = true
                }
                else
                {
                    api.sharedInstance.SearchWordCompletion(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                        {
                            self.filterSubArr = NSArray()
                            self.filterSubArr = responseObject["Result"] as! NSArray //fix 2.3
//                            self.filterSubArr = responseObject["Result"] as! NSMutableArray
                            self.tblSearch.hidden = false
                            self.tblSearch.reloadData()
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""),vc:self)
                    })
                }
            }
            
            
        }
        
        //        else
        //        {
        //        filterSubArr = []
        //
        //        for subject in AppDelegate.arrDomainFilter
        //        {
        //            let myString:NSString! = subject.nvCategoryName as NSString
        //
        //            let substringRange :NSRange! = myString.rangeOfString(substring)
        //
        //            if (substringRange.location == 0)
        //            {
        //                filterSubArr.addObject(subject.nvCategoryName)
        //                isAuto = true
        //            }
        //        }
        //        }
        //                if !isAuto
        //                {
        //                    tblSearch.hidden = true
        //                }
        self.tblSearch.reloadData()
        
    }
    
    ////for the autoComplete
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {        //בשביל ה-autoComplete
        tblSearch.hidden = false
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        
        startString += string
        if Reachability.isConnectedToNetwork() == false && string != ""//כדי שהטקסט יכיל גם את האות האחרונה שהקליד
        {
            textField.text = startString
        }
        let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        if substring == ""
        {
           tblSearch.hidden = true
        }
        else
        {
            if startString.characters.count > 120
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY120_CHARACTERS", comment: ""),vc: self)
                return false
            }
            tblSearch.hidden = false

        searchAutocompleteEntriesWithSubstring(substring)
        }
        return true
    }
    func btnSearchAction(sender: UIButton)
    {
        Global.sharedInstance.isProvider = false
        let viewCon:AdvantageSearchViewController = clientStoryBoard!.instantiateViewControllerWithIdentifier("AdvantageSearchViewController") as! AdvantageSearchViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        viewCon.delegateSearch = self
        self.presentViewController(viewCon, animated: true, completion: nil)
    }
    
    func openSearchResults()
    {
        let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        let vc = clientStoryBoard?.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
        frontviewcontroller?.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController

        
        
        
//        let viewCon = clientStoryBoard!.instantiateViewControllerWithIdentifier("SearchResults") as! SearchResultsViewController
//        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
//        self.navigationController?.pushViewController(viewCon, animated: false)
    }

    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
    //שהלחיצה ב-didSelect של טבלת חיפוש התחומים תהיה יותר בקלות
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(tblSearch)) {
            
            return false
        }
        return true
    }
    
}
