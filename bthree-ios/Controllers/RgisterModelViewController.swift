//
//  RgisterModelViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 2/17/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol viewErrorForFirstSectionDelegate{
    func viewErrorForFirstSection()
}
protocol openRegisterDelegate {
    func openRegisterView()
}
//מודל רישום ספק
class RgisterModelViewController: UIViewController,presentPaymentByCardDelegate,dismissPayByCardDelegate,openChooseUserDelegate,openCustomerDetailsDelegate,openRegisterDelegate,UIGestureRecognizerDelegate
{
    
    //MARK: - Properties
    
    var subView = UIView()
    var labelPositionisLeft = true
    var isWeb = true
    var lastPageRegistered:Int = 0
    var generic:Generic = Generic()
    
    var viewCon:RegisterPaymentPage7ViewController = RegisterPaymentPage7ViewController()
    
    //מערך שמכיל עבור כל מסך בהרשמה מילון ששומר את התצוגה של המסך ואת הגודל שלו
    var viewsArray:Array<Array<AnyObject>> = []
    var delgateNotif:closeInTableDelegate!=nil
    
    var titles:Array<String> = [NSLocalizedString("BUSINESS_DETAILS", comment: ""),NSLocalizedString("GLOBAL_DATA", comment: ""),NSLocalizedString("NOTIFICATIONS", comment: ""),NSLocalizedString("BUSINESS_PROPHIL", comment: ""),NSLocalizedString("SYNC_WITH_CONTACTS", comment: ""),NSLocalizedString("PAYMENT", comment: "")]
    
    var view1 : BusinessDetailsViewController!
    var view2 : GlobalDataViewController!
    var view3 : NotificationsViewController!
    var view4 : RegisterBuisnessProphielViewController!
    var view5 : ContactSyncViewController!
    var view6 : PaymethodViewController!
    var view7 : BusinessDetailsViewController!
    var arrayLabel:Array<UILabel> = []
    var arrayImages:Array<UIImageView> = []
    var arrayViews:Array<UIView> = []
    
    var delegateCloseLast:closeLastCellSelegate!=nil
    var delegate:saveInGlobalDelegate!=nil
    var delegateFirstSection:viewErrorForFirstSectionDelegate!=nil
    var delegateSecond1Section:validSection1Delegate!=nil
    var supplierStoryBoard:UIStoryboard?
    var x = 0
    var getSysAlert = false//מציין האם קבלו נתונים לדף התראות
    var getFieldsAndCatg = false//מציין האם קבלו נתונים לדף נתונים כלליים
    
    //MARK: - Outlets
    
    @IBOutlet var lbl1: UILabel!
    @IBOutlet var lbl2: UILabel!
    @IBOutlet var lbl3: UILabel!
    @IBOutlet var lbl4: UILabel!
    @IBOutlet var lbl5: UILabel!
    @IBOutlet var lbl6: UILabel!
    @IBOutlet var lbl7: UILabel!
    
    @IBOutlet weak var lblNews: UILabel!
    
    @IBOutlet weak var viewBack: UIView!
    
    @IBOutlet weak var lblBack: UILabel!
    
    @IBOutlet weak var viewStep1: UIView!
    
    @IBOutlet weak var viewStep2: UIView!
    
    @IBOutlet weak var viewStep3: UIView!
    
    @IBOutlet weak var viewStep4: UIView!
    
    @IBOutlet weak var viewStep5: UIView!
    
    @IBOutlet weak var viewStep6: UIView!
    
    @IBOutlet weak var png1: UIImageView!
    
    @IBOutlet weak var png2: UIImageView!
    
    @IBOutlet weak var png3: UIImageView!
    
    @IBOutlet weak var png4: UIImageView!
    
    @IBOutlet weak var png5: UIImageView!
    
    @IBOutlet weak var png6: UIImageView!
    
    @IBOutlet weak var png7: UIImageView!
    
    @IBOutlet var view1_2: UIView!
    @IBOutlet var view2_3: UIView!
    @IBOutlet var view3_4: UIView!
    @IBOutlet var view4_5: UIView!
    @IBOutlet var view5_6: UIView!
    @IBOutlet var view6_7: UIView!
    @IBOutlet var imgModel: UIView!
    @IBOutlet var modelModel: UIView!
    
    @IBOutlet weak var viewContainBack: UIView!
    
    @IBOutlet weak var reqFieldLbl: UILabel!
    
    @IBOutlet weak var titleModelLbl: UILabel!
    
    @IBOutlet weak var btnCon: UIButton!
    
    //press on contine button
    @IBAction func btnCon(sender: UIButton) {
        
        if AppDelegate.x < 5//במהלך הרישום
        {
            if AppDelegate.x == 0
            {
                ///save the provider details in global
                delegate.saveDataInGlobal()
                if AppDelegate.arrDomains.count == 0
                {
                    generic.showNativeActivityIndicator(self)
                    if Reachability.isConnectedToNetwork() == false
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                        generic.hideNativeActivityIndicator(self)
                    }
                    else
                    {
                        let dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        api.sharedInstance.GetFieldsAndCatg(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                            
                            self.getFieldsAndCatg = true
                            
                            let domain:Domain = Domain()
                            
                            AppDelegate.arrDomains = domain.domainToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                            
                            if AppDelegate.arrDomains.count == 0
                            {
                                Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
                                
                            }
                            else
                            {
                                var idLast = -1
                                for domain in AppDelegate.arrDomains
                                {
                                    //הבדיקה היא כדי שלא יהיו כפולים כאשר נגשנו לשדה nvFieldName
                                    //                if domain.iFieldRowId != idLast || idLast == -1
                                    //                {
                                    AppDelegate.arrDomainFilter.append(domain)
                                    // }
                                    //idLast = domain.iFieldRowId
                                }
                                self.addViewToModel()
                            }
                            self.generic.hideNativeActivityIndicator(self)
                            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                                if AppDelegate.showAlertInAppDelegate == false
                                {
                                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                                    AppDelegate.showAlertInAppDelegate = true
                                }
                        })
                    }
                }
                else
                {
                    getFieldsAndCatg = true
                }
            }
            
            if AppDelegate.x == 1//נתונים כלליים
            {
                //קריאה לפונקציה שתסגור ותשמור את הסל שהיה פתוח בלחיצה על המשך
                delegateCloseLast.closeCellFromOtherCell()
                
                Global.sharedInstance.isContinuPressed = true
                
                if Global.sharedInstance.domainBuisness != "" &&
                    Global.sharedInstance.isValidHours == true &&
                    Global.sharedInstance.isHoursSelected.contains(true) &&
                    Global.sharedInstance.generalDetails.arrObjProviderServices.count != 0 &&
                    Global.sharedInstance.fIsSaveConBussinesServicesPressed == true
                {
                    x = 0
                }
                else
                {
                    //ריענון הטבלה להצגת ההודעות שגיאה
                    Global.sharedInstance.delegateValidData.validData()
                    x = 1
                }
                if Global.sharedInstance.dicSysAlerts.count == 0
                {
                    generic.showNativeActivityIndicator(self)
                    if Reachability.isConnectedToNetwork() == false
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                        generic.hideNativeActivityIndicator(self)
                    }
                    else
                    {
                        let dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        api.sharedInstance.GetSysAlertsList(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                            
                            self.getSysAlert = true
                            
                            let sysAlert:SysAlerts = SysAlerts()
                            
                            Global.sharedInstance.arrSysAlerts = sysAlert.sysToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                            
                            if Global.sharedInstance.arrSysAlerts.count != 0
                            {
                                Global.sharedInstance.dicSysAlerts = sysAlert.sysToDic(Global.sharedInstance.arrSysAlerts)
                                Global.sharedInstance.arrayDicForTableViewInCell[0]![1] = sysAlert.SysnvAletName(8)
                                Global.sharedInstance.arrayDicForTableViewInCell[2]![1] = sysAlert.SysnvAletName(9)
                                Global.sharedInstance.arrayDicForTableViewInCell[2]![2] = sysAlert.SysnvAletName(12)
                                Global.sharedInstance.arrayDicForTableViewInCell[3]![1] = sysAlert.SysnvAletName(10)
                                Global.sharedInstance.arrayDicForTableViewInCell[3]![2] = sysAlert.SysnvAletName(12)
                                self.addViewToModel()
                                
//                                Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
//                                self.arrayImages[AppDelegate.x].hidden = true
//                                AppDelegate.x += 1
//                                
//                                if AppDelegate.x > self.lastPageRegistered
//                                {
//                                    self.lastPageRegistered += 1
//                                }
//                                
//                                if AppDelegate.x > 1{
//                                    self.reqFieldLbl.hidden = true
//                                }
//                                else{
//                                    self.reqFieldLbl.hidden = false
//                                }
//                                self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
//                                
//                                self.reqFieldLbl.textColor = UIColor.blackColor()
//                                
//                                
//                                self.titleModelLbl.text = self.titles[AppDelegate.x]
//                                self.arrayLabel[AppDelegate.x].backgroundColor = UIColor.clearColor()
//                                self.arrayLabel[AppDelegate.x].font =  UIFont (name: "OpenSansHebrew-Bold", size: 17)
//                                self.arrayLabel[AppDelegate.x].textColor = UIColor.whiteColor()
//                                for i in 0 ..< AppDelegate.x  {
//                                    self.arrayViews[i].backgroundColor = Colors.sharedInstance.color4
//                                }
//                                self.arrayImages[AppDelegate.x].hidden = false
                                
                            }
                            self.generic.hideNativeActivityIndicator(self)
                            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                                if AppDelegate.showAlertInAppDelegate == false
                                {
                                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                                    AppDelegate.showAlertInAppDelegate = true
                                }
                        })
                        
                    }
                }
                else
                {
                    self.getSysAlert = true
                }
            }
            if AppDelegate.x == 3 //פרופיל עסקי
            {
                Global.sharedInstance.isFromSearchResults = false
            }
            if AppDelegate.x == 4//סנכרון עם אנשי קשר
            {
                var phones:Array<String> = []
                for contact in Global.sharedInstance.contactList {
                    if contact.bIsSync == true
                    {
                        phones.append(contact.nvPhone)
                    }
                }
                Global.sharedInstance.dicSyncContacts["nvPhoneList"] = phones
                
                Global.sharedInstance.dicSyncContacts["iProviderId"] = Global.sharedInstance.currentProvider.iIdBuisnessDetails
            }
            if Global.sharedInstance.fIsValidDetails == false{
                return
            }
            
            if AppDelegate.x == 1
            {
                if Global.sharedInstance.domainBuisness == ""
                {
                    x = 1
                    delegateFirstSection.viewErrorForFirstSection()
                }
                if  x == 1
                {
                    return
                }
            }
            
            if isWeb == true
            {
                if (getSysAlert == true && AppDelegate.x == 1) || (getFieldsAndCatg == true && AppDelegate.x == 0) || (AppDelegate.x != 0 && AppDelegate.x != 1)
                {
                    addViewToModel()
//                    Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
//                    arrayImages[AppDelegate.x].hidden = true
//                    AppDelegate.x += 1
//                    
//                    if AppDelegate.x > lastPageRegistered
//                    {
//                        lastPageRegistered += 1
//                    }
//                    
//                    if AppDelegate.x == 4{
//                        Global.sharedInstance.isClickCon = true
//                    }
//                    if AppDelegate.x > 1{
//                        reqFieldLbl.hidden = true
//                    }
//                    else{
//                        reqFieldLbl.hidden = false
//                    }
//                    self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
//                    
//                    if AppDelegate.x == 4 || AppDelegate.x == 5{
//                        reqFieldLbl.textColor = UIColor.clearColor()
//                    }
//                    else{
//                        reqFieldLbl.textColor = UIColor.blackColor()
//                    }
//                    
//                    titleModelLbl.text = titles[AppDelegate.x]
//                    arrayLabel[AppDelegate.x].backgroundColor = UIColor.clearColor()
//                    arrayLabel[AppDelegate.x].font =  UIFont (name: "OpenSansHebrew-Bold", size: 17)
//                    arrayLabel[AppDelegate.x].textColor = UIColor.whiteColor()
//                    for i in 0 ..< AppDelegate.x  {
//                        arrayViews[i].backgroundColor = Colors.sharedInstance.color4
//                    }
//                    arrayImages[AppDelegate.x].hidden = false
                }
            }
        }
            
        else//סיום הרישום
        {
            //קבלת פרטי הלקוח
            var dicGetCustomerDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            
            dicGetCustomerDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
            
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.GetCustomerDetails(dicGetCustomerDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject> {
                        
                        var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        
                        dicForDefault["nvClientName"] = responseObject["Result"]!["nvFirstName"]
                        //שמירת שם הלקוח במכשיר
                        Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "currentClintName")
                        
                        var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        
                        dicUserId["currentUserId"] = responseObject["Result"]!["iUserId"]
                        //שמירת הuserId- במכשיר
                        Global.sharedInstance.defaults.setObject(dicUserId, forKey: "currentUserId")
                        
                        Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
                        //בגלל שנשמר יום אחד קודם צריך להוסיף 3 שעות(זה לא עזר להוסיף timeZone)
                        Global.sharedInstance.currentUser.dBirthdate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dBirthdate
                            , options: [])!
                        Global.sharedInstance.currentUser.dMarriageDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 3, toDate: Global.sharedInstance.currentUser.dMarriageDate
                            , options: [])!
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        if AppDelegate.showAlertInAppDelegate == false
                        {
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                            AppDelegate.showAlertInAppDelegate = true
                        }
                })
            }
            
            //--------------save providerAllDetails in server-------------
            
            var dicAddProviderDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            //קוד לקוח
            dicAddProviderDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
            dicAddProviderDetails["objProviderBuisnessDetails"] = Global.sharedInstance.currentProvider.getDic()
            dicAddProviderDetails["objProviderGeneralDetails"] = Global.sharedInstance.generalDetails.getDic()
            dicAddProviderDetails["objProviderAlertsSettings"] = Global.sharedInstance.addProviderAlertSettings.getDic()
            dicAddProviderDetails["objProviderProfile"] = Global.sharedInstance.addProviderBuisnessProfile.getDic()
            if Global.sharedInstance.dicSyncContacts["nvPhoneList"] != nil
            {
                dicAddProviderDetails["nvPhoneList"] = Global.sharedInstance.dicSyncContacts["nvPhoneList"]
            }
            else
            {
                dicAddProviderDetails["nvPhoneList"] = []
            }
            if dicAddProviderDetails["bAutoApproval"] != nil
            {
                dicAddProviderDetails["bAutoApproval"] = Global.sharedInstance.dicSyncContacts["bAutoApproval"]
            }
            else
            {
                dicAddProviderDetails["bAutoApproval"] = true
            }
            
            generic.showNativeActivityIndicator(self)
            
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.AddProviderAllDetails(dicAddProviderDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!["ErrorCode"] as! Int == 1
                    {
                        api.sharedInstance.getProviderAllDetails(Global.sharedInstance.currentUser.iUserId)//קבלת פרטי הספק,כדי שנוכל לדעת בהמשך אם הוא רשום גם בתור ספק
                        
                        //---------------מעבר עמוד
                        
                        let frontviewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                        let vc = self.supplierStoryBoard?.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
                        frontviewcontroller?.pushViewController(vc, animated: false)
                        
                        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                        
                        let mainRevealController = SWRevealViewController()
                        
                        mainRevealController.frontViewController = frontviewcontroller
                        mainRevealController.rearViewController = rearViewController
                        
                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                        window.rootViewController = mainRevealController
                        
                        
                        Global.sharedInstance.isProvider = true
                        var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        dicForDefault["supplierRegistered"] = true
                        
                        Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "isSupplierRegistered")
                        
                        var dicSupplierName:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        dicSupplierName["nvSupplierName"] = Global.sharedInstance.currentProvider.nvSupplierName
                        
                        Global.sharedInstance.defaults.setObject(dicSupplierName, forKey: "supplierNameRegistered")
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_REGISTER", comment: ""), vc: self)
                        //מעבר ללקוח קיים
                        self.openRegisterView()
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                })
            }
            //--------------------------------------------------------
        }
    }
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var vBack: UIView!
    //press on back button
    @IBAction func btnBack(sender: UIButton) {
        goBack()
    }
    
    //go to prev page
    func goBack() {
        if AppDelegate.x != 0//לא עמוד ראשון
        {
            if AppDelegate.x == 1//נתונים כלליים
            {
                Global.sharedInstance.isFromBack = true
            }
            else if AppDelegate.x == 2//נמצאים עכשיו בדף התראות
            {
                //סגירת הטבלה הפנימית-ע״מ למנוע בעיות במיקום הטבלה
                delgateNotif.closeInTable()
            }
            
            Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
            arrayViews[AppDelegate.x - 1].backgroundColor = Colors.sharedInstance.color6
            arrayImages[AppDelegate.x].hidden = true
            AppDelegate.x -= 1
            if AppDelegate.x > 1{
                reqFieldLbl.hidden = true
            }
            else{
                reqFieldLbl.hidden = false
                
            }
            self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
            if AppDelegate.x == 4 || AppDelegate.x == 5{
                reqFieldLbl.textColor = UIColor.clearColor()
            }
            else{
                reqFieldLbl.textColor = UIColor.blackColor()
            }
            titleModelLbl.text = titles[AppDelegate.x]
            arrayImages[AppDelegate.x].hidden = false
            for i in 0 ..< AppDelegate.x  {
                arrayViews[i].backgroundColor = Colors.sharedInstance.color4
            }
            
        }
        else//העמוד הראשון של הרישום
        {
            // Create the alert controller
            let alertController = UIAlertController(title: NSLocalizedString("MESSAGE_TITLE", comment: ""), message: NSLocalizedString("EXIT_REGISTER", comment: ""), preferredStyle: .Alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertActionStyle.Default) {
                UIAlertAction in
                self.openRegisterView()
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            // Present the controller
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: - initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblBack.text = NSLocalizedString("BACK", comment: "")
        
        reqFieldLbl.text = NSLocalizedString("REQUIRED_FIELD", comment: "")
        
        Global.sharedInstance.globalData = self
        
        supplierStoryBoard = UIStoryboard(name: "SupplierExist", bundle: nil)
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        
        let backView: UIView = UIView(frame: CGRectMake(0, 0, 120, 40))
        
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "3.png"))
        titleImageView.frame = CGRectMake(0, 0, titleImageView.frame.size.width * 0.8, 40)
        backView.addSubview(titleImageView)
        
        self.navigationItem.titleView = backView
        
        Global.sharedInstance.arrayViewModel = []
        AppDelegate.x = 0
        
        btnBack.enabled = true
        viewBack.hidden = false
        lblBack.hidden = false
        //תמיכה באייפון 5 ובקטנים יותר
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            titleModelLbl.font = Colors.sharedInstance.fontSecondHeader
            lblBack.font = Colors.sharedInstance.fontText3
        }
        Global.sharedInstance.heightModel = self.view.frame.size.height
        Global.sharedInstance.rgisterModelViewController = self
        
        self.view.bringSubviewToFront(btnCon)
        titleModelLbl.text = titles[AppDelegate.x]
        arrayLabel.append(lbl1)
        arrayLabel.append(lbl2)
        arrayLabel.append(lbl3)
        arrayLabel.append(lbl4)
        arrayLabel.append(lbl5)
        arrayLabel.append(lbl6)
        arrayImages.append(png1)
        arrayImages.append(png2)
        arrayImages.append(png3)
        arrayImages.append(png4)
        arrayImages.append(png5)
        arrayImages.append(png6)
        arrayViews.append(view1_2)
        arrayViews.append(view2_3)
        arrayViews.append(view3_4)
        arrayViews.append(view4_5)
        arrayViews.append(view5_6)
        subView.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + self.navigationController!.navigationBar.frame.size.height  , view.frame.width, view.frame.height * 0.2 - 9)
        
        view1 = self.storyboard?.instantiateViewControllerWithIdentifier("BusinessDetailsViewController") as! BusinessDetailsViewController
        
        view1.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height  , view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - ( (view.frame.size.height * 0.1) + 18 ))
        
        view2 = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        view2.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height * 0.9, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - ((view.frame.size.height * 0.1) + 18 ))
        
        
        view3 = self.storyboard?.instantiateViewControllerWithIdentifier("NotificationsViewController") as! NotificationsViewController
        
        view3.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height * 0.9, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - ( (view.frame.size.height * 0.1) + 18 ))
        
        view4 = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterBuisnessProphielViewController") as! RegisterBuisnessProphielViewController
        
        view4.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view5 = self.storyboard?.instantiateViewControllerWithIdentifier("ContactSyncViewController") as! ContactSyncViewController
        
        view5.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        view6 = self.storyboard?.instantiateViewControllerWithIdentifier("PaymethodViewController") as! PaymethodViewController
        view6.delegate = self
        view6.view.frame = CGRectMake(0, subView.frame.height + self.navigationController!.navigationBar.frame.size.height, view.frame.width, view.frame.height - subView.frame.height - self.navigationController!.navigationBar.frame.size.height - 90)
        
        Global.sharedInstance.arrayViewModel = []
        
        Global.sharedInstance.arrayViewModel.append(view1)
        Global.sharedInstance.arrayViewModel.append(view2)
        Global.sharedInstance.arrayViewModel.append(view3)
        Global.sharedInstance.arrayViewModel.append(view4)
        Global.sharedInstance.arrayViewModel.append(view5)
        Global.sharedInstance.arrayViewModel.append(view6)
        
        btnCon.setTitle(NSLocalizedString("CONTINUE", comment: ""), forState: .Normal)
        
        UIView.animateWithDuration(4, delay: 2, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: [.Repeat,.TransitionFlipFromRight], animations:
            {
            }, completion: nil)
        
        labelPositionisLeft = !labelPositionisLeft
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.dismissKeyboard))
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.dismissKeyboard))
        tap1.delegate = self
        //add tap to back
        let tapBack: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goBack))
        
        
        //modelModel.addGestureRecognizer(tap)
        viewContainBack.addGestureRecognizer(tap1)
        
        //add tap to top numbers buttons to navigate pages
        let tapOnStep1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        let tapOnStep2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        let tapOnStep3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        let tapOnStep4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        let tapOnStep5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        let tapOnStep6: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RgisterModelViewController.goToStep(_:)))
        viewStep1.addGestureRecognizer(tapOnStep1)
        viewStep2.addGestureRecognizer(tapOnStep2)
        viewStep3.addGestureRecognizer(tapOnStep3)
        viewStep4.addGestureRecognizer(tapOnStep4)
        viewStep5.addGestureRecognizer(tapOnStep5)
        viewStep6.addGestureRecognizer(tapOnStep6)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Search Result Hairdresser.jpg")!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: - presentPaymentByCardDelegate
    
    //show payment page
    func presentPaymentByCard(){
        viewCon = storyboard?.instantiateViewControllerWithIdentifier("RegisterPaymentPage7ViewController") as! RegisterPaymentPage7ViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        viewCon.delegate = self
        self.presentViewController(viewCon, animated: true, completion: nil)
        
    }
    //dismiss payment page
    func dismissPayByCard()
    {
        viewCon.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func openBuisnessDetails()
    {
        let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        
        viewCon.delegateFirstSection = viewCon1
        viewCon.delegateSecond1Section = viewCon1
        
        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        front.pushViewController(viewCon, animated: false)
        
        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = front
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    //פתיחת לקוח קיים
    func openCustomerDetails()
    {
        let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        let viewCon:ModelCalenderViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as! ModelCalenderViewController
        
        let front = self.storyboard?.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        front.pushViewController(viewCon, animated: false)
        
        let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = front
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //פתיחת לקוח קיים ,במקרה שיצא מרישום ספק ע״י חזור חזור
    func openRegisterView(){
        
        Global.sharedInstance.isProvider = false
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        
        let rgister:entranceCustomerViewController = self.storyboard!.instantiateViewControllerWithIdentifier("entranceCustomerViewController") as! entranceCustomerViewController
        rgister.modalPresentationStyle = UIModalPresentationStyle.Custom
        frontViewController.pushViewController(rgister, animated: false)
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontViewController
        mainRevealController.rearViewController = rearViewController
        
        self.view.window!.rootViewController = mainRevealController
        self.view.window?.makeKeyAndVisible()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(vBack)) {
            goBack()
            return false
        }
        return true
    }
    //בלחיצה על המספרים למעלה למעבר עמוד
    func goToStep(tap:UITapGestureRecognizer)
    {
        let view:UIView = tap.view!
        //אם כבר עברו את הרישום בעמוד שאליו רוצים להגיע
        //view.tag = מספר העמוד שאליו רוצים להגיע
        if lastPageRegistered >= view.tag
        {
            //כדי לעבור לאחד מהעמודים הקודמים
            if AppDelegate.x > view.tag
            {
                if AppDelegate.x == 1
                {
                    Global.sharedInstance.isFromBack = true
                }
                else if AppDelegate.x == 2//נמצאים עכשיו בדף התראות
                {
                    //סגירת הטבלה הפנימית-ע״מ למנוע בעיות במיקום הטבלה
                    delgateNotif.closeInTable()
                }
                
                arrayImages[AppDelegate.x].hidden = true
                for var i = AppDelegate.x; i > view.tag; i -= 1
                {
                    Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
                    AppDelegate.x -= 1
                    
                    titleModelLbl.text = titles[AppDelegate.x]
                }
                arrayImages[AppDelegate.x].hidden = false
            }
                //כדי לעבור לאחד מהעמודים הבאים
            else if AppDelegate.x < view.tag
            {
                if AppDelegate.x == 0
                {
                    ///save the provider details in global
                    delegate.saveDataInGlobal()
                }
                else if AppDelegate.x == 1
                {
                    Global.sharedInstance.isContinuPressed = true
                    
                    
                    if Global.sharedInstance.domainBuisness != "" &&
                        Global.sharedInstance.isValidHours == true &&
                        Global.sharedInstance.isHoursSelected.contains(true) &&
                        Global.sharedInstance.generalDetails.arrObjProviderServices.count != 0 &&
                        Global.sharedInstance.fIsSaveConBussinesServicesPressed == true
                    {
                        x = 0
                    }
                    else
                    {
                        //ריענון הטבלה להצגת ההודעות שגיאה
                        Global.sharedInstance.delegateValidData.validData()
                        x = 1
                    }
                }
                arrayImages[AppDelegate.x].hidden = true
                for i in AppDelegate.x ..< view.tag
                {
                    Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
                    AppDelegate.x += 1
                    
                    if AppDelegate.x == 4{
                        Global.sharedInstance.isClickCon = true
                    }
                    titleModelLbl.text = titles[AppDelegate.x]
                }
                
                arrayImages[AppDelegate.x].hidden = false
            }
            
            if AppDelegate.x > 1{
                reqFieldLbl.hidden = true
            }
            else{
                reqFieldLbl.hidden = false
            }
            
            self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
            if AppDelegate.x == 4 || AppDelegate.x == 5{
                reqFieldLbl.textColor = UIColor.clearColor()
            }
            else{
                reqFieldLbl.textColor = UIColor.blackColor()
            }
        }
    }
    
    func addViewToModel()
    {
        Global.sharedInstance.arrayViewModel[AppDelegate.x].view.removeFromSuperview()
        arrayImages[AppDelegate.x].hidden = true
        AppDelegate.x += 1
        
        if AppDelegate.x > lastPageRegistered
        {
            lastPageRegistered += 1
        }
        
        if AppDelegate.x == 4{
            Global.sharedInstance.isClickCon = true
        }
        if AppDelegate.x > 1{
            reqFieldLbl.hidden = true
        }
        else{
            reqFieldLbl.hidden = false
        }
        self.view.addSubview(Global.sharedInstance.arrayViewModel[AppDelegate.x].view)
        
        if AppDelegate.x == 4 || AppDelegate.x == 5{
            reqFieldLbl.textColor = UIColor.clearColor()
        }
        else{
            reqFieldLbl.textColor = UIColor.blackColor()
        }
        
        titleModelLbl.text = titles[AppDelegate.x]
        arrayLabel[AppDelegate.x].backgroundColor = UIColor.clearColor()
        arrayLabel[AppDelegate.x].font =  UIFont (name: "OpenSansHebrew-Bold", size: 17)
        arrayLabel[AppDelegate.x].textColor = UIColor.whiteColor()
        for i in 0 ..< AppDelegate.x  {
            arrayViews[i].backgroundColor = Colors.sharedInstance.color4
        }
        arrayImages[AppDelegate.x].hidden = false
    }
}
