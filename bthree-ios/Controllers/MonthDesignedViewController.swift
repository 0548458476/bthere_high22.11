//
//  MonthDesignedViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/14/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

//לקוח תצוגת חודש ביומן 
class MonthDesignedViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate
    //,reloadTableFreeDaysDelegate
{
    //MARK: - @IBOutlet
    @IBOutlet weak var tblWorkers: UITableView!
    @IBOutlet weak var lblDayOfWeek1: UILabel!
    
    @IBOutlet weak var lblDayOfWeek2: UILabel!
    
    @IBOutlet weak var lblDayOfWeek3: UILabel!
    
    @IBOutlet weak var lblDayOfWeek4: UILabel!
    
    @IBOutlet weak var lblDayOfWeek5: UILabel!
    
    @IBOutlet weak var lblDayOfWeek6: UILabel!
    
    @IBOutlet weak var lblDayOfWeek7: UILabel!
    
    @IBOutlet var collDays: UICollectionView!

    @IBOutlet var lblHebrewDate: UILabel!
    @IBOutlet var lblCurrentDate: UILabel!
    
    @IBOutlet weak var viewSync: UIView!
    
   @IBOutlet weak var btnSync: eyeSynCheckBox!

    //MARK: - @IBAction
    @IBAction func btnSync(sender: eyeSynCheckBox) {
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            
            Global.sharedInstance.isSyncWithGoogelCalendar = true
            i = 1
            collDays.reloadData()
        }
            
        else{
            Global.sharedInstance.isSyncWithGoogelCalendar = false
            i = 1
            collDays.reloadData()
            
        }
        
    }
    //func set prev date on click prev btn
    @IBAction func btnBefore(sender: UIButton)
    {
        //currentDate =
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.removeMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        moneForBackColor = 1
        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        //today = Calendar.sharedInstance.carrentDate
        today = Mycalendar.dateByAddingUnit(.Month, value: -1, toDate: today, options: [])!
        
        changeLblDate()
       //refresh bthere event on change date
        setEventBthereInMonth()
    }
    //func set next date on click next btn
    @IBAction func btnNext(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.addMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        moneForBackColor = 1
        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        //today = Calendar.sharedInstance.carrentDate
        today = Mycalendar.dateByAddingUnit(.Month, value: 1, toDate: today, options: [])!
        
        changeLblDate()
        //refresh bthere event on change date
        setEventBthereInMonth()
    }

    
    //MARK: - varibals
    private let kKeychainItemName = "Google Calendar API"
    private let kClientID = "284147586677-69842kmfbfll1dmec57c9gklqnpa5n2u.apps.googleusercontent.com"

    let output = UITextView()

    var dateFormatter = NSDateFormatter()
    
    var today: NSDate = NSDate()

    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
     var arrEventsCurrentDay:Array<EKEvent> = []

    var days:Array<Int> = []
    var numDaysInMonth:Int = 0
    var dateFirst:NSDate = NSDate()
    var dayInWeek:Int = 0
    var i = 1
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var hasEvent = false
    var moneForBackColor = 1
    let calendar = NSCalendar.currentCalendar()
    var titles : [String] = []
    var startDates : [NSDate] = []
    var endDates : [NSDate] = []
    
    let eventStore = EKEventStore()
    var shouldShowDaysOut = true
    var animationFinished = true
    var currentDate:NSDate = NSDate()
    var bthereEventDateDayInt:Array<Int> = Array<Int>()// ימים שיש בהם אירוע
    
    //MARK: - Initials
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        today = NSDate()
        Calendar.sharedInstance.carrentDate = NSDate()
        Global.sharedInstance.changeSizeOfLabelByDevice(lblHebrewDate, size: 11)
        Global.sharedInstance.changeSizeOfLabelByDevice(lblCurrentDate, size: 20)
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true{//אם הלקוח הנוכחי בחר שהוא רוצה סנכרון עם היומן האישי אז התצוגה נפתחת כשהעין לא מסומנת ז״א יש סנכרון
            btnSync.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSync.isCecked = false
            i = 1
            collDays.reloadData()
        }
     
        Global.sharedInstance.desingMonth = self
        //Global.sharedInstance.currDateSelected = NSDate()
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[6]

        
               // Menu delegate [Required]
               // Do any additional setup after loading the view.
       numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
       dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
       dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        //getFirstDay(NSDate())
        setEventBthereInMonth()

        if language == "he"
        {
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
        }
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        changeLblDate()
        setEventsArray()
        changeLblDate()

               for var event in Global.sharedInstance.arrEvents {
            
                
                    //let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
        }
//         let calendars = eventStore.calendarsForEntityType(.Event)
//        
//        for calendar in calendars {
//           if calendar.title == "לוח שנה" {
//            
//                let oneMonthAgo = NSDate(timeIntervalSinceNow: -30*24*3600)
//                let oneMonthAfter = NSDate(timeIntervalSinceNow: +30*24*3600)
//                
//                let predicate = eventStore.predicateForEventsWithStartDate(oneMonthAgo, endDate: oneMonthAfter, calendars: [calendar])
//                
//                var events = eventStore.eventsMatchingPredicate(predicate)
//                
//                for event in events {
//                    titles.append(event.title)
//                    startDates.append(event.startDate)
//                    endDates.append(event.endDate)
//                }
//         }
//            
//        }

        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
        
    }
    //sort orders by current month
    func setEventBthereInMonth()
    {
       self.bthereEventDateDayInt = []
        var orderDetailsObj:OrderDetailsObj =  OrderDetailsObj()
        var a:Int = Int()
        var b:Int = Int()
        var c:Int = Int()
        var d:Int = Int()
        if Global.sharedInstance.ordersOfClientsArray.count > 0
        {
            for item in Global.sharedInstance.ordersOfClientsArray
            {
                orderDetailsObj = item as  OrderDetailsObj
                a = Calendar.sharedInstance.getMonth(orderDetailsObj.dtDateOrder, reduce: 0, add: 0)
                b = Calendar.sharedInstance.getMonth( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0)
                c = Calendar.sharedInstance.getYear(orderDetailsObj.dtDateOrder, reduce: 0, add: 0)
                d = Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0)
             //   check if is currentMonth
                if Calendar.sharedInstance.getMonth(orderDetailsObj.dtDateOrder, reduce: 0, add: 0) == Calendar.sharedInstance.getMonth( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0) &&  Calendar.sharedInstance.getYear(orderDetailsObj.dtDateOrder, reduce: 0, add: 0) == Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
                        , reduce: 0, add: 0)
                {
                    
                    //                    d = Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
                    //                        , reduce: 0, add: 0)
                    
                    bthereEventDateDayInt.append(Calendar.sharedInstance.getDay(orderDetailsObj.dtDateOrder, reduce: 0, add: 0))
                }
                
            }
        }
        collDays.reloadData()
    }

    // When the view appears, ensure that the Google Calendar API service is authorized
    // and perform API calls
    override func viewDidAppear(animated: Bool) {
        
        Global.sharedInstance.getEventsFromMyCalendar()
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true{//אם הלקוח הנוכחי בחר שהוא רוצה סנכרון עם היומן האישי אז התצוגה נפתחת כשהעין לא מסומנת ז״א יש סנכרון
            btnSync.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSync.isCecked = false
            i = 1
            collDays.reloadData()
        }
        
        
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        
        today = NSDate()
        Calendar.sharedInstance.carrentDate = today
        
        changeLblDate()
        setEventBthereInMonth()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
          }
    
    
    
    // MARK: Optional methods
    
//    func shouldShowWeekdaysOut() -> Bool {
//        return shouldShowDaysOut
//    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }

   //MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        if section == 0{
        //            return 1
        //        }
        if (numDaysInMonth > 30 && dayInWeek == 6
        ) || (numDaysInMonth > 29 && dayInWeek == 7){
            return 42
        }
        return 35
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        //Aligning right to left on UICollectionView
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        let cell:DayMonthCalendarCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("DayMonthCalendarCollectionViewCell",forIndexPath: indexPath) as! DayMonthCalendarCollectionViewCell
        cell.lblIsBthereEvent.hidden = true
     //        cell.lblBtEvent.hidden = true

        
//        if Global.sharedInstance.model == 2{//אם זה תחת מודל של יומן תורים מול לקוח
//        cell.delegate = Global.sharedInstance.calendarAppointment
//        }
//        else{
        cell.delegate = Global.sharedInstance.calendarClient//תחת המודל של יומן שלי ללקוח
        
        cell.whichModelOpenMe = 1
//        }
        cell.imgToday.hidden = true
        cell.lblDayDesc.alpha = 1.0
//         cell.viewIsFree.backgroundColor = UIColor.clearColor()
   //     var s =
       // var d:NSDate()
       cell.lblDayDesc.text = ""
        cell.lblDayDesc.textColor = Colors.sharedInstance.color1

        if moneForBackColor % 7 == 0{
            if Global.sharedInstance.whichReveal == false{
            cell.lblDayDesc.textColor = Colors.sharedInstance.color3

                 cell.isShabat = true

            }
            else{
                   cell.lblDayDesc.textColor = Colors.sharedInstance.color4
            }
        }

        moneForBackColor += 1
        if indexPath.row >= (dayInWeek - 1)  && indexPath.row < (numDaysInMonth + dayInWeek - 1 )
        {
            if bthereEventDateDayInt.contains(i)
            {
                cell.lblIsBthereEvent.hidden = false
                //             cell.backgroundColor = UIColor.redColor()
                //             cell.lblBtEvent.hidden = false
            }



            cell.setDisplayData(i)
          
         
            i += 1
        }
        else
        {
            cell.setNull()
            cell.btnEnterToDay.enabled = false
       cell.viewIsFree.backgroundColor =  UIColor.clearColor()
        }
      
//        cell.delegate = self
//        cell.btnCheck.tag = indexPath.row
        if language == "he"
        {
            cell.transform = scalingTransform
        }
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        componentsCurrent.day = i
        let date:NSDate = Calendar.sharedInstance.from(componentsCurrent.year , month: componentsCurrent.month, day: i)
       // self.ifHasEventInDayFunc(date)
//        if  self.ifHasEventInDayFunc(date)
//        {
//            cell.btnEnterToDay.backgroundColor = UIColor.greenColor()
//        }
        return cell
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if (numDaysInMonth > 30 && dayInWeek == 6) || (numDaysInMonth > 29 && dayInWeek == 7)
        {
            return CGSize(width: view.frame.size.width / 7.6, height:  view.frame.size.width / 7.6)
        }
        return CGSize(width: view.frame.size.width / 7, height:  view.frame.size.width / 7)
    }
    
    //MARK: - functions
    //bool func that  get date and check if has event in this date
    func ifHasEventInDayFunc( dt:NSDate)-> Bool
    {
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: dt)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                //                arrEventsCurrentDay.append(event)
                //                hasEvent = true
                return true
            }
        }
        return false
    }
    //change the text on lblDate on prev or next click
    func changeLblDate(){
        
//        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
//            let fontSize:CGFloat = self.lblCurrentDate.font.pointSize;
//            lblCurrentDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 20)
//            lblHebrewDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 11)
//            
//        }

        var str:String = ""
        var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
        var index:Int = 0
//          if s1.count > 0
//        {
        let characters = s1[1].characters.map { String($0) }
            
        if characters[0] == String(0){
            str = NSDateFormatter().monthSymbols[Int(characters[1])! - 1]
                //monthArray[Int(characters[1])!]
        }
        else{
            str = NSDateFormatter().monthSymbols[Int(s1[1])! - 1]
                //monthArray[Int(s1[1])!]
            
        }
     //   }
        lblCurrentDate.text = str + " " + s1[2]
        let hebrew: NSLocale?
        if language == "he"
        {
        // Hebrew, Israel
        hebrew = NSLocale(localeIdentifier: "he_IL")
        }
        else
        {
        hebrew = NSLocale(localeIdentifier: "en_IL")
        }
        
//NSHebrewCalendar
        //NSHebrewCalendar
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.locale = hebrew
        dateFormat.calendar = Mycalendar
        dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
        let dateString: String = dateFormat.stringFromDate(today)
        lblHebrewDate.text = dateString
    }
    
    //sort device event by current Month
    func setEventsArray()
    {
        arrEventsCurrentDay = []
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday 
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
        //reloadTableFreeDaysDelegate

//    func reloadTableFreeDays()  {
//        i = 1
//        collDays.reloadData()
//    }

    
//    // Construct a query and get a list of upcoming events from the user calendar
//    func fetchEvents() {
//        let query = GTLQueryCalendar.queryForEventsListWithCalendarId("primary")
//        query.maxResults = 10
//        query.timeMin = GTLDateTime(date: NSDate(), timeZone: NSTimeZone.localTimeZone())
//        query.singleEvents = true
//        query.orderBy = kGTLCalendarOrderByStartTime
//        service.executeQuery(
//            query,
//            delegate: self,
//            didFinishSelector: "displayResultWithTicket:finishedWithObject:error:"
//        )
//    }
//    
//    // Display the start dates and event summaries in the UITextView
//    func displayResultWithTicket(
//        ticket: GTLServiceTicket,
//        finishedWithObject response : GTLCalendarEvents,
//        error : NSError?) {
//            
//            if let error = error {
//                showAlert("Error", message: error.localizedDescription)
//                return
//            }
//            
//            var eventString = ""
//            
//            if let events = response.items() where !events.isEmpty {
//                for event in events as! [GTLCalendarEvent] {
//                    var start : GTLDateTime! = event.start.dateTime ?? event.start.date
//                    var startString = NSDateFormatter.localizedStringFromDate(
//                        start.date,
//                        dateStyle: .ShortStyle,
//                        timeStyle: .ShortStyle
//                    )
//                    eventString += "\(startString) - \(event.summary)\n"
//                }
//            } else {
//                eventString = "No upcoming events found."
//            }
//            
//            output.text = eventString
//    }
//    
//    
//    // Creates the auth controller for authorizing access to Google Calendar API
//    private func createAuthController() -> GTMOAuth2ViewControllerTouch {
//        let scopeString = " ".join(scopes)
//        return GTMOAuth2ViewControllerTouch(
//            scope: scopeString,
//            clientID: kClientID,
//            clientSecret: nil,
//            keychainItemName: kKeychainItemName,
//            delegate: self,
//            finishedSelector: "viewController:finishedWithAuth:error:"
//        )
//    }
//    
//    // Handle completion of the authorization process, and update the Google Calendar API
//    // with the new credentials.
//    func viewController(vc : UIViewController,
//        finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
//            
//            if let error = error {
//                service.authorizer = nil
//                showAlert("Authentication Error", message: error.localizedDescription)
//                return
//            }
//            
//            service.authorizer = authResult
//            dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    // Helper for showing an alert
//    func showAlert(title : String, message: String) {
//        let alert = UIAlertView(
//            title: title,
//            message: message,
//            delegate: nil,
//            cancelButtonTitle: "OK"
//        )
//        alert.show()
//    }
    
    func showSync()
    {
        if btnSync.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSync.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendar = true// פלאג כללי ליומן לקוח מול ספק שמסמל האם הלקוח בחר סנכרון או לא
            i = 1
            collDays.reloadData()
        }
            
        else
        {
            btnSync.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendar = false
            i = 1
            collDays.reloadData()
            
        }
        
    }
   
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


