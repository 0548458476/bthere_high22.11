//
//  RegisterViewController.swift
//  bthree-ios
//
//  Created by Tami wexelbom on 2/16/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import AddressBook
import MediaPlayer
import AssetsLibrary
import CoreLocation
import CoreMotion
import Foundation
import GoogleMaps

protocol popUpPhoneDelegate {
    func deleteTxtPhone()
}

protocol didReadRegulationDelegte {
    func didReadRegulation()
}

protocol addImage{
    func ImagesCamera()
}

protocol openChooseUserDelegate{
    func openBuisnessDetails()
}

protocol openCustomerDetailsDelegate
{
    func openCustomerDetails()
}

//רישום לקוח
class RegisterViewController: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,addImage,FBSDKLoginButtonDelegate,GPPSignInDelegate,openChooseUserDelegate,openCustomerDetailsDelegate,didReadRegulationDelegte,WDImagePickerDelegate,popUpPhoneDelegate{
    
    
    //MARK: - Properties
    
    var dateServer = NSDate()
    
    var generic:Generic = Generic()
    var isLoginInFaceBook = false
    
    ///flags for textField to check validation-which textField filled before
    var fDidBegin = false
    var flag_FullName:Bool = false
    var flag_LastName:Bool = false
    var flag_Phone:Bool = false
    var flag_Email:Bool = false
    var flag_Date:Bool = false
    var isOnlyFirstName:Bool = false
    var image:UIImage = UIImage()
    var keyboardIsShowing = false
    private var popoverController: UIPopoverController!
    var isCheckFromClickRegister:Bool = false//האם הגיע לבדיקת השלמת התקנון רק כשלוחצים על הרשם
    ///משתנה המציין אם הכוכבית במספר טלפון נמצא רק בתחילת המספר
    var cochOnlyInBegin = true
    var arrGoodPhone:Array<Character> = Array<Character>()
    private var iimagePicker: WDImagePicker!
    //מציין האם נגמרו הבדיקות תקינות
    var isCheckEmailEnd:Bool = false
    var isCheckPhoneEnd:Bool = false
    var isCheckNameEnd:Bool = false
    var isCheckLastNameEnd:Bool = false
    
    private var imagePickerWD: WDImagePicker!
    var timer: NSTimer? = nil
    
    var dic2ForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
    
    ///for underline of buttons
    var attrs = [
        NSFontAttributeName : UIFont(name: "OpenSansHebrew-Bold", size: 12)!,
        NSForegroundColorAttributeName : Colors.sharedInstance.color3,
        NSUnderlineStyleAttributeName : 1]
    var attributedStringsUserExist = NSMutableAttributedString(string:"")
    var attributedStringsLanguage = NSMutableAttributedString(string:"")
    let kClientId:NSString = "684155944655-dc4qpsliein6tk7al78m1gfo0b1blkfc.apps.googleusercontent.com"
    var imagePicker: UIImagePickerController!
    var arr:Array<String> = Array<String>()
    let line:UILabel = UILabel()
    
    var cell2:PersonalDetailsTableViewCell?
    var indexPathDate:NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblRegisterBusiness: UILabel!
    @IBOutlet weak var viewRegisterProvider: UIView!
    @IBOutlet weak var imageBig: UIImageView!
    @IBOutlet weak var viewBigImage: UIView!
    @IBOutlet weak var viewOpenCamera: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var btnDelImage: UIButton!
    
    @IBAction func btnDelImage(sender: AnyObject) {
        //txtAddress.dismissSuggestionTableView()
        img.image = nil
        btnDelImage.hidden = true
        setImageBig(viewBigImage, hidden: true)
    }
    @IBOutlet weak var btnGetP: checkBox2!
    @IBOutlet weak var lblPaymentsDetails: UILabel!
    @IBOutlet weak var lblFillDetails: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblSync: UILabel!
    @IBOutlet weak var lblGetP: UILabel!
    
    @IBOutlet weak var sQuickRegiser: UILabel!
    
    @IBOutlet weak var lblDateBorn: UILabel!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var sFillNextDetails: UILabel!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var reqFieldError: UILabel!
    @IBOutlet weak var dp: UIDatePicker!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var validPhone: UILabel!
    @IBOutlet weak var reqField: UILabel!
    @IBOutlet weak var scrRegister: UIScrollView!
    @IBOutlet var imgRegister: UIView!
    @IBOutlet var lblLoginTitle: UILabel!
    
    @IBOutlet weak var btnLoginButton: FBSDKLoginButton!
    @IBOutlet weak var btn_googlePlus: UIButton!
    
    @IBOutlet weak var validEmal: UILabel!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viLastName: UIView!
    @IBOutlet weak var viFirstName: UIView!
    
    
    @IBOutlet weak var uploadImageLbl: UILabel!
    
    @IBOutlet weak var paymentsImformationlbl: UILabel!
    @IBOutlet weak var viPhone: UIView!
    
    @IBOutlet weak var viDateBorn: UIView!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblImageUrl: UILabel!
    
    @IBOutlet weak var viImage: UIView!
    @IBOutlet weak var viEmail: UIView!
    
    //when click on GooglePlush button
    @IBAction func btnGooglePlus(sender: AnyObject)
    {
        setImageBig(viewBigImage, hidden: true)
        Alert.sharedInstance.showAlertWith2Buttons(self,whichChoose: 1)
    }
    
    @IBOutlet weak var tblDetails: UITableView!
    
    @IBOutlet weak var lblRead: UILabel!
    
    @IBOutlet weak var lblGetAdverties: UILabel!
    
    @IBOutlet weak var btnGetAdverties: CheckBox!//type of class that defines cheakbox
    
    @IBOutlet weak var btnRead: CheckBox!
    
    @IBOutlet weak var lblLastName: UILabel!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var validLName: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var txtAddress: SuggestiveTextField!
    
    @IBOutlet weak var validAddress: UILabel!
    
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBAction func btnRegister(sender: AnyObject) {
        setImageBig(viewBigImage, hidden: true)
        //txtAddress.dismissSuggestionTableView()
        isCheckFromClickRegister = true
        ///-----בדיקה שהשדות תקינים-------------------
        Global.sharedInstance.isRegisterClientClick = true
        Global.sharedInstance.isRegisterProviderClick = false
        
        isCheckEmailEnd = false
        isCheckPhoneEnd = false
        isCheckNameEnd = false
        if fullName.text != ""
        {
            validationName()
        }
        else if txtPhone.text != ""
        {
            self.validationPhone()
        }
        else if txtEmail.text != ""
        {
            self.validationEmail()
        }
        else
        {
            self.validToRegister()
        }
    }
    
    @IBOutlet weak var btnSync: checkBox2!
    @IBAction func btnLogin(sender: UIButton) {
        ///-----בדיקה שהשדות תקינים-------------------
        Global.sharedInstance.isRegisterClientClick = true
        
        isCheckEmailEnd = false
        isCheckPhoneEnd = false
        isCheckNameEnd = false
        
        validationName()
    }
    @IBOutlet weak var openCamera: UIButton!
    
    @IBAction func btnCamera(sender: UIButton) {
        setImageBig(viewBigImage, hidden: true)
        Global.sharedInstance.isCamera = true
        ImagesCamera()
    }
    @IBOutlet weak var fullName: UITextField!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var dateBorn: UITextField!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var btnRegulations: UIButton!
    
    @IBAction func btnOpenPayments(sender: UIButton) {
        setImageBig(viewBigImage, hidden: true)
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentMethodViewController") as! PaymentMethodViewController
        vc.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBOutlet weak var btnOpenPayments: UIButton!
    
    /// open the regulations pages
    @IBAction func btnRegulations(sender: AnyObject) {
        setImageBig(viewBigImage, hidden: true)
        let vcRegulations = self.storyboard?.instantiateViewControllerWithIdentifier("RegulationsViewController") as! RegulationsViewController
        vcRegulations.delegate = self
        
        vcRegulations.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(vcRegulations, animated: true, completion: nil)
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !Global.sharedInstance.rtl
        {
            dateBorn.textAlignment = .Left
        }
        
        dateBorn.addTarget(self, action: #selector(RegisterViewController.myTargetFunction(_:)), forControlEvents: UIControlEvents.TouchDown)
        
        Global.sharedInstance.registerViewCon = self
        Global.sharedInstance.viewConNoInternet = self
        
        ///////////////////////////למחוק!!!!
//        txtLastName.text = "דודי"
//        fullName.text = "aa"
//        txtPhone.text = "050"
//        txtEmail.text = "bc@de."
//
//        self.btnRead.isCecked = true
     ///////////////////////////////////למחוק!!!!
      
        btnDelImage.hidden = true
        self.viImage.bringSubviewToFront(openCamera)
        
        let tapProvider:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openProvider))
        viewRegisterProvider.addGestureRecognizer(tapProvider)
        
        let tapCameara:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCamera))
        viewOpenCamera.userInteractionEnabled = true
        viewOpenCamera.addGestureRecognizer(tapCameara)
        let tapOnImage:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showImage))
        img.userInteractionEnabled = true
        img.addGestureRecognizer(tapOnImage)
        
        
        let tapOnDp:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        dp.userInteractionEnabled = true
        dp.addGestureRecognizer(tapOnDp)
        
        btnSync.isCecked = true
        fullName.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("FIRSTNAME_LBL", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        dateBorn.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("DATEBURN_LBL", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtPhone.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("PHONE", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtEmail
            .attributedPlaceholder = NSAttributedString(string:NSLocalizedString("EMAIL", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
       txtLastName.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("L_NAME", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        
        let backView: UIView = UIView(frame: CGRectMake(0, 0, 120, 40))
        
        let titleImageView: UIImageView = UIImageView(image: UIImage(named: "3.png"))
        titleImageView.frame = CGRectMake(0, 0, titleImageView.frame.size.width * 0.8, 40)
        // Here I am passing origin as (45,5) but can pass them as your requirement.
        backView.addSubview(titleImageView)
        
        self.navigationItem.titleView = backView
        
        btnSync.isCecked = true
        scrRegister.scrollEnabled = false
        fullName.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
        dateBorn.delegate = self
        if DeviceType.IS_IPHONE_6{
            lblRead.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            attrs = [ NSFontAttributeName : UIFont(name: "OpenSansHebrew-Bold", size: 14)!,
                      NSForegroundColorAttributeName : Colors.sharedInstance.color3,
                      NSUnderlineStyleAttributeName : 1]
            btnRegulations.setAttributedTitle(attributedStringsUserExist, forState: .Normal)
            lblGetP.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblSync.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblMessage.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            
        }
        else
        {
            if DeviceType.IS_IPHONE_6P
            {
                lblRead.font = Colors.sharedInstance.fontText3
                attrs = [ NSFontAttributeName : UIFont(name: "OpenSansHebrew-Bold", size: 17)!,
                          NSForegroundColorAttributeName : Colors.sharedInstance.color3,
                          NSUnderlineStyleAttributeName : 1]
                btnRegulations.setAttributedTitle(attributedStringsUserExist, forState: .Normal)
                lblSync.font = Colors.sharedInstance.fontText3
            }
            else
            {
                
                if DeviceType.IS_IPHONE_5{
                    
                    sQuickRegiser.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    lblRead.font = Colors.sharedInstance.fontText4
                    lblGetP.font = Colors.sharedInstance.fontText4
                    lblSync.font = Colors.sharedInstance.fontText4
                    lblFillDetails.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    reqFieldError.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
                    
                    fullName.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    email.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    txtPhone.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    dateBorn.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
                    lblMessage.font = UIFont(name: "OpenSansHebrew-Light", size: 13)
                }
                else
                {
                    reqFieldError.font = Colors.sharedInstance.fontText4
                    
                }
                
            }
        }
        lblSync.text = NSLocalizedString("SYNC_CALENDAR", comment: "")
        btnRegister.setTitle(NSLocalizedString("REGISTER_BTN", comment: ""), forState: .Normal)
        reqFieldError.text = NSLocalizedString("REQUIREFIELD", comment: "")
        sQuickRegiser.text = NSLocalizedString("QUICKREGISTERWITH", comment: "")
        sFillNextDetails.text = NSLocalizedString("ORFILLNEXTDETAILS", comment: "")
        
        reqField.text = NSLocalizedString("REQUIREFIELD", comment: "")
        lblMessage.text = NSLocalizedString("ADD_ASS_BUSINESS", comment: "")
        lblRead.text = NSLocalizedString("READCONDITIONOK", comment: "")
        dp.backgroundColor = Colors.sharedInstance.color1
        dp.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dp.setValue(0.8, forKeyPath: "alpha")
        dp.datePickerMode = UIDatePickerMode.Date
        dp.setValue(false, forKey: "highlightsToday")
        dp.addTarget(self, action: #selector(RegisterViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        validEmal.hidden = true
        validPhone.hidden = true
        reqField.hidden = true
        validLName.hidden = true
        
        fullName.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        txtEmail.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        txtPhone.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        dateBorn.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        img.userInteractionEnabled = true
        
        dp.hidden = true
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -80
        let minDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        
        components.year = -0
        let maxDate: NSDate = gregorian.dateByAddingComponents(components, toDate: currentDate, options: NSCalendarOptions(rawValue: 0))!
        
        self.dp.minimumDate = minDate
        self.dp.maximumDate = maxDate
        
        lblRead.layer.cornerRadius = 10
        view.bringSubviewToFront(btnRegulations)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        self.imgRegister.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        //init properties to connect to google+
      
        Global.sharedInstance.googleSignIn = GPPSignIn.sharedInstance()
        Global.sharedInstance.googleSignIn.clientID = "428242712697-jblmgroafen9ln2nh7v639dn16i3vdqr.apps.googleusercontent.com"
        //before the new key in 31-10-16
        //Global.sharedInstance.googleSignIn.clientID = "684155944655-dc4qpsliein6tk7al78m1gfo0b1blkfc.apps.googleusercontent.com"
        Global.sharedInstance.googleSignIn.shouldFetchGoogleUserEmail = true
        Global.sharedInstance.googleSignIn.shouldFetchGoogleUserID = true
        Global.sharedInstance.googleSignIn.shouldFetchGooglePlusUser = true

        Global.sharedInstance.googleSignIn.scopes = [kGTLAuthScopePlusLogin]
        Global.sharedInstance.googleSignIn.scopes = ["profile"]
        Global.sharedInstance.googleSignIn.delegate = self
        
        view.bringSubviewToFront(btn_googlePlus)
        
        lblRead.layer.cornerRadius = 10
        view.bringSubviewToFront(btnRegulations)
        
        arr = [NSLocalizedString("FIRST_NAME", comment: ""),NSLocalizedString("LAST_NAME", comment: ""),NSLocalizedString("PHONE_NUMBER", comment: ""),NSLocalizedString("MAIL", comment: ""),NSLocalizedString("DATEBURN_LBL", comment: ""),NSLocalizedString("IMAGEUPLOAD_LBL", comment: "")]
        
        if (FBSDKAccessToken.currentAccessToken() == nil)
        {
            print("Not logged in..")
        }
        else
        {
            print("Logged in..")
        }
        btnLoginButton.readPermissions = ["public_profile", "email", "user_friends"]
        btnLoginButton.frame = CGRectMake(30, 25, 80, 30)
        btnLoginButton.delegate = self
        
        btnLoginButton.backgroundColor = UIColor.clearColor()
        btnLoginButton.setImage(nil, forState: UIControlState.Normal)
        btnLoginButton.setTitleColor(UIColor.clearColor(), forState: .Normal)
        
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Selected)
        
        lblGetP.text = NSLocalizedString("AGREE_GET_ADVERTISEMENTS", comment: "")
        btnRegulations.setTitle(NSLocalizedString("REGULATION", comment: ""), forState: .Normal)
        
        lblRegisterBusiness.text = NSLocalizedString("REGISTER_BTN", comment: "")
        
        ///for underline of buttons
        ///----------------------------------------------
        let buttonTitleStr = NSMutableAttributedString(string:NSLocalizedString("REGULATION", comment: ""), attributes:attrs)
        attributedStringsUserExist.appendAttributedString(buttonTitleStr)
        btnRegulations.setAttributedTitle(attributedStringsUserExist, forState: .Normal)
        setImageBig(viewBigImage, hidden: true)
    }
    
    override func viewDidAppear(animated: Bool) {
        setImageBig(viewBigImage, hidden: true)
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicForDefault["supplierRegistered"] = false
        
        Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "isSupplierRegistered")
        
        var dic1ForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic1ForDefault["nvSupplierName"] = ""
        Global.sharedInstance.defaults.setObject(dic1ForDefault, forKey: "supplierNameRegistered")
        
        dic2ForDefault["nvClientName"] = ""
        Global.sharedInstance.defaults.setObject(dic2ForDefault, forKey: "currentClintName")
        
        
        Colors.sharedInstance.addTopAndBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viFirstName)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewLastName)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viPhone)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viEmail)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viDateBorn)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viImage)
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
    }
    
    override func viewDidLayoutSubviews() {
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P{
            scrRegister.contentSize.height = 700
        }
        else
        {
            if DeviceType.IS_IPHONE_5{
                scrRegister.contentSize.height = 600
            }
            else{
                scrRegister.contentSize.height = 550
                
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Camera
    
    /// ImagesCamera() - this function open images file
    /// or camera to get an image.
    func ImagesCamera()
    {
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.ActionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title: NSLocalizedString("OPEN_CAMERA", comment: ""), style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }))
        /// to open images file
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("OPEN_ALBUM", comment: ""), style:UIAlertActionStyle.Default, handler:{ (UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                self.showResizablePicker(self.viImage)
                
            }
            }
            
            ))
        settingsActionSheet.addAction(UIAlertAction(title:NSLocalizedString("CANCEL", comment: ""), style:UIAlertActionStyle.Cancel, handler:nil))
        self.presentViewController(settingsActionSheet, animated:true, completion:nil)
        
    }
    
    func showCamera()
    {
        setImageBig(viewBigImage, hidden: true)
        Global.sharedInstance.isCamera = true
        ImagesCamera()
    }
    
    
    // MARK: - login in facebook
    //====================login in facebook=======================
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        setImageBig(viewBigImage, hidden: true)
        if error == nil
        {
            
            print("Login complete.")
            
            if(FBSDKAccessToken.currentAccessToken() != nil) {
                returnUserData()
            }
        }
        else
        {
            print(error.localizedDescription)
        }
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Selected)
        
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
    {btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        print("User logged out...")
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Selected)
        
    }
    
    func returnUserData()
    {
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        isLoginInFaceBook = true
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,id,name, first_name, last_name,birthday"])
        //["fields":"email,name,birthday"])
        
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            print("fetched user: \(result)")
            
            let userName : NSString = result.valueForKey("name") as! NSString
            
            if let a = result.valueForKey("birthday"){
                let birthday : NSString = result.valueForKey("birthday") as! NSString
            }
            
            
            print("User Name is: \(userName)")
            
            let userEmail : NSString = result.valueForKey("email") as! NSString
            
            let firstName: NSString = result.valueForKey("first_name") as! NSString
            let lastName: NSString = result.valueForKey("last_name") as! NSString
            
            
            print("User Email is: \(userEmail)")
            let facebookID:NSString = result.valueForKey("id") as! NSString
            let pictureURL = "https://graph.facebook.com/\(facebookID)/picture?type=large&return_ssl_resources=1"
            self.fullName.text = firstName as String
            self.txtLastName.text = lastName as String
            self.email.text = userEmail as String
            self.reqField.hidden = true
            self.validPhone.hidden = true
            self.validEmal.hidden = true
            
            let url = NSURL(string:pictureURL)
            let data=NSData(contentsOfURL: url!)
            let image=UIImage(data: data!)
            
        })
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        btnLoginButton.setBackgroundImage(UIImage(named: "40.png"), forState: .Selected)
        
    }
    
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        dateServer = sender.date
        dateBorn.text = dateFormatter.stringFromDate(sender.date)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        cell2 = tblDetails.cellForRowAtIndexPath(indexPathDate) as? PersonalDetailsTableViewCell
        
        cell2!.txtfDetails.text = dateFormatter.stringFromDate(sender.date)
        
    }
    // MARK: - KeyBoard
    
    //=======================KeyBoard================
    
    
    func dismissKeyboard() {
        setImageBig(viewBigImage, hidden: true)
        dp.hidden = true
        //tableView.hidden = true
        view.endEditing(true)
    }
    
    // MARK: - TextField
    //=========================TextField==============
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch(textField){
        case(fullName):
            txtLastName.becomeFirstResponder()
        case(txtLastName):
            txtPhone.becomeFirstResponder()
        case(txtPhone):
            txtEmail.becomeFirstResponder()
        case(txtEmail):
            dismissKeyboard()
        default:
            fullName.becomeFirstResponder()
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        setImageBig(viewBigImage, hidden: true)
        
        if textField == dateBorn
        {
            textField.inputView = UIView()
            dp.hidden = false
        }
        else if textField == fullName
        {
            self.reqField.hidden = true
        }
        else if textField == txtLastName
        {
            self.validLName.hidden = true
        }
        else if textField == txtPhone
        {
            self.validPhone.hidden = true
        }
        else
        {
            dp.hidden = true
        }
        
        checkValidation(textField)
    }
    
    
    func myTargetFunction(textField: UITextField) {
        // user touch field
        if textField == dateBorn
        {
            if dp.hidden == true
            {
                textField.inputView = UIView()
                dp.hidden = false
            }
            else
            {
                dp.hidden = true
            }
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        fDidBegin = false
        timer?.invalidate()
        timer = nil
        if true {
            
            timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.doDelayed), userInfo: string, repeats: false)
            
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            
            
            if textField == txtPhone
            {
                if startString.characters.count > 10
                {    txtPhone.resignFirstResponder()
                    Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_TEN_DIGITS", comment: ""), vc: self)
                    
                    //                dismissKeyboard()
                    return false
                }
                else
                {
                    return true
                }
            }
            
            
            
            return true
        }
        
        
        
        
    }
    
    func doDelayed(t: NSTimer) {
        if fDidBegin == true
        {
            fDidBegin = false
        }
        else
        {
            
            dismissKeyboard()
        }
        timer = nil
    }
    
    func getHints(timer: NSTimer) {
        print("Hints for textField: \(timer.userInfo!)")
    }
    
    //    func textInputMode() -> UITextInputMode {
    //        if isEmailField {
    //            for inputMode: UITextInputMode in UITextInputMode.activeInputModes() {
    //                if (inputMode.primaryLanguage == "en-US") {
    //                    return inputMode
    //                }
    //            }
    //        }
    //        return super.textInputMode()
    //    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        
        fDidBegin = true
        dismissKeyboard()
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        switch(textField){
        case fullName:
            
            //lblFullName.textColor = UIColor.blackColor()
            dp.hidden = true
            
        case txtLastName:
            
            //lblLastName.textColor = UIColor.blackColor()
            dp.hidden = true
            
            //        case txtAddress:
            //
            //            //            if txtAddress.text == ""
            //            //            {
            //            //                  }
            //            //lblAddress.textColor = UIColor.blackColor()
            //            dp.hidden = true
            ////            }
            
            
        case txtPhone:
            validPhone.hidden = true
            //lblPhone.textColor = UIColor.blackColor()
            dp.hidden = true
            
        case dateBorn:
            dp.hidden = true
            
            
        default:
            validEmal.hidden = true
            //lblEmail.textColor = UIColor.blackColor()
            dp.hidden = true
        }
    }
    
    // MARK: -Login in GooglePlus
    //=======================Login in GooglePlus======================
    
    
    // GooglePlush Delegate Methods
    //the method called from the func in appDelegate:"openURL" after clicked on "allow" button in the screen and it retrieve the details of the user from GooglePlush
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        
        if (GPPSignIn.sharedInstance().userID != nil)
        {
            let user:GTLPlusPerson = GPPSignIn.sharedInstance().googlePlusUser
            
            fullName.text = user.name.givenName //+ " " + user.name.familyName
            txtLastName.text = user.name.familyName
            if (user.emails != nil)
            {
                //init the e-mail(at indexPath 3)
                //                cellPersonal = tblDetails.cellForRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0)) as? PersonalDetailsTableViewCell
                //                cellPersonal?.txtfDetails.text = user.emails.first?.value
                txtEmail.text = user.emails.first?.value
            }
            else
            {
                print("no email")
            }
            //תמונת פרופיל
            //2do-להמיר את הurl של התמונה ע״מ לשמור את זה
            print(user.image)
        }
        else
        {
            print("User ID is nil")
        }
    }
    func didDisconnectWitherror(error:NSError?){
        print(error)
    }
    
    //MARK: - Validation
    
    ///פונקציה זו בודקת את תקינות השדות
    ///מקבלת את הtextField הנוכחי שעליו נמצא הפוקוס,
    ///וכן flag  לבדיקה ע״י מי הפונקציה נקראה:1=נקראה ע״י לחיצה על הרשם,2=נקראה ע״י כניסה לאחד מהשדות
    func checkValidation(textField:UITextField)
    {
        setImageBig(viewBigImage, hidden: true)
        ///* הערה:בכל אחת מהבדיקות בדקתי גם האם הטקסטפילד לא שווה אלי בעצמי למקרה שהטקסטפילד לא תקין והא היה האחרון שערכתי ועכשיו אני רוצה להכנס אליו שוב לתקן אותו
        
        
        //====================בדיקה מי ה-textField הקודם שמולא כדי לבדוק את תקינותו==============================
        
        ///---------------------------------------בדיקת השם------------------------
        
        if flag_FullName == true && textField != fullName && fullName.text != ""
        {
            validationName()
            
            flag_FullName = false
        }
            
            ///-----------------------------בדיקת השם משפחה---------------------------
            
        else if flag_LastName == true && textField != txtLastName && txtLastName.text != ""
        {
            validationLastName()
            
            flag_LastName = false
        }
            
            ///-----------------------------בדיקת הטלפון-----------------------
            
        else if flag_Phone == true && textField != txtPhone && txtPhone.text != ""
        {
            validationPhone()
            
            flag_Phone = false
        }
            ///--------------------בדיקת המייל------------------------
            
        else if flag_Email == true && textField != txtEmail && txtEmail.text != ""
        {
            validationEmail()
            
            flag_Email = false
        }
        
        //=======================================================================
        
        //בדיקה מי ה-textField הנוכחי שנבחר
        //והדלקת דגל מתאים,כדי לדעת בפעם הבאה מי היה הקודם לצורך בדיקת תקינות
        switch textField
        {
        case fullName:
            flag_FullName = true
            break
            
        case txtLastName:
            flag_LastName = true
            break
            
        case txtPhone:
            flag_Phone = true
            break
            
        case txtEmail:
            flag_Email = true
            break
            
        case dateBorn:
            flag_Date = true
            break
            
        default:
            break
        }
    }
    //for first and last name
    func isValidName(input: String) -> Bool {
        var numSpace = 0
        for chr in input.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z")  && !(chr >= "א" && chr <= "ת") && !(chr == " "))  {
                return false
            }
            if chr == " "
            {
                numSpace += 1
            }
        }
        if numSpace == input.characters.count || numSpace == input.characters.count - 1// אם יש רק רווחים או רק אות אחת והשאר רווחים
        {
            return false
        }
        else if numSpace > 0
        {
            let arr = input.componentsSeparatedByString(" ")
            for word in arr {
                if word.characters.count == 1
                {
                    return false
                }
            }
        }
        return true
    }
    
    func isValidAddress(input: String) -> Bool {
        for scalar in (txtAddress.text?.unicodeScalars)! {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F:   // Variation Selectors
                return false
            default:
                continue
            }
        }
        return true
    }
    
    func validationName()
    {
        isCheckNameEnd = true
        
        if fullName.text!.characters.count == 1 || isValidName(fullName.text!) == false ///שם פרטי
        {
            
            reqField.text = NSLocalizedString("ERROR_FNAME", comment: "")
            reqField.hidden = false
            Global.sharedInstance.isValid_FullName = false
        }
        else
        {
            Global.sharedInstance.isValid_FullName = true
        }
        
        if Global.sharedInstance.isRegisterClientClick == true || Global.sharedInstance.isRegisterProviderClick == true
        {
            if txtLastName.text != ""
            {
                self.validationLastName()
            }
            else if txtPhone.text != ""
            {
                self.validationPhone()
            }
            else if txtEmail.text != ""
            {
                self.validationEmail()
            }
            else
            {
                self.validToRegister()
            }
        }
    }
    
    func validationLastName()
    {
        isCheckLastNameEnd = true
        
        if txtLastName.text!.characters.count == 1 || isValidName(txtLastName.text!) == false ///שם משפחה
        {
            
            validLName.text = NSLocalizedString("ERROR_LNAME", comment: "")
            validLName.hidden = false
            Global.sharedInstance.isValid_lName = false
        }
        else
        {
            Global.sharedInstance.isValid_lName = true
        }
        
        if Global.sharedInstance.isRegisterClientClick == true || Global.sharedInstance.isRegisterProviderClick == true
        {
            if txtPhone.text != ""
            {
                self.validationPhone()
            }
            else if txtEmail.text != ""
            {
                self.validationEmail()
            }
            else
            {
                self.validToRegister()
            }
        }
    }
    
    func validationPhone()
    {
        var flag = true
        arrGoodPhone = []
        var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
        dicPhone["nvPhone"] = txtPhone.text
        ///בדיקה שהטלפון לא קיים כבר במערכת
        self.generic.showNativeActivityIndicator(self)
        
        if Reachability.isConnectedToNetwork() == false//if there is connection
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.CheckPhoneValidity(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.isCheckPhoneEnd = true
                
                if responseObject["Result"] as! Int == 0 //קיים
                {
                    self.validPhone.hidden = false
                    self.validPhone.text = NSLocalizedString("PHONE_EXIST", comment: "")
                    Global.sharedInstance.isValid_Phone = false
                }
                else //לא קיים
                {
                    Global.sharedInstance.isValid_Phone = true
                }
                let index0 = self.txtPhone.text?.startIndex.advancedBy(0)
                let index1 = self.txtPhone.text?.startIndex.advancedBy(1)
                if self.txtPhone.text?.characters.count < 10 || (self.txtPhone.text?.characters[index0!] != "0" || self.txtPhone.text?.characters[index1!] != "5")
                {
                    Global.sharedInstance.isValid_Phone = false
                    self.validPhone.text = NSLocalizedString("PHONE_ERROR", comment: "")
                    self.validPhone.hidden = false
                    
                }
                
                //בדיקה שהטלפון תקין
                let specialCharacterRegEx  = "[*]?[0-9]+"
                let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
                let specialresult = texttest2.evaluateWithObject(self.txtPhone.text)
                
                
                if !specialresult && self.txtPhone.text != ""// לא תקין
                {
                    for char in (self.txtPhone.text?.characters)!
                    {
                        if (char >= "0" && char <= "9") || char == "*"
                        {
                            let c:Character = char
                            self.arrGoodPhone.append(c)
                        }
                            
                        else
                        {
                            self.validPhone.text = NSLocalizedString("PHONE_ERROR", comment: "")
                            self.validPhone.hidden = false
                            self.txtPhone.text = ""
                            Global.sharedInstance.isValid_Phone = true
                            flag = false
                            break
                        }
                    }
                    
                    if flag == true
                    {
                        
                        self.txtPhone.text = ""
                        for i in 0 ..< self.arrGoodPhone.count
                        {
                            self.txtPhone.text = self.txtPhone.text! + String(Array(self.arrGoodPhone)[i])
                        }
                        Global.sharedInstance.isValid_Phone = true
                    }
                }
                    
                else
                {
                    //אם לא היה תקין בגלל השרת
                    if Global.sharedInstance.isValid_Phone != false
                    {
                        Global.sharedInstance.isValid_Phone = true
                    }
                }
                
                ///אם הגיעו מהכפתור של הרשם
                if Global.sharedInstance.isRegisterClientClick == true || Global.sharedInstance.isRegisterProviderClick == true
                {
                    if self.txtEmail.text != ""
                    {
                        self.validationEmail()
                    }
                    else
                    {
                        self.validToRegister()
                    }
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    print("Error: ", NSError!.localizedDescription)
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }
    
    func validationEmail()
    {
        var dicMail:Dictionary<String,String> = Dictionary<String,String>()
        dicMail["nvEmail"] = txtEmail.text
        
        ///בדיקה שהמייל לא קיים כבר במערכת
        self.generic.showNativeActivityIndicator(self)
        
        if Reachability.isConnectedToNetwork() == false
        {
            self.generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        }
        else
        {
            api.sharedInstance.CheckMailValidity(dicMail, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.isCheckEmailEnd = true
                
                if responseObject["Result"] as! Int == 0 //קיים
                {
                    self.validEmal.hidden = false
                    self.validEmal.text = NSLocalizedString("MAIL_EXIST", comment: "")
                    Global.sharedInstance.isValid_Email = false
                }
                else //לא קיים
                {
                    Global.sharedInstance.isValid_Email = true
                }
                
                ///בדיקה שהמייל תקין
                if !Validation.sharedInstance.mailValidation(
                    self.txtEmail.text!) && self.txtEmail.text != ""
                {
                    self.validEmal.hidden = false
                    self.validEmal.text = NSLocalizedString("MAIL_ERROR", comment: "")
                    
                    Global.sharedInstance.isValid_Email = false
                }
                else //תקין
                {
                    //אם לא היה תקין בגלל השרת
                    if Global.sharedInstance.isValid_Email != false
                    {
                        Global.sharedInstance.isValid_Email = true
                    }
                }
                
                ///אם הגיעו מהכפתור של הרשם יש לבדוק אם אפשר לעבור עמוד
                if Global.sharedInstance.isRegisterClientClick == true || Global.sharedInstance.isRegisterProviderClick == true
                {
                    self.validToRegister()
                }
                self.generic.hideNativeActivityIndicator(self)
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    print("Error: ", NSError!.localizedDescription)
                    self.generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            })
        }
    }
    
    func validToRegister()
    {
        var x = 0
        
        ///---------בדיקה שהשדות חובה מלאים
        
        if self.fullName.text == ""
        {
            self.reqField.hidden = false
            
            self.reqField.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
        if self.txtLastName.text == ""
        {
            self.validLName.hidden = false
            
            self.validLName.text = NSLocalizedString("REQUIREFIELD", comment: "")
            x = 1
        }
        
        if self.txtEmail.text == ""
        {
            self.validEmal.hidden = false
            self.validEmal.text = NSLocalizedString("REQUIREFIELD", comment: "")
            
            x = 1
        }
        if self.txtPhone.text == ""{
            self.validPhone.hidden = false
            self.validPhone.text = NSLocalizedString("REQUIREFIELD", comment: "")
            
            x = 1
        }
        
        ///-----------------------------------
        
        ///אם סמנו וי
        if self.btnRead.isCecked == false && isCheckFromClickRegister == true
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("READING_ARTICLE_NOT_FILLED", comment: "")
                , vc: self)
            x = 1
            isCheckFromClickRegister = false
        }
        
        if !Global.sharedInstance.isValid_FullName || !Global.sharedInstance.isValid_lName || !Global.sharedInstance.isValid_Phone /*|| !Global.sharedInstance.isValid_Address*/ || !Global.sharedInstance.isValid_Email
        {
            x = 1
            if Global.sharedInstance.isRegisterClientClick == true
            {
                Global.sharedInstance.isRegisterClientClick = false
            }
            else if Global.sharedInstance.isRegisterProviderClick == true
            {
                Global.sharedInstance.isRegisterProviderClick = false
            }
            
        }
        
        ///-----------------------------------
        
        if x == 0 ///הכל תקין
        {
            var dicPhone:Dictionary<String,String> = Dictionary<String,String>()
            dicPhone["nvPhoneNumber"] = self.txtPhone.text!
            
            if Reachability.isConnectedToNetwork() == false
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
            }
            else
            {
                api.sharedInstance.GetAndSmsValidationCode(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == 0
                    {
                        var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                        dicForDefault["verification"] = responseObject["Result"]
                        dicForDefault["phone"] = self.txtPhone.text!
                        //שמירת הטלפון והקוד ב-default
                        Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "verificationPhone")
                        var dateToServer = ""
                        if self.dateBorn.text != "" {
                            let arrDate_ddMMyyyy = Global.sharedInstance.cutStringBySpace(self.dateBorn.text!, strToCutBy: "/")
                            let arrDate_yyyMMdd:Array<String> = [arrDate_ddMMyyyy[2],arrDate_ddMMyyyy[1],arrDate_ddMMyyyy[0]]
                            
                            dateToServer = arrDate_yyyMMdd.joinWithSeparator("")
                        }
                        var base64 = ""
                        if let a = self.img.image
                        {
                            base64 = Global.sharedInstance.setImageToString(self.img.image!)
                        }
                        else{
                            base64 = ""
                        }
                        
//                        let dateFormatter1 = NSDateFormatter()
//                        dateFormatter1.timeStyle = .NoStyle
//                        dateFormatter1.dateFormat = "dd/MM/yyyy"
                        
//                        var dateToSer = NSDate()
//                        if self.dateBorn.text! != ""
//                        {
//                            dateToSer = dateFormatter1.dateFromString(self.dateBorn.text!)!
//                        }
//                        else
//                        {
//                            dateToSer = NSDate()
//                        }
                        ///שמירת הנתונים בגלובל
                        let user:User = User(_iUserId: 0,
                            _nvUserName: self.txtEmail.text!,
                            _nvFirstName: self.fullName.text!,
                            _nvLastName:  self.txtLastName.text!,
                            _dBirthdate: self.dateServer,//dateToSer,
                            _nvMail: self.txtEmail.text!,
                            _nvAdress: "",
                            _iCityType: 1,
                            _nvPhone: self.txtPhone.text!,
                            _nvPassword: (responseObject["Result"] as! Int).description,
                            _nvVerification: (responseObject["Result"] as! Int).description,
                            _bAutomaticUpdateApproval: self.btnGetP.isCecked,//אם סימן אני מאשר
                            _bDataDownloadApproval: true,//?
                            _bAdvertisingApproval: self.btnRead.isCecked,//אם סימן קראתי את התקנון
                            _bTermOfUseApproval: true,//תמיד
                            _iUserStatusType: 24,
                            _bIsGoogleCalendarSync: self.btnSync.isCecked,
                            //בינתיים
                            //2do -להכניס את המחרוזת של התמונה ב base64
                            _nvImage:base64,
                            _iCreatedByUserId: 1,//?
                            _iLastModifyUserId: 1,//?
                            _iSysRowStatus: 1//?
                        )
                        
                        Global.sharedInstance.currentUser = user
                        
                        self.dic2ForDefault["nvClientName"] = Global.sharedInstance.currentUser.nvFirstName
                        Global.sharedInstance.defaults.setObject(self.dic2ForDefault, forKey: "currentClintName")
                        if Global.sharedInstance.isRegisterClientClick == true || Global.sharedInstance.isRegisterProviderClick == true
                        {
                            Global.sharedInstance.isFromRegister = true
                            ///מעבר עמוד
                            let viewCon:MessageVerificationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVerificationViewController") as! MessageVerificationViewController
                            viewCon.phone = self.txtPhone.text!
                            viewCon.verification = ((responseObject["Result"] as? Int)?.description)!
                            viewCon.isProviderClickd = Global.sharedInstance.isRegisterProviderClick
                            viewCon.delegate = self
                            viewCon.isFromPersonalDetails = false
                            viewCon.delegateShowPhone = self
                            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
                            self.presentViewController(viewCon, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("ERROR_PHONE", comment: ""), vc: self)
                    }
                    }
                    ,failure: {(AFHTTPRequestOperation, NSError) -> Void in
                })
            }
        }
    }
    
    //open client exist
    func openCustomerDetails()
    {
        let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
        let viewCon:ModelCalenderViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as! ModelCalenderViewController
        
        self.navigationController?.pushViewController(viewCon, animated: false)
    }
    
    //when come back from ReadRegulation page
    func didReadRegulation() {
        setImageBig(viewBigImage, hidden: true)
        if Global.sharedInstance.fReadRegulation == true
        {
            btnRead.isCecked = true
        }
        else
        {
            btnRead.isCecked = false
        }
    }
    //MARK:-------cropImage
    
    func showResizablePicker(button: UIView) {
        self.imagePickerWD = WDImagePicker()
        self.imagePickerWD.cropSize = CGSizeMake(280, 280)
        self.imagePickerWD.delegate = self
        self.imagePickerWD.resizableCropArea = true
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController = UIPopoverController(contentViewController: self.imagePickerWD.imagePickerController)
            self.popoverController.presentPopoverFromRect(button.frame, inView: self.view, permittedArrowDirections: .Any, animated: true)
        } else {
            self.presentViewController(self.imagePickerWD.imagePickerController, animated: true, completion: nil)
        }
    }
    
    func imagePicker(imagePicker: WDImagePicker, pickedImage: UIImage) {
        
        self.img.image = Global.sharedInstance.resizeImage(pickedImage, newWidth: pickedImage.size.width*0.5)
        btnDelImage.hidden = false
        self.hideImagePicker()
    }
    
    func hideImagePicker() {
        ImagesCamera()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.popoverController.dismissPopoverAnimated(true)
        } else {
            self.imagePickerWD.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    //After choosing an image / photo image, the image is obtained
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.img.image = image
        btnDelImage.hidden = false
        
        if picker.sourceType == UIImagePickerControllerSourceType.Camera
        {
            self.img.image = Global.sharedInstance.resizeImage( self.img.image!, newWidth: image.size.width*0.5)
            btnDelImage.hidden = false
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                self.popoverController.dismissPopoverAnimated(true)
            } else {
                picker.dismissViewControllerAnimated(true, completion: nil)
            }
        }
        else
        {
            self.img.image = Global.sharedInstance.resizeImage(image, newWidth: image.size.width*0.5)
            btnDelImage.hidden = false
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                self.popoverController.dismissPopoverAnimated(true)
            } else {
                picker.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    //add crop to image
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = image
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage!, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    func openProvider()
    {
        isCheckFromClickRegister = true
        Global.sharedInstance.isRegisterClientClick = false
        Global.sharedInstance.isRegisterProviderClick = true
        
        isCheckEmailEnd = false
        isCheckPhoneEnd = false
        isCheckNameEnd = false
        if fullName.text != ""
        {
            validationName()
        }
        else if txtPhone.text != ""
        {
            self.validationPhone()
        }
        else if txtEmail.text != ""
        {
            self.validationEmail()
        }
        else
        {
            self.validToRegister()
        }
        
    }
    
    //open provider registration
    func openBuisnessDetails(){
        
        Global.sharedInstance.isProvider = true
        
        let viewCon:RgisterModelViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        
        viewCon.delegateFirstSection = viewCon1
        viewCon.delegateSecond1Section = viewCon1
        
        self.navigationController?.pushViewController(viewCon, animated: false)
    }
    
    //show the image big when tap on the image
    func showImage() {
        
        if img.image != nil && img.image != UIImage()
        {
            view.endEditing(true)
            dp.hidden = true
            imageBig.image = img.image
            setImageBig(viewBigImage, hidden: false)
        }
    }
    
    func deleteTxtPhone()
    {
        setImageBig(viewBigImage, hidden: true)
        txtPhone.text = ""
    }
    
    func setImageBig(view: UIView, hidden: Bool) {
        UIView.transitionWithView(view, duration: 0.5, options: .TransitionCrossDissolve, animations: {() -> Void in
            view.hidden = hidden
            }, completion: { _ in })
    }
    
}
