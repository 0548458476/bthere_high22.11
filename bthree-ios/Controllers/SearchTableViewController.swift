//
//  SearchTableViewController.swift
//  bthree-ios
//
//  Created by User on 28.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// טבלת החיפוש מיומן לקוח
class SearchTableViewController: UITableViewController{

    var filterSubArr:NSArray = NSArray()//fix 2.3 from nsmutbleArray
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterSubArr.count == 0
        {
            self.view.removeFromSuperview()
        }
        
        if filterSubArr.count < 4
        {
            return filterSubArr.count
        }
        
        return  4
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Search")as! SearchTableViewCell
        cell.setDisplayData(filterSubArr[indexPath.row] as! String)
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Global.sharedInstance.modelCalender!.txtSearch.text = filterSubArr[indexPath.row] as? String
        self.view.removeFromSuperview()
    }
}
