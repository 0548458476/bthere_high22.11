//
//  BusinessDetailsViewController.swift
//  BThere
//
//  Created by Tami wexelbom on 8.2.2016.
//  Copyright © 2016 tamy. All rights reserved.
//
import UIKit
import CoreLocation
import GoogleMaps

protocol saveInGlobalDelegate{
    func saveDataInGlobal()
}
//דף 1 בהרשמה -פרטי עסק
class BusinessDetailsViewController: UIViewController ,UITextFieldDelegate,saveInGlobalDelegate,UITableViewDelegate,UITableViewDataSource , SPGooglePlacesAutocompleteQueryDelegate,UIScrollViewDelegate {
    
    //MARK: - Outlet
    let googlePlacesAutocomplete = SPGooglePlacesAutocompleteQuery()
    var listAutocompletePlace:[SPGooglePlacesAutocompletePlace] = []
    var coordinatePackage:CLLocationCoordinate2D!
    var isSearch = false
    
    var siteSuffix = [".com",".net",".org",".edu",".biz",".gov",".mil",".info",".name",".me",".tv",".us",".mobi",".co.il"]
    
    
    //@IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewCity: UITableView!
    
    
    
    @IBOutlet weak var btnFaceBook: FBSDKLoginButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBAction func btnBack(sender: UIButton) {
    }
    
    @IBOutlet weak var lblValidId: UILabel!
    @IBOutlet weak var lblValidPhone: UILabel!
    @IBOutlet weak var lblValidFax: UILabel!
    
    @IBOutlet weak var lblValidName: UILabel!
    @IBOutlet weak var lblValidSite: UILabel!
    @IBOutlet weak var lblValidEmail: UILabel!
    
    @IBOutlet weak var lblValidAddress: UILabel!
    
    @IBOutlet weak var lblValidCity: UILabel!
    
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var lblID: UILabel!
    
    @IBOutlet var lblAddres: UILabel!
    
    @IBOutlet var lblPhone: UILabel!
    
    @IBOutlet var lblFax: UILabel!
    
    @IBOutlet var lblSite: UILabel!
    
    @IBOutlet var lblEmail: UILabel!
    
    @IBOutlet var lblSocialNetwork: UILabel!
    @IBOutlet var lblClickToCync: UILabel!
    
    @IBOutlet weak var viewBusinessName: UIView!
    @IBOutlet weak var viewId: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewPhon: UIView!
    @IBOutlet weak var viewFax: UIView!
    @IBOutlet weak var viewSite: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewSave: UIView!
    
    @IBOutlet weak var txtBusinessName: UITextField!
    @IBOutlet weak var txtId: UITextField!
    
    @IBOutlet weak var txtAdderss: SuggestiveTextField!
    
    @IBOutlet weak var txtCity: SuggestiveTextField!
    
    @IBOutlet weak var txtPhon: UITextField!
    @IBOutlet weak var txtFax: UITextField!
    @IBOutlet weak var txtSite: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var fBusinessName = false
    var fId = false
    var fAdderss = false
    var fCity = false
    var fPhone = false
    var fEmail = false
    var fFax = false
    var fSite = false
    
    var addressEdit = false
    var adressResult:[String] = []//מכיל את הכתובות להשלמה
    var citiesResult:[String] = []//מכיל את הערים להשלמה
    var isTxtAdress = false
    var isTxtCity = false
    var searchResults: [String]!
    //var searchResultController:SearchResultsController!
//    var resultsArray = [String]()
    var googleMapsView:GMSMapView!
    
    
    @IBAction func btnFacebook(sender: UIButton) {
    }
    @IBAction func btnInstagram(sender: UIButton) {
    }
    @IBAction func btnTwitter(sender: UIButton) {
    }
    @IBAction func btnGooglPlus(sender: UIButton) {
    }
    @IBOutlet weak var btnYouTube: UIButton!
    
    @IBOutlet weak var LblValidAddres: UILabel!
    @IBOutlet var btnSave: UIButton!
    @IBAction func btnSave(sender: UIButton) {
    }
    
    @IBOutlet var btnContinue: UIButton!
    @IBAction func btnContinue(sender: UIButton) {
        //Global.sharedInstance
    }
    @IBOutlet var lblRequiredField: UILabel!
    
    
    @IBOutlet weak var viewSocialNetworks: UIView!
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /////////////////////////למחוק!!!!

//        txtBusinessName.text = "doctor"
//        txtId.text = "111111111"
//        txtEmail.text = "ww@ww.ww"
//        txtAdderss.text = ""
//        txtPhon.text = "1212121"
///////////////////////////////////למחוק!!!!
    
        scrollView.delegate = self
        googlePlacesAutocomplete.delegate = self
        
        self.searchResults = Array()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        self.tableViewCity.registerClass(UITableViewCell.self,forCellReuseIdentifier: "cellIdentifier")
        tableView.delegate = self
        tableView.dataSource = self
        
        tableViewCity.delegate = self
        tableViewCity.dataSource = self
        
        
        lblValidAddress.hidden = true
        lblValidCity.hidden = true
        lblValidEmail.hidden = true
        lblValidFax.hidden = true
        lblValidId.hidden = true
        lblValidName.hidden = true
        lblValidPhone.hidden = true
        lblValidSite.hidden = true
        //      txtEmail.keyboardType = UIKeyboardType.EmailAddress
        
        
        txtBusinessName.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("BUSINESS_NAME", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtId.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("ID_TZ", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtFax.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("FAX", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtPhon.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("PHONE", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtSite.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("SITE", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtEmail.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("EMAIL", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtAdderss.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("ADDRES", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        txtCity.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("CITY", comment: ""), attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),NSFontAttributeName :UIFont(name: "OpenSansHebrew-Light", size: 16)!])
        
        Global.sharedInstance.rgisterModelViewController?.delegate = self
        btnFaceBook.readPermissions = ["public_profile", "email", "user_friends"]
        btnFaceBook.frame = CGRectMake(30, 25, 80, 30)
        //btnFaceBook.delegate = self
        
        btnFaceBook.backgroundColor = UIColor.clearColor()
        btnFaceBook.setImage(nil, forState: UIControlState.Normal)
        btnFaceBook.setTitleColor(UIColor.clearColor(), forState: .Normal)
        btnFaceBook.setBackgroundImage(UIImage(named: "40.png"), forState: .Normal)
        btnFaceBook.setBackgroundImage(UIImage(named: "40.png"), forState: .Selected)
        
        btnSave.setTitle(NSLocalizedString("SAVE", comment: ""), forState: .Normal)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(BusinessDetailsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //scrolling whene the keyboard open
        self.registerForKeyboardNotifications()
        txtPhon.delegate = self
        txtId.delegate = self
        
        txtFax.delegate = self
        
        txtSite.delegate = self
        
        txtAdderss.delegate = self
        txtCity.delegate = self
        
        txtBusinessName.delegate = self
        
        txtEmail.delegate = self
        
        //2do
        //זה זמני עד שיהיה רישום עסק באמצעות רשתות חברתיות
        //כרגע - זה מוסתר
        viewSocialNetworks.hidden = true
        viewSave.hidden = true
        
        //txtBusinessName.text = ""
        
        tableView.hidden = true
        tableViewCity.hidden = true
        txtCity.layer.borderWidth = 1
        txtCity.layer.borderColor = UIColor.blackColor().CGColor
        
        txtAdderss.layer.borderWidth = 1
        txtAdderss.layer.borderColor = UIColor.blackColor().CGColor
        
    }
    
    //top and bootom border to the views
    override func viewDidAppear(animated: Bool)
    {
        if Global.sharedInstance.isFromBack == true
        {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BusinessDetailsViewController.keyboardWasShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BusinessDetailsViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
            Global.sharedInstance.isFromBack = false
        }
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewBusinessName)
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewId)
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewAddress)
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewPhon)
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewFax)
        Colors.sharedInstance.addTopBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewSite)
        Colors.sharedInstance.addTopAndBottomBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewEmail)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color5, width: 1, any: viewSave)
        
        if Global.sharedInstance.RegisterNotEnd == true//לא סיים את תהליך הרישום
        {
            //אתחול השדות לפי מה שהמשתמש כבר הכניס ושמר
            txtBusinessName.text = Global.sharedInstance.currentProviderBuisnessDetails.nvSupplierName
            txtId.text = Global.sharedInstance.currentProviderBuisnessDetails.nvBusinessIdentity
            txtAdderss.text = Global.sharedInstance.currentProviderBuisnessDetails.nvAddress
            txtCity.text = Global.sharedInstance.currentProviderBuisnessDetails.nvCity
            
            txtPhon.text = Global.sharedInstance.currentProviderBuisnessDetails.nvPhone
            txtFax.text = Global.sharedInstance.currentProviderBuisnessDetails.nvFax
            txtSite.text = Global.sharedInstance.currentProviderBuisnessDetails.nvSiteAddress
            txtEmail.text = Global.sharedInstance.currentProviderBuisnessDetails.nvEmail
        }
        
        if Global.sharedInstance.didOpenBusinessDetails == false
        {
            Global.sharedInstance.didOpenBusinessDetails = true
            let viewCon:WelcomeAlertViewController = storyboard?.instantiateViewControllerWithIdentifier("WelcomeAlertViewController") as! WelcomeAlertViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            viewCon.delegate = Global.sharedInstance.rgisterModelViewController
            self.presentViewController(viewCon, animated: true, completion: nil)
        }
        
        // self.navigationItem.setHidesBackButton(true, animated:false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - textField delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch(textField){
        case(txtBusinessName):
            txtId.becomeFirstResponder()
        case(txtId):
            txtCity.becomeFirstResponder()
        case(txtCity):
            txtAdderss.becomeFirstResponder()
        case(txtAdderss):
            txtPhon.becomeFirstResponder()
        case(txtPhon):
            txtFax.becomeFirstResponder()
        case(txtFax):
            txtSite.becomeFirstResponder()
        case(txtSite):
            txtEmail.becomeFirstResponder()
        case(txtEmail):
            dismissKeyboard()
        default:
            txtBusinessName.becomeFirstResponder()
        }
        
        return true
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    //MARK: - scrolling when the keyboard open
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BusinessDetailsViewController.keyboardWasShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BusinessDetailsViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let _ = txtEmail
        {
            if (!CGRectContainsPoint(aRect, txtEmail!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(txtEmail!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.scrollEnabled = false
        
    }
    func checkValidity() -> Bool
    {
        if self.txtBusinessName.text == ""
        {
            self.lblValidName.hidden = false
            self.lblValidName.text = NSLocalizedString("REQUIREFIELD", comment: "")
            self.fBusinessName = false
        }
        else if self.txtBusinessName.text!.characters.count < 2 || isValidString(txtBusinessName.text!) == false
        {
            //self.txtBusinessName.text = ""
            self.lblValidName.hidden = false
            self.lblValidName.text = NSLocalizedString("NOT_VALID", comment: "")
            
            self.fBusinessName = false
        }
            
        else
        {
            self.fBusinessName = true
        }
        
        if self.txtId.text == ""
        {
            self.lblValidId.hidden = false
            self.lblValidId.text = NSLocalizedString("REQUIREFIELD", comment: "")
            self.fId = false
        }
        else if self.txtId.text!.characters.count < 9 || self.txtId.text!.characters.count > 9 //|| !Validation.sharedInstance.isTzValid(txtId.text!)
        {
            //self.txtId.text = ""
            self.lblValidId.hidden = false
            self.lblValidId.text = NSLocalizedString("NOT_VALID", comment: "")
            self.fId = false
        }
        else
        {
            self.fId = true
        }
        
        if self.txtAdderss.text == ""
        {
            self.lblValidAddress.hidden = false
            self.lblValidAddress.text = NSLocalizedString("REQUIREFIELD", comment: "")
            self.fAdderss = false
        }
        else if self.txtAdderss.text!.characters.count < 2 || isValidAddress(txtAdderss) == false

        {
            //self.txtAdderss.text = ""
            self.lblValidAddress.hidden = false
            self.lblValidAddress.text = NSLocalizedString("NOT_VALID", comment: "")
            
            self.fAdderss = false
        }
        else
        {
            self.fAdderss = true
        }
        //for city
        if self.txtCity.text == ""
        {
            self.lblValidCity.hidden = false
            self.lblValidCity.text = NSLocalizedString("REQUIREFIELD", comment: "")
            self.fCity = false
        }
        else if self.txtCity.text!.characters.count < 2 || isValidAddress(txtCity) == false

        {
            //self.txtCity.text = ""
            self.lblValidCity.hidden = false
            self.lblValidCity.text = NSLocalizedString("NOT_VALID", comment: "")
            
            self.fCity = false
        }
        else
        {
            self.fCity = true
        }

        if self.txtEmail.text == ""
        {
            self.lblValidEmail.hidden = false
            self.lblValidEmail.text = NSLocalizedString("REQUIREFIELD", comment: "")
            fEmail = false
        }
        else if self.isValidEmail(self.txtEmail.text!) == false
        {
            self.lblValidEmail.hidden = false
            self.lblValidEmail.text = NSLocalizedString("NOT_VALID", comment: "")
            fEmail = false
        }
        else{
            fEmail = true
        }
        if self.txtPhon.text == ""
        {
            self.lblValidPhone.hidden = false
            self.lblValidPhone.text = NSLocalizedString("REQUIREFIELD", comment: "")
            fPhone = false
        }
        else if self.isValidPhone(self.txtPhon.text!) == false || txtPhon.text?.characters.count < 5
        {
            self.lblValidPhone.hidden = false
            self.lblValidPhone.text = NSLocalizedString("NOT_VALID", comment: "")
            fPhone = false
        }
        else{
            fPhone = true
        }
        
        if txtFax.text != ""//כי זה לא שדה חובה
        {let index = txtFax.text?.characters.startIndex.advancedBy(0)
            if self.isValidPhone(self.txtFax.text!) == false || txtFax.text?.characters.count < 9 || txtFax.text?.characters[index!] != "0"
                
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("FAX_VALId", comment: ""), vc: self)
                fFax = false
            }
            else{
                fFax = true
            }
        }
        else if txtFax.text != "" {
            if self.txtFax.text!.characters.count < 9 || self.txtFax.text!.characters.count > 9
            {
                //self.txtId.text = ""
                self.lblValidFax.hidden = false
                self.lblValidFax.text = NSLocalizedString("NOT_VALID", comment: "")
                self.fFax = false
            }
        }
        else{
            fFax = true
        }
        
        if txtSite.text != ""//כי זה לא שדה חובה
        {
            //if validateUrl(txtSite.text!) == false
            if verifyUrl(txtSite.text) == false
            {
                self.lblValidSite.hidden = false
                self.lblValidSite.text = NSLocalizedString("NOT_VALID", comment: "")
                fSite = false
            }
            else{
                fSite = true
            }
        }
        else{
            fSite = true
        }
        
        if fEmail == true && fPhone == true && fBusinessName == true  && fAdderss == true && fCity == true && fId == true && fFax == true && fSite == true
        {
            return true
        }
        
        return false
    }
    
    func saveDataInGlobal()
    {
        if checkValidity() == true
        {
            if txtAdderss.restorationIdentifier != nil
            {
                Global.sharedInstance.placeIdForMap = txtAdderss.restorationIdentifier!
            }

//            var cityFromAddress:String = ""
//            var AddressArr:Array<String> = (txtCity.text?.componentsSeparatedByString(", "))!
//            cityFromAddress = AddressArr[0]

            
            let provider:Provider = Provider(_iUserId: Global.sharedInstance.currentProvider.iUserId,_iIdBuisnessDetails: Global.sharedInstance.currentProvider.iIdBuisnessDetails,
                                             _nvSupplierName: txtBusinessName.text!,
                                             _nvBusinessIdentity: txtId.text!,
                                             _nvAddress: txtAdderss.text!,
                                             _nvCity: txtCity.text!,
                                             _iCityType: 1,
                                             _nvPhone: txtPhon.text!,
                                             _nvFax: txtFax.text!,
                                             _nvEmail: txtEmail.text!,
                                             _nvSiteAddress: txtSite.text!)
            
            Global.sharedInstance.currentProvider = provider
            //שמירת פרטי הספק במכשיר
            Global.sharedInstance.defaults.setObject(provider.getDic(), forKey: "providerDic")
            Global.sharedInstance.fIsValidDetails = true
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        }
        else
        {
            Global.sharedInstance.fIsValidDetails = false
        }
    }
    
    
    //for phone and fax
    func isValidPhone(input: String) -> Bool {
        for chr in input.characters {
            if (!(chr >= "0" && chr <= "9") && !(chr == "-") && !(chr == "*")) {
                return false
            }
        }
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    //for business Name
    func isValidString(name:String)->Bool
    {
        var numSpace = 0
        for chr in name.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z")  && !(chr >= "א" && chr <= "ת") && !(chr >= "0" && chr <= "9") && chr != " " )
            {
                return false
            }
            if chr == " "
            {
                numSpace += 1
            }
        }
        if numSpace == name.characters.count || numSpace == name.characters.count - 1// אם יש רק רווחים או רק אות אחת והשאר רווחים
        {
            return false
        }
        return true
    }
    
    // MARK: - TextField
    //=========================TextField==============
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        dismissKeyboard()
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch(textField)
        {
        case txtBusinessName:
            if self.txtBusinessName.text!.characters.count < 2 && self.txtBusinessName.text != "" || isValidString(txtBusinessName.text!) == false && txtBusinessName.text != ""
            {
                //self.txtBusinessName.text = ""
                self.lblValidName.hidden = false
                self.lblValidName.text = NSLocalizedString("NOT_VALID", comment: "")
                
                self.fBusinessName = false
            }
                
            else
            {
                self.fBusinessName = true
            }
        case txtId:
            if  (self.txtId.text!.characters.count < 9 || self.txtId.text!.characters.count > 9)//|| !Validation.sharedInstance.isTzValid(txtId.text!))
            && self.txtId.text != ""
            {
                //self.txtId.text = ""
                self.lblValidId.hidden = false
                self.lblValidId.text = NSLocalizedString("NOT_VALID", comment: "")
                
                //Alert.sharedInstance.showAlert("נא להכניס 9 מספרים בלבד", vc: self)
                
                self.fId = false
            }
            else
            {
                self.fId = true
            }
            
        case txtAdderss:
            
            if self.txtAdderss.text == ""
            {
                tableView.hidden = true
            }

            if self.txtAdderss.text!.characters.count < 2 && self.txtAdderss.text != "" || isValidAddress(txtAdderss) == false
            {
                //self.txtAdderss.text = ""
                self.lblValidAddress.hidden = false
                self.lblValidAddress.text = NSLocalizedString("NOT_VALID", comment: "")
                
                self.fAdderss = false
            }
            else
            {
                self.fAdderss = true
            }
            txtAdderss.dismissSuggestionTableView()
            
        case txtCity:
            
            if self.txtCity.text == ""
            {
                tableViewCity.hidden = true
            }
            if self.txtCity.text!.characters.count < 2 && self.txtCity.text != "" || isValidAddress(txtCity) == false
  
            {
                //self.txtCity.text = ""
                self.lblValidCity.hidden = false
                self.lblValidCity.text = NSLocalizedString("NOT_VALID", comment: "")
                
                self.fCity = false
            }
            else
            {
                self.fCity = true
            }
            txtCity.dismissSuggestionTableView()
            
        case txtEmail:
            if self.isValidEmail(self.txtEmail.text!) == false && self.txtEmail.text != ""
            {
                //self.txtEmail.text = ""
                self.lblValidEmail.hidden = false
                self.lblValidEmail.text = NSLocalizedString("NOT_VALID", comment: "")
                
                self.fEmail = false
            }
            else
            {
                self.fEmail = true
            }
        case txtPhon:
            
            if self.isValidPhone(self.txtPhon.text!) == false || txtPhon.text?.characters.count < 5 && self.txtPhon.text != ""
                
            {
                //self.txtPhon.text = ""
                self.lblValidPhone.hidden = false
                self.lblValidPhone.text = NSLocalizedString("NOT_VALID", comment: "")
                
                self.fPhone = false
            }
            else
            {
                self.fPhone = true
            }
            
        case txtFax:
            
            if txtFax.text != ""//כי זה לא שדה חובה
            { let index = txtFax.text?.characters.startIndex.advancedBy(0)
                if self.isValidPhone(self.txtFax.text!) == false ||  txtFax.text?.characters.count < 9 || txtFax.text?.characters[index!] != "0"
                {
                    //                    self.lblValidFax.hidden = false
                    //                    self.lblValidFax.text = NSLocalizedString("NOT_VALID", comment: "")
                    Alert.sharedInstance.showAlert(NSLocalizedString("FAX_VALId", comment: ""), vc: self)
                    self.fFax = false
                }
                else
                {
                    self.fFax = true
                }
            }
            else
            {
                self.fFax = true
            }
        case txtSite:
            if txtSite.text != ""//כי זה לא שדה חובה
            {
                if verifyUrl(txtSite.text) == false
                {
                    self.lblValidSite.hidden = false
                    self.lblValidSite.text = NSLocalizedString("NOT_VALID", comment: "")
                    
                    self.fSite = false
                }
                else
                {
                    self.fSite = true
                }
            }
            else
            {
                self.fSite = true
            }
            
            
        default:return
            //            lblEmail.textColor = UIColor.blackColor()
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        switch textField
        {
        case txtEmail:
            lblValidEmail.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            break
        case txtPhon:
            lblValidPhone.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            break
        case txtFax:
            lblValidFax.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            break
        case txtAdderss:
            addressEdit = true
            Global.sharedInstance.isAddressCity = false
            lblValidAddress.hidden = true
            isTxtAdress = true
            isTxtCity = false
            //            tableView.hidden = false
            break
            
        case txtCity:
            addressEdit = true
            Global.sharedInstance.isAddressCity = true
            lblValidCity.hidden = true
            isTxtAdress = false
            isTxtCity = true
            //            tableView.hidden = false
            break
            
        case txtBusinessName:
            lblValidName.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            break
        case txtId:
            lblValidId.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            break
        case txtSite:
            lblValidSite.hidden = true
            addressEdit = false
            isTxtAdress = false
            isTxtCity = false
            //        case txtEmail:
            //            lblValidEmail.hidden = true
            //            break
            //        case txtEmail:
            //            lblValidEmail.hidden = true
            //            break
            
        default:break//lblValidEmail.hidden = true
        }
        
    }
    
    //    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    ////        var location = CLLocationCoordinate2D(latitude: 47.620506, longitude: -122.349277)
    ////        var gp = GooglePlaces()
    ////        gp.search(location, radius: 2000, query: "coffee") { (items, errorDescription) -> Void in
    ////
    ////            println("Result count: \(items!.count)")
    ////            for index in 0..<items!.count {
    ////                println([items![index].name])
    ////            }
    ////        }
    //        let placesClient = GMSPlacesClient()
    //
    //        placesClient.autocompleteQuery(txtAdderss.text!, bounds: nil, filter: nil) { (results, error:NSError?) -> Void in
    //            self.resultsArray.removeAll()
    //            if results == nil {
    //                return
    //            }
    //            for result in results!{
    //                if let result = result as? GMSAutocompletePrediction{
    //                    self.resultsArray.append(result.attributedFullText.string)
    //                }
    //            }
    //            self.tableView.reloadData()
    //            //.searchResultController.reloadDataWithArray(self.resultsArray)
    //        }
    //        return true
    //    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        if textField == txtCity {
            googlePlacesAutocomplete.input = textField.text! + string
            googlePlacesAutocomplete.fetchPlaces()
        }
        else if textField == txtAdderss {
            if txtCity.text != "" && !(textField.text?.containsString(txtCity.text!))!
            {
            googlePlacesAutocomplete.input = txtCity.text! + textField.text! + string
            }
            else
            {
            googlePlacesAutocomplete.input = textField.text! + string
            }
            googlePlacesAutocomplete.fetchPlaces()
        }
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        
        if textField == txtId
        {
            if startString.characters.count > 9
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_NINE_CHAR", comment: ""), vc: self)
                return false
            }
            else
            {
                return true
            }
        }
        
        if textField == txtFax
        {
            
            
            if  startString.characters.count > 9
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("FAX_VALId", comment: ""), vc: self)
                return false
            }
            else
            {
                return true
            }
        }
        if textField == txtBusinessName
        {
            if startString.characters.count > 30
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_ONLY30_CHARACTERS", comment: ""), vc: self)
                return false
            }
            else
            {
                return true
            }
        }
        return true
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        
        if Global.sharedInstance.cutStringBySpace(urlString!, strToCutBy: ".").count == 2 || Global.sharedInstance.cutStringBySpace(urlString!, strToCutBy: ".").count == 3
        {
            for chr in (urlString?.characters)! {
                if ((chr >= "a" && chr <= "z") || (chr >= "A" && chr <= "Z") || (chr >= "0" && chr <= "9") || (chr == ".")) {
                    return true
                }
            }
        }
        return false
//        let urlRegEx: String = "[A-Za-z]{2,}+\\.+[A-Za-z]{2,}+\\.+[A-Za-z]{2,}"
//        let urlTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
//        //בשביל כתובת שמסתיימת ב:.co.il
//        let urlRegCo:String = "[A-Za-z]{2,}+\\.+[A-Za-z]{2,}+\\.+[A-Za-z]{2,}+\\.+[A-Za-z]{2,}"
//        let urlTestCo: NSPredicate = NSPredicate(format: "SELF MATCHES %@", urlRegCo)
//        
//        if urlTest.evaluateWithObject(urlString) == true || urlTestCo.evaluateWithObject(urlString) == true && urlString!.hasSuffix(".co.il")
//        {
//            //בודק האם הסיומת של האתר מכילה את אחת מהסיומות הקבועות(לפי המערך)
//            for suffix in siteSuffix {
//                if urlString!.hasSuffix(suffix) == true
//                {
//                    return true
//                }
//            }
//        }
//        return false
    }
    
    //MARK: - table view
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellIdentifier", forIndexPath: indexPath)
        
        //self.resultsArray
        cell.textLabel?.text = self.searchResults[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath){
        // 1
        //self.dismissViewControllerAnimated(true, completion: nil)
        tableView.hidden = true
        // 2
        let correctedAddress:String! = self.searchResults[indexPath.row].stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.symbolCharacterSet())
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)&sensor=false")
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) -> Void in
            // 3
            do {
                if data != nil{
                    let dic = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableLeaves) as!  NSDictionary
                    
                    let lat = dic["results"]?.valueForKey("geometry")?.valueForKey("location")?.valueForKey("lat")?.objectAtIndex(0) as! Double
                    let lon = dic["results"]?.valueForKey("geometry")?.valueForKey("location")?.valueForKey("lng")?.objectAtIndex(0) as! Double
                    // 4
                    //self.delegate.locateWithLongitude(lon, andLatitude: lat, andTitle: self.searchResults[indexPath.row] )
                }
            }catch {
                print("Error")
            }
        }
        // 5
        task.resume()
    }
    
    //-MARK: SPGooglePlacesAutocompleteQueryDelegate
    func googlePlaceReload(listPlaces:[SPGooglePlacesAutocompletePlace])
    {
        if isSearch{
            googlePlacesAutocomplete.googleGeocoded(referenceToGeocoded:listPlaces[0].reference)
            isSearch = false
        }
        else{
            var placesName:[String] = []
            var placesId:[String] = []
            
            for place in listPlaces{
                placesName += [place.name]
                placesId += [place.placeId]
            }
            
            //st
            if isTxtAdress == true
            {
                adressResult = placesName
            }
            else if isTxtCity == true
            {
                citiesResult = placesName
            }
            listAutocompletePlace=listPlaces
            if Global.sharedInstance.isAddressCity == false
            {
            txtAdderss.setSuggestions(placesName)
            txtAdderss.setSuggestionsPlacesId(placesId)
            txtAdderss.matchStrings(txtAdderss.text)
            txtAdderss.showSuggestionTableView()
            }
            else
            {
            txtCity.setSuggestions(placesName)
            txtCity.setSuggestionsPlacesId(placesId)
            txtCity.matchStrings(txtCity.text)
            txtCity.showSuggestionTableView()
            }
            //            tableView.hidden = false
            
        }
    }
    
    func googlePlaceGeocoded(latitude:Double, longitude:Double)
    {
        //        mapView.clear()
        //        var marker = GMSMarker()
        //        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        //        marker.title = txfAddressSearch.text
        //        mapView.selectedMarker = marker
        //        marker.map = mapView
        //
        //        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 15)
        //        mapView.camera = camera
        
        if Global.sharedInstance.isAddressCity == false
        {
        self.txtAdderss.text = ""
        }
        else
        {
        self.txtCity.text = ""
        }
    }
    
    func googlePlaceReverseGeocode(address: String) {
        if Global.sharedInstance.isAddressCity == false
        {
        txtAdderss.text = address
        }
        else
        {
            txtCity.text = address
        }
    }
    
    func googlePlaceReverseGeocode(address: String , country: String ,city: String)
    {
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if scrollView.contentOffset.y < 100
        {
            if addressEdit == true
            {
                if Global.sharedInstance.isAddressCity == false
                {
                txtAdderss.showSuggestionTableView()
                }
                else
                {
                    txtCity.showSuggestionTableView()
                }
            }
        }
        else
        {
            if addressEdit == true
            {
                if Global.sharedInstance.isAddressCity == false
                {
                txtAdderss.dismissSuggestionTableView()
                }
                else
                {
                txtCity.dismissSuggestionTableView()
                }
            }
        }
    }
    
    func isValidAddress(textField:UITextField) -> Bool {
        
        if textField == txtAdderss
        {
            //            if adressResult.contains(textField.text!)
            //            {
            isTxtAdress  = false
            return true
            //            }
        }
        else if textField == txtCity
        {
            if citiesResult.contains(textField.text!)
            {
                isTxtCity = false
                return true
            }
        }
        
        return false
        
        
        //לא עובד טוב
//        for scalar in (address.unicodeScalars) {
//            switch scalar.value {
//            case 0x1F600...0x1F64F, // Emoticons
//            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
//            0x1F680...0x1F6FF, // Transport and Map
//            0x2600...0x26FF,   // Misc symbols
//            0x2700...0x27BF,   // Dingbats
//            0xFE00...0xFE0F:   // Variation Selectors
//                return false
//            default:
//                continue
//            }
//        }
//        return true
    }
    
}

