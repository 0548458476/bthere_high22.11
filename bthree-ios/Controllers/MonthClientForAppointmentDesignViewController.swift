//
//  MonthClientForAppointmentDesignViewController.swift
//  Bthere
//
//  Created by User on 24.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

//תצוגת חודש של יומן ספק שהלקוח רואה

class MonthClientForAppointmentDesignViewController:UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate,reloadTableFreeDaysDelegate{
    //MARK: - @IBOutlet
    @IBOutlet weak var tblWorkers: UITableView!
    
    @IBOutlet weak var lblDayOfWeek1: UILabel!
    
    @IBOutlet weak var lblDayOfWeek2: UILabel!
    
    @IBOutlet weak var lblDayOfWeek3: UILabel!
    
    @IBOutlet weak var lblDayOfWeek4: UILabel!
    
    @IBOutlet weak var lblDayOfWeek5: UILabel!
    
    @IBOutlet weak var lblDayOfWeek6: UILabel!
    
    @IBOutlet weak var lblDayOfWeek7: UILabel!
    
    @IBOutlet var collDays: UICollectionView!
    
    @IBOutlet var lblHebrewDate: UILabel!
    @IBOutlet var lblCurrentDate: UILabel!
        
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSync: eyeSynCheckBox!
    //MARK: - @IBAction

    @IBAction func btnSync(sender: eyeSynCheckBox)
    {
        if sender.isCecked == false{
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = true
            i = 1
            collDays.reloadData()
        }
            
        else{
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = false
            i = 1
            collDays.reloadData()
            
        }
    }
    //func set prev date on click prev btn

    @IBAction func btnBefore(sender: UIButton) {
        //currentDate =
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.removeMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        moneForBackColor = 1
        //refresh bthere event when date selected change

            setEventBthereInMonth()
        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        //today = Calendar.sharedInstance.carrentDate
        today = Mycalendar.dateByAddingUnit(.Month, value: -1, toDate: today, options: [])!
        
        changeLblDate()
    
    }
    //func set next date on click next btn

    @IBAction func btnNext(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.addMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
        moneForBackColor = 1
        //refresh bthere event when date selected change

        setEventBthereInMonth()

        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        //today = Calendar.sharedInstance.carrentDate
        today = Mycalendar.dateByAddingUnit(.Month, value: 1, toDate: today, options: [])!
        
        changeLblDate()
       
    }
    
    //MARK: - varibals
    private let kKeychainItemName = "Google Calendar API"
    private let kClientID = "284147586677-69842kmfbfll1dmec57c9gklqnpa5n2u.apps.googleusercontent.com"
    
    var delegate:openMonthDelegte!=nil
    
    let output = UITextView()
    
    var today: NSDate = NSDate()
    
    var dateFormatter = NSDateFormatter()

    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var arrEventsCurrentDay:Array<EKEvent> = []
    
    var days:Array<Int> = []
    var numDaysInMonth:Int = 0
    var dateFirst:NSDate = NSDate()
    var dayInWeek:Int = 0
    var i = 1
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var hasEvent = false
    var moneForBackColor = 1//בשביל הימים שעברו(שקיפות כנראה)
    let calendar = NSCalendar.currentCalendar()
    var titles : [String] = []
    var startDates : [NSDate] = []
    var endDates : [NSDate] = []
    
    let eventStore = EKEventStore()
    var shouldShowDaysOut = true
    var animationFinished = true
    var currentDate:NSDate = NSDate()
    var bthereEventDateDayInt:Array<Int> = Array<Int>()// ימים שיש בהם אירוע
    
    //MARK: - Initials
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            let fontSize:CGFloat = self.lblCurrentDate.font.pointSize;
            lblCurrentDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 20)
        }


        today = NSDate()
        Calendar.sharedInstance.carrentDate
 = NSDate()//check if  use

//        Global.sharedInstance.isSyncWithGoogleCalendarAppointment = Global.sharedInstance.currentUser.bIsGoogleCalendarSync
        if Global.sharedInstance.isSyncWithGoogleCalendarAppointment == true
        {
            btnSync.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSync.isCecked = false
            i = 1
            collDays.reloadData()
        }
        
        Global.sharedInstance.designMonthAppointment = self
        // Global.sharedInstance.currDateSelected = NSDate()
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        
        // Menu delegate [Required]
        // Do any additional setup after loading the view.
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        //getFirstDay(NSDate())
        if language == "he"
        {
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
        }
        dateFormatter.dateFormat = "dd/MM/yyyy"
        changeLblDate()
        //refresh device event when date selected change
        setEventsArray()


            let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
            viewSync.addGestureRecognizer(tapSync)

        setEventBthereInMonth()
   // }
    }

    override func viewDidAppear(animated: Bool) {
        
        Global.sharedInstance.getEventsFromMyCalendar()
  
        if Global.sharedInstance.isSyncWithGoogleCalendarAppointment == true
        {
            btnSync.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSync.isCecked = false
            i = 1
            collDays.reloadData()
        }
        
        self.today = NSDate()
        
        self.numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        self.dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        self.dayInWeek = Calendar.sharedInstance.getDayOfWeek(self.dateFirst)!
        self.i = 1
        self.moneForBackColor = 1
        
        self.collDays.reloadData()
        changeLblDate()
        //set bthere event by month
        setEventBthereInMonth()
    }
    //set bthere event by month
    func setEventBthereInMonth()
             {
              self.bthereEventDateDayInt = []
                var orderDetailsObj:OrderDetailsObj =  OrderDetailsObj()
                    var a:Int = Int()
                   var b:Int = Int()
                   var c:Int = Int()
                   var d:Int = Int()
        if Global.sharedInstance.ordersOfClientsArray.count > 0
        {
            for item in Global.sharedInstance.ordersOfClientsArray
            {
               orderDetailsObj = item as  OrderDetailsObj
                a = Calendar.sharedInstance.getMonth(orderDetailsObj.dtDateOrder, reduce: 0, add: 0)
                b = Calendar.sharedInstance.getMonth( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0)
                c = Calendar.sharedInstance.getYear(orderDetailsObj.dtDateOrder, reduce: 0, add: 0)
                d = Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0)
                if Calendar.sharedInstance.getMonth(orderDetailsObj.dtDateOrder, reduce: 0, add: 0) == Calendar.sharedInstance.getMonth( Calendar.sharedInstance.carrentDate
                    , reduce: 0, add: 0) &&  Calendar.sharedInstance.getYear(orderDetailsObj.dtDateOrder, reduce: 0, add: 0) == Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
                        , reduce: 0, add: 0)
                {
                    
//                    d = Calendar.sharedInstance.getYear( Calendar.sharedInstance.carrentDate
//                        , reduce: 0, add: 0)

                    bthereEventDateDayInt.append(Calendar.sharedInstance.getDay(orderDetailsObj.dtDateOrder, reduce: 0, add: 0))
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    // MARK: Optional methods

    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    //MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if (numDaysInMonth > 30 && dayInWeek == 6
            ) || (numDaysInMonth > 29 && dayInWeek == 7){
            return 42
        }
        return 35
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if (numDaysInMonth > 30 && dayInWeek == 6) || (numDaysInMonth > 29 && dayInWeek == 7)//if has 31 in month
        {
            return CGSize(width: view.frame.size.width / 7, height:  view.frame.size.width / 8.5)
        }        
        return CGSize(width: view.frame.size.width / 7, height:  view.frame.size.width / 7)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        //Aligning right to left on UICollectionView
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        let cell:DayMonthCalendarCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("DayMonthCalendarCollectionViewCell",forIndexPath: indexPath) as! DayMonthCalendarCollectionViewCell
        
    //אם זה תחת מודל של יומן תורים מול לקוח
        cell.delegate = Global.sharedInstance.calendarAppointment
        cell.backgroundColor =  UIColor.clearColor()
        cell.lblIsBthereEvent.hidden = true

        cell.whichModelOpenMe = 2

        cell.imgToday.hidden = true
        cell.lblDayDesc.alpha = 1.0
        cell.viewIsFree.backgroundColor = UIColor.clearColor()
        //     var s =
        // var d:NSDate()
        cell.lblDayDesc.text = ""
        cell.lblDayDesc.textColor = Colors.sharedInstance.color1

        if moneForBackColor % 7 == 0{
            if Global.sharedInstance.whichReveal == false{
                cell.lblDayDesc.textColor = Colors.sharedInstance.color3
                
                cell.isShabat = true
                
            }
            else{
                cell.lblDayDesc.textColor = Colors.sharedInstance.color4
            }
        }
        moneForBackColor += 1
        if indexPath.row >= (dayInWeek - 1)  && indexPath.row < (numDaysInMonth + dayInWeek - 1 ){

            if bthereEventDateDayInt.contains(i)
            {
                cell.lblIsBthereEvent.hidden = false
            }
            cell.setDisplayData(i)
            i += 1
        }
        else{
            cell.setNull()
            cell.btnEnterToDay.enabled = false
            cell.viewIsFree.backgroundColor =  UIColor.clearColor()
        }
        if language == "he"
        {
            cell.transform = scalingTransform
        }
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        componentsCurrent.day = i
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
   
    //MARK: - functions
    //bool func that  get date and check if has event in this date
    func ifHasEventInDayFunc( dt:NSDate)-> Bool
    {
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: dt)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                return true
            }
        }
        return false
    }
    //change the text on lblDate on prev or next click
    func changeLblDate(){
        
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            let fontSize:CGFloat = self.lblCurrentDate.font.pointSize;
            lblCurrentDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 20)
            lblHebrewDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 11)

        }
        var str:String = ""
        var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
        //var index:Int = 0
        let characters = s1[1].characters.map { String($0) }
        if characters[0] == String(0){
            str = NSDateFormatter().monthSymbols[Int(characters[1])! - 1]
            //monthArray[Int(characters[1])!]
            
        }
        else{
            str = NSDateFormatter().monthSymbols[Int(s1[1])! - 1]
            //monthArray[Int(s1[1])!]
            
        }
        lblCurrentDate.text = str + " " + s1[2]
        let hebrew: NSLocale?
        if language == "he"
        {
            // Hebrew, Israel
            hebrew = NSLocale(localeIdentifier: "he_IL")
        }
        else
        {
            hebrew = NSLocale(localeIdentifier: "en_IL")
        }
        
        //NSHebrewCalendar
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.locale = hebrew
        dateFormat.calendar = Mycalendar
        dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
        let dateString: String = dateFormat.stringFromDate(today)
        lblHebrewDate.text = dateString
    }
    //set device event by month
    func setEventsArray()
    {
        arrEventsCurrentDay = []
        for var item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            
            if yearEvent == yearToday && monthEvent == monthToday
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
    
    // When the view appears, ensure that the Google Calendar API service is authorized
    // and perform API calls

    
    //reloadTableFreeDaysDelegate
    
    func reloadTableFreeDays()  {
        i = 1
        collDays.reloadData()
        delegate.openMonthFromChooseWorker()
    }

    
    func showSync()
    {
        if btnSync.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSync.isCecked = true
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = true
            i = 1
            collDays.reloadData()
        }
            
        else
        {
            btnSync.isCecked = false
            Global.sharedInstance.isSyncWithGoogleCalendarAppointment = false
            i = 1
            collDays.reloadData()
            
        }
    }

}
//remove duplicate values from array
func uniq<S : SequenceType, T : Hashable where S.Generator.Element == T>(source: S) -> [T] {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}

