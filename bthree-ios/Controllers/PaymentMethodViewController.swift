//
//  PaymentMethodViewController.swift
//  bthree-ios
//
//  Created by Tami wexelbom on 2/10/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//אמצעי תשלום
class PaymentMethodViewController: NavigationModelViewController,UIGestureRecognizerDelegate,UITextFieldDelegate{

    
    @IBOutlet weak var reqTz: UILabel!
    @IBOutlet weak var reqThreeNums: UILabel!
    @IBOutlet weak var reqTokef: UILabel!
    @IBOutlet weak var reqNumCard: UILabel!
    @IBOutlet weak var numCart: UIView!
    @IBOutlet weak var vie3Nums: UIView!
    @IBOutlet weak var vieTokef: UIView!
    @IBOutlet weak var vieNumCart: UIView!
    @IBOutlet weak var cnlBtn: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var tzLbl: UILabel!
    @IBOutlet weak var tzTxt: UITextField!
    @IBOutlet weak var threeNumsLbl: UILabel!
    @IBOutlet weak var threeNumsTxt: UITextField!
    @IBOutlet weak var tokefLbl: UILabel!
    @IBOutlet weak var tokefTxt: UITextField!
    @IBOutlet weak var numCardLbl: UILabel!
    @IBOutlet weak var numCardTxt: UITextField!
    
    //onClick send, check validity
    @IBAction func btnSend(sender: AnyObject)
    {
        var x = 0
        if numCardTxt.text == ""
        {
            x = 1
            reqNumCard.text = NSLocalizedString("REQUIREFIELD", comment: "")
            reqNumCard.hidden = false
        }
        if tokefTxt.text == ""
        {
            x = 1
            reqTokef.text = NSLocalizedString("REQUIREFIELD", comment: "")
            reqTokef.hidden = false
        }
        else if checkValidityTokek(tokefTxt.text!) == false
        {
            x = 1
            reqTokef.text = NSLocalizedString("NOT_VALID", comment: "")
            reqTokef.hidden = false
        }
        if threeNumsTxt.text == ""
        {
            x = 1
            reqThreeNums.text = NSLocalizedString("REQUIREFIELD", comment: "")
            reqThreeNums.hidden = false
        }
        else if threeNumsTxt.text?.characters.count > 3
        {
            x = 1
            reqThreeNums.text = NSLocalizedString("NOT_VALID", comment: "")
            reqThreeNums.hidden = false
        }
        if tzTxt.text == ""
        {
            x = 1
            reqTz.text = NSLocalizedString("REQUIREFIELD", comment: "")
            reqTz.hidden = false
        }
        
        if x == 0
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let frontviewcontroller:UINavigationController = UINavigationController()
            
            let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
            let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
            
            frontviewcontroller.pushViewController(vc, animated: false)
            
            
            //initialize REAR View Controller- it is the LEFT hand menu.
            
            let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
            
            let mainRevealController = SWRevealViewController()
            
            mainRevealController.frontViewController = frontviewcontroller
            mainRevealController.rearViewController = rearViewController
            
            let window :UIWindow = UIApplication.sharedApplication().keyWindow!
            window.rootViewController = mainRevealController
        }
    }
    @IBOutlet var tblPaymentMethod: UITableView!
    
    @IBAction func btnCancel(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller:UINavigationController = UINavigationController()
        
        let storyboardCExist = UIStoryboard(name: "ClientExist", bundle: nil)
        let vc = storyboardCExist.instantiateViewControllerWithIdentifier("DefinationsClientViewController") as! DefinationsClientViewController
        
        frontviewcontroller.pushViewController(vc, animated: false)
        
        
        //initialize REAR View Controller- it is the LEFT hand menu.
        
        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        
        let mainRevealController = SWRevealViewController()
        
        mainRevealController.frontViewController = frontviewcontroller
        mainRevealController.rearViewController = rearViewController
        
        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        window.rootViewController = mainRevealController

    }
    // MARK: - Initial
   
    //==============================Initial=========================
    

    override func viewDidAppear(animated: Bool) {
        
        Colors.sharedInstance.addTopAndBottomBorderWithColor(UIColor.lightGrayColor(), width: 1, any: vieNumCart)
        Colors.sharedInstance.addBottomBorderWithColor(UIColor.lightGrayColor(), width: 1, any: vieTokef)
        Colors.sharedInstance.addBottomBorderWithColor(UIColor.lightGrayColor(), width: 1, any: vie3Nums)
        Colors.sharedInstance.addBottomBorderWithColor(UIColor.lightGrayColor(), width: 1, any: numCart)
        Colors.sharedInstance.addTopBorderWithColor(UIColor.lightGrayColor(), width: 1, any: viewHeader
        )
        let topBorder: UIView = UIView()
        topBorder.frame = CGRectMake(0, 0, self.view.frame.width*(8/9), 1)
    }

    override func viewWillLayoutSubviews() {
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.bringSubviewToFront(cnlBtn)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PaymentMethodViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        let topBorder: UIView = UIView()
        topBorder.backgroundColor = UIColor.lightGrayColor()
        topBorder.frame = CGRectMake(0, 0, self.view.frame.width, 1)
        
        self.view.backgroundColor=UIColor.blackColor().colorWithAlphaComponent(0.87)
        reqNumCard.hidden = true
        reqThreeNums.hidden = true
        reqTokef.hidden = true
        reqTz.hidden  = true
        
        numCardTxt.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        tokefTxt.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        threeNumsTxt.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        tzTxt.addTarget(self, action: #selector(UITextFieldDelegate.textFieldDidEndEditing(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    //MARK: - gestureRecognizer for dismissKeyboard
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
        dismissKeyboard()
        return false
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: - TextField
    //=========================TextField==============================
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch(textField){
        case numCardTxt:
            reqNumCard.hidden = true
        case tokefTxt:
            reqTokef.hidden = true
        case threeNumsTxt:
            reqThreeNums.hidden = true
        default:
            reqTz.hidden = true
        }
    }
    
    func checkValidityTokek(tokef:String)->Bool
    {
       // 08/16
        let badCharacters = NSCharacterSet.decimalDigitCharacterSet().invertedSet
        let tokefArr = tokef.componentsSeparatedByString("/")
        
        var date: String = tokefArr[0]
        var year: String = tokefArr[1]
        
        if tokefArr.count == 1// "אם אין "/
        {
            return false
        }

        else if date.characters.count > 2 || year.characters.count > 2// אם בחודש יותר מ2 ספרות
        {
            return false
        }
        if  date.rangeOfCharacterFromSet(badCharacters) != nil || year.rangeOfCharacterFromSet(badCharacters) != nil//אם התאריך או השנה אינם תקינים - אינם מכילים ספרות
        {
            return false
        }
        else if Int(date) > 12//אם החודש גדול מ-12 - לא תקין
        {
            return false
        }
        
        else
        {
            //אם התאריך או השנה קטן מ10, הוספתי 0
            if date.characters.count == 1
            {
                date = "0\(date)"
                
            }
            if year.characters.count == 1
            {
                year = "0\(year)"
            }
            tokefTxt.text = "\(date)/\(year)"
            return true
            
        }
    }
}
