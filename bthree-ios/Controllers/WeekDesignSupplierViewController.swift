//
//  WeekDesignSupplierViewController.swift
//  Bthere
//
//  Created by User on 1.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//
import UIKit
import EventKit
import EventKitUI

//ספק - תצוגת שבוע
class WeekDesignSupplierViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var delegate:clickToDayInWeekDelegate!
    //MARK: - Outlet
    @IBOutlet var btnDay1: UIButton!
    @IBOutlet var btnDay2: UIButton!
    @IBOutlet var btnDay3: UIButton!
    @IBOutlet var btnDay4: UIButton!
    @IBOutlet var btnDay5: UIButton!
    @IBOutlet var btnDay6: UIButton!
    @IBOutlet var btnDay7: UIButton!
    var  arrayDays: NSMutableArray = NSMutableArray()
    var  arrayButtons: Array<UIButton> = Array<UIButton>()
    var  arrayLabelsDayNum: Array<UILabel> = Array<UILabel>()
    var  arrayLabelsdate: Array<UILabel> = Array<UILabel>()
    var monthArray:Array<Int> = Array<Int>()
    var datesInWeekArray:Array<NSDate> = Array<NSDate>()//array of nsdate in week
    @IBOutlet weak var ingTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var imgCurrentDay: UIImageView!
    
    @IBOutlet weak var lblDay1: UILabel!
    
    @IBOutlet weak var lblDay2: UILabel!
    
    @IBOutlet weak var lblDay3: UILabel!
    
    @IBOutlet weak var lblDay4: UILabel!
    
    @IBOutlet weak var lblDay5: UILabel!
    
    @IBOutlet weak var lblDay6: UILabel!
    
    @IBOutlet weak var lblDay7: UILabel!
    
    
    @IBOutlet weak var lblDayOfWeek1: UILabel!
    
    @IBOutlet weak var lblDayOfWeek2: UILabel!
    
    @IBOutlet weak var lblDayOfWeek3: UILabel!
    
    @IBOutlet weak var lblDayOfWeek4: UILabel!
    
    @IBOutlet weak var lblDayOfWeek5: UILabel!
    
    @IBOutlet weak var lblDayOfWeek6: UILabel!
    
    @IBOutlet weak var lblDayOfWeek7: UILabel!
    
    
    @IBOutlet weak var lblDays: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    //MARK: - IBAction

    //enter to date in week
    @IBAction func btnEnterDateClick(sender: AnyObject) {
    
    let tag:Int = (sender as! UIButton).tag
    
    let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
    
    yearToday =  componentsCurrent.year
    monthToday = componentsCurrent.month
    dayToday = componentsCurrent.day
    let td:Int =  arrayDays[tag] as! Int

    componentsCurrent.day = td
    let dateSelected = calendar.dateFromComponents(componentsCurrent)
  
    Global.sharedInstance.currDateSelected = dateSelected!
    Global.sharedInstance.dateDayClick = dateSelected!
    
    

    delegate.clickToDayInWeek()

    }
    
    //כפתור הקודם (לא הבא)
    @IBAction func btnNext(sender: AnyObject) {
    
    //בלחיצה על הקודם מוצגים הימים בשבוע הקודם
    
    hasEvent = false
    let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
    
    let otherDate:NSDate = currentDate
    
    //show month for each day in week
    lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -1).description
    lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -2).description
    lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -3).description
    lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -4).description
    lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -5).description
    lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -6).description
    lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: -7).description
    
    currentDate =  Calendar.sharedInstance.reduceAddDay_Date(otherDate, reduce: day, add: -7)
    let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
    monthToday = componentsCurrent.month
    
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -1))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -2))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -3))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -4))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -5))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -6))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: -7))
    
    lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"
          var monthName:String = ""
    //get the month and day of week names - short
    
    if monthToday == 0
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
    }
    else
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
    }
    // - long
    //NSDateFormatter().monthSymbols[monthToday - 1]
    
    //let dayName = NSDateFormatter().weekdaySymbols[day]
    
    lblDate.text = "\(monthName) \(yearToday)"
    
    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
    
    //אתחול המערך של תאריכי השבוע
    datesInWeekArray = []
    for i in 0..<7
    {
    datesInWeekArray.append(
    Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
    }
    
    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
    
        var  isFindToday:Bool = false
        
    //בדיקה האם היום הנוכחי מוצג כעת
    for item in datesInWeekArray
    {
        
        let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)
        
        let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
        if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
        {
            isFindToday = true
        }
        
    }
    if isFindToday
    {
        imgCurrentDay.hidden = false
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
        UIView.animateWithDuration(1, animations: {
            self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                CGFloat(self.dayOfWeekToday))
        })
    }
    else
    {
        imgCurrentDay.hidden = true
    }
    //refresh event array
    setEventsArray(currentDate)
    setDateEnablity()
    collWeek.reloadData()
    
    }
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnPrevious: UIButton!
    
    //כפתור הבא (לא הקודם)
    @IBAction func btnPrevious(sender: AnyObject) {
    //בלחיצה על הבא מוצגים הימים בשבוע הבא
    
    hasEvent = false
    let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
    
    let otherDate:NSDate = currentDate
    
    //show month for each day in week
    lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 13).description
    lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 12).description
    lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 11).description
    lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 10).description
    lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 9).description
    lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 8).description
    lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 7).description
    
    currentDate =  Calendar.sharedInstance.reduceAddDay_Date(otherDate, reduce: day, add: 13)
    
    let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
    
    monthToday = componentsCurrent.month
    
    
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 13))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 12))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 11))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 10))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 9))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 8))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 7))
    
lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"
              var monthName:String = ""
    
    //get the month and day of week names - short
    if monthToday == 0
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
    }
    else
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
    }
    
    // - long
    //NSDateFormatter().monthSymbols[monthToday - 1]
    
    //let dayName = NSDateFormatter().weekdaySymbols[day]
    
    lblDate.text = "\(monthName) \(yearToday)"
    
    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
    
    //אתחול המערך של תאריכי השבוע
    datesInWeekArray = []
    for i in 0..<7
    {
    datesInWeekArray.append(
    Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
    }
    
    dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
    var  isFindToday:Bool = false
    
        //בדיקה האם היום הנוכחי מוצג כעת
    for item in datesInWeekArray
    {
        let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)
        
        let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
        if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
        {
            isFindToday = true
        }
        }
    
    if isFindToday
    {
        imgCurrentDay.hidden = false
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
        UIView.animateWithDuration(1, animations: {
            self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                CGFloat(self.dayOfWeekToday))
        })
    }
    else
    {
        imgCurrentDay.hidden = true
    }
    
    setEventsArray(currentDate)
    setDateEnablity()
    collWeek.reloadData()
    }
    
    @IBOutlet weak var collWeek: UICollectionView!
    
    //MARK: - Properties
    
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var arrHoursInt:Array<Int> = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    var arrHours:Array<String> = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"]
    
    var arrEventsCurrentDay:Array<EKEvent> = []
    var flag = false
    var hasEvent = false
    var currentDate:NSDate = NSDate()
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var dayOfWeekToday = 0
    
    
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSyncWithGoogelSupplier: eyeSynCheckBox!
//    @IBOutlet weak var btnSync: eyeSynCheckBox!
    //MARK: - Initial
    //func that get date to init the design by date
    func initDateOfWeek(date:NSDate)
    {
        datesInWeekArray = []
        currentDate = date
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        
        for i in 0..<7
        {
            datesInWeekArray.append(
                Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: i+1))
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imgCurrentDay.hidden = true

        
        if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true{
            btnSyncWithGoogelSupplier.isCecked = true
        }
        
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        
        hasEvent = false
              var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        collWeek.transform = scalingTransform
              setEventsArray(currentDate)
        setDate()
        arrayDays = [Int(lblDay1.text!)!,Int(lblDay2.text!)!,Int(lblDay3.text!)!,Int(lblDay4.text!)!,Int(lblDay5.text!)!,Int(lblDay6.text!)!,Int(lblDay7.text!)!]
        
        
        
        arrayButtons.append(btnDay1)
        arrayButtons.append(btnDay2)
        arrayButtons.append(btnDay3)
        arrayButtons.append(btnDay4)
        arrayButtons.append(btnDay5)
        arrayButtons.append(btnDay6)
        arrayButtons.append(btnDay7)
        
        
        arrayLabelsDayNum.append(lblDayOfWeek1)
        arrayLabelsDayNum.append(lblDayOfWeek2)
        arrayLabelsDayNum.append(lblDayOfWeek3)
        arrayLabelsDayNum.append(lblDayOfWeek4)
        arrayLabelsDayNum.append(lblDayOfWeek5)
        arrayLabelsDayNum.append(lblDayOfWeek6)
        arrayLabelsDayNum.append(lblDayOfWeek7)
        
        arrayLabelsdate.append(lblDay1)
        arrayLabelsdate.append(lblDay2)
        arrayLabelsdate.append(lblDay3)
        arrayLabelsdate.append(lblDay4)
        arrayLabelsdate.append(lblDay5)
        arrayLabelsdate.append(lblDay6)
        arrayLabelsdate.append(lblDay7)
        setDateEnablity()
        imgCurrentDay.hidden = true
        
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        monthArray = []
        
        Global.sharedInstance.getEventsFromMyCalendar()
        
        if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
        {
            btnSyncWithGoogelSupplier.isCecked = true
        }
        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        
        hasEvent = false
        
        
        dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
        var isFindToday:Bool = false
        for item in datesInWeekArray
        {
            let otherDay: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: item)
            
            
            let today: NSDateComponents = NSCalendar.currentCalendar().components([.Era, .Year, .Month, .Day], fromDate: NSDate())
            if today.day == otherDay.day && today.month == otherDay.month && today.year == otherDay.year
            {
                isFindToday = true
            }
            
        }
        
        if isFindToday
        {
            imgCurrentDay.hidden = false
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(NSDate())!
            UIView.animateWithDuration(1, animations: {
                self.ingTrailing.constant = /* self.ingTrailing.constant +*/ ((self.view.frame.width / 8) *
                    CGFloat(self.dayOfWeekToday))
            })
        }
        else{
            imgCurrentDay.hidden = true
        }
        
        setEventsArray(currentDate)
        setDate()
        arrayDays = [Int(lblDay1.text!)!,Int(lblDay2.text!)!,Int(lblDay3.text!)!,Int(lblDay4.text!)!,Int(lblDay5.text!)!,Int(lblDay6.text!)!,Int(lblDay7.text!)!]
        
        arrayButtons.append(btnDay1)
        arrayButtons.append(btnDay2)
        arrayButtons.append(btnDay3)
        arrayButtons.append(btnDay4)
        arrayButtons.append(btnDay5)
        arrayButtons.append(btnDay6)
        arrayButtons.append(btnDay7)
        
        
        arrayLabelsDayNum.append(lblDayOfWeek1)
        arrayLabelsDayNum.append(lblDayOfWeek2)
        arrayLabelsDayNum.append(lblDayOfWeek3)
        arrayLabelsDayNum.append(lblDayOfWeek4)
        arrayLabelsDayNum.append(lblDayOfWeek5)
        arrayLabelsDayNum.append(lblDayOfWeek6)
        arrayLabelsDayNum.append(lblDayOfWeek7)
        
        arrayLabelsdate.append(lblDay1)
        arrayLabelsdate.append(lblDay2)
        arrayLabelsdate.append(lblDay3)
        arrayLabelsdate.append(lblDay4)
        arrayLabelsdate.append(lblDay5)
        arrayLabelsdate.append(lblDay6)
        arrayLabelsdate.append(lblDay7)
        setDateEnablity()
        
    }
    
    func setDateEnablity()
    {
    for i in 0  ..< datesInWeekArray.count
    {
    if self.small(datesInWeekArray[i], rhs: NSDate())
    {
    (arrayButtons[6-i] as UIButton).enabled = false
    (arrayLabelsDayNum[6-i] as UILabel).textColor = Colors.sharedInstance.color7
    (arrayLabelsdate[6-i] as UILabel).textColor =  Colors.sharedInstance.color7
    }
    // - כדי שירענן את הכפתורים ולא ישאיר לפי הקודם
    else
    {
    (arrayButtons[6-i] as UIButton).enabled = true
    (arrayLabelsDayNum[6-i] as UILabel).textColor =  UIColor.whiteColor()
    (arrayLabelsdate[6-i] as UILabel).textColor =  UIColor.whiteColor()
    }
    }
    }
    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSyncWithGoogel(sender: eyeSynCheckBox) {
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
            collWeek.reloadData()
        }
        else
        {
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
            collWeek.reloadData()
        }
    }
    //MARK: - collectionView
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    

    }
    //this is function with long logic , please follow gently and carefully !
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        // cell of content in hours where is event show
        let cell:EventsWeekViewsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("EventsWeekViews",forIndexPath: indexPath) as! EventsWeekViewsCollectionViewCell
        
        // reset flags which view to show
        cell.IsToppTop = false
        cell.IsMiddleeTop = false
        cell.IsButtomTop = false
        cell.IsToppButtom = false
        cell.IsMiddleeButtom = false
        cell.IsButtomButtom = false
        
        cell.viewTopInTop.hidden = true
        cell.viewMiddleInTop.hidden = true
        cell.viewButtomInTop.hidden = true
        cell.viewButtominButtom.hidden = true
        cell.viewMiddleInButtom.hidden = true
        cell.viewTopInButtom.hidden = true
        
        cell.hasEvent =  false
        cell.txtviewDesc.text = ""
        cell.txtViewDescBottom.text = ""
        cell.viewBottom.backgroundColor = UIColor.clearColor()
        cell.viewTop.backgroundColor = UIColor.clearColor()
        // cell of hour in side of cell
        let cell1:HoursCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Hours",forIndexPath: indexPath) as! HoursCollectionViewCell
        
        
 
        cell.transform = scalingTransform
        cell1.transform = scalingTransform
     
        
        
        if indexPath.row == 0 || indexPath.row % 8 == 0
        {
            // set hours contex
            cell1.setDisplayData(arrHours[indexPath.row / 8])
            return cell1
        }
        else
        {
            
            hasEvent = false// reset
            
            dayOfWeekToday = Calendar.sharedInstance.getDayOfWeek(currentDate)!
            
            let curDate = Calendar.sharedInstance.reduceAddDay_Date(currentDate, reduce: dayOfWeekToday, add: indexPath.row % 8)
//refresh device event by curDate
            setEventsArray(curDate)
//אירועי מכשיר
            if Global.sharedInstance.isSyncWithGoogelCalendar
            {
                if hasEvent == true
                {
                    cell.hasEvent =  true
                    
                    if arrEventsCurrentDay.count > 0
                    {
                        for var eve in arrEventsCurrentDay
                        {
                            if eve.allDay
                            {
                                
                            }
                            
                            let componentsStart = calendar.components([.Hour, .Minute], fromDate: eve.startDate)
                            
                            let componentsEnd = calendar.components([.Hour, .Minute], fromDate: eve.endDate)
                            
                            let hourS = componentsStart.hour //hour start
                            let minuteS = componentsStart.minute// minute start
                            
                            let hourE = componentsEnd.hour//hour end
                            
                            let minuteE = componentsEnd.minute//minute end
                            
                            var hourS_Show:String = hourS.description
                            var hourE_Show:String = hourE.description
                            var minuteS_Show:String = minuteS.description
                            var minuteE_Show:String = minuteE.description
                            
                            if hourS < 10 //for single hour that show 09
                            {
                                hourS_Show = "0\(hourS)"
                            }
                            if hourE < 10
                            {
                                hourE_Show = "0\(hourE)"
                            }
                            if minuteS < 10
                            {
                                minuteS_Show = "0\(minuteS)"
                            }
                            if minuteE < 10
                            {
                                minuteE_Show = "0\(minuteE)"
                            }
                            
                            
                            if arrHoursInt[indexPath.row / 8] == hourS //in the start hour
                            {
                                var str = eve.title
                                
                                if eve.allDay
                                {
                                    str = ""
                                }
                                
                                if minuteS > 0// שעת התחלה חצי להדליק את התחתון
                                {

                                    cell.IsToppButtom = true
                                    cell.setDisplayData(1, isTop: false, description:str,descTop: false)
                                    
                                }
                                else//שעת התחלה 00 להדליק את העליון
                                {
                                    
                                    if hourE == hourS //event less hour set on top
                                    {
                                        cell.IsToppTop = true
                                        
                                        cell.setDisplayData(1, isTop: true, description: str,descTop: true)
                                        
                                    }
                                    else
                                    {
                                        if hourS == hourE - 1 && minuteE == 0
                                        {
                                            cell.IsButtomButtom = true
                                        }
                                        else
                                        {
                                            cell.IsMiddleeButtom = true
                                        }
                                        cell.IsToppTop = true

                                        cell.setDisplayData(2, isTop: false,description: str,descTop: true)
                                    }
                                }
                                
                            }
                            else if arrHoursInt[indexPath.row / 8] > hourS && arrHoursInt[indexPath.row / 8] < hourE //that time in event time
                            {
                                
                                if arrHoursInt[indexPath.row / 8] == 1 && eve.allDay
                                {
                                    
                                    cell.IsMiddleeButtom = true
                                    cell.IsToppTop = true
                                    
                                    cell.setDisplayData(2, isTop: false,description: eve.title,descTop: true)
                                }
                                else
                                {
                                    let hour = hourE - 1
                                    
                                    if arrHoursInt[indexPath.row / 8] ==  hour
                                        
                                    {
                                        if minuteE > 0 ||  eve.allDay
                                            // שעת סיום חצי
                                        {
                                            cell.IsMiddleeButtom = true
                                        }
                                        else
                                        {
                                            cell.IsButtomButtom = true
                                        }
                                    }
                                    else
                                    {
                                        cell.IsMiddleeButtom = true
                                    }
                                    cell.IsMiddleeTop = true
                                    cell.setDisplayData(2, isTop: false,description: "",descTop: true)//""
                                    
                                }
                                
                            }
                            else if arrHoursInt[indexPath.row / 8] == hourE //if is the finish hour
                            {
                                if minuteE > 0// שעת סיום חצי
                                {
                                    if eve.allDay
                                    {
                                        //
                                        cell.IsMiddleeTop = true
                                        cell.setDisplayData(2, isTop: false,description: "",descTop: true)
                                        
                                    }
                                    else
                                    {
                                        //
                                        cell.IsButtomTop = true
                                        cell.setDisplayData(1, isTop: true,description: "",descTop: true)//""
                                        
                                    }
                                    
                                    
                                }
                                else//שעת סיום 00
                                {

                                    cell.setDisplayData(0, isTop: false,description: "",descTop: true)//""
                                    
                                    
                                }
                                
                            }
                            if eve.allDay //the event in all the day
                            {
                                cell.IsToppTop = false
                                cell.IsMiddleeTop = false
                                cell.IsButtomTop = false
                                cell.IsToppButtom = false
                                cell.IsMiddleeButtom = false
                                cell.IsButtomButtom = false
                                
                                if arrHoursInt[indexPath.row / 8] == 0
                                {
                                    cell.IsButtomButtom = true
                                    cell.IsMiddleeTop = true
                                    cell.setDisplayData(2, isTop: false, description:"",descTop: true)
                                    
                                }
                                else if arrHoursInt[indexPath.row / 8] == 1
                                    
                                {
                                    cell.IsToppTop = true
                                    cell.IsMiddleeButtom = true
                                    cell.setDisplayData(2, isTop: false, description:eve.title,descTop: true)
                                }
                                else
                                {
                                    cell.IsMiddleeTop = true
                                    cell.IsMiddleeButtom = true
                                    cell.setDisplayData(2, isTop: false, description:"",descTop: true)
                                }
                            }
                        }
                        
                    }
                    
                    
                }
            }
            else //not need to show Event
            {
                
                cell.setDisplayData(0, isTop: false,description: "",descTop: true)
                
                return cell
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    return 192//8*24
    
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    
    return CGSize(width: view.frame.size.width / 8, height:  view.frame.size.width / 9)
    
    }
    
    //MARK: - ScrollView
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
    
    //       collWeek.reloadData()
    // flag = true
    
    }
    
    func setDate()
    {
    //the day of week from date - (int)
    let day:Int = Calendar.sharedInstance.getDayOfWeek(currentDate)! - 1
    
    let otherDate:NSDate = currentDate
    
    //show month for each day in week
    lblDay1.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 6).description
    lblDay2.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 5).description
    lblDay3.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 4).description
    lblDay4.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 3).description
    lblDay5.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 2).description
    lblDay6.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 1).description
    lblDay7.text = Calendar.sharedInstance.reduceAddDay(otherDate, reduce: day, add: 0).description
    
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 6))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 5))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 4))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 3))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 2))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 1))
    monthArray.append(Calendar.sharedInstance.getMonth(otherDate, reduce: day, add: 0))
    


    lblDays.text = "\(lblDay7.text!) - \(lblDay1.text!)"//change 13.10.2016
    var monthName:String = ""

    //get the month and day of week names - short
    if monthToday == 0
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday]
    }
    else
    {
    monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthToday - 1]
    }
    let components = calendar.components([.Day, .Month, .Year], fromDate: currentDate)
    
    yearToday =  components.year
    // - long
    //NSDateFormatter().monthSymbols[monthToday - 1]
    //let dayName = NSDateFormatter().weekdaySymbols[day]
    
    lblDate.text = "\(monthName) \(yearToday)"
    }
    //set device event in array
    func setEventsArray(today:NSDate)
    {
        arrEventsCurrentDay = []
        for item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: today)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            let dayEvent = componentsEvent.day
            
            if yearEvent == yearToday && monthEvent == monthToday && dayEvent == dayToday
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
    //check which date is small
    internal func small(lhs: NSDate, rhs: NSDate) -> Bool {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        let isToday:Bool = calendar.isDateInToday(lhs);
        if isToday
        {
            return false
        }
        else
        {
            return lhs.compare(rhs) == .OrderedAscending
        }
    }
    // if eye design is set on  need to show device event
    func showSync()
    {
        if btnSyncWithGoogelSupplier.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSyncWithGoogelSupplier.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true
            collWeek.reloadData()
        }
        else
        {
            btnSyncWithGoogelSupplier.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
            collWeek.reloadData()
        }
  
    }
    
}
