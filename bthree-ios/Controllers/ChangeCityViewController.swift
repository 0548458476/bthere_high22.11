//
//  ChangeCityViewController.swift
//  Bthere
//
//  Created by User on 11.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

//דף שנה מיקום- מתוצאות חיפוש
class ChangeCityViewController: UIViewController,UITextFieldDelegate, SPGooglePlacesAutocompleteQueryDelegate,UIGestureRecognizerDelegate {
    
    var delegate:ReloadResultsDelegate!=nil
    var generic:Generic = Generic()
    
    @IBOutlet weak var btnClose: UIButton!

    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var txtSearch: SuggestiveTextField!
    
    @IBOutlet weak var tblSearch: UITableView!
    
    @IBOutlet weak var lblChangeLocation: UILabel!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBAction func btnSearch(sender: AnyObject) {
        Global.sharedInstance.dicSearchProviders["nvCity"] = txtSearch.text
        Global.sharedInstance.dicSearchProviders["nvlong"] = nil
        Global.sharedInstance.dicSearchProviders["nvlat"] = nil
        
        if Global.sharedInstance.whichSearchTag == 1
        {
            generic.showNativeActivityIndicator(self)
            
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.SearchByKeyWord(Global.sharedInstance.dicSearchProviders, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                        Global.sharedInstance.dicResults = []
                        self.delegate.ReloadResultsDelegate()
                        self.dismissViewControllerAnimated(false, completion: nil)
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        self.delegate.ReloadResultsDelegate()
                        self.dismissViewControllerAnimated(false, completion: nil)
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                })
            }
        }
        else if Global.sharedInstance.whichSearchTag == 2
        {
            generic.showNativeActivityIndicator(self)
            if Reachability.isConnectedToNetwork() == false
            {
                generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
            }
            else
            {
                api.sharedInstance.SearchProviders(Global.sharedInstance.dicSearchProviders, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                        Global.sharedInstance.dicResults = []
                        self.delegate.ReloadResultsDelegate()
                        self.dismissViewControllerAnimated(false, completion: nil)
                    }
                    else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                    {
                        Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                        self.delegate.ReloadResultsDelegate()
                        self.dismissViewControllerAnimated(false, completion: nil)
                    }
                    self.generic.hideNativeActivityIndicator(self)
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        self.generic.hideNativeActivityIndicator(self)
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                })
            }
        }

    }
    
    let googlePlacesAutocomplete = SPGooglePlacesAutocompleteQuery()
    var listAutocompletePlace:[SPGooglePlacesAutocompletePlace] = []
    var coordinatePackage:CLLocationCoordinate2D!
    var isSearch = false
    var adressResult:[String] = []//מכיל את הכתובות להשלמה
    var searchResults: [String]!

    //MARK: initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblChangeLocation.text = NSLocalizedString("CHANGE_LOCATION", comment: "")
        txtSearch.placeholder = NSLocalizedString("CHANGE_LOCATION", comment: "")
        
        googlePlacesAutocomplete.delegate = self
        txtSearch.delegate = self
        self.searchResults = Array()
        tblSearch.separatorStyle = UITableViewCellSeparatorStyle.None
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillLayoutSubviews() {
        let bounds = UIScreen.mainScreen().bounds
        
        let width = bounds.size.width
        let height = bounds.size.height
        let y = bounds.origin.y
        self.view.superview!.bounds = CGRect(x:0,y:-40,width:width,height: height)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- tableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.adressResult.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SearchTableViewCell = tableView.dequeueReusableCellWithIdentifier("SearchTableViewCell") as!SearchTableViewCell

        cell.lblSubject.text = self.adressResult[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        txtSearch.text = (tableView.cellForRowAtIndexPath(indexPath) as! SearchTableViewCell).lblSubject.text
        
        tableView.hidden = true
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        if self.adressResult.count > 4
        {
        return tableView.frame.size.height / CGFloat(self.adressResult.count)
        }
        return 55
    }


//MARK: - textField


    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtSearch
        {
            Global.sharedInstance.isAddressCity = true
            googlePlacesAutocomplete.input = textField.text! + string
            googlePlacesAutocomplete.fetchPlaces()
        }
        return true
    }

        //-MARK: SPGooglePlacesAutocompleteQueryDelegate
    
    
        func googlePlaceReload(listPlaces:[SPGooglePlacesAutocompletePlace])
        {
            if isSearch{
                googlePlacesAutocomplete.googleGeocoded(referenceToGeocoded:listPlaces[0].reference)
                isSearch = false
            }
            else{
                var placesName:[String] = []
                var placesId:[String] = []
                
                for place in listPlaces{
                    placesName += [place.name]
                    placesId += [place.placeId]
                }
                
                //st
                
                adressResult = placesName
                
                listAutocompletePlace=listPlaces
                
                tblSearch.hidden = false
                tblSearch.reloadData()
                
//                txtSearch.setSuggestions(placesName)
//                txtSearch.setSuggestionsPlacesId(placesId)
//                txtSearch.matchStrings(txtSearch.text)
//                txtSearch.showSuggestionTableView()

            }
        }
        
        func googlePlaceGeocoded(latitude:Double, longitude:Double)
        {

            txtSearch.text = ""
        }
        
        func googlePlaceReverseGeocode(address: String) {

                txtSearch.text = address


        }
        
        func googlePlaceReverseGeocode(address: String , country: String ,city: String)
        {
            
        }
    
    func dismissKeyBoard()
    {
        self.view.endEditing(true)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(tblSearch)) {
            
            return false
        }
        return true
    }
    

}
