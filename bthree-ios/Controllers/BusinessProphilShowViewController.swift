//
//  BusinessProphilShowViewController.swift
//  Bthere
//
//  Created by User on 3.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import AssetsLibrary
import CoreLocation
import CoreMotion
import Foundation
import MapKit

class BusinessProphilShowViewController: UIViewController,GMSMapViewDelegate,NSURLConnectionDelegate,CLLocationManagerDelegate {

    //MARK: - Properties
    //===========Properties=========
    
    var delegate:getProviderServicesForSupplierDelegate!=nil
    
    var latitude:CLLocationDegrees = 0
    var longitude:CLLocationDegrees = 0
    var isOnChosenAddress = true
    var longMessage:String = ""
    
    var topSaleOpen:NSLayoutConstraint!
    var topSaleClose:NSLayoutConstraint!
    var DayFlagArr:Array<Int> = [0,0,0,0,0,0,0]
    
    var hourShow:String = ""
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var TopSale: NSLayoutConstraint!
    var ttopSale:NSLayoutConstraint?
    
    //////----top view
    
    
    @IBOutlet weak var viewTop_Orange_1: UIView!
    @IBOutlet weak var lblNmeBuisness: UILabel!
    @IBOutlet weak var lblSlogenBuisness: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewOfLogo: UIView!
    
    ////view 2----------
    @IBOutlet weak var view_Black_2: UIView!
    
    //עפולה, 2 ק״מ ממיקומך
    @IBOutlet weak var imgWaze: UIImageView!
    
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var lblNumKM: UILabel!
    
    @IBOutlet weak var lblKMFromYou: UILabel!
    
    // דירוג: 8.9 | 32 מדרגים
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var lblNumRuting: UILabel!
    
    @IBOutlet weak var lblNumVoters: UILabel!
    
    @IBOutlet weak var lblVoters: UILabel!
    
    ////view 3----------
    
    
    @IBOutlet weak var imgLightBlue3: UIImageView!
    
    @IBOutlet weak var view_lightBlue_3: UIView!
    
    @IBOutlet weak var lblHoursWork: UILabel!
    
    @IBOutlet weak var txtViewHoursWork: UITextView!
    
    @IBAction func btnLikeF(sender: AnyObject) {
    }
    
    @IBOutlet weak var btnLikeF: UIButton!
    
    @IBOutlet weak var btnShareF: UIButton!
    
    @IBAction func btnShareF(sender: AnyObject) {
    }
    
    @IBOutlet weak var btnInviteTor: UIButton!
    //קבע תור
    @IBAction func btnInviteTor(sender: AnyObject) {
       
    }
    //-----googleMaps----------
    @IBOutlet weak var viewParent_Map: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    
    
    
    @IBOutlet weak var addressLabel: UILabel!
    
    //About
    @IBOutlet weak var viewAbout_4: UIView!
    
    @IBOutlet weak var lblAbout: UILabel!
    
    @IBAction func btnReadMore(sender: AnyObject) {
        Alert.sharedInstance.showAlert(longMessage, vc: self)
    }
    
    @IBOutlet weak var btnReadMore: UIButton!
    
    //   @IBOutlet weak var lblTextAbout: UILabel!
    
    // to show the text
    @IBOutlet weak var txtViewAbout: UITextView!
    
    
    @IBOutlet weak var view_lightBlue_5: UIView!
    
    @IBOutlet weak var imgLightBlue5: UIImageView!
    
    @IBOutlet weak var viewUnderSale: UIView!

    @IBOutlet weak var topToSale: NSLayoutConstraint!

    //Opinion
    @IBOutlet weak var view_Opinion_6: UIView!
    
    @IBOutlet weak var lblOpinion: UILabel!
    
    
    @IBOutlet weak var btnMakeTor: UIButton!
    //לקביעת תור
    @IBAction func btnMakeTor(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate.getProviderServicesForSupplierFunc()
    }
    
    @IBOutlet weak var view_thank_7: UIButton!
    @IBOutlet weak var view_lightBlue8: UIView!
    
    
    @IBOutlet weak var imgLightBlue8: UIImageView!
    
    var buisnessName = ""
    var address = ""
    var locationManager = CLLocationManager()
    var key = ""
    //מערך זה מכיל את שעות פעילות העסק ממוין לפי ימים
   // (כל יום יכול להכיל שתי אוביקטים של שעות פעילות)
    var arrWorkingHours:Array<Array<objWorkingHours>> = [Array<objWorkingHours>(),Array<objWorkingHours>(),Array<objWorkingHours>(),Array<objWorkingHours>(),Array<objWorkingHours>(),Array<objWorkingHours>(),Array<objWorkingHours>()]
    
    //MARK: - initial
    override func viewDidLoad() {
        super.viewDidLoad()

        ttopSale = TopSale
        
        btnInviteTor.setTitle(NSLocalizedString("MAKE_APPOINTMENT", comment: ""), forState: .Normal)
        
        key="AIzaSyBGjEDOd6MtNtfTv76CzIp_WyMxzvj_KJg"
        lblSlogenBuisness.text = ""
        //imgLightBlue3.image = UIImage(named: "IMG_05072016_131013.png")
        //imgLightBlue8.image = UIImage(named: "IMG_05072016_131024.png")
        btnReadMore.hidden = true
        lblAbout.text = NSLocalizedString("ABOUT", comment: "")
        lblHoursWork.text = NSLocalizedString("HOURS_WORK", comment: "")
        
        lblOpinion.text = NSLocalizedString("OPINION", comment: "")
        btnMakeTor.setTitle(NSLocalizedString("FOR_TOR", comment: ""), forState: .Normal)
        view_thank_7.titleLabel?.font = UIFont(name: "OpenSansHebrew-Light", size: 16)

        //אשמח לראותך משה - בינתיים הורדתי ע״פ הדרישה, אח״כ יהיה שם הלקוח...
        
        //        let dicName:Dictionary<String,String> = Global.sharedInstance.defaults.valueForKey("currentClintName") as! Dictionary<String,String>
        //        dicName["nvClientName"]
        //
        //        if dicName["nvClientName"] != ""
        //        {
        //        view_thank_7.setTitle((NSLocalizedString("THANK", comment: "") + " - " + dicName["nvClientName"]!), forState: .Normal)
        //        }
        //        else
        //        {
                view_thank_7.setTitle(NSLocalizedString("THANK", comment: ""), forState: .Normal)
        //        }

        view_thank_7.enabled = false
        
        
        ////show a map:
        viewMap.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        

        //imgLogo.image = UIImage(named: "clients@x1.png")
 
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if Global.sharedInstance.providerBuisnessProfile.nvLong != ""
        {
            self.longitude = Double(Global.sharedInstance.providerBuisnessProfile.nvLong)!
        }
        if Global.sharedInstance.providerBuisnessProfile.nvLat != ""
        {
            self.latitude = Double(Global.sharedInstance.providerBuisnessProfile.nvLat)!
        }
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(self.latitude,longitude: self.longitude, zoom: 15)
        self.viewMap.camera = camera
        self.viewMap.animateToZoom(18)
 
        
        //פונקציה המקבלת כתובת בסטרינג ומחזירה את קווי האורך והרוחב שלה
        //לא עבד תקין לכל הכתובות, אז קיבלנו את קווי האורך והרוחב מהשרת.
        
//        let location: String = address
//        let geocoder: CLGeocoder = CLGeocoder()

//        geocoder.geocodeAddressString(location,completionHandler: {
//            (placemarks: [CLPlacemark]?, error: NSError?) -> Void in
        
//            if (placemarks?.count > 0) {
//                
//                self.latitude = (placemarks?.last?.location?.coordinate.latitude)!
//                self.longitude = (placemarks?.last?.location?.coordinate.longitude)!
//                
//                self.viewMap.clear()
//                let marker = GMSMarker()
//                marker.position = CLLocationCoordinate2DMake(self.latitude, self.longitude)
//                self.viewMap.selectedMarker = marker
//                marker.map = self.viewMap
//                
//                let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(self.latitude, longitude: self.longitude, zoom: 15)
//                self.viewMap.camera = camera
//                self.viewMap.animateToZoom(18)
//                
//            }
//            else
//            {
//                let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(0.0,longitude: 0.0, zoom: 15)
//                self.viewMap.camera = camera
//                self.viewMap.animateToZoom(18)
//            }
//        }
//        )

        imgLogo.contentMode = .ScaleAspectFit
        imgLightBlue3.contentMode = .ScaleAspectFit
        imgLightBlue5.contentMode = .ScaleAspectFit
        imgLightBlue8.contentMode = .ScaleAspectFit
        
        //המרת התמונה של הלוגו מהשרת
        var dataDecoded:NSData = NSData(base64EncodedString: (
            Global.sharedInstance.providerBuisnessProfile.nvILogoImage), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        var decodedimage:UIImage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            
            UIGraphicsBeginImageContext(imgLogo.frame.size)
            decodedimage.drawInRect(imgLogo.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            imgLogo.image = image
        }
        else
        {
             imgLogo.image = UIImage(named: "clients@x1.png")
        }
        //המרת התמונה הראשונה של העסק
        dataDecoded = NSData(base64EncodedString: (Global.sharedInstance.providerBuisnessProfile.nvHeaderImage), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        decodedimage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!

            UIGraphicsBeginImageContext(imgLightBlue3.frame.size)
            decodedimage.drawInRect(imgLightBlue3.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imgLightBlue3.image = image
        }
        else
        {
            imgLightBlue3.image = UIImage(named: "IMG_05072016_131013.png")
        }
        
        //המרת התמונה של המבצע
        dataDecoded = NSData(base64EncodedString: (Global.sharedInstance.providerBuisnessProfile.nvCampaignImage), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        decodedimage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            var  heightViewSale: NSLayoutConstraint?
            
            //במקרה שיש תמונה למבצע - צריך להציג את הויו של המבצע...
            ttopSale = TopSale
            view.removeConstraint(ttopSale!)
            let verticalConstraint = NSLayoutConstraint(item: view_Opinion_6, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: view_Opinion_6, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            TopSale = verticalConstraint
            view.addConstraint(verticalConstraint)

            decodedimage = UIImage(data: dataDecoded)!
            
            UIGraphicsBeginImageContext(imgLightBlue5.frame.size)
            decodedimage.drawInRect(imgLightBlue5.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imgLightBlue5.image = image
            
            scrollView.contentSize.height = 1400
        }
        else
        {
            imgLightBlue5.image = UIImage()
            
            //במקרה שאין תמונה למבצע - צריך להסתיר את הויו של המבצע...
            ttopSale = TopSale
            view.removeConstraint(ttopSale!)
            let verticalConstraint = NSLayoutConstraint(item: view_Opinion_6, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: view_lightBlue_5, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            TopSale = verticalConstraint
            view.addConstraint(verticalConstraint)
            
            scrollView.contentSize.height = 1400 * 0.85
        }
        
        //המרת התמונה האחרונה
        dataDecoded = NSData(base64EncodedString: (Global.sharedInstance.providerBuisnessProfile.nvFooterImage), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        decodedimage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            
            UIGraphicsBeginImageContext(imgLightBlue8.frame.size)
            decodedimage.drawInRect(imgLightBlue8.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imgLightBlue8.image = image
        }
        else
        {
            imgLightBlue8.image = UIImage(named: "IMG_05072016_131024.png")
        }
        
        lblSlogenBuisness.text = Global.sharedInstance.providerBuisnessProfile.nvSlogen
        
        txtViewAbout.text = Global.sharedInstance.providerBuisnessProfile.nvAboutComment
        
        lblNmeBuisness.text = buisnessName
        
        if lblNmeBuisness.text?.characters.count > 24
        {
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
            {
                lblNmeBuisness.font = UIFont(name: "OpenSansHebrew-Bold", size: 13)
            }
            else
            {
                lblNmeBuisness.font = UIFont(name: "OpenSansHebrew-Bold", size: 14)
            }
        }

        //self.fetchPlaces()
        txtViewAbout.editable = false
        txtViewHoursWork.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
        
        viewMap.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        lblCity.text = address
        
        let workingHours:objWorkingHours = objWorkingHours()
        
        arrWorkingHours = workingHours.sortHoursArrayByDays(Global.sharedInstance.providerBuisnessProfile.ProviderWorkingHoursObj)
        
        
        DayFlagArr = [0,0,0,0,0,0,0]
        hourShow = ""
        txtViewHoursWork.text = ""
        
        for i in 0 ..< 7
        {
            if arrWorkingHours[i].count != 0
            {
                if DayFlagArr[i] != 1
                {
                    //אם עובד ביום זה שתי משמרות
                    if arrWorkingHours[i].count == 2
                    {
                        if arrWorkingHours[i][0].nvFromHour != "" && !(arrWorkingHours[i][0].nvFromHour == "00:00:00" && arrWorkingHours[i][0].nvToHour == "00:00:00" && arrWorkingHours[i][1].nvFromHour == "00:00:00" && arrWorkingHours[i][1].nvToHour == "00:00:00")
                        {
                            for j in 0 ..< 7
                            {
                                if arrWorkingHours[j].count == 2
                                {
                                    if arrWorkingHours[i][0].nvFromHour == arrWorkingHours[j][0].nvFromHour && arrWorkingHours[i][0].nvToHour == arrWorkingHours[j][0].nvToHour && arrWorkingHours[i][1].nvFromHour == arrWorkingHours[j][1].nvFromHour && arrWorkingHours[i][1].nvToHour == arrWorkingHours[j][1].nvToHour
                                    {
                                        hourShow = "\(hourShow) \(convertDays(j)),"
                                        DayFlagArr[j] = 1
                                    }
                                }
                            }
                            hourShow = String(hourShow.characters.dropLast())
                            hourShow = "\(hourShow) - "
                            
                            
                            if arrWorkingHours[i].count != 0
                            {
                                hourShow = "\(hourShow) \(cutHour(arrWorkingHours[i][0].nvFromHour))-\(cutHour(arrWorkingHours[i][0].nvToHour)), \(cutHour(arrWorkingHours[i][1].nvFromHour))-\(cutHour(arrWorkingHours[i][1].nvToHour)),\n"
                            }
                        }
                    }
                        //אם עובד ביום זה משמרת אחת
                    else if arrWorkingHours[i].count == 1
                    {
                        if arrWorkingHours[i][0].nvFromHour != "" && !(arrWorkingHours[i][0].nvFromHour == "00:00:00" && arrWorkingHours[i][0].nvToHour == "00:00:00")
                        {
                            for j in 0 ..< 7
                            {
                                if arrWorkingHours[j].count == 1
                                {
                                    if arrWorkingHours[i][0].nvFromHour == arrWorkingHours[j][0].nvFromHour && arrWorkingHours[i][0].nvToHour == arrWorkingHours[j][0].nvToHour
                                    {
                                        hourShow = "\(hourShow) \(convertDays(j)),"
                                        DayFlagArr[j] = 1
                                    }
                                }
                            }
                            hourShow = String(hourShow.characters.dropLast())
                            hourShow = "\(hourShow) - "
                            
                            
                            if arrWorkingHours[i].count != 0
                            {
                                hourShow = "\(hourShow) \(cutHour(arrWorkingHours[i][0].nvFromHour))-\(cutHour(arrWorkingHours[i][0].nvToHour)),\n"
                            }
                        }
                    }
                }
            }
        }
        hourShow = String(hourShow.characters.dropLast())

        
        txtViewHoursWork.text = "\(NSLocalizedString("HOURS_ACTIVITY", comment: "")):\n\(hourShow)"
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblCity.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
            lblRating.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
            lblVoters.font = UIFont(name: "OpenSansHebrew-Regular", size: 14)
           
            
            lblOpinion.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblAbout.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
            lblHoursWork.font = UIFont(name: "OpenSansHebrew-Bold", size: 19)
                    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - GoogleMaps
    //==================GoogleMaps===================
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 3
        if status == .AuthorizedWhenInUse {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            viewMap.myLocationEnabled = true
            viewMap.settings.myLocationButton = true
            
            viewMap.animateToZoom(18)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            viewMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            if let address = response?.firstResult() {
                
                let lines = address.lines!// as! [String]
                self.addressLabel.text = lines.joinWithSeparator("\n")
                
                //*********
                let labelHeight = self.addressLabel.intrinsicContentSize().height
                self.viewMap.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,bottom: labelHeight, right: 0)
                
                UIView.animateWithDuration(0.25) {
                    self.view.layoutIfNeeded()
                }
            }
            else
            {
                self.googlePlaceGeocoded(self.latitude, longitude: self.longitude)
            }
        }
    }

    func didTapMyLocationButtonForMapView(mapView: GMSMapView) -> Bool {
        
        self.googlePlaceGeocoded(self.latitude, longitude: self.longitude)
        isOnChosenAddress = true
        return true
    }
    
    var data: NSMutableData?
    var connectionAutocomplete = NSURLConnection()
    
    
//    func fetchPlaces(){
//        data = NSMutableData()  // Declare Globally
//        let urlPath: String = googleURLString()
//        let url: NSURL = NSURL(string: urlPath)!
//        let request: NSURLRequest = NSURLRequest(URL: url)
//        connectionAutocomplete = NSURLConnection(request: request, delegate: self, startImmediately: true)!
//        connectionAutocomplete.start()
//        
//        
//    }
    
    
//    func googleURLString() ->String{
//        var url:String = ""
//        url = NSString(format:"https://maps.googleapis.com/maps/api/place/details/json?input=bar&placeid=%@&key=%@",Global.sharedInstance.placeIdForMap,key) as String
//        return url
//    }
    
    
    // MARK:NSURLConnectionDelegate
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.data?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!){
        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        do {
            let jsonResult: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
            
            if let result = jsonResult.objectForKey("result") as? Dictionary<String, AnyObject> {
                if let geometry = result["geometry"] as? Dictionary<String, AnyObject> {
                    if let location = geometry["location"] as? Dictionary<String, Double> {
                        if let lat = location["lat"]{
                            latitude = location["lat"]!
                            if let lng = location["lng"]{
                                longitude = location["lng"]!

                                self.googlePlaceGeocoded(latitude, longitude: longitude)
                            }
                        }
                    }
                }
            }
            return
        }
        catch (_) {
            //here you can get access to all of the errors that occurred when trying to serialize
        }
    }

    func googlePlaceGeocoded(latitude:Double, longitude:Double)
    {
        viewMap.clear()
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        viewMap.selectedMarker = marker
        marker.map = viewMap
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 15)
        viewMap.camera = camera
        viewMap.animateToZoom(18)
    }
    
    func convertDays(day:Int) -> String {
        switch day {
        case 0:
            return NSLocalizedString("SUNDAY", comment: "")
        case 1:
            return NSLocalizedString("MONDAY", comment: "")
        case 2:
            return NSLocalizedString("TUESDAY", comment: "")
        case 3:
            return NSLocalizedString("WEDNSDAY", comment: "")
        case 4:
            return NSLocalizedString("THIRTHDAY", comment: "")
        case 5:
            return NSLocalizedString("FRIDAY", comment: "")
        case 6:
            return NSLocalizedString("SHABAT", comment: "")
        default:
            return NSLocalizedString("SUNDAY", comment: "")
        }
    }
    
    func cutHour(hour:String) -> String {
        var fixedHour = String(hour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        return fixedHour
    }

}
