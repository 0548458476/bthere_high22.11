//
//  MonthDesignSupplierViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/14/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
//ספק קיים- תצוגת חודש יומן
class MonthDesignSupplierViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    
    @IBOutlet weak var tblWorkers: UITableView!//
    
    @IBOutlet weak var viewOpenTblWorkers: UIView!
    
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSyn: eyeSynCheckBox!
   
    @IBAction func btnSyncWithGoogel(sender: eyeSynCheckBox) {
        
        if sender.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true//משתנה גלובלי המסמל האם יש סנכרון עם היומן האישי ביומן של הספק
        }
        else
        {
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
        }
        i = 1
        collDays.reloadData()

    }
 
    @IBAction func btnOpenWorkers(sender: AnyObject) {
        
        if tblWorkers.tag == 0
        {
            tblWorkers.hidden = false
            tblWorkers.tag = 1
        }
        else
        {
            tblWorkers.hidden = true
            tblWorkers.tag = 0
        }
    }
    
    
    
    @IBOutlet weak var lblWorkerName: UILabel!
    
    var arrayWorkers:Array<String> = ["לא משנה לי","איש צוות 1","איש צוות 2","איש צוות 3"]
    
    

    private let kKeychainItemName = "Google Calendar API"
    private let kClientID = "284147586677-69842kmfbfll1dmec57c9gklqnpa5n2u.apps.googleusercontent.com"
    
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    //    private let scopes = [kGTLAuthScopeCalendarReadonly]
    //
    //    private let service = GTLServiceCalendar()
    let output = UITextView()
    
    //    var gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
    //    //NSCalendarIdentifierGregorian
    //    var hebrew: NSCalendar = NSCalendar(calendarIdentifier: NSHebrewCalendar)!
    //    //NSCalendarIdentifierHebrew
    
    
    var dateFormatter = NSDateFormatter()
    
    
    @IBOutlet weak var lblDayOfWeek1: UILabel!
    
    @IBOutlet weak var lblDayOfWeek2: UILabel!
    
    @IBOutlet weak var lblDayOfWeek3: UILabel!
    
    @IBOutlet weak var lblDayOfWeek4: UILabel!
    
    @IBOutlet weak var lblDayOfWeek5: UILabel!
    
    @IBOutlet weak var lblDayOfWeek6: UILabel!
    
    @IBOutlet weak var lblDayOfWeek7: UILabel!
    
    @IBOutlet var collDays: UICollectionView!
    
    var today: NSDate = NSDate()
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var arrEventsCurrentDay:Array<EKEvent> = []
    //    var monthArray:Array<String> = [""]
    var days:Array<Int> = []
    var numDaysInMonth:Int = 0
    var dateFirst:NSDate = NSDate()
    var dayInWeek:Int = 0
    var i = 1
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var hasEvent = false
   // var moneForBackColor = 1
    let calendar = NSCalendar.currentCalendar()
    
    //MARK: - Initial
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        today = NSDate()
        
        Global.sharedInstance.isSyncWithGoogelCalendarSupplier = Global.sharedInstance.currentUser.bIsGoogleCalendarSync
        
        if Global.sharedInstance.currentUser.bIsGoogleCalendarSync == true
        {
            btnSyn.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSyn.isCecked = false
            i = 1
            collDays.reloadData()
        }

        lblDayOfWeek1.text = NSDateFormatter().veryShortWeekdaySymbols[0]
        lblDayOfWeek2.text = NSDateFormatter().veryShortWeekdaySymbols[1]
        lblDayOfWeek3.text = NSDateFormatter().veryShortWeekdaySymbols[2]
        lblDayOfWeek4.text = NSDateFormatter().veryShortWeekdaySymbols[3]
        lblDayOfWeek5.text = NSDateFormatter().veryShortWeekdaySymbols[4]
        lblDayOfWeek6.text = NSDateFormatter().veryShortWeekdaySymbols[5]
        lblDayOfWeek7.text = NSDateFormatter().veryShortWeekdaySymbols[6]
        
        // Menu delegate [Required]
        // Do any additional setup after loading the view.
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        //getFirstDay(NSDate())
        if language == "he"
        {
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransformMakeScale(-1, 1)
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
            collDays.transform = scalingTransform
        }
        dateFormatter.dateFormat = "dd/MM/yyyy"
        changeLblDate()
        setEventsArray()
      
        tblWorkers.hidden = true
        tblWorkers.tag = 0
        
        let tap = UITapGestureRecognizer(target:self, action:#selector(openWorkers))
        viewOpenTblWorkers.userInteractionEnabled = true
        viewOpenTblWorkers.addGestureRecognizer(tap)
        
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)

    }
    
    
    override func viewDidAppear(animated: Bool) {//כדי שאם שינו בתצוגה אחרת את הפלאג זה ישתנה בתצוגה זו גם בעת הלחיצה
        
        Global.sharedInstance.getEventsFromMyCalendar()
        
        Global.sharedInstance.isSyncWithGoogelCalendarSupplier = Global.sharedInstance.currentUser.bIsGoogleCalendarSync
        
        if Global.sharedInstance.currentUser.bIsGoogleCalendarSync == true
        {
            btnSyn.isCecked = true
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSyn.isCecked = false
            i = 1
            collDays.reloadData()
        }
        
        today = NSDate()
        
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(NSDate())//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(NSDate())//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!//מחזירה את היום בשבוע של הראשון בחודש
        
        
//        if Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
//        {
            i = 1
            collDays.reloadData()
//        }
//        else
//        {
//            i = 1
//            collDays.reloadData()
//        }
           changeLblDate()
    }

    @IBOutlet var lblHebrewDate: UILabel!
    @IBOutlet var lblCurrentDate: UILabel!
    
    var shouldShowDaysOut = true
    var animationFinished = true
    var currentDate:NSDate = NSDate()
    
    // click on prev date
    @IBAction func btnBefore(sender: UIButton) {
        //currentDate =
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.removeMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
      //  moneForBackColor = 1
        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        today = Mycalendar.dateByAddingUnit(.Month, value: -1, toDate: today, options: [])!
        
        changeLblDate()
    }
    // click on next date
    @IBAction func btnNext(sender: UIButton) {
        Calendar.sharedInstance.carrentDate = Calendar.sharedInstance.addMonth(Calendar.sharedInstance.carrentDate)
        numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.carrentDate)//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
        dateFirst = Calendar.sharedInstance.getFirstDay(Calendar.sharedInstance.carrentDate)//מחזירה את התאריך הראשון של החודש שנשלח
        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
        i = 1
       // moneForBackColor = 1
        collDays.reloadData()
        
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        today = Mycalendar.dateByAddingUnit(.Month, value: 1, toDate: today, options: [])!
        
        changeLblDate()
    }
    //    @IBAction func btnNext(sender: UIButton) {
    //        currentDate = Calendar.sharedInstance.addMonth(NSDate())
    //    numDaysInMonth = Calendar.sharedInstance.getNumsDays(Calendar.sharedInstance.addMonth(NSDate()))//מחזיר מס׳ ימים בחודש שנשלח בפעם הראשונה התאריך של היום
    //        dateFirst = Calendar.sharedInstance.getFirstDay(currentDate)//מחזירה את התאריך הראשון של החודש שנשלח
    //        dayInWeek = Calendar.sharedInstance.getDayOfWeek(dateFirst)!
    //        i = 0
    //        collDays.reloadData()
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    
    
    // MARK: Optional methods
    
    //    func shouldShowWeekdaysOut() -> Bool {
    //        return shouldShowDaysOut
    //    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    
    
    
    
    //MARK: - UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        if section == 0{
        //            return 1
        //        }
        if (numDaysInMonth > 30 && dayInWeek == 6
            ) || (numDaysInMonth > 29 && dayInWeek == 7){
            return 42
        }
        return 35
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //Aligning right to left on UICollectionView
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(-1, 1)
        
        
        //        if indexPath.section == 0 {
        //            let cell:ImageDetailsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageDetailsCollectionViewCell", forIndexPath: indexPath) as! ImageDetailsCollectionViewCell
        //            cell.setDisplayData(detailsProduct.sNameFileD)
        //            cell.transform = scalingTransform
        //            return cell
        //        }
        //to continue
        let cell:dayMonthCalendarSupplierCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("dayMonthCalendarSupplierCollectionViewCell",forIndexPath: indexPath) as! dayMonthCalendarSupplierCollectionViewCell
        
        cell.delegate = Global.sharedInstance.eleventCon//תחת המודל של יומן שלי ללקוח
        
        cell.imgToday.hidden = true
        cell.lblDayDesc.alpha = 1.0
        //     var s =
        // var d:NSDate()
        cell.lblDayDesc.text = ""
        cell.lblDayDesc.textColor = Colors.sharedInstance.color1
//        if moneForBackColor % 7 == 0{
//            if Global.sharedInstance.whichReveal == false{
//                cell.lblDayDesc.textColor = Colors.sharedInstance.color3
//                cell.isShabat = true
//            }
//            else{
//                cell.lblDayDesc.textColor = Colors.sharedInstance.color4
//            }
//        }
        //moneForBackColor += 1
        if indexPath.row >= (dayInWeek - 1)  && indexPath.row < (numDaysInMonth + dayInWeek - 1 ){
            cell.setDisplayData(i)
            i += 1
        }
        else{
            cell.setNull()
            cell.btnEnterToDay.enabled = false
           // cell.btnEnterToDay.backgroundColor =  UIColor.greenColor()
        }
        
        //        cell.delegate = self
        //        cell.btnCheck.tag = indexPath.row
        if language == "he"
        {
            cell.transform = scalingTransform
        }
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        componentsCurrent.day = i
//        let date:NSDate = Calendar.sharedInstance.from(componentsCurrent.year , month: componentsCurrent.month, day: i)
        // self.ifHasEventInDayFunc(date)
        //        if  self.ifHasEventInDayFunc(date)
        //        {
        //            cell.btnEnterToDay.backgroundColor = UIColor.greenColor()
        //        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if (numDaysInMonth > 30 && dayInWeek == 6) || (numDaysInMonth > 29 && dayInWeek == 7)
        {
            return CGSize(width: view.frame.size.width / 7.6, height:  view.frame.size.width / 7.6)
        }
        return CGSize(width: view.frame.size.width / 7, height:  view.frame.size.width / 7)
    }
    
    func changeLblDate(){
        if DeviceType.IS_IPHONE_5 ||  DeviceType.IS_IPHONE_4_OR_LESS{
            //let fontSize:CGFloat = self.lblCurrentDate.font.pointSize;
            lblCurrentDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 20)
            lblHebrewDate.font = UIFont(name: lblCurrentDate.font.fontName, size: 11)
            
        }

        var str:String = ""
        var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
        //var index:Int = 0
        let characters = s1[1].characters.map { String($0) }
        if characters[0] == String(0){
            str = NSDateFormatter().monthSymbols[Int(characters[1])! - 1]
            //monthArray[Int(characters[1])!]
        }
        else{
            str = NSDateFormatter().monthSymbols[Int(s1[1])! - 1]
            //monthArray[Int(s1[1])!]
            
        }
        lblCurrentDate.text = str + " " + s1[2]
        let hebrew: NSLocale?
        if language == "he"
        {
            // Hebrew, Israel
            hebrew = NSLocale(localeIdentifier: "he_IL")
        }
        else
        {
            hebrew = NSLocale(localeIdentifier: "en_IL")
        }
        
        //NSHebrewCalendar
        let Mycalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierHebrew)!
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.locale = hebrew
        dateFormat.calendar = Mycalendar
        dateFormat.dateStyle = NSDateFormatterStyle.ShortStyle
        let dateString: String = dateFormat.stringFromDate(today)
        lblHebrewDate.text = dateString
    }
    //set all event on device in array
    func setEventsArray()
    {
        arrEventsCurrentDay = []
        for item in Global.sharedInstance.eventList
        {
            let event = item as! EKEvent
            
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
            
            yearToday =  componentsCurrent.year
            monthToday = componentsCurrent.month
            dayToday = componentsCurrent.day
            
            let yearEvent =  componentsEvent.year
            let monthEvent = componentsEvent.month
            
            if yearEvent == yearToday && monthEvent == monthToday
            {
                arrEventsCurrentDay.append(event)
                hasEvent = true
            }
        }
        
    }
    //MARK: - UITableViewDelegate

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayWorkers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")

        cell.textLabel?.text = arrayWorkers[indexPath.row]
        cell.textLabel?.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
        
//        let cell =
//            tableView.dequeueReusableCellWithIdentifier("subjects")as!SubjectsTableViewCell
//        
//        cell.setDisplayData(arrayWorkers[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath){
        lblWorkerName.text = arrayWorkers[indexPath.row]
        tblWorkers.hidden = true
        tblWorkers.tag = 0
        
        
    }
    
    
    func openWorkers()
    {
        if tblWorkers.tag == 0
        {
            tblWorkers.hidden = false
            tblWorkers.tag = 1
        }
        else
        {
          tblWorkers.hidden = true
          tblWorkers.tag = 0
        }

    }
   
    func showSync()
    {
        if btnSyn.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSyn.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = true//משתנה גלובלי המסמל האם יש סנכרון עם היומן האישי ביומן של הספק
            i = 1
            collDays.reloadData()
        }
        else
        {
            btnSyn.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendarSupplier = false
            i = 1
            collDays.reloadData()
        }

    }

}





// MARK: - CVCalendarViewAppearanceDelegate



// MARK: - IB Actions



/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


