//
//  Provider.swift
//  bthree-ios
//
//  Created by User on 27.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class Provider: NSObject {
    
    var iUserId:Int = 0
    var iIdBuisnessDetails:Int = 0
    
    var nvSupplierName:String = ""
    var nvBusinessIdentity:String = ""
    var nvAddress:String = ""
    var nvCity:String = ""
    var iCityType:Int = 0
    var nvPhone:String = ""
    var nvFax:String = ""
    var nvEmail:String = ""
    var nvSiteAddress:String = ""
   
    override init() {
        iUserId = 0
        iIdBuisnessDetails = 0
        
        nvSupplierName = ""
        nvBusinessIdentity = ""
        nvAddress = ""
        nvCity = ""
        iCityType = 0
        nvPhone = ""
        nvFax = ""
        nvEmail = ""
        nvSiteAddress = ""
    }
    
    init(_iUserId:Int,_iIdBuisnessDetails:Int,_nvSupplierName:String,_nvBusinessIdentity:String,_nvAddress:String,_nvCity:String,_iCityType:Int,_nvPhone:String,_nvFax:String,_nvEmail:String,_nvSiteAddress:String) {
        
        iUserId = _iUserId
        iIdBuisnessDetails = _iIdBuisnessDetails
        
        nvSupplierName = _nvSupplierName
        nvBusinessIdentity = _nvBusinessIdentity
        nvAddress = _nvAddress
        nvCity = _nvCity
        iCityType = _iCityType
        nvPhone = _nvPhone
        nvFax = _nvFax
        nvEmail = _nvEmail
        nvSiteAddress = _nvSiteAddress
    }

    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = iUserId
        dic["iBusinessId"] = Global.sharedInstance.currentProvider.iIdBuisnessDetails
        //dic["iIdBuisnessDetails"] = iIdBuisnessDetails
        
        dic["nvSupplierName"] = nvSupplierName
        dic["nvBusinessIdentity"] = nvBusinessIdentity
        dic["nvAddress"] = nvAddress
        dic["nvCity"] = nvCity
        dic["iCityType"] = iCityType
        dic["nvPhone"] = nvPhone
        dic["nvFax"] = nvFax
        dic["nvEmail"] = nvEmail
        dic["nvSiteAddress"] = nvSiteAddress
        
        return dic
    }
    //2do-delete
    func getDicExample()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = 14281
        dic["iBusinessId"] = 0
        dic["nvSupplierName"] = "abc"
        dic["nvBusinessIdentity"] = "asddf"
        dic["nvAddress"] = "sdfu dsfds"
        dic["nvCity"] = "dfsf"
        dic["iCityType"] = 0
        dic["nvPhone"] = "054777777"
        dic["nvFax"] = ""
        dic["nvEmail"] = "sdfdf@dfg.dfgd"
        dic["nvSiteAddress"] = ""
        
        return dic
    }

    

    //לשים לב לפני השימוש בפונקציה שהשמשתנים שחוזרים מהשרת הם בדיוק באותם שמות
    func dicToProvider(dic:Dictionary<String,AnyObject>)->Provider
    {
        let provider:Provider = Provider()
        
        provider.iUserId = Global.sharedInstance.parseJsonToInt(dic["iUserId"]!)//√
        
        if dic["iSupplierId"] != nil
        {
            provider.iIdBuisnessDetails = Global.sharedInstance.parseJsonToInt(dic["iSupplierId"]!)
        }
        else
        {
             provider.iIdBuisnessDetails = Global.sharedInstance.parseJsonToInt(dic["iBusinessId"]!)
        }
        
        provider.nvSupplierName = Global.sharedInstance.parseJsonToString(dic["nvSupplierName"]!)//√
        provider.nvBusinessIdentity = Global.sharedInstance.parseJsonToString(dic["nvBusinessIdentity"]!)//√
        provider.nvAddress = Global.sharedInstance.parseJsonToString(dic["nvAddress"]!)//√
        provider.iCityType = Global.sharedInstance.parseJsonToInt(dic["iCityType"]!)//√
        provider.nvPhone = Global.sharedInstance.parseJsonToString(dic["nvPhone"]!)//√
        provider.nvFax = Global.sharedInstance.parseJsonToString(dic["nvFax"]!)//√
        provider.nvEmail = Global.sharedInstance.parseJsonToString(dic["nvEmail"]!)//√
        provider.nvSiteAddress = Global.sharedInstance.parseJsonToString(dic["nvSiteAddress"]!)//√
        
        return provider
    }
}
