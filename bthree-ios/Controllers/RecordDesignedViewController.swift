//
//  RecordDesignedViewController.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/14/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

//לקוח -תצוגת רשימה
class RecordDesignedViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
//MARK: - varibals
    var headersArray:Array<String> = ["2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  ","2015 אוק׳ 23ו׳,  "]
    var RowsArrayHours:Array<String> = ["08:00-09:30","08:00-09:30","08:00-09:30"]
    var RowsArrayDescs:Array<String> = ["מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית(","מירב כהן )פסיכולוגית("]
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    //var arrEventsIn5Month:Array<EKEvent> = []
    var year5Month =  0
    var month5Month = 0
    var day5Month = 0
    var yearEvent =  0
    var monthEvent = 0
    var dayEvent = 0
    //מכיל את כל הארועים מהמכשיר ושל ביזר להצגה מחולק לפי תאריך(לכל תאריך יש את הארועים שלו:הקי=תאריך הארוע)
    var dicArrayEventsToShow:Dictionary<String,Array<allKindEventsForListDesign>> = Dictionary<String,Array<allKindEventsForListDesign>>()
    var sortDicEvents:[(String,Array<allKindEventsForListDesign>)] = []//מכיל את כל הארועים הנ״ל בצורה ממויינת לפי תאריך ולפי שעות לכל יום
    
    var dicBthereEvent:Dictionary<String,Array<allKindEventsForListDesign>> = Dictionary<String,Array<allKindEventsForListDesign>>()//למקרה שהסנכרון עם המכשיר מכובה משתמשים במערך זה
    var sortDicBTHEREevent:[(String,Array<allKindEventsForListDesign>)] = []//מכיל את כל ארועים ביזר בצורה ממויינת לפי תאריך ולפי שעות לכל יום
    
    let dateFormatter = NSDateFormatter()
    
    var generic:Generic = Generic()
    var reloadFromInitEvents = false//מציין האם הגיעו לריענון הטבלה מהפונקציה ל initEvents
    //ואז יש שאול האם אין נתונים להציג
    
    //MARK: - @IBAction
    //click on sync eye
    @IBAction func btnSync(sender: eyeSynCheckBox) {
        if sender.isCecked == false{
            Global.sharedInstance.getEventsFromMyCalendar()
            Global.sharedInstance.isSyncWithGoogelCalendar = true
        }
        else{
            Global.sharedInstance.isSyncWithGoogelCalendar = false
        }
    }
    
    @IBOutlet weak var viewSync: UIView!
    
    @IBOutlet weak var btnSync: eyeSynCheckBox!
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        reloadFromInitEvents = false
        
        GetCustomerOrders()
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true
        {
            btnSync.isCecked = true
        }
        
        AppDelegate.i = 0

        tblData.separatorStyle = .None
       
        let tapSync = UITapGestureRecognizer(target:self, action:#selector(self.showSync))
        viewSync.addGestureRecognizer(tapSync)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        initEvents()
    }

    @IBOutlet var tblData: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true
        {
            return sortDicEvents.count//כל הארועים
        }
        if reloadFromInitEvents == true && sortDicBTHEREevent.count == 0
        //if sortDicBTHEREevent.count == 0
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_EVENTS", comment: ""), vc: self)
        }
        return sortDicBTHEREevent.count//רק ארועי ביזר
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Global.sharedInstance.isSyncWithGoogelCalendar == true
        {
            return sortDicEvents[section].1.count + 1//כל הארועים
        }
       return sortDicBTHEREevent[section].1.count + 1//רק ארועי ביזר
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var event:(String,Array<allKindEventsForListDesign>)?
        if Global.sharedInstance.isSyncWithGoogelCalendar == true
        {
            event = sortDicEvents[indexPath.section]
        }
        else
        {
            event = sortDicBTHEREevent[indexPath.section]
        }
        
//        let event =  Global.sharedInstance.arrEvents[indexPath.section]
        
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        let componentsToday = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateFormatter.dateFromString(event!.0)!)
        // print(event.startDate)
        yearToday =  componentsCurrent.year
        monthToday = componentsCurrent.month
        dayToday = componentsCurrent.day
        
        let yearEvent =  componentsEvent.year
        let monthEvent = componentsEvent.month
        let monthName = NSDateFormatter().shortStandaloneMonthSymbols[monthEvent - 1]
        let dayEvent = componentsEvent.day
        
        if indexPath.row == 0
        {
            let cell:HeaderRecordTableViewCell = tableView.dequeueReusableCellWithIdentifier("HeaderRecordTableViewCell")as!HeaderRecordTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            let dayWeek = Calendar.sharedInstance.getDayOfWeek(dateFormatter.dateFromString(event!.0)!)
            let dayInWeek = NSDateFormatter().veryShortWeekdaySymbols[dayWeek! - 1]
            if componentsToday.day == dayEvent && componentsToday.month == monthEvent && componentsToday.year == yearEvent
            {
                cell.imgToday.hidden = false
            }
            else
            {
                cell.imgToday.hidden = true
            }
            let str =  "," + String(dayEvent) + " " + String(monthName) + " " + String(yearEvent)
            cell.setDisplayData(str,daydesc: dayInWeek)
            return cell
        }
        else
        {
            let cell:RowRecordTableViewCell = tableView.dequeueReusableCellWithIdentifier("RowRecordTableViewCell")as!RowRecordTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.delegate = Global.sharedInstance.calendarClient
            
            cell.event = event!.1[indexPath.row - 1]
            
            let hour =  "\(event!.1[indexPath.row - 1].fromHour) - \(event!.1[indexPath.row - 1].toHour)"
            cell.setDisplayData(hour, desc:event!.1[indexPath.row - 1].title,EventFrom: event!.1[indexPath.row - 1].tag)
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0{
        return 25
        }
        return 50
    }
    
    func showSync()
    {
        if btnSync.isCecked == false
        {
            Global.sharedInstance.getEventsFromMyCalendar()
            btnSync.isCecked = true
            Global.sharedInstance.isSyncWithGoogelCalendar = true
        }
        else
        {
            btnSync.isCecked = false
            Global.sharedInstance.isSyncWithGoogelCalendar = false
        }
        tblData.reloadData()
    }
    
   
    
    func small(lhs: NSDate, rhs: NSDate) -> Bool {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        
//        let isToday:Bool = calendar.isDateInToday(lhs);
//        if isToday
//        {
//            return false
//        }
//        else
//        {
            return lhs.compare(rhs) == .OrderedAscending
//        }
    }

    func GetCustomerOrders()  {
        var dic:Dictionary<String,AnyObject> =  Dictionary<String,AnyObject>()
        var arr = NSArray()
        dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        generic.showNativeActivityIndicator(self)
        if Reachability.isConnectedToNetwork() == false
        {
            generic.hideNativeActivityIndicator(self)
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
        }
        else
        {
        api.sharedInstance.GetCustomerOrders(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            print(responseObject["Result"])
            arr = responseObject["Result"] as! NSArray
            let ps:OrderDetailsObj = OrderDetailsObj()
            
            Global.sharedInstance.ordersOfClientsArray = ps.OrderDetailsObjToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
            
            self.initEvents()
            self.generic.hideNativeActivityIndicator(self)
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                self.generic.hideNativeActivityIndicator(self)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
        }
    }
    
    func initEvents()
    {
        Global.sharedInstance.setAllEventsArray()
        
        dicArrayEventsToShow.removeAll()
        dicArrayEventsToShow = Dictionary<String,Array<allKindEventsForListDesign>>()
        dicBthereEvent.removeAll()
        dicArrayEventsToShow = Dictionary<String,Array<allKindEventsForListDesign>>()
        
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        Global.sharedInstance.getEventsFromMyCalendar()
        
        //------------------אתחול המערכים להצגת הארועים בצורה ממויינת------------------------
        let dateToday = NSDate()
        let dateIn5Month = Calendar.sharedInstance.addMonths(NSDate(), numMonthAdd: 5)
        
        //עובר על הארועים מהמכשיר
        for event in Global.sharedInstance.arrEvents
        {
            let dateEvent = event.startDate
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
            year5Month =  components5Month.year
            month5Month = components5Month.month
            day5Month = components5Month.day
            yearEvent =  componentsEvent.year
            monthEvent = componentsEvent.month
            dayEvent = componentsEvent.day
            
            //אם בטווח של 5 חודשים מהיום
            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
            {
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: event.startDate)
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: event.endDate)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
                //ליצור אובקט
                //בדיקה אם קיים כזה קי(תאריך)
                let eventPhone:allKindEventsForListDesign = allKindEventsForListDesign(
                    _dateEvent: event.startDate,
                    _title: event.title,
                    _fromHour: "\(hourS_Show):\(minuteS_Show)",
                    _toHour: "\(hourE_Show):\(minuteE_Show)",
                    _tag: 2,
                    _nvAddress: "",
                    _nvSupplierName: "",
                    _iDayInWeek: -1,
                    _nvServiceName: "",
                    _nvComment: "")
                
                if dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] != nil
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
                else
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)] = Array<allKindEventsForListDesign>()
                    dicArrayEventsToShow[dateFormatter.stringFromDate(event.startDate)]?.append(eventPhone)
                }
            }
        }
        //עובר על הארועים של ביזר
        for eventBthere in Global.sharedInstance.ordersOfClientsArray
        {
            let dateEvent = eventBthere.dtDateOrder
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let components5Month = calendar.components([.Day, .Month, .Year], fromDate: dateIn5Month)
            let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: dateEvent)
            year5Month =  components5Month.year
            month5Month = components5Month.month
            day5Month = components5Month.day
            yearEvent =  componentsEvent.year
            monthEvent = componentsEvent.month
            dayEvent = componentsEvent.day
            
            //אם בטווח של 5 חודשים מהיום
            if (small(dateEvent, rhs: dateIn5Month) == true && small(dateToday, rhs: dateEvent) == true) || calendar.isDateInToday(dateEvent) || (year5Month == yearEvent && month5Month == monthEvent && day5Month == dayEvent)
            {
                let hourStart = Global.sharedInstance.getStringFromDateString(eventBthere.nvFromHour)
                let hourEnd = Global.sharedInstance.getStringFromDateString(eventBthere.nvToHour)
                
                let componentsStart = calendar.components([.Hour, .Minute], fromDate: hourStart)
                let componentsEnd = calendar.components([.Hour, .Minute], fromDate: hourEnd)
                
                let hourS = componentsStart.hour
                let minuteS = componentsStart.minute
                
                let hourE = componentsEnd.hour
                let minuteE = componentsEnd.minute
                
                var hourS_Show:String = hourS.description
                var hourE_Show:String = hourE.description
                var minuteS_Show:String = minuteS.description
                var minuteE_Show:String = minuteE.description
                
                if hourS < 10
                {
                    hourS_Show = "0\(hourS)"
                }
                if hourE < 10
                {
                    hourE_Show = "0\(hourE)"
                }
                if minuteS < 10
                {
                    minuteS_Show = "0\(minuteS)"
                }
                if minuteE < 10
                {
                    minuteE_Show = "0\(minuteE)"
                }
               
               var serviceName = ""
                for item in eventBthere.objProviderServiceDetails
                {
                    if serviceName == ""
                    {
                        serviceName = item.nvServiceName
                    }
                    else
                    {
                        serviceName = "\(serviceName),\(item.nvServiceName)"
                    }
                }
                
                var nvServiceName = ""
                for item in eventBthere.objProviderServiceDetails
                {
                    if nvServiceName == ""
                    {
                        nvServiceName = item.nvServiceName
                    }
                    else
                    {
                        nvServiceName = "\(nvServiceName),\(item.nvServiceName)"
                    }
                }
                
                let eventBtheree:allKindEventsForListDesign = allKindEventsForListDesign(
                    _dateEvent: eventBthere.dtDateOrder,
                    _title: "\(nvServiceName),\(eventBthere.nvSupplierName)",
                    _fromHour: "\(hourS_Show):\(minuteS_Show)",
                    _toHour: "\(hourE_Show):\(minuteE_Show)",
                    _tag: 1,
                    _nvAddress: eventBthere.nvAddress,
                    _nvSupplierName: eventBthere.nvSupplierName,
                    _iDayInWeek: eventBthere.iDayInWeek,
                    _nvServiceName: serviceName,
                    _nvComment: eventBthere.nvComment)
                //בשביל שאם לא רוצה סנכרון
                if dicBthereEvent[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] != nil
                {
                    dicBthereEvent[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
                }
                else
                {
                    dicBthereEvent[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] = Array<allKindEventsForListDesign>()
                    dicBthereEvent[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
                }
                
                if dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] != nil
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
                }
                else
                {
                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)] = Array<allKindEventsForListDesign>()
                    dicArrayEventsToShow[dateFormatter.stringFromDate(eventBthere.dtDateOrder)]?.append(eventBtheree)
                }
            }
        }
        //----------מיון לפי ימים
        //sortDicEvents = []
        sortDicEvents = [(String,Array<allKindEventsForListDesign>)]()
        sortDicEvents = dicArrayEventsToShow.sort{ dateFormatter.dateFromString($0.0)!.compare(dateFormatter.dateFromString($1.0)!) == .OrderedAscending}
        //ארועי ביזר בלבד!!
        sortDicBTHEREevent = [(String,Array<allKindEventsForListDesign>)]()
        sortDicBTHEREevent = dicBthereEvent.sort{ dateFormatter.dateFromString($0.0)!.compare(dateFormatter.dateFromString($1.0)!) == .OrderedAscending}
        //-------מיון לכל יום לפי השעות
        var i = 0
        for event in sortDicEvents//כל הארועים
        {
            sortDicEvents[i].1.sortInPlace({ $0.dateEvent.compare($1.dateEvent) == NSComparisonResult.OrderedAscending })
            i += 1
        }
        
        i = 0
        for event in sortDicBTHEREevent//ארועי ביזר
        {
            sortDicBTHEREevent[i].1.sortInPlace({ $0.dateEvent.compare($1.dateEvent) == NSComparisonResult.OrderedAscending })
            
            i += 1
        }
        reloadFromInitEvents = true
        tblData.reloadData()
    }

}
