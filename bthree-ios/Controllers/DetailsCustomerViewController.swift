//
//  DetailsCustomerViewController.swift
//  Bthere
//
//  Created by User on 24.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים דף פרטי לקוח
class DetailsCustomerViewController: UIViewController {

    //MARK: - Outlet
    
    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var txtVComment: UITextView!
    
    @IBOutlet weak var lblUpdate: UILabel!
    
    @IBOutlet weak var viewDelete: UIView!
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapUpdate:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(updateDetails))
        lblUpdate.addGestureRecognizer(tapUpdate)
        let tapDelete:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DeleteCustomer))
        viewDelete.addGestureRecognizer(tapDelete)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDetails()
    {
       print("update")
    }
    
    func DeleteCustomer()
    {
        print("Delete")
    }
}
