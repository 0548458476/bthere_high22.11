//
//  EleventhHourViewController.swift
//  bthree-ios
//
//  Created by User on 14.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// לקוח דף הדקה ה-90
class EleventhHourViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBAction func btnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblSales: UITableView!
    
    var today:NSDate = NSDate()
    var imagesArray:Array <String> = ["22.png","26.png","28.png"]
    var datesArray:Array <NSDate> = [NSDate(),NSDate(timeIntervalSinceReferenceDate: -12345.0),NSDate(timeIntervalSinceReferenceDate: +123000089.0)]
    //"12/05/16","היום","15/03/16"
    var textArray:Array <String> = ["תספורת אישה בחצי מחיר",
        "1 + 1 על מסיכות השיער",
        "גוונים ב₪200 /n עד השעה 2:00"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addTopBorder(tblSales, color: UIColor.blackColor())
        tblSales.separatorStyle = .None
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("EleventhHour")as! EleventhHourTableViewCell
        
        let separatorView: UIView = UIView(frame: CGRectMake(0, cell.frame.height, cell.frame.width + (cell.frame.width / 3), 1))
        separatorView.layer.borderColor = UIColor.blackColor().CGColor
        separatorView.layer.borderWidth = 1
        cell.contentView.addSubview(separatorView)
        

        cell.setDisplayData(imagesArray[indexPath.row], date: shortDate(datesArray[indexPath.row]), text: textArray[indexPath.row])


        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.height * 0.1125
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func addTopBorder(any:UIView,color:UIColor)
    {
        let borderTop = CALayer()
        
        borderTop.frame = CGRectMake(0, 0, any.layer.frame.width + (any.layer.frame.width / 3), 1)
        
        borderTop.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderTop)
    }

    func shortDate(date:NSDate) -> String {
        
        let calendar = NSCalendar.currentCalendar()
        
        let components = calendar.components([.Day, .Month, .Year], fromDate: date)
        
        let components1 = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        let yearToday =  components1.year
        let monthToday = components1.month
        let dayToday = components1.day
        
        if year == yearToday && month == monthToday && day == dayToday
        {
        return NSLocalizedString("TODAY", comment: "")
        }

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.stringFromDate(date)
    }
    
}
