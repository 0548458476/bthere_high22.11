//
//  objUsers.swift
//  bthree-ios
//
//  Created by User on 30.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//caontain user details as customer
class objUsers: NSObject {

    //MARK: - Variables
    
    var nvUserName:String = ""
    var nvFirstName:String = ""
    var nvLastName:String = ""
    var dBirthdate:NSDate = NSDate()
    var nvMail:String = ""
    var nvAdress:String = ""
    var iCityType:Int = 0
    var nvPhone:String = ""
    var nvPassword:String = ""
    var nvVerification:String = ""
    var bAutomaticUpdateApproval:Bool = false
    var bDataDownloadApproval:Bool = false
    var bTermOfUseApproval:Bool = false
    var bAdvertisingApproval:Bool = false
    var iUserStatusType:Int = 0
    var bIsGoogleCalendarSync:Bool = false
    var iCreatedByUserId:Int = 0
    var iLastModifyUserId:Int = 0
    var iSysRowStatus:Int = 0
    
    //MARK: - Initial
    
    override init() {
        nvUserName = ""
        nvFirstName = ""
        nvLastName = ""
        dBirthdate = NSDate()
        nvMail = ""
        nvAdress = ""
        iCityType = 0
        nvPhone = ""
        nvPassword = ""
        nvVerification = ""
        bAutomaticUpdateApproval = false
        bDataDownloadApproval = false
        bTermOfUseApproval = false
        bAdvertisingApproval = false
        iUserStatusType = 0
        bIsGoogleCalendarSync = false
        iCreatedByUserId = 0
        iLastModifyUserId = 0
        iSysRowStatus = 0
    }
    
    init( _nvUserName:String,_nvFirstName:String,_nvLastName:String, _dBirthdate:NSDate,_nvMail:String,_nvAdress:String,_iCityType:Int,_nvPhone:String,_nvPassword:String,_nvVerification:String,_bAutomaticUpdateApproval:Bool,_bDataDownloadApproval:Bool,_bTermOfUseApproval:Bool,_bAdvertisingApproval:Bool,_iUserStatusType:Int,_bIsGoogleCalendarSync:Bool,_iCreatedByUserId:Int,_iLastModifyUserId:Int,_iSysRowStatus:Int)
    {
        nvUserName = _nvUserName
        nvFirstName = _nvFirstName
        nvLastName = _nvLastName
        dBirthdate = _dBirthdate
        nvMail = _nvMail
        nvAdress = _nvAdress
        iCityType = _iCityType
        nvPhone = _nvPhone
        nvPassword = _nvPassword
        nvVerification = _nvVerification
        bAutomaticUpdateApproval = _bAutomaticUpdateApproval
        bDataDownloadApproval = _bDataDownloadApproval
        bTermOfUseApproval = _bTermOfUseApproval
        bAdvertisingApproval = _bAdvertisingApproval
        iUserStatusType = _iUserStatusType
        bIsGoogleCalendarSync = _bIsGoogleCalendarSync
        iCreatedByUserId = _iCreatedByUserId
        iLastModifyUserId = _iLastModifyUserId
        iSysRowStatus = _iSysRowStatus
    }
    
    //MARK: - Functions
    
    //return the obj as dictionary, help to send to server
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        dic["nvUserName"] = nvUserName
        dic["nvFirstName"] = nvFirstName
        dic["nvLastName"] = nvLastName
        dic["dBirthdate"] = Global.sharedInstance.convertNSDateToString(dBirthdate)
        dic["nvMail"] = nvMail
        dic["nvAdress"] = nvAdress
        dic["iCityType"] = iCityType
        dic["nvPhone"] = nvPhone
        dic["nvPassword"] = nvPassword
        dic["nvVerification"] = nvVerification
        dic["bAutomaticUpdateApproval"] = bAutomaticUpdateApproval
        dic["bDataDownloadApproval"] = bDataDownloadApproval
        dic["bTermOfUseApproval"] = bTermOfUseApproval
        dic["bAdvertisingApproval"] = bAdvertisingApproval
        dic["iUserStatusType"] = iUserStatusType
        dic["bIsGoogleCalendarSync"] = bIsGoogleCalendarSync
        dic["iCreatedByUserId"] = iCreatedByUserId
        dic["iLastModifyUserId"] = iLastModifyUserId
        dic["iSysRowStatus"] = iSysRowStatus
        
        return dic
    }
    
    //convert dictionary to objUsers object
    //get:dictionary, return objUsers object
    func dicToObjUsers(dic:Dictionary<String,AnyObject>) -> objUsers {
        
        let user:objUsers = objUsers()
        
        user.nvUserName = Global.sharedInstance.parseJsonToString(dic["nvUserName"]!)
        user.nvFirstName = Global.sharedInstance.parseJsonToString(dic["nvFirstName"]!)
        user.nvLastName = Global.sharedInstance.parseJsonToString(dic["nvLastName"]!)
        
        let dateFormatter = NSDateFormatter()
        //dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        if Global.sharedInstance.parseJsonToString(dic["dBirthdate"]!) != ""
        {
            user.dBirthdate = Global.sharedInstance.getStringFromDateString(dic["dBirthdate"]! as! String)
        }
        user.nvMail = Global.sharedInstance.parseJsonToString(dic["nvMail"]!)
        if dic["nvAdress"] != nil
        {
            user.nvAdress = Global.sharedInstance.parseJsonToString(dic["nvAdress"]!)
        }
        user.iCityType = Global.sharedInstance.parseJsonToInt(dic["iCityType"]!)
        user.nvPhone = Global.sharedInstance.parseJsonToString(dic["nvPhone"]!)
        user.nvPassword = Global.sharedInstance.parseJsonToString(dic["nvPassword"]!)
        user.nvVerification = Global.sharedInstance.parseJsonToString(dic["nvVerification"]!)
        user.bAutomaticUpdateApproval = dic["bAutomaticUpdateApproval"] as! Bool
        user.bDataDownloadApproval = dic["bDataDownloadApproval"] as! Bool
        user.bTermOfUseApproval = dic["bTermOfUseApproval"] as! Bool
        user.bAdvertisingApproval = dic["bAdvertisingApproval"] as! Bool
        user.iUserStatusType = Global.sharedInstance.parseJsonToInt(dic["iUserStatusType"]!)
        if dic["bIsGoogleCalendarSync"] != nil
        {
            user.bIsGoogleCalendarSync = dic["bIsGoogleCalendarSync"] as! Bool
        }
        else
        {
            user.bIsGoogleCalendarSync = false
        }
        user.iCreatedByUserId = Global.sharedInstance.parseJsonToInt(dic["iCreatedByUserId"]!)
        user.iLastModifyUserId = Global.sharedInstance.parseJsonToInt(dic["iLastModifyUserId"]!)
        user.iSysRowStatus = Global.sharedInstance.parseJsonToInt(dic["iSysRowStatus"]!)
        
        return user
    }
}
