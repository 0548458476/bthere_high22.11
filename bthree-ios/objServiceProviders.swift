//
//  objServiceProviders.swift
//  bthree-ios
//
//  Created by User on 30.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class objServiceProviders: NSObject {

    var objsers:objUsers = objUsers()
     //מכיל את השעות הפעילות עם חיסור של ההפסקות
    var arrObjWorkingHours:Array<objWorkingHours> = Array<objWorkingHours>()
    var bSameWH:Bool = false
    
    override init() {
        objsers = objUsers()
        arrObjWorkingHours = Array<objWorkingHours>()
        bSameWH = false
    }
    
    init(_objsers:objUsers,_arrObjWorkingHours:Array<objWorkingHours>,_bSameWH:Bool) {
        
        objsers = _objsers
        arrObjWorkingHours = _arrObjWorkingHours
        bSameWH = _bSameWH
    }
    
    func getdicWorkingHours()->Array<AnyObject>{
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in arrObjWorkingHours
        {
            dic = it.getDic()
            arr.append(dic)
        }
        return arr
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["objUsers"] = objsers.getDic()
        dic["bSameWH"] = bSameWH
        dic["objWorkingHours"] = getdicWorkingHours()
        
        return dic
    }
    
    func objServiceProvidersToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<objServiceProviders>{
        
        var arrServiceProv:Array<objServiceProviders> = Array<objServiceProviders>()
        var ServiceProv:objServiceProviders = objServiceProviders()
        
        for i in 0  ..< arrDic.count
        {
            ServiceProv = dicToServiceProviders(arrDic[i])
            arrServiceProv.append(ServiceProv)
        }
        return arrServiceProv
    }
    
    func dicToServiceProviders(dic:Dictionary<String,AnyObject>)->objServiceProviders
    {
        let serviceProv:objServiceProviders = objServiceProviders()
        
        serviceProv.objsers = serviceProv.objsers.dicToObjUsers(dic["objUsers"]! as! Dictionary<String,AnyObject>)
        serviceProv.bSameWH = dic["bSameWH"] as! Bool
        
        let workingHours:objWorkingHours = objWorkingHours()
        serviceProv.arrObjWorkingHours = workingHours.objWorkingHoursToArray(dic["objWorkingHours"] as! Array<Dictionary<String,AnyObject>>)
        
        return serviceProv
    }
}
