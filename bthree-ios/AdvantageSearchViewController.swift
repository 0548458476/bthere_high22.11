//
//  AdvantageSearchViewController.swift
//  bthree-ios
//
//  Created by User on 22.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
//דף חיפוש מתקדם
class AdvantageSearchViewController: UIViewController,UITextFieldDelegate,dismissViewControllerDelegate,UIGestureRecognizerDelegate,SPGooglePlacesAutocompleteQueryDelegate,openFromMenuDelegate {
    
    var delegate:dismissViewControllerDelegate!=nil
    var delegateSearch:openSearchResultsDelegate!=nil
    var isSubjectFlag = false
    var filterSubArr:Array<Domain> = []
    var filterAreaArr:Array<String> = []
    //to cut the address and show only city
    var cityArr:Array<String> = []
    var isValidSearch:Bool = false
    
    let googlePlacesAutocomplete = SPGooglePlacesAutocompleteQuery()
    var listAutocompletePlace:[SPGooglePlacesAutocompletePlace] = []
    var coordinatePackage:CLLocationCoordinate2D!
    var isSearch = false
    
    var generic:Generic = Generic()
    var searchResults: [String]!
    //var searchResultController:SearchResultsController!
    var resultsArray = [String]()
    var googleMapsView:GMSMapView!
    
    var chosenDomain:Int = 0
    var chosenDate:NSDate?
    var chosenFromHour:String = ""
    var chosenToHour:String = ""
    var chosenCity:String = ""
    var dateToServer = ""
    
    var hhFromHour:Float = 0.0
    var mmFromHour:Float = 0.0
    var hhToHour:Float = 0.0
    var mmToHour:Float = 0.0
    var isSelectedSubject:Bool = false

    @IBOutlet weak var vSubjects1: UIView!
    
    @IBOutlet weak var lblSubject: UILabel!
    
    @IBOutlet weak var vDates1: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var vArea1: UIView!
    
    @IBOutlet weak var lblArea: UILabel!
    
    @IBOutlet weak var imgPlusMenu: UIImageView!
    @IBOutlet weak var vHours1: UIView!
    
    @IBOutlet weak var lblHour: UILabel!
    
    var dicSearchProvider:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()//עבור השליחה לשרת
    
    @IBAction func btnSearch(sender: AnyObject) {
        
        Global.sharedInstance.searchDomain = txtfSubject.text!
        Global.sharedInstance.viewConNoInternet = self
        txtCity.dismissSuggestionTableView()
        if hhFromHour > hhToHour
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_HOURS", comment: ""),vc: self)
        }
        else
        {
            if txtCity.text != ""
            {
                lblArea.text = txtCity.text
                chosenCity = txtCity.text!
            }
            else
            {
                chosenCity = ""
            }
            dicSearchProvider["iSupplierFieldType"] = chosenDomain
            if chosenCity == ""
            {
               dicSearchProvider["nvCity"] = nil
            }
            else
            {
            dicSearchProvider["nvCity"] = chosenCity
            isValidSearch = true
            }
            
            if dateToServer == ""
            {
                dicSearchProvider["dtDateDesirable"] = nil

            }
            else
            {
            dicSearchProvider["dtDateDesirable"] = dateToServer
            isValidSearch = true
            }
            if chosenFromHour == ""
            {
                dicSearchProvider["tFromHour"] = nil
            }
            else
            {
                dicSearchProvider["tFromHour"] = chosenFromHour
                isValidSearch = true
            }
            if chosenToHour == ""
            {
                dicSearchProvider["tToHour"] = nil
            }
            else
            {
                dicSearchProvider["tToHour"] = chosenToHour
                isValidSearch = true
            }

            dicSearchProvider["nvlong"] = Global.sharedInstance.currentLong?.description
            dicSearchProvider["nvlat"] = Global.sharedInstance.currentLat?.description
            
            if dicSearchProvider["nvLong"] != nil || dicSearchProvider["nvlat"] != nil || Int((dicSearchProvider["iSupplierFieldType"]?.description)!) != 0
            {
                isValidSearch = true
            }

            if isValidSearch == false
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("NOT_DATA_TO_SEARCH", comment: ""),vc: self)
            }
            else
            {
                generic.showNativeActivityIndicator(self)
                
                generic.showNativeActivityIndicator(self)
                if Reachability.isConnectedToNetwork() == false
                {
                    generic.hideNativeActivityIndicator(self)
                    Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: self)
                }
                else
                {
                    api.sharedInstance.SearchProviders(dicSearchProvider, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                        
                        if responseObject["Error"]!!["ErrorCode"] as! Int == -3
                        {
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SUPPLIERS_MATCH", comment: ""))
                        }
                        else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
                        {
                            Global.sharedInstance.dicResults = responseObject["Result"] as! Array<Dictionary<String,AnyObject>>
                            self.dismissViewControllerAnimated(false, completion: nil)
                            Global.sharedInstance.dicSearchProviders = self.dicSearchProvider
                            Global.sharedInstance.whichSearchTag = 2
                            self.delegateSearch.openSearchResults()
                            
                        }
                        self.generic.hideNativeActivityIndicator(self)
                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                            self.generic.hideNativeActivityIndicator(self)
                            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                    })
                }
            }
        }
    }
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var bSubject: UIButton!
    
    @IBAction func bSubject(sender: AnyObject) {

        
        if !isSelectedSubject
        {
            if txtCity.text != ""
            {
                lblArea.text = txtCity.text
                chosenCity = txtCity.text!
            }
            else
            {
                chosenCity = ""
            }
            
        txtfSubject.placeholder = NSLocalizedString("SUBJECT_PLACE_HOLDER", comment: "")
        dismissKeyboard()
        txtCity.hidden = true
        tblCities.hidden = true
        filterSubArr = []
        filterAreaArr = []
        tblSubject.reloadData()
        //txtCity.text = ""
        //txtfSubject.text = ""
        isSubjectFlag = true
        tblSubject.hidden = false
        txtfSubject.hidden = false
        txtfSubject.becomeFirstResponder()
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bSubject, btn: btnSubject)
        closeArrow(btnArea, btn2: btnDate, btn3: btnHour)
        changeFontStyle(bArea, btn2: bDate, btn3: bHours)
        vSubjects1.hidden = true
        vArea1.hidden = true
        vHours1.hidden = true
        vDates1.hidden = true
        }
        else
        {
             // hideViews(vSubjects1)
        }
      

        openSubject()

    }
    
    //func close
    @IBOutlet weak var btnSubject: UIButton!
    @IBAction func btnSubject(sender: AnyObject) {
        openSubject()

    }
    
    @IBOutlet weak var bArea: UIButton!
    @IBAction func bArea(sender: AnyObject) {
        dismissKeyboard()
        txtCity.hidden = false
        txtCity.becomeFirstResponder()
        txtfSubject.hidden = true
        txtCity.placeholder = NSLocalizedString("AREA_PLACE_HOLDER", comment: "")
        filterAreaArr = []
        filterSubArr = []
        tblSubject.reloadData()
        tblCities.reloadData()
        //txtCity.text = ""
        isSubjectFlag = false
        tblSubject.hidden = true
        tblCities.hidden = false
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bArea, btn: btnArea)
        closeArrow(btnSubject, btn2: btnDate, btn3: btnHour)
        changeFontStyle(bSubject, btn2: bDate, btn3: bHours)
      //  hideViews(vArea1)
        vSubjects1.hidden = false
        vArea1.hidden = true
        vHours1.hidden = true
        vDates1.hidden = true
    }
    
    @IBOutlet weak var btnArea: UIButton!
    @IBAction func btnArea(sender: AnyObject) {
        
       openArea()
    }
    
    @IBOutlet weak var bDate: UIButton!
    @IBAction func bDate(sender: AnyObject) {
        dismissKeyboard()
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        //txtfSubject.text = ""
        txtfSubject.hidden = true
        //txtCity.text = ""
        txtCity.hidden = true
        tblSubject.hidden = true
        tblCities.hidden = true
        dates.hidden = false
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bDate, btn: btnDate)
        closeArrow(btnArea, btn2: btnSubject, btn3: btnHour)
        changeFontStyle(bArea, btn2: bSubject, btn3: bHours)
        vSubjects1.hidden = false
        vArea1.hidden = false
        vHours1.hidden = true
        vDates1.hidden = true
    }
    
    @IBOutlet weak var btnDate: UIButton!
    @IBAction func btnDate(sender: AnyObject) {
        openDate()
           }
    
    @IBOutlet weak var bHours: UIButton!
    @IBAction func bHours(sender: AnyObject) {
        dismissKeyboard()
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        //txtfSubject.text = ""
        txtfSubject.hidden = true
        //txtCity.text = ""
        txtCity.hidden = true
        tblSubject.hidden = true
        tblCities.hidden = true
        dates.hidden = true
        hour1.hidden = false
        hours2.hidden = false
        lineView.hidden  = false
        
        openArrow(bHours, btn: btnHour)
        closeArrow(btnArea, btn2: btnDate, btn3: btnSubject)
        changeFontStyle(bArea, btn2: bDate, btn3: bSubject)
        vSubjects1.hidden = false
        vArea1.hidden = false
        vHours1.hidden = true
        vDates1.hidden = false
    }
    
    @IBOutlet weak var btnHour: UIButton!
    @IBAction func btnHour(sender: AnyObject) {
        openHour()
           }

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var hour1: UIDatePicker!
    @IBOutlet weak var hours2: UIDatePicker!
    @IBOutlet weak var dates: UIDatePicker!
    @IBOutlet weak var tblSubject: UITableView!
    @IBOutlet weak var tblCities: UITableView!
    @IBOutlet weak var txtfSubject: UITextField!
    @IBOutlet weak var txtCity: SuggestiveTextField!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewSubject: UIView!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet var imgView: UIView!
    @IBOutlet weak var rangeHoursView: UIView!
    @IBOutlet weak var dateChooseView: UIView!
    
    @IBAction func clsBtn(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isValidSearch = false
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(AdvantageSearchViewController.imageTapped))
        imgPlusMenu.userInteractionEnabled = true
        imgPlusMenu.addGestureRecognizer(tapGestureRecognizer)
        googlePlacesAutocomplete.delegate = self
        
        bSubject.setTitle(NSLocalizedString("SUBJECT", comment: ""), forState: .Normal)
        bArea.setTitle(NSLocalizedString("AREA", comment: ""), forState: .Normal)
        bDate.setTitle(NSLocalizedString("DESIRED_DATE", comment: ""), forState: .Normal)
        bHours.setTitle(NSLocalizedString("RANGE_HOURS", comment: ""), forState: .Normal)
        btnSearch.setTitle(NSLocalizedString("SEARCH", comment: ""), forState: .Normal)
        
        self.imgView.backgroundColor = UIColor(patternImage: UIImage(named: "client.jpg")!)
        txtfSubject.hidden = true
        txtCity.hidden = true
        tblSubject.hidden = true
        tblCities.hidden = true
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden = true
        
        lblSubject.text = ""
        lblArea.text = ""
        lblDate.text = ""
        lblHour.text = ""

        hour1.layer.borderWidth = 1
        hours2.layer.borderWidth = 1
        dates.layer.borderWidth = 1
        lineView.layer.borderWidth = 1
        
        hour1.layer.borderColor = UIColor.blackColor().CGColor
        hours2.layer.borderColor = UIColor.blackColor().CGColor
        dates.layer.borderColor = UIColor.blackColor().CGColor
        lineView.layer.borderColor = UIColor.blackColor().CGColor
        

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AdvantageSearchViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)

        let tapSubject:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(AdvantageSearchViewController.openSubject))
        vSubjects1.addGestureRecognizer(tapSubject)
        let tapArea:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(AdvantageSearchViewController.openArea))
        vArea1.addGestureRecognizer(tapArea)
        let tapDate:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(AdvantageSearchViewController.openDate))
    vDates1.addGestureRecognizer(tapDate)
        let tapHour:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(AdvantageSearchViewController.openHour))
        vHours1.addGestureRecognizer(tapHour)
        
        txtfSubject.delegate = self
        txtCity.delegate = self
        
        self.tblSubject.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tblCities.separatorStyle = UITableViewCellSeparatorStyle.None
        
        txtfSubject.borderStyle = .None
        txtCity.borderStyle = .None
        
        dates.addTarget(self, action: #selector(AdvantageSearchViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        hour1.addTarget(self, action: #selector(AdvantageSearchViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        hours2.addTarget(self, action: #selector(AdvantageSearchViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        bSubject.titleLabel?.textAlignment = NSTextAlignment.Right
        bArea.titleLabel?.textAlignment = NSTextAlignment.Right
        bDate.titleLabel?.textAlignment = NSTextAlignment.Right
        bHours.titleLabel?.textAlignment = NSTextAlignment.Right

        dates.minimumDate = NSDate()
        
        tblSubject.layer.borderWidth = 1
        tblSubject.backgroundColor = UIColor.clearColor()
        
        tblCities.layer.borderWidth = 1
        tblCities.backgroundColor = UIColor.clearColor()
        
        let dateFormatterHours = NSDateFormatter()
        dateFormatterHours.dateFormat = "HH:mm"
        hour1.date = dateFormatterHours.dateFromString("00:00")!
        hours2.date = dateFormatterHours.dateFromString("00:00")!
        
        
        self.searchResults = Array()
        self.tblSubject.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        self.tblCities.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
    }
    
    override func viewWillLayoutSubviews() {
        
        let bounds = UIScreen.mainScreen().bounds
        
        let width = bounds.size.width
        let height = bounds.size.height
        self.view.superview!.bounds = CGRect(x:0,y:-52,width:width,height: height)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        isValidSearch = false
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color1, width: 1, any: viewHeader)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: viewSubject)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: viewArea)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: dateChooseView)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: rangeHoursView)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: vSubjects1)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: vArea1)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: vDates1)
        Colors.sharedInstance.addBottomBorderWithColor(Colors.sharedInstance.color6, width: 2, any: vHours1)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSubjectFlag == true
        {
            return filterSubArr.count // your number of cell here
        }
        else
        {

            return self.searchResults.count

            //return filterAreaArr.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell =
        tableView.dequeueReusableCellWithIdentifier("subjects")as!SubjectsTableViewCell
        
        gradientLine(cell, width: cell.layer.frame.width, height: cell.layer.frame.height)
        
        if isSubjectFlag == true
        {
            cell.setDisplayData(filterSubArr[indexPath.row].nvCategoryName)
        }
        else
        {
            let cell1 = tableView.dequeueReusableCellWithIdentifier("cellIdentifier", forIndexPath: indexPath)
            cell1.textLabel?.text = self.searchResults[indexPath.row]
            return cell1
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isSubjectFlag == true
        {
            lblSubject.text = filterSubArr[indexPath.row].nvCategoryName
            txtfSubject.text = filterSubArr[indexPath.row].nvCategoryName
            chosenDomain = filterSubArr[indexPath.row].iCategoryRowId
        }
        else
        {

            // 1
            //self.dismissViewControllerAnimated(true, completion: nil)
            tableView.hidden = true
            // 2
            let correctedAddress:String! = self.searchResults[indexPath.row].stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.symbolCharacterSet())
            let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)&sensor=false")
            
            let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) -> Void in
                // 3
                do {
                    if data != nil{
                        let dic = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableLeaves) as!  NSDictionary
                        
                        let lat = dic["results"]?.valueForKey("geometry")?.valueForKey("location")?.valueForKey("lat")?.objectAtIndex(0) as! Double
                        let lon = dic["results"]?.valueForKey("geometry")?.valueForKey("location")?.valueForKey("lng")?.objectAtIndex(0) as! Double
                        // 4
                        //self.delegate.locateWithLongitude(lon, andLatitude: lat, andTitle: self.searchResults[indexPath.row] )
                    }
                }catch {
                    print("Error")
                }
            }
            // 5
            task.resume()
            
        }
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return txtfSubject.frame.size.height
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    //MARK: - keyboard
    
    ///dismiss keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard() {
        txtCity.dismissSuggestionTableView()
        self.view.endEditing(true)
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        txtCity.hidden = true
        tblCities.hidden = true
        filterSubArr = []
        filterAreaArr = []
        tblSubject.reloadData()
        //txtCity.text = ""
        //txtfSubject.text = ""
        tblSubject.hidden = true
        txtfSubject.hidden = true
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        closeAllArrow()
        changeAllFontStyle()
        vSubjects1.hidden = false
        vArea1.hidden = false
        vHours1.hidden = false
        vDates1.hidden = false

    }
    
    //MARK: - textField
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == txtfSubject
        {
            //בדיקה שלא הכניס ספרות
            let badCharacters = NSCharacterSet.decimalDigitCharacterSet().invertedSet
            if txtfSubject.text!.rangeOfCharacterFromSet(badCharacters) == nil && txtfSubject.text != "" {
                txtfSubject.text = ""
                lblSubject.text = ""
                Alert.sharedInstance.showAlert(NSLocalizedString("ILLEGAL_DOMAIN", comment: ""), vc: self)
            }
        }
    }
    

    //for autoCopmlete:
    func searchAutocompleteEntriesWithSubstring(substring: String)
    {
        if isSubjectFlag == true
        {
            filterSubArr = []
            
            for subject in AppDelegate.arrDomainFilter
            {
                let myString:NSString! = subject.nvCategoryName as NSString
                
                let substringRange :NSRange! = myString.rangeOfString(substring)
                
                if (substringRange.location == 0)
                {
                    filterSubArr.append(subject/*.nvCategoryName*/)
                }
            }
        }
        else
        {  
        }
        tblSubject.reloadData()
    }

    ////for the autoComplete
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtCity
        {
            Global.sharedInstance.isAddressCity = true
            googlePlacesAutocomplete.input = textField.text! + string
            googlePlacesAutocomplete.fetchPlaces()
        }
        else //txtfSubject
        {
              //בשביל ה-autoComplete
            let substring = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            searchAutocompleteEntriesWithSubstring(substring)
            
            if string == "" && txtfSubject.text?.characters.count == 1
            {
                txtfSubject.text = ""
                lblSubject.text = ""
            }
        }
      

        return true
    }
    
    //MARK: - on click buttons
    func closeArrow(btn1:UIButton,btn2:UIButton,btn3:UIButton)
    {

        btn1.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn2.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn3.setTitleColor(UIColor.blackColor(), forState: .Normal)
    }
    
    func closeAllArrow()
    {

        
        btnSubject.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btnArea.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btnDate.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btnHour.setTitleColor(UIColor.blackColor(), forState: .Normal)
    }
    
    func changeAllFontStyle()
    {
        bSubject.setTitleColor(UIColor.blackColor(), forState: .Normal)
        bArea.setTitleColor(UIColor.blackColor(), forState: .Normal)
        bDate.setTitleColor(UIColor.blackColor(), forState: .Normal)
        bHours.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        bSubject.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        bArea.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        bDate.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        bHours.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
    }

    
    func changeFontStyle(btn1:UIButton,btn2:UIButton,btn3:UIButton)
    {
        btn1.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn2.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btn3.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        btn1.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        btn2.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
        btn3.titleLabel!.font = UIFont (name: "OpenSansHebrew-Light", size: 18)
    }
    
    func openArrow(btnText:UIButton,btn:UIButton)
    {
        
        btn.setTitleColor(Colors.sharedInstance.color3, forState: .Normal)
        btnText.setTitleColor(Colors.sharedInstance.color3, forState: .Normal)
        btnText.titleLabel?.font = UIFont (name: "OpenSansHebrew-Bold", size: 18)
    }
    

    func gradientLine(cellView:UIView,width:CGFloat,height:CGFloat)
    {
        
        let lineForCells:UIImageView = UIImageView(frame: CGRectMake(0,height + (height / 2.2), width , 1.5))
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [
            UIColor.whiteColor().CGColor,
            UIColor(red: 76/255, green: 76/255, blue: 76/255, alpha: 1.0).CGColor,
            UIColor.whiteColor().CGColor]
        gradient.locations = [0.0, 0.5, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: width + (width / 3), height: 1)
        
        lineForCells.layer.insertSublayer(gradient, atIndex: 0)
        cellView.addSubview(lineForCells)
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        if (sender as UIDatePicker) == dates
        {
            chosenDate = sender.date
            let dateString = dateFormatter.stringFromDate(sender.date)
            //bDate.setTitle(dateString, forState: .Normal)
            lblDate.text = dateString

           dateToServer = Global.sharedInstance.convertNSDateToString(sender.date)
        }
        else
        {
            hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hour1.date))
            mmFromHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hour1.date))
            
            hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hours2.date))
            mmToHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hours2.date))
            
            if mmFromHour != 0
            {
                hhFromHour = hhFromHour + mmFromHour / 60
            }
            
            if mmToHour != 0
            {
                hhToHour = hhToHour + mmToHour / 60
            }
            if hhFromHour > hhToHour
            {
                if sender == hours2
                {
                    
                    hour1.date =  hours2.date
                }
                else if sender == hour1
                {
                    hours2.date = hour1.date
                }
            }
            
            
            let outputFormatter: NSDateFormatter = NSDateFormatter()
            outputFormatter.dateFormat = "HH:mm"
            //24hr time format
            let hoursString = outputFormatter.stringFromDate(self.hour1.date) + "-" + outputFormatter.stringFromDate(self.hours2.date)
            // bHours.setTitle(hoursString,forState: .Normal)
            
            //אם שעת התחלה וסיום זהות - לא נשמר
            if outputFormatter.stringFromDate(self.hour1.date) == outputFormatter.stringFromDate(self.hours2.date)
            {
                lblHour.text = ""
                chosenFromHour = ""
                chosenToHour = ""
                hhFromHour = 0.0
                hhToHour = 0.0
            }
            else
            {
                lblHour.text = hoursString
                let hoursArr = hoursString.componentsSeparatedByString("-")
                if hoursArr.count == 2
                {
                    chosenFromHour = hoursArr[0]
                    chosenToHour = hoursArr[1]
                }
            }
            
            
        }
    }
    
    
    func dismissViewController()
    {
        self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
        delegate.dismissViewController()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view!.isDescendantOfView(tblSubject) || touch.view!.isDescendantOfView(vSubjects1) || touch.view!.isDescendantOfView(vArea1) || touch.view!.isDescendantOfView(vDates1) || touch.view!.isDescendantOfView(vHours1)) {
            
            return false
        }
        return true
    }
    
    
    //-MARK: SPGooglePlacesAutocompleteQueryDelegate
    
    func googlePlaceReload(listPlaces:[SPGooglePlacesAutocompletePlace])
    {
        if isSearch{
            googlePlacesAutocomplete.googleGeocoded(referenceToGeocoded:listPlaces[0].reference)
            isSearch = false
        }
        else{
            var placesName:[String] = []
            for place in listPlaces{
                placesName += [place.name]
            }
            listAutocompletePlace=listPlaces
            txtCity.setSuggestions(placesName)
            txtCity.matchStrings(txtCity.text)
            txtCity.showSuggestionTableView()
            //            tableView.hidden = false
            
        }
    }
    
    func googlePlaceGeocoded(latitude:Double, longitude:Double)
    {
        //        mapView.clear()
        //        var marker = GMSMarker()
        //        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        //        marker.title = txfAddressSearch.text
        //        mapView.selectedMarker = marker
        //        marker.map = mapView
        //
        //        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 15)
        //        mapView.camera = camera
        self.txtCity.text = ""
    }
    
    func googlePlaceReverseGeocode(address: String) {
        txtCity.text = address
    }
    func googlePlaceReverseGeocode(address: String , country: String ,city: String)
    {
        
    }


    
    
    
    func openSubject()
    {
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        txtfSubject.placeholder = NSLocalizedString("SUBJECT_PLACE_HOLDER", comment: "")
        dismissKeyboard()
        txtCity.hidden = true
        tblCities.hidden = true
        filterSubArr = []
        filterAreaArr = []
        tblSubject.reloadData()
        //txtCity.text = ""
        //txtfSubject.text = ""
        isSubjectFlag = true
        tblSubject.hidden = false
        txtfSubject.hidden = false
        txtfSubject.becomeFirstResponder()
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bSubject, btn: btnSubject)
        closeArrow(btnArea, btn2: btnDate, btn3: btnHour)
        changeFontStyle(bArea, btn2: bDate, btn3: bHours)
        vSubjects1.hidden = true
        vArea1.hidden = true
        vHours1.hidden = true
        vDates1.hidden = true

    }
    
    func openArea()  {
        dismissKeyboard()
        txtCity.placeholder = NSLocalizedString("AREA_PLACE_HOLDER", comment: "")
        txtCity.hidden = false
        txtCity.becomeFirstResponder()
        txtfSubject.hidden = true
        filterAreaArr = []
        filterSubArr = []
        tblSubject.reloadData()
        tblCities.reloadData()
        //txtCity.text = ""
        isSubjectFlag = false
        txtCity.hidden = false
        tblSubject.hidden = true
        tblCities.hidden = false
        dates.hidden = true
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bArea, btn: btnArea)
        closeArrow(btnSubject, btn2: btnDate, btn3: btnHour)
        changeFontStyle(bSubject, btn2: bDate, btn3: bHours)
        // hideViews(vArea1)
        vSubjects1.hidden = false
        vArea1.hidden = true
        vHours1.hidden = true
        vDates1.hidden = true
    }
    func openDate() {
        dismissKeyboard()
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        //txtfSubject.text = ""
        txtfSubject.hidden = true
        //txtCity.text = ""
        txtCity.hidden = true
        tblSubject.hidden = true
        tblCities.hidden = true
        dates.hidden = false
        hour1.hidden = true
        hours2.hidden = true
        lineView.hidden  = true
        
        openArrow(bDate, btn: btnDate)
        closeArrow(btnArea, btn2: btnSubject, btn3: btnHour)
        changeFontStyle(bArea, btn2: bSubject, btn3: bHours)
        vSubjects1.hidden = false
        vArea1.hidden = false
        vHours1.hidden = true
        vDates1.hidden = true

    }
    
    func openHour(){
        dismissKeyboard()
        if txtCity.text != ""
        {
            lblArea.text = txtCity.text
            chosenCity = txtCity.text!
        }
        else
        {
            chosenCity = ""
        }
        //txtfSubject.text = ""
        txtfSubject.hidden = true
        //txtCity.text = ""
        txtCity.hidden = true
        tblSubject.hidden = true
        tblCities.hidden = true
        dates.hidden = true
        hour1.hidden = false
        hours2.hidden = false
        lineView.hidden  = false
        
        openArrow(bHours, btn: btnHour)
        closeArrow(btnArea, btn2: btnDate, btn3: btnSubject)
        changeFontStyle(bArea, btn2: bDate, btn3: bSubject)
        vSubjects1.hidden = false
        vArea1.hidden = false
        vHours1.hidden = true
        vDates1.hidden = false

    }
    
    
    func imageTapped(){
        
        let storyboard1:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewCon:MenuPlusViewController = storyboard1.instantiateViewControllerWithIdentifier("MenuPlusViewController") as! MenuPlusViewController
        viewCon.delegate = self
        viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
        self.presentViewController(viewCon, animated: true, completion: nil)
    }

    func openFromMenu(con:UIViewController)
    {
        self.presentViewController(con, animated: true, completion: nil)
    }
    
}
