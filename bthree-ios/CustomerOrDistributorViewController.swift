//
//  CustomerOrDistributorViewController.swift
//  bthree-ios
//
//  Created by User on 22.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
//דף מעבר בין ספק ללקוח (לבדוק אם משתמשים)
class CustomerOrDistributorViewController: UIViewController{
    
    @IBOutlet weak var viewProvider: UIView!
    @IBOutlet weak var viewCustomer: UIView!
    
    var delegate:openChooseUserDelegate!=nil
    var delegate1:openCustomerDetailsDelegate!=nil
    var supplierStoryBoard:UIStoryboard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        Crashlytics.sharedInstance().crash()
        supplierStoryBoard = UIStoryboard(name: "SupplierExist", bundle: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CustomerOrDistributorViewController.openProvider))
        self.viewProvider.addGestureRecognizer(tap)
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CustomerOrDistributorViewController.openCustomer))
        self.viewCustomer.addGestureRecognizer(tap1)
//        self.navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openProvider()
    {
//        var dicGetProviderDetails:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
//        
//        dicGetProviderDetails = Global.sharedInstance.defaults.valueForKey("verificationPhone") as! Dictionary<String,AnyObject>
//        
//        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//        dicToServer["nvPhone"] = dicGetProviderDetails["phone"]
//        dicToServer["nvPassword"] = dicGetProviderDetails["verification"]
//        
//        api.sharedInstance.GetProviderDetails(dicToServer, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
//            
//            if responseObject["Error"]!!["ErrorCode"] as! Int == -2 || responseObject["Error"]!!["ErrorCode"] as! Int == -1
//            {
//                Alert.sharedInstance.showAlert("ארעה שגיאה בלתי צפויה", vc: self)
//            }
//            else
//            {
//                Global.sharedInstance.isProvider = true
//                
//                print(responseObject["Result"])
//                
//                //קבלת פרטי הספק מה-response
//                if responseObject["Error"]!!["ErrorCode"] as! Int != -3//ספק לא קיים
//                    
//                {
//                    Global.sharedInstance.currenProviderDetails = Global.sharedInstance.currenProviderDetails.dicToObjGeneralDetails(responseObject["Result"] as! Dictionary<String,AnyObject>)
//                }
//            }
//            
//            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
//        })
        
        //----------------פונקציה נוספת:-----------------------
        
//        api.sharedInstance.GetProviderBuisnessDetails(dicToServer, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
//            
//            if responseObject["Error"]!!["ErrorCode"] as! Int == -3//ספק לא קיים
//            {
//                var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//                
//                dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
//                
//                if Reachability.isConnectedToNetwork() == false
//                {
//                    Alert.sharedInstance.showAlertDelegate("לא נמצא חיבור לאינטרנט")
//                }
//                else
//                {
//                 print(dic)   //שמירת פרטי הלקוח
//                    api.sharedInstance.AddProviderUser(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
//                        
//                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
//                    })
//                }
//                
//                self.dismissViewControllerAnimated(false, completion: nil)
//                //פתיחת רישום ספק
//                
//                if let _ = self.delegate
//                {
//                    self.delegate.openBuisnessDetails()
//                }
//                    
//                else
//                {
//                    Global.sharedInstance.isProvider = true
//                    let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
//                    
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    
//                    let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
//                    
//                    let rgister:RgisterModelViewController = storyboard.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
//                    
//                    frontViewController.pushViewController(rgister, animated: false)
//                    
//                    rgister.delegateFirstSection = viewCon1
//                    rgister.delegateSecond1Section = viewCon1
//                    
//                    let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
//                    
//                    let mainRevealController = SWRevealViewController()
//                    
//                    mainRevealController.frontViewController = frontViewController
//                    mainRevealController.rearViewController = rearViewController
//                    
//                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
//                    window.rootViewController = mainRevealController
//                }
//            }
//            else if responseObject["Error"]!!["ErrorCode"] as! Int == -2 || responseObject["Error"]!!["ErrorCode"] as! Int == -1
//            {
//                Alert.sharedInstance.showAlert("ארעה שגיאה בלתי צפויה", vc: self)
//            }
//            else
//            {
//                if responseObject["Error"]!!["ErrorCode"] as! Int == -4//ספק קיים
//                {
//                    Global.sharedInstance.isProvider = true
//                    
//                    //קבלת פרטי הספק מה-response
//                    Global.sharedInstance.currentProviderBuisnessDetails = Global.sharedInstance.currentProviderBuisnessDetails.dicToProvider(responseObject["Result"] as! Dictionary<String,AnyObject>)
//                    
//                    //מעבר לספק קיים
//                    let frontviewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
//                    
//                    let storyboardSupplierExist = UIStoryboard(name: "SupplierExist", bundle: nil)
//                    let vc = storyboardSupplierExist.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
//                    
//                    frontviewcontroller?.pushViewController(vc, animated: false)
//                    
//                    
//                    //initialize REAR View Controller- it is the LEFT hand menu.
//                    
//                    let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
//                    
//                    let mainRevealController = SWRevealViewController()
//                    
//                    mainRevealController.frontViewController = frontviewcontroller
//                    mainRevealController.rearViewController = rearViewController
//                    
//                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
//                    window.rootViewController = mainRevealController
//                }
//                else if responseObject["Error"]!!["ErrorCode"] as! Int == 0//לא גמר את כל תהליך הרישום
//                {
//                    print(responseObject["Result"])
//                    Global.sharedInstance.RegisterNotEnd = true
//                    
//                    //קבלת פרטי הספק מה-response
//                    Global.sharedInstance.currentProviderBuisnessDetails = Global.sharedInstance.currentProviderBuisnessDetails.dicToProvider(responseObject["Result"] as! Dictionary<String,AnyObject>)
//                    
//                    self.dismissViewControllerAnimated(false, completion: nil)
//                    //פתיחת רישום ספק
//                    
//                    if let _ = self.delegate
//                    {
//                        self.delegate.openBuisnessDetails()
//                    }
//                        
//                    else
//                    {
//                        Global.sharedInstance.isProvider = true
//                        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
//                        
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        
//                        let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
//                        
//                        let rgister:RgisterModelViewController = storyboard.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
//                        
//                        frontViewController.pushViewController(rgister, animated: false)
//                        
//                        rgister.delegateFirstSection = viewCon1
//                        rgister.delegateSecond1Section = viewCon1
//                        
//                        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
//                        
//                        let mainRevealController = SWRevealViewController()
//                        
//                        mainRevealController.frontViewController = frontViewController
//                        mainRevealController.rearViewController = rearViewController
//                        
//                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
//                        window.rootViewController = mainRevealController
//                    }
//                }
//            }
//            
//            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
//        })


        ///--------------פונקציה נוספת----------
                var dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­["iUserId"] = Global.sharedInstance.currentUser.iUserId
       
            api.sharedInstance.getProviderAllDetails(dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                var provDetailsAll:ProviderDetailsObj = ProviderDetailsObj()
                if responseObject["Error"]!!["ErrorCode"] as! Int == -1
                {
                    Alert.sharedInstance.showAlert("שגיאה", vc: self)
                }
                else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//ספק לא קיים
                {
//                    var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//                    
//                    dic["iUserId"] = Global.sharedInstance.currentUser.iUserId

//                    if Reachability.isConnectedToNetwork() == false
//                    {
//                        Alert.sharedInstance.showAlertDelegate("לא נמצא חיבור לאינטרנט")
//                    }
//                    else
//                    {

//                        print(dic)   //שמירת פרטי הלקוח
//                        api.sharedInstance.AddProviderUser(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
//                            
//                            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
//                        })
//                    }
                    
                    self.dismissViewControllerAnimated(false, completion: nil)
                    //פתיחת רישום ספק
                    
                    if let _ = self.delegate
                    {
                        self.delegate.openBuisnessDetails()
                    }
                        
                    else
                    {
                        Global.sharedInstance.isProvider = true
                        let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
                        
                        let rgister:RgisterModelViewController = storyboard.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
                        
                        frontViewController.pushViewController(rgister, animated: false)
                        
                        rgister.delegateFirstSection = viewCon1
                        rgister.delegateSecond1Section = viewCon1
                        
                        let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                        
                        let mainRevealController = SWRevealViewController()
                        
                        mainRevealController.frontViewController = frontViewController
                        mainRevealController.rearViewController = rearViewController
                        
                        let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                        window.rootViewController = mainRevealController
                    }
                }
                else
                {
                   Global.sharedInstance.isProvider = true
                    
                   provDetailsAll = provDetailsAll.dicToProviderDetailsObj(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    
                    //מעבר לספק קיים
                    let frontviewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
                    
                    let storyboardSupplierExist = UIStoryboard(name: "SupplierExist", bundle: nil)
                    let vc = storyboardSupplierExist.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
                    
                    frontviewcontroller?.pushViewController(vc, animated: false)
                    
                    
                    //initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                }
            
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                
        })
        
        
        
        
        
        //ישן לפי מה ששמור ב-default
        
        //if Global.sharedInstance.defaults.valueForKey("isSupplierRegistered") != nil
        //        {
        //            var dicIsSupplierRegister:Dictionary<String,Bool> = Global.sharedInstance.defaults.valueForKey("isSupplierRegistered") as! Dictionary<String,Bool>
        //
        //            if dicIsSupplierRegister["supplierRegistered"] == true
        //            {
        //                Global.sharedInstance.isProvider = true
        //                let frontviewcontroller = storyboard!.instantiateViewControllerWithIdentifier("navigation") as? UINavigationController
        //
        //                let storyboardSupplierExist = UIStoryboard(name: "SupplierExist", bundle: nil)
        //                let vc = storyboardSupplierExist.instantiateViewControllerWithIdentifier("CalendarSupplierViewController") as! CalendarSupplierViewController
        //
        //                frontviewcontroller?.pushViewController(vc, animated: false)
        //
        //
        //                //initialize REAR View Controller- it is the LEFT hand menu.
        //
        //                let rearViewController = storyboard!.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        //
        //                let mainRevealController = SWRevealViewController()
        //
        //                mainRevealController.frontViewController = frontviewcontroller
        //                mainRevealController.rearViewController = rearViewController
        //
        //                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        //                window.rootViewController = mainRevealController
        //            }
        //            else
        //            {
        //                var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        //
        //                dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        //
        //                if Reachability.isConnectedToNetwork() == false
        //                {
        //                    Alert.sharedInstance.showAlertDelegate("לא נמצא חיבור לאינטרנט")
        //                }
        //                else
        //                {
        //                    api.sharedInstance.AddProviderUser(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
        //
        //                        },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        //                    })
        //                }
        //
        //                self.dismissViewControllerAnimated(false, completion: nil)
        //
        //                if let _ = delegate
        //                {
        //                    delegate.openBuisnessDetails()
        //                }
        //
        //                else
        //                {
        //                    Global.sharedInstance.isProvider = true
        //                    let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        //
        //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //
        //                    let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        //
        //                    let rgister:RgisterModelViewController = storyboard.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        //
        //                    frontViewController.pushViewController(rgister, animated: false)
        //
        //                    rgister.delegateFirstSection = viewCon1
        //                    rgister.delegateSecond1Section = viewCon1
        //
        //                    let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        //
        //                    let mainRevealController = SWRevealViewController()
        //
        //                    mainRevealController.frontViewController = frontViewController
        //                    mainRevealController.rearViewController = rearViewController
        //
        //                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        //                    window.rootViewController = mainRevealController
        //                }
        //            }
        //        }
        
        //=======================
        
        //        else
        //        {
        //            var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        //
        //            dic["iUserId"] = Global.sharedInstance.currentUser.iUserId
        //
        //            if Reachability.isConnectedToNetwork() == false
        //            {
        //                Alert.sharedInstance.showAlertDelegate("לא נמצא חיבור לאינטרנט")
        //            }
        //            else
        //            {
        //                api.sharedInstance.AddProviderUser(dic, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
        //
        //                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        //                })
        //            }
        //
        //            self.dismissViewControllerAnimated(false, completion: nil)
        //
        //            if let _ = delegate
        //            {
        //                delegate.openBuisnessDetails()
        //            }
        //
        //            else
        //            {
        //                Global.sharedInstance.isProvider = true
        //                let viewCon1:GlobalDataViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GlobalDataViewController") as! GlobalDataViewController
        //
        //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //
        //                let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
        //
        //                let rgister:RgisterModelViewController = storyboard.instantiateViewControllerWithIdentifier("RgisterModelViewController") as! RgisterModelViewController
        //
        //                frontViewController.pushViewController(rgister, animated: false)
        //
        //                rgister.delegateFirstSection = viewCon1
        //                rgister.delegateSecond1Section = viewCon1
        //
        //                let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        //
        //                let mainRevealController = SWRevealViewController()
        //
        //                mainRevealController.frontViewController = frontViewController
        //                mainRevealController.rearViewController = rearViewController
        //
        //                let window :UIWindow = UIApplication.sharedApplication().keyWindow!
        //                window.rootViewController = mainRevealController
        //            }
        //        }
    }
    
    func openCustomer()
    {
        //קבלת פרטי הלקוח
        var dicGetCustomerDetails:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicGetCustomerDetails["iUserId"] = Global.sharedInstance.currentUser.iUserId
        
        api.sharedInstance.GetCustomerDetails(dicGetCustomerDetails, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Result"] != nil {
                
                Global.sharedInstance.currentUser = Global.sharedInstance.currentUser.dicToUser(responseObject["Result"] as! Dictionary<String,AnyObject>)
            }
            //print(Global.sharedInstance.currentUser)
            
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
        })
        
        
        Global.sharedInstance.isProvider = false
        self.dismissViewControllerAnimated(false, completion: nil)
        
        if let _ = delegate1
        {
            delegate1.openCustomerDetails()
        }
            
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let clientStoryBoard = UIStoryboard(name: "ClientExist", bundle: nil)
            
            let frontViewController = storyboard.instantiateViewControllerWithIdentifier("navigation") as! UINavigationController
            
            let rgister:ModelCalenderViewController = clientStoryBoard.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as! ModelCalenderViewController
            rgister.modalPresentationStyle = UIModalPresentationStyle.Custom
            frontViewController.pushViewController(rgister, animated: false)
            
            let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
            
            let mainRevealController = SWRevealViewController()
            
            mainRevealController.frontViewController = frontViewController
            mainRevealController.rearViewController = rearViewController
            
            self.view.window!.rootViewController = mainRevealController
            self.view.window?.makeKeyAndVisible()
            
            //            let viewCon:ModelCalenderViewController = storyboard?.instantiateViewControllerWithIdentifier("ModelCalenderViewController") as! ModelCalenderViewController
            //            self.navigationController?.navigationBarHidden = false
            //            self.navigationController?.pushViewController(viewCon, animated: false)
        }
    }
    
    
}
