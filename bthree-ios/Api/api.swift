//
//  api.swift
//
//  Created by User on 21.2.2016.
//  Copyright © 2016 Gili. All rights reserved.
//

import UIKit

class api: NSObject
{
//var url: String = "http://10.0.0.88/BThereWS/BThereWS.svc/"
//הרגיל
var url: String = "http://ws.webit-track.com/BThereWS/BThereWS.svc/"
    //כשמעלים גירסא
//var url: String = "http://ws.webit-track.com/BThereLiveWs/BThereWS.svc/"
    
    let manager = AFHTTPRequestOperationManager()
    
    typealias successBlock = (AFHTTPRequestOperation! ,AnyObject!)-> Void
    
    typealias failureBlock = (AFHTTPRequestOperation? , NSError?) ->Void
    
    class var sharedInstance: api
    {
        struct Static  {
            static var onceToken: dispatch_once_t = 0
            
            static var instance: api? = nil
        }
        
        dispatch_once(&Static.onceToken)
            {
                
                Static.instance = api()
                
        }
        return Static.instance!
    }
    
    override init()
    {
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: NSJSONReadingOptions.AllowFragments) as AFJSONResponseSerializer
        
        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as Set<NSObject>
    }
    func buildUrl(path:String)->String
    {
        let pathUrl:String = url.stringByAppendingString(path)
        
        return pathUrl
    }

    func CheckMailValidity(params:Dictionary<String,String>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckMailValidity"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure:failure)
    }
    
    func CheckPhoneValidity(params:Dictionary<String,String>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckPhoneValidity"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure:failure
        )
    }
    
    func RegisterUser(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "RegisterUser"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,
        failure: failure
//            { (operation: AFHTTPRequestOperation?,error: NSError!) in
//            print("Error: ", error.localizedDescription)
//            Alert.sharedInstance.showAlertDelegate("נראה כי החיבור לאינטרנט אינו פעיל")
//        }
        )
    }
    func UpdateUser(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "UpdateUser"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,
                     failure: failure
            //            { (operation: AFHTTPRequestOperation?,error: NSError!) in
            //            print("Error: ", error.localizedDescription)
            //            Alert.sharedInstance.showAlertDelegate("נראה כי החיבור לאינטרנט אינו פעיל")
            //        }
        )
    }

    func GetFieldsAndCatg(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetFieldsAndCatg"
        
        manager.POST(self.buildUrl(path), parameters: nil, success: success,failure: failure
//                     failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
//            if Global.sharedInstance.showAlertInAppDelegate == false
//            {
//                Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
//                Global.sharedInstance.showAlertInAppDelegate = true
//            }
//        }
        )
    }
    
    func AddProviderUser(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderUser"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
             Global.sharedInstance.currentProvider.iUserId = responseObject["Result"] as! Int

            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    
    func AddProviderBuisnessDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderBuisnessDetails"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            Global.sharedInstance.currentProvider.iIdBuisnessDetails = responseObject["Result"] as! Int
            
            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func AddProviderGeneralDetails(params:Dictionary<String,AnyObject> , success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderGeneralDetails"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            print(responseObject)
            
            }, failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    // מקבלת מספר טלפון ושולחת SMS עם קוד אימות ומחזירה את הקוד גם כן לאפליקציה על מנת לעשות השוואה עם הקוד שמכניס
    func GetAndSmsValidationCode(params:Dictionary<String,String> , success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetAndSmsValidationCode"
        
        manager.POST(self.buildUrl(path), parameters: params, success:success, failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func GetSysAlertsList(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetSysAlertsList"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure
        )
    }
    
    
//    func GetSysAlertsList(params:Dictionary<String,AnyObject> , success: (successBlock)!, failure: (failureBlock)!)
//    {
//        let path = "GetSysAlertsList"
//        
//        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
//            
//            let sysAlert:SysAlerts = SysAlerts()
//            
//            Global.sharedInstance.arrSysAlerts = sysAlert.sysToArray(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
//            
//            if Global.sharedInstance.arrSysAlerts.count != 0
//            {
//                Global.sharedInstance.dicSysAlerts = sysAlert.sysToDic(Global.sharedInstance.arrSysAlerts)
//                Global.sharedInstance.arrayDicForTableViewInCell[0]![1] = sysAlert.SysnvAletName(8)
//                Global.sharedInstance.arrayDicForTableViewInCell[2]![1] = sysAlert.SysnvAletName(9)
//                Global.sharedInstance.arrayDicForTableViewInCell[2]![2] = sysAlert.SysnvAletName(12)
//                Global.sharedInstance.arrayDicForTableViewInCell[3]![1] = sysAlert.SysnvAletName(10)
//                Global.sharedInstance.arrayDicForTableViewInCell[3]![2] = sysAlert.SysnvAletName(12)
//                //2do
//                //במקום זה צריך להיות textField
//                //            Global.sharedInstance.arrayDicForTableViewInCell[4]![1] = sysAlert.SysnvAletName(12)
//            }
//            }, failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
//                print("Error: ", error.localizedDescription)
//                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
//        })
//    }
    
    func AddProviderAlertSettings(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderAlertSettings"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            print(responseObject)
            
            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func AddProviderBuisnessProfile(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderBuisnessProfile"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            
            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    
    func SyncContacts(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "SyncContacts"
        
        manager.POST(self.buildUrl(path), parameters: params, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func RestoreVerCode(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "RestoreVerCode"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    
    func LoginUser(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "LoginUser"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func AddProviderAllDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddProviderAllDetails"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure)
    }
    
    
    func GetProviderDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetProviderDetails"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func GetProviderBuisnessDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetProviderBuisnessDetails"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    //get iUserId,returns customer's details
    func GetCustomerDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetCustomerDetails"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure
        )
    }

    func SearchProviders(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "SearchProviders"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure)
    }
    
    //ומחזירה רשימה של אוביקטים מסוג SearchResulstsObj

    func SearchByKeyWord(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "SearchByKeyWord"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure
        )
    }
    
    
    func SearchWordCompletion(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "SearchWordCompletion"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure
        )
    }
    
    func getServicesProviderForSupplierfunc(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "getServicesProviderForSupplier"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: failure
        )
    }
    func getProviderServicesForSupplier(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "getProviderServicesForSupplier"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }

    
    func GetCouponsForProvider(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetCouponsForProvider"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }

    
    func ­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderContact­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderContact­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func GetProviderAlertSettings(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderAlertSettings"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func GetProviderProfile(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetProviderProfile"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    //get:iUserId,returns:provider all details he registered
    //if provider not exist -> returns -2
    func getProviderAllDetails(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "getProviderAllDetails"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success, failure: failure
        )
    }
    
    func SendAgainSms(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "SendAgainSms"
        
        manager.POST(self.buildUrl(path), parameters: nil, success: { (operation:AFHTTPRequestOperation?, responseObject:AnyObject) -> Void in
            
            print(responseObject["Result"])
            
            },  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
    
    //GetProviderListForCustomer
    func GetProviderListForCustomer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetProviderListForCustomer"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    //פונקציה שמוחקת ספקים ללקוח (שלא יראה אותם ברשימת נותני השרות שלו) אך לא באמת מחקת את הציוות.
    func RemoveProviderFromCustomer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "RemoveProviderFromCustomer"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    //GetFreeDaysForServiceProvider
    func GetFreeDaysForServiceProvider(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetFreeDaysForServiceProvider"
        
        manager.POST(self.buildUrl(path), parameters: params,success: success,  failure: failure
        )
    }
    
    //GetCustomerOrders
    func GetCustomerOrders(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetCustomerOrders"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
//newOrder
    func newOrder(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "newOrder"
        
        manager.POST(self.buildUrl(path), parameters: params,success: success,  failure: failure

        )
    }
   
    func CheckProviderExistByMail(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckProviderExistByMail"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
    
    func CheckProviderExistByPhone(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckProviderExistByPhone"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
    
    //invoke server function to get provider details
    func getProviderAllDetails(iUserId:Int)
    {
        var dicUserId:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicUserId["iUserId"] = iUserId
        
        //קבלת פרטי הספק
        api.sharedInstance.getProviderAllDetails(dicUserId, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -1//שגיאה
            {
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == -2//ספק לא קיים
            {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("supplierNameRegistered")
            }
                
            else
            {
                if let _:Dictionary<String,AnyObject> = responseObject["Result"] as? Dictionary<String,AnyObject>
                {
                    Global.sharedInstance.currentProviderDetailsObj = Global.sharedInstance.currentProviderDetailsObj.dicToProviderDetailsObj(responseObject["Result"] as! Dictionary<String,AnyObject>)
                    
                    //שמירת שם העסק במכשיר
                   var dicForDefault:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    
                   dicForDefault["nvSupplierName"] = Global.sharedInstance.currentProviderDetailsObj.providerBuisnessDetailsObj.nvSupplierName
                    
                    Global.sharedInstance.defaults.setObject(dicForDefault, forKey: "supplierNameRegistered")
                }
            }
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                if AppDelegate.showAlertInAppDelegate == false
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                    AppDelegate.showAlertInAppDelegate = true
                }
        })
    }
    //2do
    func addNewCoupon(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "addNewCoupon"
        
        manager.POST(self.buildUrl(path), parameters: params,success: success,  failure: failure
        )
    }

    func DeleteFromWaitingList(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "DeleteFromWaitingList"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }

    func getWaitingListForCustomer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "getWaitingListForCustomer"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
                print("Error: ", error.localizedDescription)
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    //2do
    func addUserToWaitingList(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "addUserToWaitingList"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
    
    func CheckIfOrderIsAvailable(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckIfOrderIsAvailable"
        
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertAppDelegate(NSLocalizedString("NO_CONNECTION", comment: ""))
        })
    }
    
    func GetAlertSettingsForCustomer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetAlertSettingsForCustomer"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }
    
    func AddAlertSettingsForCustomer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "AddAlertSettingsForCustomer"
        manager.POST(self.buildUrl(path), parameters: params, success: success,  failure: { (operation: AFHTTPRequestOperation?,error: NSError!) in
            print("Error: ", error.localizedDescription)
            Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
        })
    }

}






