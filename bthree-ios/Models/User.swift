//
//  User.swift
//  bthree-ios
//
//  Created by Lior Ronen on 2/10/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var iUserId:Int = 0
    
    var nvUserName:String = ""
    var nvFirstName:String = ""
    var nvLastName:String = ""
    var dBirthdate:NSDate = NSDate()//* = added later
    
    var nvMail:String = ""
    var nvAdress:String = ""
    var iCityType:Int = 0
    var nvPhone:String = ""
    var nvPassword:String = ""
    var nvVerification:String = ""
    var bAutomaticUpdateApproval:Bool = false
    var bDataDownloadApproval:Bool = false
    var bTermOfUseApproval:Bool = false
    var bAdvertisingApproval:Bool = false//* = added later
    var iUserStatusType:Int = 0
    //var iUserType:Int = 0 //*//ספק = 18,לקוח = 17
    var bIsGoogleCalendarSync:Bool = false//* = added later

    var nvImage:String = ""
    var iCreatedByUserId:Int = 0
    var iLastModifyUserId:Int = 0
    var iSysRowStatus:Int = 0
    var dMarriageDate:NSDate = NSDate()//תאריך נישואין לצורך עדכון בלבד
    var  iCalendarViewType:Int = Int() //לצורך עדכון בלבד תצוגת יומן
    override init() {
        iUserId = 0
        
        nvUserName = ""
        nvFirstName = ""
        nvLastName = "" //*
        dBirthdate = NSDate()
        nvMail = ""
        nvAdress = ""
        iCityType = 1
        nvPhone = ""
        nvPassword = "1234"
        nvVerification = "1234"
        bAutomaticUpdateApproval = false
        bDataDownloadApproval = true
        bTermOfUseApproval = false
        bAdvertisingApproval = false //*
        iUserStatusType = 0
        //iUserType = 0 //*
        bIsGoogleCalendarSync = false //*
        nvImage = ""
        iCreatedByUserId = 0
        iLastModifyUserId = 0
        iSysRowStatus = 0
    }
    
    init(_iUserId:Int,_nvUserName:String,_nvFirstName:String,_nvLastName:String,_dBirthdate:NSDate,_nvMail:String,_nvAdress:String,_iCityType:Int,_nvPhone:String,_nvPassword:String,_nvVerification:String,_bAutomaticUpdateApproval:Bool,_bDataDownloadApproval:Bool,_bAdvertisingApproval:Bool,_bTermOfUseApproval:Bool,_iUserStatusType:Int,
        //_iUserType:Int
        _bIsGoogleCalendarSync:Bool,_nvImage:String,_iCreatedByUserId:Int,_iLastModifyUserId:Int,_iSysRowStatus:Int) {
        
        iUserId = _iUserId
            
        nvUserName = _nvUserName
        nvFirstName = _nvFirstName
        nvLastName = _nvLastName
        dBirthdate = _dBirthdate //*
        nvMail = _nvMail
        nvAdress = _nvAdress
        iCityType = _iCityType
        nvPhone = _nvPhone
        nvPassword = _nvPassword
        nvVerification = _nvVerification
        bAutomaticUpdateApproval = _bAutomaticUpdateApproval
        bDataDownloadApproval = _bDataDownloadApproval
        bTermOfUseApproval = _bTermOfUseApproval
        bAdvertisingApproval = _bAdvertisingApproval //*
        iUserStatusType = _iUserStatusType
        //iUserType = _iUserType
        bIsGoogleCalendarSync = _bIsGoogleCalendarSync //*
        nvImage = _nvImage
        iCreatedByUserId = _iCreatedByUserId
        iLastModifyUserId = _iLastModifyUserId
        iSysRowStatus = _iSysRowStatus
    }

    
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = iUserId
        
        dic["nvUserName"] = nvUserName
        dic["nvFirstName"] = nvFirstName
        dic["nvLastName"] = nvLastName
        
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
        let componentsBDay = calendar.components([.Day, .Month, .Year], fromDate: dBirthdate)
        
        if componentsCurrent.year != componentsBDay.year || componentsCurrent.month != componentsBDay.month || componentsCurrent.day != componentsBDay.day
        {
            dic["dBirthdate"] = Global.sharedInstance.convertNSDateToString(dBirthdate) //*
        }
        else
        {
            dic["dBirthdate"] = nil
        }
//        componentsBDay = calendar.components([.Day, .Month, .Year], fromDate: dMarried)
//        if componentsCurrent.year != componentsBDay.year || componentsCurrent.month != componentsBDay.month || componentsCurrent.day != componentsBDay.day
//        {
//            dic["dMarried"] = Global.sharedInstance.convertNSDateToString(dMarried) //*
//        }
//        else
//        {
             dic["dMarriageDate"] = nil
//        }
        
        dic["nvMail"] = nvMail
        dic["nvAdress"] = nvAdress
        dic["iCityType"] = iCityType
        dic["nvPhone"] = nvPhone
        dic["nvPassword"] = nvPassword
        dic["nvVerification"] = nvVerification
        dic["bAutomaticUpdateApproval"] = bAutomaticUpdateApproval
        dic["bDataDownloadApproval"] = bDataDownloadApproval
        dic["bTermOfUseApproval"] = bTermOfUseApproval
        dic["bAdvertisingApproval"] = bAdvertisingApproval //*
        dic["iUserStatusType"] = iUserStatusType
        //dic["iUserType"] = iUserType
        dic["bIsGoogleCalendarSync"] = bIsGoogleCalendarSync //*
        dic["nvImage"] = nvImage
        dic["iCreatedByUserId"] = iCreatedByUserId
        dic["iLastModifyUserId"] = iLastModifyUserId
        dic["iSysRowStatus"] = iSysRowStatus
        return dic
    }
    
    func dicToUser(dic:Dictionary<String,AnyObject>)->User
    {
        let user:User = User()
        user.iUserId = Global.sharedInstance.parseJsonToInt(dic["iUserId"]!)
        
        user.nvUserName = Global.sharedInstance.parseJsonToString(dic["nvUserName"]!)
        user.nvFirstName = Global.sharedInstance.parseJsonToString(dic["nvFirstName"]!)
        user.nvLastName = Global.sharedInstance.parseJsonToString(dic["nvLastName"]!)
        user.dBirthdate = Global.sharedInstance.getStringFromDateString(Global.sharedInstance.parseJsonToString(dic["dBirthdate"]!)) //*
        user.dMarriageDate = Global.sharedInstance.getStringFromDateString(Global.sharedInstance.parseJsonToString(dic["dMarriageDate"]!))
        user.nvMail = Global.sharedInstance.parseJsonToString(dic["nvMail"]!)
//        user.nvAdress = Global.sharedInstance.parseJsonToString(dic["nvAdress"]!)
        user.iCityType = Global.sharedInstance.parseJsonToInt(dic["iCityType"]!)
        user.nvPhone = Global.sharedInstance.parseJsonToString(dic["nvPhone"]!)
        user.nvPassword = Global.sharedInstance.parseJsonToString(dic["nvPassword"]!)
        user.nvVerification = Global.sharedInstance.parseJsonToString(dic["nvVerification"]!)
        user.bAutomaticUpdateApproval = dic["bAutomaticUpdateApproval"] as! Bool
        user.bDataDownloadApproval = dic["bDataDownloadApproval"] as! Bool
        user.bTermOfUseApproval = dic["bTermOfUseApproval"] as! Bool
        user.bAdvertisingApproval = dic["bAdvertisingApproval"] as! Bool //*
        user.iUserStatusType = Global.sharedInstance.parseJsonToInt(dic["iUserStatusType"]!)
        //dic["iUserType"] = iUserType
        user.bIsGoogleCalendarSync = dic["bIsGoogleCalendarSync"] as! Bool //*
        user.nvImage = Global.sharedInstance.parseJsonToString(dic["nvImage"]!)
        user.iCreatedByUserId = Global.sharedInstance.parseJsonToInt(dic["iCreatedByUserId"]!)
        user.iLastModifyUserId = Global.sharedInstance.parseJsonToInt(dic["iLastModifyUserId"]!)
        user.iSysRowStatus = Global.sharedInstance.parseJsonToInt(dic["iSysRowStatus"]!)
        user.iCalendarViewType = Global.sharedInstance.parseJsonToInt(dic["iCalendarViewType"]!)
        
        return user
    }
    func usersToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<User>{
        
        var arrDomain:Array<User> = Array<User>()
        var objUser:User = User()
        
        for i in 0  ..< arrDic.count 
        {
            objUser = dicToUser(arrDic[i])
            arrDomain.append(objUser)
        }
        return arrDomain
    }
    func getDicToEdit()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = iUserId
        
        dic["nvUserName"] = nvUserName
        dic["nvFirstName"] = nvFirstName
        dic["nvLastName"] = nvLastName
        
//        let calendar:NSCalendar = NSCalendar.currentCalendar()
//        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
//        
//        let componentsBDay = calendar.components([.Day, .Month, .Year], fromDate: dBirthdate)
//        
//        if componentsCurrent.year != componentsBDay.year || componentsCurrent.month != componentsBDay.month || componentsCurrent.day != componentsBDay.day
//        {
//            dic["dBirthdate"] = Global.sharedInstance.convertNSDateToString(dBirthdate) //*
//        }
//        else
//        {
//            dic["dBirthdate"] = nil
//        }
//        let componentsMDay = calendar.components([.Day, .Month, .Year], fromDate: dMarried)
//        if componentsCurrent.year != componentsMDay.year || componentsCurrent.month != componentsMDay.month || componentsCurrent.day != componentsMDay.day
//        {
//            dic["dMarriageDate"] = Global.sharedInstance.convertNSDateToString(dMarried) //*
//        }
//        else
//        {
//            dic["dMarriageDate"] = nil
//        }
        dic["nvMail"] = nvMail
        dic["nvAdress"] = nvAdress
        dic["iCityType"] = iCityType
        dic["nvPhone"] = nvPhone
        dic["nvPassword"] = nvPassword
        dic["nvVerification"] = nvVerification
        dic["bAutomaticUpdateApproval"] = true
        dic["bDataDownloadApproval"] = true// bDataDownloadApproval
        dic["bTermOfUseApproval"] = bTermOfUseApproval
        dic["bAdvertisingApproval"] = true//bAdvertisingApproval //*
        dic["iUserStatusType"] = iUserStatusType
        //dic["iUserType"] = iUserType
        dic["bIsGoogleCalendarSync"] = bIsGoogleCalendarSync //*
        dic["nvImage"] = nvImage
        dic["iCreatedByUserId"] = iCreatedByUserId
        dic["iLastModifyUserId"] = iLastModifyUserId
        dic["iSysRowStatus"] = iSysRowStatus
        dic["dBirthdate"] = Global.sharedInstance.convertNSDateToString(dBirthdate) //*
        dic["dMarriageDate"] = Global.sharedInstance.convertNSDateToString(dMarriageDate)
        
        dic["iCalendarViewType"] = iCalendarViewType// change
        return dic
    }

}
