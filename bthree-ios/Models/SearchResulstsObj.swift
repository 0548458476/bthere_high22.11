//
//  SearchResulstsObj.swift
//  Bthere
//
//  Created by Lior Ronen on 18/08/2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class SearchResulstsObj: NSObject {

    var iProviderId:Int = 0
    var nvProviderName:String = ""
    var nvProviderSlogan:String = ""
    var nvAdress:String = ""
    var iDistance:Float = 0.0
    var iCustomerRank:Int = 0
    var IInternalRank:Double = 0.0
    var nvProviderLogo:String = ""
    var nvProviderHeader:String = ""
    
    override init() {
         iProviderId = 0
         nvProviderName = ""
         nvProviderSlogan = ""
         nvAdress = ""
         iDistance = 0.0
         iCustomerRank = 0
         IInternalRank = 0.0
         nvProviderLogo = ""
         nvProviderHeader = ""
    }
    
    init(_iProviderId:Int,_nvProviderName:String,_nvProviderSlogan:String,_nvAdress:String,_iDistance:Float,_iCustomerRank:Int,_IInternalRank:Double,_nvProviderLogo:String,_nvProviderHeader:String) {
        
        iProviderId = _iProviderId
        nvProviderName = _nvProviderName
        nvProviderSlogan = _nvProviderSlogan
        nvAdress = _nvAdress
        iDistance = _iDistance
        iCustomerRank = _iCustomerRank
        IInternalRank = _IInternalRank
        nvProviderLogo = _nvProviderLogo
        nvProviderHeader = _nvProviderHeader
    }
    
    //בשביל לאתחל את currentProviderToCustomer שאז לא צריך לאתחל את כל הפרטים
    init(_nvProviderName:String,_nvAdress:String,_nvProviderSlogan:String,_nvProviderLogo:String){
        
        nvProviderName = _nvProviderName
        nvAdress = _nvAdress
        nvProviderSlogan = _nvProviderSlogan
        nvProviderLogo = _nvProviderLogo
    }
    func objProvidersToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<SearchResulstsObj>{
        
        var arrProv:Array<SearchResulstsObj> = Array<SearchResulstsObj>()
        var Prov:SearchResulstsObj = SearchResulstsObj()
        
        for i in 0  ..< arrDic.count
        {
            Prov = dicToProviders(arrDic[i])
            arrProv.append(Prov)
        }
        return arrProv
    }
    
    func dicToProviders(dic:Dictionary<String,AnyObject>)->SearchResulstsObj
    {

        let provider:SearchResulstsObj = SearchResulstsObj()
        
        provider.iProviderId = Global.sharedInstance.parseJsonToInt(dic["iProviderId"]!)//√
        
        if dic["nvProviderName"] != nil
        {
            provider.nvProviderName = dic["nvProviderName"]! as! String
        }
        else
        {
          provider.nvProviderName =  ""
        }
        
        provider.nvProviderName = Global.sharedInstance.parseJsonToString(dic["nvProviderName"]!)//√
        provider.nvProviderSlogan = Global.sharedInstance.parseJsonToString(dic["nvProviderSlogan"]!)//√
        provider.nvAdress = Global.sharedInstance.parseJsonToString(dic["nvAdress"]!)//√
        provider.iDistance = Global.sharedInstance.parseJsonToFloat(dic["iDistance"]!)//√
        provider.iCustomerRank = Global.sharedInstance.parseJsonToInt(dic["iCustomerRank"]!)//√
        provider.IInternalRank = Global.sharedInstance.parseJsonToDouble(dic["IInternalRank"]!)//√
        provider.nvProviderLogo = Global.sharedInstance.parseJsonToString(dic["nvProviderLogo"]!)//√
        provider.nvProviderHeader = Global.sharedInstance.parseJsonToString(dic["nvProviderHeader"]!)//√
        
        return provider

        
           }

}
