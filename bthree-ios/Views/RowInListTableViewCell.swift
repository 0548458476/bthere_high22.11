//
//  RowInListTableViewCell.swift
//  Bthere
//
//  Created by User on 23.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
//ספק קיים
class RowInListTableViewCell: UITableViewCell {

    var dateTurn:String = ""
    var arrImg:Array<String> = ["18@x1.png","5@x1.png","Phone.png"]
    
    var arrText:Array<String> = ["פרטי התור","ביטול התור","חייג"]
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var hourTurn:String = ""
    var indexPathRow:Int = 0
    var hourDesc = ""
    var desc = ""
    var eventFrom = 0
    var sortArrEvnt:[(String,Array<allKindEventsForListDesign>)] = []//מכיל את כל הארועים הנ״ל בצורה ממויינת לפי תאירך ולפי שעות לכל יום
    
    @IBOutlet weak var collList: UICollectionView!
    
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
            lineView.backgroundColor = Colors.sharedInstance.color6
            self.contentView.addSubview(lineView)
            // Configure the view for the selected state
        }
    
    
    func setDisplayData(index:Int)
    {
        indexPathRow = index
    }
    
    func setDisplayData(_hourDesc:String,_desc:String,_EventFrom:Int,_index:Int){
        
        
        hourDesc = _hourDesc
        desc = _desc
        eventFrom = _EventFrom
        indexPathRow = _index
        
        collList.reloadData()
        
        
//        if EventFrom == 1//ביזר
//        {
//            lblDesc.font = UIFont.boldSystemFontOfSize(16)
//            lblHour.font = UIFont.boldSystemFontOfSize(16)
//        }
//        
//        lblDesc.text = desc
//        lblHour.text = hourDesc
    }
    
    
}
//MARK: - CollectionView

extension RowInListTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //        let event =  Global.sharedInstance.arrEvents[indexPathRow]
        
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("RowInListCollectionViewCell", forIndexPath: indexPath) as! RowInListCollectionViewCell
            //                    let componentsStart = calendar.components([.Hour, .Minute], fromDate: event.startDate)
            //
            //                    let componentsEnd = calendar.components([.Hour, .Minute], fromDate: event.endDate)
            //
            //                    let hourS = componentsStart.hour
            //                    let minuteS = componentsStart.minute
            //
            //                    let hourE = componentsEnd.hour
            //                    let minuteE = componentsEnd.minute
            //
            //                    var hourS_Show:String = hourS.description
            //                    var hourE_Show:String = hourE.description
            //                    var minuteS_Show:String = minuteS.description
            //                    var minuteE_Show:String = minuteE.description
            //
            //                    if hourS < 10
            //                    {
            //                        hourS_Show = "0\(hourS)"
            //                    }
            //                    if hourE < 10
            //                    {
            //                        hourE_Show = "0\(hourE)"
            //                    }
            //                    if minuteS < 10
            //                    {
            //                        minuteS_Show = "0\(minuteS)"
            //                    }
            //                    if minuteE < 10
            //                    {
            //                        minuteE_Show = "0\(minuteE)"
            //                    }
            //                    let st =  "\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)"
            cell.setDisplayData(hourDesc, desc:desc)
            hourTurn = hourDesc
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("OpenRowListCollectionViewCell", forIndexPath: indexPath) as! OpenRowListCollectionViewCell
            cell1.dateTurn = dateTurn
            cell1.hourTurn = hourTurn
            cell1.setDisplayData(arrText[indexPath.row - 1],img: arrImg[indexPath.row - 1],index: indexPath.row)
            return cell1
        }
    }
    
//     func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        switch indexPath.row {
//        case 1:
//            return
//        case 2:
//            return
//        case 3:
//            return
//        default:
//            return
//        }
//    }
    
}

extension RowInListTableViewCell : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            //        let itemsPerRow:CGFloat = 1
            //        let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 5.5
            // let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
    }
    
}

