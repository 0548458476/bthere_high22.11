//
//  NewEventDateTableViewCell.swift
//  Bthere
//
//  Created by User on 12.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//קבע אירוע -  הסל של התאריך
class NewEventDateTableViewCell: UITableViewCell {

    var delegate:editTextInCellDelegate!=nil
    @IBOutlet weak var dpDate: UIDatePicker!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        dpDate.layer.borderWidth = 1
        dpDate.layer.borderColor = UIColor.blackColor().CGColor
        
        dpDate.backgroundColor = UIColor.clearColor()
        dpDate.setValue(UIColor.blackColor(), forKeyPath: "textColor")
        dpDate.datePickerMode = UIDatePickerMode.Date
        dpDate.setValue(false, forKey: "highlightsToday")
        dpDate.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        dpDate.layer.borderWidth = 1
        dpDate.layer.borderColor = UIColor.blackColor().CGColor
        
        dpDate.backgroundColor = UIColor.clearColor()
        dpDate.setValue(UIColor.blackColor(), forKeyPath: "textColor")
        dpDate.datePickerMode = UIDatePickerMode.Date
        dpDate.setValue(false, forKey: "highlightsToday")
        dpDate.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        delegate.editTextInCell(1, text: sender.date)
        //Global.sharedInstance.dateEvent = sender.date
 
    }

}
