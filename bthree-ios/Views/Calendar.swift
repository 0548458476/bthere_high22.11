//
//  Calendar.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/16/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class Calendar: NSObject {
    let calendar = NSCalendar.currentCalendar()
    class var sharedInstance: Calendar
    {
        struct Static  {
            static var onceToken: dispatch_once_t = 0
            static var instance: Calendar? = nil
        }
        dispatch_once(&Static.onceToken)
            {
                Static.instance = Calendar()
        }
        
        return Static.instance!
    }
    var carrentDate:NSDate = NSDate()
    func getNumsDays(date:NSDate)->Int{
        
        //let dateComponents = NSDateComponents()
        //let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Month, .Year], fromDate: date)
        let dateFinal = calendar.dateFromComponents(components)!
        let range = calendar.rangeOfUnit(.Day, inUnit: .Month, forDate: dateFinal)
        let numDays = range.length
        //print(numDays) // 31
        return numDays
        
    }
    
    func getDayOfWeek(today:NSDate)->Int? {
        
        //        let formatter  = NSDateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        //        if let todayDate = formatter.dateFromString(today) {
        // let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = calendar.components(.Weekday, fromDate: today)
        let weekDay = myComponents.weekday
        //print(weekDay)
        return weekDay
        //        } else {
        //            return nil
        //        }
    }
    
    func getFirstDay(date:NSDate)->NSDate{//מחזירה את היום הראשון בחודש שנשלח
        
        // let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day, .Month, .Year], fromDate: date)
        components.day = 1//TODO:לבדק שתמיד חוזר הראשון
        let dateFinal = calendar.dateFromComponents(components)!
        //print(dateFinal)
       // print(NSDate())
        return dateFinal
    }
    //מוסיפה חודש בודד לתאריך
    func addMonth(date:NSDate) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: 1, toDate: date, options: [])//
        
        return date!
    }
    //מןסיפה מספר חודשים רצוי לתאריך
    func addMonths(date:NSDate,numMonthAdd:Int) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: numMonthAdd, toDate: date, options: [])//
        
        return date!
    }
    
    func addDay(date:NSDate) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 1, toDate: date, options: [])//
        
        return date!
    }
    func addDays(date:NSDate, numDays: Int) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: numDays, toDate: date, options: [])//
        
        return date!
    }

    func reduceDay(date:NSDate) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -1, toDate: date, options: [])//
        
        return date!
    }
    func MinusDay(date:NSDate, day:Int) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -day, toDate: date, options: [])//
        
        return date!
    }

    //func that reduce days from a month nd add days and return the day of month
    func reduceAddDay(date:NSDate,reduce:Int,add:Int) -> Int{
        
        var date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -reduce, toDate: date, options: [])//
        
        date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: add, toDate: date!, options: [])
        
        let components = calendar.components([.Day], fromDate: date!)
        
        let dayInMonth = components.day
        
        return dayInMonth
        
    }
    func getMonth(date:NSDate,reduce:Int,add:Int) -> Int{
        
        var date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -reduce, toDate: date, options: [])//
        
        date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: add, toDate: date!, options: [])
        
             let componentsmonth = calendar.components([.Month], fromDate: date!)
        
        let monthInMonth = componentsmonth.month
        
        return monthInMonth
        
    }
    func getYear(date:NSDate,reduce:Int,add:Int) -> Int{
        
        var date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -reduce, toDate: date, options: [])//
        
        date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: add, toDate: date!, options: [])
        
        let componentsmonth = calendar.components([.Year], fromDate: date!)
        
        let YearInDate = componentsmonth.year
        
        return YearInDate
        
    }
    func getDay(date:NSDate,reduce:Int,add:Int) -> Int{
        
//        var date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -reduce, toDate: date, options: [])//
//        
//        date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: add, toDate: date!, options: [])
        
        let componentsmonth = calendar.components([.Day], fromDate: date)
        
        let DayInDate = componentsmonth.day
        
        return DayInDate
        
    }

    
    func reduceAddDay_Date(date:NSDate,reduce:Int,add:Int) -> NSDate{
        
        var date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -reduce, toDate: date, options: [])//
        
        date = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: add, toDate: date!, options: [])
        
        return date!
    }
    
    func removeMonth(date:NSDate) -> NSDate{
        let date = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: -1, toDate: date, options: [])//
        
        return date!
    }
    
    func from( year:Int, month:Int, day:Int) -> NSDate {
        let c = NSDateComponents()
        c.year = year
        c.month = month
        c.day = day
        
        let gregorian = NSCalendar(identifier:NSCalendarIdentifierGregorian)
        let date = gregorian!.dateFromComponents(c)
        return date!
    }
    
    func logicalOneYearAgo(from: NSDate) -> NSDate {
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        let offsetComponents: NSDateComponents = NSDateComponents()
        offsetComponents.year = -1
        return gregorian.dateByAddingComponents(offsetComponents, toDate: from, options:[])!
    }
    
    func oneYearNext(from:NSDate) -> NSDate {
      
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        let offsetComponents: NSDateComponents = NSDateComponents()
        offsetComponents.year = 1
        let nextYear: NSDate = gregorian.dateByAddingComponents(offsetComponents, toDate: from, options: [])!
return nextYear
    }
    
    func getPartsOfDate(from:NSDate , to:NSDateComponents) ->NSDate
    {

      //   calendar.timeZone = NSTimeZone(abbreviation: "GMT")!
        let componentsFrom = calendar.components([.Day, .Month, .Year], fromDate: from) //year month day

      //  let componentsTo = calendar.components([.Minute,.Hour,.Day, .Month, .Year], fromDate: to)
             //print(event.startDate)
        componentsFrom.hour = 0
        componentsFrom.minute = 0

        componentsFrom.hour = to.hour
        componentsFrom.minute =  to.minute
        
        var d:NSDate = NSDate()

        let dateFinal = calendar.dateFromComponents(componentsFrom)!
        return dateFinal

    }
    
    func saveEventInDeviceCalander()
    {
        
        //    var eventStore : EKEventStore = EKEventStore()
        //
        //    // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        //
        //    eventStore.requestAccessToEntityType(.Event, completion: { (granted, error) in
        //        if (granted) && (error == nil) {
        //            let event = EKEvent(eventStore: eventStore)
        //            var s =  "\(lblNameServicer.text) \(lblServiceType.text)"//Global.sharedInstance.currentProviderToCustomer.nvProviderSlogan)"
        //            event.notes = "Bthere"
        //            event.title = s // change slogan to servicename
        //            event.allDay = false
        //            event.startDate =     Global.sharedInstance.eventBthereDateStart
        //            event.endDate =      Global.sharedInstance.eventBthereDateEnd
        //            event.calendar = eventStore.defaultCalendarForNewEvents
        //            do {
        //                try eventStore.saveEvent(event, span: .ThisEvent)
        //                print("success - " + event.title)
        //            } catch let e as NSError {
        //                print(e)
        //                //                    completion?(success: false, error: e)
        //                return
        //            }
        //            //                completion?(success: true, error: nil)
        //        } else {
        //
        //            //                completion?(success: false, error: error)
        //        }
        //    })
        
    }

}
