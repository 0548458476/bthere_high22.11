//
//  ServicesTableViewCell.swift
//  bthree-ios
//
//  Created by User on 14.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol reloadCollectionDelegate {
    func reloadCollection(providerServiceSort:Array<objProviderServices>)
}

class ServicesTableViewCell: UITableViewCell,reloadCollectionDelegate {

    @IBOutlet weak var collServices: UICollectionView!
    var index:Int = 0//מיקום השרות בטבלה הכללית
    var timer: NSTimer? = nil
//    var arrayServiceDesc:Array<String> = ["תספורת אישה","פן","תספורת גבר","גוונים"]
//    var arrayServiceHours:Array<String> = ["20 second","40 second","1.30-2.30","20-40"]
//    var arrayServicePrices:Array<String> = ["150-250","150-70","150-450","120-50"]

    var ProviderServicesArray:Array<objProviderServices> = Array<objProviderServices>()
    var delegateMoreInfo:showMoreInfoDelegate!=nil
    var indexPathRow:Int = 0
    var parent_ListServicesViewController:ListServicesViewController = ListServicesViewController()
    var multipleSelected = false

    func addBottomBorder(any:UIView,color:UIColor)
    {
        let borderBottom = CALayer()
        borderBottom.frame = CGRectMake(0, CGRectGetHeight(any.layer.frame), CGRectGetWidth(any.layer.frame) + (any.layer.frame.width / 3) , 2)
        
        borderBottom.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderBottom)
    }
   
    var arrText:Array<String> = [NSLocalizedString("MORE_INFORMATION", comment: ""),NSLocalizedString("MULTIPLE_SELECT", comment: ""),NSLocalizedString("ORDER", comment: "")]
    var arrTextForCellSelected:Array<String> = [NSLocalizedString("MORE_INFORMATION", comment: ""),NSLocalizedString("MULTIPLE_SELECT", comment: ""),NSLocalizedString("ORDER_MARKED", comment: "")]
    var arrTextAfterCellSelected:Array<String> = [NSLocalizedString("MORE_INFORMATION", comment: ""),NSLocalizedString("MULTIPLE_SELECT", comment: ""),""]
    // var arrImages:Array<String> = ["v-small.png","circle.png","more-info.png"]
    var arrImages:Array<String> = ["28.png","circleEmpty1.png","27.png"]
    var arrImagesForCellSelected:Array<String> = ["28.png","29.png","27.png"]
    var arrImagesCellSelected:Array<String> = ["28.png","29.png",""]
    var arrImagesAfterCellSelected:Array<String> = ["28.png","circleEmpty1.png",""]
    
    //MARK: - initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Global.sharedInstance.indexCellSelected = -1
        Global.sharedInstance.isFirstCellSelected = false
        Global.sharedInstance.multipleServiceName = []
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //בפעם הראשונה שפותחים את הדף - תוצאת החיפוש הראשונה נגללת מעט שמאלה כדי להראות למשתמש את האפשרות לגלול
        if index == 0 && Global.sharedInstance.firstService == true
        {
            Global.sharedInstance.firstService = false
            
            UIView.animateWithDuration(2.5, delay: 1.5, options:UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                
                self.collServices.layoutIfNeeded()
                
                let rightOffset = CGPointMake(self.collServices.contentSize.width - self.collServices.bounds.size.width, 0)
                self.collServices.setContentOffset(rightOffset, animated: true)
                
                self.timer = NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: #selector(self.doDelayed), userInfo: nil, repeats: false)
                
            }) { (completed:Bool) -> Void in
                
            }
        }
        else//fix 2.3
        {
            if  #available(iOS 10.0, *){
                self.collServices.layoutIfNeeded()
                self.collServices.setContentOffset(CGPoint(x: 0 , y: 0), animated: false)
                
            }
        }
        Global.sharedInstance.arrayServicesNames = []
        for item in ProviderServicesArray {
            Global.sharedInstance.arrayServicesNames.append(item.nvServiceName)
        }
    }
    
    
    func setDisplayData(index:Int)
    {
        indexPathRow = index
//        if Global.sharedInstance.indexCellSelected != -1
//        {
    
            collServices.reloadData()
//        }
    }
    
    func doDelayed(t: NSTimer) {
        
        UIView.animateWithDuration(2.5, delay: 1.5, options:UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            self.collServices.layoutIfNeeded()
            self.collServices.setContentOffset(CGPoint(x: 0 , y: 0), animated: true)
            
        }) { (completed:Bool) -> Void in
            
        }
        timer = nil
    }
    
    //MARK: - Delegate Function
    
    func reloadCollection(providerServiceSort:Array<objProviderServices>)
    {
        ProviderServicesArray = providerServiceSort
        if ProviderServicesArray.count > 0
        {
            collServices.reloadData()
        }
        else
        {
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_RESOLTS", comment: ""), vc: Global.sharedInstance.viewCon!)
        }
    }
}

// MARK: - collectionView
extension ServicesTableViewCell : UICollectionViewDataSource {
  

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("BigService", forIndexPath: indexPath) as! BigServiceCollectionViewCell
            cell.delegate = delegateMoreInfo
            cell.index = indexPathRow
            cell.setDisplayData(ProviderServicesArray[indexPathRow])
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("SmallInService", forIndexPath: indexPath) as! SmallInServiceCollectionViewCell
            cell1.index = index
            cell1.delegate = delegateMoreInfo
            cell1.index = indexPathRow
            cell1.delegateReloadTbl = parent_ListServicesViewController
            
            if indexPath.row != 1
            {
                if Global.sharedInstance.indexCellSelected != -1 //בחרו מישהו אחד לפחות
                {
                    if cell1.tag == 1 && indexPath.row == 2//סל של בחירה מרובה ובחור
                    {
                        multipleSelected = true
                        if Global.sharedInstance.isFirstCellSelected == false//הראשון
                        {
                            cell1.setDisplayData(arrImagesForCellSelected[indexPath.row - 1], text: arrTextForCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                            
                            //isFirst = true
                        }
                        else
                        {
                            cell1.setDisplayData(arrImagesCellSelected[indexPath.row - 1], text: arrTextAfterCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                        }
                    }
                    else if cell1.tag == 0 && indexPath.row == 2//סל של בחירה מרובה ולא בחור
                    {
                        cell1.setDisplayData(arrImages[indexPath.row - 1], text: arrTextAfterCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                    }
                        
                    else if multipleSelected == true && indexPath.row == 3
                    {
                        multipleSelected = false
                        if Global.sharedInstance.isFirstCellSelected == false//הראשון
                        {
                            cell1.setDisplayData(arrImagesForCellSelected[indexPath.row - 1], text: arrTextForCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                            Global.sharedInstance.isFirstCellSelected = true
                        }
                        else
                        {
                            cell1.setDisplayData(arrImagesCellSelected[indexPath.row - 1], text: arrTextAfterCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                        }
                    }
                    else//לא בחור
                    {
                        cell1.setDisplayData(arrImagesAfterCellSelected[indexPath.row - 1], text: arrTextAfterCellSelected[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                    }
                }
                    
                else//לא בחרו עדיין אף אחד
                {
                    cell1.setDisplayData(arrImages[indexPath.row - 1], text: arrText[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
                }
            }
            else
            {
                cell1.setDisplayData(arrImages[indexPath.row - 1], text: arrText[indexPath.row - 1],index: indexPath.row, serviceName: ProviderServicesArray[indexPathRow].nvServiceName)
            }
            return cell1
        }
    }
    
}

extension ServicesTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            //        let itemsPerRow:CGFloat = 1
            //        let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 4.5
            // let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
}
}
