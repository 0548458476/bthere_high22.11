//
//  NewEventHoursTableViewCell.swift
//  Bthere
//
//  Created by User on 12.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

//קבע אירוע -  הסל של השעות
class NewEventHoursTableViewCell: UITableViewCell {

    var delegate:editTextInCellDelegate!=nil
    @IBOutlet weak var dpToHour: UIDatePicker!
    
    @IBOutlet weak var dpFromHour: UIDatePicker!
    
    @IBOutlet weak var viewLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        dpFromHour.layer.borderWidth = 1
        dpToHour.layer.borderWidth = 1
        viewLine.layer.borderWidth = 1
        
        dpFromHour.layer.borderColor = UIColor.whiteColor().CGColor
        dpToHour.layer.borderColor = UIColor.whiteColor().CGColor
        viewLine.layer.borderColor = UIColor.whiteColor().CGColor
        
        dpFromHour.backgroundColor = UIColor.blackColor()
        dpFromHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpFromHour.datePickerMode = UIDatePickerMode.Time
        dpFromHour.setValue(false, forKey: "highlightsToday")
        dpFromHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        dpToHour.backgroundColor = UIColor.blackColor()
        dpToHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpToHour.datePickerMode = UIDatePickerMode.Time
        dpToHour.setValue(false, forKey: "highlightsToday")
        dpToHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        dpFromHour.layer.borderWidth = 1
        dpToHour.layer.borderWidth = 1
        viewLine.layer.borderWidth = 1
        
        dpFromHour.layer.borderColor = UIColor.whiteColor().CGColor
        dpToHour.layer.borderColor = UIColor.whiteColor().CGColor
        viewLine.layer.borderColor = UIColor.whiteColor().CGColor
        
        dpFromHour.backgroundColor = UIColor.blackColor()
        dpFromHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpFromHour.datePickerMode = UIDatePickerMode.Time
        dpFromHour.setValue(false, forKey: "highlightsToday")
        dpFromHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        dpToHour.backgroundColor = UIColor.blackColor()
        dpToHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dpToHour.datePickerMode = UIDatePickerMode.Time
        dpToHour.setValue(false, forKey: "highlightsToday")
        dpToHour.addTarget(self, action: #selector(self.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {


            let dateFormatter = NSDateFormatter()
            dateFormatter.timeStyle = .NoStyle
            dateFormatter.dateFormat = "HH:mm"
        
        delegate.editTextInCell(2, text: "\(dateFormatter.stringFromDate(dpFromHour.date))-\(dateFormatter.stringFromDate(dpToHour.date))")
            
            // דליגייט שעורך את הלייבל בסקשן הנוכחי עבור כל דייט פיקר
        
    }
    


}
