//
//  WorkerInListTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/6/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol editWorkerDelegate{
    func reloadTableForEdit(tag:Int,my:WorkerInListTableViewCell)
}
class WorkerInListTableViewCell: UITableViewCell {
    var delegate:editWorkerDelegate! = nil
    @IBOutlet var lblWorkerName: UILabel!
    var isOpen:Bool = false
    var isOpenHours:Bool = false
    var row:Int = 0
    var isEdit:Int = 0
    @IBOutlet weak var btnCoverEdit: UIButton!
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet var btnEdit: UIButton!
    var isOpenHoursForCell:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }
    
    func setDisplayData(st:String){
        lblWorkerName.text = st
    }
    
    @IBAction func btnEditWorker(sender: UIButton) {
        self.isEdit = 1
        delegate.reloadTableForEdit(self.tag,my: self)
    }
    
    @IBAction func btntry(sender: UIButton) {
        self.isEdit = 1
        delegate.reloadTableForEdit(self.tag,my: self)
    }
    @IBAction func btnDeleteWorker(sender: UIButton) {
        
        let alert = UIAlertController(title: "", message:
           NSLocalizedString("DELETE_WORKER", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
            let edit = self.isEdit
            self.isEdit = 0
            Global.sharedInstance.generalDetails.arrObjServiceProviders.removeAtIndex(self.tag)
            if Global.sharedInstance.generalDetails.arrObjServiceProviders.count == 0{
                Global.sharedInstance.isFromSave = false
                Global.sharedInstance.isOpenNewWorker = false
                //----
                Global.sharedInstance.isOpenWorker = false
                //----
                Global.sharedInstance.isOpenHoursForPlus = false
                Global.sharedInstance.isOpenHoursForPlusAction = false
                Global.sharedInstance.isSetDataNull = true
                Global.sharedInstance.isOpenHours = false
                self.delegate.reloadTableForEdit(self.tag,my: self)
            }
            else
            {
                self.isEdit = 2
                self.delegate.reloadTableForEdit(self.tag,my: self)
            }
            
            if edit == 1
            {
                Global.sharedInstance.selectedCell[2] = false
                Global.sharedInstance.currentEditCellOpen = -1
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        
        // alert.setValue(attributedString, forKey: "attributedMessage")
        
        Global.sharedInstance.globalData.presentViewController(alert, animated: true, completion: nil)
            }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func layoutSubviews() {

        
    }
}
