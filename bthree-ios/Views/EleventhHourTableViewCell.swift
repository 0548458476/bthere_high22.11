//
//  EleventhHourTableViewCell.swift
//  bthree-ios
//
//  Created by User on 14.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class EleventhHourTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLogo: UIImageView!
    
    @IBOutlet weak var viewLogo: UIView!
    
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var txtViewText: UITextView!
    
    @IBOutlet weak var lblDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        viewLogo.layer.borderWidth = 1
//        viewLogo.layer.borderColor = UIColor.blackColor().CGColor
        
        let border = CALayer()
        
        let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
        if language == "he"
        {
            border.frame = CGRectMake(0, 0, 1, CGRectGetHeight(viewLogo.layer.frame) + (self.contentView.frame.width * 0.0328))
        }
        else
        {
            border.frame = CGRectMake((self.contentView.frame.width * 0.033) + viewLogo.layer.frame.width , 0, 1, CGRectGetHeight(viewLogo.layer.frame) + 16)
        }
        border.backgroundColor = UIColor.blackColor().CGColor
        
        viewLogo.layer.addSublayer(border)
        txtViewText.editable = false
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(img:String,date:String,text:String)
    {
    imageLogo.image = UIImage(named: img)
    lblDate.text = date
    txtViewText.text = text
    }

}
