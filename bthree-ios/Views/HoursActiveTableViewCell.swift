//
//  HoursActiveTableViewCell.swift
//  Bthere
//
//  Created by User on 5.7.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol datePickerDelegate
{
    func setDatePickerNull()
}

protocol hoursActiveDelegate {
    func checkValidityHours(index:Int)
}

protocol enabledBtnDaysDelegate {
    func enabledBtnDays()
    func enabledTrueBtnDays()
}
protocol ZeroDPMaxMinDelegte {
    func ZeroDPMaxMin()
}
//שעות פעילות בהרשמה
class HoursActiveTableViewCell: UITableViewCell,hoursActiveDelegate,enabledBtnDaysDelegate,datePickerDelegate,ZeroDPMaxMinDelegte {
    
    //MARK: - Properties
    
    var generic = Generic()
    
    var DayFlagArr:Array<Int> = [0,0,0,0,0,0,0]
    var delagetReloadHeight:reloadTblDelegate!=nil
    
    let dateFormatter = NSDateFormatter()
    
    //MARK: - Outlet
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewSelectAll: UIView!
    @IBOutlet weak var lblHoursShow: UILabel!
    @IBOutlet weak var btnDay1: dayCheckBox!
    @IBOutlet weak var btnDay2: dayCheckBox!
    @IBOutlet weak var btnDay3: dayCheckBox!
    @IBOutlet weak var btnDay4: dayCheckBox!
    @IBOutlet weak var btnDay5: dayCheckBox!
    @IBOutlet weak var btnDay6: dayCheckBox!
    @IBOutlet weak var btnDay7: dayCheckBox!
    @IBOutlet weak var viewSelectAllDays: UIView!
    @IBOutlet weak var btnSelectAllDays: UIButton!
    @IBOutlet weak var dtFromHour: UIDatePicker!
    @IBOutlet weak var dtToHour: UIDatePicker!
    @IBOutlet weak var lblChooseAll: UILabel!
    
    @IBAction func btnSelectAllDays(sender: AnyObject)
    {
     isValidSelectAllDays()   
    }
    
    //בלחיצה על יום
    @IBAction func btnDay(sender: AnyObject)
    {
        if Global.sharedInstance.currentEditCellOpen == 1//משעות פעילות
        {
            if Global.sharedInstance.addRecess == false//בוחר שעות ולא הפסקות
            {
                             //בדיקת תקינות ליום הקודם
                if (sender as! dayCheckBox).isCecked == false//רוצה להדליק
                {
                    (sender as! dayCheckBox).isCecked = true
                    
                    if Global.sharedInstance.currentBtnDayTag != -1
                    {
                        Global.sharedInstance.lastBtnDayTag = Global.sharedInstance.currentBtnDayTag
                    }
                    else
                    {
                        Global.sharedInstance.lastBtnDayTag = sender.tag
                    }
                    
                    Global.sharedInstance.currentBtnDayTag = sender.tag
                    
                    //בדיקת תקינות ליום הקודם
                    checkValidityHours(Global.sharedInstance.lastBtnDayTag)
                    
                    if Global.sharedInstance.fIsValidHours[Global.sharedInstance.lastBtnDayTag] == false && Global.sharedInstance.isHoursSelected[Global.sharedInstance.lastBtnDayTag] == true && Global.sharedInstance.lastBtnDayTag != Global.sharedInstance.currentBtnDayTag
                    {
                        Global.sharedInstance.fIsValidHours[Global.sharedInstance.lastBtnDayTag] = true
                    }
//מופעל רק על היום הקודם...
                    //אם גלל שעת התחלה וסיום זהות,
                    //(אם גולל שעות לא תקינות מופעל ב handeleDatePicker)
                    if Global.sharedInstance.fIsValidHours[Global.sharedInstance.lastBtnDayTag] == true && Global.sharedInstance.lastBtnDayTag != Global.sharedInstance.currentBtnDayTag
                    {
                        checkHoursEquals(Global.sharedInstance.lastBtnDayTag)
                    }
                    //אתחול הדייט פיקר
                    setDatePickerHoursBySelected(sender.tag + 1)
                    
                    Global.sharedInstance.isHoursSelected[sender.tag] = true
                    
                    checkIfSelectAll()

                    showSelectedHoursAndDays()
                }
                else//מכבה
                {
                    if Global.sharedInstance.isSelectAllHours == true
                    {
                        Global.sharedInstance.isSelectAllHours = false
                        Global.sharedInstance.isSelectAllRest = false
                        
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    (sender as! dayCheckBox).isCecked = false

                    Global.sharedInstance.isHoursSelected[sender.tag] = false
                    Global.sharedInstance.isHoursSelectedRest[sender.tag] = false
                    
                    showSelectedHoursAndDays()
                    showSelectedRecessAndDays()
                    
                }
                //                }
            }
            else//בוחר הפסקות
            {
                if (sender as! dayCheckBox).isCecked == false//מדליק
                {
                    (sender as! dayCheckBox).isCecked = true
                    if Global.sharedInstance.currentBtnDayTagRest != -1
                    {
                        Global.sharedInstance.lastBtnDayTagRest = Global.sharedInstance.currentBtnDayTagRest
                    }
                    else
                    {
                        Global.sharedInstance.lastBtnDayTagRest = sender.tag
                    }
                    
                    Global.sharedInstance.currentBtnDayTagRest = sender.tag

                    checkValidityHours(Global.sharedInstance.lastBtnDayTagRest)
                    
                    if Global.sharedInstance.fIsValidRest[Global.sharedInstance.lastBtnDayTagRest] == false && Global.sharedInstance.isHoursSelectedRest[Global.sharedInstance.lastBtnDayTagRest] == true && Global.sharedInstance.lastBtnDayTagRest != Global.sharedInstance.currentBtnDayTagRest
                    {
                        Global.sharedInstance.fIsValidRest[Global.sharedInstance.lastBtnDayTagRest] = true
                    }
                    //מופעל רק על היום הקודם...
                    //אם גלל שעת התחלה וסיום זהות,
                    //(אם גולל שעות לא תקינות מופעל ב handeleDatePicker)
                    if Global.sharedInstance.fIsValidRest[Global.sharedInstance.lastBtnDayTagRest] == true && Global.sharedInstance.lastBtnDayTagRest != Global.sharedInstance.currentBtnDayTagRest
                    {
                        checkHoursEquals(Global.sharedInstance.lastBtnDayTagRest)
                    }
//
                    if Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].iDayInWeekType != 0
                    {
                        //בדיקה אם ההפסקות שבחר בעבר אינן נמצאות בטווח של השעות, למקרה ששינה את השעות
                        if checkIfRestInHours(Global.sharedInstance.currentBtnDayTagRest) == false
                        {
                            Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest] = objWorkingHours()
                            Global.sharedInstance.isHoursSelectedRest[Global.sharedInstance.currentBtnDayTagRest]  = false
                        }
                    }

                    setDatePickerRest(sender.tag)
                    
                    setDatePickerRestBySelected(sender.tag + 1)
                    checkValidityHours(Global.sharedInstance.lastBtnDayTag)
                    Global.sharedInstance.isHoursSelectedRest[sender.tag] = true
                    checkIfSelectAll()
                    showSelectedRecessAndDays()
                }
                else//מכבה
                {
                    if Global.sharedInstance.isSelectAllRest == true
                        //if btnSelectAllDays.tag == 2//בחור
                    {
                        Global.sharedInstance.isSelectAllRest = false
                        //btnSelectAllDays.tag = 1
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    (sender as! dayCheckBox).isCecked = false
                    Global.sharedInstance.isHoursSelectedRest[sender.tag] = false
                    showSelectedRecessAndDays()
                    //            Global.sharedInstance.lastBtnDayTag = sender.tag//נוכחי
                    //            setDatePickerToDefult()
                }
            }
        }
        else//הוספת עובדים
        {
            if Global.sharedInstance.addRecess == false//בחירת שעות ולא הפסקות
            {
                if (sender as! dayCheckBox).isCecked == false//מדליק
                {
                    (sender as! dayCheckBox).isCecked = true
                    if Global.sharedInstance.currentBtnDayTagChild != -1//כבר בחרו שעות ליום מסויים
                    {
                        Global.sharedInstance.lastBtnDayTagChild = Global.sharedInstance.currentBtnDayTagChild
                    }
                    else//בחירת שעות פעם ראשונה
                    {
                        Global.sharedInstance.lastBtnDayTagChild = sender.tag
                    }
                    
                    Global.sharedInstance.currentBtnDayTagChild = sender.tag
                    
                    
                    checkValidityHours(Global.sharedInstance.lastBtnDayTagChild)
                    
                    
                    if Global.sharedInstance.fIsValidHoursChild[Global.sharedInstance.lastBtnDayTagChild] == false && Global.sharedInstance.isHoursSelectedChild[Global.sharedInstance.lastBtnDayTagChild] == true && Global.sharedInstance.lastBtnDayTagChild != Global.sharedInstance.currentBtnDayTagChild
                    {
                        Global.sharedInstance.fIsValidHoursChild[Global.sharedInstance.lastBtnDayTagChild] = true
                    }
                    //מופעל רק על היום הקודם...
                    //אם גלל שעת התחלה וסיום זהות,
                    //(אם גולל שעות לא תקינות מופעל ב handeleDatePicker)
                    if Global.sharedInstance.fIsValidHoursChild[Global.sharedInstance.lastBtnDayTagChild] == true && Global.sharedInstance.lastBtnDayTagChild != Global.sharedInstance.currentBtnDayTagChild
                    {
                        checkHoursEquals(Global.sharedInstance.lastBtnDayTagChild)
                    }

                    setDatePickerHoursBySelected_Workers(sender.tag + 1)
                    Global.sharedInstance.isHoursSelectedChild[sender.tag] = true
                    checkIfSelectAll()
                    
                    //showSelectedHoursAndDaysChild()
                }
                else//מכבה
                {
                    if Global.sharedInstance.isSelectAllHoursChild == true
                    {
                        Global.sharedInstance.isSelectAllHoursChild = false
                        Global.sharedInstance.isSelectAllRestChild = false
                        
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    (sender as! dayCheckBox).isCecked = false

                    Global.sharedInstance.isHoursSelectedChild[sender.tag] = false
                    
                    Global.sharedInstance.isHoursSelectedRestChild[sender.tag] = false
                    
                    //showSelectedHoursAndDaysChild()
                //להציג גם את ההפסקות, כי גם בהן נעשה שינוי
                }
            }
            else//בוחר הפסקות
            {
                if (sender as! dayCheckBox).isCecked == false//מדליק
                {
                    (sender as! dayCheckBox).isCecked = true
                    if Global.sharedInstance.currentBtnDayTagRestChild != -1
                    {
                        Global.sharedInstance.lastBtnDayTagRestChild = Global.sharedInstance.currentBtnDayTagRestChild
                    }
                    else
                    {
                        Global.sharedInstance.lastBtnDayTagRestChild = sender.tag
                    }
                    
                    Global.sharedInstance.currentBtnDayTagRestChild = sender.tag
                    
                    checkValidityHours(Global.sharedInstance.lastBtnDayTagRest)
                    
  
                    if Global.sharedInstance.fIsValidRestChild[Global.sharedInstance.lastBtnDayTagRestChild] == false && Global.sharedInstance.isHoursSelectedRestChild[Global.sharedInstance.lastBtnDayTagRestChild] == true && Global.sharedInstance.lastBtnDayTagRestChild != Global.sharedInstance.currentBtnDayTagRestChild
                    {
                        Global.sharedInstance.fIsValidRestChild[Global.sharedInstance.lastBtnDayTagRestChild] = true
                    }
                    //מופעל רק על היום הקודם...
                    //אם גלל שעת התחלה וסיום זהות,
                    //(אם גולל שעות לא תקינות מופעל ב handeleDatePicker)
                    if Global.sharedInstance.fIsValidRestChild[Global.sharedInstance.lastBtnDayTagRestChild] == true && Global.sharedInstance.lastBtnDayTagRestChild != Global.sharedInstance.currentBtnDayTagRestChild
                    {
                        checkHoursEquals(Global.sharedInstance.lastBtnDayTagRestChild)
                    }
                    if Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].iDayInWeekType != 0
                    {
                        //בדיקה אם ההפסקות שבחר בעבר אינן נמצאות בטווח של השעות, למקרה ששינה את השעות
                        if checkIfRestInHours(Global.sharedInstance.currentBtnDayTagRestChild) == false
                        {
                            Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild] = objWorkingHours()
                            Global.sharedInstance.isHoursSelectedRestChild[Global.sharedInstance.currentBtnDayTagRestChild]  = false
                        }
                    }
                    
                    setDatePickerRest(sender.tag)
                    
                    setDatePickerRestBySelected_Workers(sender.tag + 1)
                    
                    //setDatePickerRestBySelected(sender.tag + 1)
                    checkValidityHours(Global.sharedInstance.lastBtnDayTag)
                    Global.sharedInstance.isHoursSelectedRestChild[sender.tag] = true
                    checkIfSelectAll()
                    //showSelectedRecessAndDays()
                }
                else//מכבה
                {
                    if Global.sharedInstance.isSelectAllRestChild == true
                        //if btnSelectAllDays.tag == 2//בחור
                    {
                        Global.sharedInstance.isSelectAllRestChild = false
                        //btnSelectAllDays.tag = 1
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    (sender as! dayCheckBox).isCecked = false
                    Global.sharedInstance.isHoursSelectedRestChild[sender.tag] = false
                    //showSelectedRecessAndDays()
                }
            }
        }
    }
    
    
    //MARK: - Initial
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        dtFromHour.locale = NSLocale(localeIdentifier: "en_GB")//set datePicker to 24 format without am/pm
        dtToHour.locale = NSLocale(localeIdentifier: "en_GB")
        
        btnDay1.setTitle(NSLocalizedString("SUNDAY", comment: ""), forState: .Normal)
        btnDay2.setTitle(NSLocalizedString("MONDAY", comment: ""), forState: .Normal)
        btnDay3.setTitle(NSLocalizedString("TUESDAY", comment: ""), forState: .Normal)
        btnDay4.setTitle(NSLocalizedString("WEDNSDAY", comment: ""), forState: .Normal)
        btnDay5.setTitle(NSLocalizedString("THIRTHDAY", comment: ""), forState: .Normal)
        btnDay6.setTitle(NSLocalizedString("FRIDAY", comment: ""), forState: .Normal)
        btnDay7.setTitle(NSLocalizedString("SHABAT", comment: ""), forState: .Normal)
        
        lblChooseAll.text = NSLocalizedString("CHOOSE_ALL", comment: "")
        
        dateFormatter.dateFormat = "HH:mm:00"
        
        self.contentView.bringSubviewToFront(viewSelectAllDays)
        Global.sharedInstance.isFirstHoursOpen = true
        Global.sharedInstance.isSelectAllHours = false
        Global.sharedInstance.isSelectAllRest = false
        
        Global.sharedInstance.GlobalDataVC?.delegateActiveHours = self
        viewSelectAllDays.hidden = true
        Global.sharedInstance.lastBtnDayTag = -1
        Global.sharedInstance.lastBtnDayTagRest = -1
        
        
        dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
        dtToHour.date = dateFormatter.dateFromString("00:00:00")!
        
        dtToHour.addTarget(self, action: #selector(ItemInSection2TableViewCell.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        dtFromHour.addTarget(self, action: #selector(ItemInSection2TableViewCell.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectAllTap))
        viewSelectAllDays.addGestureRecognizer(tap)
        
        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
        
        if Global.sharedInstance.addRecess == true
        {
           enabledBtnDays()
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        if Global.sharedInstance.addRecess == true//לחצו על הפסקות
        {
            if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
            {
                //אתחול הכפתור של ה"סמן את כל הימים"
                if Global.sharedInstance.isSelectAllRest == true//בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                }
                else//לא בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                }
                
                if Global.sharedInstance.isFirstRecessHoursOpen == false // אם זה פעם ראשונה מאתחל את הדייט פיקר של ההפסקות ב-0
                {
                    Global.sharedInstance.isFirstRecessHoursOpen = true
                    
                    for i in 0 ..< 7
                    {
                        Global.sharedInstance.isHoursSelectedRest[i] = false//איפוס המערך של הפלאגים
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                            
                        default:
                            break
                        }
                    }
                    if Global.sharedInstance.currentBtnDayTagRest != -1
                    {
                        setDatePickerRestBySelected(Global.sharedInstance.currentBtnDayTagRest + 1)
                    }
                    
                }
                    //אם זה לא פעם ראשונה וזה אחרי שפתחו שוב את השעות פעילות
                    //אתחול ההפסקות בהתאם למי שנבחר כבר(אם נבחר)
                else if Global.sharedInstance.onOpenRecessHours == true
                {
                    Global.sharedInstance.onOpenRecessHours = false
                    
                    for i in 0 ..< 7
                    {
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                            
                        default:
                            break
                        }
                    }
                    if Global.sharedInstance.currentBtnDayTagRest != -1
                    {
                        setDatePickerRestBySelected(Global.sharedInstance.currentBtnDayTagRest + 1)
                    }
                }
            }
            else//עובדים
            {
                //אתחול הכפתור של ה"סמן את כל הימים"
                if Global.sharedInstance.isSelectAllRestChild == true//בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                }
                else//לא בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                }
                
                //מאתחל את הדייט פיקר של ההפסקות ב-0
                //אם זה פעם ראשונה או בהוספת עובד חדש או אם זה מעריכה וגם אין לו הפסקות
                if !Global.sharedInstance.isHoursSelectedRestChild.contains(true) && (Global.sharedInstance.isOpenNewWorker == true || Global.sharedInstance.isFromSave == false)
                    || (Global.sharedInstance.isFromEdit == true && !Global.sharedInstance.isHoursSelectedChild.contains(true))
                {
                    for i in 0 ..< 7
                    {
                        Global.sharedInstance.isHoursSelectedRestChild[i] = false//איפוס המערך של הפלאגים
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                            break
                            
                        default:
                            break
                        }
                    }
                    if Global.sharedInstance.currentBtnDayTagRestChild != -1
                    {
                        setDatePickerRestBySelected_Workers(Global.sharedInstance.currentBtnDayTagRestChild + 1)
                    }
                    else
                    {
                        setDatePickerToZero()
                    }
                }
                    
                    //אחרי שפתחו שוב את ההפסקות
                    //אתחול ההפסקות בהתאם למי שנבחר כבר(אם נבחר)
                else
                {
                    //אם זה מהוספת עובד חדש או בפעם הראשונה
                    if Global.sharedInstance.isOpenNewWorker == true || Global.sharedInstance.isFromSave == false
                    {
                        for i in 0 ..< 7
                        {
                            switch i
                            {
                            case 0:
                                btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 1:
                                btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 2:
                                btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 3:
                                btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 4:
                                btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 5:
                                btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 6:
                                btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                                
                            default:
                                break
                            }
                        }
                    
                        if Global.sharedInstance.currentBtnDayTagRestChild != -1
                        {
                            setDatePickerRestBySelected_Workers(Global.sharedInstance.currentBtnDayTagRestChild + 1)
                        }
                        
                    }
                    else if Global.sharedInstance.isServiceProviderEditOpen == true//מעריכה
                    {
                        for i in 0 ..< 7
                        {
                            switch i
                            {
                            case 0:
                                btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 1:
                                btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 2:
                                btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 3:
                                btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 4:
                                btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 5:
                                btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                            case 6:
                                btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRestChild[i]
                                break
                                
                            default:
                                break
                            }
                            setDatePickerRestBySelected_Workers(i+1)
                        }
                    }
                }
            }
        }
            //בפתיחה של השעות פעילות
        else if Global.sharedInstance.onOpenTimeOpenHours == true 
        {
            if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
            {
                //אתחול הכפתור של ה"סמן את כל הימים"
                if Global.sharedInstance.isSelectAllHours == true//בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                }
                else//לא בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                }
                
                if !Global.sharedInstance.isHoursSelected.contains(true)
                // אם זה פעם ראשונה מאתחל את הדייט פיקר של השעות ב-0
                {
                    Global.sharedInstance.isFirstHoursOpen = false
                    
                    for i in 0 ..< 7
                    {
                        Global.sharedInstance.isHoursSelected[i] = false//איפוס המערך של הפלאגים
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelectedRest[i]
                            break
                            
                        default:
                            break
                        }
                        setDatePickerHoursBySelected(i+1)
                    }
                }
                    //אם זה לא פעם ראשונה וזה אחרי שפתחו שוב את השעות פעילות
                    //אתחול השעות בהתאם למי שנבחר כבר(אם נבחר)
                else if Global.sharedInstance.onOpenTimeOpenHours == true
                {
                    Global.sharedInstance.onOpenTimeOpenHours = false
                    
                    for i in 0 ..< 7
                    {
                        //אתחול הכפתורים בהתאם למה שנבחר להם(שלא יציג לפי מה שנבחר בהפסקות)
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelected[i]
                            break
                            
                        default:
                            break
                        }
                        //הצגת השעות שנחרו בפועל
                        setDatePickerHoursBySelected(i+1)
                    }
                }
            }
            else//עובדים
            {
                //אתחול הכפתור של ה"סמן את כל הימים"
                if Global.sharedInstance.isSelectAllHoursChild == true//בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                }
                else//לא בחור
                {
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                }
                
                //מאתחל את הדייט פיקר של השעות ב-0
        //אם  לא בחר שעות וגם - זה פעם ראשונה או בהוספת עובד חדש
            //או אם מעריכה ואין לו שעות
                if !Global.sharedInstance.isHoursSelectedChild.contains(true) && (Global.sharedInstance.isOpenNewWorker == true || Global.sharedInstance.isFromSave == false)
                    || (Global.sharedInstance.isFromEdit == true && !Global.sharedInstance.isHoursSelectedChild.contains(true))
                {
                    for i in 0 ..< 7
                    {
                        Global.sharedInstance.isHoursSelectedChild[i] = false//איפוס המערך של הפלאגים
                        switch i
                        {
                        case 0:
                            btnDay1.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 1:
                            btnDay2.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 2:
                            btnDay3.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 3:
                            btnDay4.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 4:
                            btnDay5.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 5:
                            btnDay6.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                        case 6:
                            btnDay7.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                            break
                            
                        default:
                            break
                        }
                        setDatePickerHoursBySelected_Workers(i+1)
                    }
                }
                    
                    //אחרי שפתחו שוב את השעות
                    //אתחול השעות בהתאם למי שנבחר כבר(אם נבחר)
                else
                {
                    //אם זה מהוספת עובד חדש או בפעם הראשונה
                    if Global.sharedInstance.isOpenNewWorker == true || Global.sharedInstance.isFromSave == false
                    {
                        for i in 0 ..< 7
                        {
                            switch i
                            {
                            case 0:
                                btnDay1.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 1:
                                btnDay2.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 2:
                                btnDay3.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 3:
                                btnDay4.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 4:
                                btnDay5.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 5:
                                btnDay6.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 6:
                                btnDay7.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                                
                            default:
                                break
                            }
                            setDatePickerHoursBySelected_Workers(i+1)
                        }
                        
                    }
                    else if Global.sharedInstance.isServiceProviderEditOpen == true//מעריכה
                    {
                        for i in 0 ..< 7
                        {
                            switch i
                            {
                            case 0:
                                btnDay1.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                
                                break
                            case 1:
                                btnDay2.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 2:
                                btnDay3.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 3:
                                btnDay4.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 4:
                                btnDay5.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 5:
                                btnDay6.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                            case 6:
                                btnDay7.isCecked = Global.sharedInstance.isHoursSelectedChild[i]
                                break
                                
                            default:
                                break
                            }
                            setDatePickerHoursBySelected_Workers(i+1)
                        }
                    }
                }
            }
        }
    }
    
    override func layoutSubviews(){
        dtToHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dtFromHour.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
        dtToHour.backgroundColor = UIColor.blackColor()
        dtFromHour.backgroundColor = UIColor.blackColor()
    }
    
    //MARK: - DatePicker
    
    ///בלחיצה על ה-datePicker של השעות פעילות
    func handleDatePicker(sender: UIDatePicker)
    {
        viewSelectAllDays.hidden = false
        //        btnSelectAllDays.enabled = true
        
        let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
        
        let outputFormatter: NSDateFormatter = NSDateFormatter()
        outputFormatter.dateFormat = "HH:mm:00"
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            if Global.sharedInstance.addRecess == false//בוחרים שעות פעילות ולא הפסקות
            {
                Global.sharedInstance.workingHours = objWorkingHours()
                
                /*if language == "he"
                {*/
                    Global.sharedInstance.workingHours.nvFromHour = outputFormatter.stringFromDate(dtFromHour.date)
                    
                    Global.sharedInstance.workingHours.nvToHour = outputFormatter.stringFromDate(dtToHour.date)
                //}
                /*else
                {
                    Global.sharedInstance.workingHours.nvFromHour = outputFormatter.stringFromDate(self.dtToHour.date)
                    
                    Global.sharedInstance.workingHours.nvToHour = outputFormatter.stringFromDate(dtFromHour.date)
                }*/
                
                Global.sharedInstance.workingHours.iDayInWeekType = Global.sharedInstance.currentBtnDayTag + 1
                
                if Global.sharedInstance.currentBtnDayTag != -1//אם לא בחר יום מסויים(וזה הפעם הראשונה) הנתונים לא נשמרים
                {
                    //שמירת שעה ליום מסויים ,השמירה דווקא כך ולא ע״י append כדי שיוכל לשנות את השעה וזה יתעדכן
                    Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag] = Global.sharedInstance.workingHours
                    
                    checkValidityHours(Global.sharedInstance.currentBtnDayTag)
                    
                    checkIfSelectAll()
                    
                    if Global.sharedInstance.fIsValidHours[Global.sharedInstance.currentBtnDayTag] == false//לא תקין(שעת ההתחלה גדולה משעת הסיום)
                    {
                        if sender == dtFromHour
                        {
                            dtToHour.date = dtFromHour.date
                            Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour
                        }
                        else
                        {
                            dtFromHour.date = dtToHour.date
                            Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour
                        }
                        //ביטול בחירת היום הזה
                        checkHoursEquals(Global.sharedInstance.currentBtnDayTag)
                    }
                    showSelectedHoursAndDays()
                }
            }
            else//בוחרים הפסקות
            {
                Global.sharedInstance.workingHoursRest = objWorkingHours()
                
                //if language == "he"
                //{
                    Global.sharedInstance.workingHoursRest.nvFromHour = outputFormatter.stringFromDate(dtFromHour.date)
                    
                    Global.sharedInstance.workingHoursRest.nvToHour = outputFormatter.stringFromDate(dtToHour.date)
                //}
                /*else
                {
                    Global.sharedInstance.workingHoursRest.nvFromHour = outputFormatter.stringFromDate(dtToHour.date)
                    
                    Global.sharedInstance.workingHoursRest.nvToHour = outputFormatter.stringFromDate(dtFromHour.date)
                }*/
                
                Global.sharedInstance.workingHoursRest.iDayInWeekType = Global.sharedInstance.currentBtnDayTagRest + 1
                
                
                if Global.sharedInstance.currentBtnDayTagRest != -1//אם לא בחר יום מסויים(וזה הפעם הראשונה) הנתונים לא נשמרים
                {
                    ///שמירת שעה ליום מסויים ,השמירה דווקא כך ולא ע״י append כדי שיוכל לשנות את השעה וזה יתעדכן
                    Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest] = Global.sharedInstance.workingHoursRest
                    
                    showSelectedRecessAndDays()//הצגת הפסקות שנבחרו על הלייבל
                    
                    checkValidityHours(Global.sharedInstance.currentBtnDayTagRest)
                    checkIfSelectAll()
                    
                    if Global.sharedInstance.fIsValidRest[Global.sharedInstance.currentBtnDayTagRest] == false//לא תקין(שעת ההתחלה גדולה משעת הסיום)
                    {
                        if sender == dtFromHour
                        {
                            dtToHour.date = dtFromHour.date
                            Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvToHour = Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvFromHour
                        }
                        else
                        {
                            dtFromHour.date = dtToHour.date
                            Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvFromHour = Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvToHour
                        }
                        //ביטול בחירת היום הזה
                        checkHoursEquals(Global.sharedInstance.currentBtnDayTagRest)
                    }
                    
                    showSelectedRecessAndDays()//הצגת הפסקות שנבחרו על הלייבל
                }
            }
        }
            
        else//בוחר לעובד
        {
            //בחירת שעות ולא הפסקות
            if Global.sharedInstance.addRecess == false
            {
                Global.sharedInstance.workingHoursChild = objWorkingHours()
                
                //if language == "he"
               // {
                    Global.sharedInstance.workingHoursChild.nvFromHour = outputFormatter.stringFromDate(dtFromHour.date)
                    
                    Global.sharedInstance.workingHoursChild.nvToHour = outputFormatter.stringFromDate(dtToHour.date)
               // }
                /*else
                {
                    Global.sharedInstance.workingHoursChild.nvFromHour = outputFormatter.stringFromDate(dtToHour.date)
                    
                    Global.sharedInstance.workingHoursChild.nvToHour = outputFormatter.stringFromDate(dtFromHour.date)
                }*/
                
                Global.sharedInstance.workingHoursChild.iDayInWeekType = Global.sharedInstance.currentBtnDayTagChild + 1
                
                if Global.sharedInstance.currentBtnDayTagChild != -1//אם לא בחר יום מסויים(וזה הפעם הראשונה) הנתונים לא נשמרים
                {
                    ///שמירת שעה ליום מסויים,השמירה דווקא כך ולא ע״י append כדי שיוכל לשנות את השעה וזה יתעדכן
                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild] = Global.sharedInstance.workingHoursChild
                    
                    checkValidityHours(Global.sharedInstance.currentBtnDayTagChild)
                    
                    checkIfSelectAll()
                    
                    if Global.sharedInstance.fIsValidHoursChild[Global.sharedInstance.currentBtnDayTagChild] == false//לא תקין(שעת ההתחלה גדולה משעת הסיום)
                    {
                        //שעת התחלה אם היא מתאחרת לאחר שעת סיום צריך לעדכן את שעת סיום ולהשוותה לשעת התחלה עד שהמשתמש יעדכנה
                        if sender == dtFromHour
                        {
                            dtToHour.date = dtFromHour.date
                            Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour
                        }
                        else
                        {
                            dtFromHour.date = dtToHour.date
                            Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour
                        }
                        //ביטול בחירת היום הזה
                        checkHoursEquals(Global.sharedInstance.currentBtnDayTagChild)
                    }
                }
            }
                //בוחרים הפסקות
            else //if Global.sharedInstance.recessForWorker == true || Global.sharedInstance.recessForWorkerFromPlus
            {
                Global.sharedInstance.workingHoursRestChild = objWorkingHours()
                
                //if language == "he"
               // {
                    Global.sharedInstance.workingHoursRestChild.nvFromHour = outputFormatter.stringFromDate(dtFromHour.date)
                    
                    Global.sharedInstance.workingHoursRestChild.nvToHour = outputFormatter.stringFromDate(dtToHour.date)
                //}
                /*else
                {
                    Global.sharedInstance.workingHoursRestChild.nvFromHour = outputFormatter.stringFromDate(dtToHour.date)
                    
                    Global.sharedInstance.workingHoursRestChild.nvToHour = outputFormatter.stringFromDate(dtFromHour.date)
                }*/
                
                Global.sharedInstance.workingHoursRestChild.iDayInWeekType = Global.sharedInstance.currentBtnDayTagRestChild + 1
                
                if Global.sharedInstance.currentBtnDayTagRestChild != -1//אם לא בחר יום מסויים(וזה הפעם הראשונה) הנתונים לא נשמרים
                {
                    ///שמירת שעה ליום מסויים ,השמירה דווקא כך ולא ע״י append כדי שיוכל לשנות את השעה וזה יתעדכן
                    Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild] = Global.sharedInstance.workingHoursRestChild
                    
                    showSelectedRecessAndDays()//הצגת הפסקות שנבחרו על הלייבל
                    
                    checkValidityHours(Global.sharedInstance.currentBtnDayTagRestChild)
                    
                    checkIfSelectAll()
                    
                    if Global.sharedInstance.fIsValidRestChild[Global.sharedInstance.currentBtnDayTagRestChild] == false//לא תקין(שעת ההתחלה גדולה משעת הסיום)
                    {
                        //שעת תחילת הפסקה אם היא מתאחרת לאחר שעת סוף הפסקה צריך לעדכן את שעת סוף ההפסקה ולהשוותה לתחילת ההפסקה עד שהמשתמש יעדכנה
                        if sender == dtFromHour
                        {
                            dtToHour.date = dtFromHour.date
                            Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvToHour = Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvFromHour
                        }
                        else
                        {
                            dtFromHour.date = dtToHour.date
                            Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvFromHour = Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvToHour
                        }
                        //ביטול בחירת היום הזה
                        checkHoursEquals(Global.sharedInstance.currentBtnDayTagRestChild)
                    }
                }
            }
        }
    }
    
    //מאתחל את הדייט פיקר של שעות פעילות ליום מסויים
    func setDatePickerHoursBySelected(index:Int)
    {
        dtFromHour.maximumDate = .None
        dtFromHour.minimumDate = .None
        dtToHour.maximumDate = .None
        dtToHour.minimumDate = .None
        
        var flag = false
        
        var toHourD:NSDate?
        var fromHourD:NSDate?
        
        //אם זה פעם ראשונה שפותח את השעות פעילות(גם אם פותח אחרי שבחר לעובדים לפני שבחר לעסק)
        if Global.sharedInstance.currentBtnDayTag == -1
        {
            setDatePickerToZero()
        }
        
        if Global.sharedInstance.arrWorkHours[index-1].iDayInWeekType == index//בחרתי כבר שעות ליום זה(היה דלוק ,נכבה ונדלק שוב)
        {
            if Global.sharedInstance.arrWorkHours[index-1].nvToHour != "" && Global.sharedInstance.arrWorkHours[index-1].nvFromHour != ""
            {
                //מאתחל את השעות לפי מה שכבר בחר
                fromHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index-1].nvFromHour)
                toHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index-1].nvToHour)
                
                dtFromHour.date = fromHourD!
                dtToHour.date = toHourD!
                
                flag = true
            }
        }
        
        var btnDay = false
        
        switch index
        {
        case 1:
            btnDay = btnDay1.isCecked
            break
        case 2:
            btnDay = btnDay2.isCecked
            break
        case 3:
            btnDay = btnDay3.isCecked
            break
        case 4:
            btnDay = btnDay4.isCecked
            break
        case 5:
            btnDay = btnDay5.isCecked
            break
        case 6:
            btnDay = btnDay6.isCecked
        case 7:
            btnDay = btnDay7.isCecked
            break
        default:
            break
        }
        
        if flag == false//לא בחר שעות ליום הזה
        {
            if btnDay == true//אם לחץ על היום הזה כדי לבחור לו שעות
            {
                if Global.sharedInstance.lastBtnDayTag != -1//אם יש יום קודם -צריך לאתחל לפיו
                {
                    if Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvFromHour != "" && Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvToHour != ""
                    {
                        //הצגת התאריך בדייט פיקר לפי מה שבחר ביום הקודם
                        dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvFromHour)!
                        dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvToHour)!
                        //שמירה במערך בגלובל
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvFromHour
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.lastBtnDayTag].nvToHour
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].iDayInWeekType = index
                    }
                        //אתחול לפי מה שמוצג עכשיו בפיקר,כדי לאפשר גלילה לפני שבוחרים יום
                    else if dateFormatter.stringFromDate(dtToHour.date) != "" && dateFormatter.stringFromDate(dtFromHour.date) != ""
                    {
                        //שמירה במערך בגלובל
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour = dateFormatter.stringFromDate(dtFromHour.date)
                        
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour = dateFormatter.stringFromDate(dtToHour.date)
                        Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].iDayInWeekType = index
                    }
                        
                    else //אתחול דיפולטיבי
                    {
                        dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                        dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                        Global.sharedInstance.lastBtnDayTag = index-1
                    }
                }
                else
                {
                    //אתחול דיפולטיבי
                    dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                    dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                }
            }
        }
    }
    
    //מאתחל את השעות של העובדים ליום מסויים לפי מה שהמשתמש בחר
    func setDatePickerHoursBySelected_Workers(index:Int)
    {
        var flag = false
        
        var toHourD:NSDate?
        var fromHourD:NSDate?
        
            //אם זה לא מעריכה
            if Global.sharedInstance.hoursForWorkerFromEdit == false
            {
                if Global.sharedInstance.currentBtnDayTagChild == -1
                {
                    setDatePickerToZero()
                }
                
                else if index-1 == Global.sharedInstance.currentBtnDayTagChild//בגלל שמגיעים לפה כמה פעמים כי זה מלולאה-אין צורך לאתחל לכל הימים אלא רק לפי הנוכחי(אם לא שאלה זו זה יאתחל לפי האחרון)
                {
                    if Global.sharedInstance.arrWorkHoursChild[index-1].iDayInWeekType == index//אם כבר בחר ליום זה שעות,כיבה ועכשיו מדליק שוב
                    {
                        if Global.sharedInstance.arrWorkHoursChild[index-1].nvToHour != "" && Global.sharedInstance.arrWorkHoursChild[index-1].nvFromHour != ""
                        {
                            fromHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index-1].nvFromHour)
                            toHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index-1].nvToHour)
                            
                            dtFromHour.maximumDate = .None
                            dtFromHour.minimumDate = .None
                            dtToHour.maximumDate = .None
                            dtToHour.minimumDate = .None
                            
                            dtFromHour.date = fromHourD!
                            dtToHour.date = toHourD!
                            
                            flag = true
                        }
                    }
                    
                    var btnDay = false
                    
                    switch index {
                    case 1:
                        btnDay = btnDay1.isCecked
                        break
                    case 2:
                        btnDay = btnDay2.isCecked
                        break
                    case 3:
                        btnDay = btnDay3.isCecked
                        break
                    case 4:
                        btnDay = btnDay4.isCecked
                        break
                    case 5:
                        btnDay = btnDay5.isCecked
                        break
                    case 6:
                        btnDay = btnDay6.isCecked
                        break
                    case 7:
                        btnDay = btnDay7.isCecked
                        break
                    default:
                        break
                    }
                    
                    if flag == false//לא בחר שעות ליום הזה
                    {
                        if btnDay == true//מדליק את היום לבחירת שעות
                        {
                            //אם יש יום קודם שנבחרו לו שעות -יש לאתחל לפיו
                            if Global.sharedInstance.lastBtnDayTagChild != -1
                            {
                                if Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour != "" && Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvToHour != ""
                                {
         //הצגת התאריך בדייט פיקר לפי מה שבחר ביום הקודם
                                    dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour)!
                                    dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvToHour)!
                                    //שמירה במערך בגלובל
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvToHour
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].iDayInWeekType = index
                                }
         //אתחול לפי מה שמוצג עכשיו בפיקר,כדי לאפשר גלילה לפני שבוחרים יום
                                else if dateFormatter.stringFromDate(dtToHour.date) != "" && dateFormatter.stringFromDate(dtFromHour.date) != ""
                                {
        //שמירה במערך בגלובל
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour = dateFormatter.stringFromDate(dtFromHour.date)
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour = dateFormatter.stringFromDate(dtToHour.date)
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].iDayInWeekType = index
                                }
                                else
                                {
//אתחול דיפולטיבי
                                    dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                                    dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                                    Global.sharedInstance.lastBtnDayTagChild = index-1
                                }
                            }
                            else
                            {
                                //אתחול דיפולטיבי
                                dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                                dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                            }
                        }
                    }
                }
            }
            else//מעריכה
            {
                if Global.sharedInstance.arrWorkHoursChild[index-1].iDayInWeekType == index//אם כבר בחר שעות ליום הזה
                {
                    if Global.sharedInstance.arrWorkHoursChild[index-1].nvToHour != "" && Global.sharedInstance.arrWorkHoursChild[index-1].nvFromHour != ""
                    {
                        //אתחול הפיקר בהתאם למה שכבר בחר(מה ששמור)
                        fromHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index-1].nvFromHour)
                        toHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index-1].nvToHour)
                        //איפוס המקסימום והמינימום למקרה שמגיע מהפסקות ואז זה מציג בפיקר לפי המקסימום והמינימום של היום האחרון שבחר לו הפסקות ולא את השעות
                        dtFromHour.maximumDate = .None
                        dtFromHour.minimumDate = .None
                        dtToHour.maximumDate = .None
                        dtToHour.minimumDate = .None
                        
                        dtFromHour.date = fromHourD!
                        dtToHour.date = toHourD!
                        
                        flag = true
                    }
                }
                
                var btnDay = false
                
                switch index {
                case 1:
                    btnDay = btnDay1.isCecked
                    break
                case 2:
                    btnDay = btnDay2.isCecked
                    break
                case 3:
                    btnDay = btnDay3.isCecked
                    break
                case 4:
                    btnDay = btnDay4.isCecked
                    break
                case 5:
                    btnDay = btnDay5.isCecked
                    break
                case 6:
                    btnDay = btnDay6.isCecked
                case 7:
                    btnDay = btnDay7.isCecked
                    break
                default:
                    break
                }
                
                if flag == false//לא בחר שעות ליום הזה
                {
                    if btnDay == true//אם לחץ על היום הזה כדי לבחור לו שעות
                    {
                        if Global.sharedInstance.isVclicedForWorkerForEdit == true
                        {
                            if Global.sharedInstance.lastBtnDayTagChild != -1
                            {
                                if Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour != ""
                                {
                                    //הצגת התאריך בדייט פיקר לפי מה שבחר ביום הקודם
                                    dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour)!
                                    dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvToHour)!
                                    //שמירה במערך בגלובל
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvFromHour
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.lastBtnDayTagChild].nvToHour
                                    Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].iDayInWeekType = index
                                }
                                else
                                {
                                    //אתחול דיפולטיבי
                                    dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                                    dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                                    Global.sharedInstance.lastBtnDayTagChild = index-1
                                }
                            }
                        }
                        else
                        {
                            //אתחול דיפולטיבי
                            dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                            dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                        }
                    }
                }
            }
//        }
    }
    
    //מאתחל את ההפסקות ליום מסויים לפי מה שהמשתמש בחר
    func setDatePickerRestBySelected(index:Int)
    {
        var flag = false
        
        var toHourD:NSDate?
        var fromHourD:NSDate?
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            if Global.sharedInstance.arrWorkHoursRest[index-1].iDayInWeekType == index//אם בחר הפסקות ליום הזה
            {
                if Global.sharedInstance.arrWorkHoursRest[index-1].nvToHour != "" && Global.sharedInstance.arrWorkHoursRest[index-1].nvFromHour != ""
                {
                     //אם ההפסקות שבחר בעבר נמצאות בטווח של השעות, למקרה ששינה את השעות

                        fromHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index-1].nvFromHour)
                        toHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index-1].nvToHour)
                        
                        dtFromHour.date = fromHourD!
                        dtToHour.date = toHourD!
                        
                        flag = true
                }
            }
        }
        else//מעובדים
        {
            setDatePickerRestBySelected_Workers(index)
        }
    }
    
    //מאתחל את השעות של העובדים ליום מסויים לפי מה שהמשתמש בחר
    func setDatePickerRestBySelected_Workers(index:Int)
    {
        var toHourD:NSDate?
        var fromHourD:NSDate?
        
        if index-1 == Global.sharedInstance.currentBtnDayTagRestChild || Global.sharedInstance.currentBtnDayTagRestChild == -1
        {
            if Global.sharedInstance.isServiceProviderEditOpen == true//מעריכה
            {
                if Global.sharedInstance.arrWorkHoursRestChild[index-1].iDayInWeekType == index//אם כבר בחר שעות ליום הזה
                {
                    if Global.sharedInstance.arrWorkHoursRestChild[index-1].nvToHour != "" && Global.sharedInstance.arrWorkHoursRestChild[index-1].nvFromHour != ""
                    {
                            fromHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index-1].nvFromHour)
                            toHourD = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index-1].nvToHour)
                            
                            dtFromHour.date = fromHourD!
                            dtToHour.date = toHourD!
                    }
                }
            }
            else
            {
                if Global.sharedInstance.isHoursSelectedRestChild[index-1] == false &&
                Global.sharedInstance.arrWorkHoursRestChild[index-1].iDayInWeekType != 0//אם מדליק וגם נבחרו שעות
                {
                    dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index-1].nvFromHour)!
                    dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index-1].nvToHour)!
                }
            }
        }
    }
    
    //אתחול הדייט פיקר לעובד חדש ומבטל את המקסימום והמינימום (אם יש)
    func setDatePickerToZero()
    {
        dtFromHour.maximumDate = .None
        dtFromHour.minimumDate = .None
        dtToHour.maximumDate = .None
        dtToHour.minimumDate = .None
        
        
        dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
        dtToHour.date = dateFormatter.dateFromString("00:00:00")!
        dtFromHour.setDate(dtFromHour.date, animated: false)
        dtToHour.setDate(dtToHour.date, animated: false)
    }
    
    //איפוס הדייט פיקר בלחיצה על הפסקות שלא יציג לפי השעות פעילות
    func setDatePickerNull()
    {
        dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
        dtToHour.date = dateFormatter.dateFromString("00:00:00")!
    }
    
    //אתחול ההפסקות
    //מקבלת אינדקס של יום
    func setDatePickerRest(index:Int)
    {
        var flag = false
        
        var toHourD:NSDate?
        var fromHourD:NSDate?
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            for item in Global.sharedInstance.arrWorkHours
            {
                if item.iDayInWeekType == index + 1
                {
                        fromHourD = dateFormatter.dateFromString(item.nvFromHour)
                        toHourD = dateFormatter.dateFromString(item.nvToHour)
                    
                    dtFromHour.minimumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Minute, value: 30, toDate: fromHourD!, options: [])!
                    
                    dtFromHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: -1, toDate: toHourD!, options: [])!
                    
                    dtToHour.minimumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 1, toDate: fromHourD!, options: [])!
                    
                    dtToHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Minute, value: -30, toDate: toHourD!
                        , options: [])!
                    
                    if Global.sharedInstance.isHoursSelectedRest[index] == false && Global.sharedInstance.arrWorkHoursRest[index].iDayInWeekType == 0//אם מדליק וגם לא נבחרו שעות
                    {
                        dtFromHour.date = dtFromHour.minimumDate!
                        dtToHour.date = dtToHour.maximumDate!
                        //שמירה במערך בגלובל
                        Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvFromHour = dateFormatter.stringFromDate(dtFromHour.date)
                        Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvToHour = dateFormatter.stringFromDate(dtToHour.date)
                        Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].iDayInWeekType = Global.sharedInstance.currentBtnDayTagRest + 1
                    }
                    flag = true
                    break
                }
            }
            if !flag
            {
                
                //פקודות אלו מאפשרות לו לגלול את השעות ללא הגבלה של מקסימום ומינימום שנבחרו ליום אחר
                
                dtFromHour.maximumDate = .None
                dtFromHour.minimumDate = .None
                dtToHour.maximumDate = .None
                dtToHour.minimumDate = .None
                
                
                dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                dtFromHour.setDate(dtFromHour.date, animated: false)
                dtToHour.setDate(dtToHour.date, animated: false)
                
            }
        }
        else//מעובדים
        {
            for item in Global.sharedInstance.arrWorkHoursChild
            {
                if item.iDayInWeekType == index + 1
                {
                   fromHourD = dateFormatter.dateFromString(item.nvFromHour)
                   toHourD = dateFormatter.dateFromString(item.nvToHour)
                    
                    dtFromHour.minimumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Minute, value: 30, toDate: fromHourD!, options: [])!
                    
                    dtFromHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: -1, toDate: toHourD!, options: [])!
                    
                    dtToHour.minimumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 1, toDate: fromHourD!, options: [])!
                    
                    dtToHour.maximumDate = NSCalendar.currentCalendar().dateByAddingUnit(.Minute, value: -30, toDate: toHourD!
                        , options: [])!
                    
                    if Global.sharedInstance.isHoursSelectedRestChild[index] == false &&
                        Global.sharedInstance.arrWorkHoursRestChild[index].iDayInWeekType == 0 && Global.sharedInstance.currentBtnDayTagRestChild != -1//אם מדליק וגם לא נבחרו שעות
                    {
                        dtFromHour.date = dtFromHour.minimumDate!
                        dtToHour.date = dtToHour.maximumDate!
                        //שמירה במערך בגלובל
                        Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvFromHour = dateFormatter.stringFromDate(dtFromHour.date)
                        Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvToHour = dateFormatter.stringFromDate(dtToHour.date)
                        Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].iDayInWeekType = Global.sharedInstance.currentBtnDayTagRestChild + 1
                    }
                    flag = true
                }
            }
            if !flag
            {
                //פקודות אלו מאפשרות לו לגלול את השעות ללא הגבלה של מקסימום ומינימום שנבחרו ליום אחר
                
                dtFromHour.maximumDate = .None
                dtFromHour.minimumDate = .None
                dtToHour.maximumDate = .None
                dtToHour.minimumDate = .None
                
                
                dtFromHour.date = dateFormatter.dateFromString("00:00:00")!
                dtToHour.date = dateFormatter.dateFromString("00:00:00")!
                dtFromHour.setDate(dtFromHour.date, animated: false)
                dtToHour.setDate(dtToHour.date, animated: false)
                
            }
        }
    }
    
    //בדיקת תקינות לשעות-במעבר ליום הבא בדק את היום הקודם
    func checkValidityHours(index:Int)
    {
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות
            {
                if index != -1
                {
                    if Global.sharedInstance.arrWorkHours[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvFromHour)!
                        
                        let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        var hhFromHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        var hhToHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        if Global.sharedInstance.isHoursSelectedRest[index] == true// אם יש הפסקות ביום הזה
                        {
                            let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvFromHour)!
                            
                            //hours and minutes from hour - rest
                            var hhFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
                            let mmFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
                            
                            
                            let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvToHour)!
                            
                            //hours and minutes to hour - rest
                            var hhToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
                            let mmToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
                            
                            if mmFromHour != 0//== 30
                            {
                                hhFromHour = hhFromHour + mmFromHour / 60 //+ 0.5
                            }
                            
                            if mmToHour != 0//== 30
                            {
                                hhToHour = hhToHour + mmToHour / 60//0.5
                            }
                            
                            if mmFromHourRest != 0 //== 30
                            {
                                hhFromHourRest = hhFromHourRest + mmFromHourRest / 60//0.5
                            }
                            
                            if mmToHourRest != 0 //== 30
                            {
                                hhToHourRest = hhToHourRest + mmToHourRest/60//0.5
                            }
                            //אם השעות לא תקינות
                            if hhFromHour > hhToHour
                            {
                                Global.sharedInstance.fIsValidHours[index] = false
                            }
                                //אם ההפסקות לא תקינות
                            else if hhFromHourRest > hhToHourRest
                            {
                                Global.sharedInstance.fIsValidRest[index] = false
                            }
                                //אם השעות וההפסקות לא תקינות
                            else if hhFromHour > hhFromHourRest || hhToHour < hhToHourRest
                            {
                                Global.sharedInstance.fIsValidHours[index] = false
                                Global.sharedInstance.fIsValidRest[index] = false
                                
                            }
                                //אם תקין
                            else
                            {
                                Global.sharedInstance.fIsValidHours[index] = true
                                Global.sharedInstance.fIsValidRest[index] = true
                            }
                            if hhFromHour == hhToHour
                            {
                                Global.sharedInstance.isHoursSelected[index] = false
                            }
                        }
                            
                        else//אין הפסקות
                        {
                            if mmFromHour != 0//== 30
                            {
                                hhFromHour = hhFromHour + mmFromHour / 60 //+ 0.5
                            }
                            if mmToHour != 0//== 30
                            {
                                hhToHour = hhToHour + mmToHour / 60//0.5
                            }
                            
                            if hhFromHour > hhToHour
                            {
                                Global.sharedInstance.fIsValidHours[index] = false
                            }
                            else
                            {
                                Global.sharedInstance.fIsValidHours[index] = true
                            }
                            if hhFromHour == hhToHour
                            {
                                Global.sharedInstance.isHoursSelected[index] = false
                            }
                        }
                    }
                }
            }
            else//בוחר הפסקות
            {
                if index != -1
                {
                    if Global.sharedInstance.arrWorkHoursRest[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה הפסקות
                    {
                        let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvFromHour)!
                        
                        let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        var hhFromHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        var hhToHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvFromHour)!
                        
                        //hours and minutes from hour - rest
                        var hhFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
                        let mmFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
                        
                        
                        let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvToHour)!
                        
                        //hours and minutes to hour - rest
                        var hhToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
                        let mmToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
                        
                        if mmFromHour != 0//== 30
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60
                        }
                        
                        if mmToHour != 0
                        {
                            hhToHour = hhToHour + mmToHour / 60
                        }
                        
                        if mmFromHourRest != 0
                        {
                            hhFromHourRest = hhFromHourRest + mmFromHourRest / 60
                        }
                        
                        if mmToHourRest != 0
                        {
                            hhToHourRest = hhToHourRest + mmToHourRest/60
                        }
                        //אם השעות לא תקינות
                        if hhFromHour > hhToHour
                        {
                            Global.sharedInstance.fIsValidHours[index] = false
                        }
                            //אם ההפסקות לא תקינות
                        else if hhFromHourRest > hhToHourRest
                        {
                            Global.sharedInstance.fIsValidRest[index] = false
                        }
                            //אם השעות וההפסקות לא תקינות
                        else if hhFromHour > hhFromHourRest || hhToHour < hhToHourRest
                        {
                            Global.sharedInstance.fIsValidHours[index] = false
                            Global.sharedInstance.fIsValidRest[index] = false
                            
                        }
                            //אם תקין
                        else
                        {
                            Global.sharedInstance.fIsValidHours[index] = true
                            Global.sharedInstance.fIsValidRest[index] = true
                        }
                    }
                }
            }
        }
        else//עובדים
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות
            {
                if index != -1
                {
                    if Global.sharedInstance.arrWorkHoursChild[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvFromHour)!
                        
                        let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        var hhFromHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        var hhToHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        if Global.sharedInstance.isHoursSelectedRestChild[index] == true// אם יש הפסקות ביום הזה
                        {
                            let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvFromHour)!
                            
                            //hours and minutes from hour - rest
                            var hhFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
                            let mmFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
                            
                            
                            let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvToHour)!
                            
                            //hours and minutes to hour - rest
                            var hhToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
                            let mmToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
                            
                            if mmFromHour != 0//== 30
                            {
                                hhFromHour = hhFromHour + mmFromHour / 60 //+ 0.5
                            }
                            
                            if mmToHour != 0//== 30
                            {
                                hhToHour = hhToHour + mmToHour / 60//0.5
                            }
                            
                            if mmFromHourRest != 0 //== 30
                            {
                                hhFromHourRest = hhFromHourRest + mmFromHourRest / 60//0.5
                            }
                            
                            if mmToHourRest != 0 //== 30
                            {
                                hhToHourRest = hhToHourRest + mmToHourRest/60//0.5
                            }
                            //אם השעות לא תקינות
                            if hhFromHour > hhToHour
                            {
                                Global.sharedInstance.fIsValidHoursChild[index] = false
                            }
                                //אם ההפסקות לא תקינות
                            else if hhFromHourRest > hhToHourRest
                            {
                                Global.sharedInstance.fIsValidRestChild[index] = false
                            }
                                //אם השעות וההפסקות לא תקינות
                            else if hhFromHour > hhFromHourRest || hhToHour < hhToHourRest
                            {
                                Global.sharedInstance.fIsValidHoursChild[index] = false
                                Global.sharedInstance.fIsValidRestChild[index] = false
                                
                            }
                                //אם תקין
                            else
                            {
                                Global.sharedInstance.fIsValidHoursChild[index] = true
                                Global.sharedInstance.fIsValidRestChild[index] = true
                            }

                            if hhFromHour == hhToHour
                            {
                                Global.sharedInstance.isHoursSelectedChild[index] = false
                            }
                        }
                            
                        else//אין הפסקות
                        {
                            if mmFromHour != 0//== 30
                            {
                                hhFromHour = hhFromHour + mmFromHour / 60 //+ 0.5
                            }
                            if mmToHour != 0//== 30
                            {
                                hhToHour = hhToHour + mmToHour / 60//0.5
                            }
                            
                            if hhFromHour > hhToHour
                            {
                                Global.sharedInstance.fIsValidHoursChild[index] = false
                            }
                            else
                            {
                                Global.sharedInstance.fIsValidHoursChild[index] = true
                            }
                            if hhFromHour == hhToHour
                            {
                                Global.sharedInstance.isHoursSelectedChild[index] = false
                            }
                        }
                    }
                }
            }
            else//בוחר הפסקות
            {
                if index != -1
                {
                    if Global.sharedInstance.arrWorkHoursRestChild[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה הפסקות
                    {
                        let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvFromHour)!
                        
                        let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        var hhFromHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        var hhToHour:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvFromHour)!
                        
                        //hours and minutes from hour - rest
                        var hhFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
                        let mmFromHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
                        
                        
                        let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvToHour)!
                        
                        //hours and minutes to hour - rest
                        var hhToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
                        let mmToHourRest:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
                        
                        if mmFromHour != 0//== 30
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60 //+ 0.5
                        }
                        
                        if mmToHour != 0//== 30
                        {
                            hhToHour = hhToHour + mmToHour / 60//0.5
                        }
                        
                        if mmFromHourRest != 0 //== 30
                        {
                            hhFromHourRest = hhFromHourRest + mmFromHourRest / 60//0.5
                        }
                        
                        if mmToHourRest != 0 //== 30
                        {
                            hhToHourRest = hhToHourRest + mmToHourRest/60//0.5
                        }
                        
                        //אם השעות לא תקינות
                        if hhFromHour > hhToHour
                        {
                            Global.sharedInstance.fIsValidHoursChild[index] = false
                        }
                            //אם ההפסקות לא תקינות
                        else if hhFromHourRest > hhToHourRest
                        {
                            Global.sharedInstance.fIsValidRestChild[index] = false
                        }
                            //אם השעות וההפסקות לא תקינות
                        else if hhFromHour > hhFromHourRest || hhToHour < hhToHourRest
                        {
                            Global.sharedInstance.fIsValidHoursChild[index] = false
                            Global.sharedInstance.fIsValidRestChild[index] = false
                            
                        }
                            //אם תקין
                        else
                        {
                            Global.sharedInstance.fIsValidHoursChild[index] = true
                            Global.sharedInstance.fIsValidRestChild[index] = true
                        }
                    }
                }
            }
        }
    }
    
    //הצגת השעות שנבחרו בלייבל
    func showSelectedHoursAndDaysChild()
    {
        DayFlagArr = [0,0,0,0,0,0,0]
        if Global.sharedInstance.currentEditCellOpen == 1
        {
        Global.sharedInstance.hourShow = ""
        
        for i in 0 ..< 7
        {
            if Global.sharedInstance.isHoursSelectedChild[i] == true
            {
                if DayFlagArr[i] != 1
                {
                    if Global.sharedInstance.arrWorkHoursChild[i].nvFromHour != ""
                    {
                        for j in 0 ..< 7
                        {
                            if Global.sharedInstance.isHoursSelectedChild[j] == true
                            {
                                if Global.sharedInstance.arrWorkHoursChild[i].nvFromHour == Global.sharedInstance.arrWorkHoursChild[j].nvFromHour && Global.sharedInstance.arrWorkHoursChild[i].nvToHour == Global.sharedInstance.arrWorkHoursChild[j].nvToHour
                                {
                                    Global.sharedInstance.hourShowChild = "\(Global.sharedInstance.hourShowChild) \(convertDays(j)),"
                                    DayFlagArr[j] = 1
                                }
                            }
                        }
                        Global.sharedInstance.hourShowChild = String(Global.sharedInstance.hourShowChild.characters.dropLast())
                        Global.sharedInstance.hourShowChild = "\(Global.sharedInstance.hourShowChild) - "
                        
                        if Global.sharedInstance.isHoursSelectedChild[i] == true
                        {
                            Global.sharedInstance.hourShowChild = "\(Global.sharedInstance.hourShowChild) \(cutHour(Global.sharedInstance.arrWorkHoursChild[i].nvFromHour))-\(cutHour(Global.sharedInstance.arrWorkHoursChild[i].nvToHour)),"
                        }
                    }
                }
            }
        }
        Global.sharedInstance.hourShowChild = String(Global.sharedInstance.hourShowChild.characters.dropLast())
        }
    
    if Global.sharedInstance.hoursForWorkerFromEdit == false//למחוק את  ה-if כשנציג את הסל של הצגת השעות גם בעובדים
        {
            delagetReloadHeight.reloadTbl()
        }
    }
    
    //cut last characters of string
    func cutHour(hour:String) -> String {
        var fixedHour = String(hour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        fixedHour = String(fixedHour.characters.dropLast())
        return fixedHour
    }
    
    func convertDays(day:Int) -> String {
        switch day {
        case 0:
            return NSLocalizedString("SUNDAY", comment: "")
        case 1:
            return NSLocalizedString("MONDAY", comment: "")
        case 2:
            return NSLocalizedString("TUESDAY", comment: "")
        case 3:
            return NSLocalizedString("WEDNSDAY", comment: "")
        case 4:
            return NSLocalizedString("THIRTHDAY", comment: "")
        case 5:
            return NSLocalizedString("FRIDAY", comment: "")
        case 6:
            return NSLocalizedString("SHABAT", comment: "")
        default:
            return NSLocalizedString("SUNDAY", comment: "")
        }
    }
    
    //הצגת השעות שנבחרו בלייבל
    func showSelectedHoursAndDays()
    {
        DayFlagArr = [0,0,0,0,0,0,0]
        Global.sharedInstance.hourShow = ""
        
        for i in 0 ..< 7
        {
            if Global.sharedInstance.isHoursSelected[i] == true
            {
                if DayFlagArr[i] != 1
                {
                    if Global.sharedInstance.arrWorkHours[i].nvFromHour != "" && !(Global.sharedInstance.arrWorkHours[i].nvFromHour == "00:00:00" && Global.sharedInstance.arrWorkHours[i].nvToHour == "00:00:00")
                    {
                        for j in 0 ..< 7
                        {
                            if Global.sharedInstance.isHoursSelected[j] == true
                            {
                                if Global.sharedInstance.arrWorkHours[i].nvFromHour == Global.sharedInstance.arrWorkHours[j].nvFromHour &&
                                    Global.sharedInstance.arrWorkHours[i].nvToHour == Global.sharedInstance.arrWorkHours[j].nvToHour
                                {
                                    Global.sharedInstance.hourShow = "\(Global.sharedInstance.hourShow) \(convertDays(j)),"
                                    DayFlagArr[j] = 1
                                }
                            }
                        }
                        Global.sharedInstance.hourShow = String(Global.sharedInstance.hourShow.characters.dropLast())
                        Global.sharedInstance.hourShow = "\(Global.sharedInstance.hourShow) - "
                        
                        if Global.sharedInstance.isHoursSelected[i] == true
                        {
                            Global.sharedInstance.hourShow = "\(Global.sharedInstance.hourShow) \(cutHour(Global.sharedInstance.arrWorkHours[i].nvFromHour))-\(cutHour(Global.sharedInstance.arrWorkHours[i].nvToHour)),"
                        }
                    }
                }
            }
        }
        Global.sharedInstance.hourShow = String(Global.sharedInstance.hourShow.characters.dropLast())
        
        delagetReloadHeight.reloadTbl()
    }
    
    //הצגת השעות בלייבל, לאחר לחיצה על סמן את כל הימים
    func showHoursFromSelectAll()
    {
        Global.sharedInstance.hourShow = "\(NSLocalizedString("ALLDAYS", comment: "")) \(cutHour(Global.sharedInstance.arrWorkHours[0].nvFromHour))-\(cutHour(Global.sharedInstance.arrWorkHours[0].nvToHour))"

    delagetReloadHeight.reloadTbl()
    }
    
    //הצגת ההפסקות שנבחרו בלייבל
    func showSelectedRecessAndDays()
    {
        DayFlagArr = [0,0,0,0,0,0,0]
        Global.sharedInstance.hourShowRecess = ""
        
        for i in 0 ..< 7
        {
            if Global.sharedInstance.isHoursSelectedRest[i] == true
            {
                if DayFlagArr[i] != 1
                {
                    if Global.sharedInstance.arrWorkHoursRest[i].nvFromHour != "" && !(Global.sharedInstance.arrWorkHoursRest[i].nvFromHour == "00:00:00" && Global.sharedInstance.arrWorkHoursRest[i].nvToHour == "00:00:00")
                    {
                        for j in 0 ..< 7
                        {
                            if Global.sharedInstance.isHoursSelectedRest[j] == true
                            {
                                if Global.sharedInstance.arrWorkHoursRest[i].nvFromHour == Global.sharedInstance.arrWorkHoursRest[j].nvFromHour && Global.sharedInstance.arrWorkHoursRest[i].nvToHour == Global.sharedInstance.arrWorkHoursRest[j].nvToHour
                                {
                                    Global.sharedInstance.hourShowRecess = "\(Global.sharedInstance.hourShowRecess) \(convertDays(j)),"
                                    DayFlagArr[j] = 1
                                }
                            }
                        }
                        Global.sharedInstance.hourShowRecess = String(Global.sharedInstance.hourShowRecess.characters.dropLast())
                        Global.sharedInstance.hourShowRecess = "\(Global.sharedInstance.hourShowRecess) - "
                        
                        if Global.sharedInstance.isHoursSelectedRest[i] == true
                        {
                            Global.sharedInstance.hourShowRecess = "\(Global.sharedInstance.hourShowRecess) \(cutHour(Global.sharedInstance.arrWorkHoursRest[i].nvFromHour))-\(cutHour(Global.sharedInstance.arrWorkHoursRest[i].nvToHour)),"
                        }
                    }
                }
            }
        }
        Global.sharedInstance.hourShowRecess = String(Global.sharedInstance.hourShowRecess.characters.dropLast())
        
        if Global.sharedInstance.hourShowRecess != ""
        {
            Global.sharedInstance.hourShowRecess = "\(NSLocalizedString("RECESS", comment: "")):\(Global.sharedInstance.hourShowRecess)"
        }
        
        delagetReloadHeight.reloadTbl()
    }
    
    //הצגת ההפסקות בלייבל, לאחר לחיצה על סמן את כל הימים
    func showRecessFromSelectAll()
    {
        Global.sharedInstance.hourShowRecess = "\(NSLocalizedString("ALLDAYS", comment: "")) \(cutHour(Global.sharedInstance.arrWorkHoursRest[0].nvFromHour))-\(cutHour(Global.sharedInstance.arrWorkHoursRest[0].nvToHour))"
        
        delagetReloadHeight.reloadTbl()
    }
    
    func selectAllTap()
    {
         //בלחיצה על סמן את כל הימים
        isValidSelectAllDays()
    }
    
    func selectAll()
    {
        if Global.sharedInstance.currentEditCellOpen == 1
        {
            if Global.sharedInstance.addRecess == false//בוחרים שעות פעילות ולא הפסקות
            {
                if Global.sharedInstance.isSelectAllHours == false
                    //if btnSelectAllDays.tag == 1// היה לא בחור
                {
                    if Global.sharedInstance.isHoursSelected.contains(true)//אם יש לפחות מישהו אחד שבחרו לו שעות
                    {
                        Global.sharedInstance.isSelectAllHours = true
                        
                        //btnSelectAllDays.tag = 2//- נהפך לבחור
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                        
                        for i in 0 ..< Global.sharedInstance.arrWorkHours.count
                        {
                            Global.sharedInstance.arrWorkHours[i].iDayInWeekType = i+1
                            
                            Global.sharedInstance.arrWorkHours[i].nvFromHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour
                            
                            Global.sharedInstance.arrWorkHours[i].nvToHour = Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour
                            
                            Global.sharedInstance.isHoursSelected[i] = true
                        }
                        
                        dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvFromHour)!
                         dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[Global.sharedInstance.currentBtnDayTag].nvToHour)!
                        
                        showSelectedHoursAndDays()
                        
                        btnDay1.isCecked = true
                        btnDay2.isCecked = true
                        btnDay3.isCecked = true
                        btnDay4.isCecked = true
                        btnDay5.isCecked = true
                        btnDay6.isCecked = true
                        btnDay7.isCecked = true
                    }
                    else//אם עדין לא בחרו שעות לאף יום
                    {
                        Alert.sharedInstance.showAlert(
                            NSLocalizedString("CANT_SELECT_ALL_DAYS_BEFORE_HOURS", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
                    }
                }
                else//(tag=2) אם בחור נהפך ללא בחור
                {
                    Global.sharedInstance.isSelectAllHours = false
                    
                    //btnSelectAllDays.tag = 1//לא בחור
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    for i in 0 ..< Global.sharedInstance.arrWorkHours.count
                    {
                        Global.sharedInstance.isHoursSelected[i] = false
                    }
                    showSelectedHoursAndDays()
                    
                    btnDay1.isCecked = false
                    btnDay2.isCecked = false
                    btnDay3.isCecked = false
                    btnDay4.isCecked = false
                    btnDay5.isCecked = false
                    btnDay6.isCecked = false
                    btnDay7.isCecked = false
                }
            }
            else//בוחרים הפסקות
            {
                if Global.sharedInstance.isSelectAllRest == false
                {
                    if Global.sharedInstance.isHoursSelectedRest.contains(true)
                        //אם יש לפחות מישהו אחד שבחרו לו הפסקות
                    {
                        //עובר על ההפסקות ומוסיף לכולם את אותן השעות
                        for i in 0 ..< Global.sharedInstance.arrWorkHours.count
                        {
                            Global.sharedInstance.arrWorkHoursRest[i].iDayInWeekType = i+1
                            
                            Global.sharedInstance.arrWorkHoursRest[i].nvFromHour = Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvFromHour
                            
                            Global.sharedInstance.arrWorkHoursRest[i].nvToHour = Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvToHour
                            
                            Global.sharedInstance.isHoursSelectedRest[i] = true
                        }
                        
                        dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvFromHour)!
                        dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[Global.sharedInstance.currentBtnDayTagRest].nvToHour)!
                        
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                        
                        Global.sharedInstance.isSelectAllRest = true
                        showSelectedRecessAndDays()
                        
                        btnDay1.isCecked = true
                        btnDay2.isCecked = true
                        btnDay3.isCecked = true
                        btnDay4.isCecked = true
                        btnDay5.isCecked = true
                        btnDay6.isCecked = true
                        btnDay7.isCecked = true
                    }
                    else//אם לא בחרו הפסקות לאף יום
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("CANT_SELECT_ALL_DAYS_BEFORE_RECESS", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
                    }
                }
                else//(tag=2) אם בחור נהפך ללא בחור
                {
                    Global.sharedInstance.isSelectAllRest = false
                    
                    //btnSelectAllDays.tag = 1//לא בחור
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    for i in 0 ..< Global.sharedInstance.arrWorkHoursRest.count
                    {
                        Global.sharedInstance.isHoursSelectedRest[i] = false
                    }
                    showSelectedRecessAndDays()
                   
                    btnDay1.isCecked = false
                    btnDay2.isCecked = false
                    btnDay3.isCecked = false
                    btnDay4.isCecked = false
                    btnDay5.isCecked = false
                    btnDay6.isCecked = false
                    btnDay7.isCecked = false
                }
                
            }
        }
        else if Global.sharedInstance.currentEditCellOpen == 2//עובדים
        {
            if Global.sharedInstance.addRecess == false//בוחרים שעות פעילות ולא הפסקות
            {
                if Global.sharedInstance.isSelectAllHoursChild == false
                {
                 if Global.sharedInstance.isHoursSelectedChild.contains(true)//אם יש לפחות מישהו אחד שבחרו לו שעות
                    {
                        Global.sharedInstance.isSelectAllHoursChild = true
                //- נהפך לבחור
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                        
                        for i in 0 ..< Global.sharedInstance.arrWorkHoursChild.count
                        {
                            Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType = i+1
                            
                            Global.sharedInstance.arrWorkHoursChild[i].nvFromHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour
                            
                            Global.sharedInstance.arrWorkHoursChild[i].nvToHour = Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour
                            
                            Global.sharedInstance.isHoursSelectedChild[i] = true
                        }
                        
                        dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvFromHour)!
                        dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[Global.sharedInstance.currentBtnDayTagChild].nvToHour)!
                        
                        btnDay1.isCecked = true
                        btnDay2.isCecked = true
                        btnDay3.isCecked = true
                        btnDay4.isCecked = true
                        btnDay5.isCecked = true
                        btnDay6.isCecked = true
                        btnDay7.isCecked = true
                    }
                    else//אם עדין לא בחרו שעות לאף יום
                    {
                        Alert.sharedInstance.showAlert(
                            NSLocalizedString("CANT_SELECT_ALL_DAYS_BEFORE_HOURS", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
                    }
                }
                else//(tag=2) אם בחור נהפך ללא בחור
                {
                    Global.sharedInstance.isSelectAllHoursChild = false
                    
                    //לא בחור
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    
                    for i in 0 ..< Global.sharedInstance.arrWorkHoursChild.count
                    {
                        Global.sharedInstance.isHoursSelectedChild[i] = false
                    }
                    
                    btnDay1.isCecked = false
                    btnDay2.isCecked = false
                    btnDay3.isCecked = false
                    btnDay4.isCecked = false
                    btnDay5.isCecked = false
                    btnDay6.isCecked = false
                    btnDay7.isCecked = false
                }
            }
            else//בוחרים הפסקות
            {
                if Global.sharedInstance.isSelectAllRestChild == false
                    //if btnSelectAllDays.tag == 1// היה לא בחור
                {
                    if Global.sharedInstance.isHoursSelectedRestChild.contains(true)
                        //אם יש לפחות מישהו אחד שבחרו לו הפסקות
                    {
                        //עובר על ההפסקות ומוסיף לכולם את אותן השעות
                        for i in 0 ..< Global.sharedInstance.arrWorkHoursChild.count
                        {
                            Global.sharedInstance.arrWorkHoursRestChild[i].iDayInWeekType = i+1
                            
                            Global.sharedInstance.arrWorkHoursRestChild[i].nvFromHour = Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvFromHour
                            
                            Global.sharedInstance.arrWorkHoursRestChild[i].nvToHour = Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvToHour
                            
                            Global.sharedInstance.isHoursSelectedRestChild[i] = true
                        }
                        
                        dtFromHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvFromHour)!
                        dtToHour.date = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[Global.sharedInstance.currentBtnDayTagRestChild].nvToHour)!
                        
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                        
                        Global.sharedInstance.isSelectAllRestChild = true
                        
                        btnDay1.isCecked = true
                        btnDay2.isCecked = true
                        btnDay3.isCecked = true
                        btnDay4.isCecked = true
                        btnDay5.isCecked = true
                        btnDay6.isCecked = true
                        btnDay7.isCecked = true
                    }
                    else//אם לא בחרו הפסקות לאף יום
                    {
                        Alert.sharedInstance.showAlert(NSLocalizedString("CANT_SELECT_ALL_DAYS_BEFORE_RECESS", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
                    }
                }
                else//(tag=2) אם בחור נהפך ללא בחור
                {
                    Global.sharedInstance.isSelectAllRestChild = false
                    
                    //לא בחור
                    btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    
                    for i in 0 ..< Global.sharedInstance.arrWorkHoursRestChild.count
                    {
                        Global.sharedInstance.isHoursSelectedRestChild[i] = false
                    }
                    
                    btnDay1.isCecked = false
                    btnDay2.isCecked = false
                    btnDay3.isCecked = false
                    btnDay4.isCecked = false
                    btnDay5.isCecked = false
                    btnDay6.isCecked = false
                    btnDay7.isCecked = false
                }
            }
        }
    }
    
    //מאפשר את הלחיצה על הימים
    func enabledBtnDays()
    {
        if Global.sharedInstance.currentEditCellOpen == 1//משעות פעילות
        {
            if Global.sharedInstance.arrWorkHours[0].iDayInWeekType != 0 && checkIfCanAddRecess(0) == true
            {
                btnDay1.enabled = Global.sharedInstance.isHoursSelected[0]
            }
            else
            {
                btnDay1.enabled = false
            }
            if Global.sharedInstance.arrWorkHours[1].iDayInWeekType != 0 && checkIfCanAddRecess(1) == true
            {
                btnDay2.enabled = Global.sharedInstance.isHoursSelected[1]
            }
            else
            {
                btnDay2.enabled = false
            }
            if Global.sharedInstance.arrWorkHours[2].iDayInWeekType != 0 && checkIfCanAddRecess(2) == true
            {
                btnDay3.enabled = Global.sharedInstance.isHoursSelected[2]
            }
            else
            {
                btnDay3.enabled = false
            }
            
            if Global.sharedInstance.arrWorkHours[3].iDayInWeekType != 0 && checkIfCanAddRecess(3) == true
            {
                btnDay4.enabled = Global.sharedInstance.isHoursSelected[3]
            }
            else
            {
                btnDay4.enabled = false
            }
            
            if Global.sharedInstance.arrWorkHours[4].iDayInWeekType != 0 && checkIfCanAddRecess(4) == true
            {
                btnDay5.enabled = Global.sharedInstance.isHoursSelected[4]
            }
            else
            {
                btnDay5.enabled = false
            }
            if Global.sharedInstance.arrWorkHours[5].iDayInWeekType != 0 && checkIfCanAddRecess(5) == true
            {
                btnDay6.enabled = Global.sharedInstance.isHoursSelected[5]
            }
            else
            {
                btnDay6.enabled = false
            }
            
            if Global.sharedInstance.arrWorkHours[6].iDayInWeekType != 0 && checkIfCanAddRecess(6) == true
            {
                btnDay7.enabled = Global.sharedInstance.isHoursSelected[6]
            }
            else
            {
                btnDay7.enabled = false
            }
        }
        else//מעובדים
        {
            if Global.sharedInstance.arrWorkHoursChild[0].iDayInWeekType != 0 && checkIfCanAddRecess(0) == true
            {
                btnDay1.enabled = Global.sharedInstance.isHoursSelectedChild[0]
            }
            else
            {
                btnDay1.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[1].iDayInWeekType != 0 && checkIfCanAddRecess(1) == true
            {
                btnDay2.enabled = Global.sharedInstance.isHoursSelectedChild[1]
            }
            else
            {
                btnDay2.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[2].iDayInWeekType != 0 && checkIfCanAddRecess(2) == true
            {
                btnDay3.enabled = Global.sharedInstance.isHoursSelectedChild[2]
            }
            else
            {
                btnDay3.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[3].iDayInWeekType != 0 && checkIfCanAddRecess(3) == true
            {
                btnDay4.enabled = Global.sharedInstance.isHoursSelectedChild[3]
            }
            else
            {
                btnDay4.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[4].iDayInWeekType != 0 && checkIfCanAddRecess(4) == true
            {
                btnDay5.enabled = Global.sharedInstance.isHoursSelectedChild[4]
            }
            else
            {
                btnDay5.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[5].iDayInWeekType != 0 && checkIfCanAddRecess(5) == true
            {
                btnDay6.enabled = Global.sharedInstance.isHoursSelectedChild[5]
            }
            else
            {
                btnDay6.enabled = false
            }
            if Global.sharedInstance.arrWorkHoursChild[6].iDayInWeekType != 0 && checkIfCanAddRecess(6) == true
            {
                btnDay7.enabled = Global.sharedInstance.isHoursSelectedChild[6]
            }
            else
            {
                btnDay7.enabled = false
            }
        }
        Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays = self
    }
    
    //מאפשר את הלחיצה על הכפתורים
    func enabledTrueBtnDays()
    {
        btnDay1.enabled = true
        btnDay2.enabled = true
        btnDay3.enabled = true
        btnDay4.enabled = true
        btnDay5.enabled = true
        btnDay6.enabled = true
        btnDay7.enabled = true
    }
    
    //בדיקה האם שעת ההתחלה ושעת הסיום זהות
    //במקרה כזה השעות ההן נמחקות, והיום נראה כאינו בחור.
    func checkHoursEquals(index:Int)
    {
        var hhFromHour:Float = 0.0
        var hhToHour:Float = 0.0
        var hhFromHourDate:NSDate = NSDate()
        var hhToHourDate:NSDate = NSDate()
        
        if index != -1
        {
            if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
            {
                if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
                {
                    if Global.sharedInstance.arrWorkHours[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        hhFromHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvFromHour)!
                        
                        hhToHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        if mmFromHour != 0
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60
                        }
                        
                        if mmToHour != 0
                        {
                            hhToHour = hhToHour + mmToHour / 60
                        }
                    }
                    if Global.sharedInstance.isHoursSelectedRest[index] == true// אם יש הפסקות ביום הזה
                    {
                        if hhFromHour == hhToHour
                        {
                            Global.sharedInstance.isHoursSelected[index] = false
                            Global.sharedInstance.isHoursSelectedRest[index] = false
                            
                            Global.sharedInstance.arrWorkHours[index] = objWorkingHours()
                            Global.sharedInstance.arrWorkHoursRest[index] = objWorkingHours()
                            
                            switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                        }
                    }
                    else //אין הפסקות
                    {
                        if hhFromHour == hhToHour
                        {
                            Global.sharedInstance.isHoursSelected[index] = false
                            Global.sharedInstance.arrWorkHours[index] = objWorkingHours()
                            
                            switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                        }
                    }
                }
                else//הפסקות
                {
                    if Global.sharedInstance.arrWorkHoursRest[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        hhFromHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvFromHour)!
                        
                        hhToHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        if mmFromHour != 0
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60
                        }
                        if mmToHour != 0
                        {
                            hhToHour = hhToHour + mmToHour / 60
                        }
                    }
                    if hhFromHour == hhToHour
                    {
                        Global.sharedInstance.isHoursSelectedRest[index] = false
                        Global.sharedInstance.arrWorkHoursRest[index] = objWorkingHours()
                        
                        switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                    }
                }
            }
                
            else if Global.sharedInstance.currentEditCellOpen == 2//עובדים
            {
                if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
                {
                    if Global.sharedInstance.arrWorkHoursChild[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        hhFromHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvFromHour)!
                        
                        hhToHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        if mmFromHour != 0
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60
                        }
                        if mmToHour != 0
                        {
                            hhToHour = hhToHour + mmToHour / 60
                        }
                    }
                    
                    if Global.sharedInstance.isHoursSelectedRestChild[index] == true// אם יש הפסקות ביום הזה
                    {
                        if hhFromHour == hhToHour
                        {
                            Global.sharedInstance.isHoursSelectedChild[index] = false
                            Global.sharedInstance.isHoursSelectedRestChild[index] = false
                            
                            Global.sharedInstance.arrWorkHoursChild[index] = objWorkingHours()
                            Global.sharedInstance.arrWorkHoursRestChild[index] = objWorkingHours()
                            
                            switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                        }
                    }
                    else //אין הפסקות
                    {
                        if hhFromHour == hhToHour
                        {
                            Global.sharedInstance.isHoursSelectedChild[index] = false
                            Global.sharedInstance.arrWorkHoursChild[index] = objWorkingHours()
                            
                            switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                        }
                    }
                }
                else//הפסקות
                {
                    if Global.sharedInstance.arrWorkHoursRestChild[index].iDayInWeekType != 0//כדי שלא יקרוס כשעדיין לא בחר ליום זה שעות
                    {
                        hhFromHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvFromHour)!
                        
                        hhToHourDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvToHour)!
                        
                        //hours and minutes from hour - working
                        hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
                        let mmFromHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
                        
                        //hours and minutes to hour - working
                        hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
                        let mmToHour:Float = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
                        
                        if mmFromHour != 0
                        {
                            hhFromHour = hhFromHour + mmFromHour / 60
                        }
                        if mmToHour != 0
                        {
                            hhToHour = hhToHour + mmToHour / 60
                        }
                    }
                    if hhFromHour == hhToHour
                    {
                        Global.sharedInstance.isHoursSelectedRestChild[index] = false
                        Global.sharedInstance.arrWorkHoursRestChild[index] = objWorkingHours()
                        
                        switchBtnDay(index,bool: false)//מכבה את הכפתור של היום הזה
                    }
                }
            }
        }
    }
    
    func isValidSelectAllDays()
    {
        
        var previousFrom = ""
        var currentFrom = ""
        var previousTo = ""
        var currentTo = ""
        var fIsEquals = true
        var currentIndex = 0//שומר את היום האחרון שלפיו נאתחל את כל הימים(למקרה שלוחץ על סמן את כל הימים בלי לבחור יום מסויים,לדוג:כיבה יום או בעריכה כשפותחים ומיד לוחצים על סמן הכל)
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
            {
                if Global.sharedInstance.isSelectAllHours == false//אם לחצתי כדי להדליק את הכפתור סמן את כל הימים בשעות
                {
                    //מעבר על שעות הפעילות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                    for i in 0...6
                    {
                        if Global.sharedInstance.isHoursSelected[i] == true
                        {
                            currentIndex = i
                            currentFrom = Global.sharedInstance.arrWorkHours[i].nvFromHour
                            currentTo = Global.sharedInstance.arrWorkHours[i].nvToHour
                              //אם משעה ועד שעה שווים או שלא בחרו שעות
                            if currentFrom == currentTo
                            {
                                fIsEquals = false
                                break
                            }
                            else if previousFrom == ""
                            {
                                previousFrom = currentFrom
                                previousTo = currentTo
                            }
                            else
                            {
                                if currentFrom != previousFrom || currentTo != previousTo
                                {
                                    fIsEquals = false
                                    break
                                }
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        Global.sharedInstance.currentBtnDayTag = currentIndex
                        self.selectAll()
                    }
                }
                else//אם לחצתי כדי לכבות
                {
                    selectAll()
                }
            }
            else//הפסקות
            {
                if Global.sharedInstance.isSelectAllRest == false//אם לחצתי כדי להדליק את הכפתור סמן את כל הימים בהפסקות
                {
                    //אם כל הימים בחורים
                    if !Global.sharedInstance.isHoursSelected.contains(false)
                    {
                    //מעבר על שעות הפעילות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                    for i in 0...6
                    {
                        currentFrom = Global.sharedInstance.arrWorkHours[i].nvFromHour
                        currentTo = Global.sharedInstance.arrWorkHours[i].nvToHour
                        
                    if previousFrom == ""
                        {
                            previousFrom = currentFrom
                            previousTo = currentTo
                        }
                        else
                        {
                            if currentFrom != previousFrom || currentTo != previousTo
                            {
                                fIsEquals = false
                                break
                            }
                        }
                    }
                
                    if fIsEquals == true//אם השעות פעילות זהות, יש לבדוק אם ההפסות הבחורות זהות
                    {
                        previousFrom = ""
                        currentFrom = ""
                        previousTo = ""
                        currentTo = ""
                        fIsEquals = true
                        
                        //מעבר על ההפסקות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                        for i in 0...6
                        {
                            if Global.sharedInstance.isHoursSelectedRest[i] == true
                            {
                                currentIndex = i
                                currentFrom = Global.sharedInstance.arrWorkHoursRest[i].nvFromHour
                                currentTo = Global.sharedInstance.arrWorkHoursRest[i].nvToHour
                                
                                //אם משעה ועד שעה שווים או שלא בחרו שעות
                                if currentFrom == currentTo
                                {
                                    fIsEquals = false
                                    break
                                }
                                else if previousFrom == ""
                                {
                                    previousFrom = currentFrom
                                    previousTo = currentTo
                                }
                                else
                                {
                                    if currentFrom != previousFrom || currentTo != previousTo
                                    {
                                        fIsEquals = false
                                        break
                                    }
                                }
                            }
                        }
                        if fIsEquals == true
                        {
                            Global.sharedInstance.currentBtnDayTagRest = currentIndex
                            self.selectAll()
                            
                        }
                    }
                }
                }
                else//אם לחצתי כדי לכבות
                {
                    selectAll()
                }
            }
        }
        else if Global.sharedInstance.currentEditCellOpen == 2//עובדים
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
            {
                if Global.sharedInstance.isSelectAllHoursChild == false//אם לחצתי כדי להדליק את הכפתור סמן את כל הימים בשעות
                {
                    //מעבר על שעות הפעילות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                    for i in 0...6
                    {
                        if Global.sharedInstance.isHoursSelectedChild[i] == true
                        {
                            currentIndex = i
                            currentFrom = Global.sharedInstance.arrWorkHoursChild[i].nvFromHour
                            currentTo = Global.sharedInstance.arrWorkHoursChild[i].nvToHour
                            //אם משעה ועד שעה שווים או שלא בחרו שעות
                            if currentFrom == currentTo
                            {
                                fIsEquals = false
                                break
                            }
                            else if previousFrom == ""
                            {
                                previousFrom = currentFrom
                                previousTo = currentTo
                            }
                            else
                            {
                                if currentFrom != previousFrom || currentTo != previousTo
                                {
                                    fIsEquals = false
                                    break
                                }
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        Global.sharedInstance.currentBtnDayTagChild = currentIndex
                        self.selectAll()
                    }
                }
                else//אם לחצתי כדי לכבות
                {
                    selectAll()
                }
            }
            else//הפסקות
            {
                if Global.sharedInstance.isSelectAllRestChild == false//אם לחצתי כדי להדליק את הכפתור סמן את כל הימים בהפסקות
                {
                    //אם כל הימים בחורים
                    if !Global.sharedInstance.isHoursSelectedChild.contains(false)
                    {
                        //מעבר על שעות הפעילות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                        for i in 0...6
                        {
                            currentFrom = Global.sharedInstance.arrWorkHoursChild[i].nvFromHour
                            currentTo = Global.sharedInstance.arrWorkHoursChild[i].nvToHour
                            
                            if previousFrom == ""
                            {
                                previousFrom = currentFrom
                                previousTo = currentTo
                            }
                            else
                            {
                                if currentFrom != previousFrom || currentTo != previousTo
                                {
                                    fIsEquals = false
                                    break
                                }
                            }
                        }
                        if fIsEquals == true//אם השעות פעילות זהות, יש לבדוק אם ההפסקות הבחורות זהות
                        {
                            previousFrom = ""
                            currentFrom = ""
                            previousTo = ""
                            currentTo = ""
                            fIsEquals = true
                            
                            //מעבר על ההפסקות לוודא שכולן זהות, אחרת לא ניתן לסמן את כל הימים
                            for i in 0...6
                            {
                                if Global.sharedInstance.isHoursSelectedRestChild[i] == true
                                {
                                    currentIndex = i
                                    currentFrom = Global.sharedInstance.arrWorkHoursRestChild[i].nvFromHour
                                    currentTo = Global.sharedInstance.arrWorkHoursRestChild[i].nvToHour
                                    
                                    //אם משעה ועד שעה שווים או שלא בחרו שעות
                                    if currentFrom == currentTo
                                    {
                                        fIsEquals = false
                                        break
                                    }
                                    else if previousFrom == ""
                                    {
                                        previousFrom = currentFrom
                                        previousTo = currentTo
                                    }
                                    else
                                    {
                                        if currentFrom != previousFrom || currentTo != previousTo
                                        {
                                            fIsEquals = false
                                            break
                                        }
                                    }
                                }
                            }
                            if fIsEquals == true
                            {
                                Global.sharedInstance.currentBtnDayTagRestChild = currentIndex
                                self.selectAll()
                            }
                            
                        }
                    }
                }
                else//אם לחצתי כדי לכבות
                {
                    selectAll()
                }
            }
        }
    }
    //פונקציה הבודקת בכל לחיצה על יום או בגלילה, האם כל הימים בחורים וזהים - כדי לדעת האם להדליק את הכפתור ״סמן את כל הימים״
    func checkIfSelectAll()
    {
        var previousFrom = ""
        var currentFrom = ""
        var previousTo = ""
        var currentTo = ""
        var fIsEquals = true
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
            {
                
                //אם כל הימים בחורים
                if !Global.sharedInstance.isHoursSelected.contains(false)
                {
                    //בדיקה האם כל הימים זהים
                    for i in 0...6
                    {
                        currentFrom = Global.sharedInstance.arrWorkHours[i].nvFromHour
                        currentTo = Global.sharedInstance.arrWorkHours[i].nvToHour
                        
                        if previousFrom == ""
                        {
                            previousFrom = currentFrom
                            previousTo = currentTo
                        }
                        else
                        {
                            if currentFrom != previousFrom || currentTo != previousTo
                            {
                                fIsEquals = false
                                break
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        
                        Global.sharedInstance.isSelectAllHours = true
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                    }
                    else
                    {
                        Global.sharedInstance.isSelectAllHours = false
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    
                }
            }
            else//הפסקות
            {
                
                //אם כל ההפסקות בחורות
                if !Global.sharedInstance.isHoursSelectedRest.contains(false)
                {
                    //בדיקה האם כל ההפסקות זהות
                    for i in 0...6
                    {
                        currentFrom = Global.sharedInstance.arrWorkHoursRest[i].nvFromHour
                        currentTo = Global.sharedInstance.arrWorkHoursRest[i].nvToHour
                        
                        if previousFrom == ""
                        {
                            previousFrom = currentFrom
                            previousTo = currentTo
                        }
                        else
                        {
                            if currentFrom != previousFrom || currentTo != previousTo
                            {
                                fIsEquals = false
                                break
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        
                        Global.sharedInstance.isSelectAllRest = true
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                    }
                    else
                    {
                        Global.sharedInstance.isSelectAllRest = false
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    
                }
            }
            
            

        }
        else if Global.sharedInstance.currentEditCellOpen == 2//עובדים
        {
            if Global.sharedInstance.addRecess == false//שעות פעילות ולא הפסקות
            {
                
                //אם כל הימים בחורים
                if !Global.sharedInstance.isHoursSelectedChild.contains(false)
                {
                    //בדיקה האם כל הימים זהים
                    for i in 0...6
                    {
                        currentFrom = Global.sharedInstance.arrWorkHoursChild[i].nvFromHour
                        currentTo = Global.sharedInstance.arrWorkHoursChild[i].nvToHour
                        
                        if previousFrom == ""
                        {
                            previousFrom = currentFrom
                            previousTo = currentTo
                        }
                        else
                        {
                            if currentFrom != previousFrom || currentTo != previousTo
                            {
                                fIsEquals = false
                                break
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        
                        Global.sharedInstance.isSelectAllHoursChild = true
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                    }
                    else
                    {
                        Global.sharedInstance.isSelectAllHoursChild = false
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    
                }
            }
            else//הפסקות
            {
                
                //אם כל ההפסקות בחורות
                if !Global.sharedInstance.isHoursSelectedRestChild.contains(false)
                {
                    //בדיקה האם כל ההפסקות זהות
                    for i in 0...6
                    {
                        currentFrom = Global.sharedInstance.arrWorkHoursRestChild[i].nvFromHour
                        currentTo = Global.sharedInstance.arrWorkHoursRestChild[i].nvToHour
                        
                        if previousFrom == ""
                        {
                            previousFrom = currentFrom
                            previousTo = currentTo
                        }
                        else
                        {
                            if currentFrom != previousFrom || currentTo != previousTo
                            {
                                fIsEquals = false
                                break
                            }
                        }
                    }
                    if fIsEquals == true
                    {
                        
                        Global.sharedInstance.isSelectAllRestChild = true
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "15a.png"), forState: .Normal)
                    }
                    else
                    {
                        Global.sharedInstance.isSelectAllRestChild = false
                        btnSelectAllDays.setBackgroundImage(UIImage(named: "9.png"), forState: .Normal)
                    }
                    
                }
            }
            
            
            
        }
    }

    //checkHoursEquals פונקצית עזר לפונקציה
    //הפונקציה מכבה / מדליקה את היום שקבלה בהתאם למשתנה הבוליאני שקבלה
    func switchBtnDay(index:Int,bool:Bool)
    {
        switch index
        {
        case 0:
            btnDay1.isCecked = false
            break
        case 1:
            btnDay2.isCecked = false
            break
        case 2:
            btnDay3.isCecked = false
            break
        case 3:
            btnDay4.isCecked = false
            break
        case 4:
            btnDay5.isCecked = false
            break
        case 5:
            btnDay6.isCecked = false
            break
        case 6:
            btnDay7.isCecked = false
            break
        default:
            break
        }
    }
  //בדיקה אם ההפסקות שבחר בעבר נמצאות בטווח של השעות, למקרה ששינה את השעות
    func checkIfRestInHours(index:Int)->Bool
    {
        var hhFromHour:Float = 0.0
        var hhToHour:Float = 0.0
        var hhFromHourRest:Float = 0.0
        var hhToHourRest:Float = 0.0
        
        //הפעלת הפונקציה שממריה את השעות וההפסקות לfloat כדי לבדוק את התקינות
        var arr = convertHoursRestsToFloat(index)
        
        hhFromHour = arr[0]
        hhToHour = arr[1]
        hhFromHourRest = arr[2]
        hhToHourRest = arr[3]
        
        if hhFromHourRest < hhFromHour || hhToHourRest > hhToHour
        {
            return false
        }
        return true
    }
    
    //פונקציה הבודקת האם טווח השעות קטן או שווה לשעה
    // ולכן אין לאפשר בחירת הפסקה
    func checkIfCanAddRecess(index:Int) -> Bool {
        
        var hhFromHour:Float = 0.0
        var hhToHour:Float = 0.0
        
        //המרת השעות ל float
        let arr = convertHoursToFloat(index)
        
        hhFromHour = arr[0]
        hhToHour = arr[1]
        
        if (hhFromHour + 1) >= hhToHour
        {
            return false
        }
        
        return true
    }
    
    //פונקצית עזר הממירה את השעות ןההפסקות ל float
    func convertHoursRestsToFloat(index:Int) -> Array<Float> {
        var arrHours:Array<Float> = [0.0,0.0,0.0,0.0]
        
        var hhFromHour:Float = 0.0
        var mmFromHour:Float = 0.0
        var hhToHour:Float = 0.0
        var mmToHour:Float = 0.0
        var hhFromHourRest:Float = 0.0
        var mmFromHourRest:Float = 0.0
        var hhToHourRest:Float = 0.0
        var mmToHourRest:Float = 0.0
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvFromHour)!
            
            let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvToHour)!
            
            
            //hours and minutes from hour
            hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
            mmFromHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
            
            //hours and minutes to hour
            hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
            mmToHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
            
            let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvFromHour)!
            
            //hours and minutes from hour - rest
            hhFromHourRest = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
            mmFromHourRest = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
            
            let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRest[index].nvToHour)!
            
            //hours and minutes to hour - rest
            hhToHourRest = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
            mmToHourRest = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
            
            if mmFromHour != 0
            {
                hhFromHour = hhFromHour + mmFromHour / 60
            }
            
            if mmToHour != 0
            {
                hhToHour = hhToHour + mmToHour / 60
            }
            
            if mmFromHourRest != 0
            {
                hhFromHourRest = hhFromHourRest + mmFromHourRest / 60
            }
            
            if mmToHourRest != 0
            {
                hhToHourRest = hhToHourRest + mmToHourRest/60
            }
            
        }
        else//עובדים
        {
            let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvFromHour)!
            
            let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvToHour)!
            
            
            //hours and minutes from hour
            hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
            mmFromHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
            
            //hours and minutes to hour
            hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
            mmToHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
            
            let hhFromHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvFromHour)!
            
            //hours and minutes from hour - rest
            hhFromHourRest = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourRestDate))
            mmFromHourRest = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourRestDate))
            
            let hhToHourRestDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursRestChild[index].nvToHour)!
            
            //hours and minutes to hour - rest
            hhToHourRest = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourRestDate))
            mmToHourRest = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourRestDate))
            
            if mmFromHour != 0
            {
                hhFromHour = hhFromHour + mmFromHour / 60
            }
            
            if mmToHour != 0
            {
                hhToHour = hhToHour + mmToHour / 60
            }
            
            if mmFromHourRest != 0
            {
                hhFromHourRest = hhFromHourRest + mmFromHourRest / 60
            }
            
            if mmToHourRest != 0
            {
                hhToHourRest = hhToHourRest + mmToHourRest/60
            }
        }
        
        arrHours[0] = hhFromHour
        arrHours[1] = hhToHour
        arrHours[2] = hhFromHourRest
        arrHours[3] = hhToHourRest

        return arrHours
    }
    
    //פונקצית עזר הממירה את השעות בלבד ל float
    func convertHoursToFloat(index:Int) -> Array<Float> {
        var arrHours:Array<Float> = [0.0,0.0]
        
        var hhFromHour:Float = 0.0
        var mmFromHour:Float = 0.0
        var hhToHour:Float = 0.0
        var mmToHour:Float = 0.0
        
        if Global.sharedInstance.currentEditCellOpen == 1//שעות פעילות
        {
            let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvFromHour)!
            
            let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHours[index].nvToHour)!
            
            //hours and minutes from hour
            hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
            mmFromHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
            
            //hours and minutes to hour
            hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
            mmToHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))
            
            
            if mmFromHour != 0
            {
                hhFromHour = hhFromHour + mmFromHour / 60
            }
            
            if mmToHour != 0
            {
                hhToHour = hhToHour + mmToHour / 60
            }
            
        }
        else//עובדים
        {
            let hhFromHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvFromHour)!
            
            let hhToHourDate:NSDate = dateFormatter.dateFromString(Global.sharedInstance.arrWorkHoursChild[index].nvToHour)!
            
            
            //hours and minutes from hour
            hhFromHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhFromHourDate))
            mmFromHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhFromHourDate))
            
            //hours and minutes to hour
            hhToHour = Float(NSCalendar.currentCalendar().component(.Hour, fromDate: hhToHourDate))
            mmToHour = Float(NSCalendar.currentCalendar().component(.Minute, fromDate: hhToHourDate))

            
            if mmFromHour != 0
            {
                hhFromHour = hhFromHour + mmFromHour / 60
            }
            
            if mmToHour != 0
            {
                hhToHour = hhToHour + mmToHour / 60
            }
        }
        
        arrHours[0] = hhFromHour
        arrHours[1] = hhToHour
        
        return arrHours
    }
    
    func ZeroDPMaxMin()
    {
        dtFromHour.maximumDate = .None
        dtFromHour.minimumDate = .None
        dtToHour.maximumDate = .None
        dtToHour.minimumDate = .None
        setDatePickerNull()
    }

}
