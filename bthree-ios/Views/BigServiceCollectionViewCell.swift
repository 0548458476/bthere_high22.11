//
//  BigServiceCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 14.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class BigServiceCollectionViewCell: UICollectionViewCell {

    var delegate:showMoreInfoDelegate!=nil
    var index = 0
    
    @IBOutlet var lblDesc: UILabel!
    
    @IBOutlet var lblPrice: UILabel!
    
    @IBOutlet var lblTime: UILabel!
    
    
    @IBAction func btnOrderService(sender: AnyObject)
    {
        if Global.sharedInstance.arrayServicesKodsToServer.count == 0//לא בחר בחירה מרובה
        {
            Global.sharedInstance.arrayServicesKodsToServer = []
            Global.sharedInstance.serviceName = lblDesc.text!
            Global.sharedInstance.indexRowForIdService = index
            Global.sharedInstance.arrayServicesKodsToServer.append(Global.sharedInstance.arrayServicesKods[index])
        }
        
        delegate.showOdrerTurn()
    }
    @IBOutlet weak var btnOrderService: UIButton!
    
    func setDisplayData(descService:String,time:String,price:String)
    {
        lblDesc.text = descService
        lblPrice.text = "₪\(price)"
        
        lblTime.text = "\(NSLocalizedString("MINUTES", comment: ""))\(time)"
    }
    
    func setDisplayData(pr:objProviderServices)
    {
        Global.sharedInstance.arrayServicesKods.append(pr.iProviderServiceId)
        lblDesc.text = pr.nvServiceName
        if pr.nUntilPrice != 0.0
        {
            lblPrice.text = "₪\(pr.iPrice) - \(pr.nUntilPrice)"
        }
        else
        {
            if pr.iPrice != 0.0
            {
                lblPrice.text = "₪\(String(pr.iPrice))"
            }
            else
            {
                lblPrice.text = ""
            }
        }
        
        if pr.iUntilSeviceTime != 0
        {
            if (pr.iUntilSeviceTime / 60) > 0
            {
                let hours = pr.iUntilSeviceTime / 60
                let minutes  = pr.iUntilSeviceTime % 60
                lblTime.text = "\(pr.iTimeOfService) - \(hours).\(minutes) \(NSLocalizedString("HOURS", comment: ""))"
            }
            else
            {
                lblTime.text = "\(pr.iTimeOfService) - \(pr.iUntilSeviceTime) \(NSLocalizedString("MINUTES", comment: ""))"
            }
        }
        else
        {
            if (pr.iTimeOfService / 60) > 0
            {
                let hours = pr.iTimeOfService / 60
                let minutes  = pr.iTimeOfService % 60
                lblTime.text = "\(hours).\(minutes) \(NSLocalizedString("HOURS", comment: ""))"
            }
            else
            {
                lblTime.text = "\(pr.iTimeOfService) \(NSLocalizedString("MINUTES", comment: ""))"
            }
        }
    }
    
    override func awakeFromNib() {
        Global.sharedInstance.arrayServicesKodsToServer = []
    }

    
}
