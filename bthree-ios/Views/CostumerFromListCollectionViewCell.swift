//
//  CostumerFromListCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 21.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class CostumerFromListCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var defaultImg: UIImageView!
    
    @IBOutlet weak var viewDefaultimg: UIView!
    @IBOutlet weak var imgCostumer: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnStar: UIButton!
    
    @IBOutlet weak var viewSideLine: UIView!
    
    func setDisplayData(imgName:String,costumerName:String,isStar:Bool)
    {
//        viewSideLine.layer.shadowColor = UIColor.blackColor().CGColor
//        viewSideLine.layer.shadowOffset = CGSizeMake(0.0, 1.0)
//        
//        viewSideLine.layer.shadowOpacity = 1
//        viewSideLine.layer.shadowRadius = 4.0
//        let shadowRect: CGRect = CGRectInset(viewSideLine.bounds, 4, 0)
//        // inset top/bottom
//        viewSideLine.layer.shadowPath = UIBezierPath(rect: shadowRect).CGPath
//        self.contentView.bringSubviewToFront(viewSideLine)

        if imgName == ""
        {
            viewDefaultimg.hidden = false
            defaultImg.hidden = false
            imgCostumer.hidden = true
            defaultImg.image = UIImage(named: "supplier-iphone-icons@x1-33.png")
        }
        else
        {
            viewDefaultimg.hidden = true
            defaultImg.hidden = true
            imgCostumer.hidden = false
            imgCostumer.image = UIImage(named: imgName)
        }
        

        lblName.text = costumerName
        btnStar.hidden = !isStar  
    }
}
