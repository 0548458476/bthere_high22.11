
//
//  AppointmentTableViewCell.swift
//  Bthere
//
//  Created by User on 25.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {

    var arrImg:Array<String> = ["detailsAppointment.png","cancelAppointment.png"]
    
    var arrText:Array<String> = [NSLocalizedString("DETAILS_TURN", comment: ""),NSLocalizedString("CANCEL_TURN", comment: "")]
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    
    var indexPathRow:Int = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = Colors.sharedInstance.color6
        self.contentView.addSubview(lineView)
        // Configure the view for the selected state
    }
    
    
    func setDisplayData(index:Int)
    {
        indexPathRow = index
    }
    
}
//MARK: - CollectionView

extension AppointmentTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let event =  Global.sharedInstance.arrEvents[indexPathRow]
        
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("RowInAppointmentCollectionViewCell", forIndexPath: indexPath) as! RowInAppointmentCollectionViewCell
            let componentsStart = calendar.components([.Hour, .Minute], fromDate: event.startDate)
            
            let componentsEnd = calendar.components([.Hour, .Minute], fromDate: event.endDate)
            
            let hourS = componentsStart.hour
            let minuteS = componentsStart.minute
            
            let hourE = componentsEnd.hour
            let minuteE = componentsEnd.minute
            
            var hourS_Show:String = hourS.description
            var hourE_Show:String = hourE.description
            var minuteS_Show:String = minuteS.description
            var minuteE_Show:String = minuteE.description
            
            if hourS < 10
            {
                hourS_Show = "0\(hourS)"
            }
            if hourE < 10
            {
                hourE_Show = "0\(hourE)"
            }
            if minuteS < 10
            {
                minuteS_Show = "0\(minuteS)"
            }
            if minuteE < 10
            {
                minuteE_Show = "0\(minuteE)"
            }
            let st =  "\(hourS_Show):\(minuteS_Show) - \(hourE_Show):\(minuteE_Show)"
            cell.setDisplayData(st, desc:event.title)
            
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("OptionAppointmentCollectionViewCell", forIndexPath: indexPath) as! OptionAppointmentCollectionViewCell
            
            cell1.setDisplayData(arrImg[indexPath.row - 1],desc:arrText[indexPath.row - 1],index: indexPath.row)
            return cell1
        }
    }
}

extension AppointmentTableViewCell : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            //        let itemsPerRow:CGFloat = 1
            //        let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 5.5
            // let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
    }
    
}

