//
//  giveMyServiceTableViewCell.swift
//  Bthere
//
//  Created by User on 10.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class giveMyServiceTableViewCell: UITableViewCell {
    
    var index:Int = 0//שומרת את הסקשין של השורה
    
    
    @IBOutlet weak var colViewResult: UICollectionView!
    
    func addBottomBorder(any:UIView,color:UIColor)
    {
        
        let borderBottom = CALayer()
        borderBottom.frame = CGRectMake(0, CGRectGetHeight(any.layer.frame), CGRectGetWidth(any.layer.frame) + (any.layer.frame.width / 3) , 2)
        
        borderBottom.backgroundColor = color.CGColor;
        
        any.layer.addSublayer(borderBottom)
    }
    
    var timer: NSTimer? = nil 
    var arrText:Array<String> =
        [NSLocalizedString("DELETE", comment: ""),NSLocalizedString("NAVIGATE", comment: ""),NSLocalizedString("ORDER", comment: "")]
    var arrImages:Array<String> =
        ["garb.png","icon-waze.png","27.png"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //בפעם הראשונה שפותחים את הדף - תוצאת החיפוש הראשונה נגללת מעט שמאלה כדי להראות למשתמש את האפשרות לגלול
        if index == 0 && Global.sharedInstance.firstGiveService == true
        {
            Global.sharedInstance.firstGiveService = false
            
            UIView.animateWithDuration(2.5, delay: 1.5, options:UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                
                self.colViewResult.layoutIfNeeded()
                let rightOffset = CGPointMake(self.colViewResult.contentSize.width - self.colViewResult.bounds.size.width, 0)
                self.colViewResult.setContentOffset(rightOffset, animated: true)
                
                self.timer = NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: #selector(self.doDelayed), userInfo: nil, repeats: false)
                
            }) { (completed:Bool) -> Void in
                
            }
        }
        else//fix 2.3
        {
            if  #available(iOS 10.0, *){
                self.colViewResult.layoutIfNeeded()
                self.colViewResult.setContentOffset(CGPoint(x: 0 , y: 0), animated: false)
                
            }
        }
    }
    
    func doDelayed(t: NSTimer) {
        
        UIView.animateWithDuration(2.5, delay: 2.5, options:UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            self.colViewResult.layoutIfNeeded()
            self.colViewResult.setContentOffset(CGPoint(x: 0 , y: 0), animated: true)
            
        }) { (completed:Bool) -> Void in
            
        }
    
    
    }
}

    extension giveMyServiceTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
        //        return Global.sharedInstance.dicResults.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("big", forIndexPath: indexPath) as! bigCollectionViewCell
            cell.setDisplayData(Global.sharedInstance.dicResults[index])
            cell.indexRow = index
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("smallGiveMyServicesCollectionViewCell", forIndexPath: indexPath) as! smallGiveMyServicesCollectionViewCell
            cell1.delegateDeleteItem = Global.sharedInstance.giveServices
            cell1.delegateCloseCollection = Global.sharedInstance.giveServices
            cell1.setDisplayData(arrImages[indexPath.row - 1], text: arrText[indexPath.row - 1],index: indexPath.row)
            cell1.indexRow = index
            cell1.coll = colViewResult
            
            return cell1
        }
    }
}

extension giveMyServiceTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 4.5
            // let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
    }
    func setDisplayData(indexPath:Int) {
        index = indexPath
    }
    
}
