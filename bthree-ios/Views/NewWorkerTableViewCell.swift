//
//  NewWorkerTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/2/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol reloadTableForNewWorkerDelegate{
    func reloadTableForNewWorker(cell:NewWorkerTableViewCell)
}
class NewWorkerTableViewCell: UITableViewCell {
    var delegate:reloadTableForNewWorkerDelegate! = nil
    var isOpen:Bool = false
    
    @IBAction func btnNewWorker(sender: UIButton) {
        delegate.reloadTableForNewWorker(self)
    }
    
    @IBOutlet var addWorker: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addWorker.text = NSLocalizedString("ADD_WORKER", comment: "")
    }

    override func layoutSubviews() {
        let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(lineView)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
