//
//  bigCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 7.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class bigCollectionViewCell: UICollectionViewCell {
    var indexRow:Int = 0
    var ProviderServicesArray:Array<objProviderServices> =  Array<objProviderServices>()
    
    var storyboard1 = UIStoryboard(name: "Main", bundle: nil)
    
    @IBAction func btnOpenBusiness(sender: AnyObject) {
        // ListServicesViewController
        Global.sharedInstance.providerID = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
        Global.sharedInstance.viewCon!.indexRow = indexRow
       
        getProviderServicesForSupplierFunc()

 }
    func getProviderServicesForSupplierFunc()
    {
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = Global.sharedInstance.providerID
        var arrUsers:Array<User> = Array<User>()
        
        api.sharedInstance.getProviderServicesForSupplier(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                var Arr:NSArray = NSArray()
                
                Arr = responseObject["Result"] as! NSArray
                let ps:objProviderServices = objProviderServices()
             
                self.ProviderServicesArray = ps.objProviderServicesToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                if self.ProviderServicesArray.count == 0
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
                }
                else
                {
                    Global.sharedInstance.viewCon!.ProviderServicesArray = self.ProviderServicesArray
                    
                    let frontviewcontroller:UINavigationController = UINavigationController()
                    frontviewcontroller.pushViewController(Global.sharedInstance.viewCon!, animated: false)
                    //initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = self.storyboard1.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                }
            }
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                
        })
        
    }

    @IBOutlet weak var btnOpenBusiness: UIButton!

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewBlack: UIView!
    
    @IBOutlet weak var imgImage: UIImageView!
    
    //עפולה, 2 ק״מ ממיקומך
    @IBOutlet weak var imgWaze: UIImageView!
    
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var lblNumKM: UILabel!
    
    @IBOutlet weak var lblKMFromYou: UILabel!
    
    // דירוג: 8.9 | 32 מדרגים
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var lblNumRuting: UILabel!
    
    @IBOutlet weak var lblNumVoters: UILabel!
    
    @IBOutlet weak var lblVoters: UILabel!

    func setDisplayData(result:Dictionary<String,AnyObject>)
    {
        lblVoters.text = NSLocalizedString("LEVEL", comment: "")
        //Global.sharedInstance.resultOfSearch = result
        self.bringSubviewToFront(viewBlack)
        
        lblName.text = result["nvProviderName"]?.description
        lblDesc.text = result["nvProviderSlogan"]?.description
        lblCity.text = result["nvAdress"]?.description
        if result["iDistance"]?.description != "-1"
        {
            lblNumKM.text = String(format: "%.2f", (result["iDistance"] as! Float))
                //result["iDistance"]?.description
            lblKMFromYou.text = NSLocalizedString("KILOMETER_FROM_YOU", comment: "")
        }
        else
        {
            lblNumKM.text = ""
            lblKMFromYou.text = ""
        }
        lblNumVoters.text = result["iCustomerRank"]?.description
        lblNumRuting.text = result["IInternalRank"]?.description
        
        
        
        var dataDecoded:NSData = NSData(base64EncodedString: (result["nvProviderHeader"]?.description)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        var decodedimage:UIImage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            imgImage.image = decodedimage
            
        }
        else
        {
            imgImage.image = UIImage(named: "IMG_05072016_131013.png")
        }
        
        dataDecoded = NSData(base64EncodedString: (result["nvProviderLogo"]?.description)!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            imgLogo.image = decodedimage
            imgLogo.contentMode = .ScaleAspectFill
        }
        else
        {
            imgLogo.backgroundColor = Colors.sharedInstance.color4
            imgLogo.image = UIImage(named: "clients@x1.png")
            imgLogo.contentMode = .ScaleAspectFit
        }    
    }
    
}
