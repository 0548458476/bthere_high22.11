//
//  RowRecordTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/14/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class RowRecordTableViewCell: UITableViewCell {

    //MARK: - OUTLET
    
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblHour: UILabel!
    
    //MARK: - PROPERTIES
    
    var event:allKindEventsForListDesign = allKindEventsForListDesign()
    var calendar:NSCalendar = NSCalendar.currentCalendar()
    var delegate:openDetailsOrderDelegate!=nil
    
    //MARK: - INITIAL
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapOrderTurn))
        self.contentView.addGestureRecognizer(tap)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = Colors.sharedInstance.color6
        self.contentView.addSubview(lineView)
    }
    
    //EventFrom - מציין האם הארוע מהמכשיר =2 או מביזר =1
    func setDisplayData(hourDesc:String,desc:String,EventFrom:Int){
        
        if EventFrom == 1//ביזר
        {
            lblDesc.font = UIFont (name: "OpenSansHebrew-Bold", size: 16)
            lblHour.font = UIFont (name: "OpenSansHebrew-Bold", size: 16)
        }
        else
        {
            lblDesc.font = UIFont (name: "OpenSansHebrew-Light", size: 16)
            lblHour.font = UIFont (name: "OpenSansHebrew-Light", size: 16)
        }
        
        lblDesc.text = desc
        lblHour.text = hourDesc
    }
    
    func tapOrderTurn(sender:UITapGestureRecognizer)
    {
        Global.sharedInstance.whichDesignOpenDetailsAppointment = 3
        
        //בודק האם ה-cell שעליו לחצו הוא תור של ביזר ואז בלחיצה יש להציג את פרטי התור
        if event.tag == 1
        {
            Global.sharedInstance.hourBthereEvent = event.fromHour
            
            let orderDetailsFoBthereEvent:OrderDetailsObj = OrderDetailsObj(
                _iCoordinatedServiceId: 0,
                _iProviderUserId: 0,
                _nvFirstName: "",
                _nvSupplierName: event.nvSupplierName,
                _objProviderServiceDetails: [],
                _iDayInWeek: 0,
                _dtDateOrder: event.dateEvent,
                _nvFromHour: event.fromHour,
                _nvToHour: event.toHour,
                _nvAddress: event.nvAddress,
                _nvComment: event.nvComment,
                _nvLogo: "",
                _iUserId: 0)
            
            Global.sharedInstance.orderDetailsFoBthereEvent = orderDetailsFoBthereEvent
            Global.sharedInstance.dateEventBthereClick = event.dateEvent
            Global.sharedInstance.serviceName = event.nvServiceName
            Global.sharedInstance.dayFreeEvent = NSDateFormatter().weekdaySymbols[event.iDayInWeek - 1]
            
            
            delegate.openDetailsOrder(3)//רשימה של יומן לקוח
            
        }
    }

}
