//
//  MyWaitingListTableViewCell.swift
//  bthree-ios
//
//  Created by User on 29.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class MyWaitingListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var colWaitingList: UICollectionView!
    
    var delegate:deleteFromWaitingListDelegate!=nil
    var indexRow:Int = 0
    let calendar = NSCalendar.currentCalendar()
    var today:NSDate = NSDate()
    var imagesArray:Array <String> = ["22.png","26.png","28.png"]
    var datesArray:Array <NSDate> = [NSDate(),NSDate(timeIntervalSinceReferenceDate: -12345.0),NSDate(timeIntervalSinceReferenceDate: +123000089.0)]
    
    var textArray:Array <String> =
        ["תספורת אישה בחצי מחיר",
         "1 + 1 על מסיכות השיער",
         "גוונים ב₪200 עד השעה 2:00"]
    
    var generic:Generic = Generic()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setDisplayData(index:Int)
    {
        indexRow = index
    }
    
    
    func getHour(date:NSDate)-> String
    {
        let components = calendar.components([.Hour,.Minute], fromDate: date)
        let hour =  components.hour
        let minute =  components.minute
        return "\(hour):\(minute)"
    }
    
    func shortDate(date:NSDate) -> String {
        
        let components = calendar.components([.Day, .Month, .Year], fromDate: date)
        
        let components1 = calendar.components([.Day, .Month, .Year], fromDate: NSDate())
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        let yearToday =  components1.year
        let monthToday = components1.month
        let dayToday = components1.day
        
        if year == yearToday && month == monthToday && day == dayToday
        {
            return NSLocalizedString("TODAY", comment: "")
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.stringFromDate(date)
    }
    
}


extension MyWaitingListTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("BigWaitingListCollectionViewCell", forIndexPath: indexPath) as! BigWaitingListCollectionViewCell
            
//            cell.setDisplayData(imagesArray[indexRow], date: shortDate(datesArray[indexRow]),hour:  getHour(datesArray[indexRow]), text: textArray[indexRow])
            
            cell.setDisplayData(Global.sharedInstance.arrWaitingList[indexRow].nvLogo, date:shortDate(Global.sharedInstance.arrWaitingList[indexRow].dtDateOrder), hour: getHour(Global.sharedInstance.arrWaitingList[indexRow].dtDateOrder), text: Global.sharedInstance.arrWaitingList[indexRow].nvComment)
            
            
            cell.index = indexRow
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("SmallWaitingListCollectionViewCell", forIndexPath: indexPath) as! SmallWaitingListCollectionViewCell
            
            cell1.index = indexRow
            
            return cell1
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1
        {
            var dicDelete:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            
            dicDelete["iWaitingForServiceId"] = Global.sharedInstance.arrWaitingList[indexRow].iWaitingForServiceId
            
            
                api.sharedInstance.DeleteFromWaitingList(dicDelete, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                    
                    if responseObject["Error"]!!["ErrorCode"] as! Int != 1
                    {
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("ERROR_SERVER", comment: ""))
                    }
                    else
                    {
                        Global.sharedInstance.arrWaitingList.removeAtIndex(self.indexRow)
                        self.delegate.deleteFromWaitingList()
                    }
                    },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                        Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_INTERNET", comment: ""))
                })
        }
    }
    
    
}

extension MyWaitingListTableViewCell : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 4.5
            let itemWidth = collectionView.bounds.width / itemsPerRow
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
}
}


