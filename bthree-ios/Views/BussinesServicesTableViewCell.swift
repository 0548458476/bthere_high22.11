//
//  BussinesServicesTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/2/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

protocol delKbNotifBusinessDelegate {
    func delKbNotifBusiness()
}


class BussinesServicesTableViewCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,delKbNotifBusinessDelegate,saveDataToWorkerDelegate{
    
    
    //MARK: - Properties
    var delegateKb:delKbNotificationDelegate!=nil
    var delegateKbCalendar:delKbCalenderNotifDelegate!=nil
    
    var arrayServiceProduct:Array<String> = [NSLocalizedString("CUT_HAIR", comment: ""),NSLocalizedString("HAIR_DO", comment: ""),NSLocalizedString("LEVEL", comment: "")]
    var arrayOptions:Array<String> = [NSLocalizedString("PRODUCT", comment: ""),NSLocalizedString("SERVICE", comment: "")]
    var delegateSave:reloadTableForSaveDelegate! = nil
    var delegateScroll:scrollOnEditDelegate!=nil
    
    var fromPrice:Float = 0 // לשמירת ממחיר
    var untilPrice:Float = 0 // לשמירת עד מחיר
    var fromServiceTime:Int = 0 //לשמירת ממשך זמן
    var untilServiceTime:Int = 0 //לשמירת עד משך זמן
    
    var selectedTextField:UITextField?
    
    //MARK: - Outlet
    
    @IBOutlet weak var tblSelectOption: UITableView!
    
    @IBOutlet weak var lblServiceProduct: UILabel!
    @IBOutlet weak var lblDuringTime: UILabel!
    @IBOutlet weak var lblSale: UILabel!
    @IBOutlet weak var lblRangTime: UILabel!
    @IBOutlet weak var lblNumCustomers: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var txtServiceName: UITextField!
    @IBOutlet weak var lblInputServiceName: UILabel!
    @IBOutlet weak var lblShowPrice: UILabel!
    @IBOutlet weak var btnNoSelect: checkBoxForDetailsWorker!
    @IBAction func btnNoSelect(sender: AnyObject) {
        //btnYesSelect.isCecked = btnNoSelect.isCecked
        btnYesSelect.isCecked = false
        btnNoSelect.isCecked = true
    }
    
    @IBOutlet weak var btnYesSelect: CheckBoxForDetailsWorker2!
    @IBAction func btnYesSelect(sender: AnyObject) {
        //btnNoSelect.isCecked = btnYesSelect.isCecked
        btnYesSelect.isCecked = true
        btnNoSelect.isCecked = false
    }
    @IBOutlet weak var txtTimeInterval: UITextField!
    
    @IBOutlet weak var txtPrice: UITextField!
    
    @IBOutlet weak var maxConcurrentCustomers: UITextField!
    
    @IBOutlet weak var txtTimeOfService: UITextField!
    
    @IBOutlet weak var txtDiscount: UITextField!
    
    
    
    
    
    
    @IBAction func btnSave(sender: UIButton)
    {
        Global.sharedInstance.fIsSaveConBussinesServicesPressed = true
        Global.sharedInstance.selectedCellForEditService.append(false)
        
        if txtDiscount.text == "" || txtDiscount.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtDiscount.text == NSLocalizedString("NOT_VALID", comment: "")
        {
            //txtDiscount.textColor = UIColor.redColor()
            //txtDiscount.text = NSLocalizedString("REQUIREFIELD", comment: "")
            Global.sharedInstance.isValidDiscount = false
        }
        else
        {
            Global.sharedInstance.isValidDiscount = true
            
            if Global.sharedInstance.fDiscount == false
            {
                Global.sharedInstance.fDiscount = true
                if txtDiscount.text != "0%"
                {
                    txtDiscount.text = txtDiscount.text! + "%"
                }
            }
        }
        if lblInputServiceName.text == NSLocalizedString("SERVICE", comment: ""){
            if txtPrice.text == "" || txtPrice.text == NSLocalizedString("REQUIREFIELD", comment: "") || lblInputServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                //txtPrice.textColor = UIColor.redColor()
                //txtPrice.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else if validationPrice(txtPrice.text!) == false{
                 txtPrice.textColor = UIColor.redColor()
                 txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else
            {
                Global.sharedInstance.isValidPrice = true
            }
            
            if txtServiceName.text == "" || txtServiceName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                Global.sharedInstance.isValidServiceName = false
            }
            else if isValidString(txtServiceName.text!) == false
            {
                txtServiceName.textColor = UIColor.redColor()
                txtServiceName.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else
            {
                Global.sharedInstance.isValidServiceName = true
            }
            
            if txtTimeInterval.text == "" || txtTimeInterval.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeInterval.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                Global.sharedInstance.isValidTimeInterval = false
            }
            else
            {
                Global.sharedInstance.isValidTimeInterval = true
            }
            if txtTimeOfService.text == "" || txtTimeOfService.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeOfService.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                Global.sharedInstance.isValidTimeOfService = false
            }
            else if validationTimeOfService(txtTimeOfService.text!) == false{
                 txtTimeOfService.textColor = UIColor.redColor()
                 txtTimeOfService.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidTimeOfService = false
            }
            else
            {
                Global.sharedInstance.isValidTimeOfService = true
            }
            
            if maxConcurrentCustomers.text == "" || maxConcurrentCustomers.text == NSLocalizedString("REQUIREFIELD", comment: "") || maxConcurrentCustomers.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                //maxConcurrentCustomers.textColor = UIColor.redColor()
                //maxConcurrentCustomers.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidMaxConcurrent = false
            }
            else
            {
                Global.sharedInstance.isValidMaxConcurrent = true
            }
        }
        else{
            if txtPrice.text == "" || txtPrice.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtPrice.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                Global.sharedInstance.isValidPrice = false
            }
            else if validationPrice(txtPrice.text!) == false{
                 txtPrice.textColor = UIColor.redColor()
                 txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else
            {
                Global.sharedInstance.isValidPrice = true
            }
            
            if txtServiceName.text == "" || txtServiceName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                //txtServiceName.textColor = UIColor.redColor()
                //txtServiceName.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else if isValidString(txtServiceName.text!) == false
            {
                txtServiceName.textColor = UIColor.redColor()
                txtServiceName.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
                
            else
            {
                Global.sharedInstance.isValidServiceName = true
            }
            
        }
        
        if lblInputServiceName.text == NSLocalizedString("SERVICE", comment: ""){
            if Global.sharedInstance.isValidDiscount == true && Global.sharedInstance.isValidPrice == true &&  Global.sharedInstance.isValidServiceName == true &&  Global.sharedInstance.isValidTimeInterval == true &&  Global.sharedInstance.isValidTimeOfService == true &&  Global.sharedInstance.isValidMaxConcurrent == true
            {
                delegateSave.reloadTableForSave(self.tag,btnTag: btnSave.tag)
                
                let discount = Global.sharedInstance.cutStringBySpace(txtDiscount.text!, strToCutBy: "%")
                
                var objProviderServicess:objProviderServices = objProviderServices()
                
                objProviderServicess = objProviderServices(
                    _nvServiceName: txtServiceName.text!/*txtServiceName.text!*/,
                    _iPrice:fromPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _nUntilPrice:untilPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _iTimeOfService: fromServiceTime,//Global.sharedInstance.parseJsonToInt(txtTimeOfService.text!),
                    _iUntilSeviceTime: untilServiceTime,//Global.sharedInstance.parseJsonToInt(txtTimeOfService.text!),
                    //_iConcurrentCustomers: Int(.text!)!,
                    _iTimeInterval: Global.sharedInstance.parseJsonToInt(txtTimeInterval.text!),
                    _iMaxConcurrentCustomers: Global.sharedInstance.parseJsonToInt(maxConcurrentCustomers.text!),
                    _iDiscount: Global.sharedInstance.parseJsonToInt(discount[0]),
                    _iServiceType:90,_bDisplayPerCustomer:btnYesSelect.isCecked)
                //_iMinConcurrentCustomers: Global.sharedInstance.parseJsonToInt(txtMinConcurrentCustomers.text!),
                
                if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0 || txtPrice.tag == -11//פעם ראשונה
                {
                    Global.sharedInstance.generalDetails.arrObjProviderServices.append(objProviderServicess)
                }
                else{
                    Global.sharedInstance.generalDetails.arrObjProviderServices.removeAtIndex(txtPrice.tag)
                    Global.sharedInstance.generalDetails.arrObjProviderServices.insert(objProviderServicess, atIndex: txtPrice.tag)
                }
                Global.sharedInstance.fIsEmptyBussinesServices = false
                Global.sharedInstance.headersCellRequired[4] = true
            }
            else
            {
                Global.sharedInstance.fIsEmptyBussinesServices = true
            }
        }
        else{
            if Global.sharedInstance.isValidPrice == true &&  Global.sharedInstance.isValidServiceName == true {
                delegateSave.reloadTableForSave(self.tag,btnTag: btnSave.tag)
                
                let discount = Global.sharedInstance.cutStringBySpace(txtDiscount.text!, strToCutBy: "%")
                
                var objProviderServicess:objProviderServices = objProviderServices()
                
                
                objProviderServicess = objProviderServices(
                    _nvServiceName: txtServiceName.text!,
                    _iPrice: fromPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _nUntilPrice: untilPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _iTimeOfService: 0,
                    _iUntilSeviceTime: 0,
                    //_iConcurrentCustomers: Int(.text!)!,
                    _iTimeInterval: 0,
                    _iMaxConcurrentCustomers: 0,
                    _iDiscount: Global.sharedInstance.parseJsonToInt(discount[0]),
                    _iServiceType:89,_bDisplayPerCustomer:btnYesSelect.isCecked)
                //_iMinConcurrentCustomers: Global.sharedInstance.parseJsonToInt(txtMinConcurrentCustomers.text!),
                
                if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0 || txtPrice.tag == -11//פעם ראשונה
                {
                    Global.sharedInstance.generalDetails.arrObjProviderServices.append(objProviderServicess)
                }
                else{
                    Global.sharedInstance.generalDetails.arrObjProviderServices.removeAtIndex(txtPrice.tag)
                    Global.sharedInstance.generalDetails.arrObjProviderServices.insert(objProviderServicess, atIndex: txtPrice.tag)
                }
                Global.sharedInstance.fIsEmptyBussinesServices = false
                Global.sharedInstance.headersCellRequired[4] = true
            }
            else
            {
                // Global.sharedInstance.headersCellRequired[4] = false
                Global.sharedInstance.fIsEmptyBussinesServices = true
            }
            
        }
        
    }
    @IBOutlet var btnSave: UIButton!
    
    
    //MARK: - Initial
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        let tapOpenTbl:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openTbl))
        viewOpenTable.addGestureRecognizer(tapOpenTbl)
        
        Global.sharedInstance.GlobalDataVC?.delegateSaveBussines = self
        
        lblServiceProduct.text = NSLocalizedString("SERVICE_PRODUCT", comment: "")
        lblServiceName.text = NSLocalizedString("CHOOESE_SERVICE_PRDUCT", comment: "")
        lblPrice.text = NSLocalizedString("PRICE", comment: "")
        lblShowPrice.text = NSLocalizedString("SHOW_PRICE", comment: "")
        lblDuringTime.text = NSLocalizedString("DURING_TIME", comment: "")
        lblNumCustomers.text = NSLocalizedString("NUM_IN_PARALLEL", comment: "")
        lblRangTime.text = NSLocalizedString("TIME_SPACE", comment: "")
        lblSale.text = NSLocalizedString("DISCOUNT_ENTER_SOME_TURN", comment: "")
        
        lblInputServiceName.text = NSLocalizedString("SERVICE", comment: "")
        lblServiceName.text = NSLocalizedString("SERVICE_NAME", comment: "")
        tblSelectOption.hidden = true
        Global.sharedInstance.businessService = self
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            lblDuringTime.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            //            lblMinCustomers.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblNumCustomers.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblPrice.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblRangTime.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblSale.font = UIFont(name: "OpenSansHebrew-Light", size: 12.5)
            lblServiceName.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblShowPrice.font = UIFont(name: "OpenSansHebrew-Light", size: 16)
            
        }
        txtDiscount.delegate = self
        //       txtMinConcurrentCustomers.delegate = self
        txtPrice.delegate = self
        txtServiceName.delegate = self
        txtTimeInterval.delegate = self
        txtTimeOfService.delegate = self
        maxConcurrentCustomers.delegate = self
        tblSelectOption.layer.borderColor = UIColor.whiteColor().CGColor
        tblSelectOption.layer.borderWidth = 1
        tblSelectOption.allowsSelection = true
        
        //openServieProduct.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        // btnOpenRangTbl.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        //btnOpenTblTimeDuring.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        //btnOpenTimeTbl.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        //         btnOpenTimeTbl.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        //tblProductService.hidden = true
        // Initialization code
        //tblProductService.delegate = self
        //tblProductService.dataSource = self
        ////tblProductService.separatorStyle = .None
        //tblTimeDuring.hidden = true
        // Initialization code
        //tblTimeDuring.delegate = self
        //tblTimeDuring.dataSource = self
        //tblTimeDuring.separatorStyle = .None
        //openServieProduct.bringSubviewToFront(imgOpenMenu)
        //tblRangTime.hidden = true
        // Initialization code
        //tblRangTime.delegate = self
        //tblRangTime.dataSource = self
        //tblRangTime.separatorStyle = .None
        
        tblSelectOption.delegate = self
        tblSelectOption.dataSource = self
        self.bringSubviewToFront(tblSelectOption)
    }
    
    func setDisplayData(service:objProviderServices){
        
        txtServiceName.textColor = UIColor.blackColor()
        txtPrice.textColor = UIColor.blackColor()
        txtDiscount.textColor = UIColor.blackColor()
        txtTimeInterval.textColor = UIColor.blackColor()
        txtTimeOfService.textColor = UIColor.blackColor()
        maxConcurrentCustomers.textColor = UIColor.blackColor()
        
        txtServiceName.text = service.nvServiceName
        if service.nUntilPrice > 0
        {
            if ((Int)( service.iPrice * 10 ) % 10) == 0 && ((Int)( service.nUntilPrice * 10 ) % 10) == 0
            {
                txtPrice.text = "\(String(Int(service.iPrice)))-\(String(Int(service.nUntilPrice)))"
            }
            else if ((Int)( service.iPrice * 10 ) % 10) > 0 && ((Int)( service.nUntilPrice * 10 ) % 10) == 0
            {
                txtPrice.text = "\(String(service.iPrice))-\(String(Int(service.nUntilPrice)))"
            }
            else if ((Int)( service.iPrice * 10 ) % 10) == 0 && ((Int)( service.nUntilPrice * 10 ) % 10) > 0
            {
                txtPrice.text = "\(String(Int(service.iPrice)))-\(String(service.nUntilPrice))"
            }
            else
            {
                txtPrice.text = "\(String(service.iPrice))-\(String(service.nUntilPrice))"
            }
        }
        else
        {
            if ((Int)( service.iPrice * 10 ) % 10) == 0
            {
                txtPrice.text = String(Int(service.iPrice))
            }
            else
            {
                txtPrice.text = String(service.iPrice)
            }
        }
        
        txtDiscount.text = String(service.iDiscount) + "%"
        if service.iServiceType == 90{
            lblInputServiceName.text = NSLocalizedString("SERVICE", comment: "")
            lblServiceName.text = NSLocalizedString("SERVICE_NAME", comment: "")
            txtTimeInterval.text = String( service.iTimeInterval)
            
            if service.iUntilSeviceTime > 0
            {
                if ((Int)( service.iTimeOfService * 10 ) % 10) == 0 && ((Int)( service.iUntilSeviceTime * 10 ) % 10) == 0
                {
                    txtTimeOfService.text = "\(String(Int(service.iTimeOfService)))-\(String(Int(service.iUntilSeviceTime)))"
                }
                else if ((Int)( service.iTimeOfService * 10 ) % 10) > 0 && ((Int)( service.iUntilSeviceTime * 10 ) % 10) == 0
                {
                    txtTimeOfService.text = "\(String(service.iTimeOfService))-\(String(Int(service.iUntilSeviceTime)))"
                }
                else if ((Int)( service.iTimeOfService * 10 ) % 10) == 0 && ((Int)( service.iUntilSeviceTime * 10 ) % 10) > 0
                {
                    txtTimeOfService.text = "\(String(Int(service.iTimeOfService)))-\(String(service.iUntilSeviceTime))"
                }
                else
                {
                    txtTimeOfService.text = "\(String(service.iTimeOfService))-\(String(service.iUntilSeviceTime))"
                }
            }
            else
            {
                if ((Int)( service.iTimeOfService * 10 ) % 10) == 0
                {
                    txtTimeOfService.text = String(Int(service.iTimeOfService))
                }
                else
                {
                    txtTimeOfService.text = String(service.iTimeOfService)
                }
            }
            
            maxConcurrentCustomers.text = String(service.iMaxConcurrentCustomers)
        }
        else{
            lblInputServiceName.text = NSLocalizedString("PRODUCT", comment: "")
            lblServiceName.text = NSLocalizedString("PRODUCT_NAME", comment: "")
            txtTimeInterval.text = ""
            txtTimeOfService.text = ""
            maxConcurrentCustomers.text = ""
            
        }
        if service.bDisplayPerCustomer == true
        {
            btnYesSelect.isCecked = true
        }
        else
        {
            btnNoSelect.isCecked = true
        }
        //        txtMinConcurrentCustomers.text = String(service.iMinConcurrentCustomers)
        
        
    }
    
    func setDisplayDataNull()
    {
        
        Global.sharedInstance.isNewServicePlusOpen = true // נדלק לאחר שהוסיף שרות או מוצר, כדי שבעת גלילה לא יתרוקנו הנתונים
        txtTimeInterval.text = "5"
        txtDiscount.text = "0%"
        txtPrice.text = ""
        txtServiceName.text = ""

        txtTimeOfService.text = "6"
        //       txtMinConcurrentCustomers.text = ""
        maxConcurrentCustomers.text = "1"
        btnYesSelect.isCecked = true
        btnNoSelect.isCecked = false
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != tblSelectOption{
            return  arrayServiceProduct.count
        }
        return arrayOptions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView != tblSelectOption{
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("UITableViewCell")! as UITableViewCell
            
            //        let lineView: UIView = UIView(frame: CGRectMake(0, cell.contentView.frame.height - 1, cell.contentView.frame.size.width + 60, 1))
            //        lineView.backgroundColor = UIColor.whiteColor()
            //        cell.contentView.addSubview(lineView)
            cell.textLabel!.text = arrayServiceProduct[indexPath.row]
            cell.textLabel!.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
            cell.textLabel?.textColor = UIColor.whiteColor()
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("CalendarDesignTableViewCell")as!CalendarDesignTableViewCell
        //        if indexPath.row == 1{
        //        let lineView: UIView = UIView(frame: CGRectMake(0, cell.contentView.frame.height - 1, cell.contentView.frame.size.width + 60, 1))
        //        lineView.backgroundColor = UIColor.whiteColor()
        //        cell.contentView.addSubview(lineView)
        //        }
        cell.selectionStyle = .None
        cell.lblText?.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
        //cell.lblText!.text = arrayServiceProduct[indexPath.row]
        cell.setDisplayData(arrayOptions[indexPath.row])
        cell.lblText?.textColor = UIColor.whiteColor()
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {var str:String = ""
        if tableView != tblSelectOption{
            //         str = (tblProductService.cellForRowAtIndexPath(indexPath)?.textLabel?.text)!
        }
        else
        {
            str = arrayOptions[indexPath.row]
        }
        
        switch (tableView)
        {
        case tblSelectOption:
            lblInputServiceName.text = str
            tblSelectOption.hidden = true
            btnOpenTbl.tag = 0
            if lblInputServiceName.text == NSLocalizedString("PRODUCT", comment: "")
            {
                lblServiceName.text = NSLocalizedString("CHOSE_PRODUCT_NAME", comment: "")
                lblSale.text = NSLocalizedString("DISCOUNT_ORDRER_MORE_PRODUCTS", comment: "")
                lblServiceName.text = NSLocalizedString("PRODUCT_NAME", comment: "")
                txtTimeInterval.enabled = false
                txtTimeInterval.text = ""
                maxConcurrentCustomers.enabled = false
                maxConcurrentCustomers.text = ""
                txtTimeOfService.enabled = false
                txtTimeOfService.text = ""
                txtPrice.text = ""
                txtServiceName.text = ""
                txtDiscount.text = "0%"
                //                txtMinConcurrentCustomers.enabled = false
            }
            else
            {
                lblServiceName.text = NSLocalizedString("CHOOSE_SERVICE_NAME", comment: "")
                lblServiceName.text = NSLocalizedString("SERVICE_NAME", comment: "")
                txtTimeInterval.enabled = true
                txtTimeOfService.enabled = true
                txtTimeOfService.text = "6"
                txtTimeInterval.text = "5"
                txtPrice.text = ""
                txtServiceName.text = ""
                txtDiscount.text = "0%"
                maxConcurrentCustomers.enabled = true
                maxConcurrentCustomers.text = "1"
            }
        default:
            return
            
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tblSelectOption.frame.height/2    }
    
    
    //MARK: - text field
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if delegateKbCalendar != nil
        {
            delegateKbCalendar.delKbCalenderNotif()
        }
        if delegateKb != nil
        {
            // remove the keyBoard event from itemInSection3 cell
            delegateKb.delKbNotification()
        }
        
        if Global.sharedInstance.didServicesClosed == true
        {
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(ItemInSection3TableViewCell.keyboardWillShow(_:)),name: UIKeyboardWillShowNotification,object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(ItemInSection3TableViewCell.keyboardWillHide(_:)),name: UIKeyboardWillHideNotification,object: nil)
            Global.sharedInstance.didServicesClosed = false
        }
        
        
        if textField.text == NSLocalizedString("REQUIREFIELD", comment: "") || textField.text == NSLocalizedString("NOT_VALID", comment: "")
        {
            textField.text = ""
        }
        textField.textColor = UIColor.blackColor()
        selectedTextField = textField
        
        if textField == txtDiscount
        {
            if textField.text == "0%"
            {
                Global.sharedInstance.fDiscount = false
                textField.text = ""
            }
            else if textField.text == ""
            {
                Global.sharedInstance.fDiscount = false
            }
            else if txtDiscount.text!.rangeOfString("%") != nil
            {
                Global.sharedInstance.fDiscount = false
                txtDiscount.text =  txtDiscount.text!.substringToIndex(txtDiscount.text!.endIndex.predecessor())
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == txtServiceName
        {
            if txtServiceName.text == "" || txtServiceName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                Global.sharedInstance.isValidServiceName = false
            }
            else if isValidString(txtServiceName.text!) == false
            {
                txtServiceName.textColor = UIColor.redColor()
                txtServiceName.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else
            {
                Global.sharedInstance.isValidServiceName = true
            }
        }
            
        else if textField == txtPrice
        {
            if txtPrice.text == "" || txtPrice.text == NSLocalizedString("REQUIREFIELD", comment: "") || lblInputServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                Global.sharedInstance.isValidPrice = false
            }
            else if validationPrice(txtPrice.text!) == false{
                txtPrice.textColor = UIColor.redColor()
                txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else
            {
                Global.sharedInstance.isValidPrice = true
            }
            
            let arrPrice = Global.sharedInstance.cutStringBySpace(txtPrice.text!, strToCutBy: "-")
            if arrPrice.count == 2
            {
                if Int(arrPrice[1]) < Int(arrPrice[0])
                {
                    txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                    txtPrice.textColor = UIColor.redColor()
                }
            }
            
            
        }
        else if textField == txtTimeOfService
        {
            if txtTimeOfService.text == "" || txtTimeOfService.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeOfService.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                Global.sharedInstance.isValidTimeOfService = false
            }
            else if validationTimeOfService(txtTimeOfService.text!) == false{
                txtTimeOfService.textColor = UIColor.redColor()
                txtTimeOfService.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidTimeOfService = false
            }
            else
            {
                Global.sharedInstance.isValidTimeOfService = true
            }
        }
            
        else if textField == maxConcurrentCustomers
        {
            if maxConcurrentCustomers.text == "" || maxConcurrentCustomers.text == NSLocalizedString("REQUIREFIELD", comment: "") || maxConcurrentCustomers.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                Global.sharedInstance.isValidMaxConcurrent = false
            }
            else
            {
                Global.sharedInstance.isValidMaxConcurrent = true
            }
            
            if maxConcurrentCustomers.text! != ""
            {
                if CInt(maxConcurrentCustomers.text!)! < 1  {
                    maxConcurrentCustomers.text = "1"
                }
            }
            
        }
        else if textField == txtTimeInterval
        {
            if txtTimeInterval.text == "" || txtTimeInterval.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeInterval.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                Global.sharedInstance.isValidTimeInterval = false
            }
            else
            {
                Global.sharedInstance.isValidTimeInterval = true
            }
        }
            
        else if textField == txtDiscount
        {
            if textField.text == ""
            {
                Global.sharedInstance.fDiscount = false
            }
            else if textField.text != "" && ((textField.textColor != UIColor.redColor() && Global.sharedInstance.fDiscount == false) || textField.text?.rangeOfString("%") == nil)
            {
                Global.sharedInstance.fDiscount = true
                if txtDiscount.text != "0%" {
                    textField.text = textField.text! + "%"
                }
            }
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch(textField){
        case(txtServiceName):
            txtPrice.becomeFirstResponder()
        case(txtPrice):
            txtTimeOfService.becomeFirstResponder()
        case(txtTimeOfService):
            maxConcurrentCustomers.becomeFirstResponder()
        case(maxConcurrentCustomers):
            txtTimeInterval.becomeFirstResponder()
        case(txtTimeInterval):
            //       txtMinConcurrentCustomers.becomeFirstResponder()
            //       case(txtMinConcurrentCustomers):
            txtDiscount.becomeFirstResponder()
        case(txtDiscount):
            txtDiscount.resignFirstResponder()
        default:
            txtServiceName.becomeFirstResponder()
        }
        return true
    }
    
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool
    {
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        if textField.text == NSLocalizedString("REQUIREFIELD", comment: "")
        {
            textField.textColor = UIColor.blackColor()
            textField.text = ""
        }
        else
        {
            if textField == txtTimeInterval
            {
                if startString.characters.count > 2
                {
                    showAlert(NSLocalizedString("ENTER_ONLY2_CHARACTERS", comment: ""))
                    return false
                }
                else
                {
                    return true
                }
            }
            else if textField == txtDiscount
            {
                if startString.rangeOfString("%") == nil
                {
                    Global.sharedInstance.fDiscount = false
                }
                if startString.characters.count > 2
                {
                    if string == ""
                    {
                        return true
                    }
                    else
                    {
                        showAlert(NSLocalizedString("ENTER_ONLY2_CHARACTERS", comment: ""))
                        return false
                    }
                }
                else
                {
                    return true
                }
                
            }
            else if textField == maxConcurrentCustomers
            {
                if startString.characters.count > 3
                {
                    showAlert(NSLocalizedString("ENTER_ONLY3_CHARACTERS", comment: ""))
                    return false
                }
                else
                {
                    return true
                }
            }
            else if textField == txtTimeOfService
            {
                if startString.characters.count > 3
                {
                    if startString.rangeOfString("-") == nil
                    {
                        showAlert(NSLocalizedString("ENTER_ONLY3_CHARACTERS", comment: ""))
                        return false
                    }
                        
                    else if startString.characters.count > (startString.startIndex.distanceTo(startString.rangeOfString("-")!.endIndex)) + 3
                    {
                        showAlert(NSLocalizedString("ENTER_ONLY3_CHARACTERS", comment: ""))
                        return false
                    }
                    else
                    {
                        return true
                    }
                }
                else
                {
                    return true
                }
            }
            else if textField == txtPrice
            {
                if startString.characters.count > 6
                {
                    if startString.rangeOfString("-") == nil
                    {
                        showAlert(NSLocalizedString("ENTER_ONLY6_CHARACTERS", comment: ""))
                        return false
                    }
                        
                    else if startString.characters.count > (startString.startIndex.distanceTo(startString.rangeOfString("-")!.endIndex)) + 6
                    {
                        showAlert(NSLocalizedString("ENTER_ONLY6_CHARACTERS", comment: ""))
                        return false
                    }
                    else
                    {
                        return true
                    }
                }
                else
                {
                    return true
                }
            }
            else if textField == txtServiceName
            {
                if startString.characters.count > 30
                {
                    showAlert(NSLocalizedString("ENTER_ONLY30_CHARACTERS", comment: ""))
                    return false
                }
                else
                {
                    return true
                }
            }
        }
        return true
    }
    
    //for service Name
    func isValidString(name:String)->Bool
    {
        var numSpace = 0
        for chr in name.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z")  && !(chr >= "א" && chr <= "ת") && !(chr >= "0" && chr <= "9") && chr != " " )
            {
                return false
            }
            if chr == " "
            {
                numSpace += 1
            }
        }
        if numSpace == name.characters.count || numSpace == name.characters.count - 1// אם יש רק רווחים או רק אות אחת והשאר רווחים
        {
            return false
        }
        return true
    }
    //for price
    func validationPrice(price:String) -> Bool
    {
        var numOfChar = 0
        var numOfPoints = 0
        fromPrice = 0.0
        untilPrice = 0.0
        for char in (price.characters)
        {
            
            if !(char >= "0" && char <= "9") && char != "-" && char != "."
            {
                return false
            }
            if char == "-"
            {
                numOfChar += 1
                
                if numOfChar > 1
                {
                    return false
                }
            }
            else if char == "."
            {
                numOfPoints += 1
                if numOfPoints > 2
                {
                    return false
                }
            }
            
        }
        // var arr:Array<String> = []
        if numOfChar == 1
        {
            let arr = price.componentsSeparatedByString("-")
            
            if arr.count != 2
            {
                return false
            }
            else if Float(arr[0]) > Float(arr[1]) || Float(arr[0]) > 999999 || Float(arr[1]) > 999999
            {
                return false
            }
            
            
            if arr[0].rangeOfString(".") != nil && arr[0].rangeOfString(".")!.count == 1
            {
                let range: Range<String.Index> = arr[0].rangeOfString(".")!
                let index: Int = arr[0].startIndex.distanceTo(range.startIndex)
                if index == 0 || index == arr[0].characters.count
                {
                    return false
                }
            }
            if arr[1].rangeOfString(".") != nil && arr[1].rangeOfString(".")!.count == 1
            {
                let range: Range<String.Index> = arr[1].rangeOfString(".")!
                let index: Int = arr[0].startIndex.distanceTo(range.startIndex)
                if index == 0 || index == (arr[1].characters.count - 1)
                {
                    return false
                }
            }
            
            fromPrice = Float(arr[0])!
            untilPrice = Float(arr[1])!
            
            return true
        }
        else//אין -
        {
            fromPrice = Float(txtPrice.text!)!
            return true
        }
        
    }
    //for time of service
    func validationTimeOfService(timeOfService:String) -> Bool
    {
        fromServiceTime = 0
        untilServiceTime = 0
        var numOfChar = 0
        for char in (timeOfService.characters)
        {
            
            if !(char >= "0" && char <= "9") && char != "-"
            {
                return false
            }
            if char == "-"
            {
                numOfChar += 1
                
                if numOfChar > 1
                {
                    return false
                }
            }
        }
        //var arr:Array<String> = []
        if numOfChar == 1
        {
            let arr = timeOfService.componentsSeparatedByString("-")
            
            if arr.count != 2
            {
                return false
            }
            else if Int(arr[0]) > Int(arr[1]) || Int(arr[0]) > 999 || Int(arr[1]) > 999
            {
                return false
            }
            fromServiceTime = Int(arr[0])!
            untilServiceTime = Int(arr[1])!
            return true
        }
        fromServiceTime = Int(txtTimeOfService.text!)!
        return true
        
    }
    
    @IBOutlet weak var viewOpenTable: UIView!
    
    @IBOutlet weak var btnOpenTbl: UIButton!
    @IBAction func btnOpenTbl(sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            tblSelectOption.hidden = false
            tblSelectOption.reloadData()
        }
        else{
            tblSelectOption.hidden = true
            sender.tag = 0
        }
    }
    func keyboardWillShow(note: NSNotification) {
        
        if Global.sharedInstance.isFirstOpenKeyBoard == false
        {
            Global.sharedInstance.isFirstCloseKeyBoard = false
            Global.sharedInstance.isFirstOpenKeyBoard = true
            
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                if delegateScroll != nil
                {
                    delegateScroll.scrollOnEdit(keyboardSize,textField: selectedTextField!)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didServicesClosed = true
                }
            }
        }
    }
    
    func keyboardWillHide(note: NSNotification)
    {
        if Global.sharedInstance.isFirstCloseKeyBoard == false
        {
            Global.sharedInstance.isFirstCloseKeyBoard = true
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                if delegateScroll != nil
                {
                    delegateScroll.scrollOnEndEdit(keyboardSize)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didServicesClosed = true
                }
                Global.sharedInstance.isFirstOpenKeyBoard = false
            }
        }
    }
    // remove the keyBoard events to prevent invoke the events from other cells
    func delKbNotifBusiness()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        Global.sharedInstance.didServicesClosed = true
    }
    
    func showAlert(mess:String)
    {
        let alertController = UIAlertController(title: "", message:
            mess, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.Default,handler: nil))
        Global.sharedInstance.GlobalDataVC!.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func  saveDataToWorker()->Bool{
        
        Global.sharedInstance.fIsSaveConBussinesServicesPressed = true
        Global.sharedInstance.selectedCellForEditService.append(false)
        
        if txtDiscount.text == "" || txtDiscount.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtDiscount.text == NSLocalizedString("NOT_VALID", comment: "")
        {
            //txtDiscount.textColor = UIColor.redColor()
            //txtDiscount.text = NSLocalizedString("REQUIREFIELD", comment: "")
            Global.sharedInstance.isValidDiscount = false
        }
        else
        {
            Global.sharedInstance.isValidDiscount = true
            
            if Global.sharedInstance.fDiscount == false
            {
                Global.sharedInstance.fDiscount = true
                if txtDiscount.text != "0%"
                {
                    txtDiscount.text = txtDiscount.text! + "%"
                }
            }
        }
        if lblInputServiceName.text == NSLocalizedString("SERVICE", comment: "")
        {
            if txtPrice.text == "" || txtPrice.text == NSLocalizedString("REQUIREFIELD", comment: "") || lblInputServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                //txtPrice.textColor = UIColor.redColor()
                // txtPrice.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else if validationPrice(txtPrice.text!) == false{
                 txtPrice.textColor = UIColor.redColor()
                 txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else
            {
                Global.sharedInstance.isValidPrice = true
            }
            
            if txtServiceName.text == "" || txtServiceName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                // txtServiceName.textColor = UIColor.redColor()
                //txtServiceName.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else if isValidString(txtServiceName.text!) == false
            {
                txtServiceName.textColor = UIColor.redColor()
                txtServiceName.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else
            {
                Global.sharedInstance.isValidServiceName = true
            }
            
            //            if txtTimeInterval.text == "" || txtTimeInterval.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeInterval.text == NSLocalizedString("NOT_VALID", comment: "")
            //            {
            //                txtTimeInterval.textColor = UIColor.redColor()
            //                txtTimeInterval.text = NSLocalizedString("REQUIREFIELD", comment: "")
            //                Global.sharedInstance.isValidTimeInterval = false
            //            }
            //            else
            //            {//מוסלש כי זה ירד משדה חובה בהתאם לאפיון
            Global.sharedInstance.isValidTimeInterval = true
            //}
            if txtTimeOfService.text == "" || txtTimeOfService.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtTimeOfService.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                //txtTimeOfService.textColor = UIColor.redColor()
                // txtTimeOfService.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidTimeOfService = false
            }
            else if validationTimeOfService(txtTimeOfService.text!) == false{
                 txtTimeOfService.textColor = UIColor.redColor()
                 txtTimeOfService.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidTimeOfService = false
            }
            else
            {
                Global.sharedInstance.isValidTimeOfService = true
            }
            
            if maxConcurrentCustomers.text == "" || maxConcurrentCustomers.text == NSLocalizedString("REQUIREFIELD", comment: "") || maxConcurrentCustomers.text == NSLocalizedString("NOT_VALID", comment: "")
                
            {
                // maxConcurrentCustomers.textColor = UIColor.redColor()
                // maxConcurrentCustomers.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidMaxConcurrent = false
            }
            else
            {
                Global.sharedInstance.isValidMaxConcurrent = true
            }
        }
        else{
            if txtPrice.text == "" || txtPrice.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtPrice.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                // txtPrice.textColor = UIColor.redColor()f
                // txtPrice.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
            else if validationPrice(txtPrice.text!) == false{
                 txtPrice.textColor = UIColor.redColor()
                 txtPrice.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidPrice = false
            }
                
            else
            {
                Global.sharedInstance.isValidPrice = true
            }
            
            if txtServiceName.text == "" || txtServiceName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtServiceName.text == NSLocalizedString("NOT_VALID", comment: "")
            {
                // txtServiceName.textColor = UIColor.redColor()
                //txtServiceName.text = NSLocalizedString("REQUIREFIELD", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
            else if isValidString(txtServiceName.text!) == false
            {
                txtServiceName.textColor = UIColor.redColor()
                txtServiceName.text = NSLocalizedString("NOT_VALID", comment: "")
                Global.sharedInstance.isValidServiceName = false
            }
                
            else
            {
                Global.sharedInstance.isValidServiceName = true
            }
            
        }
        
        if lblInputServiceName.text == NSLocalizedString("SERVICE", comment: ""){
            if Global.sharedInstance.isValidDiscount == true /*&&  Global.sharedInstance.isValidMinConcurrentCustomers == true*/ &&  Global.sharedInstance.isValidPrice == true &&  Global.sharedInstance.isValidServiceName == true &&  Global.sharedInstance.isValidTimeInterval == true &&  Global.sharedInstance.isValidTimeOfService == true &&  Global.sharedInstance.isValidMaxConcurrent == true
            {
                delegateSave.reloadTableForSave(self.tag,btnTag:3)
                
                let discount = Global.sharedInstance.cutStringBySpace(txtDiscount.text!, strToCutBy: "%")
                
                var objProviderServicess:objProviderServices = objProviderServices()
                
                
                
                objProviderServicess = objProviderServices(
                    _nvServiceName: txtServiceName.text!/*txtServiceName.text!*/,
                    _iPrice: fromPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _nUntilPrice: untilPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _iTimeOfService: fromServiceTime,//Global.sharedInstance.parseJsonToInt(txtTimeOfService.text!),
                    _iUntilSeviceTime: untilServiceTime,//Global.sharedInstance.parseJsonToInt(txtTimeOfService.text!),
                    //_iConcurrentCustomers: Int(.text!)!,
                    _iTimeInterval: Global.sharedInstance.parseJsonToInt(txtTimeInterval.text!),
                    _iMaxConcurrentCustomers: Global.sharedInstance.parseJsonToInt(maxConcurrentCustomers.text!),
                    _iDiscount: Global.sharedInstance.parseJsonToInt(discount[0]),
                    _iServiceType:90,_bDisplayPerCustomer:btnYesSelect.isCecked)
                //_iMinConcurrentCustomers: Global.sharedInstance.parseJsonToInt(txtMinConcurrentCustomers.text!),
                
                if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0 || txtPrice.tag == -11//פעם ראשונה
                {
                    Global.sharedInstance.generalDetails.arrObjProviderServices.append(objProviderServicess)
                }
                else{
                    Global.sharedInstance.generalDetails.arrObjProviderServices.removeAtIndex(txtPrice.tag)
                    Global.sharedInstance.generalDetails.arrObjProviderServices.insert(objProviderServicess, atIndex: txtPrice.tag)
                }
                Global.sharedInstance.fIsEmptyBussinesServices = false
                Global.sharedInstance.headersCellRequired[4] = true
                return true
            }
            else
            {
                //    Global.sharedInstance.headersCellRequired[4] = false
                Global.sharedInstance.fIsEmptyBussinesServices = true
                return false
            }
            
        }
        else{
            if Global.sharedInstance.isValidPrice == true &&  Global.sharedInstance.isValidServiceName == true {
                
                delegateSave.reloadTableForSave(self.tag,btnTag: 3)
                
                let discount = Global.sharedInstance.cutStringBySpace(txtDiscount.text!, strToCutBy: "%")
                
                var objProviderServicess:objProviderServices = objProviderServices()
                objProviderServicess = objProviderServices(
                    _nvServiceName: txtServiceName.text!,
                    _iPrice: fromPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _nUntilPrice: untilPrice,//Global.sharedInstance.parseJsonToFloat(txtPrice.text!),
                    _iTimeOfService: 0,
                    _iUntilSeviceTime: 0,
                    //_iConcurrentCustomers: Int(.text!)!,
                    _iTimeInterval: 0,
                    _iMaxConcurrentCustomers: 0,
                    _iDiscount: Global.sharedInstance.parseJsonToInt(discount[0]),
                    _iServiceType:89,_bDisplayPerCustomer:btnYesSelect.isCecked)
                //_iMinConcurrentCustomers: Global.sharedInstance.parseJsonToInt(txtMinConcurrentCustomers.text!),
                
                if Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0 || txtPrice.tag == -11//פעם ראשונה
                {
                    Global.sharedInstance.generalDetails.arrObjProviderServices.append(objProviderServicess)
                }
                else{
                    Global.sharedInstance.generalDetails.arrObjProviderServices.removeAtIndex(txtPrice.tag)
                    Global.sharedInstance.generalDetails.arrObjProviderServices.insert(objProviderServicess, atIndex: txtPrice.tag)
                }
                Global.sharedInstance.fIsEmptyBussinesServices = false
                Global.sharedInstance.headersCellRequired[4] = true
                return true
            }
            else
            {
                //  Global.sharedInstance.headersCellRequired[4] = false
                Global.sharedInstance.fIsEmptyBussinesServices = true
                return false
            }
            
        }
        return false
    }
    
    func openTbl()
    {
        if btnOpenTbl.tag == 0{
            btnOpenTbl.tag = 1
            tblSelectOption.hidden = false
            tblSelectOption.reloadData()
        }
        else{
            tblSelectOption.hidden = true
            btnOpenTbl.tag = 0
        }
    }
    
}
