//
//  OptionAppointmentCollectionViewCell.swift
//  Bthere
//
//  Created by User on 25.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class OptionAppointmentCollectionViewCell: UICollectionViewCell {
    var indexPath:Int = 0
    @IBOutlet weak var imgOption: UIImageView!
    var storyB = UIStoryboard(name: "SupplierExist", bundle: nil)
    
    @IBOutlet weak var btnOpenOption: UIButton!
    
    @IBAction func btnOpenOption(sender: AnyObject) {
        
        switch indexPath {
        case 1:
            let viewCon = storyB.instantiateViewControllerWithIdentifier("DetailsAppointmentViewController") as! DetailsAppointmentViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            Global.sharedInstance.appointmentsCostumers?.presentViewController(viewCon, animated: true, completion: nil)
            
            return
        case 2:
            
            return
                default:
            return
        }
        
    }
    
    @IBOutlet weak var lblDescription: UILabel!
    
    
    func setDisplayData(imgName:String,desc:String,index:Int)
    {
        //        btnOpenOption.layer.borderWidth = 1
        //        btnOpenOption.layer.borderColor = UIColor.blackColor().CGColor
        imgOption.image = UIImage(named: imgName)
        lblDescription.text = desc
        indexPath = index
        
    }

}
