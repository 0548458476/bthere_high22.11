//
//  lagnForClientTableViewCell.swift
//  Bthere
//
//  Created by User on 10.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class lagnForClientTableViewCell: UITableViewCell {

    var delegate:deleteMessageDelegate!=nil
    var delegateChangeLanguage:changeLanguageDelegate!=nil
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBut: UIView!
    var row:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let recognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LanguageTableViewCell.SelectLang))
//        recognizer.numberOfTapsRequired = 1
        viewLang.userInteractionEnabled = true
        viewLang.tag = 0
//        viewLang.addGestureRecognizer(recognizer)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBOutlet weak var lblLang: UILabel!
    func setDisplayData(lang:String,tag:Int){
        lblLang.text = lang
        if tag == 1
        {
            viewLang.backgroundColor = Colors.sharedInstance.color3
            lblLang.textColor = UIColor.whiteColor()
            viewLang.tag = 1
        }
        else
        {
            viewLang.backgroundColor = UIColor.clearColor()
            lblLang.textColor = UIColor.blackColor()
            viewLang.tag = 0
        }
    }
    
    func SelectLang(){
        if viewLang.tag == 0
        {
            viewLang.backgroundColor = Colors.sharedInstance.color3
            lblLang.textColor = UIColor.whiteColor()
            viewLang.tag = 1
            delegate.deleteMessage(self.row)
        }
        else
        {
            if viewLang.tag == 0
            {
            viewLang.backgroundColor = UIColor.clearColor()
            lblLang.textColor = UIColor.blackColor()
            viewLang.tag = 0
            }
        }
//         delegateChangeLanguage.changeLanguage(lblLang.text!)
    }

}
