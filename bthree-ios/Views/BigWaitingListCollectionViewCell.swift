//
//  BigWaitingListCollectionViewCell.swift
//  Bthere
//
//  Created by User on 26.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class BigWaitingListCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageLogo: UIImageView!
    
    @IBOutlet weak var viewLogo: UIView!
    
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var txtViewText: UITextView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    let border = CALayer()
    
    let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
    var index:Int = 0
    
    override func awakeFromNib() {
        
        border.backgroundColor = UIColor.blackColor().CGColor
        viewLogo.layer.borderWidth = 1
        viewLogo.layer.borderColor = Colors.sharedInstance.color1.CGColor
        //viewLogo.layer.addSublayer(border)
        txtViewText.editable = false
    }
    
    func setDisplayData(img:String,date:String,hour:String,text:String)
    {
        imageLogo.contentMode = .ScaleAspectFit
        
        //המרת התמונה של הלוגו מהשרת
        let dataDecoded:NSData = NSData(base64EncodedString: (
            img), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
        
        var decodedimage:UIImage = UIImage()
        if UIImage(data: dataDecoded) != nil
        {
            decodedimage = UIImage(data: dataDecoded)!
            
            UIGraphicsBeginImageContext(imageLogo.frame.size)
            decodedimage.drawInRect(imageLogo.bounds)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            imageLogo.image = image
        }
        else
        {
            imageLogo.image = UIImage()
        }

        lblDate.text = date
        lblHour.text = hour
        txtViewText.text = text
    }
}
