//
//  CancelAppointmentViewController.swift
//  Bthere
//
//  Created by User on 24.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

//ספק קיים:ביטול תור
class CancelAppointmentViewController: UIViewController {

    //MARK: - Properties
    
    var dateTurn = ""
    var hourTurn = ""
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblHourTurn: UILabel!
    @IBOutlet weak var lblDateTurn: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBAction func btnClose(sender: AnyObject) {
         self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblHourTurn.text = hourTurn
        lblDateTurn.text = dateTurn
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
