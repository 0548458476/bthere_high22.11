
//
//  dayMonthCalendarSupplierCollectionViewCell.swift
//  Bthere
//
//  Created by User on 22.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
//cell of day in month in supplier design
class dayMonthCalendarSupplierCollectionViewCell: UICollectionViewCell {
        var dateFormatter = NSDateFormatter()
        @IBOutlet var lblDayDesc: UILabel!
        let calendar = NSCalendar.currentCalendar()
        var dayToday:Int = 0
        var monthToday:Int = 0
        var yearToday:Int = 0
        var isShabat:Bool = Bool()
        var delegate:clickToDayDelegate!
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        
        @IBOutlet weak var viewIsFree: UIView!
        @IBOutlet var imgToday: UIImageView!
        var dateDay:NSDate = NSDate()
        @IBOutlet var btnEnterToDay: UIButton!
        
        
        //func cell 
        func setDisplayData(DayInt:Int){
            
            btnEnterToDay.enabled = true
            btnEnterToDay.backgroundColor =  UIColor.clearColor()
            
            // להוסיף כפתור  בכל
            
            Global.sharedInstance.setEventsArray()
            let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
            componentsCurrent.day = DayInt
            let date:NSDate = Calendar.sharedInstance.from(componentsCurrent.year , month: componentsCurrent.month, day: DayInt)
            
            let date1 = NSCalendar.currentCalendar().dateFromComponents(componentsCurrent)!

            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let formattedDateString = formatter.stringFromDate(date1)
       
            formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
            let date2:NSDate = formatter.dateFromString(formattedDateString)!
            var st = ""
            var isFound = 0

//            }//הועתק מיומן תורים אם לא צריך את זה ביומן ספק או משהו ברעיון של זה צריך למחק
            if isFound == 0
            {
                self.viewIsFree.backgroundColor = UIColor.clearColor()//check
            }
            else{
                isFound = 0
            }
            
            dateDay = date2
            
            let components = calendar.components([.Day, .Month, .Year], fromDate: date)
            dayToday = components.day
            
            //check if date is passed
            if small(date, rhs: NSDate())
            {
                
                if !self.isShabat
                {
                    lblDayDesc.textColor = UIColor.grayColor()
                }
                
                btnEnterToDay.enabled = false
                
            }
            
            
            if Global.sharedInstance.arrEventsCurrentMonth.count > 0 && Global.sharedInstance.isSyncWithGoogelCalendarSupplier == true
            {
                
                for var item in Global.sharedInstance.arrEventsCurrentMonth
                {
                    let event = item 
                    
              
                    
                    let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
                    
          
                    let dayEvent = componentsEvent.day
                    if small(date, rhs: NSDate())
                        
                    {
                        lblDayDesc.textColor = UIColor.grayColor()

                        btnEnterToDay.enabled = false
                    }
                    
                    if dayEvent == dayToday
                    {
                        
                        
                        let underlineAttributedString = NSMutableAttributedString(string:String(DayInt))
                    
                        let range = (String(DayInt) as NSString).rangeOfString(String(DayInt))
                        underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName , value:NSUnderlineStyle.StyleSingle.rawValue, range: range)
                        if small(date, rhs: NSDate())
                        {
                            underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.grayColor() , range: range)
                            
                            btnEnterToDay.enabled = false
                            
                        }
                                              lblDayDesc.attributedText = underlineAttributedString
                        break
                    }
                    else{
                        if small(date, rhs: NSDate())
                        {
                            
                            lblDayDesc.textColor = UIColor.grayColor()
                            
                            btnEnterToDay.enabled = false
                            
                        }
                        lblDayDesc.text = String(DayInt)
                    }
                    
                }
            }
            else{
                lblDayDesc.text = String(DayInt)
            }
            
            
            
            var str:String = ""
            dateFormatter.dateFormat = "dd/MM/yyyy"
            var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
            var s2 = dateFormatter.stringFromDate(NSDate()).componentsSeparatedByString("/")
            if s1[1] == s2[1] && s2[2] == s1[2]{
                let characters = s1[0].characters.map { String($0) }
                if characters[0] == String(0){
                    if DayInt == Int(characters[1]){
                        imgToday.hidden = false
                        
                    }
                    else if DayInt < Int(characters[1]){
                        lblDayDesc.alpha = 0.4
                    }}
                else if DayInt == Int(s1[0]){
                    imgToday.hidden = false
                }
                else if DayInt < Int(s1[0]){
                    if DayInt % 7 == 0
                    {
                        
                        lblDayDesc.alpha = 1
                        
                    }
                    else{
                        lblDayDesc.alpha = 0.5
                    }
                }
            }
            if isShabat
            {
                if self.small(date, rhs: NSDate())
                {
                    lblDayDesc.alpha = 0.5
                                 }
            }
            if self.small(date, rhs: NSDate())
            {
                lblDayDesc.alpha = 0.5
            }

        }
    /// if click on day
        @IBAction func btnEnterToDayClick(sender: AnyObject) {
                  Global.sharedInstance.dateDayClick = dateDay
            Global.sharedInstance.currDateSelected = dateDay
            Global.sharedInstance.isShowClickDate =  true
            delegate.clickToDay()
        }
        
        func setNull(){
            lblDayDesc.text = ""
        }
    
    // func that check if date is passed
        internal func small(lhs: NSDate, rhs: NSDate) -> Bool {
            let calendar:NSCalendar = NSCalendar.currentCalendar()
            
            let isToday:Bool = calendar.isDateInToday(lhs);
            if isToday
            {
                return false
            }
            else
            {
                return lhs.compare(rhs) == .OrderedAscending
            }
        }
        override func layoutSubviews() {
            self.addSubview(viewIsFree)
            self.sendSubviewToBack(viewIsFree)
        }
        
   

    
}
