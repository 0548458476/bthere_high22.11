//
//  RowInAppointmentCollectionViewCell.swift
//  Bthere
//
//  Created by User on 25.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class RowInAppointmentCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblHour: UILabel!
    
    
    func setDisplayData(hourDesc:String,desc:String){
        lblDesc.text = desc
        lblHour.text = hourDesc
    }

}
