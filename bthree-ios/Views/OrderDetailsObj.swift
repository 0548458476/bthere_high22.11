//
//  OrderDetailsObj.swift
//  Bthere
//
//  Created by User on 22.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class OrderDetailsObj: NSObject {
    
//    int iCoordinatedServiceId
//    int iProviderUserId
//    string nvFirstName
//    string nvSupplierName
//    //ProviderServiceDetailsObj[] objProviderServiceDetails
//    int iDayInWeek
//    string dtDateOrder
//    string nvFromHour
//    string nvAddress

    var iCoordinatedServiceId:Int = 0
    var iProviderUserId:Int = 0
    var nvFirstName:String = ""
    var nvSupplierName:String = ""
    var objProviderServiceDetails:Array<ProviderServiceDetailsObj> = []
    var iDayInWeek:Int = 0

    var dtDateOrder:NSDate = NSDate()//תאריך הארוע

    var nvFromHour:String = ""
    var nvToHour:String = ""
    var nvAddress:String = ""
    var objProviderServiceDetail = ProviderServiceDetailsObj()
    var nvComment:String = ""
    var nvLogo:String = ""
    var iUserId:Int = 0
    
    override init()
    {
        iCoordinatedServiceId = 0
        iProviderUserId = 0
        nvFirstName = ""
        nvSupplierName = ""

        iDayInWeek = 0
        dtDateOrder = NSDate()
        nvFromHour = ""
        nvToHour = ""
        nvAddress = ""
        objProviderServiceDetails = []
        nvComment = ""
        nvLogo = ""
        iUserId = 0
        
    }
    
    init(_iCoordinatedServiceId:Int,_iProviderUserId:Int,_nvFirstName:String,_nvSupplierName:String,_objProviderServiceDetails:Array<ProviderServiceDetailsObj>,_iDayInWeek:Int,_dtDateOrder:NSDate,_nvFromHour:String,_nvToHour:String,_nvAddress:String,_nvComment:String, _nvLogo:String, _iUserId:Int) {
      iCoordinatedServiceId = _iCoordinatedServiceId
        iProviderUserId = _iProviderUserId
        nvFirstName = _nvFirstName
        nvSupplierName = _nvSupplierName
        objProviderServiceDetails = _objProviderServiceDetails
        iDayInWeek = _iDayInWeek
        dtDateOrder = _dtDateOrder
        nvFromHour = _nvFromHour
        nvToHour = _nvToHour
        nvAddress = _nvAddress
        nvComment = _nvComment
        nvLogo = _nvLogo
        iUserId = _iUserId
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iCoordinatedServiceId"] = iCoordinatedServiceId
        dic["iProviderUserId"] = iProviderUserId
        dic["nvFirstName"] = nvFirstName
        dic["nvSupplierName"] = nvSupplierName
        dic["dtDateOrder"] = Global.sharedInstance.convertNSDateToString(self.dtDateOrder)
        dic["objProviderServiceDetails"] = objProviderServiceDetails
        dic["iDayInWeek"] = iDayInWeek
        dic["nvFromHour"] = nvFromHour
        dic["nvToHour"] = nvToHour
        dic["nvAddress"] = nvAddress
        dic["nvComment"] = nvComment
        dic["nvLogo"] = nvLogo
        dic["iUserId"] = iUserId
        return dic
    }
    
    func getFromDic(dic:Dictionary<String,AnyObject>) -> OrderDetailsObj {
        
        let orderDetailsObj:OrderDetailsObj = OrderDetailsObj()
        
        orderDetailsObj.iCoordinatedServiceId = Global.sharedInstance.parseJsonToInt(dic["iCoordinatedServiceId"]!)
        
        orderDetailsObj.iProviderUserId = Global.sharedInstance.parseJsonToInt(dic["iProviderUserId"]!)
        
        orderDetailsObj.nvFirstName = Global.sharedInstance.parseJsonToString(dic["nvFirstName"]!)
        
        orderDetailsObj.nvSupplierName = Global.sharedInstance.parseJsonToString(dic["nvSupplierName"]!)
        
        orderDetailsObj.iDayInWeek = Global.sharedInstance.parseJsonToInt(dic["iDayInWeek"]!)
        
        orderDetailsObj.nvFromHour = Global.sharedInstance.parseJsonToString(dic["nvFromHour"]!)
        
        orderDetailsObj.nvToHour = Global.sharedInstance.parseJsonToString(dic["nvToHour"]!)
        
        orderDetailsObj.nvAddress = Global.sharedInstance.parseJsonToString(dic["nvAddress"]!)
        let dt:NSDate = Global.sharedInstance.getStringFromDateString(dic["dtDateOrder"] as! String)
        orderDetailsObj.dtDateOrder = dt
        
        
        orderDetailsObj.nvComment = Global.sharedInstance.parseJsonToString(dic["nvComment"]!)
        
        orderDetailsObj.nvLogo = Global.sharedInstance.parseJsonToString(dic["nvLogo"]!)
        
        orderDetailsObj.iUserId = Global.sharedInstance.parseJsonToInt(dic["iUserId"]!)
        
        var arr = NSArray()
        
        arr = dic["objProviderServiceDetails"] as! NSArray
        
        orderDetailsObj.objProviderServiceDetails = objProviderServiceDetail.ProviderServiceDetailsObjToArrayGet(arr as! Array<Dictionary<String, AnyObject>>)//שתי האוביקטים שמוסלשים בגלל שלא ידעתי איך לאתחל אותם ואני לא בטוחה שצריך את זה
        return orderDetailsObj
        
    }
    
    func OrderDetailsObjToArrayGet(arrDic:Array<Dictionary<String,AnyObject>>)->Array<OrderDetailsObj>{

        //formatter.locale = NSLocale(localeIdentifier: "US_en")
        //formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
        
        var arrOrderDetailsObj:Array<OrderDetailsObj> = Array<OrderDetailsObj>()
        var orderDetailsObj:OrderDetailsObj = OrderDetailsObj()
        
        for i in 0  ..< arrDic.count
        {
            orderDetailsObj = getFromDic(arrDic[i])
            arrOrderDetailsObj.append(orderDetailsObj)
            
        }
        return arrOrderDetailsObj
    }

    func convertNSDateToString(dateTOConvert:NSDate)-> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //let dateStr = dateFormatter.stringFromDate(dateTOConvert)
        
        var myDateString = String(Int64(dateTOConvert.timeIntervalSince1970 * 1000))
        myDateString = "/Date(\(myDateString))/"
        
        
        return myDateString
    }

}
