//
//  SmallInServiceCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 14.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class SmallInServiceCollectionViewCell: UICollectionViewCell {
 
    var delegate:showMoreInfoDelegate!=nil
    var myServiceName:String = ""
    
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var btnOpen: UIButton!
   
    var index:Int = 0
    var pressedButton:Int =  0
    var delegateReloadTbl:reloadTblServiceDelegte!=nil
    
    @IBAction func btnOpen(sender: AnyObject) {
        
        Global.sharedInstance.serviceName = ""
        
        if pressedButton == 1//מידע נוסף
        {
            delegate.showMoreInfo(index)
        }
        else if pressedButton == 2//בחירה מרובה
        {
            if self.tag == 0//לא בחור
            {
                if !Global.sharedInstance.arrayServicesKodsToServer.contains(Global.sharedInstance.arrayServicesKods[index])
                {
                  Global.sharedInstance.arrayServicesKodsToServer.append(Global.sharedInstance.arrayServicesKods[index])
                }
                
                if !Global.sharedInstance.multipleServiceName.contains(Global.sharedInstance.arrayServicesNames[index])
                {
                     Global.sharedInstance.multipleServiceName.append(Global.sharedInstance.arrayServicesNames[index])
                }
                
                Global.sharedInstance.numCellsCellected += 1
               
                Global.sharedInstance.indexCellSelected = index
               
                self.tag = 1
                Global.sharedInstance.isFirstCellSelected = false
                delegateReloadTbl.reloadTblService()
            }
            else//בחור-נהפך ללא בחור
            {
                if Global.sharedInstance.arrayServicesKodsToServer.contains(Global.sharedInstance.arrayServicesKods[index])
                {
                    let indexs = Global.sharedInstance.arrayServicesKodsToServer.indexOf(Global.sharedInstance.arrayServicesKods[index])
                    Global.sharedInstance.arrayServicesKodsToServer.removeAtIndex(indexs!)
                }
                
                if Global.sharedInstance.multipleServiceName.contains(Global.sharedInstance.arrayServicesNames[index])
                {
                    let indexs = Global.sharedInstance.multipleServiceName.indexOf(Global.sharedInstance.arrayServicesNames[index])
                    Global.sharedInstance.multipleServiceName.removeAtIndex(indexs!)
                }
                
                if Global.sharedInstance.numCellsCellected != 0
                {
                    Global.sharedInstance.numCellsCellected -= 1
                }
                self.tag = 0
                Global.sharedInstance.isFirstCellSelected = false
                if Global.sharedInstance.numCellsCellected == 0
                {
                    Global.sharedInstance.indexCellSelected = -1
                }
                delegateReloadTbl.reloadTblService()
            }
        }
        else if pressedButton == 3 && self.lblText.text != ""//הזמן
        {
            if Global.sharedInstance.multipleServiceName.count != 0//מבחירה מרובה
            {
                Global.sharedInstance.serviceName = ""
                for item in Global.sharedInstance.multipleServiceName
                {
                    if Global.sharedInstance.serviceName == ""
                    {
                        Global.sharedInstance.serviceName = item
                    }
                    else
                    {
                        Global.sharedInstance.serviceName = "\(Global.sharedInstance.serviceName), \(item)"
                    }
                }
            }
            else
            {
                Global.sharedInstance.serviceName = myServiceName//בחירה רגילה
            }
            
            Global.sharedInstance.indexRowForIdService = index
            if !Global.sharedInstance.arrayServicesKodsToServer.contains(Global.sharedInstance.arrayServicesKods[index])
            {
                Global.sharedInstance.arrayServicesKodsToServer.append(Global.sharedInstance.arrayServicesKods[index])
            }
            
            delegate.showOdrerTurn()
        }
    }
    
    func setDisplayData(image:String,text:String,index:Int,serviceName:String)
    {
        myServiceName = serviceName
        btnOpen.layer.borderColor = UIColor.blackColor().CGColor
        btnOpen.layer.borderWidth = 1
        pressedButton = index
        
        img.image = UIImage(named: image)
        lblText.text = text
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            lblText.font = UIFont(name: "OpenSansHebrew-Light", size: 12)
        }
        
    }

}
