//
//  smallGiveMyServicesCollectionViewCell.swift
//  Bthere
//
//  Created by User on 25.8.2016.
//  Copyright © 2016 Webit. All rights reserved.

import UIKit

class smallGiveMyServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var btnOpen: UIButton!
    
    var ProviderServicesArray:Array<objProviderServices> =  Array<objProviderServices>()
    var indexRow:Int = 0//הסקשין של השורה
    var coll:UICollectionView?
    var pressedButton:Int =  0
    var delegateDeleteItem:deleteItemDelegate!=nil
    var delegateCloseCollection:closeCollectionDelegate!=nil
    
    @IBAction func btnOpen(sender: AnyObject) {
        
        if pressedButton == 1//מחק
        {
            // Create the alert controller
            let alertController = UIAlertController(title: NSLocalizedString("MESSAGE_TITLE", comment: ""), message: NSLocalizedString("DELETE_GIVE_SERVICE", comment: ""), preferredStyle: .Alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.Default) {
                UIAlertAction in
                self.delegateCloseCollection.closeCollection(self.coll!)
               
                Global.sharedInstance.isDeletedGiveMyService = false
                
                //מחיקת הנותן שרות מהמערך
                self.delegateDeleteItem.deleteItem(self.indexRow)
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
             Global.sharedInstance.giveMyServices!.presentViewController(alertController, animated: true, completion: nil)
        }
        else if pressedButton == 2//נווט
        {
             self.navigateToLatitude(31.771959, longitude: 35.217018)
        }
        else if pressedButton == 3//הזמן
        {
            Global.sharedInstance.providerID = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
            
            getProviderServicesForSupplierFunc()
        }
    }
    
    
    func getProviderServicesForSupplierFunc()
    {
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = Global.sharedInstance.providerID
        var arrUsers:Array<User> = Array<User>()
        
        api.sharedInstance.getProviderServicesForSupplier(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                var Arr:NSArray = NSArray()
                
                Arr = responseObject["Result"] as! NSArray
                let ps:objProviderServices = objProviderServices()
                
                self.ProviderServicesArray = ps.objProviderServicesToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                if self.ProviderServicesArray.count == 0
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
                }
                else
                {
                    Global.sharedInstance.viewCon!.ProviderServicesArray = self.ProviderServicesArray
                    Global.sharedInstance.viewCon!.indexRow = self.indexRow
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let frontviewcontroller:UINavigationController = UINavigationController()
                    frontviewcontroller.pushViewController(Global.sharedInstance.viewCon!, animated: false)
                    
                    //initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                    
                }
            }
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                
        })
    }
    
    func setDisplayData(image:String,text:String,index:Int)
    {
        btnOpen.layer.borderColor = UIColor.blackColor().CGColor
        btnOpen.layer.borderWidth = 1
        pressedButton = index
        
        img.image = UIImage(named: image)
        lblText.text = text
        
    }
    
    
    func navigateToLatitude(latitude:Double,longitude:Double){
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "waze://")!) {
            //Waze is installed. Launch Waze and start navigation
            let urlStr:NSString = NSString(format: "waze://?ll=%f,%f&navigate=yes", latitude, longitude)
            UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
        } else {
            //Waze is not installed. Launch AppStore to install Waze app
            UIApplication.sharedApplication().openURL(NSURL(string: "http://itunes.apple.com/us/app/id323229106")!)
        }
    }
    
}
