//
//  ServiceInListTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/7/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol editServiceDelegate{
    func reloadTableForEditService(tag:Int,my:ServiceInListTableViewCell)
}

class ServiceInListTableViewCell: UITableViewCell {
    var delegate:editServiceDelegate! = nil
    var isOpen:Bool = false
    var isEdit:Int = 0
    @IBOutlet var lblDescService: UILabel!
    
    @IBOutlet weak var viewEditClicked: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let border = CALayer()
        
        let language = NSBundle.mainBundle().preferredLocalizations.first! as NSString
        if language == "he"
        {
            border.frame = CGRectMake(CGRectGetWidth(btnEditService.layer.frame) + 16 , 0, 1, CGRectGetHeight(btnEditService.layer.frame) + 16)
        }
        else
        {
            border.frame = CGRectMake(0, 0, 1, CGRectGetHeight(btnEditService.layer.frame) + 16)
        }
        border.backgroundColor = UIColor.whiteColor().CGColor
        
       // btnEditService.layer.addSublayer(border)

    }

    @IBAction func btnEditService(sender: UIButton) {
        self.isEdit = 1
        delegate.reloadTableForEditService(self.tag,my: self)
    }
    override func layoutSubviews() {
        let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(lineView)
    }
    @IBOutlet var btnEditService: UIButton!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDispalyData(str:String){
        lblDescService.text = str
    }
    

    @IBAction func btnDeleteService(sender: UIButton) {
        let alert = UIAlertController(title: "", message:
            NSLocalizedString("DELETE_SERVICE", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
        self.isEdit = 0
        Global.sharedInstance.generalDetails.arrObjProviderServices.removeAtIndex(self.tag)
        if  Global.sharedInstance.generalDetails.arrObjProviderServices.count == 0{
            Global.sharedInstance.isFromFirstSave = false
            
            self.delegate.reloadTableForEditService(self.tag,my: self)
        }
        else{
            self.isEdit = 2
           self.delegate.reloadTableForEditService(self.tag,my: self) 
        }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        Global.sharedInstance.globalData.presentViewController(alert, animated: true, completion: nil)

    }

}
