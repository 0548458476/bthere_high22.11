//
//  ListServicesTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/7/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol reloadServicesTableDelegate{
    func reloadServicesTable(tbl:UITableView)
}
class ListServicesTableViewCell: UITableViewCell {
    var delegate:reloadServicesTableDelegate! = nil
    @IBOutlet var tblServices: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     tblServices.separatorStyle = .None
        // Configure the view for the selected state
    }
    
    func setDisplayData(){
        delegate.reloadServicesTable(tblServices)
    }

}
