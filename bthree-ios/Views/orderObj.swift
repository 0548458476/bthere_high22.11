//
//  orderObj.swift
//  Bthere
//
//  Created by User on 10.8.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class OrderObj: NSObject {

    
    var iSupplierId:Int = 0/*ספק*/
    var iUserId:Int = 0/*לקוח*/
   
    var iSupplierUserId:Array<Int> = []/*נותן השרות*/
    var iProviderServiceId:Array<Int> = []/*סוג שירות*/
    var dtDateOrder:NSDate = NSDate()
    var nvFromHour:String = ""
    var nvComment:String = ""
    
    override init() {
        iSupplierId = 0
        iUserId = 0
        dtDateOrder = NSDate()
        nvFromHour = ""
        nvComment = ""
        iSupplierUserId = []
        iProviderServiceId = []
      
    }
    
    init(_iSupplierId:Int,_iUserId:Int,_iSupplierUserId:Array<Int>,_iProviderServiceId:Array<Int>,_dtDateOrder:NSDate,_nvFromHour:String,_nvComment:String) {
      iSupplierId = _iSupplierId
       iUserId = _iUserId
       iSupplierUserId = _iSupplierUserId
        iProviderServiceId = _iProviderServiceId
        dtDateOrder = _dtDateOrder
      nvFromHour = _nvFromHour
        nvComment = _nvComment
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iSupplierId"] = iSupplierId
        dic["iUserId"] = iUserId
        dic["iSupplierUserId"] = iSupplierUserId
        dic["iProviderServiceId"] = iProviderServiceId
        dic["dtDateOrder"] = Global.sharedInstance.convertNSDateToString(self.dtDateOrder)        
        dic["nvFromHour"] = nvFromHour
        dic["nvComment"] = nvComment
        return dic
    }
    
    //לשים לב לפני השימוש בפונקציה שהשמשתנים שחוזרים מהשרת הם בדיוק באותם שמות
    func getOrderDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iSupplierId"] = self.iSupplierId
        dic["iUserId"] = self.iUserId
        dic["iSupplierUserId"] = self.iSupplierUserId
        dic["iProviderServiceId"] = self.iProviderServiceId
        dic["dtDateOrder"] = Global.sharedInstance.convertNSDateToString(self.dtDateOrder)
        dic["nvFromHour"] = self.nvFromHour
        dic["nvComment"] = self.nvComment
        return dic
    }

}
