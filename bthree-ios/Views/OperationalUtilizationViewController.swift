//
//  OperationalUtilizationViewController.swift
//  Bthere
//
//  Created by User on 18.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
// דוח ניצול גרפים  
class OperationalUtilizationViewController: NavigationModelViewController,LineChartDelegate{
    var label = UILabel()
    var lineChart: LineChart!
    
    
    //MARK: - Outlet
    
    
    @IBOutlet weak var viewBack: UIView!
    
    @IBOutlet weak var viewMiddle: UIView!
    
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var viewFromDate: UIView!
    
    @IBOutlet weak var lblFromDate: UILabel!
    
    @IBOutlet weak var viewToDate: UIView!
    
    @IBOutlet weak var lblToDate: UILabel!
    
    
    @IBOutlet weak var dpFromTo: UIDatePicker!
    
   //MARK: - Initial
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapBack: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OperationalUtilizationViewController.goBack))
        viewBack.addGestureRecognizer(tapBack)
       
        dpFromTo.setValue(Colors.sharedInstance.color1, forKeyPath: "textColor")
        dpFromTo.backgroundColor = UIColor.whiteColor()
        dpFromTo.datePickerMode = .Date
        
        
        dpFromTo.addTarget(self, action: #selector(OperationalUtilizationViewController.dataPickerChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        dpFromTo.hidden = true
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg-pic-supplier@x1.jpg")!)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissDp))
        view.addGestureRecognizer(tap)
        
        let tapOpenDpFrom:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openDpFrom))
        let tapOpenDpTo:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openDpTo))
        viewFromDate.addGestureRecognizer(tapOpenDpFrom)
        viewToDate.addGestureRecognizer(tapOpenDpTo)
        
        // var views: [String: AnyObject] = [:]
        
        //        label.text = "..."
        //        label.translatesAutoresizingMaskIntoConstraints = false
        //        label.textAlignment = NSTextAlignment.Center
        //        self.view.addSubview(label)
        //        views["label"] = label
        
        //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[label]-|", options: [], metrics: nil, views: views))
        //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-80-[label]", options: [], metrics: nil, views: views))
        
        //array of dots in the axis
        let data: [CGFloat] = [0, 120, 80, 260, 140, 320, 210, 500, 440]
//        let data: [CGFloat] = [0, 10,-30,-40]
        
        // simple line with custom x axis labels
        let xLabels: [String] = ["x", "x", "x", "x", "x", "x", "x", "x", "x", "x"]

        lineChart = LineChart()
        lineChart.animation.enabled = true
        lineChart.area = true
        
        lineChart.x.labels.visible = true
        lineChart.y.labels.visible = true
        
        lineChart.x.grid.count = 8
        lineChart.y.grid.count = 11
        
        lineChart.x.labels.values = xLabels
      
        lineChart.dots.visible = false
        lineChart.addLine(data)
        
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.delegate = self
        self.view.addSubview(lineChart)
        
        //views["chart"] = lineChart
        //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[chart]-|", options: [], metrics: nil, views: views))
        //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[label]-[chart(==200)]", options: [], metrics: nil, views: views))
        
        let horizontalConstraint = NSLayoutConstraint(item: lineChart, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: viewMiddle, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        view.addConstraint(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint(item: lineChart, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: viewMiddle, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
        view.addConstraint(verticalConstraint)
        
        let widthConstraint = NSLayoutConstraint(item: lineChart, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: viewMiddle, attribute: NSLayoutAttribute.Width, multiplier: 0.84, constant: 0)
        view.addConstraint(widthConstraint)
        
        let heightConstraint = NSLayoutConstraint(item: lineChart, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: viewMiddle, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0)
        view.addConstraint(heightConstraint)
        
        
        //        var delta: Int64 = 4 * Int64(NSEC_PER_SEC)
        //        var time = dispatch_time(DISPATCH_TIME_NOW, delta)
        //
        //        dispatch_after(time, dispatch_get_main_queue(), {
        //            self.lineChart.clear()
        //            self.lineChart.addLine(data2)
        //        });
        
        //        var scale = LinearScale(domain: [0, 100], range: [0.0, 100.0])
        //        var linear = scale.scale()
        //        var invert = scale.invert()
        //        println(linear(x: 2.5)) // 50
        //        println(invert(x: 50)) // 2.5
         self.view.bringSubviewToFront(dpFromTo)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * Line chart delegate method.
     */
    func didSelectDataPoint(x: CGFloat, yValues: Array<CGFloat>) {
        label.text = "x: \(x)     y: \(yValues)"
    }
    
    
    
    /**
     * Redraw chart on device rotation.
     */
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        if let chart = lineChart {
            chart.setNeedsDisplay()
        }
    }
    
    func dismissDp() {
       // view.endEditing(true)
        dpFromTo.hidden = true
    }
  
    //MARK: - Date Picker
    
    func openDpFrom() {
//        dpFromTo.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
//        dpFromTo.backgroundColor = Colors.sharedInstance.color1
        viewFromDate.tag = 1
        viewToDate.tag = 0
        dpFromTo.hidden = false
    }
    
    func openDpTo() {
//        dpFromTo.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
//        dpFromTo.backgroundColor = Colors.sharedInstance.color1
        viewFromDate.tag = 0
        viewToDate.tag = 1
        dpFromTo.hidden = false
    }
    
    func dataPickerChanged(datePicker:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        let strDate = dateFormatter.stringFromDate(datePicker.date)

        if viewFromDate.tag == 1
        {
            lblFromDate.text = strDate
            viewFromDate.tag = 0
        }
        else
        {
            lblToDate.text = strDate
            viewToDate.tag = 0
        }
        
    }
    
    func goBack() {
        
         let revealController: SWRevealViewController = self.revealViewController()
        
        let ReportsController: ReportViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ReportViewController")as! ReportViewController
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: ReportsController)
        revealController.pushFrontViewController(navigationController, animated: true)
        //revealController.revealToggleAnimated(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
