//
//  DayMonthCalendarCollectionViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/16/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
protocol clickToDayDelegate {
  func clickToDay()
}
class DayMonthCalendarCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Variables

    var dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    var dayToday:Int = 0
    var monthToday:Int = 0
    var yearToday:Int = 0
    var isShabat:Bool = Bool()
    var delegate:clickToDayDelegate!
    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
    var dateDay:NSDate = NSDate()
    var whichModelOpenMe:Int = 0// 2 = האם הגיעו מתצוגת חודש של יומן לקוח = 1 או של יומן ספק שהלקוח רואה
    var isSyncWithGoogle = true//שומר את הפלאג המתאים שלפיו צריך לשאול בין אם זה יומן לקוח או ספק שהלקוח רואה
    
    //MARK: - Outlet
    
    @IBOutlet var lblBtEvent: UILabel!
    @IBOutlet var lblIsBthereEvent: UILabel!
    @IBOutlet var lblDayDesc: UILabel!

    @IBOutlet weak var viewIsFree: UIView!
    @IBOutlet var imgToday: UIImageView!
    @IBOutlet var btnEnterToDay: UIButton!
// enter to day design by day selected on free time to orders
    @IBAction func btnEnterToDayClick(sender: AnyObject) {
        
        
        Global.sharedInstance.dateDayClick = dateDay
        Global.sharedInstance.currDateSelected = dateDay
        Global.sharedInstance.isShowClickDate =  true
        delegate.clickToDay()
    }
    
    //MARK: - Initial

    func setDisplayData(DayInt:Int){

        lblIsBthereEvent.backgroundColor = UIColor(patternImage: UIImage(named: "circle_orange")!)
        btnEnterToDay.enabled = true
        btnEnterToDay.backgroundColor =  UIColor.clearColor()
        
        // להוסיף כפתור  בכל
        
        Global.sharedInstance.setEventsArray()
        
        let componentsCurrent = calendar.components([.Day, .Month, .Year], fromDate: Calendar.sharedInstance.carrentDate)
        componentsCurrent.day = DayInt
        
        let date:NSDate = Calendar.sharedInstance.from(componentsCurrent.year , month: componentsCurrent.month, day: DayInt)
        
    
        let date1 = NSCalendar.currentCalendar().dateFromComponents(componentsCurrent)!
     
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let formattedDateString = formatter.stringFromDate(date1)

        formatter.timeZone = NSTimeZone(abbreviation: "GMT+0:00")
        let date2:NSDate = formatter.dateFromString(formattedDateString)!
        var st = ""
        var isFound = 0
// from supplier calander that client see design
        if Global.sharedInstance.model == 2
        {
            for var item in Global.sharedInstance.dateFreeDays{
                st = formatter.stringFromDate(item)
                
                if st == formattedDateString {
                    
                    self.viewIsFree.backgroundColor = Colors.sharedInstance.color4
                    isFound = 1
                    break
                }
            }
            
            if isFound == 0
            {
                self.viewIsFree.backgroundColor = UIColor.clearColor()            }
            else
            {
                isFound = 0
            }
        }
        
        dateDay = date2
        
        let components = calendar.components([.Day, .Month, .Year], fromDate: date)
        dayToday = components.day
        
        
        if small(date, rhs: NSDate())
        {

           // the date is passed
           btnEnterToDay.enabled = false
        }
        //ביומן ספק שהלקוח רואה, אם אין תור פנוי - לא לחיץ
        if whichModelOpenMe == 2 && self.viewIsFree.backgroundColor != Colors.sharedInstance.color4
        {
            btnEnterToDay.enabled = false
        }
        
        if whichModelOpenMe == 2 //== con//יומן ספק שהלקוח רואה
        {
            isSyncWithGoogle = Global.sharedInstance.isSyncWithGoogleCalendarAppointment
        }
        
        else if whichModelOpenMe == 1 //== con1//לקוח
        {
            isSyncWithGoogle = Global.sharedInstance.isSyncWithGoogelCalendar
        }
        
        
        if Global.sharedInstance.arrEventsCurrentMonth.count > 0 && isSyncWithGoogle == true
        {
            for var item in Global.sharedInstance.arrEventsCurrentMonth
            {
                
                let event = item 
                
                let componentsEvent = calendar.components([.Day, .Month, .Year], fromDate: event.startDate)
                
                let dayEvent = componentsEvent.day

// if has event in this day
                if dayEvent == dayToday
                {

                    let underlineAttributedString = NSMutableAttributedString(string:String(DayInt))
             
                    let range = (String(DayInt) as NSString).rangeOfString(String(DayInt))
                    underlineAttributedString.addAttribute(NSUnderlineStyleAttributeName , value:NSUnderlineStyle.StyleSingle.rawValue, range: range)
                    if small(date, rhs: NSDate())
                    {
                        underlineAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.grayColor() , range: range)
                        
                    }
           
                    lblDayDesc.attributedText = underlineAttributedString
                    break
                }
                else{

                    lblDayDesc.text = String(DayInt)
                }

            }
        }
        else{
            lblDayDesc.text = String(DayInt)
        }
        

        //var str:String = ""
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var s1 = dateFormatter.stringFromDate(Calendar.sharedInstance.carrentDate).componentsSeparatedByString("/")
        var s2 = dateFormatter.stringFromDate(NSDate()).componentsSeparatedByString("/")
        
        if s1[1] == s2[1] && s2[2] == s1[2]{
            let characters = s1[0].characters.map { String($0) }
            if characters[0] == String(0){
                if DayInt == Int(characters[1]){
                    imgToday.hidden = false
                    
                }
                else if DayInt < Int(characters[1]){
           
                }}
            else if DayInt == Int(s1[0]){
                imgToday.hidden = false
            }
            else if DayInt < Int(s1[0]){
                if DayInt % 7 == 0
                {

                }
                else{
                    lblDayDesc.alpha = 0.5
                }
            }
        }
        if isShabat
        {
            if self.small(date, rhs: NSDate())
            {
                lblDayDesc.alpha = 0.5
                
                btnEnterToDay.enabled = false
            }
        }
        if self.small(date, rhs: NSDate())
        {
            lblDayDesc.textColor
                = UIColor.grayColor()
            lblDayDesc.alpha = 0.5
        }
        if Global.sharedInstance.model == 1
        {
            btnEnterToDay.enabled = true
        }

    }

    override func layoutSubviews() {
        self.addSubview(viewIsFree)
        self.sendSubviewToBack(viewIsFree)
    }
    

    
    func setNull(){
        lblDayDesc.text = ""
    }
    // func that check if date passed
    internal func small(lhs: NSDate, rhs: NSDate) -> Bool {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        
        let isToday:Bool = calendar.isDateInToday(lhs);
        if isToday
        {
            return false
        }
        else
        {
        return lhs.compare(rhs) == .OrderedAscending
        }
    }



}
