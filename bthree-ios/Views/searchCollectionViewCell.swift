//
//  searchCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 7.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

protocol getProviderServicesForSupplierDelegate {
    func getProviderServicesForSupplierFunc()
}

import UIKit

//part of search result - נווט, לדף העסק, הזמן
class searchCollectionViewCell: UICollectionViewCell,getProviderServicesForSupplierDelegate {
    
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var btnOpen: UIButton!
      var ProviderServicesArray:Array<objProviderServices> =  Array<objProviderServices>()
    var indexRow:Int = 0
    var pressedButton:Int =  0
    var generic:Generic = Generic()
    
    //click on the cell (instead of didSelect)
    @IBAction func btnOpen(sender: AnyObject) {
        
        if pressedButton == 1//נווט
        {
            self.navigateToLatitude(31.771959, longitude: 35.217018)
        }
        else if pressedButton == 2//לדף העסק
        {
            generic.showNativeActivityIndicator(Global.sharedInstance.searchResult!)
            Global.sharedInstance.isFromSearchResults = true
            
            Global.sharedInstance.providerID = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
            
            var dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            
            dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­["iProviderId"] = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
            
            //קבלת נתונים לדף פרופיל העסק
            api.sharedInstance.GetProviderProfile(dic­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­GetProviderProfile­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                print(responseObject["Result"])
                
                let dic:Dictionary<String,AnyObject> =
                    responseObject["Result"] as! Dictionary<String,AnyObject>
                
                Global.sharedInstance.providerBuisnessProfile = Global.sharedInstance.providerBuisnessProfile.dicToAddProviderBuisnessProfile(dic)
                
                Global.sharedInstance.viewCon2?.buisnessName = Global.sharedInstance.dicResults[self.indexRow]["nvProviderName"]!.description
                
                Global.sharedInstance.viewCon2?.address = Global.sharedInstance.dicResults[self.indexRow]["nvAdress"]!.description
                
                Global.sharedInstance.viewCon2!.delegate = self
                Global.sharedInstance.searchResult?.presentViewController(Global.sharedInstance.viewCon2!, animated: true, completion: nil)
                self.generic.hideNativeActivityIndicator(Global.sharedInstance.searchResult!)
                
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                    self.generic.hideNativeActivityIndicator(Global.sharedInstance.searchResult!)
            })
            
        }
        else if pressedButton == 3//הזמן
        {
            
            Global.sharedInstance.providerID = Int(Global.sharedInstance.dicResults[indexRow]["iProviderId"]! as! NSNumber)
            
            getProviderServicesForSupplierFunc()
        }
        
    }    

  //func to get the provider free hours
    func getProviderServicesForSupplierFunc()
    {
        var dicSearch:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicSearch["iProviderId"] = Global.sharedInstance.providerID
        
        api.sharedInstance.getProviderServicesForSupplier(dicSearch, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
            
            if responseObject["Error"]!!["ErrorCode"] as! Int == -3
            {
                Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
            }
            else if responseObject["Error"]!!["ErrorCode"] as! Int == 1
            {
                var Arr:NSArray = NSArray()
                
                Arr = responseObject["Result"] as! NSArray
                let ps:objProviderServices = objProviderServices()
                
                self.ProviderServicesArray = ps.objProviderServicesToArrayGet(responseObject["Result"] as! Array<Dictionary<String,AnyObject>>)
                if self.ProviderServicesArray.count == 0
                {
                    Alert.sharedInstance.showAlertDelegate(NSLocalizedString("NO_SERVICES", comment: ""))
                }
                else
                {
                    Global.sharedInstance.viewCon!.ProviderServicesArray = self.ProviderServicesArray
                    Global.sharedInstance.viewCon!.indexRow = self.indexRow
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let frontviewcontroller:UINavigationController = UINavigationController()
                    frontviewcontroller.pushViewController(Global.sharedInstance.viewCon!, animated: false)
                    
                    //initialize REAR View Controller- it is the LEFT hand menu.
                    
                    let rearViewController = storyboard.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
                    
                    let mainRevealController = SWRevealViewController()
                    
                    mainRevealController.frontViewController = frontviewcontroller
                    mainRevealController.rearViewController = rearViewController
                    
                    let window :UIWindow = UIApplication.sharedApplication().keyWindow!
                    window.rootViewController = mainRevealController
                    
                }
            }
            },failure: {(AFHTTPRequestOperation, NSError) -> Void in
                
        })
        
    }

    func setDisplayData(image:String,text:String,index:Int)
    {
        btnOpen.layer.borderColor = UIColor.blackColor().CGColor
        btnOpen.layer.borderWidth = 1
        pressedButton = index
        
        img.image = UIImage(named: image)
        lblText.text = text
    }
    
//func to move to waze site - to navigate
    func navigateToLatitude(latitude:Double,longitude:Double){
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "waze://")!) {
            //Waze is installed. Launch Waze and start navigation
            let urlStr:NSString = NSString(format: "waze://?ll=%f,%f&navigate=yes", latitude, longitude)
            UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
        } else {
            //Waze is not installed. Launch AppStore to install Waze app
            UIApplication.sharedApplication().openURL(NSURL(string: "http://itunes.apple.com/us/app/id323229106")!)
        }
    }

}


