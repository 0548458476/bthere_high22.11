//
//  OptionsForCostumerCollectionViewCell.swift
//  bthree-ios
//
//  Created by User on 21.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol deleteItemInTableView{
    func deleteItem(indexPath:Int)
}
protocol closeCollectionDelegate{
    func closeCollection(collection:UICollectionView)
}

class OptionsForCostumerCollectionViewCell: UICollectionViewCell {
    var delegate:deleteItemInTableView!=nil
      var delegateCloseCollectionDelegate:closeCollectionDelegate!=nil
        var viewDelegate:MyCostumersViewController!
    
    @IBOutlet weak var viewLeft: UIView!
    var indexPath:Int = 0
    var secton:Int = 0
    var row:Int = 0
    var item:Int = 0
    var coll:UICollectionView?
    @IBOutlet weak var imgOption: UIImageView!
    var storyB = UIStoryboard(name: "SupplierExist", bundle: nil)
    
    @IBOutlet weak var btnOpenOption: UIButton!
    
    @IBAction func btnOpenOption(sender: AnyObject) {

        switch indexPath {
        
        case 1:
            let viewCon = storyB.instantiateViewControllerWithIdentifier("DeleteCustomerViewController") as! DeleteCustomerViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            viewCon.row = row
            viewCon.delegate = Global.sharedInstance.mycustomers
            viewCon.delegateCloseCollectionDelegate = Global.sharedInstance.mycustomers
            viewCon.coll = coll
          
            Global.sharedInstance.PresentViewMe?.presentViewController(viewCon, animated: true, completion: nil)
//            Global.sharedInstance.nameCostumersArray.removeAtIndex(indexPath)
            return
        case 2:
           
            return
        case 3:
            let viewCon = storyB.instantiateViewControllerWithIdentifier("CostumerAppointmentsViewController") as! CostumerAppointmentsViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            Global.sharedInstance.PresentViewMe?.presentViewController(viewCon, animated: true, completion: nil)
            return
        case 4:
            let viewCon = storyB.instantiateViewControllerWithIdentifier("DetailsCustomerViewController") as! DetailsCustomerViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            Global.sharedInstance.PresentViewMe?.presentViewController(viewCon, animated: true, completion: nil)
            return
        default:
            return
        }

    }
    
    @IBOutlet weak var lblDescription: UILabel!
    
    
    func setDisplayData(imgName:String,desc:String,index:Int)
    {
//        btnOpenOption.layer.borderWidth = 1
//        btnOpenOption.layer.borderColor = UIColor.blackColor().CGColor
        imgOption.image = UIImage(named: imgName)
        lblDescription.text = desc
        indexPath = index
        
    }
}
