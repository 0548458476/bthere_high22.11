//
//  ItemInSection3TableViewCell.swift
//  bthree-ios
//
//  Created by User on 24.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import GoogleMaps

protocol addRowForSection3Delegate{
    func addRow(tag:Int , row:Int)
}

protocol saveDataDelegate{
    func saveData()->Bool
}

protocol delKbNotificationDelegate {
    func delKbNotification()
}

protocol tableViewAddressDelegate {
    func tableViewAddress(lastContentOffset:CGFloat)
    
}
//הוספת עובדים
class ItemInSection3TableViewCell: UITableViewCell,UITextFieldDelegate,saveDataDelegate,delKbNotificationDelegate,saveDataToWorkerDelegate{
    
    //MARK: - Properties
    
    var delegate:addRowForSection3Delegate! = nil
    var delegateSave:reloadTableForSaveDelegate! = nil
    var delegateScroll:scrollOnEditDelegate!=nil
    var delegateKbBusiness:delKbNotifBusinessDelegate!=nil
    var delegateKbCalendar:delKbCalenderNotifDelegate!=nil
    var delegateZeroMaxMin:ZeroDPMaxMinDelegte!=nil
    var delegateGeneric:genericDelegate!=nil
    
    var sectionCell:Int = 0
    var contentOffset:CGFloat = 0
    var serviceProvidersForEdit = objServiceProvidersForEdit()
    var flag_FirstName:Bool = false
    var flag_LastName:Bool = false
    var flag_Phone:Bool = false
    var flag_PhoneInternet:Bool = true
    var flag_Email:Bool = false
    var flag_EmailInternet:Bool = true
    var flag_Date:Bool = false
    var indexCell:Int = 0
    var isSetData = false//כדי למנוע הצגת שדות לא תקינים במקרה שפותח את עריכה לאחר שרוצה להכניס עובד נוסף שאינו תקין
    var selfEmailExist = ""// שמירת המייל הנוכחי של הבנ״א בשביל עריכה לפני שמתחיל לערוך כדי שאם ימחק וירצה להכניס שוב את המייל יתן לו להכניס
    var selfPhoneExist = ""//כנ״ל רק לגבי טלפון
    
    var arrGoodPhone:Array<Character> = Array<Character>()
    
    var generic:Generic = Generic()
    
    var selectedTextField:UITextField?
   
    //MARK: - Outlet
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doTheHoursEqualTo: UILabel!
    @IBOutlet weak var doYouWantOpenPersonalCalendar: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblHoursWork: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnCheckBoxNo: checkBoxForDetailsWorker!
    @IBOutlet weak var btnCheckBox: CheckBoxForDetailsWorker2!
    
    @IBAction func btnCheckBox(sender: AnyObject) {
        
        btnEqalHours.enabled = true
        btnEqalHoursNo.enabled = true
        btnCheckBoxNo.isCecked = false
    }
    @IBAction func btnCheckBoxNo(sender: AnyObject) {
        
        if btnEqalHoursNo.isCecked == true
        {
            if Global.sharedInstance.isOpenHoursForPlus == true{
                self.tag = -60
                Global.sharedInstance.isOpenHoursForPlusAction = false
                Global.sharedInstance.fromEdit = true
                delegate.addRow(self.tag, row: self.indexCell)
            }
            else if Global.sharedInstance.fromEdit == false{
                self.tag = -50
                delegate.addRow(self.tag, row: self.indexCell)
                
                
            }
            else{
                self.tag = -90
                Global.sharedInstance.clickNoCalendar = true
                delegate.addRow(self.tag, row: self.indexCell)
            }
        }
        
        
        btnEqalHoursNo.isCecked = false
        btnEqalHours.enabled = false
        btnEqalHoursNo.enabled = false
        //btnCheckBox.isCecked = false
        btnEqalHours.isCecked = false
        
    }
    @IBAction func btnEqual(sender: CheckBoxForDetailsWorker2)
    {
        Global.sharedInstance.isClickNoEqual = false
        if Global.sharedInstance.addRecess == true//נבחרו הפסקות
        {
            Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
        }
        Global.sharedInstance.addRecess = false
        
        if btnEqalHoursNo.isCecked == true
        {
            btnEqalHoursNo.isCecked = false
            sender.isCecked = true
            if Global.sharedInstance.isOpenHoursForPlus == true{
                self.tag = -60//לחיצה על וי בששעות פעילות זהות בכפתור הוסף עובד
                Global.sharedInstance.isOpenHoursForPlusAction = false
                Global.sharedInstance.fromEdit = true
                delegate.addRow(self.tag, row: self.indexCell)
            }
            else  if Global.sharedInstance.fromEdit == true{
                self.tag = -80//לחיצה על וי בשעות פעילות בעריכה
                Global.sharedInstance.isOpenHoursForNewWorker =  false
                Global.sharedInstance.isClickEqual = true
                delegate.addRow(self.tag, row: self.indexCell)
                
            }
            else{
                self.tag = -50
                delegate.addRow(self.tag, row: self.indexCell)
            }
        }
    }
    
    @IBAction func btnNoEqwal(sender: checkBoxForDetailsWorker) {
       
        btnEqalHours.isCecked = false
        if Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays != nil
        {
            Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
        }
    }
    @IBOutlet weak var btnEqalHoursNo: checkBoxForDetailsWorker!
    @IBOutlet weak var btnEqalHours: CheckBoxForDetailsWorker2!
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtMail: UITextField!
    
    @IBOutlet weak var conView: UIView!
    
    @IBAction func btnSave(sender: UIButton) {
        
        delegateSave.reloadTableForSave(self.tag,btnTag: 1)
    }
    
    @IBAction func checkNo(sender: CheckBox)
    {
        Global.sharedInstance.onOpenTimeOpenHours = true
        Global.sharedInstance.hoursForWorker = true
        
        if sender.isCecked == false
        {
            if Global.sharedInstance.isOpenHoursForPlus == true
            {
                Global.sharedInstance.hoursForWorkerFromPlus = true
                Global.sharedInstance.isOpenHoursForPlusAction = true
            }
            
            if Global.sharedInstance.fromEdit == false ||  Global.sharedInstance.isOpenHoursForPlusAction == true
            {
                delegate.addRow(self.tag, row: self.indexCell)//לחיצה על איקס בשעות פעילות שונות להוסף עובד
            }
            else if Global.sharedInstance.fromEdit == false
            {
                Global.sharedInstance.isOpenHoursForNewWorker = false
               
                Global.sharedInstance.fromEdit = false
                self.tag = -70
                delegate.addRow(self.tag, row: self.indexCell)
                
            }
            if Global.sharedInstance.fromEdit == true
            {
                self.tag = -70//לחיצה על איקס בשעות פעילות בעריכה
                Global.sharedInstance.hoursForWorkerFromEdit = true
                Global.sharedInstance.isOpenHoursForNewWorker =  true
                delegate.addRow(self.tag, row: self.indexCell)
                btnEqalHours.isCecked = false
                btnEqalHoursNo.isCecked = true
                Global.sharedInstance.itemInSection3TableViewCell?.delegateZeroMaxMin = Global.sharedInstance.hoursActive
                if Global.sharedInstance.itemInSection3TableViewCell?.delegateZeroMaxMin != nil
                {
                    Global.sharedInstance.itemInSection3TableViewCell?.delegateZeroMaxMin.ZeroDPMaxMin()
                }
                
            }
            btnEqalHours.isCecked = false
            sender.isCecked = true
        }
    }
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblFirstName.text = NSLocalizedString("FIRST_NAME", comment: "")
        lblLastName.text = NSLocalizedString("LAST_NAME", comment: "")
        lblEmail.text = NSLocalizedString("MAIL_DP", comment: "")
        lblPhone.text = NSLocalizedString("PHONE_USER", comment: "")
        lblHoursWork.text = NSLocalizedString("HOURS_ACTIVE", comment: "")
        doTheHoursEqualTo.text = NSLocalizedString("", comment: "")
        
        isSetData = false
        selfPhoneExist = ""
        selfEmailExist = ""
        
        Global.sharedInstance.GlobalDataVC!.delegateSaveWorker = self
        txtPhone.delegate = self
        btnEqalHours.enabled = false
        //        btnEqalHoursNo.enabled = false
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            doTheHoursEqualTo.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblFirstName.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblLastName.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblPhone.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblEmail.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblHoursWork.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
        }
        else if DeviceType.IS_IPHONE_6 ||  DeviceType.IS_IPHONE_6P
        {
            doTheHoursEqualTo.font = UIFont(name: "OpenSansHebrew-Light", size: 17)
        }
        Global.sharedInstance.saveTableViewCell?.delegateSaveData = self
        
        Global.sharedInstance.itemInSection2TableViewCell?.saveData = self
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtPhone.delegate = self
        txtMail.delegate = self
        
        txtPhone.textColor = UIColor.blackColor()
        txtMail.textColor = UIColor.blackColor()
    }
    
    func setDisplayData(user:objUsers,bsameW:Bool){
        
        isSetData = true
        
        Global.sharedInstance.isVclicedForWorkerForEdit = bsameW
        
        txtFirstName.text = user.nvFirstName
        txtLastName.text = user.nvLastName
        txtPhone.text = user.nvPhone
        txtMail.text = user.nvMail
        
        selfPhoneExist = txtPhone.text!
        selfEmailExist = txtMail.text!
        
        txtMail.textColor = UIColor.blackColor()
        txtPhone.textColor = UIColor.blackColor()
        //אם לחץ וי ביומן אישי ווי בשעות זהות
        if user.iUserStatusType == 25 && bsameW == true && Global.sharedInstance.isClickNoEqual == false && Global.sharedInstance.isClickEqual == false && Global.sharedInstance.clickNoCalendar == false
        {
            btnEqalHoursNo.isCecked = false
            btnEqalHours.isCecked = true
            btnEqalHours.enabled = true
            btnEqalHoursNo.enabled = true
        }
            
            //אם לחץ וי ביומן אישי ואיקס בשעות זהות
        else if user.iUserStatusType == 25 && bsameW == false && Global.sharedInstance.isClickNoEqual == false && Global.sharedInstance.isClickEqual == false && Global.sharedInstance.clickNoCalendar == false
        {
            btnEqalHoursNo.isCecked = true
            btnEqalHours.isCecked = false
            btnEqalHours.enabled = true
            btnEqalHoursNo.enabled = true
        }
            
            //אם לחץ איקס ביומן אישי
        else if  (Global.sharedInstance.isClickNoEqual == false || Global.sharedInstance.clickNoCalendar == true) && Global.sharedInstance.isClickEqual == false
        {
            btnEqalHoursNo.isCecked = false
            btnEqalHours.isCecked = false
            btnEqalHours.enabled = false
            btnEqalHoursNo.enabled = false
        }
        else if Global.sharedInstance.isClickNoEqual == true && Global.sharedInstance.clickNoCalendar == false
        {
            btnEqalHoursNo.isCecked = true
            btnEqalHours.enabled = true
        }
        else if  Global.sharedInstance.isClickEqual == true && Global.sharedInstance.clickNoCalendar == false
        {
            btnEqalHoursNo.isCecked = false
            btnEqalHours.isCecked = true
            btnEqalHours.enabled = true
            Global.sharedInstance.isClickEqual = false
            
        }
        if Global.sharedInstance.clickNoCalendar == true{
            Global.sharedInstance.clickNoCalendar = false
        }
    }
    
    func setdisplayData()
    {
        txtFirstName.text = txtFirstName.text
        txtLastName.text = txtLastName.text
        txtPhone.text = txtPhone.text
        txtMail.text = txtMail.text
    }
    
    func setDisplayDataNull()
    {
        Global.sharedInstance.isOpenWorker = true
        
        txtFirstName.text = ""
        txtLastName.text = ""
        txtPhone.text = ""
        txtMail.text = ""
        //האם השעות זהות לשעות פעילות העסק
        btnEqalHours.isCecked = true    //√
        btnEqalHoursNo.isCecked = false  //x
        btnEqalHoursNo.enabled = true
        btnEqalHours.enabled = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Delegate function
    
    func saveData()->Bool
    {
        var userStatus:Int?
        var bsameWH:Bool?
        var arrObjWorkingHourss:Array<objWorkingHours>?
        var objUserss:objUsers?
        
            userStatus = 25
            
            if btnEqalHours.isCecked
            {
                bsameWH = true
                arrObjWorkingHourss = []
            }
            else
            {
                bsameWH = false
                saveHoursToWorker()
                arrObjWorkingHourss = Global.sharedInstance.serviceProvider.arrObjWorkingHours
            }
        
        objUserss = objUsers(
            _nvUserName: txtMail.text!,
            _nvFirstName: txtFirstName.text!,
            _nvLastName: txtLastName.text!,
            _dBirthdate: NSDate(),
            _nvMail: txtMail.text!,
            _nvAdress: "",//txtAddress.text!,
            _iCityType: 1,
            _nvPhone: txtPhone.text!,
            _nvPassword: "",
            _nvVerification: "",
            _bAutomaticUpdateApproval: false,
            _bDataDownloadApproval: false,
            _bTermOfUseApproval: false,
            _bAdvertisingApproval: false,
            _iUserStatusType: userStatus!,
            _bIsGoogleCalendarSync: false,
            _iCreatedByUserId: Global.sharedInstance.currentProvider.iUserId,
            _iLastModifyUserId: Global.sharedInstance.currentProvider.iUserId,
            _iSysRowStatus: 1)
        
        Global.sharedInstance.serviceProvider.objsers = objUserss!
        Global.sharedInstance.serviceProvider = objServiceProviders(
            _objsers: objUserss!,
            _arrObjWorkingHours: arrObjWorkingHourss!,
            _bSameWH: bsameWH!)
        
        serviceProvidersForEdit = objServiceProvidersForEdit()
        serviceProvidersForEdit = objServiceProvidersForEdit(
            _objsers: objUserss!,
            _arrObjWorkingHours: Global.sharedInstance.serviceProviderForEdit.arrObjWorkingHours,
            _arrObjWorkingRest: Global.sharedInstance.serviceProviderForEdit.arrObjWorkingRest,
            _isHoursSelected: Global.sharedInstance.isHoursSelectedChild,
            _isHoursSelectedRest: Global.sharedInstance.isHoursSelectedRestChild,
            _isSelectAllHours: Global.sharedInstance.isSelectAllHoursChild,
            _isSelectAllRecess: Global.sharedInstance.isSelectAllRestChild,
            _bSameWH: bsameWH!)
        
        //-----------כדי שבעת עריכה לא יהיה כפולים
        //אם מעריכה
        if Global.sharedInstance.isServiceProviderEditOpen == true
        {
            Global.sharedInstance.isFromEdit = true
            
            if checkValidity() == true && Global.sharedInstance.isAllFlagsFalse == false
            {
                if Global.sharedInstance.generalDetails.arrObjServiceProviders.count > 0
                {
                    Global.sharedInstance.generalDetails.arrObjServiceProviders.removeAtIndex(Global.sharedInstance.indexForArr)
                    
                    Global.sharedInstance.arrObjServiceProvidersForEdit.removeAtIndex(Global.sharedInstance.indexForArr)//בשביל עריכה
                }
                
                Global.sharedInstance.generalDetails.arrObjServiceProviders.insert(Global.sharedInstance.serviceProvider, atIndex:Global.sharedInstance.indexForArr)
                
                Global.sharedInstance.arrObjServiceProvidersForEdit.insert(serviceProvidersForEdit, atIndex:Global.sharedInstance.indexForArr)
                Global.sharedInstance.isValidWorkerDetails = true
                
                Global.sharedInstance.arrWorkHoursChild = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
                
                Global.sharedInstance.serviceProviderForEdit = objServiceProvidersForEdit()
                
                Global.sharedInstance.serviceProvider = objServiceProviders()
                serviceProvidersForEdit = objServiceProvidersForEdit()
                
                return true
            }
            if Global.sharedInstance.isAllFlagsFalse == true
            {
                Global.sharedInstance.isAllFlagsFalse = false
            }
        }
        else//מהוספת עובד חדש או בפעם הראשונה
        {
            if checkValidity() == true && Global.sharedInstance.isAllFlagsFalse == false
            {
                Global.sharedInstance.generalDetails.arrObjServiceProviders.append(Global.sharedInstance.serviceProvider)//בשביל השליחה לשרת
                
                Global.sharedInstance.arrObjServiceProvidersForEdit.append(serviceProvidersForEdit)//בשביל עריכה
                
                Global.sharedInstance.isValidWorkerDetails = true
                
                Global.sharedInstance.arrWorkHoursChild =  [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
                
                Global.sharedInstance.serviceProviderForEdit = objServiceProvidersForEdit()
                
                Global.sharedInstance.serviceProvider = objServiceProviders()
                serviceProvidersForEdit = objServiceProvidersForEdit()
                
                return true
            }
            
            if Global.sharedInstance.isAllFlagsFalse == true
            {
                Global.sharedInstance.arrWorkHoursChild =  [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
                
                Global.sharedInstance.serviceProvider = objServiceProviders()
                
                return false
            }
        }
        Global.sharedInstance.isValidWorkerDetails = false
        
        Global.sharedInstance.arrWorkHoursChild =  [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
        
        Global.sharedInstance.serviceProvider = objServiceProviders()
        
        return false
    }
    
    //MARK: - TextField
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        Global.sharedInstance.txtAdressItem3 = false
        
        if delegateKbBusiness != nil
        {
            // remove the keyBoard event from businessServices cell
            delegateKbBusiness.delKbNotifBusiness()
        }
        
        if delegateKbCalendar != nil
        {
            delegateKbCalendar.delKbCalenderNotif()
        }
        
        if Global.sharedInstance.didSection3Closed == true
        {
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(ItemInSection3TableViewCell.keyboardWillShow(_:)),name: UIKeyboardWillShowNotification,object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(ItemInSection3TableViewCell.keyboardWillHide(_:)),name: UIKeyboardWillHideNotification,object: nil)
            Global.sharedInstance.didSection3Closed = false
        }
        
        selectedTextField = textField
        //כדי שלא ימחקו הנתונים תמיד

        
        if textField.text == NSLocalizedString("REQUIREFIELD", comment: "") || textField.text == NSLocalizedString("NOT_VALID", comment: "")  || textField.text == NSLocalizedString("MAIL_EXIST", comment: "") || textField.text == NSLocalizedString("PHONE_EXIST", comment: "")
        {
            textField.text = ""
        }

        textField.textColor = UIColor.blackColor()
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == txtFirstName
        {
            if txtFirstName.text != "" && isValidName(txtFirstName.text!) == false
            {
                self.txtFirstName.textColor = UIColor.redColor()
                self.txtFirstName.text = NSLocalizedString("NOT_VALID", comment: "")
            }
        }
        else if textField == txtLastName
        {
            if txtLastName.text != "" && isValidName(txtLastName.text!) == false
            {
                self.txtLastName.textColor = UIColor.redColor()
                self.txtLastName.text = NSLocalizedString("NOT_VALID", comment: "")
            }
        }
        else if textField == txtPhone
        {
            if txtPhone.text != ""
            {
                isPhoneExist()
            }
            if txtPhone.text != "" && (isValidPhone(txtPhone.text!) == false || txtPhone.text?.characters.count < 10)
            {
                self.txtPhone.textColor = UIColor.redColor()
                self.txtPhone.text = NSLocalizedString("NOT_VALID", comment: "")
            }
        }
        else if textField  == txtMail
        {
            if txtMail.text != ""
            {
                isEmailExist()
            }
            if txtMail.text != "" && isValidEmail(txtMail.text!) == false
            {
                self.txtMail.textColor = UIColor.redColor()
                txtMail.text = NSLocalizedString("NOT_VALID", comment: "")
            }
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch(textField){
        case(txtFirstName):
            txtLastName.becomeFirstResponder()
        case(txtLastName):
            txtPhone.becomeFirstResponder()
        case(txtPhone):
            txtMail.becomeFirstResponder()
        case(txtMail):
            txtMail.resignFirstResponder()
        default:
            txtFirstName.becomeFirstResponder()
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        
        if textField == txtPhone
        {
            if startString.characters.count > 10
            {
                Alert.sharedInstance.showAlert(NSLocalizedString("ENTER_TEN_DIGITS", comment: ""), vc: Global.sharedInstance.globalData)
                return false
            }
            else
            {
                return true
            }
        }
        
        
        return true
    }
    
    func dismissKeboard()
    {
        if txtMail.isFirstResponder() {
            txtMail.resignFirstResponder()
        }
        if txtPhone.isFirstResponder(){
            txtPhone.resignFirstResponder()
        }
        if txtFirstName.isFirstResponder() {
            txtFirstName.resignFirstResponder()
        }
        if txtLastName.isFirstResponder(){
            txtLastName.resignFirstResponder()
        }
    }
    
    //MARK: - Validation
    
    func checkValidity() -> Bool
    {
        dismissKeboard()
        
        if self.txtFirstName.text == "" || txtFirstName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtFirstName.text == NSLocalizedString("NOT_VALID", comment: "")
        {
            self.flag_FirstName = false
        }
            
        else if txtFirstName.text!.characters.count == 1 || isValidName(txtFirstName.text!) == false ///שם פרטי
        {
            self.txtFirstName.text = ""
            self.flag_FirstName = false
        }
        else
        {
            self.flag_FirstName = true
        }
        
        if self.txtLastName.text == "" || txtLastName.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtLastName.text == NSLocalizedString("NOT_VALID", comment: "")
        {
            self.flag_LastName = false
            
        }
        else if txtLastName.text!.characters.count == 1 || isValidName(txtLastName.text!) == false//שם משפחה
        {
            self.txtLastName.text = ""
            self.flag_LastName = false
        }
        else
        {
            self.flag_LastName = true
        }
        
        if self.txtMail.text == "" || txtMail.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtMail.text == NSLocalizedString("NOT_VALID", comment: "") || txtMail.text == NSLocalizedString("MAIL_EXIST", comment: "")
        {
            self.flag_Email = false
        }
            
        else if isValidEmail(txtMail.text!) == false
        {
            self.flag_Email = false
        }
        else
        {
            if flag_EmailInternet == false
            {
                self.flag_Email = false
            }
            else
            {
                self.flag_Email = true
            }
        }
        
        if self.txtPhone.text == "" || txtPhone.text == NSLocalizedString("REQUIREFIELD", comment: "") || txtPhone.text == NSLocalizedString("NOT_VALID", comment: "") || txtPhone.text == NSLocalizedString("PHONE_EXIST", comment: "")
        {
            self.flag_Phone = false
        }
            
        else if isValidPhone(txtPhone.text!) == false || txtPhone.text?.characters.count < 10
        {
            self.flag_Phone = false
        }
        else
        {
            if flag_PhoneInternet == false
            {
                self.flag_Phone = false
            }
            else
            {
                self.flag_Phone = true
            }
        }
        
        if flag_Email == true && flag_Phone == true && flag_FirstName == true && flag_LastName == true
        {
            Global.sharedInstance.isAllFlagsFalse = false
            return true
        }
        
        if txtMail.text == NSLocalizedString("REQUIREFIELD", comment: "") && txtPhone.text == NSLocalizedString("REQUIREFIELD", comment: "") && txtFirstName.text == NSLocalizedString("REQUIREFIELD", comment: "") && txtLastName.text == NSLocalizedString("REQUIREFIELD", comment: "")
            && Global.sharedInstance.isFromEdit == false && Global.sharedInstance.isOpenHours == false
        {
            Global.sharedInstance.isAllFlagsFalse = true
            
            flag_Email = true
            flag_Phone = true
            flag_FirstName = true
            flag_LastName = true
            
            txtFirstName.text = ""
            txtFirstName.textColor = UIColor.blackColor()
            
            txtMail.text = ""
            txtMail.textColor = UIColor.blackColor()
            
            txtPhone.text = ""
            txtPhone.textColor = UIColor.blackColor()
            
            txtLastName.text = ""
            txtLastName.textColor = UIColor.blackColor()
            
            return true
        }
        Global.sharedInstance.isAllFlagsFalse = false
        
        return false
    }
    
    //for first and last name
    func isValidName(input: String) -> Bool {
        
        var numSpace = 0
        for chr in input.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z")  && !(chr >= "א" && chr <= "ת") && !(chr == " "))  {
                return false
            }
            if chr == " "
            {
                numSpace += 1
            }
        }
        if numSpace == input.characters.count || numSpace == input.characters.count - 1// אם יש רק רווחים או רק אות אחת והשאר רווחים
        {
            return false
        }
        return true
    }
    
    //for phone and fax
    func isValidPhone(input: String) -> Bool {
        let index0 = input.startIndex.advancedBy(0)
        let index1 = input.startIndex.advancedBy(1)
        if input.characters[index0] != "0" || input.characters[index1] != "5"{
            return false
        }
        
        for chr in input.characters {
            if (!(chr >= "0" && chr <= "9") && !(chr == "-"))  {
                return false
            }
        }
        return true
    }
    
    //בדיקה האם קיים כבר נותן שרות או ספק לפי מס' הטלפון
    func isPhoneExist()
    {
        var dicPhone:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicPhone["nvPhone"] = txtPhone.text
        
        delegateGeneric.showGeneric()
        if Reachability.isConnectedToNetwork() == false
        {
            delegateGeneric.hideGeneric()
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
            self.flag_EmailInternet = false
        }
        else
        {
            api.sharedInstance.CheckProviderExistByPhone(dicPhone, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.flag_EmailInternet = true
                
                if self.isSetData == false
                {
                    if responseObject["Result"] as! Int != 0 //קיים
                    {
                        self.flag_Phone = false
                        self.txtPhone.textColor = UIColor.redColor()
                        self.txtPhone.text = NSLocalizedString("PHONE_EXIST", comment: "")
                    }
                    for servPro in Global.sharedInstance.generalDetails.arrObjServiceProviders
                    {
                        if servPro.objsers.nvPhone == self.txtPhone.text//טלפון קיים
                        {
                            if Global.sharedInstance.isServiceProviderEditOpen == true//מעריכה
                            {
                                if self.selfPhoneExist != self.txtPhone.text//לא אותו טלפון שהכניס קודם
                                {
                                    self.flag_Phone = false
                                    self.txtPhone.textColor = UIColor.redColor()
                                    self.txtPhone.text = NSLocalizedString("PHONE_EXIST", comment: "")
                                    break
                                }
                            }
                            else
                            {
                                self.flag_Phone = false
                                self.txtPhone.textColor = UIColor.redColor()
                                self.txtPhone.text = NSLocalizedString("PHONE_EXIST", comment: "")
                                break
                            }
                        }
                    }
                }
                self.isSetData = false
                self.delegateGeneric.hideGeneric()
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
            })
        }
    }
    
    //בדיקה האם קיים כבר נותן שרות או ספק לפי מייל
    func isEmailExist()
    {
        var dicEmail:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicEmail["nvMail"] = txtMail.text
        
        delegateGeneric.showGeneric()
        
        if Reachability.isConnectedToNetwork() == false//if there is connection
        {
            delegateGeneric.hideGeneric()
            Alert.sharedInstance.showAlert(NSLocalizedString("NO_INTERNET", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
            self.flag_EmailInternet = false
        }
        else
        {
            api.sharedInstance.CheckProviderExistByMail(dicEmail, success:{ (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) -> Void in
                
                self.flag_EmailInternet = true
                
                if self.isSetData == false
                {
                    if responseObject["Result"] as! Int != 0 //קיים
                    {
                        self.flag_Email = false
                        self.txtMail.textColor = UIColor.redColor()
                        self.txtMail.text = NSLocalizedString("MAIL_EXIST", comment: "")
                    }
                    
                    for servPro in Global.sharedInstance.generalDetails.arrObjServiceProviders
                    {
                        if servPro.objsers.nvMail == self.txtMail.text//מייל הוכנס כבר
                        {
                            if Global.sharedInstance.isServiceProviderEditOpen == true//מעריכה
                            {
                                if self.selfEmailExist != self.txtMail.text//לא אותו מייל שהכניס קודם
                                {
                                    self.flag_Email = false
                                    self.txtMail.textColor = UIColor.redColor()
                                    self.txtMail.text = NSLocalizedString("MAIL_EXIST", comment: "")
                                    break
                                }
                            }
                            else
                            {
                                self.flag_Email = false
                                self.txtMail.textColor = UIColor.redColor()
                                self.txtMail.text = NSLocalizedString("MAIL_EXIST", comment: "")
                                break
                            }
                        }
                    }
                }
                self.isSetData = false
                self.delegateGeneric.hideGeneric()
                },failure: {(AFHTTPRequestOperation, NSError) -> Void in
            })
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
    }
    
    //for scrolling the table up, on begin editing textField, to show it above the keyBoard
    func keyboardWillShow(note: NSNotification) {
        
        if Global.sharedInstance.isFirstOpenKeyBoard == false
        {
            Global.sharedInstance.isFirstOpenKeyBoard = true
            
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                if delegateScroll != nil
                {
                    delegateScroll.scrollOnEdit(keyboardSize,textField: selectedTextField!)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didSection3Closed = true
                }
                Global.sharedInstance.isFirstCloseKeyBoard = false
            }
        }
    }
    //for scrolling the table down, after editing textField
    func keyboardWillHide(note: NSNotification) {
        
        if Global.sharedInstance.isFirstCloseKeyBoard == false
        {
            Global.sharedInstance.isFirstCloseKeyBoard = true
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                if delegateScroll != nil
                {
                    delegateScroll.scrollOnEndEdit(keyboardSize)
                }
                else
                {
                    // remove the keyBoard events - Notifications
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didSection3Closed = true
                }
                Global.sharedInstance.isFirstOpenKeyBoard = false
            }
        }
    }
    
    // remove the keyBoard events to prevent invoke the events from other cells
    func delKbNotification()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        Global.sharedInstance.didSection3Closed = true
    }
    
    ///שמירת נתוני העובד בעת סגירת הסל
    func saveDataToWorker() -> Bool//תמיד מחזיר true כדי שיסגור את הסל גם אם אינו תקין
    {
        if saveData() == true
        {
            //אתחול המשתנים של עובד
            resetPropertiesForNewWorkerHours()
            delegateSave.reloadTableForSave(2,btnTag: self.sectionCell)
            Global.sharedInstance.fisValidWorker = true
            return true
        }
        else
        {
            //אתחול המשתנים של עובד
            resetPropertiesForNewWorkerHours()

            //לוחץ על שמור והמשך בכפתור פלוס בלי למלאות כלום
            if Global.sharedInstance.isAllFlagsFalse == true && Global.sharedInstance.isOpenNewWorker == true {
                delegateSave.reloadTableForSave(-200,btnTag: -200)
            }
            //return false
            Global.sharedInstance.fisValidWorker = false
            return true
        }
    }
    
    //שמירת השעות לעובד
    func saveHoursToWorker()
    {
        var workingHours = objWorkingHours()
        Global.sharedInstance.serviceProviderForEdit = objServiceProvidersForEdit()

            //עובר על השעות של כל הימים בשבוע של העובד
            for i in 0 ..< Global.sharedInstance.arrWorkHoursChild.count {
                //יש הפסקות
                if Global.sharedInstance.isHoursSelectedRestChild[i]
                {
                    //======================שמירת השעות לעריכה
                    let workingHoursEdit = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType,
                                                           _nvFromHour: Global.sharedInstance.arrWorkHoursChild[i].nvFromHour,
                                                           _nvToHour: Global.sharedInstance.arrWorkHoursChild[i].nvToHour)
                    
                    if workingHoursEdit.iDayInWeekType != 0 && workingHoursEdit.nvFromHour != "" && workingHoursEdit.nvToHour != ""
                    {
                        
                        Global.sharedInstance.serviceProviderForEdit.arrObjWorkingHours[i] = workingHoursEdit//בשביל עריכה
                    }
                    //=========================שמירת ההפסקות לעריכה
                    let workingHoursEditRest = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHoursRestChild[i].iDayInWeekType,
                                                           _nvFromHour: Global.sharedInstance.arrWorkHoursRestChild[i].nvFromHour,
                                                           _nvToHour: Global.sharedInstance.arrWorkHoursRestChild[i].nvToHour)
                    
                    if workingHoursEditRest.iDayInWeekType != 0 && workingHoursEditRest.nvFromHour != "" && workingHoursEditRest.nvToHour != ""
                    {
                        Global.sharedInstance.serviceProviderForEdit.arrObjWorkingRest[i] = workingHoursEditRest//בשביל עריכה
                    }
//========================================
                    
                    workingHours = objWorkingHours(
                        _iDayInWeekType: Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType,
                        _nvFromHour: Global.sharedInstance.arrWorkHoursChild[i].nvFromHour,
                        _nvToHour: Global.sharedInstance.arrWorkHoursRestChild[i].nvFromHour)
                    
                    Global.sharedInstance.serviceProvider.arrObjWorkingHours.append(workingHours)
                    
                    //--------------
                    workingHours = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType,
                                                   _nvFromHour: Global.sharedInstance.arrWorkHoursRestChild[i].nvToHour,
                                                   _nvToHour: Global.sharedInstance.arrWorkHoursChild[i].nvToHour)
                    
                    Global.sharedInstance.serviceProvider.arrObjWorkingHours.append(workingHours)
                    
                }
                else //אין הפסקות
                {
                    workingHours = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType,
                        _nvFromHour: Global.sharedInstance.arrWorkHoursChild[i].nvFromHour,
                        _nvToHour: Global.sharedInstance.arrWorkHoursChild[i].nvToHour)
                    
                    if workingHours.iDayInWeekType != 0 && workingHours.nvFromHour != "" && workingHours.nvToHour != ""
                    {
                        Global.sharedInstance.serviceProvider.arrObjWorkingHours.append(workingHours)
                    }
                    let workingHoursEdit = objWorkingHours(_iDayInWeekType: Global.sharedInstance.arrWorkHoursChild[i].iDayInWeekType,
                        _nvFromHour: Global.sharedInstance.arrWorkHoursChild[i].nvFromHour,
                        _nvToHour: Global.sharedInstance.arrWorkHoursChild[i].nvToHour)
                    
                    if workingHoursEdit.iDayInWeekType != 0 && workingHoursEdit.nvFromHour != "" && workingHoursEdit.nvToHour != ""
                    {
                        Global.sharedInstance.serviceProviderForEdit.arrObjWorkingHours[i] = workingHoursEdit//בשביל עריכה
                    }  
                }
            }
    }
    
    //מאתחל משתנים של שעות פעילות לעובד
    func resetPropertiesForNewWorkerHours()
    {
        if Global.sharedInstance.addRecess == true//נבחרו הפסקות
        {
            Global.sharedInstance.GlobalDataVC!.delegateEnabledBtnDays.enabledTrueBtnDays()
        }
        Global.sharedInstance.isSelectAllRestChild = false
        Global.sharedInstance.isSelectAllHoursChild = false
        Global.sharedInstance.addRecess = false
        Global.sharedInstance.isHoursSelectedChild = [false,false,false,false,false,false,false]
        Global.sharedInstance.isHoursSelectedRestChild = [false,false,false,false,false,false,false]
        Global.sharedInstance.currentBtnDayTagChild = -1
        Global.sharedInstance.currentBtnDayTagRestChild = -1
        Global.sharedInstance.lastBtnDayTagChild = -1
        Global.sharedInstance.lastBtnDayTagRestChild = -1
        Global.sharedInstance.arrWorkHoursRestChild = [objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours(),objWorkingHours()]
    }
}
