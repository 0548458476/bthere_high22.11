//
//  NewEventTableViewCell.swift
//  Bthere
//
//  Created by User on 12.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

//קבע אירוע - הסל הראשון
class NewEventTableViewCell: UITableViewCell,UITextFieldDelegate {

    var delegate:editTextInCellDelegate!=nil
    @IBOutlet weak var txtFText: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtFText.delegate = self
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplatData(value:String)
    {
        //txtFText.placeholder = value
        txtFText.attributedPlaceholder = NSAttributedString(string:value, attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        delegate.editTextInCell(0, text: txtFText.text!)
    }

}
