//
//  RowInListCollectionViewCell.swift
//  Bthere
//
//  Created by User on 23.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
//ספק קיים:
class RowInListCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlet
    
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblHour: UILabel!
    
    //MARK: - Initial
    
    func setDisplayData(hourDesc:String,desc:String){
        lblDesc.text = desc
        lblHour.text = hourDesc
    }
}
