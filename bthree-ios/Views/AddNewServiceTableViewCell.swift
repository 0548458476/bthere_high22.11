//
//  AddNewServiceTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/7/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol reloadTableForNewServiceDelegate{
    func reloadTableForNewService(cell:AddNewServiceTableViewCell)
}
class AddNewServiceTableViewCell: UITableViewCell {
    var delegate:reloadTableForNewServiceDelegate! = nil
    var isOpen:Bool = false
    @IBAction func newService(sender: UIButton) {
        delegate.reloadTableForNewService(self)
    }
    @IBOutlet weak var lblAddServiceProduct: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblAddServiceProduct.text = NSLocalizedString("ADD_SERVICE_PRODUCT", comment: "")
    }

    override func layoutSubviews() {
        let lineView: UIView = UIView(frame: CGRectMake(0, self.contentView.frame.height - 1, self.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(lineView)

    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
