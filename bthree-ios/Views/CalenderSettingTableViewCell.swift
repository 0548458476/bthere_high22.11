//
//  CalenderSettingTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/3/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol dismiss{
    func dismmissKeybourd()
}

protocol delKbCalenderNotifDelegate {
    func delKbCalenderNotif()
}

class CalenderSettingTableViewCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,delKbCalenderNotifDelegate,saveDataToWorkerDelegate{
    
    //MARK: - Properties
    
var arrayServiceProduct:Array<String> =
    [NSLocalizedString("DAY_DESIGN", comment: ""),NSLocalizedString("WEEK_DESIGN", comment: ""),NSLocalizedString("MONTH_DESIGN", comment: ""),NSLocalizedString("LIST_DESIGN", comment: "")]
    var delegate:dismiss! = nil
    var delegateSave:reloadTableForSaveDelegate! = nil
    var delegateKb:delKbNotificationDelegate!=nil
    var delegateKbBusiness:delKbNotifBusinessDelegate!=nil
    
    var delegateScroll:scrollOnEditDelegate!=nil
    
    var isValidDate = false
    var isValidDesingTblVal = false
    var isValidMaxServiceForCustomer = false
    var isValidPeriodInWeeksForMaxServices = false
    
    var selectedTextField:UITextField?
    
    //MARK: - Outlet
    
    // @IBOutlet var txtDate: UITextField!
    
    @IBOutlet weak var lblDoWantLimit: UILabel!
    @IBOutlet weak var lblDesignCal: UILabel!
    //@IBOutlet weak var lblDateOpen: UILabel!
    
    @IBOutlet weak var btnYesSelect: CheckBoxForDetailsWorker2!
    @IBAction func btnYesSelect(sender: CheckBoxForDetailsWorker2) {
        btnNoCheck.isCecked = false
        txtPeriodInWeeksForMaxServices.enabled = true
        txtMaxServiceForCustomer.enabled = true
        if txtMaxServiceForCustomer.text == ""
        {
        txtMaxServiceForCustomer.text = "3"

        }
        sender.isCecked = true
    }
    @IBOutlet weak var btnNoCheck: checkBoxForDetailsWorker!
    @IBAction func btnNoCheck(sender: UIButton)
    {
        btnYesSelect.isCecked = false
        btnNoCheck.isCecked = true
        txtPeriodInWeeksForMaxServices.enabled = false
        txtMaxServiceForCustomer.enabled = false
        txtMaxServiceForCustomer.textColor = UIColor.blackColor()
        txtMaxServiceForCustomer.text = ""
        txtPeriodInWeeksForMaxServices.textColor = UIColor.blackColor()
        txtPeriodInWeeksForMaxServices.text = ""
    }
    @IBOutlet weak var lblNumAppointmets: UILabel!
    
    @IBOutlet weak var lblDuringTurns: UILabel!
    @IBAction func btnOpenDesingTbl(sender: UIButton) {
        
        if btnOpenDesingTbl.tag == 1
        {
            btnOpenDesingTbl.tag = 0
            tblDesignedCalendar.hidden = false
            tblDesignedCalendar.reloadData()
            // dP.hidden = true
        }
        else
        {
            btnOpenDesingTbl.tag = 1
            tblDesignedCalendar.hidden = true
            //  dP.hidden = true
        }
        
        
    }

   
    
    @IBOutlet weak var btnLimitSeries: checkBoxForDetailsWorker!
    
    @IBOutlet weak var txtMaxServiceForCustomer: UITextField!
    
    @IBOutlet weak var txtPeriodInWeeksForMaxServices: UITextField!
    
    //   @IBOutlet weak var txtDtCalendarOpenDate: UITextField!
    
    @IBOutlet var btnOpenDesingTbl: UIButton!
    
    @IBOutlet var tblDesignedCalendar: UITableView!
    
    @IBAction func btnSave(sender: UIButton) {
        
        Global.sharedInstance.fIsSaveConCalenderSettingPressed = true
        
        //        if txtDate.text == "" || txtDate.textColor == UIColor.redColor()
        //        {
        //            txtDate.textColor = UIColor.redColor()
        //            txtDate.text = NSLocalizedString("REQUIREFIELD", comment: "")
        //            isValidDate = false
        //        }
        //        else
        //        {
        //            isValidDate = true
        //        }
        
        if btnOpenDesingTbl.titleLabel!.text == nil || btnOpenDesingTbl.titleLabel?.textColor == UIColor.redColor()
        {
            btnOpenDesingTbl.setTitleColor(UIColor.redColor(), forState: .Normal)
            btnOpenDesingTbl.setTitle(NSLocalizedString("REQUIREFIELD", comment: ""), forState: .Normal)
            isValidDesingTblVal = false
        }
        else
        {
            isValidDesingTblVal = true
        }
        
        
        if txtMaxServiceForCustomer.text == "" && btnYesSelect.isCecked == true || txtMaxServiceForCustomer.text == NSLocalizedString("REQUIREFIELD", comment: "")
        {
            //txtMaxServiceForCustomer.textColor = UIColor.redColor()
            //txtMaxServiceForCustomer.text = NSLocalizedString("REQUIREFIELD", comment: "")
            isValidMaxServiceForCustomer = false
        }
        else
        {
            isValidMaxServiceForCustomer = true
        }
        
        if txtPeriodInWeeksForMaxServices.text == "" && btnYesSelect.isCecked == true || txtPeriodInWeeksForMaxServices.text == NSLocalizedString("REQUIREFIELD", comment: "")
        {
            //txtPeriodInWeeksForMaxServices.textColor = UIColor.redColor()
            //txtPeriodInWeeksForMaxServices.text = NSLocalizedString("REQUIREFIELD", comment: "")
            isValidPeriodInWeeksForMaxServices = false
            
        }
            //        else if Int(txtPeriodInWeeksForMaxServices.text!)! > 55
            //        {
            //            txtPeriodInWeeksForMaxServices.textColor = UIColor.redColor()
            //            txtPeriodInWeeksForMaxServices.text = NSLocalizedString("REQUIREFIELD", comment: "")
            //            isValidPeriodInWeeksForMaxServices = false
            //        }
        else
        {
            isValidPeriodInWeeksForMaxServices = true
        }
        
        if isValidDesingTblVal == true && isValidMaxServiceForCustomer == true && isValidPeriodInWeeksForMaxServices == true
        {
            delegateSave.reloadTableForSave(self.tag,btnTag: 1)
            
            var iFirstCalendarViewType:Int = 0
            //הוספתי בגלל שאין את הטבלה וקרס, אח״כ ייתכן שזה לא ידרש
            if let x = btnOpenDesingTbl.titleLabel!.text
            {
                switch(btnOpenDesingTbl.titleLabel!.text!)
                {
                case NSLocalizedString("DAY_DESIGN", comment: ""):
                    iFirstCalendarViewType = 1
                case NSLocalizedString("WEEK_DESIGN", comment: ""):
                    iFirstCalendarViewType = 2
                case NSLocalizedString("MONTH_DESIGN", comment: ""):
                    iFirstCalendarViewType = 3
                case NSLocalizedString("LIST_DESIGN", comment: ""):
                    iFirstCalendarViewType = 4
                default:
                    return
                }
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            //var dtCalendarOpenDate: NSDate?
            //            if txtDate.text != "" && txtDate.text != NSLocalizedString("REQUIREFIELD", comment: "")
            //            {
            //                Global.sharedInstance.isDateNil = false
            //                dtCalendarOpenDate = dateFormatter.dateFromString(txtDate.text!)!
            //            }
            //            else
            //            {
            //                dtCalendarOpenDate = NSDate()
            //                Global.sharedInstance.isDateNil = true
            //            }
            
            Global.sharedInstance.generalDetails.calendarProperties = objCalendarProperties(
                _iFirstCalendarViewType: iFirstCalendarViewType,
                _dtCalendarOpenDate:String(),
                _bLimitSeries: btnYesSelect.isCecked,
                _iMaxServiceForCustomer: Global.sharedInstance.parseJsonToInt(txtMaxServiceForCustomer.text!),
                _iPeriodInWeeksForMaxServices: Global.sharedInstance.parseJsonToInt(txtPeriodInWeeksForMaxServices.text!),
                _bSyncGoogleCalendar: false/*btnYesSynch.isCecked*/
                //2do we have to take it from the first page 
            )
            
            Global.sharedInstance.fIsEmptyCalenderSetting = false
            Global.sharedInstance.headersCellRequired[4] = true
        }
        else
        {
            Global.sharedInstance.fIsEmptyCalenderSetting = true
            //Global.sharedInstance.headersCellRequired[4] = false
        }
    }
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblDoWantLimit.text = NSLocalizedString("LIMIT_TURNS", comment: "")
        lblNumAppointmets.text = NSLocalizedString("NUM_TURNS_MEETINGS", comment: "")
        lblDuringTurns.text = NSLocalizedString("DURATION_TURNS", comment: "")
        
        Global.sharedInstance.GlobalDataVC?.delegateSaveCalendar = self
        btnYesSelect.isCecked = true
      Global.sharedInstance.isSelected = true
        if self.btnYesSelect.isCecked == true
        {
            self.txtMaxServiceForCustomer.enabled = true
            self.txtPeriodInWeeksForMaxServices.enabled = true
        }
        Global.sharedInstance.calendarSetting = self
        
        //place holder
        
//        txtMaxServiceForCustomer.attributedPlaceholder = NSAttributedString(string:"מקסימום 100", attributes: [NSForegroundColorAttributeName: UIColor.darkGrayColor()])
//        txtPeriodInWeeksForMaxServices.attributedPlaceholder = NSAttributedString(string:"מקסימום 55 ש׳", attributes: [NSForegroundColorAttributeName: UIColor.darkGrayColor()])
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
            lblDuringTurns.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
//            lblDesignCal.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblDoWantLimit.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
            lblDuringTurns.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
           
            lblNumAppointmets.font = UIFont(name: "OpenSansHebrew-Light", size: 14)
           
            
        }
        
//        tblDesignedCalendar.hidden = true
//        self.bringSubviewToFront(tblDesignedCalendar)
        
        // Initialization code
        //txtDate.delegate = self
        txtMaxServiceForCustomer.delegate = self
        txtPeriodInWeeksForMaxServices.delegate = self
        
        // txtDate.text = ""
        //txtMaxServiceForCustomer.text = ""
        txtPeriodInWeeksForMaxServices.text = ""
//        btnOpenDesingTbl.setTitle("תצוגת יום", forState: .Normal)
//        
//        tblDesignedCalendar.delegate = self
//        tblDesignedCalendar.dataSource = self
//        tblDesignedCalendar.separatorStyle = .None
        
        //tblNumAppointments.hidden = true
        // Initialization code
        //tblNumAppointments.delegate = self
        //tblNumAppointments.dataSource = self
        //tblNumAppointments.separatorStyle = .None
//        //opentblNumAppointemt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
//        btnOpenDesingTbl.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        //        dP.backgroundColor = Colors.sharedInstance.color9
        //        dP.setValue(UIColor.blackColor(), forKeyPath: "textColor")
        //        dP.setValue(1, forKeyPath: "alpha")
        //        dP.datePickerMode = UIDatePickerMode.Date
        //        dP.minimumDate = NSDate()
        //        dP.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
        //        txtDate.addTarget(self, action: "textFieldDidEndEditing:", forControlEvents: UIControlEvents.EditingChanged)
        // Initialization code
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action:#selector(CalenderSettingTableViewCell.dismissDP))
        self.contentView.addGestureRecognizer(tap)
        tap.delegate = self
        
        
//        tblDesignedCalendar.layer.borderColor = UIColor.whiteColor().CGColor
//        tblDesignedCalendar.layer.borderWidth = 1
//        tblDesignedCalendar.allowsSelection = true
//        
//        btnOpenDesingTbl.tag = 1
//        btnOpenDesingTbl.setTitleColor(UIColor.blackColor(), forState: .Normal)
//        btnOpenDesingTbl.setTitle("תצוגת יום", forState: .Normal)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if txtMaxServiceForCustomer.text == ""
        {
            txtMaxServiceForCustomer.text = "3"
        }
    }
    
    //מחקתי בינתיים, לבדוק אם נצרך!
    //
    //    //MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrayServiceProduct.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CalendarDesignTableViewCell")as!CalendarDesignTableViewCell
        let lineView: UIView = UIView(frame: CGRectMake(0, cell.contentView.frame.height - 1, cell.contentView.frame.size.width + 60, 1))
        lineView.backgroundColor = UIColor.whiteColor()
        cell.contentView.addSubview(lineView)
        cell.lblText?.font = UIFont(name: "OpenSansHebrew-Light", size: 15)
        //cell.lblText!.text = arrayServiceProduct[indexPath.row]
        cell.setDisplayData(arrayServiceProduct[indexPath.row])
        cell.lblText?.textColor = UIColor.whiteColor()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let str:String = ((tblDesignedCalendar.cellForRowAtIndexPath(indexPath) as! CalendarDesignTableViewCell).lblText?.text)!
        
        switch (tableView){
        case tblDesignedCalendar:
            btnOpenDesingTbl.setTitleColor(UIColor.blackColor(), forState: .Normal)
            btnOpenDesingTbl.setTitle(str, forState: .Normal)
            Global.sharedInstance.calendarDesigned = indexPath.row
            tblDesignedCalendar.hidden = true
            btnOpenDesingTbl.tag = 1
        default:
            btnOpenDesingTbl.tag = 1
            
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.height / 4
    }
    
    //MARK: - DatePicker
    
    
    
    func pickerView(pickerView: UIDatePicker, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = "PickerView Cell Title"
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "OpenSansHebrew-Regular", size: 23) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    
    
    func handleDatePicker(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // dateFormatter.dateFormat = "yyyyMMdd"
        
        //var toNSDate : NSDate!
        //txtDate.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        //,textfield:UITextField
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        //   txtDate.text = dateFormatter.stringFromDate(sender.date)
    }
    
    //MARK: - TextField
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool
    {
        var startString = ""
        if (textField.text != nil)
        {
            startString += textField.text!
        }
        startString += string
        
        let limitNumber = Int(startString)
        
        if textField == txtPeriodInWeeksForMaxServices
        {
            if txtPeriodInWeeksForMaxServices.text == NSLocalizedString("REQUIREFIELD", comment: "")
            {
                textField.textColor = UIColor.blackColor()
                textField.text = ""
            }
            else
            {
//                if limitNumber > 55
//                {
//                    Alert.sharedInstance.showAlert("מקסימום 55 שעות", vc: Global.sharedInstance.GlobalDataVC!)
//                    return false
//                }
//                else
//                {
                    return true
//                }
            }
        }
        else if textField == txtMaxServiceForCustomer
        {
            if txtMaxServiceForCustomer.text == NSLocalizedString("REQUIREFIELD", comment: "")
            {
                textField.textColor = UIColor.blackColor()
                textField.text = ""
            }
            else
            {
            if limitNumber > 100
            {
                 Alert.sharedInstance.showAlert(NSLocalizedString("MAX_CHAR", comment: ""), vc: Global.sharedInstance.GlobalDataVC!)
                return false
            }
            else
            {
                return true
            }
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {

        selectedTextField = textField
        
        if textField.text == NSLocalizedString("REQUIREFIELD", comment: "") || (textField.text == "3" && textField == txtMaxServiceForCustomer)
        {
            textField.text = ""
        }


        if delegateKb != nil
        {
            // remove the keyBoard event from itemInSection3 cell
            delegateKb.delKbNotification()
        }
        if delegateKbBusiness != nil
        {
            // remove the keyBoard event from businessServices cell
            delegateKbBusiness.delKbNotifBusiness()
        }
        
        if Global.sharedInstance.didCalendarSettingClose == true
        {
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(CalenderSettingTableViewCell.keyboardWillShow(_:)),name: UIKeyboardWillShowNotification,object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self,selector: #selector(CalenderSettingTableViewCell.keyboardWillHide(_:)),name: UIKeyboardWillHideNotification,object: nil)
            Global.sharedInstance.didServicesClosed = false
        }

        textField.textColor = UIColor.blackColor()

        //        if textField == txtDate
        //        {
        //        tblDesignedCalendar.hidden = true
        //        btnOpenDesingTbl.tag = 1
        //        delegate.dismmissKeybourd()
        //       // dP.hidden = false
        //
        //        }
        //        else
        //        {
        
//        tblDesignedCalendar.hidden = true
//        btnOpenDesingTbl.tag = 1
        //dP.hidden = true
        // }
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        //dP.hidden = true
//        tblDesignedCalendar.hidden = true
//        btnOpenDesingTbl.tag = 1
        delegate.dismmissKeybourd()
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //dP.hidden = true
    }
    
    
    func dismissDP()
    {
        //dP.hidden = true
//        tblDesignedCalendar.hidden = true
//        btnOpenDesingTbl.tag = 1
    }
    
    override func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//        if (touch.view!.isDescendantOfView(self.tblDesignedCalendar)) {
//            
//            return false
//        }
        return true
    }
    
    func keyboardWillShow(note: NSNotification)
    {
        if Global.sharedInstance.isFirstOpenKeyBoard == false
        {
            Global.sharedInstance.isFirstOpenKeyBoard = true
            
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                if let a = delegateScroll
                {
                    
                    delegateScroll.scrollOnEdit(keyboardSize,textField: selectedTextField!)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didCalendarSettingClose = true
                    
                }
                Global.sharedInstance.isFirstCloseKeyBoard = false
            }
        }
    }
    //for scrolling the table down, after editing textField
    func keyboardWillHide(note: NSNotification) {
        
        
        if Global.sharedInstance.isFirstCloseKeyBoard == false
        {
            Global.sharedInstance.isFirstCloseKeyBoard = true
            if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                if let a = delegateScroll
                {
                    // scroll the table - in global data
                    delegateScroll.scrollOnEndEdit(keyboardSize)
                }
                else
                {
                    // remove the keyBoard events - Notifications
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
                    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
                    Global.sharedInstance.didCalendarSettingClose = true
                    
                }
                
                Global.sharedInstance.isFirstOpenKeyBoard = false
            }
        }
    }
    
    // remove the keyBoard events to prevent invoke the events from other cells
    func delKbCalenderNotif()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        Global.sharedInstance.didCalendarSettingClose = true
    }
    
    func  saveDataToWorker()->Bool{
      //תמיד מחזיר true כדי שיסגור את הסל גם אם אינו תקין
            
            Global.sharedInstance.fIsSaveConCalenderSettingPressed = true
            
            //        if txtDate.text == "" || txtDate.textColor == UIColor.redColor()
            //        {
            //            txtDate.textColor = UIColor.redColor()
            //            txtDate.text = NSLocalizedString("REQUIREFIELD", comment: "")
            //            isValidDate = false
            //        }
            //        else
            //        {
            //            isValidDate = true
            //        }
            
//            if btnOpenDesingTbl.titleLabel!.text == nil || btnOpenDesingTbl.titleLabel?.textColor == UIColor.redColor()
//            {
//                btnOpenDesingTbl.setTitleColor(UIColor.redColor(), forState: .Normal)
//                btnOpenDesingTbl.setTitle(NSLocalizedString("REQUIREFIELD", comment: ""), forState: .Normal)
//                isValidDesingTblVal = false
//            }
//            else
//            {
                isValidDesingTblVal = true
//            }
        
            
            if txtMaxServiceForCustomer.text == "" && btnYesSelect.isCecked == true || txtMaxServiceForCustomer.text == NSLocalizedString("REQUIREFIELD", comment: "")
            {
                //txtMaxServiceForCustomer.textColor = UIColor.redColor()
                //txtMaxServiceForCustomer.text = NSLocalizedString("REQUIREFIELD", comment: "")
                isValidMaxServiceForCustomer = false
            }
            else
            {
                isValidMaxServiceForCustomer = true
            }
            
            if txtPeriodInWeeksForMaxServices.text == "" && btnYesSelect.isCecked == true || txtPeriodInWeeksForMaxServices.text == NSLocalizedString("REQUIREFIELD", comment: "")
            {
                //txtPeriodInWeeksForMaxServices.textColor = UIColor.redColor()
                //txtPeriodInWeeksForMaxServices.text = NSLocalizedString("REQUIREFIELD", comment: "")
                isValidPeriodInWeeksForMaxServices = false
                
            }
                //        else if Int(txtPeriodInWeeksForMaxServices.text!)! > 55
                //        {
                //            txtPeriodInWeeksForMaxServices.textColor = UIColor.redColor()
                //            txtPeriodInWeeksForMaxServices.text = NSLocalizedString("REQUIREFIELD", comment: "")
                //            isValidPeriodInWeeksForMaxServices = false
                //        }
            else
            {
                isValidPeriodInWeeksForMaxServices = true
            }
            
            if isValidDesingTblVal == true && isValidMaxServiceForCustomer == true && isValidPeriodInWeeksForMaxServices == true
            {
                delegateSave.reloadTableForSave(self.tag,btnTag: 1)
                
                let iFirstCalendarViewType:Int = 0
                //הוספתי בגלל שאין את הטבלה וקרס, אח״כ ייתכן שזה לא ידרש
//                if let x = btnOpenDesingTbl.titleLabel!.text
//                {
//                    switch(btnOpenDesingTbl.titleLabel!.text!)
//                    {
//                    case "תצוגת יום":
//                        iFirstCalendarViewType = 1
//                    case "תצוגת שבוע":
//                        iFirstCalendarViewType = 2
//                    case "תצוגת חודש":
//                        iFirstCalendarViewType = 3
//                    case "תצוגת רשימה":
//                        iFirstCalendarViewType = 4
//                    default:
//                     break
//                    }
//                }
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                //var dtCalendarOpenDate: NSDate?
                //            if txtDate.text != "" && txtDate.text != NSLocalizedString("REQUIREFIELD", comment: "")
                //            {
                //                Global.sharedInstance.isDateNil = false
                //                dtCalendarOpenDate = dateFormatter.dateFromString(txtDate.text!)!
                //            }
                //            else
                //            {
                //                dtCalendarOpenDate = NSDate()
                //                Global.sharedInstance.isDateNil = true
                //            }
                
                Global.sharedInstance.generalDetails.calendarProperties = objCalendarProperties(
                    _iFirstCalendarViewType: iFirstCalendarViewType,
                    _dtCalendarOpenDate:String(),
                    _bLimitSeries: btnYesSelect.isCecked,
                    _iMaxServiceForCustomer: Global.sharedInstance.parseJsonToInt(txtMaxServiceForCustomer.text!),
                    _iPeriodInWeeksForMaxServices: Global.sharedInstance.parseJsonToInt(txtPeriodInWeeksForMaxServices.text!),
                    _bSyncGoogleCalendar: false/*btnYesSynch.isCecked*/
                    //2do we have to take it from the first page 
                )
                
                Global.sharedInstance.fIsEmptyCalenderSetting = false
                Global.sharedInstance.headersCellRequired[4] = true
                return true
                
            }
            else
            {
                Global.sharedInstance.fIsEmptyCalenderSetting = true
              //  Global.sharedInstance.headersCellRequired[4] = false
                //return false
                return true
            }
        
    }
    
}
