//
//  MyCostumerTableViewCell.swift
//  bthree-ios
//
//  Created by User on 21.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
protocol scrollCollectionDelegate {
    func scrollCollection()
}
//ספק קיים
class MyCostumerTableViewCell: UITableViewCell{
    var row:Int = 0
    var indexPathRow:Int = 0
    var viewDelegate:MyCostumersViewController!
    
    @IBOutlet weak var collItems: UICollectionView!
    let isStarArr:Array<Bool> = [true,false,false,true,false]
    let imgOptionsArray:Array<String> = ["44.png","call.png","46.png","47.png"]
    let nameOptionsArray:Array<String> = ["מחק","חייג","תורי לקוח","פרטי לקוח"]
    
    func setDisplayData(index:Int)
    {
        indexPathRow = index
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func scrollCollection()  {
//  let index:NSIndexPath = NSIndexPath(forRow:0, inSection: 0)
//        collItems.collItems.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
    }

}

extension MyCostumerTableViewCell : UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row == 0
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CostumerFromListCollectionViewCell", forIndexPath: indexPath) as! CostumerFromListCollectionViewCell
            cell.setDisplayData(Global.sharedInstance.imgCostumersArray[indexPathRow], costumerName:Global.sharedInstance.nameCostumersArray[indexPathRow], isStar: isStarArr[indexPathRow])
            cell.viewSideLine.layer.shadowColor = UIColor.blackColor().CGColor
            cell.viewSideLine.layer.shadowOpacity = 0.75
            cell.viewSideLine.layer.shadowOffset = CGSizeZero
            cell.viewSideLine.layer.shadowRadius = 1.5
            return cell
        }
        else
        {
            let cell1 = collectionView.dequeueReusableCellWithReuseIdentifier("OptionsForCostumerCollectionViewCell", forIndexPath: indexPath) as! OptionsForCostumerCollectionViewCell
            cell1.row = row
            cell1.coll = collItems
            cell1.delegateCloseCollectionDelegate = viewDelegate
            cell1.delegate = viewDelegate
            cell1.setDisplayData(imgOptionsArray[indexPath.row - 1], desc: nameOptionsArray[indexPath.row - 1],index: indexPath.row)
            cell1.backgroundColor? = UIColor(red: 246/255.0, green: 240/255.0, blue: 232/255.0, alpha: 1.0)
  
            return cell1
        }
    }
    
}

extension MyCostumerTableViewCell : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0
        {
            //        let itemsPerRow:CGFloat = 1
            //        let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else
        {
            let itemsPerRow:CGFloat = 6.5
            // let hardCodedPadding:CGFloat = 0
            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth + 10, height: itemHeight)
        }
    }
    
}
