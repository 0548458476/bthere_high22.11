//
//  OpenRowListCollectionViewCell.swift
//  Bthere
//
//  Created by User on 23.5.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class OpenRowListCollectionViewCell: UICollectionViewCell {
    var delegate:openFromMenuDelegate!=nil
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var imgIcon: UIImageView!
    var dateTurn:String = ""
    var hourTurn:String = ""
    @IBOutlet weak var btnOpen: UIButton!
    
    var storyB = UIStoryboard(name: "SupplierExist", bundle: nil)
    
    @IBAction func btnOpen(sender: AnyObject) {
        switch indexPath {
        case 1://פרטי התור

            let viewCon = storyB.instantiateViewControllerWithIdentifier("DetailsAppointmentViewController") as! DetailsAppointmentViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
            Global.sharedInstance.listDesign?.presentViewController(viewCon, animated: true, completion: nil)
            

        case 2://ביטול התור
            
            let viewCon = storyB.instantiateViewControllerWithIdentifier("CancelAppointmentViewController") as! CancelAppointmentViewController
            viewCon.modalPresentationStyle = UIModalPresentationStyle.Custom
           viewCon.dateTurn = dateTurn
            viewCon.hourTurn = hourTurn
            Global.sharedInstance.listDesign?.presentViewController(viewCon, animated: true, completion: nil)
            return
        case 3://חייג
            return
        default:
            return
        }
    }

    var indexPath:Int = 0
    
    func setDisplayData(text:String,img:String,index:Int) {
        lblText.text = text
        imgIcon.image = UIImage(named: img)
        indexPath = index
    }
}
