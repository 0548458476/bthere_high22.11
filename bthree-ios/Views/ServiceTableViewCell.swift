//
//  ServiceTableViewCell.swift
//  bthree-ios
//
//  Created by Lior Ronen on 3/23/16.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet var topBorder: UIView!
    @IBOutlet var lblDesc: UILabel!
    
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var buttomBorder: UIView!
    
    @IBOutlet var lblTime: UILabel!
    
    ///MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .None
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(descService:String,time:String,price:String){
        lblDesc.text = descService
        lblPrice.text = price
        lblTime.text = time
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        for subview in self.subviews {
            
            for subview2 in subview.subviews {
                if (String(subview2).rangeOfString("UITableViewCellActionButton") != nil) {
                    for view in subview2.subviews {
                        if (String(view).rangeOfString("UIButtonLabel") != nil) {
                            
                            if let label = view as? UILabel {
                                
                                label.textColor = UIColor.blackColor()
                                label.font = UIFont (name: "OpenSansHebrew-Light", size: 22)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }


}
