//
//  MainTableViewCell.swift
//  bthree-ios
//
//  Created by User on 23.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var btnOpenCell: UIButton!
    
    @IBOutlet weak var txtHolder: UITextField!
    
    @IBOutlet weak var lblGwiazdka: UILabel!//כוכבית
    @IBOutlet weak var lblMessageError: UILabel!
    @IBOutlet var txtSub: UITextField!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var viewButtom: UIView!
    @IBOutlet weak var viewtop: UIView!
    
    var delegateKb:delKbNotificationDelegate!=nil
    var delegateKbBusiness:delKbNotifBusinessDelegate!=nil
    var delegateKbCalendar:delKbCalenderNotifDelegate!=nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnOpenCell.enabled = false
        btnOpenCell.adjustsImageWhenDisabled = false
        
        txtSub.enabled = false

        txtSub.delegate = self
        txtHolder.delegate = self

        txtSub.text = ""
        txtSub.textColor = UIColor.blackColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(str:String,hidden:Bool,imageArrow:String,textArrow:String){
        btnOpenCell.setTitle(textArrow, forState: .Normal)
        if imageArrow == ""
        {
            btnOpenCell.setBackgroundImage(UIImage(), forState: .Normal)
        }
        else
        {
          btnOpenCell.setBackgroundImage(UIImage(named: imageArrow), forState: .Normal)
        }
        self.contentView.sendSubviewToBack(btnOpenCell)
        lblDesc.text = str
        lblGwiazdka.hidden = hidden
    }

//    func setDisplayData(str:String){
//        lblDesc.text = str
//    }
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.delegateKb = Global.sharedInstance.itemInSection3TableViewCell
        self.delegateKbBusiness = Global.sharedInstance.businessService
        
        if delegateKbCalendar != nil
        {
            delegateKbCalendar.delKbCalenderNotif()
        }
        
        if delegateKb != nil
        {
        delegateKb.delKbNotification()
        }
        if delegateKbBusiness != nil
        {
        delegateKbBusiness.delKbNotifBusiness()
        }
        
        if textField == txtSub
        {
        print("txtsub")
        }
        else if textField == txtHolder
        {
            print("holder")
        }
    }

    func textFieldDidEndEditing(textField: UITextField) {
        
//        Global.sharedInstance.generalDetails.nvSlogen = txtHolder.text!
    }
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtSub
        {
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            if startString.characters.count > 120
            {
                return false
            }
            else
            {
                return true
            }
        }
        return true
    }

    
    
    

    //לא חוקי או שדה חובה
    func setInvalid(message:String)
    {
        txtSub.text = message
        txtSub.textColor = UIColor.redColor()
    }
}
