//
//  allKindEventsForListDesign.swift
//  Bthere
//
//  Created by Racheli Kroiz on 8.9.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class allKindEventsForListDesign: NSObject {

    var dateEvent:NSDate = NSDate()
    var title:String = ""
    var fromHour:String = ""
    var toHour:String = ""
    var tag:Int = 0//1=Bthere,2=phone
    var nvAddress:String = ""
    var nvSupplierName:String = ""
    var iDayInWeek:Int = 0
    var nvServiceName:String = ""
    var nvComment:String = ""
    
    override init() {
        dateEvent = NSDate()
        title = ""
        fromHour = ""
        toHour = ""
        tag = 0
        nvAddress = ""
        iDayInWeek = 0
        nvServiceName = ""
        nvComment = ""
    }
    
    init(_dateEvent:NSDate,_title:String,_fromHour:String,_toHour:String,_tag:Int,_nvAddress:String,_nvSupplierName:String,_iDayInWeek:Int,_nvServiceName:String,_nvComment:String) {
        dateEvent = _dateEvent
        title = _title
        fromHour = _fromHour
        toHour = _toHour
        tag = _tag
        nvAddress = _nvAddress
        nvSupplierName = _nvSupplierName
        iDayInWeek = _iDayInWeek
        nvServiceName = _nvServiceName
        nvComment = _nvComment
    }
}
