//
//  objProviderServices.swift
//  bthree-ios
//
//  Created by User on 30.3.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class objProviderServices: NSObject {
    
    var nvServiceName:String = ""
    var iPrice:Float = 0
    var nUntilPrice:Float
    var iTimeOfService:Int = 0
    var iUntilSeviceTime:Int = 0
    var iTimeInterval:Int = 0
    var iMaxConcurrentCustomers:Int = 0
    //var iMinConcurrentCustomers:Int = 0
    var iDiscount:Int = 0
    var iServiceType:Int = 0//89=שרות=90 או מוצר
    var bDisplayPerCustomer:Bool = true
    
//add varibals to get function
    var iProviderServiceId:Int = Int()
    var iMinConcurrentCustomers:Int = 0
    
    override init() {
        
        nvServiceName = ""
        iPrice = 0
        iTimeOfService = 0
        nUntilPrice = 0
        iTimeInterval = 0
        iUntilSeviceTime = 0
        iMaxConcurrentCustomers = 0
   //     iMinConcurrentCustomers = 0
        iDiscount = 0
        iServiceType = 0
        bDisplayPerCustomer = true
    }
    
    init(_nvServiceName:String,_iPrice:Float,_nUntilPrice:Float,_iTimeOfService:Int,_iUntilSeviceTime:Int//_iConcurrentCustomers:Int
        ,_iTimeInterval:Int,_iMaxConcurrentCustomers:Int,//_iMinConcurrentCustomers:Int,
        _iDiscount:Int,_iServiceType:Int,_bDisplayPerCustomer:Bool) {

        nvServiceName = _nvServiceName
        iPrice = _iPrice
        nUntilPrice = _nUntilPrice
        iTimeOfService = _iTimeOfService
        iUntilSeviceTime = _iUntilSeviceTime
        //iConcurrentCustomers = _iConcurrentCustomers
        iTimeInterval = _iTimeInterval
        iMaxConcurrentCustomers = _iMaxConcurrentCustomers
        //iMinConcurrentCustomers = _iMinConcurrentCustomers
        iDiscount = _iDiscount
        iServiceType = _iServiceType
        bDisplayPerCustomer = _bDisplayPerCustomer
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["nvServiceName"] = nvServiceName
        dic["iPrice"] = iPrice
        dic["nUntilPrice"] = nUntilPrice
        dic["iTimeOfService"] = iTimeOfService
        dic["iUntilSeviceTime"] = iUntilSeviceTime
        //dic["iConcurrentCustomers"] = iConcurrentCustomers
        dic["iTimeInterval"] = iTimeInterval
        dic["iMaxConcurrentCustomers"] = iMaxConcurrentCustomers
      //  dic["iMinConcurrentCustomers"] = iMinConcurrentCustomers
        dic["iDiscount"] = iDiscount
        dic["iServiceType"] = iServiceType
        dic["bDisplayPerCustomer"] = bDisplayPerCustomer
        
        return dic
    }
    
    func objProviderServicesToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<objProviderServices>{
        
        var arrServiceProv:Array<objProviderServices> = Array<objProviderServices>()
        var ProvService:objProviderServices = objProviderServices()
        
        for i in 0  ..< arrDic.count
        {
            ProvService = dicToProviderServices(arrDic[i])
            arrServiceProv.append(ProvService)
        }
        return arrServiceProv
    }
    
    func dicToProviderServices(dic:Dictionary<String,AnyObject>)->objProviderServices
    {
        let serviceProv:objProviderServices = objProviderServices()
        
        serviceProv.nvServiceName = Global.sharedInstance.parseJsonToString(dic["nvServiceName"]!)
        serviceProv.iPrice = Global.sharedInstance.parseJsonToFloat(dic["iPrice"]!)
        serviceProv.nUntilPrice = Global.sharedInstance.parseJsonToFloat(dic["nUntilPrice"]!)
        serviceProv.iTimeOfService = Global.sharedInstance.parseJsonToInt(dic["iTimeOfService"]!)
        serviceProv.iUntilSeviceTime = Global.sharedInstance.parseJsonToInt(dic["iUntilSeviceTime"]!)
        //dic["iConcurrentCustomers"] = iConcurrentCustomers
        serviceProv.iTimeInterval = Global.sharedInstance.parseJsonToInt(dic["iTimeInterval"]!)
        serviceProv.iMaxConcurrentCustomers = Global.sharedInstance.parseJsonToInt(dic["iMaxConcurrentCustomers"]!)
        //  dic["iMinConcurrentCustomers"] = iMinConcurrentCustomers
       serviceProv.iDiscount =  Global.sharedInstance.parseJsonToInt(dic["iDiscount"]!)
        serviceProv.iServiceType =  Global.sharedInstance.parseJsonToInt(dic["iServiceType"]!)
        serviceProv.bDisplayPerCustomer = dic["bDisplayPerCustomer"]! as! Bool

        return serviceProv
    }
    func objProviderServicesToArrayGet(arrDic:Array<Dictionary<String,AnyObject>>)->Array<objProviderServices>{
        
        var arrServiceProv:Array<objProviderServices> = Array<objProviderServices>()
        var ProvService:objProviderServices = objProviderServices()
        
        for i in 0  ..< arrDic.count
        {
            ProvService = dicToProviderServicesToGet(arrDic[i])
            arrServiceProv.append(ProvService)
        }
        return arrServiceProv
    }

    func dicToProviderServicesToGet(dic:Dictionary<String,AnyObject>)->objProviderServices
    {
        let serviceProv:objProviderServices = objProviderServices()
        //
        serviceProv.iProviderServiceId = Global.sharedInstance.parseJsonToInt(dic["iProviderServiceId"]!)
        serviceProv.iMinConcurrentCustomers = Global.sharedInstance.parseJsonToInt(dic["iMinConcurrentCustomers"]!)

        serviceProv.nvServiceName = Global.sharedInstance.parseJsonToString(dic["nvServiceName"]!)
        serviceProv.iPrice = Global.sharedInstance.parseJsonToFloat(dic["iPrice"]!)
        serviceProv.nUntilPrice = Global.sharedInstance.parseJsonToFloat(dic["nUntilPrice"]!)
        serviceProv.iTimeOfService = Global.sharedInstance.parseJsonToInt(dic["iTimeOfService"]!)
        serviceProv.iUntilSeviceTime = Global.sharedInstance.parseJsonToInt(dic["iUntilSeviceTime"]!)

        //dic["iConcurrentCustomers"] = iConcurrentCustomers
        serviceProv.iTimeInterval = Global.sharedInstance.parseJsonToInt(dic["iTimeInterval"]!)
        serviceProv.iMaxConcurrentCustomers = Global.sharedInstance.parseJsonToInt(dic["iMaxConcurrentCustomers"]!)
        //  dic["iMinConcurrentCustomers"] = iMinConcurrentCustomers
        serviceProv.iDiscount =  Global.sharedInstance.parseJsonToInt(dic["iDiscount"]!)
        serviceProv.iServiceType =  Global.sharedInstance.parseJsonToInt(dic["iServiceType"]!)
        serviceProv.bDisplayPerCustomer = dic["bDisplayPerCustomer"]! as! Bool
        
        return serviceProv
    }

}
