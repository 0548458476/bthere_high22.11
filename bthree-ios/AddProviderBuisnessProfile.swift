//
//  AddProviderBuisnessProfile.swift
//  bthree-ios
//
//  Created by User on 11.4.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit

class AddProviderBuisnessProfile: NSObject {

    var iProviderUserId:Int = 0
    var nvILogoImage:String = ""
    var nvHeaderImage:String = ""
    var nvFooterImage:String = ""
    var nvAboutComment:String = ""
    var nvCampaignImage:String = ""
    var nvSlogen:String = ""
    var nvLat:String = ""
    var nvLong:String = ""
    var ProviderWorkingHoursObj:Array<objWorkingHours> = Array<objWorkingHours>()
    
    
    override init() {
        iProviderUserId = 0
        nvILogoImage = ""
        nvHeaderImage = ""
        nvFooterImage = ""
        nvAboutComment = ""
        nvCampaignImage = ""
        nvSlogen = ""
        nvLat = ""
        nvLong = ""
        ProviderWorkingHoursObj = Array<objWorkingHours>()
    }
    
    init(_iProviderUserId:Int,_nvILogoImage:String,_nvHeaderImage:String,_nvFooterImage:String,_nvAboutComment:String,_nvCampaignImage:String,_nvSlogen:String,_nvLat:String,_nvLong:String,_ProviderWorkingHoursObj:Array<objWorkingHours>) {
        
        iProviderUserId = _iProviderUserId
        nvILogoImage = _nvILogoImage
        nvHeaderImage = _nvHeaderImage
        nvFooterImage = _nvFooterImage
        nvAboutComment = _nvAboutComment
        nvCampaignImage = _nvCampaignImage
        nvSlogen = _nvSlogen
        nvLat = _nvLat
        nvLong = _nvLong
        ProviderWorkingHoursObj = _ProviderWorkingHoursObj
        
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iProviderUserId"] = Global.sharedInstance.currentProvider.iIdBuisnessDetails
            //Global.sharedInstance.currentProvider.iUserId
        dic["nvILogoImage"] = nvILogoImage
        dic["nvHeaderImage"] = nvHeaderImage
        dic["nvFooterImage"] = nvFooterImage
        dic["nvAboutComment"] = nvAboutComment
        dic["nvCampaignImage"] =  nvCampaignImage
        dic["nvSlogen"] = nvSlogen
        dic["nvLat"] = nvLat
        dic["nvLong"] = nvLong
        dic["ProviderWorkingHoursObj"] = ProviderWorkingHoursObj
        
        return dic
    }
    
    //2do - delete
    func getDicExample()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iProviderUserId"] = 0
        dic["nvILogoImage"] = ""
        dic["nvHeaderImage"] = ""
        dic["nvFooterImage"] = ""
        dic["nvAboutComment"] = "dsfsd g t ret er"
        dic["nvCampaignImage"] =  ""
        dic["nvSlogen"] = "fdfd"
        
        return dic
    }

    func dicToAddProviderBuisnessProfile(dic:Dictionary<String,AnyObject>)->AddProviderBuisnessProfile
    {
        let addProviderBuisnessProfile:AddProviderBuisnessProfile = AddProviderBuisnessProfile()
        
        addProviderBuisnessProfile.iProviderUserId = Global.sharedInstance.parseJsonToInt(dic["iProviderUserId"]!)
        addProviderBuisnessProfile.nvILogoImage = Global.sharedInstance.parseJsonToString(dic["nvILogoImage"]!)
        addProviderBuisnessProfile.nvHeaderImage = Global.sharedInstance.parseJsonToString(dic["nvHeaderImage"]!)
        addProviderBuisnessProfile.nvFooterImage =  Global.sharedInstance.parseJsonToString(dic["nvFooterImage"]!)
        addProviderBuisnessProfile.nvAboutComment =  Global.sharedInstance.parseJsonToString(dic["nvAboutComment"]!)
        addProviderBuisnessProfile.nvCampaignImage =  Global.sharedInstance.parseJsonToString(dic["nvCampaignImage"]!)
        addProviderBuisnessProfile.nvSlogen = Global.sharedInstance.parseJsonToString(dic["nvSlogen"]!)
        addProviderBuisnessProfile.nvLat = Global.sharedInstance.parseJsonToString(dic["nvLat"]!)
        addProviderBuisnessProfile.nvLong = Global.sharedInstance.parseJsonToString(dic["nvLong"]!)
        
        
        let workingHours:objWorkingHours = objWorkingHours()
       if dic["objWorkingHours"]?.description != "<null>"
       {
            addProviderBuisnessProfile.ProviderWorkingHoursObj = workingHours.objWorkingHoursToArray(dic["objWorkingHours"] as! Array<Dictionary<String,AnyObject>>)
       }


        return addProviderBuisnessProfile
    }
    
    
    }
